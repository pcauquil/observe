package fr.ird.observe;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProvider;
import fr.ird.observe.entities.migration.ObserveMigrationEngine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcConfigurationBuilder;
import org.nuiton.topia.service.sql.batch.TopiaSqlBatchServiceImpl;

import java.io.File;

/**
 * Created on 23/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveTopiaConfigurationFactory {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveTopiaConfigurationFactory.class);

    /** l'url d'acces a la base locale */
    public static final String H2_LOCAL_URL =
            "jdbc:h2:file:%s;" +
                    // on peut aussi utiliser file, socket
                    "FILE_LOCK=file;" +
                    //1 or 2 is needed to restore avec crash
                    // 0: logging is disabled (faster),
                    // 1: logging of the data is enabled, but logging of the index
                    // changes is disabled (default), 2: logging of both data and index
                    // changes are enabled
                    "LOG=0;" +
                    // on peut aussi utiliser hsqldb, mysql ou postgresql
                    "MODE=postgresql;" +
                    //"MODE=hsqldb;" +
                    // Sets the default lock timeout (in milliseconds) in this
                    // database that is used for the new sessions.
                    "DEFAULT_LOCK_TIMEOUT=100;" +
                    // -1: the database is never closed until the close delay is set to
                    // some other rev or SHUTDOWN is called., 0: no delay (default; the
                    // database is closed if the last connection to it is closed)., n:
                    // the database is left open for n second after the last connection
                    // is closed.
                    "DB_CLOSE_DELAY=0;" +
                    // 0: no locking (should only be used for testing),
                    // 1: table level locking (default),
                    // 2: table level locking with garbage collection (if the
                    // application does not close all connections).
                    // LOCK_MODE 3 (READ_COMMITTED). Table level locking, but only when
                    // writing (no read locks).
                    "LOCK_MODE=3;" +
                    // Levels: 0=off, 1=error, 2=info, 3=debug.
                    "TRACE_LEVEL_FILE=0;" +
                    // on system.out: 0=off, 1=error, 2=info, 3=debug.
                    "TRACE_LEVEL_SYSTEM_OUT=0;" +
                    // maximumn cache to improve performance...
                    "CACHE_SIZE=65536;" +
                    // avoid timeout on reading tables (see http://stackoverflow.com/questions/4162557/timeout-error-trying-to-lock-table-in-h2)
                    "MVCC=true";

    protected static final JdbcConfigurationBuilder JDBC_CONFIGURATION_BUILDER = new JdbcConfigurationBuilder();

    public static ObserveTopiaConfiguration forPostgresqlDatabase(String jdbcUrl,
                                                                  String username,
                                                                  String password,
                                                                  boolean initSchema,
                                                                  boolean showMigrationSql,
                                                                  boolean showMigrationProgression) {

        JdbcConfiguration jdbcConfiguration = JDBC_CONFIGURATION_BUILDER.forPostgresqlDatabase(jdbcUrl, username, password);

        ObserveTopiaConfiguration topiaConfiguration = createTopiaConfiguration(jdbcConfiguration,
                                                                                initSchema,
                                                                                showMigrationSql,
                                                                                showMigrationProgression,
                                                                                false);

        if (log.isDebugEnabled()) {
            log.debug("PG topia configuration: " + topiaConfiguration);
        }
        return topiaConfiguration;

    }

    public static ObserveTopiaConfiguration forH2Database(File dbDirectory,
                                                          String dbName,
                                                          String username,
                                                          String password,
                                                          boolean initSchema,
                                                          boolean showMigrationSql,
                                                          boolean showMigrationProgression) {

        String dbPath = new File(dbDirectory, dbName).getPath();
        String jdbcUrl = String.format(H2_LOCAL_URL, dbPath);

        JdbcConfiguration jdbcConfiguration = JDBC_CONFIGURATION_BUILDER.forH2Database(jdbcUrl, username, password);

        ObserveTopiaConfiguration topiaConfiguration = createTopiaConfiguration(jdbcConfiguration,
                                                                                initSchema,
                                                                                showMigrationSql,
                                                                                showMigrationProgression,
                                                                                true);

        if (log.isDebugEnabled()) {
            log.debug("H2 topia configuration: " + topiaConfiguration);
        }
        return topiaConfiguration;

    }

    protected static ObserveTopiaConfiguration createTopiaConfiguration(JdbcConfiguration jdbcConfiguration,
                                                                        boolean initSchema,
                                                                        boolean showMigrationSql,
                                                                        boolean showMigrationProgression,
                                                                        boolean h2Configuration) {

        ObserveTopiaConfiguration topiaConfiguration = new ObserveTopiaConfiguration(jdbcConfiguration, h2Configuration);
        topiaConfiguration.setTopiaIdFactoryClass(ObserveTopiaIdFactory.class);
        topiaConfiguration.setInitSchema(initSchema);
        topiaConfiguration.setValidateSchema(false);

        if (log.isDebugEnabled()) {
            log.debug("jdbcUrl: " + topiaConfiguration.getJdbcConnectionUrl());
        }

        ImmutableMap<String, String> migrationServiceConfiguration = ImmutableMap.of(
                ObserveMigrationEngine.MIGRATION_CALLBACK, ObserveMigrationConfigurationProvider.get().getMigrationClassBack(h2Configuration).getName(),
                ObserveMigrationEngine.MIGRATION_SHOW_PROGRESSION, String.valueOf(showMigrationProgression),
                ObserveMigrationEngine.MIGRATION_SHOW_SQL, String.valueOf(showMigrationSql)
        );
        topiaConfiguration.addDeclaredService(ObserveTopiaApplicationContext.MIGRATION_SERVICE_NAME, ObserveMigrationEngine.class, migrationServiceConfiguration);
        topiaConfiguration.addDeclaredService(ObserveTopiaApplicationContext.SQL_BATCH_SERVICE_NAME, TopiaSqlBatchServiceImpl.class);

        return topiaConfiguration;

    }

}
