package fr.ird.observe;

/*-
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlWork;
import org.nuiton.util.GZUtil;
import org.nuiton.util.sql.SqlFileReader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.zip.GZIPInputStream;

/**
 * TODO Move this in ToPIA.
 *
 * Created on 09/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class RunScriptTopiaSqlWork implements TopiaSqlWork {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RunScriptTopiaSqlWork.class);

    protected final byte[] content;
    protected final boolean gzip;
    protected final int batchSize;

    public RunScriptTopiaSqlWork(int batchSize, byte[] content) {
        this.batchSize = batchSize;
        this.content = content;

        if (content == null || content.length == 0) {
            gzip = false;
        } else {
            try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content)) {
                gzip = GZUtil.isGzipStream(byteArrayInputStream);
            } catch (IOException e) {
                throw new TopiaException(e);
            }
        }
    }

    @Override
    public void execute(Connection connection) throws SQLException {

        boolean autoCommit = connection.getAutoCommit();

        try {
            connection.setAutoCommit(false);

            execute0(connection);
        } finally {
            connection.setAutoCommit(autoCommit);
        }
    }

    protected void execute0(Connection connection) throws SQLException {
        try (BufferedReader reader = createReader()) {

            try (Statement statement = connection.createStatement()) {

                int batchSize = 0;

                for (String statementStr : new SqlFileReader(reader)) {

                    String trimLine = statementStr.trim();

                    if (trimLine.isEmpty() || trimLine.startsWith("--")) {
                        continue;
                    }

                    statement.addBatch(trimLine);
                    if (log.isDebugEnabled()) {
                        log.debug("Sql statement: " + trimLine);
                    }
                    batchSize++;

                    if (batchSize % this.batchSize == 0) {
                        flushStatement(statement);
                    }

                }

                flushStatement(statement);
            }

        } catch (IOException e) {
            throw new TopiaException(e);
        }
    }

    protected void flushStatement(Statement statement) throws SQLException {

        statement.executeBatch();
        statement.clearBatch();

    }

    private BufferedReader createReader() {
        try {
            return new BufferedReader(new InputStreamReader(new BufferedInputStream(gzip ? new GZIPInputStream(new ByteArrayInputStream(content)) : new ByteArrayInputStream(content))));
        } catch (IOException e) {
            throw new TopiaException(e);
        }
    }
}
