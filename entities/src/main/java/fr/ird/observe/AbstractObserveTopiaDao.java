package fr.ird.observe;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.ObserveEntity;
import org.nuiton.topia.persistence.internal.AbstractTopiaDao;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

public abstract class AbstractObserveTopiaDao<E extends ObserveEntity> extends AbstractTopiaDao<E> {

    private final GetLastupdateDateSqlQuery getLastUpdateDateSqlQuery;

    protected AbstractObserveTopiaDao() {
        String schemaName = getTopiaEntityEnum().dbSchemaName();
        String tableName = getTopiaEntityEnum().dbTableName();
        getLastUpdateDateSqlQuery = new GetLastupdateDateSqlQuery(schemaName, tableName);
    }

    public Date getLastUpdateDate() {
        return topiaSqlSupport.findSingleResult(getLastUpdateDateSqlQuery);
    }

    public <O> List<O> findAllFromHql(String hql, Map<String, Object> hqlParameters) {
        return findAll(hql, hqlParameters);
    }

    private static class GetLastupdateDateSqlQuery extends TopiaSqlQuery<Timestamp> {

        protected final String sql;

        private GetLastupdateDateSqlQuery(String schemaName, String tableName) {
            this.sql = "SELECT max(" + ObserveEntity.PROPERTY_LAST_UPDATE_DATE + ") FROM " + schemaName + "." + tableName;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            return connection.prepareStatement(sql);
        }

        @Override
        public Timestamp prepareResult(ResultSet set) throws SQLException {
            return set.getTimestamp(1);
        }

    }


}
