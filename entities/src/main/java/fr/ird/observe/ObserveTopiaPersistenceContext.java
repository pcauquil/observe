package fr.ird.observe;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.LastUpdateDate;
import fr.ird.observe.entities.LastUpdateDateTopiaDao;
import fr.ird.observe.entities.ObserveEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

public class ObserveTopiaPersistenceContext extends AbstractObserveTopiaPersistenceContext {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ObserveTopiaPersistenceContext.class);

    public ObserveTopiaPersistenceContext(AbstractTopiaPersistenceContextConstructorParameter parameter) {
        super(parameter);
    }

    public <E extends ObserveEntity> Date getLastUpdateDate(Class<E> entityType) {

        LastUpdateDateTopiaDao dao = getDao(LastUpdateDate.class, LastUpdateDateTopiaDao.class);
        LastUpdateDate lastUpdateDate = dao.findUniqueByType(entityType.getName());
        if (log.isInfoEnabled()) {
            log.info("getLastUpdateDate: " + lastUpdateDate.getLastUpdateDate() + " for entity type: " + entityType.getName());
        }
        return lastUpdateDate.getLastUpdateDate();

    }

    public <E extends ObserveEntity> void updateLastUpdateDate(E entity, Date date) {

        entity.setLastUpdateDate(date);

        // on met à jour l'entité (cela permet de récupérer son topiaId si l'objet est créé)
        getDao(entity).update(entity);

        Class<E> type = getType(entity);
        updateLastUpdateDate(type, date);

    }

    public <E extends ObserveEntity> void updateLastUpdateDate(Class<E> entityType, Date date) {

        LastUpdateDateTopiaDao dao = getDao(LastUpdateDate.class, LastUpdateDateTopiaDao.class);
        String entityTypeName = entityType.getName();
        Optional<LastUpdateDate> optionalLastUpdateDate = Optional.ofNullable(dao.forTypeEquals(entityTypeName).tryFindUnique().orNull());
        LastUpdateDate lastUpdateDate;
        if (!optionalLastUpdateDate.isPresent()) {

            lastUpdateDate = dao.newInstance();
            lastUpdateDate.setLastUpdateDate(date);
            lastUpdateDate.setType(entityTypeName);
            dao.create(lastUpdateDate);
            if (log.isInfoEnabled()) {
                log.info("Add LastUpdateDate: " + date + " for entity type: " + entityTypeName);
            }
        } else {

            lastUpdateDate = optionalLastUpdateDate.get();
            if (log.isInfoEnabled()) {
                log.info("Change LastUpdateDate: " + date + " for entity type: " + entityTypeName);
            }
            lastUpdateDate.setLastUpdateDate(date);
        }

    }

    public void flush() {
        getHibernateSupport().getHibernateSession().flush();
    }

    public <E extends ObserveEntity> Class<E> getType(E entity) {

        ObserveEntityEnum entityEnum = ObserveEntityEnum.valueOf(entity);
        Objects.requireNonNull(entityEnum, "Entity " + entity + " is not managed by ToPIA");
        return (Class<E>) entityEnum.getContract();

    }

    public <E extends ObserveEntity> TopiaDao<E> getDao(E entity) {
        return getDao(getType(entity));
    }

    public long countTable(String fullyTableName) {

        CountTableSqlWork countQuery = new CountTableSqlWork(fullyTableName);
        return getSqlSupport().findSingleResult(countQuery);

    }

    protected class CountTableSqlWork extends TopiaSqlQuery<Long> {

        private final String fullyTableName;

        public CountTableSqlWork(String fullyTableName) {
            this.fullyTableName = fullyTableName;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            String sql = "SELECT count(*) FROM " + fullyTableName;

            return connection.prepareStatement(sql);
        }

        @Override
        public Long prepareResult(ResultSet set) throws SQLException {
            return set.getLong(1);
        }
    }

    public void executeSqlScript(byte... content) {

        getSqlSupport().doSqlWork(new RunScriptTopiaSqlWork(1000, content));
    }

    public void executeSqlScripts(byte[]... contents) {

        getSqlSupport().doSqlWork(new RunScriptsTopiaSqlWork(1000, contents));
    }
}
