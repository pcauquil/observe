package fr.ird.observe;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory;

/**
 * Created on 21/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveTopiaIdFactory extends LegacyTopiaIdFactory {

    private static final long serialVersionUID = 1L;

    public <E extends TopiaEntity> String newTopiaId(Class<E> entityType   ) {

        double random = Math.random();
        while (Double.toString(random).contains("E-")) {
            random = Math.random();
        }
        return newTopiaId(entityType, random+"");
    }
}
