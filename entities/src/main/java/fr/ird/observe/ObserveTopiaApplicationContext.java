package fr.ird.observe;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProvider;
import fr.ird.observe.entities.migration.ObserveMigrationEngine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.support.TopiaMetadataModelSupportImpl;
import org.nuiton.topia.persistence.jdbc.JdbcH2Helper;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;
import org.nuiton.topia.persistence.support.TopiaMetadataModelSupport;
import org.nuiton.topia.service.sql.batch.TopiaSqlBatchService;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTablesFactory;
import org.nuiton.util.StringUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ObserveTopiaApplicationContext extends AbstractObserveTopiaApplicationContext implements TopiaMetadataModelSupport {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ObserveTopiaApplicationContext.class);

    public static final String MIGRATION_SERVICE_NAME = "migration";

    public static final String SQL_BATCH_SERVICE_NAME = "sqlBatch";

    public static final String DB_VERSION = "db.version";

    private static final String CREATE_SCHEMA_SCRIPT
            = "CREATE SCHEMA OBSERVE_COMMON;\n"
            + "CREATE SCHEMA OBSERVE_LONGLINE;\n"
            + "CREATE SCHEMA OBSERVE_SEINE;\n";

    private static final String INSERT_LAST_UPDATE_PATTERN = "INSERT INTO OBSERVE_COMMON.LASTUPDATEDATE (TOPIAID, TOPIAVERSION, TOPIACREATEDATE, TYPE, LASTUPDATEDATE) VALUES ('fr.ird.observe.entities.LastUpdateDate#1236861982132#0.%03d', 0, CURRENT_TIMESTAMP, '%s', CURRENT_TIMESTAMP);";

    /**
     * Mise à {@code true} quand au moins une connection a été effectuée.
     */
    protected boolean open;

    protected final String authenticationToken;
    protected final TopiaMetadataModelSupport topiaMetadataModelSupport;
    protected final TopiaSqlTablesFactory topiaSqlTablesFactory;
    protected final ObserveMigrationConfigurationProvider observeMigrationConfigurationProvider;
    protected TopiaSqlTables tripSeineTables;
    protected TopiaSqlTables tripLonglineTables;
    protected TopiaSqlTables referentialTables;

    public ObserveTopiaApplicationContext(ObserveTopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
        this.authenticationToken = UUID.randomUUID().toString();
        this.topiaMetadataModelSupport = new TopiaMetadataModelSupportImpl("fr.ird.observe", "Observe");
        this.topiaSqlTablesFactory = new TopiaSqlTablesFactory(getMetadataModel(), this);
        this.observeMigrationConfigurationProvider = ObserveMigrationConfigurationProvider.get();
    }

    @Override
    public ObserveTopiaConfiguration getConfiguration() {
        return (ObserveTopiaConfiguration) super.getConfiguration();
    }

    public boolean isOpen() {
        return isOpened() && open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public ObserveMigrationConfigurationProvider getObserveMigrationConfigurationProvider() {
        return observeMigrationConfigurationProvider;
    }

    public ObserveMigrationEngine getMigrationService() {
        return getServices(ObserveMigrationEngine.class).get(MIGRATION_SERVICE_NAME);
    }

    public TopiaSqlBatchService getSqlBatchService() {
        return getServices(TopiaSqlBatchService.class).get(SQL_BATCH_SERVICE_NAME);
    }

    @Override
    public void createSchema() {
        try {
            boolean showSchema = false;
            if (log.isDebugEnabled()) {
                showSchema = true;
            }
            topiaFiresSupport.firePreCreateSchema(this);
            try (ObserveTopiaPersistenceContext topiaPersistenceContext = newPersistenceContext()) {
                topiaPersistenceContext.getSqlSupport().executeSql(CREATE_SCHEMA_SCRIPT);
                topiaPersistenceContext.commit();
            }

            Configuration hibernateConfiguration = getHibernateProvider().getHibernateConfiguration();
            new SchemaExport(hibernateConfiguration).execute(showSchema, true, false, true);

            topiaFiresSupport.firePostCreateSchema(this);
        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could not create schema for reason: %s", eee.getMessage()), eee);
        }
    }

    public void insertLastUpdateDate() {
        try {

            // alimentation de la table lastUpdate
            try (ObserveTopiaPersistenceContext topiaPersistenceContext = newPersistenceContext()) {

                String sql = Stream.concat(Entities.REFERENCE_ENTITIES_LIST.stream(), Entities.DATA_ENTITIES_LIST.stream())
                                   .map(entity -> String.format(INSERT_LAST_UPDATE_PATTERN, entity.ordinal(), entity.getContract().getCanonicalName()))
                                   .collect(Collectors.joining("\n"));

                topiaPersistenceContext.getSqlSupport().executeSql(sql);
                topiaPersistenceContext.commit();
            }


        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could insert lastupdateDate for reason: %s", eee.getMessage()), eee);
        }

    }

    public void executeSqlStatements(byte... content) {

        try {
            executeSqlStatements0(content);
        } catch (IOException e) {
            throw new TopiaException(e);
        }

    }

    protected void executeSqlStatements0(byte... content) throws IOException {

        if (getConfiguration().isH2Configuration()) {

            Path tempFile = Files.createTempFile("observeRestoreH2", ".sql");
            log.info(String.format("Restore script size: %s to h2 from file : %s", StringUtil.convertMemory(content.length), tempFile.toString()));
            try {
                Files.write(tempFile, content);
                JdbcH2Helper jdbcH2Helper = new JdbcH2Helper(configuration);
                jdbcH2Helper.restore(tempFile.toFile());
            } finally {
                Files.delete(tempFile);
            }

        } else {

            log.info(String.format("Load script (size: %s)", StringUtil.convertMemory(content.length)));

            try (ObserveTopiaPersistenceContext persistenceContext = newPersistenceContext()) {

                persistenceContext.executeSqlScript(content);
                persistenceContext.commit();
            }
        }

    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    @Override
    public TopiaMetadataModel getMetadataModel() {
        return topiaMetadataModelSupport.getMetadataModel();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObserveTopiaApplicationContext)) return false;
        ObserveTopiaApplicationContext that = (ObserveTopiaApplicationContext) o;
        return Objects.equals(authenticationToken, that.authenticationToken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authenticationToken);
    }

    @Override
    public void close() {
        super.close();

        ObserveTopiaConfiguration topiaConfiguration = getConfiguration();

        if (topiaConfiguration.isH2Configuration()) {

            if (log.isDebugEnabled()) {
                log.debug("Shutdown h2 database");
            }
            // Fermer proprement la base
            JdbcHelper jdbcHelper = new JdbcHelper(topiaConfiguration);
            jdbcHelper.runUpdate("SHUTDOWN COMPACT;");

        }

    }

    public TopiaSqlTables getTripSeineTables() {

        if (tripSeineTables == null) {
            log.info("Loading tripSeineTables.");
            tripSeineTables = topiaSqlTablesFactory.newReplicateEntityTables(
                    new TripReplicateTablesPredicate(),
                    ObserveEntityEnum.TripSeine);
        }
        return tripSeineTables;

        /*
        return TopiaSqlTables.builder(filterTable, ObserveEntityEnum.TripSeine) // → TripSeine
                .addAndEnterJoinTable(ObserveEntityEnum.GearUseFeaturesSeine) // → GearUseFeaturesSeine
                .addJoinTable(ObserveEntityEnum.GearUseFeaturesMeasurementSeine)
                .backToParent() // ← TripSeine
                .addAndEnterJoinTable(ObserveEntityEnum.Route) // → Route
                .addAndEnterJoinTable(ObserveEntityEnum.ActivitySeine) // → ActivitySeine
                .addAndEnterReverseJoinTable(ObserveEntityEnum.SetSeine) // → SetSeine
                .addJoinTable(ObserveEntityEnum.SchoolEstimate)
                .addJoinTable(ObserveEntityEnum.NonTargetCatch)
                .addJoinTable(ObserveEntityEnum.TargetCatch)
                .addAndEnterJoinTable(ObserveEntityEnum.TargetSample) // → TargetSample
                .addJoinTable(ObserveEntityEnum.TargetLength)
                .backToParent() // ← SetSeine
                .addAndEnterJoinTable(ObserveEntityEnum.NonTargetSample) // → NonTargetSample
                .addJoinTable(ObserveEntityEnum.NonTargetLength)
                .backToParent() // ← SetSeine
                .backToParent() // ← ActivitySeine
                .addAssociationTable(ObserveEntityEnum.ObservedSystem.name(), true)
                .addAndEnterJoinTable(ObserveEntityEnum.FloatingObject) // → FloatingObject
                .addJoinTable(ObserveEntityEnum.ObjectObservedSpecies)
                .addJoinTable(ObserveEntityEnum.ObjectSchoolEstimate)
                .addJoinTable(ObserveEntityEnum.TransmittingBuoy)
                .build();
         */

    }

    public TopiaSqlTables getTripLonglineTables() {

        if (tripLonglineTables == null) {
            log.info("Loading tripLonglineTables.");
            tripLonglineTables = topiaSqlTablesFactory.newReplicateEntityTables(
                    new TripReplicateTablesPredicate(),
                    ObserveEntityEnum.TripLongline);
        }
        return tripLonglineTables;

        /*
        return TopiaSqlTables.builder(filterTable, ObserveEntityEnum.TripLongline) // → TripLongline
                .addAndEnterJoinTable(ObserveEntityEnum.GearUseFeaturesLongline) // → GearUseFeaturesLongline
                .addJoinTable(ObserveEntityEnum.GearUseFeaturesMeasurementLongline)
                .backToParent() // ← TripLongline
                .addAndEnterJoinTable(ObserveEntityEnum.ActivityLongline) // → ActivityLongline
                .addAndEnterReverseJoinTable(ObserveEntityEnum.SetLongline) // → SetLongline
                .addJoinTable(ObserveEntityEnum.BaitsComposition)
                .addJoinTable(ObserveEntityEnum.FloatlinesComposition)
                .addJoinTable(ObserveEntityEnum.HooksComposition)
                .addJoinTable(ObserveEntityEnum.BranchlinesComposition)
                .addAssociationTable(ObserveEntityEnum.MitigationType.name(), true)
                .addAndEnterJoinTable(ObserveEntityEnum.Section) // → Section
                .addAndEnterJoinTable(ObserveEntityEnum.Basket) // → Basket
                .addJoinTable(ObserveEntityEnum.Branchline)
                .backToParent() // ← Section
                .backToParent() // ← SetLongline
                .addAndEnterJoinTable(ObserveEntityEnum.CatchLongline) // → CatchLongline
                .addJoinTable(ObserveEntityEnum.SizeMeasure)
                .addJoinTable(ObserveEntityEnum.WeightMeasure)
                .addAssociationTable("Predator", true)
                .backToParent() // ← SetLongline
                .addAndEnterJoinTable(ObserveEntityEnum.Tdr) // → Tdr
                .addJoinTable(ObserveEntityEnum.TdrRecord)
                .addAssociationTable(ObserveEntityEnum.Species.name(), true)
                .backToParent() // ← SetLongline
                .backToParent() // ← ActivityLongline
                .addJoinTable(ObserveEntityEnum.Encounter)
                .addJoinTable(ObserveEntityEnum.SensorUsed)
                .build();
         */
    }

    public TopiaSqlTables getReferentialTables() {

        if (referentialTables == null) {
            log.info("Loading referentialTables.");
            referentialTables = topiaSqlTablesFactory.newReplicateEntityTables(
                    new TripReplicateTablesPredicate(),
                    Entities.REFERENCE_ENTITIES);
//                    ObserveEntityEnum.VesselSizeCategory,
//                    ObserveEntityEnum.Country,
//                    ObserveEntityEnum.Harbour,
//                    ObserveEntityEnum.VesselType,
//                    ObserveEntityEnum.Vessel,
//                    ObserveEntityEnum.Ocean,
//                    ObserveEntityEnum.SpeciesGroup,
//                    ObserveEntityEnum.Species,
//                    ObserveEntityEnum.Sex,
//                    ObserveEntityEnum.FpaZone,
//                    ObserveEntityEnum.SpeciesList,
//                    ObserveEntityEnum.Person,
//                    ObserveEntityEnum.Organism,
//                    ObserveEntityEnum.LengthWeightParameter,
//                    ObserveEntityEnum.Program,
//                    ObserveEntityEnum.GearCaracteristicType,
//                    ObserveEntityEnum.GearCaracteristic,
//                    ObserveEntityEnum.Gear,
//                    ObserveEntityEnum.LastUpdateDate,
//                    ObserveEntityEnum.VesselActivitySeine,
//                    ObserveEntityEnum.SurroundingActivity,
//                    ObserveEntityEnum.ReasonForNullSet,
//                    ObserveEntityEnum.ReasonForNoFishing,
//                    ObserveEntityEnum.SpeciesFate,
//                    ObserveEntityEnum.ObjectFate,
//                    ObserveEntityEnum.WeightCategory,
//                    ObserveEntityEnum.DetectionMode,
//                    ObserveEntityEnum.TransmittingBuoyOperation,
//                    ObserveEntityEnum.ObjectOperation,
//                    ObserveEntityEnum.ReasonForDiscard,
//                    ObserveEntityEnum.SpeciesStatus,
//                    ObserveEntityEnum.ObservedSystem,
//                    ObserveEntityEnum.TransmittingBuoyType,
//                    ObserveEntityEnum.ObjectType,
//                    ObserveEntityEnum.Wind,
//                    ObserveEntityEnum.BaitHaulingStatus,
//                    ObserveEntityEnum.BaitSettingStatus,
//                    ObserveEntityEnum.BaitType,
//                    ObserveEntityEnum.CatchFateLongline,
//                    ObserveEntityEnum.EncounterType,
//                    ObserveEntityEnum.Healthness,
//                    ObserveEntityEnum.HookPosition,
//                    ObserveEntityEnum.HookSize,
//                    ObserveEntityEnum.HookType,
//                    ObserveEntityEnum.ItemVerticalPosition,
//                    ObserveEntityEnum.ItemHorizontalPosition,
//                    ObserveEntityEnum.LightsticksColor,
//                    ObserveEntityEnum.LightsticksType,
//                    ObserveEntityEnum.LineType,
//                    ObserveEntityEnum.MaturityStatus,
//                    ObserveEntityEnum.MitigationType,
//                    ObserveEntityEnum.SensorBrand,
//                    ObserveEntityEnum.SensorDataFormat,
//                    ObserveEntityEnum.SensorType,
//                    ObserveEntityEnum.SettingShape,
//                    ObserveEntityEnum.SizeMeasureType,
//                    ObserveEntityEnum.StomacFullness,
//                    ObserveEntityEnum.TripType,
//                    ObserveEntityEnum.VesselActivityLongline,
//                    ObserveEntityEnum.WeightMeasureType);
        }
        return referentialTables;

        /*

        return TopiaSqlTables.builder()
                .addMainTable(ObserveEntityEnum.VesselSizeCategory) // → VesselSizeCategory
                .addMainTable(ObserveEntityEnum.Country) // → Country
                .addMainTable(ObserveEntityEnum.Harbour) // → Harbour
                .addMainTable(ObserveEntityEnum.VesselType) // → VesselType
                .addMainTable(ObserveEntityEnum.Vessel) // → Vessel
                .addMainTable(ObserveEntityEnum.Ocean) // → Ocean
                .addMainTable(ObserveEntityEnum.SpeciesGroup) // → SpeciesGroup
                .addMainTable(ObserveEntityEnum.Species) // → Species
                .addAssociationTable(ObserveEntityEnum.Ocean.name(), false)
                .addMainTable(ObserveEntityEnum.Sex) // → Sex
                .addMainTable(ObserveEntityEnum.FpaZone) // → FpaZone
                .addMainTable(ObserveEntityEnum.SpeciesList) // → SpeciesList
                .addAssociationTable(ObserveEntityEnum.Species.name(), false)
                .addMainTable(ObserveEntityEnum.Person) // → Person
                .addMainTable(ObserveEntityEnum.Organism) // → Organism
                .addMainTable(ObserveEntityEnum.LengthWeightParameter) // → LengthWeightParameter
                .addMainTable(ObserveEntityEnum.Program) // → Program
                .addMainTable(ObserveEntityEnum.GearCaracteristicType) // → GearCaracteristicType
                .addMainTable(ObserveEntityEnum.GearCaracteristic) // → GearCaracteristic
                .addMainTable(ObserveEntityEnum.Gear) // → Gear
                .addAssociationTable(ObserveEntityEnum.GearCaracteristic.name(), false)
                .addMainTable(ObserveEntityEnum.LastUpdateDate) // → LastUpdateDate
                .addMainTable(ObserveEntityEnum.VesselActivitySeine) // → VesselActivitySeine
                .addMainTable(ObserveEntityEnum.SurroundingActivity) // → SurroundingActivity
                .addMainTable(ObserveEntityEnum.ReasonForNullSet) // → ReasonForNullSet
                .addMainTable(ObserveEntityEnum.ReasonForNoFishing) // → ReasonForNoFishing
                .addMainTable(ObserveEntityEnum.SpeciesFate) // → SpeciesFate
                .addMainTable(ObserveEntityEnum.ObjectFate) // → ObjectFate
                .addMainTable(ObserveEntityEnum.WeightCategory) // → WeightCategory
                .addMainTable(ObserveEntityEnum.DetectionMode) // → DetectionMode
                .addMainTable(ObserveEntityEnum.TransmittingBuoyOperation) // → TransmittingBuoyOperation
                .addMainTable(ObserveEntityEnum.ObjectOperation) // → ObjectOperation
                .addMainTable(ObserveEntityEnum.ReasonForDiscard) // → ReasonForDiscard
                .addMainTable(ObserveEntityEnum.SpeciesStatus) // → SpeciesStatus
                .addMainTable(ObserveEntityEnum.ObservedSystem) // → ObservedSystem
                .addMainTable(ObserveEntityEnum.TransmittingBuoyType) // → TransmittingBuoyType
                .addMainTable(ObserveEntityEnum.ObjectType) // → ObjectType
                .addMainTable(ObserveEntityEnum.Wind) // → Wind
                .addMainTable(ObserveEntityEnum.BaitHaulingStatus) // → BaitHaulingStatus
                .addMainTable(ObserveEntityEnum.BaitSettingStatus) // → BaitSettingStatus
                .addMainTable(ObserveEntityEnum.BaitType) // → BaitType
                .addMainTable(ObserveEntityEnum.CatchFateLongline) // → CatchFateLongline
                .addMainTable(ObserveEntityEnum.EncounterType) // → EncounterType
                .addMainTable(ObserveEntityEnum.Healthness) // → Healthness
                .addMainTable(ObserveEntityEnum.HookPosition) // → HookPosition
                .addMainTable(ObserveEntityEnum.HookSize) // → HookSize
                .addMainTable(ObserveEntityEnum.HookType) // → HookType
                .addMainTable(ObserveEntityEnum.ItemVerticalPosition) // → ItemVerticalPosition
                .addMainTable(ObserveEntityEnum.ItemHorizontalPosition) // → ItemHorizontalPosition
                .addMainTable(ObserveEntityEnum.LightsticksColor) // → LightsticksColor
                .addMainTable(ObserveEntityEnum.LightsticksType) // → LightsticksType
                .addMainTable(ObserveEntityEnum.LineType) // → LineType
                .addMainTable(ObserveEntityEnum.MaturityStatus) // → MaturityStatus
                .addMainTable(ObserveEntityEnum.MitigationType) // → MitigationType
                .addMainTable(ObserveEntityEnum.SensorBrand) // → SensorBrand
                .addMainTable(ObserveEntityEnum.SensorDataFormat) // → SensorDataFormat
                .addMainTable(ObserveEntityEnum.SensorType) // → SensorType
                .addMainTable(ObserveEntityEnum.SettingShape) // → SettingShape
                .addMainTable(ObserveEntityEnum.SizeMeasureType) // → SizeMeasureType
                .addMainTable(ObserveEntityEnum.StomacFullness) // → StomacFullness
                .addMainTable(ObserveEntityEnum.TripType) // → TripType
                .addMainTable(ObserveEntityEnum.VesselActivityLongline) // → VesselActivityLongline
                .addMainTable(ObserveEntityEnum.WeightMeasureType) // → WeightMeasureType

                .build();
*/
    }

    private static class TripReplicateTablesPredicate implements TopiaSqlTablesFactory.TopiaSqlTablesPredicate {

        protected final Set<TopiaMetadataEntity> dones = new LinkedHashSet<>();
        protected final Set<String> sectionsHolders = ImmutableSet.of(
                ObserveEntityEnum.CatchLongline.name(),
                ObserveEntityEnum.Tdr.name()
        );
        protected final Set<String> sections = ImmutableSet.of(
                ObserveEntityEnum.Branchline.name(),
                ObserveEntityEnum.Basket.name(),
                ObserveEntityEnum.Section.name()
        );

        @Override
        public boolean acceptEntity(TopiaMetadataEntity metadataEntity) {
            return dones.add(metadataEntity);
        }

        @Override
        public boolean acceptAssociation(TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

            return !(sectionsHolders.contains(propertyType.getType()) && sections.contains(metadataEntity.getType()));

        }

        @Override
        public boolean acceptReversedAssociation(TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            return true;
        }

        @Override
        public boolean acceptNmAssociation(TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            return true;
        }

    }
}
