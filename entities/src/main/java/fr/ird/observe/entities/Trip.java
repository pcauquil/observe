package fr.ird.observe.entities;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Person;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.referentiel.Vessel;
import fr.ird.observe.entities.seine.TripSeine;

import java.util.Date;

/**
 * A contract to place over {@link TripSeine} and {@link TripLongline}.
 *
 * Created on 8/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public interface Trip extends CommentableEntity {

    String PROPERTY_START_DATE = "startDate";

    String PROPERTY_END_DATE = "endDate";

    String PROPERTY_OBSERVER = "observer";

    String PROPERTY_CAPTAIN = "captain";

    String PROPERTY_DATA_ENTRY_OPERATOR = "dataEntryOperator";

    String PROPERTY_VESSEL = "vessel";

    String PROPERTY_PROGRAM = "program";

    void setHistoricalData(boolean historicalData);

    boolean isHistoricalData();

    void setStartDate(Date startDate);

    Date getStartDate();

    void setEndDate(Date endDate);

    Date getEndDate();

    void setObserver(Person observer);

    Person getObserver();

    String getObserverLabel();

    void setVessel(Vessel vessel);

    Vessel getVessel();

    void setCaptain(Person captain);

    Person getCaptain();

    void setDataEntryOperator(Person dataEntryOperator);

    Person getDataEntryOperator();

    void setProgram(Program program);

    Program getProgram();

    Ocean getOcean();

}
