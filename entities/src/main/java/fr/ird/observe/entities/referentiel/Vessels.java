package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Set;
import java.util.function.Predicate;

/**
 * Created on 11/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class Vessels {

    //FIXME
    private static final ImmutableSet<String> LONGLINE_VESSEL_TYPE_IDS = ImmutableSet.copyOf(
            Sets.newHashSet("fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.044156847891821505",
                    "fr.ird.observe.entities.referentiel.VesselType#1239832675736#0.8708229847859869",
                    "fr.ird.observe.entities.referentiel.VesselType#1239832686137#0.1"));

    //FIXME
    private static final ImmutableSet<String> SEINE_VESSEL_TYPE_IDS = ImmutableSet.copyOf(
            Sets.newHashSet("fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.044156847891821505",
                    "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.307197212385357",
                    "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.7380146830307519",
                    "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.9086075071905084",
                    "fr.ird.observe.entities.referentiel.VesselType#1239832675737#0.43324169605639407"));


    public static Predicate<Vessel> newVesselLonglinePredicate() {

        return new VesselByVesselTypeIdPredicate(LONGLINE_VESSEL_TYPE_IDS);

    }

    public static Predicate<Vessel> newVesselSeinePredicate() {

        return new VesselByVesselTypeIdPredicate(SEINE_VESSEL_TYPE_IDS);

    }

    private static class VesselByVesselTypeIdPredicate implements Predicate<Vessel> {

        final Set<String> vesselTypeIds;

        private VesselByVesselTypeIdPredicate(Set<String> vesselTypeIds) {
            this.vesselTypeIds = vesselTypeIds;
        }

        @Override
        public boolean test(Vessel input) {
            return vesselTypeIds.contains(input.getVesselType().getTopiaId());
        }
    }
}
