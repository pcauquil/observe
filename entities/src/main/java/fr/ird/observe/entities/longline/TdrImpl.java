package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.util.DateUtil;

import java.util.Date;

public class TdrImpl extends TdrAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public Date getDeployementStartDate() {
        return deployementStart == null ? null : DateUtil.getDay(deployementStart);
    }

    @Override
    public Date getDeployementStartTime() {
        return deployementStart == null ? null : DateUtil.getTime(deployementStart, false, false);
    }

    @Override
    public Date getDeployementEndDate() {
        return deployementEnd == null ? null : DateUtil.getDay(deployementEnd);
    }

    @Override
    public Date getDeployementEndTime() {
        return deployementEnd == null ? null : DateUtil.getTime(deployementEnd, false, false);
    }

    @Override
    public Date getFishingStartDate() {
        return fishingStart == null ? null : DateUtil.getDay(fishingStart);
    }

    @Override
    public Date getFishingStartTime() {
        return fishingStart == null ? null : DateUtil.getTime(fishingStart, false, false);
    }

    @Override
    public Date getFishingEndDate() {
        return fishingEnd == null ? null : DateUtil.getDay(fishingEnd);
    }

    @Override
    public Date getFishingEndTime() {
        return fishingEnd == null ? null : DateUtil.getTime(fishingEnd, false, false);
    }

    @Override
    public void setDeployementStartDate(Date date) {
        if (deployementStart != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, deployementStart, true, false);
            setDeployementStart(dateAndTime);
        }
    }

    @Override
    public void setDeployementStartTime(Date time) {
        if (deployementStart != null) {
            Date dateAndTime = DateUtil.getDateAndTime(deployementStart, time, false, false);
            setDeployementStart(dateAndTime);
        }
    }

    @Override
    public void setDeployementEndDate(Date date) {
        if (deployementEnd != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, deployementEnd, true, false);
            setDeployementEnd(dateAndTime);
        }
    }

    @Override
    public void setDeployementEndTime(Date time) {
        if (deployementEnd != null) {
            Date dateAndTime = DateUtil.getDateAndTime(deployementEnd, time, false, false);
            setDeployementEnd(dateAndTime);
        }
    }

    @Override
    public void setFishingStartDate(Date date) {
        if (fishingStart != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, fishingStart, true, false);
            setFishingStart(dateAndTime);
        }
    }

    @Override
    public void setFishingStartTime(Date time) {
        if (fishingStart != null) {
            Date dateAndTime = DateUtil.getDateAndTime(fishingStart, time, false, false);
            setFishingStart(dateAndTime);
        }
    }

    @Override
    public void setFishingEndDate(Date date) {
        if (fishingEnd != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, fishingEnd, true, false);
            setFishingEnd(dateAndTime);
        }
    }

    @Override
    public void setFishingEndTime(Date time) {
        if (fishingEnd != null) {
            Date dateAndTime = DateUtil.getDateAndTime(fishingEnd, time, false, false);
            setFishingEnd(dateAndTime);
        }
    }
}
