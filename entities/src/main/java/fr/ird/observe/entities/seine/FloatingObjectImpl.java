/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.seine;

import com.google.common.collect.Iterables;
import fr.ird.observe.entities.constants.seine.TypeTransmittingBuoyOperationPersist;
import fr.ird.observe.entities.referentiel.seine.TransmittingBuoyOperation;

/** Implantation des objectOperations pour l'entité FloatingObject. */
public class FloatingObjectImpl extends FloatingObjectAbstract {

    private static final long serialVersionUID = 1L;

    /**
     * Récupère le type d'objectOperation effectué sur les balises du DCP.
     *
     * @return l'ordinal de l'énumeration {@link TypeTransmittingBuoyOperationPersist}
     * @see TypeTransmittingBuoyOperationPersist
     */
    @Override
    public TypeTransmittingBuoyOperationPersist getTypeTransmittingBuoyOperation() {
        if (isTransmittingBuoyEmpty()) {

            // pas de balise lié au dcp.
            return TypeTransmittingBuoyOperationPersist.pasDeBalise;
        }
        if (transmittingBuoy.size() == 1) {

            // une seule balise lue sur le DCP, on peut avoir les cas suivants :
            // - visite simple
            // - récuperation
            // - pose d'une nouvelle balise
            TransmittingBuoy balise = Iterables.get(transmittingBuoy, 0);
            TransmittingBuoyOperation objectOperation = balise.getTransmittingBuoyOperation();
            switch (Integer.valueOf(objectOperation.getCode())) {
                case 1:

                    // visite
                    return TypeTransmittingBuoyOperationPersist.visite;
                case 2:

                    // recuperation
                    return TypeTransmittingBuoyOperationPersist.recuperation;
                case 3:

                    // mise a l'eau d'une nouvelle balise
                    return TypeTransmittingBuoyOperationPersist.pose;
                default:
                    throw new IllegalStateException("objectOperation must be between 1 to 3, but was " + objectOperation.getCode());
            }
        }
        if (transmittingBuoy.size() == 2) {

            // deux balises lues pour le DCP, on peut avoir un seul cas :
            // - récupération et remplacement
            return TypeTransmittingBuoyOperationPersist.recuperationEtRemplacement;
        }
        throw new IllegalStateException("on a dcp, can only have 0, 1 or 2 balise lues");

    }
}
