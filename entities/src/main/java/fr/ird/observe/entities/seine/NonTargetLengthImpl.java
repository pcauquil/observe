/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.seine;

import org.nuiton.util.NumberUtil;


public class NonTargetLengthImpl extends NonTargetLengthAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public void setWeight(Float weight) {
        if (weight != null) {

            // on arrondit à trois décimale
            weight = NumberUtil.roundTwoDigits(weight);
        }
        super.setWeight(weight);
    }

    @Override
    public void setLength(Float length) {
        if (length != null) {

            // on arrondit à 1 décimale
            length = NumberUtil.roundOneDigit(length);
        }
        super.setLength(length);
    }

}
