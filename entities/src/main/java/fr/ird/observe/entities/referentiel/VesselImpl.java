/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.referentiel;


import org.nuiton.util.NumberUtil;

public class VesselImpl extends VesselAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public void setCapacity(Float capacity) {
        // on arrondit à 2 décimales
        if (capacity != null) {
            capacity = NumberUtil.roundTwoDigits(capacity);
        }
        super.setCapacity(capacity);
    }

    @Override
    public void setSearchMaximum(Float searchMaximum) {
        // on arrondit à 2 décimales
        if (searchMaximum != null) {
            searchMaximum = NumberUtil.roundTwoDigits(searchMaximum);
        }
        super.setSearchMaximum(searchMaximum);
    }

    @Override
    public void setLength(Float length) {
        // on arrondit à 2 décimales
        if (length != null) {
            length = NumberUtil.roundTwoDigits(length);
        }
        super.setLength(length);
    }
}
