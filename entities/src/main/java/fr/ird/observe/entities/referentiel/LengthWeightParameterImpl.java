/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.referentiel;

import java.util.Map;
import java.util.Set;

public class LengthWeightParameterImpl extends LengthWeightParameterAbstract {

    private static final long serialVersionUID = 1L;

    protected Map<String, Double> coefficientValues;

    protected Boolean lengthWeightFormulaValid;

    protected Boolean weightLengthFormulaValid;

    //FIXME not generated with maven-release ?
    public static final String PROPERTY_LENGTH_WEIGHT_FORMULA_VALID = "lengthWeightFormulaValid";

    //FIXME not generated with maven-release ?
    public static final String PROPERTY_WEIGHT_LENGTH_FORMULA_VALID = "weightLengthFormulaValid";

    @Override
    public String getCode() {
        // pas utilise
        return null;
    }

    @Override
    public void setCode(String code) {
        // pas utilise
    }

    @Override
    public void setCoefficients(String value) {
        super.setCoefficients(value);
        coefficientValues = null;
        revalidateRelationPoids();
        revalidateRelationTaille();
    }

    @Override
    public Set<String> getCoefficientNames() {
        return getCoefficientValues().keySet();
    }

    @Override
    public Double getCoefficientValue(String coefficientName) {
        return getCoefficientValues().get(coefficientName);
    }

    @Override
    public Map<String, Double> getCoefficientValues() {
        if (coefficientValues == null) {
            coefficientValues =
                    LengthWeightParemeterHelper.getCoefficientValues(this);
        }
        return coefficientValues;
    }

    @Override
    public void setLengthWeightFormula(String value) {
        super.setLengthWeightFormula(value);
        revalidateRelationPoids();
    }

    @Override
    public void setWeightLengthFormula(String value) {
        super.setWeightLengthFormula(value);
        revalidateRelationTaille();
    }

    @Override
    public boolean isLengthWeightFormulaValid() {
        if (lengthWeightFormulaValid == null) {
            revalidateRelationPoids();
        }
        return lengthWeightFormulaValid != null && lengthWeightFormulaValid;
    }

    @Override
    public boolean isWeightLengthFormulaValid() {
        if (weightLengthFormulaValid == null) {
            revalidateRelationTaille();
        }
        return weightLengthFormulaValid != null && weightLengthFormulaValid;
    }

    @Override
    public void setLengthWeightFormulaValid(boolean lengthWeightFormulaValid) {
        this.lengthWeightFormulaValid = lengthWeightFormulaValid;
        fireOnPostWrite(PROPERTY_LENGTH_WEIGHT_FORMULA_VALID, null, lengthWeightFormulaValid);
    }

    @Override
    public void setWeightLengthFormulaValid(boolean weightLengthFormulaValid) {
        this.weightLengthFormulaValid = weightLengthFormulaValid;
        fireOnPostWrite(PROPERTY_WEIGHT_LENGTH_FORMULA_VALID, null, weightLengthFormulaValid);
    }

    @Override
    public Float computeWeight(float length) {
        return LengthWeightParemeterHelper.computeWeight(this, length);
    }

    @Override
    public Float computeLength(float weight) {
        return LengthWeightParemeterHelper.computeLength(this, weight);
    }

    protected void revalidateRelationPoids() {
        lengthWeightFormulaValid = null;

        // validate equation
        boolean result = LengthWeightParemeterHelper.validateWeightRelation(this);
        setLengthWeightFormulaValid(result);
    }

    protected void revalidateRelationTaille() {
        weightLengthFormulaValid = null;

        // validate equation
        boolean result = LengthWeightParemeterHelper.validateLengthRelation(this);
        setWeightLengthFormulaValid(result);
    }

}
