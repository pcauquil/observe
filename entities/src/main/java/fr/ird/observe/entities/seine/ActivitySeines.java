package fr.ird.observe.entities.seine;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.seine.ObservedSystem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created on 8/25/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class ActivitySeines {

    /**
     * Comparateur de {@link ActivitySeine} basé sur la propriété {@link
     * ActivitySeine#getTime()}.
     */
    public static final Comparator<ActivitySeine> ACTIVITE_COMPARATOR =
            (o1, o2) -> o1.getTime().compareTo(
                    o2.getTime());

    /** Logger. */
    private static final Log log = LogFactory.getLog(ActivitySeines.class);

    public static String decorate(int referenceLocale, ActivitySeine activitySeine) {

        return String.format("%1$tH:%1$tM", activitySeine.getTime())
               + " - " +
               I18nReferenceEntities.getLabel(referenceLocale,activitySeine.getVesselActivitySeine());

    }

    public static ActivitySeine getPreviousActivity(Route route, ActivitySeine activitySeine) {

        List<ActivitySeine> activitySeines = Lists.newArrayList(route.getActivitySeine());
        return getPreviousActivity(activitySeines, activitySeine);
    }

    public static ActivitySeine getPreviousActivity(List<ActivitySeine> activitySeines, ActivitySeine activitySeine) {

        if (activitySeine == null) {
            if (log.isInfoEnabled()) {
                log.info("no editing activity");
            }
            // pas d'activity courante
            return null;
        }

        if (activitySeines == null || activitySeines.size() < 2) {
            // pas plus de 2 activitys, donc pas d'activity precedente
            return null;
        }
        sort(activitySeines);
        ActivitySeine previous = null;
        for (ActivitySeine a : activitySeines) {
            if (a.getTime().getTime() <
                activitySeine.getTime().getTime()) {
                previous = a;
                continue;
            }
            // l'activity precedente a ete trouvee
            break;
        }
        if (previous != null) {
            if (log.isDebugEnabled()) {
                log.debug("previous activity " +
                          previous.getTime());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("no previous activity for " + activitySeine);
            }
        }
        return previous;
    }

    public static void sort(List<ActivitySeine> activitySeines) {
        Collections.sort(activitySeines, ACTIVITE_COMPARATOR);
    }

    /**
     * Calcule le type de banc d'une activité.
     *
     * par défaut, on considère la caléee sur BL.
     *
     * Si l'activité possède au moins un système observe de type BO alors la
     * caléee est de type BO.
     *
     * @param a l'activity a inspecter
     * @return le type de banc a appliquer
     * @since 1.5
     */
    public static SchoolTypePersist getSchoolType(ActivitySeine a) {

        // par defaut, on suppose que la set est sur BL
        SchoolTypePersist type = SchoolTypePersist.libre;

        if (!a.isObservedSystemEmpty()) {

            SchoolTypePersist boCode = SchoolTypePersist.objet;

            // des systèmes observés sont connus, si un des système est sur
            // banc objet, alors la set est considérée sur BO
            for (ObservedSystem s : a.getObservedSystem()) {
                if (boCode == s.getSchoolType()) {

                    // set sur BO
                    type = SchoolTypePersist.objet;
                    break;
                }
            }
        }
        return type;
    }

    public static ActivitySeine getLastActivityDebutDePechePositiveBefore(Route route, Date actitiveDebut) {

        if (route.isActivitySeineEmpty()) {

            // pas d'actitive dans la route
            return null;
        }

        int position = getLastActivityBefore(route, actitiveDebut);

        if (position == -1) {

            // activity avant toute les autres ou non trouvee
            return null;
        }

        List<ActivitySeine> activitySeine = Lists.newArrayList(route.getActivitySeine());
        // on parcours en ordre inverse depuis la position jusqu'à trouver
        // de debut de peche positive
        ActivitySeine result = null;
        for (int i = position; i > -1; i--) {
            ActivitySeine a = activitySeine.get(i);
            if (a.isActivityDebutDePechePositive()) {

                // on a trouve une activity de debut de peche positive
                result = a;
                break;
            }
        }
        return result;
    }

    public static ActivitySeine getNextActivityFinDePeche(Route route, ActivitySeine actitiveDebut) {
        if (route.isActivitySeineEmpty()) {

            // pas d'actitive dans la route
            return null;
        }
        List<ActivitySeine> activitySeine = Lists.newArrayList(route.getActivitySeine());

        int position = activitySeine.indexOf(actitiveDebut);
        if (position == -1) {

            // activity de debut non trouvee
            return null;
        }

        for (int i = position + 1, max = route.sizeActivitySeine(); i < max; i++) {
            ActivitySeine a = activitySeine.get(i);
            if (a.isActivityFinDePeche()) {

                // activity de fin de peche trouvee
                return a;
            }
        }
        return null;
    }

    public static ActivitySeine getNextActivityDebutDePechePositive(Route route, ActivitySeine actitiveDebut) {
        if (route.isActivitySeineEmpty()) {

            // pas d'actitive dans la route
            return null;
        }
        List<ActivitySeine> activitySeine = Lists.newArrayList(route.getActivitySeine());
        int position = activitySeine.indexOf(actitiveDebut);
        if (position == -1) {

            // activity de debut non trouvee
            return null;
        }

        for (int i = position + 1, max = route.sizeActivitySeine(); i < max; i++) {
            ActivitySeine a = activitySeine.get(i);
            if (a.isActivityDebutDePechePositive()) {

                // activity de fin de peche trouvee
                return a;
            }
        }
        return null;
    }

    public static  List<ActivitySeine> getActivityDebutDePechePositive(Route route) {
        List<ActivitySeine> result = new ArrayList<>();
        if (!route.isActivitySeineEmpty()) {
            for (ActivitySeine a : route.getActivitySeine()) {
                if (a.isActivityDebutDePechePositive()) {

                    // activity de debut de peche positive
                    result.add(a);
                }
            }
        }
        return result;
    }

    public static  List<ActivitySeine> getActivityFinDePeche(Route route) {
        List<ActivitySeine> result = new ArrayList<>();
        if (!route.isActivitySeineEmpty()) {
            for (ActivitySeine a : route.getActivitySeine()) {
                if (a.isActivityFinDePeche()) {

                    // activity de debut de peche positive
                    result.add(a);
                }
            }
        }
        return result;
    }

    public static boolean isActivityFindDeVeilleFound(Route route) {
        return getActivityFinDeVeille(route) != null;
    }

    public static  ActivitySeine getActivityFinDeVeille(Route route) {
        if (route.isActivitySeineEmpty()) {
            return null;
        }
        ActivitySeine result = null;
        for (ActivitySeine a : route.getActivitySeine()) {
            if (a.isActivityFinDeVeille()) {

                // il existe bien une activity de fin de veille
                result = a;
                break;
            }
        }
        return result;
    }

    protected static int getLastActivityBefore(Route route, Date currentTime) {
        if (route.isActivitySeineEmpty()) {

            // pas d'actitive dans la route
            return -1;
        }
        Iterator<ActivitySeine> itr = route.getActivitySeine().iterator();
        int i = -1;
        while (itr.hasNext()) {
            ActivitySeine a = itr.next();
            if (currentTime.before(a.getTime()) ||
                currentTime.equals(a.getTime())) {
                break;
            }
            i++;
        }
        return i;
    }

    public static Predicate<ActivitySeine> newDateBeforePredicate(Date time) {
        return input -> input.getTime().before(time);
    }
}
