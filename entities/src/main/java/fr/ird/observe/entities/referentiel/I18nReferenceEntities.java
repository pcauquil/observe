package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Locale;

/**
 * Created on 29/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class I18nReferenceEntities {

    public static final Locale ES_LOCALE = new Locale("es", "ES");

    public static Locale getLocale(int referenceLocaleOrdinal) {
        Locale locale = Locale.FRANCE;
        if (referenceLocaleOrdinal == 0) {
            locale = Locale.UK;
        } else if (referenceLocaleOrdinal == 2) {
            locale = ES_LOCALE;
        }
        return locale;
    }

    public static String getPropertyName(int referenceLocaleOrdinal) {
        return "label" + (referenceLocaleOrdinal + 1);
    }

    public static <E extends I18nReferentialEntity> String getLabel(int referenceLocaleOrdinal, E i18nEntity) {

        String result = null;

        switch (referenceLocaleOrdinal + 1) {
            case 1:
                result = i18nEntity.getLabel1();
                break;
            case 2:
                result = i18nEntity.getLabel2();
                break;
            case 3:
                result = i18nEntity.getLabel3();
                break;
            case 4:
                result = i18nEntity.getLabel4();
                break;
            case 5:
                result = i18nEntity.getLabel5();
                break;
            case 6:
                result = i18nEntity.getLabel6();
                break;
            case 7:
                result = i18nEntity.getLabel7();
                break;
            case 8:
                result = i18nEntity.getLabel8();
                break;
        }

        return result;

    }

    public static <E extends I18nReferentialEntity> void setLabel(int referenceLocaleOrdinal, E i18nEntity, String label) {

        switch (referenceLocaleOrdinal + 1) {
            case 1:
                i18nEntity.setLabel1(label);
                break;
            case 2:
                i18nEntity.setLabel2(label);
                break;
            case 3:
                i18nEntity.setLabel3(label);
                break;
            case 4:
                i18nEntity.setLabel4(label);
                break;
            case 5:
                i18nEntity.setLabel5(label);
                break;
            case 6:
                i18nEntity.setLabel6(label);
                break;
            case 7:
                i18nEntity.setLabel7(label);
                break;
            case 8:
                i18nEntity.setLabel8(label);
                break;
        }

    }

}
