/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.seine;

import com.google.common.base.MoreObjects;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import fr.ird.observe.entities.gps.CoordinateHelper;
import fr.ird.observe.entities.referentiel.seine.VesselActivitySeine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.NumberUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/** @author Tony Chemit - chemit@codelutin.com */
public class ActivitySeineImpl extends ActivitySeineAbstract {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ActivitySeineImpl.class);

    private static final long serialVersionUID = 2L;

    public static final String ACTIVITY_FIN_DE_VEILLE = "16";

    public static final String ACTIVITY_DEBUT_DE_PECHE = "7";

    public static final String ACTIVITY_FIN_DE_PECHE = "6";

    public static final String ACTIVITY_CHANGED_ZONE = "21";

    private static final List<String> CALEE_OPERATIONS = Collections.singletonList(ACTIVITY_FIN_DE_PECHE);

    private static final List<String> DCP_OPERATIONS = Arrays.asList("13", "14", "15");

    private static final List<String> SYSTEM_OPERATIONS = Arrays.asList("0", "11", "11", ACTIVITY_FIN_DE_VEILLE);

    public static final String PROPERTY_CHANGED_ZONE_OPERATION = "changedZoneOperation";

    public static final String PROPERTY_SET_OPERATION = "setOperation";

//    @Override
//    public OpenableEntity getOpenChild() {
//        // sur une activity, pas d'enfant OpenableEntity
//        return null;
//    }
//
//    @Override
//    public List<?> getOpenableChilds() {
//        // sur une activity, pas d'enfant OpenableEntity
//        return null;
//    }

    /** @return {@code true} si l'activite concerne un DCP */
    @Override
    public boolean isDCPOperation() {
        return vesselActivitySeine != null && DCP_OPERATIONS.contains(vesselActivitySeine.getCode());
    }

    /** @return {@code true} si l'activite concerne une calée */
    @Override
    public boolean isSetOperation() {
        return vesselActivitySeine != null && CALEE_OPERATIONS.contains(vesselActivitySeine.getCode());
    }

    /** @return {@code true} si l'activite concerne un système observé */
    @Override
    public boolean isObservedSystemOperation() {
        return vesselActivitySeine != null && SYSTEM_OPERATIONS.contains(vesselActivitySeine.getCode());
    }

    /** @return {@code true} si l'activite concerne un changement de zone */
    @Override
    public boolean isChangedZoneOperation() {
        return vesselActivitySeine != null && ACTIVITY_CHANGED_ZONE.equals(vesselActivitySeine.getCode());
    }

    @Override
    public Integer getQuadrant() {
        return CoordinateHelper.getQuadrant(longitude, latitude);
    }

//    @Override
//    public void setQuadrant(Integer quadrant) {
//        if (!Objects.equals(this.quadrant, quadrant)) {
//            Entities.printDebugInformations(PROPERTY_QUADRANT, this, quadrant);
//        }
//        Integer old = this.quadrant;
////        fireOnPreWrite(PROPERTY_QUADRANT, old, quadrant);
//        this.quadrant = quadrant;
//        fireOnPostWrite(PROPERTY_QUADRANT, old, quadrant);
//    }

    @Override
    public SchoolTypePersist getSchoolType() {
        return ActivitySeines.getSchoolType(this);
    }

    @Override
    public boolean isActivityFinDePeche() {
        return vesselActivitySeine != null && ACTIVITY_DEBUT_DE_PECHE.equals(vesselActivitySeine.getCode());
    }

    @Override
    public boolean isActivityDebutDePechePositive() {
        return isSetOperation() && (getReasonForNoFishing() == null || getSetSeine() != null);
    }

    @Override
    public boolean isActivityFinDeVeille() {
        return vesselActivitySeine != null && ACTIVITY_FIN_DE_VEILLE.equals(vesselActivitySeine.getCode());
    }

    @Override
    public void setObservedSystemDistance(Float observedSystemDistance) {
        if (observedSystemDistance != null) {

            // on arrondit à 2 décimales
            observedSystemDistance =
                    NumberUtil.roundTwoDigits(observedSystemDistance);
        }
        super.setObservedSystemDistance(observedSystemDistance);
    }

    @Override
    public void setVesselSpeed(Float vesselSpeed) {
        if (vesselSpeed != null) {

            // on arrondit à 2 décimales
            vesselSpeed = NumberUtil.roundTwoDigits(vesselSpeed);
        }
        super.setVesselSpeed(vesselSpeed);
    }

    @Override
    public void setSeaSurfaceTemperature(Float seaSurfaceTemperature) {
        if (seaSurfaceTemperature != null) {

            // on arrondit à 2 décimales
            seaSurfaceTemperature = NumberUtil.roundTwoDigits(seaSurfaceTemperature);
        }
        super.setSeaSurfaceTemperature(seaSurfaceTemperature);
    }

    @Override
    public void setVesselActivitySeine(VesselActivitySeine vesselActivitySeine) {

        boolean oldChangedZone = isChangedZoneOperation();
        super.setVesselActivitySeine(vesselActivitySeine);
        fireOnPostWrite(PROPERTY_CHANGED_ZONE_OPERATION, oldChangedZone, isChangedZoneOperation());

    }

    @Override
    public synchronized void setLatitude(Float latitude) {
        if (!Objects.equals(this.latitude, latitude)) {
            Entities.printDebugInformations(PROPERTY_LATITUDE, this, latitude);
        }
        super.setLatitude(latitude);
    }

    @Override
    public synchronized void setLongitude(Float longitude) {
        if (!Objects.equals(this.longitude, longitude)) {
            Entities.printDebugInformations(PROPERTY_LONGITUDE, this, longitude);
        }
        super.setLongitude(longitude);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("time", getTime())
                          .add("quadrant", getQuadrant())
                          .add("latitude", longitude)
                          .add("longitude", longitude)
                          .toString();
    }

}


