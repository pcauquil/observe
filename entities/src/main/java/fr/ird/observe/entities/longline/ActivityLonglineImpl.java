package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.gps.CoordinateHelper;
import fr.ird.observe.entities.referentiel.longline.VesselActivityLongline;
import org.nuiton.util.DateUtil;

import java.util.Date;
import java.util.Objects;

public class ActivityLonglineImpl extends ActivityLonglineAbstract {

    private static final long serialVersionUID = 1L;

    private static final String VESSEL_ACTIVITY_ID_FOR_SET = "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.1";

    @Override
    public Date getDate() {
        return timeStamp == null ? null : DateUtil.getDay(timeStamp);
    }

    @Override
    public Date getTime() {
        return timeStamp == null ? null : DateUtil.getTime(timeStamp, false, false);
    }

    @Override
    public void setDate(Date date) {
        if (timeStamp != null) {
            Date dateAndTime = date == null ? timeStamp : DateUtil.getDateAndTime(date, timeStamp, true, false);
            setTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setTime(Date time) {
        if (timeStamp != null) {
            Date dateAndTime = time == null ? timeStamp : DateUtil.getDateAndTime(timeStamp, time, false, false);
            setTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setTimeStamp(Date timeStamp) {
        Date oldDate = getDate();
        Date oldTime = getTime();
        super.setTimeStamp(timeStamp);
        fireOnPostWrite(PROPERTY_DATE, oldDate, getDate());
        fireOnPostWrite(PROPERTY_TIME, oldTime, getTime());
    }

    @Override
    public Integer getQuadrant() {
        return CoordinateHelper.getQuadrant(longitude, latitude);
    }

//    @Override
//    public synchronized void setQuadrant(Integer quadrant) {
//        if (!Objects.equals(this.quadrant, quadrant)){
//            Entities.printDebugInformations(PROPERTY_QUADRANT, this, quadrant);
//        }
//        Integer old = this.quadrant;
//        this.quadrant = quadrant;
//        fireOnPostWrite(PROPERTY_QUADRANT, old, quadrant);
//    }

    /** @return {@code true} si l'activite concerne une calée */
    @Override
    public boolean isSetOperation() {
        return vesselActivityLongline != null && VESSEL_ACTIVITY_ID_FOR_SET.equals(vesselActivityLongline.getTopiaId());
    }

    @Override
    public void setVesselActivityLongline(VesselActivityLongline vesselActivityLongline) {

        super.setVesselActivityLongline(vesselActivityLongline);
        fireOnPostWrite("setOperation", null, isSetOperation());

    }

    @Override
    public synchronized void setLatitude(Float latitude) {
        if (!Objects.equals(this.latitude, latitude)) {
            Entities.printDebugInformations(PROPERTY_LATITUDE, this, latitude);
        }
        super.setLatitude(latitude);
    }


    @Override
    public synchronized void setLongitude(Float longitude) {
        if (!Objects.equals(this.longitude, longitude)) {
            Entities.printDebugInformations(PROPERTY_LONGITUDE, this, longitude);
        }
        super.setLongitude(longitude);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("date", getDate())
                          .add("time", getTime())
                          .add("quadrant", getQuadrant())
                          .add("latitude", longitude)
                          .add("longitude", longitude)
                          .toString();
    }
}
