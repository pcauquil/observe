package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class SectionTemplate extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_FLOATLINE_LENGTHS = "floatlineLengths";

    protected static final Pattern FLOATLINE_LENGTHS_PATTERN = Pattern.compile("(\\d+)(\\.\\d*){0,1}(/(\\d+)(\\.\\d*){0,1})+");

    /**
     * Identifier of the template.
     */
    protected String id;

    /**
     * Floatlines lengths for this template.
     */
    protected String floatlineLengths;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        String oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);
    }

    public String getFloatlineLengths() {
        return floatlineLengths;
    }


    public void setFloatlineLengths(String floatlineLengths) {

        String oldValue = getFloatlineLengths();
        this.floatlineLengths = floatlineLengths;
        firePropertyChange(PROPERTY_FLOATLINE_LENGTHS, oldValue, this.floatlineLengths);

    }

    public boolean isFloatlineLengthsValid() {

        Matcher matcher = FLOATLINE_LENGTHS_PATTERN.matcher(floatlineLengths);
        return matcher.matches();

    }

    public List<Float> getFloatlineLengthsAsList() {

        String[] parts = floatlineLengths.split("\\/");
        List<Float> newLengths = new ArrayList<>(parts.length);
        for (String part : parts) {
            Float aFloat = Float.valueOf(part);
            newLengths.add(aFloat);
        }
        return newLengths;

    }

    public boolean isCompiliantWithBasketCount(int basketsCount) {

        Preconditions.checkArgument(isFloatlineLengthsValid());

        List<Float> floatlineLengthsAsList = getFloatlineLengthsAsList();
        return basketsCount + 1 == floatlineLengthsAsList.size();

    }

    public void applyToBaskets(List<Basket> baskets) {

        Preconditions.checkArgument(isFloatlineLengthsValid());
        Objects.requireNonNull(baskets);
        Preconditions.checkArgument(!baskets.isEmpty());
        Preconditions.checkArgument(isCompiliantWithBasketCount(baskets.size()));

        List<Float> floatlineLengthsAsList = getFloatlineLengthsAsList();

        Iterator<Float> lengthsIterator = floatlineLengthsAsList.iterator();

        float floatline1;
        float floatline2;

        Iterator<Basket> basketIterator = baskets.iterator();

        {

            // on first basket, usgin the two first lengths
            floatline1 = lengthsIterator.next();
            floatline2 = lengthsIterator.next();

            Basket basket = basketIterator.next();
            basket.setFloatline1Length(floatline1);
            basket.setFloatline2Length(floatline2);

        }

        while (basketIterator.hasNext()) {

            // floatline1 is previous floatline2
            floatline1 = floatline2;
            floatline2 = lengthsIterator.next();

            Basket basket = basketIterator.next();
            basket.setFloatline1Length(floatline1);
            basket.setFloatline2Length(floatline2);

        }

    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("id", id)
                .append("floatlineLengths", floatlineLengths)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SectionTemplate)) return false;

        SectionTemplate that = (SectionTemplate) o;

        return Objects.equals(floatlineLengths, that.floatlineLengths)
                && Objects.equals(id, that.id);

    }

    @Override
    public int hashCode() {
        int result = id == null ? 0 : id.hashCode();
        result = 31 * result + (floatlineLengths == null ? 0 : floatlineLengths.hashCode());
        return result;
    }
}
