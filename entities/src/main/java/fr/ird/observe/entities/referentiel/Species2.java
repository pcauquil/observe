package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ird.observe.entities.referentiel.seine.WeightCategory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Helper class around {@link Species}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class Species2 {

    protected Species2() {
        // avoid instance
    }

    public static Predicate<Species> newSpeciesByIdPredicate(Set<String> ids) {
        return id -> ids.contains(id.getTopiaId());
    }

    public static Predicate<Species> newSpeciesListPredicate(SpeciesList speciesList) {
        return speciesList::containsSpecies;
    }

    public static Predicate<Species> newSpeciesByOceanPredicate(Ocean ocean) {
        return o -> o.containsOcean(ocean);
    }

    public static <E extends Species> List<E> filterByOcean(Collection<E> speciess, Ocean ocean) {

        return speciess.stream().filter(newSpeciesByOceanPredicate(ocean)).collect(Collectors.toList());
    }

    public static List<WeightCategory> filterWeightCategoryBySpeciesIds(Collection<WeightCategory> speciess, Set<String> speciesIds) {

        return speciess.stream()
                       .filter(input -> speciesIds.contains(input.getSpecies().getTopiaId()))
                       .collect(Collectors.toList());
    }

    public static List<WeightCategory> filterWeightCategoryByOcean(Collection<WeightCategory> speciess, Ocean ocean) {

        return speciess.stream()
                       .filter(input -> input.getSpecies().getOcean().contains(ocean))
                       .collect(Collectors.toList());
    }

    public static String decorate(Locale locale, Species species) {

        StringBuilder builder = new StringBuilder();
        builder.append(species.getFaoCode());
        if (species.getScientificLabel() == null) {
            builder.append(" - ").append("xx");

        } else {
            builder.append(" - ").append(species.getScientificLabel());
        }

        return builder.toString();

    }

    public static List<Species> toSpecies(Collection<WeightCategory> categories) {

        Set<Species> result = Sets.newHashSet();

        for (WeightCategory category : categories) {
            result.add(category.getSpecies());
        }
        return new ArrayList<>(result);
    }

}
