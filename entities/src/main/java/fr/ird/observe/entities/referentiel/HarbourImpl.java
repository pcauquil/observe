package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.gps.CoordinateHelper;

public class HarbourImpl extends HarbourAbstract {

    private static final long serialVersionUID = 1L;

    /**
     * La valeur du quadrant (utilisé uniquement dans l'interface graphique) :
     * en base on conserve cette information dans les coordonnées {@link #longitude}
     * {@link #latitude} qui sont signés.
     *
     * @since 3.11
     */
    protected Integer quadrant;

    @Override
    public Integer getQuadrant() {
        return CoordinateHelper.getQuadrant(longitude, latitude);
    }

    @Override
    public void setQuadrant(Integer quadrant) {
        fireOnPreWrite(PROPERTY_QUADRANT, quadrant, quadrant);
        this.quadrant = quadrant;
        fireOnPostWrite(PROPERTY_QUADRANT, quadrant, quadrant);
    }

}
