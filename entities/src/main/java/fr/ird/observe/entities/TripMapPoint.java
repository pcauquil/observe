package fr.ird.observe.entities;

/*-
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.constants.TripMapPointTypePersist;

import java.io.Serializable;
import java.util.Date;

public class TripMapPoint implements Serializable {

    public static final String PROPERTY_TIME = "time";

    public static final String PROPERTY_LATITUDE = "latitude";

    public static final String PROPERTY_LONGITUDE = "longitude";

    public static final String PROPERTY_TYPE = "type";

    private static final long serialVersionUID = 1L;

    protected Date time;

    protected float latitude;

    protected float longitude;

    protected TripMapPointTypePersist type;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public TripMapPointTypePersist getType() {
        return type;
    }

    public void setType(TripMapPointTypePersist type) {
        this.type = type;
    }
}
