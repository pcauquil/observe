package fr.ird.observe.entities.seine;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created on 8/25/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class Routes {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Routes.class);

    /**
     * Comparateur de {@link Route} basé sur la propriété {@link
     * Route#getDate()}.
     */
    public static final Comparator<Route> ROUTE_COMPARATOR = (o1, o2) -> o1.getDate().compareTo(o2.getDate());

    public static void sort(List<Route> routes) {
        Collections.sort(routes, ROUTE_COMPARATOR);
    }

    public static Route getPreviousRoute(List<Route> routes, Route route) {

        if (route == null) {
            // pas de route courante
            return null;
        }

        if (routes == null || routes.size() < 2) {
            // pas plus de 2 routes, donc pas de route precedente
            return null;
        }
        sort(routes);
        Route previous = null;
        for (Route r : routes) {
            if (r.getDate().getTime() <
                route.getDate().getTime()) {
                previous = r;
                continue;
            }
            // la route courante est
            break;
        }
        if (previous != null) {
            if (log.isDebugEnabled())
                log.debug("previous route " +
                          previous.getDate());
        } else {
            if (log.isDebugEnabled()) {
                log.debug("no previous route for " + route);
            }

        }
        return previous;
    }

    public static Predicate<Route> newDateBeforePredicate(final Date date) {
        return input -> input.getDate().before(date);
    }
}
