package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TdrTopiaDao extends AbstractTdrTopiaDao<Tdr> {


    public Multimap<String, String> getTdrIdsBySeineIds(TripLongline tripLongline) {

        Set<String> setLonglineIds = TripLonglines.getSetIdsWithTdr(tripLongline);

        Multimap<String, String> result = ArrayListMultimap.create();

        GetTdrIdsQuery sqlQuery = new GetTdrIdsQuery();

        for (String setLonglineId : setLonglineIds) {

            sqlQuery.setSetId(setLonglineId);

            List<String> tdrIds = topiaSqlSupport.findMultipleResult(sqlQuery);

            result.putAll(setLonglineId, tdrIds);

        }
        return result;

    }

    public void applyTdrAssociationFix(Multimap<String, String> tdrAssociation) {

        String request = "\nUPDATE OBSERVE_LONGLINE.TDR SET SET = '%s' WHERE topiaid = '%s';";

        StringBuilder builder = new StringBuilder();

        if (tdrAssociation != null) {
            for (Map.Entry<String, String> entry : tdrAssociation.entries()) {
                String setLonglineId = entry.getKey();
                String tdrId = entry.getValue();
                builder.append(String.format(request, setLonglineId, tdrId));
            }
        }

        topiaSqlSupport.executeSql(builder.toString());

    }

    private static class GetTdrIdsQuery extends TopiaSqlQuery<String> {

        protected String setId;

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            String sql = "SELECT t.topiaId " +
                         "FROM OBSERVE_LONGLINE.TDR t " +
                         "WHERE t.SET = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, setId);
            return ps;
        }

        @Override
        public String prepareResult(ResultSet set) throws SQLException {
            return set.getString(1);
        }

        public void setSetId(String setId) {
            this.setId = setId;
        }
    }

}
