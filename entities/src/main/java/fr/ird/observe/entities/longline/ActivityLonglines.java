package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created on 8/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class ActivityLonglines {

    /**
     * Comparateur de {@link ActivityLongline} basé sur la propriété {@link
     * ActivityLongline#getTimeStamp()}.
     */
    public static final Comparator<ActivityLongline> ACTIVITY_LONGLINE_COMPARATOR = (o1, o2) -> o1.getTimeStamp().compareTo(o2.getTimeStamp());

    public static void sort(List<ActivityLongline> routes) {
        Collections.sort(routes, ACTIVITY_LONGLINE_COMPARATOR);
    }

    public static Predicate<ActivityLongline> newTimeStampBeforePredicate(Date timeStamp) {
        return input -> input.getTimeStamp().before(timeStamp);
    }
}
