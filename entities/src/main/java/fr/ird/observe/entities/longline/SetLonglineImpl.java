package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.gps.CoordinateHelper;
import org.nuiton.util.DateUtil;

import java.util.Date;
import java.util.Objects;

public class SetLonglineImpl extends SetLonglineAbstract {

    private static final long serialVersionUID = 1L;

    public Integer getSettingStartQuadrant() {
        return CoordinateHelper.getQuadrant(settingStartLongitude, settingStartLatitude);
    }

    public void setSettingStartQuadrant(Integer settingStartQuadrant) {
        if (!Objects.equals(getSettingStartQuadrant(), settingStartQuadrant)) {
            Entities.printDebugInformations(PROPERTY_SETTING_START_QUADRANT, this, settingStartQuadrant);
        }
        fireOnPostWrite(PROPERTY_SETTING_START_QUADRANT, null, settingStartQuadrant);
    }

    public Integer getSettingEndQuadrant() {
        return CoordinateHelper.getQuadrant(settingEndLongitude, settingEndLatitude);
    }

    public void setSettingEndQuadrant(Integer settingEndQuadrant) {
        if (!Objects.equals(getSettingEndQuadrant(), settingEndQuadrant)) {
            Entities.printDebugInformations(PROPERTY_SETTING_END_QUADRANT, this, settingEndQuadrant);
        }
        fireOnPostWrite(PROPERTY_SETTING_END_QUADRANT, null, settingEndQuadrant);
    }

    public Integer getHaulingStartQuadrant() {
        return CoordinateHelper.getQuadrant(haulingStartLongitude, haulingStartLatitude);
    }

    public void setHaulingStartQuadrant(Integer haulingStartQuadrant) {
        if (!Objects.equals(getHaulingStartQuadrant(), haulingStartQuadrant)) {
            Entities.printDebugInformations(PROPERTY_HAULING_START_QUADRANT, this, haulingStartQuadrant);
        }
        fireOnPostWrite(PROPERTY_HAULING_START_QUADRANT, null, haulingStartQuadrant);
    }

    public Integer getHaulingEndQuadrant() {
        return CoordinateHelper.getQuadrant(haulingEndLongitude, haulingEndLatitude);
    }

    public void setHaulingEndQuadrant(Integer haulingEndQuadrant) {
        if (!Objects.equals(getHaulingEndQuadrant(), haulingEndQuadrant)) {
            Entities.printDebugInformations(PROPERTY_HAULING_END_QUADRANT, this, haulingEndQuadrant);
        }
        fireOnPostWrite(PROPERTY_HAULING_END_QUADRANT, null, haulingEndQuadrant);
    }

    @Override
    public void setSettingStartLongitude(Float settingStartLongitude) {
        if (!Objects.equals(this.settingStartLongitude, settingStartLongitude)) {
            Entities.printDebugInformations(PROPERTY_SETTING_START_LONGITUDE, this, settingStartLongitude);
        }
        super.setSettingStartLongitude(settingStartLongitude);
    }

    @Override
    public void setSettingStartLatitude(Float settingStartLatitude) {
        if (!Objects.equals(this.settingStartLatitude, settingStartLatitude)) {
            Entities.printDebugInformations(PROPERTY_SETTING_START_LATITUDE, this, settingStartLatitude);
        }
        super.setSettingStartLatitude(settingStartLatitude);
    }

    @Override
    public void setSettingEndLatitude(Float settingEndLatitude) {
        if (!Objects.equals(this.settingEndLatitude, settingEndLatitude)) {
            Entities.printDebugInformations(PROPERTY_SETTING_END_LATITUDE, this, settingEndLatitude);
        }
        super.setSettingEndLatitude(settingEndLatitude);
    }

    @Override
    public void setSettingEndLongitude(Float settingEndLongitude) {
        if (!Objects.equals(this.settingEndLongitude, settingEndLongitude)) {
            Entities.printDebugInformations(PROPERTY_SETTING_END_LONGITUDE, this, settingEndLongitude);
        }
        super.setSettingEndLongitude(settingEndLongitude);
    }

    @Override
    public void setHaulingStartLongitude(Float haulingStartLongitude) {
        if (!Objects.equals(this.haulingStartLongitude, haulingStartLongitude)) {
            Entities.printDebugInformations(PROPERTY_HAULING_START_LONGITUDE, this, haulingStartLongitude);
        }
        super.setHaulingStartLongitude(haulingStartLongitude);
    }

    @Override
    public void setHaulingStartLatitude(Float haulingStartLatitude) {
        if (!Objects.equals(this.haulingStartLatitude, haulingStartLatitude)) {
            Entities.printDebugInformations(PROPERTY_HAULING_START_LATITUDE, this, haulingStartLatitude);
        }
        super.setHaulingStartLatitude(haulingStartLatitude);
    }

    @Override
    public Float getHaulingEndLongitude() {
        return super.getHaulingEndLongitude();
    }

    @Override
    public Float getHaulingEndLatitude() {
        return super.getHaulingEndLatitude();
    }

    @Override
    public Date getSettingStartDate() {
        return settingStartTimeStamp == null ? null : DateUtil.getDay(settingStartTimeStamp);
    }

    @Override
    public Date getSettingStartTime() {
        return settingStartTimeStamp == null ? null : DateUtil.getTime(settingStartTimeStamp, false, false);
    }

    @Override
    public void setSettingStartDate(Date date) {
        if (settingStartTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, settingStartTimeStamp, true, false);
            setSettingStartTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setSettingStartTime(Date time) {
        if (settingStartTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(settingStartTimeStamp, time, false, false);
            setSettingStartTimeStamp(dateAndTime);
        }
    }

    @Override
    public Date getSettingEndDate() {
        return settingEndTimeStamp == null ? null : DateUtil.getDay(settingEndTimeStamp);
    }

    @Override
    public Date getSettingEndTime() {
        return settingEndTimeStamp == null ? null : DateUtil.getTime(settingEndTimeStamp, false, false);
    }

    @Override
    public void setSettingEndDate(Date date) {
        if (settingEndTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, settingEndTimeStamp, true, false);
            setSettingEndTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setSettingEndTime(Date time) {
        if (settingEndTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(settingEndTimeStamp, time, true, false);
            setSettingEndTimeStamp(dateAndTime);
        }
    }

    @Override
    public Date getHaulingStartDate() {
        return haulingStartTimeStamp == null ? null : DateUtil.getDay(haulingStartTimeStamp);
    }

    @Override
    public Date getHaulingStartTime() {
        return haulingStartTimeStamp == null ? null : DateUtil.getTime(haulingStartTimeStamp, false, false);
    }

    @Override
    public void setHaulingStartDate(Date date) {
        if (haulingStartTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, haulingStartTimeStamp, true, false);
            setHaulingStartTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setHaulingStartTime(Date time) {
        if (haulingStartTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(haulingStartTimeStamp, time, false, false);
            setHaulingStartTimeStamp(dateAndTime);
        }
    }

    @Override
    public Date getHaulingEndDate() {
        return haulingEndTimeStamp == null ? null : DateUtil.getDay(haulingEndTimeStamp);
    }

    @Override
    public Date getHaulingEndTime() {
        return haulingEndTimeStamp == null ? null : DateUtil.getTime(haulingEndTimeStamp, false, false);
    }

    @Override
    public void setHaulingEndDate(Date date) {
        if (haulingEndTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(date, haulingEndTimeStamp, true, false);
            setHaulingEndTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setHaulingEndTime(Date time) {
        if (haulingEndTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(haulingEndTimeStamp, time, true, false);
            setHaulingEndTimeStamp(dateAndTime);
        }
    }

    @Override
    public int getFloatlinesCompositionProportionSum() {
        int sum = 0;
        if (!isFloatlinesCompositionEmpty()) {
            for (FloatlinesComposition composition : floatlinesComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    @Override
    public int getBranchlinesCompositionProportionSum() {
        int sum = 0;
        if (!isBranchlinesCompositionEmpty()) {
            for (BranchlinesComposition composition : branchlinesComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    @Override
    public int getHooksCompositionProportionSum() {
        int sum = 0;
        if (!isHooksCompositionEmpty()) {
            for (HooksComposition composition : hooksComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    @Override
    public int getBaitsCompositionProportionSum() {
        int sum = 0;
        if (!isBaitsCompositionEmpty()) {
            for (BaitsComposition composition : baitsComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    @Override
    public void setFloatlinesCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireOnPostWrite(PROPERTY_FLOATLINES_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

    @Override
    public void setBranchlinesCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireOnPostWrite(PROPERTY_BRANCHLINES_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

    @Override
    public void setHooksCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireOnPostWrite(PROPERTY_HOOKS_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

    @Override
    public void setBaitsCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        fireOnPostWrite(PROPERTY_BAITS_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("haulingStartLatitude", getHaulingStartLatitude())
                .add("haulingStartLongitude", getHaulingStartLongitude())
                .add("haulingStartQuadrant", getHaulingStartQuadrant())
                .add("haulingEndLatitude", getHaulingEndLatitude())
                .add("haulingEndLongitude", getHaulingEndLongitude())
                .add("haulingEndQuadrant", getHaulingEndQuadrant())
                .add("settingStartLatitude", getSettingStartLatitude())
                .add("settingStartLongitude", getSettingStartLongitude())
                .add("settingStartQuadrant", getSettingStartQuadrant())
                .add("settingEndLatitude", getSettingEndLatitude())
                .add("settingEndLongitude", getSettingEndLongitude())
                .add("settingEndQuadrant", getSettingEndQuadrant())
                .toString();
    }
}
