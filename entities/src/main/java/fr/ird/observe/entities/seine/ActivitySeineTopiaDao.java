package fr.ird.observe.entities.seine;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.seine.VesselActivitySeine;
import fr.ird.observe.entities.referentiel.seine.VesselActivitySeineImpl;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ActivitySeineTopiaDao extends AbstractActivitySeineTopiaDao<ActivitySeine> {

    public List<ActivitySeine> findAllStubByRouteId(String routeId, int referenceLocale) {

        return StubSqlQuery.findAll(topiaSqlSupport, routeId, referenceLocale);

    }

    public ActivitySeine findStubByTopiaId(String activityId, int referenceLocale) {

        return StubSqlQuery.find(topiaSqlSupport, activityId, referenceLocale);

    }

    private static class StubSqlQuery extends TopiaSqlQuery<ActivitySeine> {

        private final String sql;

        private final String id;

        private final int referenceLocale;

        static List<ActivitySeine> findAll(TopiaSqlSupport context, String routeId, int referenceLocale) {

            String sql = "SELECT" +
                         " a.topiaId," +
                         " a.time," +
                         " a.set," +
                         " va." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_seine.activity a, observe_seine.vesselactivity va" +
                         " WHERE " +
                         " a.route = ?" +
                         " AND a.vesselactivity = va.topiaid" +
                         " ORDER BY a.time";

            StubSqlQuery request = new StubSqlQuery(sql, routeId, referenceLocale);
            return context.findMultipleResult(request);

        }

        static ActivitySeine find(TopiaSqlSupport context, String activityId, int referenceLocale) {

            String sql = "SELECT" +
                         " a.topiaId," +
                         " a.time," +
                         " a.set," +
                         " va." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_seine.activity a, observe_seine.vesselactivity va" +
                         " WHERE " +
                         " a.topiaId = ?" +
                         " AND a.vesselactivity = va.topiaid" +
                         " ORDER BY a.time";

            StubSqlQuery request = new StubSqlQuery(sql, activityId, referenceLocale);
            return context.findSingleResult(request);

        }

        StubSqlQuery(String sql, String id, int referenceLocale) {
            this.sql = sql;
            this.id = id;
            this.referenceLocale = referenceLocale;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            return preparedStatement;
        }

        @Override
        public ActivitySeine prepareResult(ResultSet set) throws SQLException {

            ActivitySeine activity = new ActivitySeineImpl();
            activity.setTopiaId(set.getString(1));
            activity.setTime(set.getTime(2));

            String setId = set.getString(3);
            if (setId != null) {

                SetSeine setSeine = new SetSeineImpl();
                setSeine.setTopiaId(setId);
                activity.setSetSeine(setSeine);

            }

            VesselActivitySeine vesselActivity = new VesselActivitySeineImpl();
            String label = set.getString(4);
            I18nReferenceEntities.setLabel(referenceLocale, vesselActivity, label);
            activity.setVesselActivitySeine(vesselActivity);

            return activity;

        }

    }

}
