package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.constants.ReferenceStatusPersist;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

/**
 * Helper class around {@link ObserveReferentialEntity}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ObserveReferentialEntities {

    public static final Predicate<? extends ObserveReferentialEntity> IS_ACTIF_PREDICATE = input -> ReferenceStatusPersist.disabled != input.getStatus();

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveReferentialEntities.class);

    /**
     * Filter une liste d'entités d'un référentiel en supprimmant toutes les
     * entités qui ne sont pas actives {@link ObserveReferentialEntity#getStatus()}
     *
     * @param list la liste des entitées à filtrer
     * @param <E>  le type des entites du référentiel
     */
    public static <E extends ObserveReferentialEntity> void filterReferentielListByStatus(List<E> list) {
        filterReferentielList(list, (Predicate)ObserveReferentialEntities.IS_ACTIF_PREDICATE);
    }

    /**
     * Filter une liste d'entités d'un référentiel en supprimant toutes les
     * entités qui ne rspeciesnt pas le précidat donné.
     *
     * @param list      la liste des entitées à filtrer
     * @param predicate le prédicate à appliquer pour conserver les valeurs
     * @param <E>       le type des entites du référentiel
     */
    public static <E extends ObserveReferentialEntity> void filterReferentielList(List<E> list, Predicate<E> predicate) {
        Iterator<E> itr = list.iterator();
        while (itr.hasNext()) {
            E e = itr.next();
            if (!predicate.test(e)) {

                // l'entite n'est pas retenue, on ne l'affiche pas

                if (log.isDebugEnabled()) {
                    log.debug("remove disabled entity " + e.getTopiaId());
                }

                itr.remove();
            }
        }
    }

    public static void walk(ReferentielWalker walker) throws Exception {
        for (ObserveEntityEnum constant : Entities.REFERENCE_ENTITIES) {
            Class<? extends TopiaEntity> contractClass = constant.getContract();
            walker.walk(contractClass);
        }
    }

    public static void walkSafe(ReferentielWalker walker) {
        for (ObserveEntityEnum constant : Entities.REFERENCE_ENTITIES) {
            try {
                Class<? extends TopiaEntity> contractClass =
                        constant.getContract();
                walker.walk(contractClass);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    protected ObserveReferentialEntities() {
        // avoid instance
    }

    public interface ReferentielWalker {

        <E extends TopiaEntity> void walk(Class<E> beanClass);
    }

}
