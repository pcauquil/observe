package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Created on 1/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public interface LonglinePositionAware extends TopiaEntity {

    void setSection(Section section);

    Section getSection();

    void setBasket(Basket basket);

    Basket getBasket();

    void setBranchline(Branchline branchline);

    Branchline getBranchline();

}
