package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Predicate;

/**
 * Created on 6/24/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class Persons {

    public static final Predicate<Person> OBSERVER_PREDICATE = Person::isObserver;
    public static final Predicate<Person> CAPTAIN_PREDICATE = Person::isCaptain;
    public static final Predicate<Person> DATA_ENTRY_OPERATOR_PREDICATE = Person::isDataEntryOperator;

}
