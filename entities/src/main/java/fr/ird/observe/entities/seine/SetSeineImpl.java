/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.seine;

import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import org.nuiton.util.DateUtil;
import org.nuiton.util.NumberUtil;

import java.util.Date;

/** @author Tony Chemit - chemit@codelutin.com */
public class SetSeineImpl extends SetSeineAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean hasTargetCatch() {
        if (isTargetCatchEmpty()) {
            return false;
        }
        for (TargetCatch c : getTargetCatch()) {
            if (!c.isDiscarded()) {

                // on a trouve au moins une discarded
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasRejetThon() {
        if (isTargetCatchEmpty()) {
            return false;
        }
        for (TargetCatch c : getTargetCatch()) {
            if (c.isDiscarded()) {

                // on a trouvé au moins un rejet
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean canUseTargetSample(boolean discarded) {
        if (isTargetCatchEmpty()) {
            return false;
        }
        for (TargetCatch c : getTargetCatch()) {
            if (discarded) {

                if (c.isDiscarded() &&
                    c.getBroughtOnDeck() != null &&
                    c.getBroughtOnDeck()) {

                    // on a trouvé au moins un rejet monté sur le pont
                    return true;
                }
            } else {
                if (!c.isDiscarded()) {

                    // on a trouvé au moins un non rejet
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public TargetSample getTargetSample(boolean discarded) {
        if (isTargetSampleEmpty()) {
            return null;
        }
        for (TargetSample c : getTargetSample()) {
            if (discarded) {
                if (c.getDiscarded() != null && c.getDiscarded()) {

                    // on a trouvé au moins une rejet
                    return c;
                }
            } else {

                if (c.getDiscarded() == null || !c.getDiscarded()) {

                    // on a trouvé au moins un non rejet
                    return c;
                }
            }
        }
        return null;
    }

    @Override
    public boolean canUseNonTargetSample() {
        return !isNonTargetCatchEmpty();
    }

    @Override
    public void setCurrentSpeed(Float currentSpeed) {
        if (currentSpeed != null) {

            // on arrondit à 3 décimales
            currentSpeed = NumberUtil.roundThreeDigits(currentSpeed);
        }
        super.setCurrentSpeed(currentSpeed);
    }

    @Override
    public boolean isFreeSchoolType() {
        return schoolType != null && SchoolTypePersist.libre == schoolType;
    }

    @Override
    public boolean isSchoolObjectType() {
        return schoolType != null && SchoolTypePersist.objet == schoolType;
    }

    @Override
    public boolean isUnknownSchoolType() {
        return schoolType != null && SchoolTypePersist.undefined == schoolType;
    }

    @Override
    public void setEndSetDate(Date endSetDate) {
        if (endSetTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endSetDate, endSetTimeStamp, false, false);
            setEndSetTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setEndSetTime(Date endSetTime) {
        if (endSetTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endSetTimeStamp, endSetTime, false, false);
            setEndSetTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setEndPursingDate(Date endPursingDate) {
        if (endPursingTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endPursingDate, endPursingTimeStamp, false, false);
            setEndPursingTimeStamp(dateAndTime);
        }
    }

    @Override
    public void setEndPursingTime(Date endPursingTime) {
        if (endPursingTimeStamp != null) {
            Date dateAndTime = DateUtil.getDateAndTime(endPursingTimeStamp, endPursingTime, false, false);
            setEndPursingTimeStamp(dateAndTime);
        }
    }

    @Override
    public Date getEndSetDate() {
        return endSetTimeStamp == null ? null : DateUtil.getDay(endSetTimeStamp);
    }

    @Override
    public Date getEndSetTime() {
        return endSetTimeStamp == null ? null : DateUtil.getTime(endSetTimeStamp, false, false);
    }

    @Override
    public Date getEndPursingDate() {
        return endPursingTimeStamp == null ? null : DateUtil.getDay(endPursingTimeStamp);
    }

    @Override
    public Date getEndPursingTime() {
        return endPursingTimeStamp == null ? null : DateUtil.getTime(endPursingTimeStamp, false, false);
    }
}
