package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * Created on 12/12/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class SectionWithTemplateImpl extends SectionImpl implements SectionWithTemplate {

    private static final long serialVersionUID = 1L;

    SectionTemplate sectionTemplate;

    private final Section delegate;

    public SectionWithTemplateImpl() {
        this(new SectionImpl());
    }

    public SectionWithTemplateImpl(Section delegate) {
        this.delegate = delegate;
    }

    public Section getDelegate() {
        return delegate;
    }

    @Override
    public SectionTemplate getSectionTemplate() {
        return sectionTemplate;
    }

    @Override
    public void setSectionTemplate(SectionTemplate sectionTemplate) {
        this.sectionTemplate = sectionTemplate;
    }

    @Override
    public void addAllBasket(Iterable<Basket> basket) {
        delegate.addAllBasket(basket);
    }

    @Override
    public void addAllCatchLongline(Iterable<CatchLongline> catchLongline) {
        delegate.addAllCatchLongline(catchLongline);
    }

    @Override
    public void addAllTdr(Iterable<Tdr> tdr) {
        delegate.addAllTdr(tdr);
    }

    @Override
    public void addBasket(Basket basket) {
        delegate.addBasket(basket);
    }

    @Override
    public void addCatchLongline(CatchLongline catchLongline) {
        delegate.addCatchLongline(catchLongline);
    }

    @Override
    public void addTdr(Tdr tdr) {
        delegate.addTdr(tdr);
    }

    @Override
    public void clearBasket() {
        delegate.clearBasket();
    }

    @Override
    public void clearCatchLongline() {
        delegate.clearCatchLongline();
    }

    @Override
    public void clearTdr() {
        delegate.clearTdr();
    }

    @Override
    public Set<Basket> getBasket() {
        return delegate.getBasket();
    }

    @Override
    public Basket getBasketByTopiaId(String topiaId) {
        return delegate.getBasketByTopiaId(topiaId);
    }

    @Override
    public Collection<CatchLongline> getCatchLongline() {
        return delegate.getCatchLongline();
    }

    @Override
    public CatchLongline getCatchLonglineByTopiaId(String topiaId) {
        return delegate.getCatchLonglineByTopiaId(topiaId);
    }

    @Override
    public Integer getHaulingIdentifier() {
        return delegate.getHaulingIdentifier();
    }

    @Override
    public SetLongline getSetLongline() {
        return delegate.getSetLongline();
    }

    @Override
    public Integer getSettingIdentifier() {
        return delegate.getSettingIdentifier();
    }

    @Override
    public Collection<Tdr> getTdr() {
        return delegate.getTdr();
    }

    @Override
    public Tdr getTdrByTopiaId(String topiaId) {
        return delegate.getTdrByTopiaId(topiaId);
    }

    @Override
    public boolean isBasketEmpty() {
        return delegate.isBasketEmpty();
    }

    @Override
    public boolean isCatchLonglineEmpty() {
        return delegate.isCatchLonglineEmpty();
    }

    @Override
    public boolean isTdrEmpty() {
        return delegate.isTdrEmpty();
    }

    @Override
    public void removeBasket(Basket basket) {
        delegate.removeBasket(basket);
    }

    @Override
    public void removeCatchLongline(CatchLongline catchLongline) {
        delegate.removeCatchLongline(catchLongline);
    }

    @Override
    public void removeTdr(Tdr tdr) {
        delegate.removeTdr(tdr);
    }

    @Override
    public void setBasket(Set<Basket> basket) {
        delegate.setBasket(basket);
    }

    @Override
    public void setCatchLongline(Collection<CatchLongline> catchLongline) {
        delegate.setCatchLongline(catchLongline);
    }

    @Override
    public void setHaulingIdentifier(Integer haulingIdentifier) {
        delegate.setHaulingIdentifier(haulingIdentifier);
    }

    @Override
    public void setSetLongline(SetLongline setLongline) {
        delegate.setSetLongline(setLongline);
    }

    @Override
    public void setSettingIdentifier(Integer settingIdentifier) {
        delegate.setSettingIdentifier(settingIdentifier);
    }

    @Override
    public void setTdr(Collection<Tdr> tdr) {
        delegate.setTdr(tdr);
    }

    @Override
    public int sizeBasket() {
        return delegate.sizeBasket();
    }

    @Override
    public int sizeCatchLongline() {
        return delegate.sizeCatchLongline();
    }

    @Override
    public int sizeTdr() {
        return delegate.sizeTdr();
    }

    @Override
    public String getTopiaId() {
        return delegate.getTopiaId();
    }

    @Override
    public long getTopiaVersion() {
        return delegate.getTopiaVersion();
    }

    @Override
    public Date getTopiaCreateDate() {
        return delegate.getTopiaCreateDate();
    }

    @Override
    public void setTopiaId(String id) {
        delegate.setTopiaId(id);
    }

    @Override
    public void setTopiaVersion(long version) {
        delegate.setTopiaVersion(version);
    }

    @Override
    public void setTopiaCreateDate(Date date) {
        delegate.setTopiaCreateDate(date);
    }
}
