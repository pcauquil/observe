package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.List;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DuplicateLengthWeightParameterException extends RuntimeException {

    private static final long serialVersionUID = 203463423006930783L;

    protected final Species species;

    protected final Ocean ocean;

    protected final Sex sex;

    protected final Date date;

    protected final List<LengthWeightParameter> foundLengthWeightParameters;

    public DuplicateLengthWeightParameterException(Species species, Ocean ocean, Sex sex, Date date, List<LengthWeightParameter> foundLengthWeightParameters) {
        this.species = species;
        this.ocean = ocean;
        this.sex = sex;
        this.date = date;
        this.foundLengthWeightParameters = foundLengthWeightParameters;
    }

    public Species getSpecies() {
        return species;
    }

    public Ocean getOcean() {
        return ocean;
    }

    public Sex getSex() {
        return sex;
    }

    public Date getDate() {
        return date;
    }

    public List<LengthWeightParameter> getFoundLengthWeightParameters() {
        return foundLengthWeightParameters;
    }
}
