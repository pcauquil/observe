package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.constants.GearTypePersist;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created on 8/25/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class Programs {

    /**
     * Comparateur de {@link Program} basé sur la propriété {@link
     * Program#getLabel2()}.
     *
     * FIXME-TC-20100205 : il faudrait que le comparateur soit sur le bon
     * libelle selon la langue de base choisie...
     */
    public static final Comparator<Program> PROGRAMME_COMPARATOR = (o1, o2) -> o1.getLabel2().compareTo(o2.getLabel2());

    public static void sort(List<Program> programs) {
        //FIXME-TC20100207 : make possible sort on different libelles coming
        //from the ObserConfig.getDbLocale
        Collections.sort(programs, PROGRAMME_COMPARATOR);
    }

    public static boolean isProgramLongline(Program program) {
        return GearTypePersist.longline.equals(program.getGearType());
    }

    public static boolean isProgramSeine(Program program) {
        return GearTypePersist.seine.equals(program.getGearType());
    }

}
