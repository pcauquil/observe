package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTopiaDaoSupplier;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.NumberUtil;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class LengthWeightParameters {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LengthWeightParameters.class);

    private static final Pattern COEFFICIENTS_PATTERN = Pattern.compile("(.+)=(.+)");

    /** moteur d'évaluation d'expression */
    protected static ScriptEngine scriptEngine;

    /** variable weight à utiliser dans la relation taille */
    public static final String VARIABLE_POIDS = "P";

    /** variable taille à utiliser dans la relation weight */
    public static final String VARIABLE_TAILLE = "L";

    private static final String COEFFICIENT_A = "a";

    private static final String COEFFICIENT_B = "b";

    protected static ScriptEngine getScriptEngine() {
        if (scriptEngine == null) {
            ScriptEngineManager factory = new ScriptEngineManager();

            scriptEngine = factory.getEngineByExtension("js");
        }
        return scriptEngine;
    }

    /**
     * Recherche d'un {@link LengthWeightParameter} à partir des paramètres donnés.
     *
     * La recherche peut ne peut être aussi exacte que les paramètres donnés :
     *
     * Dans le cas d'une espèce faune, si non trouvé alors on recherche sur son
     * speciesGroup d'espèce.
     *
     * Si non trouvé pour l'océan donné (et que celui-ci est non null), alors on
     * recherche avec un ocean vide.
     *
     * Si non trouvé sur le gender (et que le gender n'est pas indéterminé) , alors
     * on recherche avec le gender indéterminé (gender=0).
     *
     * @param daoSupplier la transaction en cours d'utilisation
     * @param species     l'espèce sur lequel on recherche le paramétrage
     * @param ocean       l'ocean recherché (peut être null)
     * @param sex         le sexe recherché (on essayera sans sexe (sexe.code=0) si non trouvé)
     * @param date        le jour recherché
     * @return le paramétrage adéquate
     * @since 1.5
     */
    public static LengthWeightParameter findLengthWeightParameter(ObserveTopiaDaoSupplier daoSupplier,
                                                                  Species species,
                                                                  Ocean ocean,
                                                                  Sex sex,
                                                                  Date date) {

        Sex unknownSex = getUnknownSex(daoSupplier);

        if (sex == null) {

            // on utilise le sexe indéterminé
            sex = unknownSex;
        }

        List<LengthWeightParameter> list = findLengthWeightParameter0(daoSupplier, species, ocean, sex, date);

        if (CollectionUtils.isEmpty(list) && !unknownSex.equals(sex)) {

            // on essaye avec le sexe indéterminé
            sex = unknownSex;
            list = findLengthWeightParameter0(daoSupplier, species, ocean, sex, date);
        }

        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        // au final il ne devrait en rester qu'un

        if (list.size() > 1) {
            throw new DuplicateLengthWeightParameterException(species, ocean, sex, date, list);
        }

        //        if (log.isDebugEnabled()) {
//            StringBuilder sb = new StringBuilder("Paramétrage trouvé pour les données suivantes :");
//            sb.append("\nEspece     : ");
//            sb.append(getDecoratorService().decorate(species));
//            sb.append("\nOcean      : ");
//            sb.append(getDecoratorService().decorate(ocean));
//            sb.append("\nSex       : ");
//            sb.append(getDecoratorService().decorate(sex));
//            sb.append("\nDate       : ").append(date);
//            sb.append("\nParamétrage: ").append(getDecoratorService().decorate(result));
//            log.debug(sb.toString());
//        }

        return list.get(0);

    }
    /**
     * Recherche de la liste des {@link LengthWeightParameter} à partir des paramètres donnés.
     *
     * La recherche peut ne peut être aussi exacte que les paramètres donnés :
     *
     * Dans le cas d'une espèce faune, si non trouvé alors on recherche sur son
     * speciesGroup d'espèce.
     *
     * Si non trouvé pour l'océan donné (et que celui-ci est non null), alors on
     * recherche avec un ocean vide.
     *
     * @param daoSupplier la transaction en cours d'utilisation
     * @param species     l'espèce sur lequel on recherche le paramétrage
     * @param ocean       l'ocean recherché (peut être null)
     * @param sex         le sexe recherché (on essayera sans sexe (sexe.code=0) si non trouvé)
     * @param date        le jour recherché
     * @return le paramétrage adéquate
     * @since 1.5
     */
    public static List<LengthWeightParameter> findLengthWeightParameter0(ObserveTopiaDaoSupplier daoSupplier,
                                                                         Species species,
                                                                         Ocean ocean,
                                                                         Sex sex,
                                                                         Date date) {

        Objects.requireNonNull(daoSupplier, "daoSupplier parameter can't be null");
        Objects.requireNonNull(species, "species parameter can't be null");
        Objects.requireNonNull(sex, "sex parameter can't be null");

        List<LengthWeightParameter> list = findBySpecies(daoSupplier, species);

        if (CollectionUtils.isEmpty(list)) {

            // aucun parametrage pour le type donne
            return null;
        }

        // filtrage par ocean
        List<LengthWeightParameter> filterByOcean = filterByOcean(list, ocean);

        if (CollectionUtils.isEmpty(filterByOcean) && ocean != null) {

            // filtre par ocean null
            filterByOcean = filterByOcean(list, null);
        }
        list = filterByOcean;

        if (CollectionUtils.isEmpty(list)) {

            // pas d'ocean adequate
            return null;
        }

        // filtrage par sexe
        list = filterBySexe(list, sex);

        if (CollectionUtils.isEmpty(list)) {

            // pas de sexe adequate
            return null;
        }

        // filtrage par startDate de validite
        list = filterByDateDebutValidite(list, date);

        if (CollectionUtils.isEmpty(list)) {

            // pas de date de debut adequate
            return null;
        }

        // filtrage par endDate de validite
        list = filterByDateFinValidite(list, date);

        if (CollectionUtils.isEmpty(list)) {

            // pas de date de fin adequate
            return null;
        }

        return list;

    }

    protected static Sex getUnknownSex(ObserveTopiaDaoSupplier daoSupplier) {
        return daoSupplier.getSexDao().forCodeEquals("0").findUnique();
    }

    public static Map<String, Double> getCoefficientValues(LengthWeightParameter parametrage) {

        Map<String, Double> result = new TreeMap<>();
        String coefficients = parametrage.getCoefficients();
        if (coefficients != null) {
            for (String coefficientDef : coefficients.split(":")) {
                Matcher matcher = COEFFICIENTS_PATTERN.matcher(coefficientDef.trim());
                if (log.isDebugEnabled()) {
                    log.debug("constant to test = " + coefficientDef);
                }
                if (matcher.matches()) {

                    String key = matcher.group(1);
                    String val = matcher.group(2);
                    try {
                        Double d = Double.valueOf(val);
                        result.put(key, d);
                        if (log.isDebugEnabled()) {
                            log.debug("detects coefficient " + key + '=' + val);
                        }
                    } catch (NumberFormatException e) {
                        // pas pu recupere le count...
                        if (log.isWarnEnabled()) {
                            log.warn("could not parse dou" + COEFFICIENT_B + "le " + val + " for coefficient " + key);
                        }
                    }
                }
            }
        }
        return result;
    }

    public static boolean validateWeightRelation(LengthWeightParameter parametrage) {
        return validateRelation(parametrage,
                                parametrage.getLengthWeightFormula(),
                                VARIABLE_TAILLE
        );
    }

    public static boolean validateLengthRelation(LengthWeightParameter parametrage) {
        return validateRelation(parametrage,
                                parametrage.getWeightLengthFormula(),
                                VARIABLE_POIDS
        );
    }

    public static Float computeLength(LengthWeightParameter parametrage,
                                      float weight) {
        Double b = parametrage.getCoefficientValue(COEFFICIENT_B);
        if (b == 0) {

            // ce cas limite ne permet pas de calculer la taille a partir du weight
            return null;
        }
        Float o = computeValue(parametrage,
                               parametrage.getWeightLengthFormula(),
                               VARIABLE_POIDS,
                               weight
        );

        if (o != null) {
            o = NumberUtil.roundOneDigit(o);
        }
        return o;
    }

    public static Float computeWeight(LengthWeightParameter parametrage,
                                      float taille) {

        Float o = computeValue(parametrage,
                               parametrage.getLengthWeightFormula(),
                               VARIABLE_TAILLE,
                               taille
        );

        if (o != null) {
            o = NumberUtil.roundTwoDigits(o);
        }
        return o;
    }

    @SuppressWarnings({"unchecked"})
    public static List<LengthWeightParameter> findBySpecies(ObserveTopiaDaoSupplier daoSupplier,
                                                            Species species) {

        LengthWeightParameterTopiaDao dao = daoSupplier.getLengthWeightParameterDao();

        List<LengthWeightParameter> list = dao.forSpeciesEquals(species).findAll();

        // on supprime les paramétrages qui ont a=0 ou a=null ou b=0 ou b = null
        Iterator<LengthWeightParameter> itr = list.iterator();
        while (itr.hasNext()) {
            LengthWeightParameter p = itr.next();
            Double a = p.getCoefficientValue(COEFFICIENT_A);
            if (a == null || a == 0) {
                itr.remove();
                continue;
            }

            Double b = p.getCoefficientValue(COEFFICIENT_B);
            // on autorise d'avoir b à 0 (mais cela ne permet plus de calculer la taille à partir du poids)
//            if (b == null || b == 0) {
            if (b == null) {
                itr.remove();
            }
        }
        return list;

    }

    public static List<LengthWeightParameter> filterByOcean(List<LengthWeightParameter> list, Ocean ocean) {
        List<LengthWeightParameter> result = new ArrayList<>();
        if (ocean == null) {

            // on n'accepte que les parametrage sans ocean
            for (LengthWeightParameter parametrageLengthWeight : list) {
                if (parametrageLengthWeight.getOcean() == null) {
                    result.add(parametrageLengthWeight);
                }
            }
        } else {
            for (LengthWeightParameter parametrageLengthWeight : list) {
                if (ocean.equals(parametrageLengthWeight.getOcean())) {
                    result.add(parametrageLengthWeight);
                }
            }
        }
        return result;
    }

    public static List<LengthWeightParameter> filterBySexe(List<LengthWeightParameter> list, Sex sex) {
        List<LengthWeightParameter> result = new ArrayList<>();

        for (LengthWeightParameter parametrageLengthWeight : list) {
            if (parametrageLengthWeight.getSex() == sex) {
                result.add(parametrageLengthWeight);
            }
        }
        return result;
    }

    public static List<LengthWeightParameter> filterByDateDebutValidite(List<LengthWeightParameter> list, Date startDate) {
        List<LengthWeightParameter> result = new ArrayList<>();

        for (LengthWeightParameter parametrageLengthWeight : list) {

            if (parametrageLengthWeight.getStartDate().before(startDate) ||
                parametrageLengthWeight.getStartDate().equals(startDate)) {
                result.add(parametrageLengthWeight);
            }
        }
        return result;
    }

    public static List<LengthWeightParameter> filterByDateFinValidite(List<LengthWeightParameter> list, Date endDate) {
        List<LengthWeightParameter> result = new ArrayList<>();

        if (endDate == null) {
            // on n'accepte que les parametrages selon les critères suivants :
            // - sans date de fin (i.e en cours de validite)
            for (LengthWeightParameter parametrageLengthWeight : list) {

                Date date = parametrageLengthWeight.getEndDate();
                if (date == null) {
                    result.add(parametrageLengthWeight);
                }
            }
        } else {
            // on n'accepte que les parametrages selon les critères suivants :
            // - sans date de fin (i.e en cours de validite)
            // - ceux dont la date de fin est avant la date de fin donnée
            for (LengthWeightParameter parametrageLengthWeight : list) {

                Date date = parametrageLengthWeight.getEndDate();
                if (date == null ||
                    date.after(endDate) ||
                    date.equals(endDate)) {
                    result.add(parametrageLengthWeight);
                }
            }
        }
        return result;
    }

    protected static boolean validateRelation(LengthWeightParameter parametrage, String relation, String variable) {
        boolean result = false;
        if (!StringUtils.isEmpty(relation)) {

            Map<String, Double> coeffs = parametrage.getCoefficientValues();

            ScriptEngine engine = getScriptEngine();
            Bindings bindings = engine.createBindings();
            for (Map.Entry<String, Double> entry : coeffs.entrySet()) {
                String key = entry.getKey();
                Double value = entry.getValue();
                bindings.put(key, value);

                if (log.isDebugEnabled()) {
                    log.debug("add constant " + key + '=' + value);
                }
            }
            bindings.put(variable, 1);

            try {
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
                Double o = (Double) engine.eval("parseFloat(" + relation + ")");
                if (log.isDebugEnabled()) {
                    log.debug("evaluation ok : " + relation + " (" + variable + "=1) = " + o);
                }
                result = true;
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.debug("evalution ko : " + relation + ", reason : " + e.getMessage());
                }
            }
        }
        return result;
    }

    public static Float computeValue(LengthWeightParameter parametrage, String relation, String variable, float taille) {
        Map<String, Double> coeffs = parametrage.getCoefficientValues();
        ScriptEngine engine = getScriptEngine();
        Bindings bindings = engine.createBindings();
        for (Map.Entry<String, Double> entry : coeffs.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            bindings.put(key, value);

            if (log.isDebugEnabled()) {
                log.debug("add constant " + key + '=' + value);
            }
        }
        bindings.put(variable, taille);
        engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        Double o = null;
        try {
            o = (Double) engine.eval("parseFloat(" + relation + ")");
        } catch (ScriptException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not compute value from " + relation);
            }
        }
        return o == null ? null : o.floatValue();
    }

}
