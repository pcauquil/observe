package fr.ird.observe.entities.seine;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Collections2;
import fr.ird.observe.entities.referentiel.Species;

import java.util.Collection;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SetSeines {

    public static Collection<NonTargetLength> getNonTargetLengths(SetSeine setSeine, final Species species) {

        Collection<NonTargetLength> nonTargetLengths = null;

        if (!setSeine.isNonTargetSampleEmpty()) {
            nonTargetLengths = Collections2.filter(setSeine.getNonTargetSample().iterator().next().getNonTargetLength(), input -> species.equals(input.getSpecies()));
        }

        return nonTargetLengths;

    }
}
