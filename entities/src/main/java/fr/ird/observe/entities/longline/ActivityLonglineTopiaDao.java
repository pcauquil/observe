package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.longline.VesselActivityLongline;
import fr.ird.observe.entities.referentiel.longline.VesselActivityLonglineImpl;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ActivityLonglineTopiaDao extends AbstractActivityLonglineTopiaDao<ActivityLongline> {

    public List<ActivityLongline> findAllStubByTripId(String tripId, int referenceLocale) {

        return StubSqlQuery.findAll(topiaSqlSupport, tripId, referenceLocale);

    }

    public ActivityLongline findStubByTopiaId(String activityId, int referenceLocale) {

        return StubSqlQuery.find(topiaSqlSupport, activityId, referenceLocale);

    }

    private static class StubSqlQuery extends TopiaSqlQuery<ActivityLongline> {

        private final String sql;

        private final String id;

        private final int referenceLocale;

        public static List<ActivityLongline> findAll(TopiaSqlSupport context, String tripId, int referenceLocale) {

            String sql = "SELECT" +
                         " a.topiaId," +
                         " a.timestamp," +
//                         " a.open," +
                         " a.set," +
                         " va." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_longline.activity a, observe_longline.vesselactivity va" +
                         " WHERE " +
                         " a.trip = ?" +
                         " AND a.vesselactivity = va.topiaid" +
                         " ORDER BY a.timestamp";

            StubSqlQuery request = new StubSqlQuery(sql, tripId, referenceLocale);
            return context.findMultipleResult(request);

        }

        public static ActivityLongline find(TopiaSqlSupport context, String activityId, int referenceLocale) {

            String sql = "SELECT" +
                         " a.topiaId," +
                         " a.timestamp," +
//                         " a.open," +
                         " a.set," +
                         " va." + I18nReferenceEntities.getPropertyName(referenceLocale) +
                         " FROM observe_longline.activity a, observe_longline.vesselactivity va" +
                         " WHERE " +
                         " a.topiaId = ?" +
                         " AND a.vesselactivity = va.topiaid" +
                         " ORDER BY a.timestamp";

            StubSqlQuery request = new StubSqlQuery(sql, activityId, referenceLocale);
            return context.findSingleResult(request);

        }

        StubSqlQuery(String sql, String id, int referenceLocale) {
            this.sql = sql;
            this.id = id;
            this.referenceLocale = referenceLocale;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            return preparedStatement;
        }

        @Override
        public ActivityLongline prepareResult(ResultSet set) throws SQLException {

            ActivityLongline activity = new ActivityLonglineImpl();
            activity.setTopiaId(set.getString(1));
            activity.setTimeStamp(set.getTimestamp(2));
//            activity.setOpen(set.getBoolean(3));

            String setId = set.getString(3);
            if (setId != null) {

                SetLonglineImpl setLongline = new SetLonglineImpl();
                setLongline.setTopiaId(setId);
                activity.setSetLongline(setLongline);

            }
            VesselActivityLongline vesselActivity = new VesselActivityLonglineImpl();
            String label = set.getString(4);
            I18nReferenceEntities.setLabel(referenceLocale,vesselActivity, label);
            activity.setVesselActivityLongline(vesselActivity);

            return activity;

        }

    }

}
