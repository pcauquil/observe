package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SectionTemplateTest {

    @Test
    public void testIsFloatlineLengthsValid() throws Exception {

        SectionTemplate sectionTemplate = new SectionTemplate();

        sectionTemplate.setFloatlineLengths("1.2");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2//");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/a");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1/");
        Assert.assertFalse(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());

        sectionTemplate.setFloatlineLengths("1.2/1.0");
        Assert.assertTrue(sectionTemplate.isFloatlineLengthsValid());

    }

    @Test
    public void testIsCompiliantWithBasketCount() throws Exception {


        {

            // with 2 values -> required 1 basket
            SectionTemplate sectionTemplate = new SectionTemplate();
            sectionTemplate.setFloatlineLengths("1/2");

            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(0));
            Assert.assertTrue(sectionTemplate.isCompiliantWithBasketCount(1));
            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(2));
            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(3));

        }

        {

            // with 3 values -> required 2 baskets
            SectionTemplate sectionTemplate = new SectionTemplate();
            sectionTemplate.setFloatlineLengths("1/2/3");

            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(0));
            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(1));
            Assert.assertTrue(sectionTemplate.isCompiliantWithBasketCount(2));
            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(3));

        }

        {

            // with 4 values -> required 3 baskets
            SectionTemplate sectionTemplate = new SectionTemplate();
            sectionTemplate.setFloatlineLengths("1/2/3/4");

            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(0));
            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(1));
            Assert.assertFalse(sectionTemplate.isCompiliantWithBasketCount(2));
            Assert.assertTrue(sectionTemplate.isCompiliantWithBasketCount(3));

        }

    }

    @Test
    public void testApplyToBaskets() throws Exception {

        {

            // with 1/2 -> (1,2)
            SectionTemplate sectionTemplate = new SectionTemplate();
            sectionTemplate.setFloatlineLengths("1.1/2.2");

            List<Basket> baskets = new ArrayList<>();
            baskets.add(new BasketImpl());

            sectionTemplate.applyToBaskets(baskets);

            assertBasketsFloatlinesValues(baskets, Pair.of(1.1f, 2.2f));

        }

        {

            // with 1/2/3 -> (1,2), (2,3)
            SectionTemplate sectionTemplate = new SectionTemplate();
            sectionTemplate.setFloatlineLengths("1.1/2.2/3.3");

            List<Basket> baskets = new ArrayList<>();
            baskets.add(new BasketImpl());
            baskets.add(new BasketImpl());

            sectionTemplate.applyToBaskets(baskets);

            assertBasketsFloatlinesValues(baskets, Pair.of(1.1f, 2.2f), Pair.of(2.2f, 3.3f));

        }

        {

            // with 1/2/3/4 -> (1,2), (2,3), (3,4)
            SectionTemplate sectionTemplate = new SectionTemplate();
            sectionTemplate.setFloatlineLengths("1.1/2.2/3.3/4.4");

            List<Basket> baskets = new ArrayList<>();
            baskets.add(new BasketImpl());
            baskets.add(new BasketImpl());
            baskets.add(new BasketImpl());

            sectionTemplate.applyToBaskets(baskets);

            assertBasketsFloatlinesValues(baskets, Pair.of(1.1f, 2.2f), Pair.of(2.2f, 3.3f), Pair.of(3.3f, 4.4f));

        }

    }

    protected void assertBasketsFloatlinesValues(List<Basket> baskets, Pair<Float, Float>... expectedFloatlinesLengths) {

        for (int i = 0; i < expectedFloatlinesLengths.length; i++) {

            Pair<Float, Float> expectedValue = expectedFloatlinesLengths[i];
            Basket basket = baskets.get(i);

            Assert.assertEquals(expectedValue.getLeft(), basket.getFloatline1Length());
            Assert.assertEquals(expectedValue.getRight(), basket.getFloatline2Length());

        }
    }
}
