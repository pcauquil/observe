/*
 * #%L
 * ObServe :: Entities
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ird.observe;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Pattern;

public class RelationRegexTest {

    public static Pattern coefficientPattern =
            Pattern.compile("\\w+=[0-9]+(\\.[0-9]+)*(:\\w+=[0-9]+(\\.[0-9]+)*)*");

    @Test
    public void testCoefficientRegex() {
        String actual;

        actual = "a";
        matchesPattern(actual, false);


        actual = "=";
        matchesPattern(actual, false);

        actual = "a=";
        matchesPattern(actual, false);

        actual = "=a";
        matchesPattern(actual, false);

        actual = "=0";
        matchesPattern(actual, false);

        actual = "a=0";
        matchesPattern(actual, true);

        actual = "a=0:";
        matchesPattern(actual, false);

        actual = "a=0:b";
        matchesPattern(actual, false);

        actual = "a=0:b=";
        matchesPattern(actual, false);

        actual = "a=0:=b";
        matchesPattern(actual, false);

        actual = "a=0:b=b";
        matchesPattern(actual, false);

        actual = "a=0:b=1";
        matchesPattern(actual, true);

    }

    protected static void matchesPattern(String actual, boolean expectedValue) {
        String message = actual + " should " +
                (expectedValue ? "" : " not ") + " match pattern";
        Assert.assertEquals(message, expectedValue,
                coefficientPattern.matcher(actual).matches());
    }
}
