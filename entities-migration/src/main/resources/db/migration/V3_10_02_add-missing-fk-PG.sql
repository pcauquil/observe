---
-- #%L
-- ObServe :: Entities Migration
-- %%
-- Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
ALTER TABLE OBSERVE_LONGLINE.SET ADD CONSTRAINT FK_SET_SETTINGSHAPE FOREIGN KEY(SETTINGSHAPE) REFERENCES OBSERVE_LONGLINE.SETTINGSHAPE(topiaid);
ALTER TABLE OBSERVE_LONGLINE.SET ADD CONSTRAINT FK_SET_LINETYPE FOREIGN KEY(LINETYPE) REFERENCES OBSERVE_LONGLINE.LINETYPE(topiaid);
ALTER TABLE OBSERVE_LONGLINE.SET ADD CONSTRAINT FK_SET_LIGHTSTICKSTYPE FOREIGN KEY(LIGHTSTICKSTYPE) REFERENCES OBSERVE_LONGLINE.LIGHTSTICKSTYPE(topiaid);
ALTER TABLE OBSERVE_LONGLINE.SET ADD CONSTRAINT FK_SET_LIGHTSTICKSCOLOR FOREIGN KEY(LIGHTSTICKSCOLOR) REFERENCES OBSERVE_LONGLINE.LIGHTSTICKSCOLOR(topiaid);
ALTER TABLE OBSERVE_LONGLINE.WEIGHTMEASURE ADD CONSTRAINT FK_WEIGHTMEASURE_WEIGHTMEASURETYPE FOREIGN KEY(WEIGHTMEASURETYPE) REFERENCES OBSERVE_LONGLINE.WEIGHTMEASURETYPE(topiaid);
