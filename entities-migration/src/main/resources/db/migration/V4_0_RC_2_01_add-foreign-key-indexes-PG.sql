---
-- #%L
-- ObServe :: Entities Migration
-- %%
-- Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE INDEX idx_observe_common_gear_gearcaracteristic_gear ON OBSERVE_COMMON.gear_gearcaracteristic(gear);
CREATE INDEX idx_observe_common_ocean_species_species ON OBSERVE_COMMON.ocean_species(species);
CREATE INDEX idx_observe_common_species_specieslist_specieslist ON OBSERVE_COMMON.species_specieslist(speciesList);
CREATE INDEX idx_observe_longline_activity_trip ON OBSERVE_LONGLINE.Activity(trip);
CREATE INDEX idx_observe_longline_baitscomposition_set ON OBSERVE_LONGLINE.baitsComposition(set);
CREATE INDEX idx_observe_longline_basket_section ON OBSERVE_LONGLINE.basket(section);
CREATE INDEX idx_observe_longline_branchline_basket ON OBSERVE_LONGLINE.branchline(basket);
CREATE INDEX idx_observe_longline_branchlinescomposition_set ON OBSERVE_LONGLINE.branchlinesComposition(set);
CREATE INDEX idx_observe_longline_catch_basket ON OBSERVE_LONGLINE.Catch(basket);
CREATE INDEX idx_observe_longline_catch_branchline ON OBSERVE_LONGLINE.Catch(branchline);
CREATE INDEX idx_observe_longline_catch_predator_catch ON OBSERVE_LONGLINE.catch_predator(catch);
CREATE INDEX idx_observe_longline_catch_section ON OBSERVE_LONGLINE.Catch(section);
CREATE INDEX idx_observe_longline_catch_set ON OBSERVE_LONGLINE.Catch(set);
CREATE INDEX idx_observe_longline_encounter_activity ON OBSERVE_LONGLINE.encounter(activity);
CREATE INDEX idx_observe_longline_floatlinescomposition_set ON OBSERVE_LONGLINE.floatlinesComposition(set);
CREATE INDEX idx_observe_longline_hookscomposition_set ON OBSERVE_LONGLINE.hooksComposition(set);
CREATE INDEX idx_observe_longline_mitigationtype_set_set ON OBSERVE_LONGLINE.mitigationtype_set(set);
CREATE INDEX idx_observe_longline_section_set ON OBSERVE_LONGLINE.section(set);
CREATE INDEX idx_observe_longline_sensorused_activity ON OBSERVE_LONGLINE.sensorUsed(activity);
CREATE INDEX idx_observe_longline_sizemeasure_catch ON OBSERVE_LONGLINE.sizeMeasure(catch);
CREATE INDEX idx_observe_longline_species_tdr_tdr ON OBSERVE_LONGLINE.species_tdr(tdr);
CREATE INDEX idx_observe_longline_tdr_basket ON OBSERVE_LONGLINE.tdr(basket);
CREATE INDEX idx_observe_longline_tdr_branchline ON OBSERVE_LONGLINE.tdr(branchline);
CREATE INDEX idx_observe_longline_tdrrecord_basket ON OBSERVE_LONGLINE.tdrRecord(basket);
CREATE INDEX idx_observe_longline_tdrrecord_tdr ON OBSERVE_LONGLINE.tdrRecord(tdr);
CREATE INDEX idx_observe_longline_tdr_section ON OBSERVE_LONGLINE.tdr(section);
CREATE INDEX idx_observe_longline_tdr_set ON OBSERVE_LONGLINE.tdr(set);
CREATE INDEX idx_observe_longline_weightmeasure_catch ON OBSERVE_LONGLINE.weightMeasure(catch);
CREATE INDEX idx_observe_seine_activity_observedsystem_activity ON OBSERVE_SEINE.activity_observedsystem(activity);
CREATE INDEX idx_observe_seine_activity_route ON OBSERVE_SEINE.Activity(route);
CREATE INDEX idx_observe_seine_floatingobject_activity ON OBSERVE_SEINE.floatingObject(activity);
CREATE INDEX idx_observe_seine_gearusefeaturesmeasurement_gearusefeatures ON OBSERVE_SEINE.GearUseFeaturesMeasurement(gearUseFeatures);
CREATE INDEX idx_observe_seine_gearusefeatures_trip ON OBSERVE_SEINE.GearUseFeatures(trip);
CREATE INDEX idx_observe_seine_nontargetcatch_set ON OBSERVE_SEINE.nonTargetCatch(set);
CREATE INDEX idx_observe_seine_nontargetlength_nontargetsample ON OBSERVE_SEINE.nonTargetLength(nonTargetSample);
CREATE INDEX idx_observe_seine_nontargetsample_set ON OBSERVE_SEINE.nonTargetSample(set);
CREATE INDEX idx_observe_seine_objectobservedspecies_floatingobject ON OBSERVE_SEINE.objectObservedSpecies(floatingObject);
CREATE INDEX idx_observe_seine_objectschoolestimate_floatingobject ON OBSERVE_SEINE.objectSchoolEstimate(floatingObject);
CREATE INDEX idx_observe_seine_route_trip ON OBSERVE_SEINE.route(trip);
CREATE INDEX idx_observe_seine_schoolestimate_set ON OBSERVE_SEINE.schoolEstimate(set);
CREATE INDEX idx_observe_seine_targetcatch_set ON OBSERVE_SEINE.targetCatch(set);
CREATE INDEX idx_observe_seine_targetlength_targetsample ON OBSERVE_SEINE.targetLength(targetSample);
CREATE INDEX idx_observe_seine_targetsample_set ON OBSERVE_SEINE.targetSample(set);
CREATE INDEX idx_observe_seine_transmittingbuoy_floatingobject ON OBSERVE_SEINE.transmittingBuoy(floatingObject);
