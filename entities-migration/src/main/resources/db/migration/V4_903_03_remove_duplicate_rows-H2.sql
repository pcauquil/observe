---
-- #%L
-- ObServe :: Entities Migration
-- %%
-- Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
-- suppression des doublons dans la table observe_seine.activity_observedsystem
-------------------------------------------------------------------------------


-- creation d'un table temporaire
CREATE TABLE ACTIVITY_OBSERVEDSYSTEM_TMP(ACTIVITY VARCHAR(255) NOT NULL, OBSERVEDSYSTEM VARCHAR(255) NOT NULL);

-- copie les données de la table vers la table temporaire
INSERT INTO ACTIVITY_OBSERVEDSYSTEM_TMP (ACTIVITY, OBSERVEDSYSTEM) SELECT DISTINCT ACTIVITY, OBSERVEDSYSTEM FROM OBSERVE_SEINE.ACTIVITY_OBSERVEDSYSTEM;

-- suppession des enregistrements de la table
DELETE FROM OBSERVE_SEINE.ACTIVITY_OBSERVEDSYSTEM;

-- Ajout de la contrainte
ALTER TABLE OBSERVE_SEINE.ACTIVITY_OBSERVEDSYSTEM ADD CONSTRAINT PK_ACTIVITY_OBSERVEDSYSTEM PRIMARY KEY (ACTIVITY, OBSERVEDSYSTEM);

-- on remet les enregistrement dans la table
INSERT INTO OBSERVE_SEINE.ACTIVITY_OBSERVEDSYSTEM (ACTIVITY, OBSERVEDSYSTEM) SELECT ACTIVITY, OBSERVEDSYSTEM FROM ACTIVITY_OBSERVEDSYSTEM_TMP;

-- suppression de la table temporaire
DROP TABLE ACTIVITY_OBSERVEDSYSTEM_TMP;
