package fr.ird.observe.entities.migration;

/*-
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 01/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public final class ObserveMigrationConfigurationProviderImpl extends ObserveMigrationConfigurationProvider {

    public enum ObserveMigrationVersions {

        V_3_0("3.0", false),
        V_3_1("3.1"),
        V_3_5("3.5"),
        V_3_7("3.7"),
        V_3_8("3.8"),
        V_3_9("3.9"),
        V_3_10("3.10"),
        V_3_11("3.11"),
        V_3_12("3.12"),
        V_3_14("3.14"),
        V_3_15("3.15"),
        V_3_16("3.16"),
        V_4_0_RC2("4.0-RC2"),
        V_4_0_RC3("4.0-RC3"),
        V_4_0_RC4("4.0-RC4"),
        V_4_0_RC6("4.0-RC6"),
        V_4_0_RC7("4.0-RC7"),
        V_4_0("4.0"),
        V_4_0_1("4.0.1"),
        V_4_0_2("4.0.2"),
        V_4_0_4("4.0.4"),
        V_4_900("4.900"),
        V_4_901("4.901"),
        V_4_902("4.902"),
        V_4_903("4.903"),
        V_4_904("4.904"),
        V_4_905("4.905"),
        V_4_906("4.906"),
        V_5_0("5.0");


        private final Version version;
        private final boolean canApply;

        ObserveMigrationVersions(String versionAsString, boolean canApply) {
            this.version = Versions.valueOf(versionAsString);
            this.canApply = canApply;
        }

        ObserveMigrationVersions(String versionAsString) {
            this(versionAsString, true);
        }

        public Version getVersion() {
            return version;
        }

    }

    private final Version[] availableVersions;

    public ObserveMigrationConfigurationProviderImpl() {

        Set<Version> result = new LinkedHashSet<>();
        for (ObserveMigrationVersions version : ObserveMigrationVersions.values()) {
            if (version.canApply) {
                result.add(version.getVersion());
            }
        }
        this.availableVersions = result.toArray(new Version[result.size()]);
    }

    @Override
    public Version[] getAvailableVersions() {
        return availableVersions;
    }

    @Override
    public Version getLastVersion() {
        Version[] values = getAvailableVersions();
        int lastIndex = values.length - 1;
        return values[lastIndex];
    }

    @Override
    public Version getMinimumVersion() {
        return ObserveMigrationVersions.V_3_0.getVersion();
    }

    @Override
    public ImmutableList<Version> getVersionsAfter(Version current) {

        ImmutableList.Builder<Version> builder = ImmutableList.builder();

        for (Version version : getAvailableVersions()) {

            if (version.after(current)) {
                builder.add(version);
            }

        }

        return builder.build();

    }

    @Override
    public Class<? extends TopiaMigrationCallbackByClass> getMigrationClassBack(boolean h2) {
        return h2 ? H2DataSourceMigration.class : PGDataSourceMigration.class;
    }

}
