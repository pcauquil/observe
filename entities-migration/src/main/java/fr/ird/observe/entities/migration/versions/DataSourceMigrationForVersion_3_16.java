package fr.ird.observe.entities.migration.versions;

/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.H2DataSourceMigration;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import fr.ird.observe.entities.migration.PGDataSourceMigration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 3/23/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.16
 */
public class DataSourceMigrationForVersion_3_16 extends AbstractObserveMigrationCallBack {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataSourceMigrationForVersion_3_16.class);

    public DataSourceMigrationForVersion_3_16(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_3_16.getVersion(), callBack, scriptSuffix);
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {

        addScript("01", "add-gear-referential", queries);
        addScript("02", "add-gear-data", queries);
        migrateTripSenneData(topiaSqlSupport, queries);
        addScript("03", "remove-tripSeine-seine", queries);

    }

    private void migrateTripSenneData(TopiaSqlSupport  tx, List<String> queries) {

        String gearUseFeaturesIdPrefix = "fr.ird.observe.entities.seine.GearUseFeatures#1427183650941#";

        int gearUseFeaturesCount = -1;
        Set<SenneData> senneData = getSenneData(tx);
        for (SenneData senne : senneData) {

            String gearUseFeaturesId = gearUseFeaturesIdPrefix + (++gearUseFeaturesCount);
            if (log.isInfoEnabled()) {
                log.info(String.format("Transform senne from trip: %s to: %s", senne.tripId, gearUseFeaturesId));
            }
            // Create GearUseFeatures
            queries.add(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, TRIP, GEAR, NUMBER) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', '%s', 'fr.ird.observe.entities.referentiel.Gear#1239832686125#0.20', 1);", gearUseFeaturesId, senne.tripId));

            // Create GearUseFeaturesMeasurements
            String gearUseFeatureMeasurementIdPrefix = "fr.ird.observe.entities.seine.GearUseFeaturesMeasurement#1427183650941#" + gearUseFeaturesCount;
            if (senne.seineCircumference != null) {
                String gearUseFeatureMeasurementId = gearUseFeatureMeasurementIdPrefix + 0;
                queries.add(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEARCARACTERISTIC, GEARUSEFEATURES, MEASUREMENTVALUE) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.7', '%s', '%s' );", gearUseFeatureMeasurementId, gearUseFeaturesId, senne.seineCircumference));
            }
            if (senne.seineDepth != null) {
                String gearUseFeatureMeasurementId = gearUseFeatureMeasurementIdPrefix + 1;
                queries.add(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEARCARACTERISTIC, GEARUSEFEATURES, MEASUREMENTVALUE) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.10', '%s', '%s' );", gearUseFeatureMeasurementId, gearUseFeaturesId, senne.seineBallastWeight));
            }
            if (senne.seineBallastWeight != null) {
                String gearUseFeatureMeasurementId = gearUseFeatureMeasurementIdPrefix + 2;
                queries.add(String.format("INSERT INTO OBSERVE_SEINE.GEARUSEFEATURESMEASUREMENT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, GEARCARACTERISTIC, GEARUSEFEATURES, MEASUREMENTVALUE) VALUES ('%s', 0, TIMESTAMP '2015-03-24 00:00:00.00', 'fr.ird.observe.entities.referentiel.GearCaracteristic#1239832686124#0.9', '%s', '%s' );", gearUseFeatureMeasurementId, gearUseFeaturesId, senne.seineDepth));
            }
        }
    }

    protected Set<SenneData> getSenneData(TopiaSqlSupport  tx) {

        final Set<SenneData> result = new HashSet<>();

        tx.doSqlWork(connection -> {
            String sql = "SELECT topiaId, seineCircumference, seineDepth, seineBallastWeight FROM OBSERVE_SEINE.TRIP WHERE seineDepth IS NOT NULL OR seineDepth IS NOT NULL OR seineBallastWeight IS NOT NULL;";
            PreparedStatement ps = connection.prepareStatement(sql);
            try {
                ResultSet set = ps.executeQuery();
                while (set.next()) {
                    SenneData senneData = new SenneData();
                    senneData.tripId = set.getString(1);
                    senneData.seineCircumference = set.getInt(2);
                    senneData.seineDepth = set.getInt(3);
                    senneData.seineBallastWeight = set.getInt(4);
                    result.add(senneData);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain trip senne data", e);
            } finally {
                ps.close();
            }
        });

        return result;
    }

    private static class SenneData {

        String tripId;

        Integer seineCircumference;

        Integer seineDepth;

        Integer seineBallastWeight;
    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_3_16 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_3_16 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

    }

}
