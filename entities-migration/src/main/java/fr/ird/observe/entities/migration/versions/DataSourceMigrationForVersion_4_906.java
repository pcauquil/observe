package fr.ird.observe.entities.migration.versions;

/*-
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.H2DataSourceMigration;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import fr.ird.observe.entities.migration.PGDataSourceMigration;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 02/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public abstract class DataSourceMigrationForVersion_4_906 extends AbstractObserveMigrationCallBack {

    public DataSourceMigrationForVersion_4_906(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_4_906.getVersion(), callBack, scriptSuffix);
    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_906 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

        @Override
        protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport, List<String> queries, boolean showSql, boolean showProgression) {
            Set<String> forkeignKeys = H2DataSourceMigration.getForeignKeyConstraintNames(topiaSqlSupport, "nontargetlength");
            Set<String> collect = forkeignKeys.stream().map(String::toLowerCase).collect(Collectors.toSet());
            if (!collect.contains("fk_nontargetlength_sex")) {
                addScript("01", "add_non_target_length_sex_fk", queries);
            }
        }
    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_906 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

        @Override
        protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport, List<String> queries, boolean showSql, boolean showProgression) {
            Set<String> forkeignKeys = PGDataSourceMigration.getForeignKeyConstraintNames(topiaSqlSupport, "nontargetlength");
            Set<String> collect = forkeignKeys.stream().map(String::toLowerCase).collect(Collectors.toSet());
            if (!collect.contains("fk_nontargetlength_sex")) {
                addScript("01", "add_non_target_length_sex_fk", queries);
            }
        }

    }

}
