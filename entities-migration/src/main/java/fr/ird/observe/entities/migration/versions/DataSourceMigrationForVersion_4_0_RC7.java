package fr.ird.observe.entities.migration.versions;

/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.H2DataSourceMigration;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import fr.ird.observe.entities.migration.PGDataSourceMigration;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.util.List;

/**
 * Created on 5/27/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0-RC7
 */
public abstract class DataSourceMigrationForVersion_4_0_RC7 extends AbstractObserveMigrationCallBack {

    public DataSourceMigrationForVersion_4_0_RC7(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_4_0_RC7.getVersion(), callBack, scriptSuffix);
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {


        // See https://forge.codelutin.com/issues/6983
        // Should have been donne in RC3, but was missed!
        addScript("01", "update-senne-gear-usedInTrip", queries);

        // See https://forge.codelutin.com/issues/6991
        // Should have been donne in RC3, but was missed!
        addScript("02", "rename-unknown-longliner", queries);

    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_RC7 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_RC7 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

    }


}

