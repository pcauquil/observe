/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.migration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.version.Version;

import java.lang.reflect.Modifier;

/**
 * Le resolveur de classe de migration par version.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
class ObserveMigrationCallBackForVersionResolver implements TopiaMigrationCallbackByClass.MigrationCallBackForVersionResolver {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(ObserveMigrationCallBackForVersionResolver.class);

    protected final String prefix;

    public ObserveMigrationCallBackForVersionResolver(String prefix) {
        this.prefix = prefix;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Class<? extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion> getCallBack(Version version) {

        String mainclassName = getClass().getPackage().getName() + ".versions.DataSourceMigrationForVersion_" + version.getValidName();
        Class<? extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion> result = null;
        try {
            Class<?> mainClass = Class.forName(mainclassName);
            Class<?>[] declaredClasses = mainClass.getClasses();
            if (declaredClasses.length == 0) {

                // no specified class for the version, use the main one
                result = (Class<? extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion>) mainClass;

                // must check that the main class is not abstract
                if (Modifier.isAbstract(mainClass.getModifiers())) {
                    throw new IllegalStateException("Could not find specialized migration class for version [" + version + "] in [" + mainclassName + "]");
                }
            } else {

                // some specialized class exists, find the correct (with good prefix)
                for (Class<?> declaredClass : declaredClasses) {
                    if (declaredClass.getSimpleName().startsWith(prefix)) {

                        // found the good class
                        result = (Class<? extends TopiaMigrationCallbackByClass.MigrationCallBackForVersion>) declaredClass;
                        break;
                    }
                }

                if (result == null) {
                    throw new IllegalStateException("Could not find specialized migration class for version [" + version + "] in [" + mainclassName + "]");
                }
            }

        } catch (ClassNotFoundException e) {
            throw new TopiaException("Could not find migration class [" + mainclassName + "] for version " + version);
        }

        if (log.isDebugEnabled()) {
            log.debug("[" + prefix + "] Resolved callback for version [" + version + "] : " + result.getName());
        }
        return result;
    }
}
