package fr.ird.observe.entities.migration.versions;

/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.H2DataSourceMigration;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import fr.ird.observe.entities.migration.PGDataSourceMigration;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 4/10/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0-RC2
 */
public class DataSourceMigrationForVersion_4_0_RC2 extends AbstractObserveMigrationCallBack {

    public DataSourceMigrationForVersion_4_0_RC2(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_4_0_RC2.getVersion(), callBack, scriptSuffix);
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {


        // recherche du nom de la constrainte
        final Set<String> result = new HashSet<>();

        topiaSqlSupport.doSqlWork(connection -> {

            String sql = "select distinct (geartype||'') from observe_common.program";
            {
                PreparedStatement ps = connection.prepareStatement(sql);
                try {
                    ResultSet set = ps.executeQuery();
                    while (set.next()) {
                        String gearType = set.getString(1);
                        result.add(gearType);
                    }

                } catch (Exception e) {
                    throw new SQLException("Could not obtain program gear types", e);
                } finally {
                    ps.close();
                }
            }

        });

        if (result.contains("seine")) {

            addScript("01", "clean-program-gear-type", queries);
        }

    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_RC2 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_RC2 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

    }

}
