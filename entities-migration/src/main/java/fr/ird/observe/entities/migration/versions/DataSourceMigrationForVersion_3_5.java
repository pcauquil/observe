package fr.ird.observe.entities.migration.versions;

/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.H2DataSourceMigration;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import fr.ird.observe.entities.migration.PGDataSourceMigration;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.util.List;
import java.util.Set;

/**
 * Created on 6/19/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public abstract class DataSourceMigrationForVersion_3_5 extends AbstractObserveMigrationCallBack {

    protected static final String[] TABLES = new String[]{
            "ParametrageTaillePoidsFaune",
            "ParametrageTaillePoidsThon",
            "ActiviteEnvironnante",
            "EstimationBancObjet",
            "EspeceFauneObservee",
            "GroupeEspeceFaune",
            "CauseNonCoupSenne",
            "EchantillonFaune",
            "OperationBalise",
            "EchantillonThon",
            "CategorieBateau",
            "SystemeObserve",
            "OperationObjet",
            "EstimationBanc",
            "CategoriePoids",
            "ActiviteBateau",
            "ObjetFlottant",
            "ModeDetection",
            "VentBeaufort",
            "StatutEspece",
            "DevenirObjet",
            "DevenirFaune",
            "CauseCoupNul",
            "CaptureFaune",
            "TailleFaune",
            "RaisonRejet",
            "Observateur",
            "EspeceFaune",
            "EspeceThon",
            "CaptureThon",
            "TypeBateau",
            "TypeBalise",
            "TailleThon",
            "TypeObjet",
            "Programme",
            "Organisme",
            "BaliseLue",
            "Activite",
            "Bateau",
            "Route",
            "Ocean",
            "Maree",
            "Calee",
            "Pays",
            "ESPECEFAUNE_OCEAN",
            "ESPECETHON_OCEAN",
            "ACTIVITE_SYSTEMEOBSERVE"
    };

    public DataSourceMigrationForVersion_3_5(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_3_5.getVersion(), callBack, scriptSuffix);
    }

    protected abstract void removeFK(TopiaSqlSupport tx, String tableName, List<String> queries) throws TopiaException;

    protected abstract void removeUK(TopiaSqlSupport tx, String tableName, List<String> queries) throws TopiaException;

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {

        // Get all especeThon (to migrate them into a speciesList later)
        Set<String> speciesThonIds = getTopiaIds(topiaSqlSupport, "especethon");

        // Get all especeFaune (to migrate them into a speciesList later)
        Set<String> speciesFauneIds = getTopiaIds(topiaSqlSupport, "especefaune");

        // translate model (see http://forge.codelutin.com/issues/4115)
        // migrate wind (see http://forge.codelutin.com/issues/5304)
        // migrate persons (see http://forge.codelutin.com/issues/5303)
        // add captain and dataInputor on Trip (see http://forge.codelutin.com/issues/5305)
        // add gearType on Program (see http://forge.codelutin.com/issues/5604)
        translateModel(topiaSqlSupport, queries);

        // add SpeciesList (see http://forge.codelutin.com/issues/)
        addSpeciesList(queries, speciesThonIds, speciesFauneIds);

        // add longline schema
        addScript("02", "add-longline-schema", queries);

        // update common references
        addScript("03", "update-common-references", queries);

        // add longline references
        addScript("04", "add-longline-references", queries);

    }

    protected void translateModel(TopiaSqlSupport topiaSqlSupport, List<String> queries) {

        for (String oldTableName : TABLES) {

            removeFK(topiaSqlSupport, oldTableName, queries);
            removeUK(topiaSqlSupport, oldTableName, queries);

        }
        addScript("01", "migration", queries);
    }

    protected void addSpeciesList(List<String> queries, Set<String> speciesThonIds, Set<String> speciesFauneIds) {

        String insertListQuery = "INSERT INTO OBSERVE_COMMON.SPECIESLIST (topiaId, topiaCreateDate, topiaversion, code, label1, label2, label3) VALUES('%s', '%s', 0, '%s', '%s', '%s', '%s');";
        String insertSpeciesQuery = "INSERT INTO OBSERVE_COMMON.SPECIES_SPECIESLIST (SPECIESLIST, SPECIES) VALUES('%s','%s');";

        // add targetSpeciesSeine list
        {
            String speciesListId = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.1";
            queries.add(String.format(insertListQuery, speciesListId, "2014-06-23", "0", "PS - Target species", "PS - Thonidés (espèces ciblées)", "PS - Target species"));
            for (String oldId : speciesThonIds) {
                String newId = oldId.replace("EspeceThon", "Species");
                queries.add(String.format(insertSpeciesQuery, speciesListId, newId));
            }
        }

        // add nonTargetSpeciesSeine list
        {
            String speciesListId = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.2";
            queries.add(String.format(insertListQuery, speciesListId, "2014-06-23", "1", "PS - Non target species", "PS - Faune associée (espèces non ciblée)", "PS - Non target species"));
            for (String oldId : speciesFauneIds) {
                String newId = oldId.replace("EspeceFaune", "Species");
                queries.add(String.format(insertSpeciesQuery, speciesListId, newId));
            }
        }

        // add catchSpeciesLongline list
        queries.add(String.format(insertListQuery, "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3", "2014-06-23", "2", "LL - Catch species", "LL - Espèces capturées", "LL - Catch species"));

        // add encounterSpeciesLongline list
        queries.add(String.format(insertListQuery, "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4", "2014-06-23", "3", "LL - Species for encounters", "LL - Espèces des rencontres", "LL - Species for encounters"));

        // add baitSpeciesLongline list
        queries.add(String.format(insertListQuery, "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.5", "2014-06-23", "4", "LL - Bait species", "LL - Appâts", "LL - Bait species"));

        // add depredatorsSpeciesLongline list
        queries.add(String.format(insertListQuery, "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.6", "2014-06-23", "5", "LL - Depredators", "LL - Déprédateurs", "LL - Depredators"));

    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_3_5 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

        @Override
        protected void removeFK(TopiaSqlSupport tx, String tableName, List<String> queries) {

            H2DataSourceMigration.removeFK(tx, tableName, queries);

        }

        @Override
        protected void removeUK(TopiaSqlSupport tx, String tableName, List<String> queries) {

            H2DataSourceMigration.removeUK(tx, tableName, queries);

        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_3_5 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

        @Override
        protected void removeFK(TopiaSqlSupport tx, String tableName, List<String> queries) {

            PGDataSourceMigration.removeFK(tx, tableName, queries);

        }

        @Override
        protected void removeUK(TopiaSqlSupport tx, String tableName, List<String> queries) {

            PGDataSourceMigration.removeUK(tx, tableName, queries);

        }

    }

}

