package fr.ird.observe.entities.migration.versions;

/*
 * #%L
 * ObServe :: Entities Migration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.AbstractDataSourceMigration;
import fr.ird.observe.entities.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.entities.migration.H2DataSourceMigration;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProviderImpl;
import fr.ird.observe.entities.migration.PGDataSourceMigration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 4/16/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0-RC3
 */
public abstract class DataSourceMigrationForVersion_4_0_RC3 extends AbstractObserveMigrationCallBack {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(DataSourceMigrationForVersion_4_0_RC3.class);

    public DataSourceMigrationForVersion_4_0_RC3(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(ObserveMigrationConfigurationProviderImpl.ObserveMigrationVersions.V_4_0_RC3.getVersion(), callBack, scriptSuffix);
    }

    protected abstract void addMissingForeignKeys(TopiaSqlSupport tx, List<String> queries) throws TopiaException;

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {


        // See https://forge.codelutin.com/issues/6964
        addMissingForeignKeys(topiaSqlSupport, queries);

        // See https://forge.codelutin.com/issues/6983
        addScript("02", "update-senne-gear-usedInTrip", queries);

        // See https://forge.codelutin.com/issues/6991
        addScript("03", "rename-unknown-longliner", queries);

    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_RC3 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

        @Override
        protected void addMissingForeignKeys(TopiaSqlSupport tx, List<String> queries) {
            // Nothing to do for h2 db
        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_RC3 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

        @Override
        protected void addMissingForeignKeys(TopiaSqlSupport tx, List<String> queries) {

            removeForeignKeyIndex(tx, queries, "OBSERVE_COMMON", "gear_gearcaracteristic", "gear");
            removeForeignKeyIndex(tx, queries, "OBSERVE_COMMON", "ocean_species", "species");
            removeForeignKeyIndex(tx, queries, "OBSERVE_COMMON", "species_specieslist", "speciesList");

            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "activity", "trip");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "baitsComposition", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "basket", "section");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "branchline", "basket");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "branchlinesComposition", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "catch", "basket");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "catch", "branchline");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "catch_predator", "catch");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "catch", "section");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "catch", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "encounter", "activity");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "floatlinesComposition", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "hooksComposition", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "mitigationtype_set", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "section", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "sensorUsed", "activity");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "sizeMeasure", "catch");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "species_tdr", "tdr");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "tdr", "basket");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "tdr", "branchline");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "tdrRecord", "basket");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "tdrRecord", "tdr");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "tdr", "section");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "tdr", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_LONGLINE", "weightMeasure", "catch");

            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "activity_observedsystem", "activity");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "activity", "route");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "floatingObject", "activity");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "gearUseFeaturesMeasurement", "gearUseFeatures");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "gearUseFeatures", "trip");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "nonTargetCatch", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "nonTargetLength", "nonTargetSample");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "nonTargetSample", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "objectObservedSpecies", "floatingObject");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "objectSchoolEstimate", "floatingObject");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "route", "trip");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "schoolEstimate", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "targetCatch", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "targetLength", "targetSample");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "targetSample", "set");
            removeForeignKeyIndex(tx, queries, "OBSERVE_SEINE", "transmittingBuoy", "floatingObject");

            addScript("01", "add-foreign-key-indexes", queries);

        }

        private void removeForeignKeyIndex(TopiaSqlSupport tx, final List<String> queries, final String schemaName, final String tableName, final String columnName) {

            tx.doSqlWork(connection -> {

                // get table oid
                int oid = getTableOid(connection, schemaName, tableName);

                // get attribute num
                int attNum = getAttributeNum(connection, oid, columnName);

                Set<Integer> indexIds = getIndexId(connection, oid, attNum);

                for (Integer indexId : indexIds) {

                    String indexName = getIndexName(connection, indexId);
                    queries.add("DROP INDEX " + schemaName + "." + indexName + ";");

                }

            });

        }

        protected Integer getTableOid(Connection connection, String schemaName, String tableName) throws SQLException {

            Integer oid = null;

            String sqlOid = "SELECT '" + schemaName + "." + tableName + "'::regclass::oid;";
            PreparedStatement ps = connection.prepareStatement(sqlOid);
            try {
                ResultSet set = ps.executeQuery();
                if (set.next()) {
                    oid = set.getInt(1);
                    if (log.isDebugEnabled())
                        log.debug("found table oid " + schemaName + "." + tableName + ": " + oid);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain oid for table" + tableName, e);
            } finally {
                ps.close();
            }

            return oid;

        }

        protected Integer getAttributeNum(Connection connection, int oid, String columnName) throws SQLException {

            Integer attNum = null;

            String attNumSql = "SELECT attnum FROM pg_attribute WHERE attrelid = ? AND attname = ?";
            PreparedStatement ps = connection.prepareStatement(attNumSql);
            ps.setInt(1, oid);
            ps.setString(2, columnName.toLowerCase());
            try {
                ResultSet set = ps.executeQuery();
                if (set.next()) {
                    attNum = set.getInt(1);
                    if (log.isDebugEnabled())
                        log.debug("found attribute " + columnName + " attNum : " + attNum);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain attNum for column" + columnName, e);
            } finally {
                ps.close();
            }

            return attNum;

        }

        protected Set<Integer> getIndexId(Connection connection, int oid, int attNum) throws SQLException {

            Set<Integer> indexIds = new HashSet<>();

            String sql = "SELECT indexrelid FROM pg_index " +
                    "WHERE indrelid = ? " +
                    "AND indkey = '" + attNum + "' " +
                    "AND indisunique = FALSE " +
                    "AND indisprimary = FALSE;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, oid);

            try {
                ResultSet set = ps.executeQuery();

                while (set.next()) {

                    indexIds.add(set.getInt(1));

                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain index Id  unique for table oid " + oid + " and column attNum " + attNum, e);
            } finally {
                ps.close();
            }
            return indexIds;

        }

        protected String getIndexName(Connection connection, int indexId) throws SQLException {

            String indexName = null;

            String sqlIndexName = "SELECT relname FROM pg_class WHERE oid = ?;";
            PreparedStatement ps = connection.prepareStatement(sqlIndexName);
            ps.setInt(1, indexId);
            try {
                ResultSet set = ps.executeQuery();
                if (set.next()) {
                    indexName = set.getString(1);
                }
            } catch (Exception e) {
                throw new SQLException("Could not obtain index name  for indexId " + indexId, e);
            } finally {
                ps.close();
            }

            return indexName;

        }


    }


}
