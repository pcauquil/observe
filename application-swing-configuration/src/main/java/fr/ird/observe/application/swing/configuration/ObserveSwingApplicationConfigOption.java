/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.configuration;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.services.dto.ObserveSpeciesListConfiguration;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Les options disponibles dans l'application, modifiables via la ligne de
 * commande, ou le fichier utilisateur.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public enum ObserveSwingApplicationConfigOption implements ConfigOptionDef {

    /** le lastName du fichier de configuration (sans le prefix .) */
    CONFIG_FILE(
            ApplicationConfig.CONFIG_FILE_NAME,
            n("observe.config.configFileName.description"),
            ObserveSwingApplicationConfig.DEFAULT_OBSERVE_SWING_CONFIGURATION_FILENAME,
            String.class,
            true,
            true
    ),

    /** flag pour afficher automatiquement la popup du clavier numerique */
    DEV_MODE(
            "observe." + ObserveSwingApplicationConfig.PROPERTY_DEV_MODE,
            n("observe.config.devMode"),
            "true",
            Boolean.class,
            true,
            true
    ),

    /** version du modèle de données */
    MODEL_VERSION(
            "observe.model.version",
            n("observe.model.version"),
            null,
            Version.class,
            true,
            true
    ),

    // directories

    /** le repertoire ou est stoquee la base locale */
    DATA_DIRECTORY(
            "data.directory",
            n("observe.config.defaultDataDirectory.description"),
            "${user.home}/.observe",
            File.class,
            false,
            false
    ),

    /** le repertoire ou est stoquee la base locale */
    DB_DIRECTORY(
            "db.directory",
            n("observe.config.defaultLocalDbDirectory.description"),
            "${data.directory}/db",
            File.class,
            false,
            false
    ),

    /**
     * le lastName de la base initiale recuperee lors de la premiere connexion
     * a Obstuna.
     */
    INITIAL_DB_DUMP(
            "initial.db.dump",
            n("observe.config.defaultInitialDbDump.description"),
            "${data.directory}/initial-database.sql.gz",
            File.class,
            false,
            false
    ),

    /** le repertoire ou effectuer les sauvegarde */
    BACKUP_DIRECTORY(
            "backup.directory",
            n("observe.config.defaultBackupDirectory.description"),
            "${data.directory}/backup",
            File.class,
            false,
            false
    ),

    /** le repertoire ou sont stockees toutes les ressources de l'utilisateur */
    RESOURCES_DIRECTORY(
            "resources.directory",
            ObserveSwingApplicationConfig.PROPERTY_RESOURCES_DIRECTORY,
            n("observe.config.defaultResourcesDirectory.description"),
            "${data.directory}/resources-${version}",
            File.class,
            false,
            false
    ),

    /** le repertoire ou sont stockees les traduction i18n de l'utilisateur */
    I18N_DIRECTORY(
            "i18n.directory",
            ObserveSwingApplicationConfig.PROPERTY_I18N_DIRECTORY,
            n("observe.config.defaultI18nDirectory.description"),
            "${resources.directory}/i18n",
            File.class,
            false,
            false
    ),

    /** le repertoire ou sont stockees les reports de l'utilisateur */
    REPORT_DIRECTORY(
            "report.directory",
            ObserveSwingApplicationConfig.PROPERTY_REPORT_DIRECTORY,
            n("observe.config.defaultReportDirectory.description"),
            "${resources.directory}/report",
            File.class,
            false,
            false
    ),

    /** le repertoire ou sont stockees les cartes de l'utilisateur */
    MAP_DIRECTORY(
            "map.directory",
            ObserveSwingApplicationConfig.PROPERTY_MAP_DIRECTORY,
            n("observe.config.defaultMapDirectory.description"),
            "${resources.directory}/map",
            File.class,
            false,
            false
    ),

    /** le repertoire ou sont stockees les reports de validation de l'utilisateur */
    VALIDATION_REPORT_DIRECTORY(
            "validation.report.directory",
            ObserveSwingApplicationConfig.PROPERTY_VALIDATION_REPORT_DIRECTORY,
            n("observe.config.defaultValidationReportDirectory.description"),
            "${data.directory}/validation-report",
            File.class,
            false,
            false
    ),

    /** le repertoire temporaire (utilise pour les base de synchro) */
    TMP_DIRECTORY(
            "tmp.directory",
            n("observe.config.defaultTmpDirectory.description"),
            "${data.directory}/tmp",
            File.class,
            false,
            false
    ),

    /** Le chemin du fichier de configuration des logs. */
    LOG_CONFIGURATION_FILE(
            "logConfigurationFile",
            n("observe.config.logConfigurationFile.description"),
            "${resources.directory}/observe-log4j.properties",
            String.class,
            true,
            false),

    // local db config

    /** login sur toutes les bases h2 (non sauvegarde) */
    H2_LOGIN(
            "h2.username",
            n("observe.config.h2.login.description"),
            "sa",
            String.class,
            true,
            true
    ),

    /** mot de passe sur toutes les bases h2 (non sauvegarde) */
    H2_PASSWORD(
            "h2.password",
            n("observe.config.h2.password.description"),
            "sa",
            String.class,
            true,
            true
    ),

    /** flag pour effectuer la mise a jour des bases locales */
    H2_CAN_MIGRATE(
            "h2.canMigrate",
            n("observe.config.h2.can.migrate.description"),
            "true",
            Boolean.class,
            false,
            false
    ),


    /** port à utiliser pour lancer ObServe en mode serveur */
    H2_SERVER_PORT(
            "h2.serverPort",
            n("observe.config.h2.serverPort.description"),
            "9093",
            Integer.class,
            false,
            false
    ),

    // obstuna db config

    /** url de la base obstuna */
    OBSTUNA_URL(
            "obstuna.url",
            n("observe.config.obstuna.url.description"),
            "jdbc:postgresql:///obstuna",
            String.class,
            false,
            false
    ),

    /** login de la base obstuna */
    OBSTUNA_LOGIN(
            "obstuna.username",
            n("observe.config.obstuna.login.description"),
            "utilisateur",
            String.class,
            false,
            false
    ),

    /** mot de passe de la base obstuna (non sauvegarde) */
    OBSTUNA_PASSWORD(
            "obstuna.password",
            n("observe.config.obstuna.password.description"),
            "",
            String.class,
            true,
            false
    ),

    /** flag pour activer l'utilisation ssl pour la connexion a obstuna */
    OBSTUNA_USE_SSL_CERT(
            "obstuna.useSsl",
            n("observe.config.obstuna.useSsl.description"),
            "false",
            Boolean.class,
            false,
            false
    ),

    /** flag pour mettre a jour obstuna */
    OBSTUNA_CAN_MIGRATE(
            "pg.canMigrate",
            n("observe.config.pg.can.migrate.description"),
            "false",
            Boolean.class,
            false,
            false
    ),

    // Rest server config

    /** url du serveur restflag pour mettre a jour obstuna */
    SERVER_URL(
            "server.url",
            n("observe.config.server.url.description"),
            null,
            URL.class,
            false,
            false
    ),

    /** url du serveur restflag pour mettre a jour obstuna */
    SERVER_LOGIN(
            "server.login",
            n("observe.config.server.login.description"),
            "",
            String.class,
            false,
            false
    ),

    /** url du serveur restflag pour mettre a jour obstuna */
    SERVER_PASSWORD(
            "server.password",
            n("observe.config.server.password.description"),
            "",
            String.class,
            false,
            false
    ),

    /** url du serveur restflag pour mettre a jour obstuna */
    SERVER_DATABASE_NAME(
            "server.dataBaseName",
            n("observe.config.server.dataBaseName.description"),
            null,
            String.class,
            false,
            false
    ),

    // change storage options

    /** le mode de connexion par defaut */
    DEFAULT_DB_MODE(
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_DB_MODE,
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_DB_MODE,
            n("observe.config.defaultDbMode"),
            DbMode.USE_LOCAL.name(),
            DbMode.class,
            false,
            false
    ),

    /** le mode de creation par defaut de base locale */
    DEFAULT_CREATION_MODE(
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_CREATION_MODE,
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_CREATION_MODE,
            n("observe.config.defaultCreationMode"),
            CreationMode.IMPORT_EXTERNAL_DUMP.name(),
            CreationMode.class,
            false,
            false
    ),

    /** flag pour sauvegarder le paramétrage de la connexion a obstuna */
    STORE_REMOTE_STORAGE(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_STORE_REMOTE_STORAGE,
            ObserveSwingApplicationConfig.PROPERTY_STORE_REMOTE_STORAGE,
            n("observe.config.ui.storeRemoteStorage"),
            "true",
            Boolean.class,
            false,
            false
    ),

    /** flag pour charge ou non la base locale au démarrage */
    LOAD_LOCAL_STORAGE(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_LOAD_LOCAL_STORAGE,
            ObserveSwingApplicationConfig.PROPERTY_LOAD_LOCAL_STORAGE,
            n("observe.config.ui.loadLocalStorage"),
            "true",
            Boolean.class,
            false,
            false
    ),

    /** pour afficher la progression de la migration dans les logs */
    SHOW_MIGRATION_PROGRESSION(
            ObserveSwingApplicationConfig.PROPERTY_SHOW_MIGRATION_PROGRESSION,
            ObserveSwingApplicationConfig.PROPERTY_SHOW_MIGRATION_PROGRESSION,
            n("observe.config.showMigrationProgression"),
            "true",
            Boolean.class,
            false,
            false
    ),

    /** pour affichier les requetes sql lors de la migration dans les logs */
    SHOW_MIGRATION_SQL(
            ObserveSwingApplicationConfig.PROPERTY_SHOW_MIGRATION_SQL,
            ObserveSwingApplicationConfig.PROPERTY_SHOW_MIGRATION_SQL,
            n("observe.config.showMigrationSql"),
            "true",
            Boolean.class,
            false,
            false
    ),
    /** pour affichier toutes les requetes sql dans les logs */
    SHOW_SQL(
            ObserveSwingApplicationConfig.PROPERTY_SHOW_SQL,
            ObserveSwingApplicationConfig.PROPERTY_SHOW_SQL,
            n("observe.config.showSql"),
            "false",
            Boolean.class,
            false,
            false
    ),
    // gps default values

    /** temps maximum autorise en deux points gps */
    DEFAULT_GPS_MAX_DELAY(
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_GPS_MAX_DELAY,
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_GPS_MAX_DELAY,
            n("observe.config.defaultGpsMaxDelay"),
            "60",
            Integer.class,
            false,
            false
    ),

    /** vitesse maximum (en noeud) entre deux points gps */
    DEFAULT_GPS_MAX_SPEED(
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_GPS_MAX_SPEED,
            ObserveSwingApplicationConfig.PROPERTY_DEFAULT_GPS_MAX_SPEED,
            n("observe.config.defaultGpsMaxSpeed"),
            "25.0f",
            Float.class,
            false,
            false
    ),

    // synchro actions options

    /** flag pour autoriser le choix de la base source */
    CHANGE_SYNCHRO_SRC(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_CHANGE_SYNCHRO_SRC,
            ObserveSwingApplicationConfig.PROPERTY_CHANGE_SYNCHRO_SRC,
            n("observe.config.ui.changeSynchroSrc"),
            "false",
            Boolean.class,
            false,
            false
    ),

    // ui config

    /** flag pour voir le boutton d'affichage de la popup de clavier numerique) */
    SHOW_NUMBER_EDITOR_BUTTON(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_SHOW_NUMBER_EDITOR_BUTTON,
            ObserveSwingApplicationConfig.PROPERTY_SHOW_NUMBER_EDITOR_BUTTON,
            n("observe.config.ui.showNumberEditorButton"),
            "true",
            Boolean.class,
            false,
            false
    ),

    /** flag pour afficher automatiquement la popup du clavier numerique */
    AUTO_POPUP_NUMBER_EDITOR(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_AUTO_POPUP_NUMBER_EDITOR,
            ObserveSwingApplicationConfig.PROPERTY_AUTO_POPUP_NUMBER_EDITOR,
            n("observe.config.ui.autoPopupNumberEditor"),
            "false",
            Boolean.class,
            false,
            false
    ),

    /** flag pour voir la réglette d'affichage des heures dans l'éditeur des temps */
    SHOW_DATE_TIME_EDITOR_SLIDER(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_SHOW_DATE_TIME_EDITOR_SLIDER,
            ObserveSwingApplicationConfig.PROPERTY_SHOW_DATE_TIME_EDITOR_SLIDER,
            n("observe.config.ui.showTimeEditorSlider"),
            "true",
            Boolean.class,
            false,
            false
    ),

    /** flag pour lancer l'application en mode plein ecran */
    FULL_SCREEN(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_FULL_SCREEN,
            ObserveSwingApplicationConfig.PROPERTY_FULL_SCREEN,
            n("observe.config.ui.fullscreen"),
            "false",
            Boolean.class,
            false,
            false
    ),

    /** locale a utiliser dans l'application */
    LOCALE(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_LOCALE,
            ObserveSwingApplicationConfig.PROPERTY_LOCALE,
            n("observe.config.ui.locale"),
            Locale.FRANCE.toString(),
            Locale.class,
            false,
            false
    ),

    /** locale du referentiel */
    DB_LOCALE(
            "db." + ObserveSwingApplicationConfig.PROPERTY_LOCALE,
            ObserveSwingApplicationConfig.PROPERTY_DB_LOCALE,
            n("observe.config.db.locale"),
            Locale.FRANCE.toString(),
            Locale.class,
            false,
            false
    ),

    // program observation max

    /** la valeur maximale de qualification des observations faune associe */
    NON_TARGET_OBSERVATION(
            "observation.fauneAssociee",
            ObserveSwingApplicationConfig.PROPERTY_NON_TARGET_OBSERVATION,
            n("observe.config.observation.fauneAssociee"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** la valeur maximale de qualification des observations rejet thons */
    TARGET_DISCARDS_OBSERVATION(
            "observation.rejetsThons",
            ObserveSwingApplicationConfig.PROPERTY_TARGET_DISCARDS_OBSERVATION,
            n("observe.config.observation.rejetsThons"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** la valeur maximale de qualification des observations mensuration */
    SAMPLES_OBSERVATION(
            "observation.mensurations",
            ObserveSwingApplicationConfig.PROPERTY_SAMPLES_OBSERVATION,
            n("observe.config.observation.mensurations"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** la valeur maximale de qualification des observations objet flottant */
    OBJECTS_OBSERVATION(
            "observation.floatingObject",
            ObserveSwingApplicationConfig.PROPERTY_OBJECTS_OBSERVATION,
            n("observe.config.observation.floatingObject"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** la valeur maximale de qualification des observations activitys detaillees */
    DETAILLED_ACTIVITIES_OBSERVATION(
            "observation.activitysDetaillees",
            ObserveSwingApplicationConfig.PROPERTY_DETAILLED_ACTIVITIES_OBSERVATION,
            n("observe.config.observation.activitysDetaillees"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** la valeur maximale de qualification des observations mammiferes */
    MAMMALS_OBSERVATION(
            "observation.mammiferes",
            ObserveSwingApplicationConfig.PROPERTY_MAMMALS_OBSERVATION,
            n("observe.config.observation.mammiferes"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** la valeur maximale de qualification des observations oiseaux */
    BIRDS_OBSERVATION(
            "observation.oiseaux",
            ObserveSwingApplicationConfig.PROPERTY_BIRDS_OBSERVATION,
            n("observe.config.observation.oiseaux"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** la valeur maximale de qualification des observations gleure */
    BAIT_OBSERVATION(
            "observation.gleure",
            ObserveSwingApplicationConfig.PROPERTY_BAIT_OBSERVATION,
            n("observe.config.observation.gleure"),
            "1",
            Integer.class,
            false,
            false
    ),

    /** le type de liste d'espèces pour les captures cibles (seine) */
    SPECIES_LIST_SEINE_TARGET_CATCH_ID(
            "speciesList.seine.targetCatch",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_SEINE_TARGET_CATCH_ID,
            n("observe.config.speciesList.seine.targetCatch"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_SEINE_TARGET_CATCH_ID,
            String.class,
            false,
            false
    ),

    /** le type de liste d'espèce pour les Estimation banc (seine) */
    SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID(
            "speciesList.seine.schoolEstimate",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID,
            n("observe.config.speciesList.seine.schoolEstimate"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID,
            String.class,
            false,
            false
    ),

    /** le type de liste d'espèces pour les Estimation banc objet (seine) */
    SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID(
            "speciesList.seine.objectSchoolEstimate",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID,
            n("observe.config.speciesList.seine.objectSchoolEstimate"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID,
            String.class,
            false,
            false
    ),

    /** le type de liste d'espèces pour les captures non cibles (seine) */
    SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID(
            "speciesList.seine.nonTargetCatch",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID,
            n("observe.config.speciesList.seine.nonTargetCatch"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID,
            String.class,
            false,
            false
    ),

    /** le type de liste d'espèces pour les Faune observée (seine) */
    SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID(
            "speciesList.seine.objectObservedSpecies",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID,
            n("observe.config.speciesList.seine.objectObservedSpecies"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID,
            String.class,
            false,
            false
    ),

    /** le type de liste d'espèces pour les captures (longline) */
    SPECIES_LIST_LONGLINE_CATCH_ID(
            "speciesList.longline.catch",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_LONGLINE_CATCH_ID,
            n("observe.config.speciesList.longline.catch"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_LONGLINE_CATCH_ID,
            String.class,
            false,
            false
    ),

    /** le type de liste d'espèces pour les rencontres (longline) */
    SPECIES_LIST_LONGLINE_ENCOUNTER_ID(
            "speciesList.longline.encounter",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_LONGLINE_ENCOUNTER_ID,
            n("observe.config.speciesList.longline.encounter"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_LONGLINE_ENCOUNTER_ID,
            String.class,
            false,
            false
    ),
    /** le type de liste d'espèces pour les deprédations (longline) */
    SPECIES_LIST_LONGLINE_DEPREDATOR_ID(
            "speciesList.longline.depredator",
            ObserveSwingApplicationConfig.PROPERTY_SPECIES_LIST_LONGLINE_DEPREDATOR_ID,
            n("observe.config.speciesList.longline.encounter"),
            ObserveSpeciesListConfiguration.DEFAULT_SPECIES_LIST_LONGLINE_DEPREDATOR_ID,
            String.class,
            false,
            false
    ),

    /** le chemin vers le fond de carte */
    MAP_BACKGROUND_COLOR(
            "map.background.color",
            n("observe.config.map.background.description"),
            new Color(87, 200, 255).toString(),
            Color.class,
            false,
            false
    ),
    /** shape file 1 */
    MAP_LAYER_1(
            "map.layer1.path",
            n("observe.config.map.layer1.description"),
            "${resources.directory}/map/shapeFiles/continents/GSHHS_l_L1.shp",
            File.class,
            false,
            false
    ),
    /** shape file 2 */
    MAP_LAYER_2(
            "map.layer2.path",
            n("observe.config.map.layer2.description"),
            "${resources.directory}/map/shapeFiles/continents/GSHHS_l_L6.shp",
            File.class,
            false,
            false
    ),
    /** shape file 3 */
    MAP_LAYER_3(
            "map.layer3.path",
            n("observe.config.map.layer3.description"),
            "${resources.directory}/map/shapeFiles/lakesAndSeas/GSHHS_l_L2.shp",
            File.class,
            false,
            false
    ),
    /** shape file 4 */
    MAP_LAYER_4(
            "map.layer4.path",
            n("observe.config.map.layer4.description"),
            "${resources.directory}/map/shapeFiles/borders/WDBII_border_l_L1.shp",
            File.class,
            false,
            false
    ),
    /** shape file 5 */
    MAP_LAYER_5(
            "map.layer5.path",
            n("observe.config.map.layer5.description"),
            "${resources.directory}/map/shapeFiles/zee/World_EEZ_v8_2014.shp",
            File.class,
            false,
            false
    ),
    /** shape file 6 */
    MAP_LAYER_6(
            "map.layer6.path",
            n("observe.config.map.layer6.description"),
            null,
            File.class,
            false,
            false
    ),
    /** shape file 7 */
    MAP_LAYER_7(
            "map.layer7.path",
            n("observe.config.map.layer7.description"),
            null,
            File.class,
            false,
            false
    ),
    /** shape file 8 */
    MAP_LAYER_8(
            "map.layer8.path",
            n("observe.config.map.layer8.description"),
            null,
            File.class,
            false,
            false
    ),
    /** shape file 9 */
    MAP_LAYER_9(
            "map.layer9.path",
            n("observe.config.map.layer9.description"),
            null,
            File.class,
            false,
            false
    ),
    /** shape file 10 */
    MAP_LAYER_10(
            "map.layer10.path",
            n("observe.config.map.layer10.description"),
            null,
            File.class,
            false,
            false
    ),
    /** style Maps */
    MAP_STYLE_FILE(
            "map.style.path",
            n("observe.config.map.style.description"),
            "${resources.directory}/map/style.xml",
            File.class,
            false,
            false),


    /** Ids des noeuds ouverts dans l'arbre */
    TREE_OPEN_NODES(
            "ui." + ObserveSwingApplicationConfig.PROPERTY_TREE_OPEN_NODES,
            ObserveSwingApplicationConfig.PROPERTY_TREE_OPEN_NODES,
            n("observe.config.ui.treeOpenNodes"),
            null,
            String.class,
            false,
            false
    );

    public static final List<ObserveSwingApplicationConfigOption> MAP_LAYERS = ImmutableList.of(MAP_LAYER_1, MAP_LAYER_2, MAP_LAYER_3,
                                                                                                MAP_LAYER_4, MAP_LAYER_5, MAP_LAYER_6, MAP_LAYER_7, MAP_LAYER_8, MAP_LAYER_9, MAP_LAYER_10);

    /**
     * Clef qui represente l'option (c'est celle enregistrée dans le fichier de
     * configuration).
     */
    protected final String key;

    /**
     * Clef de la propriété (javaBeans) de la configuration associé à cette
     * option ({@code null} si pas associé à une propriété javaBeans).
     */
    protected final String propertyKey;

    /** Clef i18n de description de l'option */
    protected final String description;

    /** Type de l'option */
    protected final Class<?> type;

    /** Valeur par défaut de l'option */
    protected String defaultValue;

    /** Drapeau pour savoir si on ne doit pas sauvegarder cette option */
    protected boolean _transient;

    /** Drapeau pour savoir si l'option est modifiable */
    protected boolean _final;

//    /**
//     * Drapeau pour savoir si l'option requiere le mode admin
//     *
//     * @see ObserveRunner#isAdmin()
//     */
//    protected boolean admin;

    ObserveSwingApplicationConfigOption(String key,
                                        String description,
                                        String defaultValue,
                                        Class<?> type,
                                        boolean _transient,
                                        boolean _final) {
        this(key, null, description, defaultValue, type, _transient, _final);
    }

    ObserveSwingApplicationConfigOption(String key,
                                        String propertyKey,
                                        String description,
                                        String defaultValue,
                                        Class<?> type,
                                        boolean _transient,
                                        boolean _final) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this._final = _final;
        this._transient = _transient;
        this.propertyKey = propertyKey;
    }

    @Override
    public boolean isFinal() {
        return _final;
    }

    @Override
    public boolean isTransient() {
        return _transient;
    }

//    public boolean isAdmin() {
//        return admin;
//    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public String getDescription() {
        return t(description);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean _transient) {
        this._transient = _transient;
    }

    @Override
    public void setFinal(boolean _final) {
        this._final = _final;
    }

//    public void setAdmin(boolean admin) {
//        this.admin = admin;
//    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public static ImmutableList<ObserveSwingApplicationConfigOption> orderedByNameValues() {

        List<ObserveSwingApplicationConfigOption> values = Lists.newArrayList(values());
        Collections.sort(values, (o1, o2) -> o1.getKey().compareTo(o2.getKey()));
        return ImmutableList.copyOf(values);

    }
}
