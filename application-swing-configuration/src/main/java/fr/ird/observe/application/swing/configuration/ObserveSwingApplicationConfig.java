/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.configuration;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigHelper;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import javax.swing.JOptionPane;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * La configuration de l'application.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ObserveSwingApplicationConfig extends ApplicationConfig {

    /** le pattern du fichier de sauvegarde d'une base locale */
    public static final String BACKUP_DB_PATTERN = "obstuna-local-%1$tF--%1$tk-%1$tM-%1$tS.sql.gz";

    public static final String DB_NAME = "obstuna";

    /**
     * La version de l'application.
     */
    private static final String APPLICATION_VERSION = "application.version";

    private static final String VERSION = "version";

    static final String PROPERTY_DEFAULT_DB_MODE = "defaultDbMode";

    static final String PROPERTY_DEFAULT_CREATION_MODE = "defaultCreationMode";

    static final String PROPERTY_SPECIES_LIST_SEINE_TARGET_CATCH_ID = "speciesListSeineTargetCatchId";

    static final String PROPERTY_SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID = "speciesListSeineSchoolEstimateId";

    static final String PROPERTY_SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID = "speciesListSeineObjectSchoolEstimateId";

    static final String PROPERTY_SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID = "speciesListSeineNonTargetCatchId";

    static final String PROPERTY_SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID = "speciesListSeineObjectObservedSpeciesId";

    static final String PROPERTY_SPECIES_LIST_LONGLINE_CATCH_ID = "speciesListLonglineCatchId";

    static final String PROPERTY_SPECIES_LIST_LONGLINE_ENCOUNTER_ID = "speciesListLonglineEncounterId";

    static final String PROPERTY_SPECIES_LIST_LONGLINE_DEPREDATOR_ID = "speciesListLonglineDepredatorId";

    static final String PROPERTY_AUTO_POPUP_NUMBER_EDITOR = "autoPopupNumberEditor";

    static final String PROPERTY_SHOW_NUMBER_EDITOR_BUTTON = "showNumberEditorButton";

    static final String PROPERTY_SHOW_DATE_TIME_EDITOR_SLIDER = "showTimeEditorSlider";

    private static final String PROPERTY_LOCAL_STORAGE_EXIST = "localStorageExist";

    private static final String PROPERTY_INITIAL_DUMP_EXIST = "initialDumpExist";

    private static final String PROPERTY_MAIN_STORAGE_OPENED = "mainStorageOpened";

    static final String PROPERTY_STORE_REMOTE_STORAGE = "storeRemoteStorage";

    static final String PROPERTY_DEFAULT_GPS_MAX_DELAY = "defaultGpsMaxDelay";

    static final String PROPERTY_DEFAULT_GPS_MAX_SPEED = "defaultGpsMaxSpeed";

    static final String PROPERTY_CHANGE_SYNCHRO_SRC = "changeSynchroSrc";

    static final String PROPERTY_NON_TARGET_OBSERVATION = "nonTargetObservation";

    static final String PROPERTY_TARGET_DISCARDS_OBSERVATION = "targetDiscardsObservation";

    static final String PROPERTY_SAMPLES_OBSERVATION = "samplesObservation";

    static final String PROPERTY_OBJECTS_OBSERVATION = "objectsObservation";

    static final String PROPERTY_DETAILLED_ACTIVITIES_OBSERVATION = "detailledActivitiesObservation";

    static final String PROPERTY_MAMMALS_OBSERVATION = "mammalsObservation";

    static final String PROPERTY_BIRDS_OBSERVATION = "birdsObservation";

    static final String PROPERTY_BAIT_OBSERVATION = "baitObservation";

    static final String PROPERTY_RESOURCES_DIRECTORY = "resourcesDirectory";

    static final String PROPERTY_LOCALE = "locale";

    static final String PROPERTY_DB_LOCALE = "dbLocale";

    static final String PROPERTY_FULL_SCREEN = "fullScreen";

    static final String PROPERTY_I18N_DIRECTORY = "i18nDirectory";

    static final String PROPERTY_REPORT_DIRECTORY = "reportDirectory";

    static final String PROPERTY_MAP_DIRECTORY = "mapDirectory";

    static final String PROPERTY_LOAD_LOCAL_STORAGE = "loadLocalStorage";

    static final String PROPERTY_SHOW_MIGRATION_PROGRESSION = "showMigrationProgression";

    static final String PROPERTY_SHOW_MIGRATION_SQL = "showMigrationSql";

    static final String PROPERTY_VALIDATION_REPORT_DIRECTORY = "validationReportDirectory";

    static final String PROPERTY_SHOW_SQL = "showSql";

    static final String PROPERTY_DEV_MODE = "devMode";

    static final String PROPERTY_TREE_OPEN_NODES = "treeOpenNodes";

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveSwingApplicationConfig.class);

    static final String DEFAULT_OBSERVE_SWING_CONFIGURATION_FILENAME = "observeSwing.conf";

    /**
     * un drepeau pour savoir s'il faut lancer l'interface graphique. Cette
     * valeur peut être programmées lors des actions.
     */
    private boolean displayMainUI = true;

    /** drapeau pour savoir si une base locale existe */
    private boolean localStorageExist;

    /**
     * drapeau pour savoir si le dump initial a ete chargee depuis la base
     * centrale
     */
    private boolean initialDumpExist;

    /** drapeau pour savoir si une source de données est ouverte */
    private boolean mainStorageOpened;

    /**
     * drapeau pour savoir si la source ouverte est locale (attention, lorsqu'il
     * n'y a pas de storage d'ouvert, la valeur est null).
     */
    private Boolean mainStorageOpenedLocal;

    /**
     * drapeau pour savoir si on peut utiliser des ui dans l'environnement.
     *
     * Par defaut, on suppose qu'on peut utiliser l'environnement graphique et
     * si on désactive explicitement ou si pas d'environnement graphique
     * trouvé.
     */
    private boolean canUseUI = true;

    /** La version de l'application */
    private Version version;

    /** Texte du copyright (calculé dynamiquement). */
    private String copyrightText;

    /**
     * Liste des options qu'on ne peut pas sauvegarder (les mots de passes,
     * les options d'admin pour les simples utilisateurs,...).
     *
     * @since 1.5
     */
    private String[] unsavables;

    //FIXME
    private final ImmutableSet<String> longlinVesselTypeIds = ImmutableSet.copyOf(
            Sets.newHashSet("fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.044156847891821505",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675736#0.8708229847859869",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832686137#0.1"));

    private final ImmutableSet<String> seineVesselTypeIds = ImmutableSet.copyOf(
            Sets.newHashSet("fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.044156847891821505",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.307197212385357",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.7380146830307519",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.9086075071905084",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675737#0.43324169605639407"));

    public ObserveSwingApplicationConfig() {
        this(DEFAULT_OBSERVE_SWING_CONFIGURATION_FILENAME);
    }

    public ObserveSwingApplicationConfig(String confFileName) {
        setEncoding(Charsets.UTF_8.name());
        setConfigFileName(confFileName);
        ApplicationConfigProvider applicationConfigProvider = ApplicationConfigHelper.getProvider(getClass().getClassLoader(), ObserveSwingApplicationConfigProvider.OBSERVE_SWING_CONFIGURATION_PROVIDER_NAME);
        loadDefaultOptions(applicationConfigProvider.getOptions());
    }

    public boolean containActions(Step action) {
        List<Action> list = actions.get(action.ordinal());
        return !(list == null || list.isEmpty());
    }

    public void initConfig(Properties p, ConfigActionDef... actions) throws IOException {

        Version version = null;

        for (Object k : p.keySet()) {
            String key = String.valueOf(k);
            Object value = p.get(k);
            if (log.isDebugEnabled()) {
                log.debug("install property [" + k + "] : " + value);
            }
            String strValue = String.valueOf(value);
            if (APPLICATION_VERSION.equals(key)) {
                version = Versions.valueOf(strValue);
                if (version.isSnapshot()) {
                    version = Versions.removeSnapshot(version);
                }

            } else {
                setDefaultOption(key, strValue);
            }
        }

        if (version == null) {
            throw new IllegalStateException("No application.version found in application configuration.");
        }

        setVersion(version);
        setDefaultOption(VERSION, version.getVersion());
        setDefaultOption(APPLICATION_VERSION, version.getVersion());

        // creation des actions disponibles
        for (ConfigActionDef a : actions) {

            for (String alias : a.getAliases()) {
                addActionAlias(alias, a.getAction());
            }
        }

    }

    @Override
    protected void migrateUserConfigurationFile(File oldHomeConfig,
                                                File homeConfig) throws IOException {
        super.migrateUserConfigurationFile(oldHomeConfig, homeConfig);
        // on previent l'utilisateur que son fichier de configuration a été
        // deplacé
        JOptionPane.showInternalMessageDialog(null,
                                              t("observe.title.config.migrate"),
                                              t("observe.runner.config.migrate.file",
                                                oldHomeConfig.getName(),
                                                oldHomeConfig,
                                                homeConfig
                                              ),
                                              JOptionPane.WARNING_MESSAGE);
    }

    public void installSaveAction() {
        // ajout de l'action de sauvegarde automatique sur certaines propriétés

        List<String> result = new ArrayList<>();

        for (ObserveSwingApplicationConfigOption option : ObserveSwingApplicationConfigOption.values()) {
            String key = option.getPropertyKey();
            if (key != null) {
                // add a listener
                if (log.isDebugEnabled()) {
                    log.debug("register action listener for property " + key);
                }
                result.add(key);
            }
        }

        installSaveUserAction(result.toArray(new String[result.size()]));
    }

    public String getCopyrightText() {
        if (copyrightText == null) {
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int year = calendar.get(Calendar.YEAR);
            copyrightText = "Version " + getVersion() + " IRD @ 2008-" + year;
        }
        return copyrightText;
    }

    public boolean isDevMode() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.DEV_MODE.key);
    }

    public Version getVersion() {
        return version;
    }

    public Version getModelVersion() {
        return getOption(Version.class, ObserveSwingApplicationConfigOption.MODEL_VERSION.key);
    }

    public boolean isAutoPopupNumberEditor() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.AUTO_POPUP_NUMBER_EDITOR.key);
    }

    public boolean isShowNumberEditorButton() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.SHOW_NUMBER_EDITOR_BUTTON.key);
    }

    public boolean isShowTimeEditorSlider() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.SHOW_DATE_TIME_EDITOR_SLIDER.key);
    }

    public boolean isFullScreen() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.FULL_SCREEN.key);
    }

    public File getDataDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.DATA_DIRECTORY.key);
    }

    public File getValidationReportDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.VALIDATION_REPORT_DIRECTORY.key);
    }

    public File getLocalDBDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.DB_DIRECTORY.key);
    }

    public File getResourcesDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.RESOURCES_DIRECTORY.key);
    }

    public File getI18nDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.I18N_DIRECTORY.key);
    }

    public File getInitialDbDump() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.INITIAL_DB_DUMP.key);
    }

    public File getBackupDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.BACKUP_DIRECTORY.key);
    }

    public File getReportDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.REPORT_DIRECTORY.key);
    }

    public File getTmpDirectory() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.TMP_DIRECTORY.key);
    }

    public File getLogConfigurationFile() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.LOG_CONFIGURATION_FILE.key);
    }

    public String getH2Login() {
        return getOption(ObserveSwingApplicationConfigOption.H2_LOGIN.key);
    }

    public char[] getH2Password() {
        String result = getOption(ObserveSwingApplicationConfigOption.H2_PASSWORD.key);
        return result.toCharArray();
    }

    public int getH2ServerPort() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.H2_SERVER_PORT.key);
    }

    public String getObstunaUrl() {
        return getOption(ObserveSwingApplicationConfigOption.OBSTUNA_URL.key);
    }

    public void setObstunaUrl(String jdbcUrl) {
        setOption(ObserveSwingApplicationConfigOption.OBSTUNA_URL.key, jdbcUrl);
    }

    public String getObstunaLogin() {
        return getOption(ObserveSwingApplicationConfigOption.OBSTUNA_LOGIN.key);
    }

    public void setObstunaLogin(String login) {
        setOption(ObserveSwingApplicationConfigOption.OBSTUNA_LOGIN.key, login);
    }

    public char[] getObstunaPassword() {
        String result = getOption(ObserveSwingApplicationConfigOption.OBSTUNA_PASSWORD.key);
        return result.toCharArray();
    }

    public void setObstunaPassword(char[] password) {
        setOption(ObserveSwingApplicationConfigOption.OBSTUNA_PASSWORD.key, new String(password));
    }

    public boolean isObstunaUseSsl() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.OBSTUNA_USE_SSL_CERT.key);
    }

    public void setObstunaUseSsl(boolean useSsl) {
        setOption(ObserveSwingApplicationConfigOption.OBSTUNA_USE_SSL_CERT, useSsl);
    }

    public URL getServerUrl() {
        return (URL) getOption(ObserveSwingApplicationConfigOption.SERVER_URL);
    }

    public void setServerUrl(URL serverUrl) {
        setOption(ObserveSwingApplicationConfigOption.SERVER_URL, serverUrl);
    }

    public String getServerLogin() {
        return getOption(ObserveSwingApplicationConfigOption.SERVER_LOGIN.key);
    }

    public void setServerLogin(String serverLoginl) {
        setOption(ObserveSwingApplicationConfigOption.SERVER_LOGIN.key, serverLoginl);
    }

    public char[] getServerPassword() {
        String result = getOption(ObserveSwingApplicationConfigOption.SERVER_PASSWORD.key);
        return result.toCharArray();
    }

    public void setServerPassword(char[] password) {
        setOption(ObserveSwingApplicationConfigOption.SERVER_PASSWORD.key, new String(password));
    }

    public String getServerDataBaseName() {
        return getOption(ObserveSwingApplicationConfigOption.SERVER_DATABASE_NAME.key);
    }

    public void setServerDataBaseName(String dataBaseName) {
        setOption(ObserveSwingApplicationConfigOption.SERVER_DATABASE_NAME.key, dataBaseName);
    }

    public DbMode getDefaultDbMode() {
        return getOption(DbMode.class, ObserveSwingApplicationConfigOption.DEFAULT_DB_MODE.key);
    }

    public CreationMode getDefaultCreationMode() {
        return getOption(CreationMode.class, ObserveSwingApplicationConfigOption.DEFAULT_CREATION_MODE.key);
    }

    public boolean isShowMigrationSql() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_SQL.key);
    }

    public boolean isShowMigrationProgression() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_PROGRESSION.key);
    }

    public boolean isShowSql() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.SHOW_SQL.key);
    }

    public int getDefaultGpsMaxDelay() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.DEFAULT_GPS_MAX_DELAY.key);
    }

    public float getDefaultGpsMaxSpeed() {
        double i = getOptionAsDouble(ObserveSwingApplicationConfigOption.DEFAULT_GPS_MAX_SPEED.key);
        return (float) i;
    }

    public boolean isChangeSynchroSrc() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.CHANGE_SYNCHRO_SRC.key);
    }

    public boolean isStoreRemoteStorage() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.STORE_REMOTE_STORAGE.key);
    }

    public boolean isCanMigrateObstuna() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.OBSTUNA_CAN_MIGRATE.key);
    }

    public boolean isCanMigrateH2() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.H2_CAN_MIGRATE.key);
    }

    public boolean isLoadLocalStorage() {
        return getOptionAsBoolean(ObserveSwingApplicationConfigOption.LOAD_LOCAL_STORAGE.key);
    }

    public Locale getLocale() {
        return getOption(Locale.class, ObserveSwingApplicationConfigOption.LOCALE.key);
    }

    public Locale getDbLocale() {
        return getOption(Locale.class, ObserveSwingApplicationConfigOption.DB_LOCALE.key);
    }

    public int getNonTargetObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.NON_TARGET_OBSERVATION.key);
    }

    public int getTargetDiscardsObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.TARGET_DISCARDS_OBSERVATION.key);
    }

    public int getSamplesObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.SAMPLES_OBSERVATION.key);
    }

    public int getObjectsObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.OBJECTS_OBSERVATION.key);
    }

    public int getDetailledActivitiesObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.DETAILLED_ACTIVITIES_OBSERVATION.key);
    }

    public int getMammalsObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.MAMMALS_OBSERVATION.key);
    }

    public int getBirdsObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.BIRDS_OBSERVATION.key);
    }

    public int getBaitObservation() {
        return getOptionAsInt(ObserveSwingApplicationConfigOption.BAIT_OBSERVATION.key);
    }

    public boolean isDisplayMainUI() {
        return displayMainUI;
    }

    public boolean isCanUseUI() {
        return canUseUI;
    }

    public boolean isLocalStorageExist() {
        return localStorageExist;
    }

    public boolean isInitialDumpExist() {
        return initialDumpExist;
    }

    public boolean isMainStorageOpened() {
        return mainStorageOpened;
    }

    public Boolean getMainStorageOpenedLocal() {
        return mainStorageOpenedLocal;
    }

    public void setLocalStorageExist(boolean newValue) {
        localStorageExist = newValue;
        // always force propagation
        firePropertyChange(PROPERTY_LOCAL_STORAGE_EXIST, newValue);
    }

    public void setInitialDumpExist(boolean newValue) {
        initialDumpExist = newValue;
        // always force propagation
        firePropertyChange(PROPERTY_INITIAL_DUMP_EXIST, newValue);
    }

    public void setMainStorageOpened(boolean newValue) {
        mainStorageOpened = newValue;
        if (!newValue) {
            // on force la reinitialisation
            setMainStorageOpenedLocal(null);
        }
        // always force propagation
        firePropertyChange(PROPERTY_MAIN_STORAGE_OPENED, newValue);
    }

    public void setMainStorageOpenedLocal(Boolean newValue) {
        mainStorageOpenedLocal = newValue;
        // always force propagation
        firePropertyChange(PROPERTY_MAIN_STORAGE_OPENED + "Local", newValue);
    }

    public void setDisplayMainUI(boolean b) {
        displayMainUI = b;
    }

    public void setCanUseUI(boolean canUseUI) {
        this.canUseUI = canUseUI;
        if (!canUseUI) {
            // on ne pourra pas lancer l'ui principale
            setDisplayMainUI(false);
        }
    }

    public void setAutoPopupNumberEditor(boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.AUTO_POPUP_NUMBER_EDITOR, newValue);
    }

    public void setShowNumberEditorButton(boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.SHOW_NUMBER_EDITOR_BUTTON, newValue);
    }

    public void setShowTimeEditorSlider(boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.SHOW_DATE_TIME_EDITOR_SLIDER, newValue);
    }

    public void setFullScreen(boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.FULL_SCREEN, newValue);
    }

    public void setChangeSynchroSrc(boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.CHANGE_SYNCHRO_SRC, newValue);
    }

    public void setStoreRemoteStorage(boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.STORE_REMOTE_STORAGE, newValue);
    }

    public void setLoadLocalStorage(boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.LOAD_LOCAL_STORAGE, newValue);
    }

    public void setDefaultDbMode(DbMode newValue) {
        setOption(ObserveSwingApplicationConfigOption.DEFAULT_DB_MODE, newValue);
    }

    public void setDefaultCreationMode(CreationMode newValue) {
        setOption(ObserveSwingApplicationConfigOption.DEFAULT_CREATION_MODE, newValue);
    }

    public void setDefaultGpsMaxDelay(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.DEFAULT_GPS_MAX_DELAY, newValue);
    }

    public void setDefaultGpsMaxSpeed(float newValue) {
        setOption(ObserveSwingApplicationConfigOption.DEFAULT_GPS_MAX_SPEED, newValue);
    }

    public void setLocale(Locale newValue) {
        setOption(ObserveSwingApplicationConfigOption.LOCALE, newValue);
    }

    public void setDbLocale(Locale newValue) {
        setOption(ObserveSwingApplicationConfigOption.DB_LOCALE, newValue);
    }

    public void setCanMigrateObstuna(Boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.OBSTUNA_CAN_MIGRATE, newValue);
    }

    public void setCanMigrateH2(Boolean newValue) {
        setOption(ObserveSwingApplicationConfigOption.H2_CAN_MIGRATE, newValue);
    }

    public void setNonTargetObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.NON_TARGET_OBSERVATION, newValue);
    }

    public void setTargetDiscardsObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.TARGET_DISCARDS_OBSERVATION, newValue);
    }

    public void setSamplesObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.SAMPLES_OBSERVATION, newValue);
    }

    public void setObjectsObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.OBJECTS_OBSERVATION, newValue);
    }

    public void setDetailledActivitiesObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.DETAILLED_ACTIVITIES_OBSERVATION, newValue);
    }

    public void setMammalsObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.MAMMALS_OBSERVATION, newValue);
    }

    public void setBirdsObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.BIRDS_OBSERVATION, newValue);
    }

    public void setBaitObservation(int newValue) {
        setOption(ObserveSwingApplicationConfigOption.BAIT_OBSERVATION, newValue);
    }

    public void setShowMigrationSql(boolean showMigrationSql) {
        setOption(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_SQL, showMigrationSql);
    }

    public void setShowMigrationProgression(boolean showMigrationProgression) {
        setOption(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_PROGRESSION, showMigrationProgression);
    }

    public void setShowSql(boolean showSql) {
        setOption(ObserveSwingApplicationConfigOption.SHOW_SQL, showSql);
    }

    public String getSpeciesListSeineNonTargetCatchId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID.key);
    }

    public String getSpeciesListSeineTargetCatchId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_TARGET_CATCH_ID.key);
    }

    public String getSpeciesListSeineSchoolEstimateId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID.key);
    }

    public String getSpeciesListSeineObjectObservedSpeciesId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID.key);
    }

    public String getSpeciesListSeineObjectSchoolEstimateId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID.key);
    }

    public String getSpeciesListLonglineCatchId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_CATCH_ID.key);
    }

    public String getSpeciesListLonglineEncounterId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_ENCOUNTER_ID.key);
    }

    public String getSpeciesListLonglineDepredatorId() {
        return getOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_DEPREDATOR_ID.key);
    }

    public void setSpeciesListSeineNonTargetCatchId(String speciesListSeineNonTargetCatchId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID, speciesListSeineNonTargetCatchId);
    }

    public void setSpeciesListSeineTargetCatchId(String speciesListSeineTargetCatchId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_TARGET_CATCH_ID, speciesListSeineTargetCatchId);
    }

    public void setSpeciesListSeineSchoolEstimateId(String speciesListSeineSchoolEstimateId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID, speciesListSeineSchoolEstimateId);
    }

    public void setSpeciesListSeineObjectObservedSpeciesId(String speciesListSeineObjectObservedSpeciesId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID, speciesListSeineObjectObservedSpeciesId);
    }

    public void setSpeciesListSeineObjectSchoolEstimateId(String speciesListseineObjectSchoolEstimateId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID, speciesListseineObjectSchoolEstimateId);
    }

    public void setSpeciesListLonglineCatchId(String speciesListLonglineCatchId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_CATCH_ID, speciesListLonglineCatchId);
    }

    public void setSpeciesListLonglineEncounterId(String speciesListLonglineEncounterId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_ENCOUNTER_ID, speciesListLonglineEncounterId);
    }

    public void setSpeciesListLonglineDepredatorId(String speciesListLonglineDepredatorId) {
        setOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_DEPREDATOR_ID, speciesListLonglineDepredatorId);
    }

    public Color getMapBackgroundColor() {
        return getOptionAsColor(ObserveSwingApplicationConfigOption.MAP_BACKGROUND_COLOR.key);
    }

    public List<File> getMapLayerFiles() {
        List<File> layers = Lists.newLinkedList();

        for (ObserveSwingApplicationConfigOption layerOption : ObserveSwingApplicationConfigOption.MAP_LAYERS) {
            File layerFile = getOptionAsFile(layerOption.key);
            if (layerFile != null && layerFile.exists()) {
                layers.add(layerFile);
            }
        }
        return layers;
    }

    public File getMapStyleFile() {
        return getOptionAsFile(ObserveSwingApplicationConfigOption.MAP_STYLE_FILE.key);
    }

    public String[] getTreeOpenNodeIds() {
        String ids = getOption(ObserveSwingApplicationConfigOption.TREE_OPEN_NODES.key);
        String[] result = null;
        if (ids != null) {
            result = ids.split(",");
        }
        return result;
    }

    public void setTreeOpenNodeIds(String[] ids) {
        String joinIds = StringUtils.join(ids, ",");
        setOption(ObserveSwingApplicationConfigOption.TREE_OPEN_NODES, joinIds);
    }

    private String[] getUnsavables() {
        if (unsavables == null) {
            List<String> tmp = new ArrayList<>();
            tmp.add(ObserveSwingApplicationConfigOption.OBSTUNA_PASSWORD.getKey());
//            tmp.add(OBSTUNA_SSL_CERTIFICAT_PASSWORD.getKey());
            tmp.add(ObserveSwingApplicationConfigOption.H2_PASSWORD.getKey());
            tmp.add(ObserveSwingApplicationConfigOption.OBSTUNA_CAN_MIGRATE.getKey());

//            if (!ObserveRunner.isAdmin()) {
//                // toutes les options dite admin ne sont pas sauvables
//
//                for (ObserveSwingApplicationConfigOption option : values()) {
//                    if (option.isAdmin()) {
//
//                        // ne pas ajouter les options d'admin pour le simple utilisateur
//                        tmp.add(option.key);
//                    }
//                }
//            }
            unsavables = tmp.toArray(new String[tmp.size()]);
        }
        return unsavables;
    }

    public void saveForUser(String... excludeKeys) {
        if (log.isInfoEnabled()) {
            log.info(t("observe.message.save.configuration", getUserConfigFile()));
        }
        super.saveForUser(getUnsavables());
    }

    public void removeJaxxPropertyChangeListener() {
        List<String> tmp = new ArrayList<>();
        for (ObserveSwingApplicationConfigOption option : ObserveSwingApplicationConfigOption.values()) {
            String propertyName = option.getPropertyKey();
            if (propertyName != null) {
                tmp.add(propertyName);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("property names to seek for options : " + tmp);
        }
        String[] propertyNames = tmp.toArray(
                new String[tmp.size()]);

        PropertyChangeListener[] toRemove =
                SwingUtil.findJaxxPropertyChangeListener(
                        propertyNames,
                        getPropertyChangeListeners());
        if (toRemove == null || toRemove.length == 0) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("before remove : " + getPropertyChangeListeners().length);
            log.debug("toRemove : " + toRemove.length);
        }
        for (PropertyChangeListener listener : toRemove) {
            removePropertyChangeListener(listener);
        }
        if (log.isDebugEnabled()) {
            log.debug("after remove : " + getPropertyChangeListeners().length);
        }
    }

    public void setOption(ObserveSwingApplicationConfigOption option, Object newValue) {
        String key = option.getKey();
        Object oldValue = getOption(key);
        String value = String.valueOf(newValue);
        setOption(key, value);
        if (log.isDebugEnabled()) {
            log.debug("set option " + key + " value : " + value);
        }
        String propertyName = option.getPropertyKey();
        if (propertyName != null) {
            // l'option est javabeans, declanchement d'un changement
            if (log.isTraceEnabled()) {
                log.trace("fires config change from option " + key);
            }
            firePropertyChange(propertyName, oldValue, newValue);
        }
    }

    private void setVersion(Version version) {
        this.version = version;
    }

    public Set<String> getSeineVesselTypeIds() {
        return seineVesselTypeIds;
    }

    public Set<String> getLonglineVesselTypeIds() {
        return longlinVesselTypeIds;
    }

    public String replaceRecursiveOptions(String option) {
        return super.replaceRecursiveOptions(option);
    }

    public String getConfigurationDescription() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n=====================================================================================================================");
        builder.append("\n=== Observe Swing configuration =====================================================================================");
        builder.append(String.format("\n=== %1$-40s = %2$s", "Filename", getConfigFileName()));
        for (ObserveSwingApplicationConfigOption option : ObserveSwingApplicationConfigOption.orderedByNameValues()) {
            builder.append(String.format("\n=== %1$-40s = %2$s", option.getKey(), getOption(option)));
        }
        builder.append("\n=====================================================================================================================");
        return builder.toString();
    }

    //////////////////////////////////////////////////
    // Toutes les étapes d'actions
    //////////////////////////////////////////////////

    public enum Step {

        AfterInit, BeforeExit
    }

    private void firePropertyChange(String propertyName, Object newValue) {
        pcs.firePropertyChange(propertyName, null, newValue);
    }
}
