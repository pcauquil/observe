package fr.ird.observe.application.swing.configuration;

/*-
 * #%L
 * ObServe :: Application Swing Configuration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ConfigActionDef;

import static org.nuiton.i18n.I18n.t;

/**
 * Les actions appellables en ligne de commande.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public enum ObserveSwingApplicationActionDefinition implements ConfigActionDef {
    /**
     * Afficher l'aide dans la console.
     *
     * @since 1.0
     */
    HELP(false, t("observe.action.commandline.help"),
         "fr.ird.observe.application.swing.ObserveCLAction#help", "-h", "--help"),
    /**
     * Afficher l'aide embarqué de l'application (dans une interface
     * graphique).
     *
     * @since 1.0
     */
    HELP_UI(false, t("observe.action.commandline.help.ui"),
            "fr.ird.observe.application.swing.ObserveCLAction#helpUI", "--help-ui"),
    /**
     * Pour désactiver le lancement de l'application graphique.
     *
     * @since 1.0
     */
    NO_MAIN_UI(false, t("observe.action.commandline.disable.main.ui"),
               "fr.ird.observe.application.swing.ObserveCLAction#disableMainUI", "-n",
               "--no-main"),
    /**
     * Pour lancer l'interface graphique du configuration de l'application.
     *
     * @since 1.0
     */
    CONFIGURE_UI(false, t("observe.action.commandline.configure.ui"),
                 "fr.ird.observe.application.swing.ObserveCLAction#configure",
                 "--configure"),
    /**
     * Pour lancer une opération d'administration via un assistant
     * graphique.
     *
     * @since 1.4
     */
    ADMIN_UI(true, t("observe.action.commandline.launch.admin.ui"),
             "fr.ird.observe.application.swing.ObserveCLAction#launchAdminUI", "-a",
             "--admin"),
    /**
     * Pour lancer une opération d'administration via un assistant
     * graphique.
     *
     * @since 1.4
     */
    OBSTUNA_ADMIN_UI(true, t("observe.action.commandline.launch.obstuna.admin.ui"),
                     "fr.ird.observe.application.swing.ObserveCLAction#launchObstunaAdminUI",
                     "--obstuna-admin"),

    /**
     * Pour lancer la base locale en mode serveur.
     *
     * @since 2.1
     */
    H2_SERVER_MODE(true, t("observe.action.commandline.launch.h2.server.mode"),
                   "fr.ird.observe.application.swing.ObserveCLAction#launchH2ServerMode",
                   "--h2-server"),
    /**
     * Pour activer le support JMX pour les source de données.
     *
     * @since 1.4
     */
    USE_JMX(true, t("observe.action.commandline.use.jmx"),
            "fr.ird.observe.application.swing.ObserveCLAction#useJMX",
            "--jmx"),

    CREATE_ID(true,
              t("observe.action.commandline.create.id"),
              "fr.ird.observe.application.swing.ObserveCLAction#createId",
              "--create-id");


    public final String description;

    public final String action;

    public final String[] aliases;

    public final boolean admin;

    ObserveSwingApplicationActionDefinition(boolean admin, String description, String action, String... aliases) {
        this.description = description;
        this.action = action;
        this.aliases = aliases;
        this.admin = admin;
    }

    public String getDescription() {
        return description;
    }

    public String getAction() {
        return action;
    }

    public String[] getAliases() {
        return aliases;
    }

    public boolean isAdmin() {
        return admin;
    }
}
