/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.configuration.constants;

/**
 * Pour caractériser le type de connexion requis.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public enum DbMode {
    /** Pour utiliser une base locale */
    USE_LOCAL,
    /** Pour creer une base locale */
    CREATE_LOCAL,
    /** Pour utiliser une base distante */
    USE_REMOTE,
    /** Pour utiliser une base via un serveur web distant */
    USE_SERVER
}
