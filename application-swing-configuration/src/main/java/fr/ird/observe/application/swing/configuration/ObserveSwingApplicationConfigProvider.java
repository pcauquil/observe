package fr.ird.observe.application.swing.configuration;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.config.ConfigOptionDef;

import java.util.Locale;

import static org.nuiton.i18n.I18n.l;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveSwingApplicationConfigProvider implements ApplicationConfigProvider {

    static final String OBSERVE_SWING_CONFIGURATION_PROVIDER_NAME = "observeSwing";

    @Override
    public String getName() {
        return OBSERVE_SWING_CONFIGURATION_PROVIDER_NAME;
    }

    @Override
    public String getDescription(Locale locale) {
        return l(locale, "observe.config.name");
    }

    @Override
    public ConfigOptionDef[] getOptions() {
        return ObserveSwingApplicationConfigOption.values();
    }

    @Override
    public ConfigActionDef[] getActions() {
        return ObserveSwingApplicationActionDefinition.values();
    }
}
