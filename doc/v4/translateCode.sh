#! /bin/bash

debug () {
  if [ "$DEBUG" = "true" ]; then
    echo "$currentRow/$nbRows  $1"
  fi
}

info() {
  echo "$currentRow/$nbRows  $1"
}

replaceInFile() {
  local file=$1
  local oldValue=$2
  local newValue=$3

  grep ${oldValue} ${file} > /dev/null
  if [ $? -eq 0 ]; then
    info "Replace $oldValue to $newValue in $file"
    sed -i 's/'"$oldValue"'/'"$newValue"'/' ${file}
  fi
}

renameClass() {
  local oldClass=$1
  local newClass=$2
  local oldClassC=$3
  local newClassC=$4
  debug ""
  debug "Rename class $oldClass to $newClass"
  debug "Rename class constant $oldClassC to $newClassC"

  for file in $(cat ${files}); do
    replaceInFile ${file} ${oldClass} ${newClass}
    replaceInFile ${file} ${oldClassC} ${newClassC}
  done
}

renameAttribute() {
  local oldName=$1
  local newName=$2
  local oldNameC=$3
  local newNameC=$4

  oldFirstLetter=${oldName:0:1}
  oldFirstLetterCap=${oldFirstLetter^^}
  oldGetter="get${oldFirstLetterCap}${oldName:1}"
  oldGetter2="is${oldFirstLetterCap}${oldName:1}"
  oldSetter="set${oldFirstLetterCap}${oldName:1}"
  newFirstLetter=${newName:0:1}
  newFirstLetterCap=${newFirstLetter^^}
  newGetter="get${newFirstLetterCap}${newName:1}"
  newGetter2="is${newFirstLetterCap}${newName:1}"
  newSetter="set${newFirstLetterCap}${newName:1}"

  debug "Rename attribut $oldName to $newName"
  debug "Rename attribut constant $oldNameC to $newNameC"
  debug "Getter: $oldGetter to $newGetter"
  debug "Getter: $oldGetter2 to $newGetter2"
  debug "Setter: $oldSetter to $newSetter"

  for file in $(cat ${files}); do
    replaceInFile ${file} ${oldName} ${newName}
    replaceInFile ${file} ${oldNameC} ${newNameC}
    replaceInFile ${file} ${oldGetter} ${newGetter}
    replaceInFile ${file} ${oldGetter2} ${newGetter2}
    replaceInFile ${file} ${oldSetter} ${newSetter}
  done
}

basedir=$1
classFile=$2
attributFile=$3

DEBUG=false

if [ $# -eq 4 -a "$4" = "debug" ]; then
  DEBUG=true
fi

files=/tmp/observe_translateCodeFiles
rm -rf ${files}
find ${basedir} -name "*.java" | grep -v "db/migration" > ${files}
find ${basedir} -name "*.jaxx" >> ${files}
find ${basedir} -name "*.css" >> ${files}
find ${basedir} -name "*-validation.xml" >> ${files}
find ${basedir} -name "*.properties" >> ${files}

(cd ${basedir} ; svn revert -R .)

nbClasses=$(cat ${classFile} | wc -l)
nbAttributes=$(cat ${attributFile} | wc -l)
nbRows=$(($nbClasses + $nbAttributes))
currentRow=0

while read line
do
  currentRow=$(($currentRow +1))

  if [ "${line:0:1}" == "#" ] ; then
     debug "skip comment $line"
     continue
  fi

  oldName=$(echo ${line} | cut -d';' -f1)
  newName=$(echo ${line} | cut -d';' -f2)
  oldNameC=$(echo ${line} | cut -d';' -f3)
  newNameC=$(echo ${line} | cut -d';' -f4)

  renameAttribute ${oldName} ${newName} ${oldNameC} ${newNameC}

done < ${attributFile}

while read line
do
  currentRow=$(($currentRow +1))

  if [ "${line:0:1}" == "#" ] ; then
     debug "skip comment $line"
     continue
  fi

  oldName=$(echo ${line} | cut -d';' -f1)
  newName=$(echo ${line} | cut -d';' -f2)
  oldNameC=$(echo ${line} | cut -d';' -f3)
  newNameC=$(echo ${line} | cut -d';' -f4)

  renameClass ${oldName} ${newName} ${oldNameC} ${newNameC}

done < ${classFile}

# pour le module business
#reset; ./doc/v4/translateCode.sh /home/tchemit/projets/codelutin.com/observe2/observe-business doc/v4/traduction_tables.csv doc/v4/traduction_attributes.csv

