========================================
Réponse aux tests de mars 2009 (proto 4)
========================================

:Author:   Tony Chemit <chemit@codelutin.com>
:Version:  1.0
:Status:   en cours
:Date:     15 avril 2009
:Abstract: Réponse à la synthèse des tests de mars 2009 sur ObServe.

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2

Général
-------

- les logos on été intégrés.
- ajout d'un message lors du premier démarrage si la base est lockée.
- positionnement toujours à gauche de la scrollbar horizontal de navigation.
- ajout d'un menu contextuel dans l'arbre de navigation A FAIRE.
- ajouter une couleur (rouge sur les objets de l'arbre de navigation non encore saisis pour le moment uniquement pour les fils d'une calée) A FAIRE.
- fermeture profonde des marées et routes (la fermeture d'une route ferme aussi son activite ouverte) idem pour les marées. A FAIRE.
- passer toutes les propriétés de type primitif (données numériques) par des types objet afifn de gérer la nullité sur les objets A FAIRE (au mois 1J)
- suppression par défaut de l'option générer une base vide dans l'assitant de création de base A FAIRE
- faire une doc utilisateur et de dev A FAIRE

Interfaces graphiques
---------------------

Libellés divers
===============

- Cause coup nul A FAIRE
- correction faute orthographe dans le changeur de base. A FAIRE
- rendre plus complête les descriptions de l'assistant de base et des opérations base A FAIRE.
- remplacement du libellé "Réinitialiser" par "annuler". A FAIRE
- remplacement du libéllé "Accéder à..." en "Descendre vers..." A FAIRE
- ajout des unités sur les libellés A FAIRE


Arbre des objets
================

- trier les routes par ordre croissant sur les jours d'obervsation
- trier les activités par ordre croissant sur les heures d'observation
- les objets de type route et activité sont repositionnés dans l'arbre 
  après chaque modif ou création.
- on laisse les objets fictifs "routes" et "activité" car ils contiennent 
  maintenant des données et cela demanderait trop d'effort pour changer 
  le mode de fonctionnement actuel.
- ajouter un menu contextuel pour l'écran d'édition reprennant exactement les opérations de l'écran A FAIRE.

Formulaire Marée
================
- mettre une croix de reset devant la date fin : non fait car cela n'est 
  pas opportun : la date de fin est recalculée à chaque modification d'une 
  route, et vaut toujours la date de la dernière route. A FAIRE

Formulaire Route
================

- positionnement du loch matin en dessous du jour d'observation A FAIRE
- contrôle : la saisie d'une date précédant celle de la route la plus ancienne déclanche un warning ?  A FAIRE.
- ajouter un reset sur les lochs (lorsqu'on pourra gerer les primitifs null) A FAIRE
- pour le moment, il n'est pas possible de différencier la non saisie d'un loch (car valeur par défaut 0), on 
  pourrait utiliser la valeur -1 pour indiquer en base qu'aucune valeur n'a été saisie. Je regarde si on peut 
  utiliser plutôt des Integer au lieu de int, cve qui permettrait d'avoir un état de nullité A FAIRE.
- ajout d'un contrôle pour vérifier qu'une activité 16 (fin de veille) est la dernière activité d'une route 
  à la fermeture d'une route sinon on propose la création de cette activité. A FAIRE

Formulaire Activité
===================

- contrôle : la saisie d'une heure qui précède celle de l'activité disposant de l'heure la plus avancée doit
  générer un warning. A FAIRE.
- on ne peut pas ajouter un warning tant qe l'observateur n'a pas cliqué sur les heures (les controles portent 
  sur les objets et pas sur les uis). A FAIRE
- initialiser l'heure de début avec l'heure de fin de la dernière activité ou bien l'heure courant si pas d'activité. A FAIRE.
- ajouter un reset sur la température A FAIRE
- ajout des libellés sur les quadrants. A FAIRE
- contrôle sur le quadrant par rapport à l'océan de la marée  (Indien : 1 ou 2, Atlantique : 1,2, 3 ou 4). A FAIRE
- contrôle de la différence de température entre 2 activitè < 12 °. A FAIRE
- contrôle : warning si pas de température saisie. A FAIRE
- ajout des coordonnées sexagécimales (uniquement degre - minute, secondes toujours à 0) A FAIRE. 
- contrôle : saisie des quadrant et coordonnées facultatif (warning)  A FAIRE
- GPX utilise le format DD de coordonnées donc pas de modification en base à faire.
- les warning apparaissent aussi en mode création A FAIRE
- contrôle : vérifier que la vitesse obtenue par calcul sur l'activité précédente est plausible (< 30 noeud) A FAIRE.
- contrôle : les horaires sont croissants : debut < fin coulissage < fin A FAIRE
- contrôle : pas deux couples date/horaire identique A FAIRE
- pour une activite de type 7 "fin de peche" on doit récupérer l'activité de 
  type 6 "début de pêche" et vérifier qu'un délai de 45 minutes s'est écoulé A FAIRE.
- il n'est pas possible pour le moment de bloquer l'éditeur des heures.
- Amélioration de la fermeture d'une activité (sur le noeud "Activités" le bouton clôturer est désormais présent). A FAIRE
- controle : ajout warning tant q'une DCP n'a pas été ajouté A FAIRE
- Ajout d'une indication textuelle au dessus de l'activité bateau : "(Seule l'activité 6 – Début de pêche permet de saisir une calée)" A FAIRE

Formulaire Systèmes observés
============================

- ajout des libellés. A FAIRE
- deplacement de la distance en dessous de la liste des systèmes observés. A FAIRE

Formulaire Caléee
=================

- suppression de la case à cocher "rejet faune" : la valeur est calculée par 
  rapport aux données et n'est plus modifiable par l'utilisateur. A FAIRE
- contrôle sur les horaires : h debut < h fin coulissage < h fin et | h fin - h fin coulissage | > 45 minutes A FAIRE
- contrôle sur la direction courant : 0/+360° A FAIRE

Formulaire Estimation Banc
==========================

- ajout d'une clef d'unicité sur l'espèce thon A FAIRE
- modification libellé "créer" en "Insérer l'estimation" A FAIRE

Formulaire Capture Thon
=======================

- ajout d'une clef d'unicité sur l'espèce thon + categorie poids + identifiant cuve A FAIRE
- la liste des epèces de thon est filtrées sur celle appartenant à une catégorie poids donc il existe des espèces non listées, A VOIR SI OK.
- modification libellé "Espèce de thon" en "Thon mis en cuve" A FAIRE
- modification libellé "créer" en "Insérer cette capture / catégorie" A FAIRE

Formulaire Rejet Thon
=====================

- la liste des epèces de thon est filtrées sur celle appartenant à une catégorie poids donc il existe des espèces non listées, A VOIR SI OK.
- ajout au référentiel Rejet Thon de "4 - Poisson abimé" A FAIRE
- suppression de la case à cocher "rejet thon" : la valeur est désormais calculée par rapport aux données A FAIRE
- la case à cocher monté sur le pont doit être sélectionnée par défaut A FAIRE
- ajout d'une clef d'unicité sur l'espèce thon + categorie poids + identifiant cuve A FAIRE
- A droite de la case à cocher "Monté sur le point" ajouter l'explication "Seules les espèces montées sur le pont pourront êtres échantillonées" A FAIRE.
- Il n'y a pas de lien avec la capture des thon sur les espèces disponibles A FAIRE.
- modification libellé "créer" en "Insérer ce rejet / catégorie / raison" A FAIRE

Formulaire Echantillon Thon
===========================

- modification libellé "Echantillon Thon" en "Echantillon Thon rejeté" dans l'arbre de navigation A FAIRE
- modification libellé "longueur " en "longueur (cm inf.)" A FAIRE
- ajout du filtre sur les espèces précedemment rejeé ET monté à bord (actuellement le second test n'est pas fait) A FAIRE
- modification libellé "créer" en "Insérer cet échantillon" A FAIRE

Formulaire Capture Faune
========================

- modification libellé "Capture Faune" en "Faune accessoire conservée ou rejetée" A FAIRE
- contrôle : 0 < poids estimé (en T) < 400 T A FAIRE
- contrôle : 0 < nombre  estimé < 10000 A FAIRE
- contrôle : poids moyen, taille moyenne par rapport au tableau tableau faunaminmax donné (on pourra le transformer en csv) A FAIRE
  (si espèce non trouvée dans le fichier alors pas de contrôle )
- modification libellé "créer" en "Insérer cette espèce/poids/devenir" A FAIRE

Formulaire Echantillon Faune
============================

- modification libellé "Echantillon Faune" en "Echantillon Faune accessoire" A FAIRE
- modification libellé "créer" en "Insérer cette échantillon" A FAIRE
- on filtre les espèces sur celles capturée ou rejetés (actullement uniquement celle rejetées) A FAIRE
- modification libellé "longueur " en "Taille (cm)" A FAIRE
- l'écran est éditable uniquement si ces captures faunes on été saisies A FAIRE
- contrôle : longueur contraint dans le tableau faunaminmax A FAIRE

Formulaire DCP
==============
- ajouter dans réferentiel Type objet : "9 - Objet expérimental" (doit etre OK) A FAIRE
- modification libellé "Opération de l'objet" en "Opération sur l'objet" A FAIRE
- la liste des opérations est remontée tout en haut A FAIRE
- modification du liébellé de commentaire pour indiquer sur quel objet le commentaire est rattaché. A FAIRE

Formulaire Opération sur balise
===============================
- contrôle : la saisie du code est facultatif (passage en warning) A FAIRE
- permettre l'utilisation d'un éditeur de texte simple ou éditeur numérique pour le code de balise selon la valeur enregistrée en base A FAIRE
- ajouter un reset sur le nombre de mise à l'eau A FAIRE

Validation
----------

- passer tous les contrôles n2 en n1 A FAIRE
- revoir les écrans avec tableaux pour faire la validation directement sur les objets et non pas sur des modeles d'ui  A FAIRE

Import GPS
----------

- a faire A FAIRE

Changement de source
--------------------

- faire la doc A FAIRE
- ajouter un résumé des opérations effectuées A FAIRE

Opérations sur base
-------------------

- faire la doc A FAIRE
- supprimer l'action de validation n2 A FAIRE

Base Postgis
------------

- créer un script sh pour lancer la création de la base pg A FAIRE
- créer le script sql pour inserer le trigger de modification du champ theo_gem A FAIRE


