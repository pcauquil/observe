package fr.ird.observe.util;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ApplicationConfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;

/**
 * Pour mettre du code util commun.
 *
 * Created on 01/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ObserveUtil {

    public static Properties loadProperties(Properties sourceProperties, ApplicationConfig config) {

        Properties targetProperties = new Properties();
        for (Map.Entry<Object, Object> entry : sourceProperties.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            String newValue = config.replaceRecursiveOptions(value);
            targetProperties.setProperty(key, newValue);
        }
        return targetProperties;
    }

    public static  List<Class> sortTypes(Collection<Class> types, Function<Class, String> function) {

        List<Class> list = new ArrayList<>(types);
        new ClassComparator(function).sort(list);
        return list;

    }

    private static class ClassComparator implements Comparator<Class> {

        private final Map<Class, String> cache;
        private final Function<Class, String> function;

        private ClassComparator(Function<Class, String> function) {
            this.cache = new HashMap<>();
            this.function = function;
        }

        @Override
        public int compare(Class o1, Class o2) {
            String s1 = getValue(o1);
            String s2 = getValue(o2);
            return s1.compareTo(s2);
        }

        String getValue(Class klass) {
            String result = cache.get(klass);
            if (result == null) {
                result = function.apply(klass);
                cache.put(klass, result);
            }
            return result;
        }

        public void sort(List<Class> list) {
            Collections.sort(list, this);
            cache.clear();
        }
    }
}
