package fr.ird.observe.services.dto.gson;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.actions.report.ReportVariable;
import fr.ird.observe.services.dto.gson.reference.DataReferenceAdapter;
import fr.ird.observe.services.dto.gson.reference.DataReferenceSetAdapter;
import fr.ird.observe.services.dto.gson.reference.ReferentialReferenceAdapter;
import fr.ird.observe.services.dto.gson.reference.ReferentialReferenceSetAdapter;
import fr.ird.observe.services.dto.gson.reference.UnknownReferenceAdapter;
import fr.ird.observe.services.dto.referential.ReferentialMultimap;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import org.nuiton.version.Version;

import java.sql.Blob;
import java.util.Date;
import java.util.function.Supplier;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveDtoGsonSupplier implements Supplier<Gson> {

    public static final Supplier<Gson> DEFAULT_GSON_SUPPLIER = () -> new ObserveDtoGsonSupplier().get();

    protected final boolean prettyPrint;

    protected GsonBuilder gsonBuilder;

    protected Gson gson;

    public ObserveDtoGsonSupplier() {
        this(false);
    }

    public ObserveDtoGsonSupplier(boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
    }

    @Override
    public Gson get() {
        if (gson == null) {
            gson = getGsonBuilder(prettyPrint).create();
        }
        return gson;
    }

    protected GsonBuilder getGsonBuilder(boolean prettyPrint) {

        if (gsonBuilder == null) {

            // Assign only when initialization is finished to be thread-safe
            gsonBuilder = new GsonBuilder();

            if (prettyPrint) {
                gsonBuilder.setPrettyPrinting();
            }
            // Type adapters : base types
            gsonBuilder.registerTypeAdapter(Integer.class, new IntegerAdapter());
            gsonBuilder.registerTypeAdapter(Date.class, new DateAdapter());
            gsonBuilder.registerTypeAdapter(java.sql.Timestamp.class, new DateAdapter());
            gsonBuilder.registerTypeAdapter(java.sql.Time.class, new DateAdapter());
            gsonBuilder.registerTypeAdapter(java.sql.Date.class, new DateAdapter());
            gsonBuilder.registerTypeAdapter(Class.class, new ClassAdapter());
            gsonBuilder.registerTypeAdapter(Blob.class, new BlobAdapter());

            gsonBuilder.registerTypeAdapter(ImmutableList.class, new ImmutableListAdapter());
            gsonBuilder.registerTypeAdapter(ImmutableSet.class, new ImmutableSetAdapter());
            gsonBuilder.registerTypeAdapter(ImmutableMap.class, new ImmutableMapAdapter());
            gsonBuilder.registerTypeAdapter(ImmutableMultimap.class, new ImmutableMultimapAdapter());

            gsonBuilder.registerTypeAdapter(DataReference.class, new DataReferenceAdapter());
            gsonBuilder.registerTypeAdapter(ReferentialReference.class, new ReferentialReferenceAdapter());
            gsonBuilder.registerTypeAdapter(AbstractReference.class, new UnknownReferenceAdapter());
            gsonBuilder.registerTypeAdapter(DataReferenceSet.class, new DataReferenceSetAdapter());
            gsonBuilder.registerTypeAdapter(ReferentialReferenceSet.class, new ReferentialReferenceSetAdapter());

            gsonBuilder.registerTypeAdapter(Form.class, new FormAdapter());
            gsonBuilder.registerTypeAdapter(ReportVariable.class, new ReportVariableAdapter());
            gsonBuilder.registerTypeAdapter(Version.class, new VersionAdapter());

            gsonBuilder.registerTypeAdapter(ReferentialMultimap.class, new ReferentialMultimapAdapter());
            gsonBuilder.enableComplexMapKeySerialization();
        }
        return gsonBuilder;

    }

}
