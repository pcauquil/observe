package fr.ird.observe.services.dto;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.io.Serializable;

public class Form<R extends IdDto> implements ObserveDto, Serializable {

    public static final String PROPERTY_TYPE = "type";

    public static final String PROPERTY_OBJECT = "object";

    public static final String PROPERTY_REFERENTIAL_REFERENCE_SETS_REQUEST_NAME = "referentialReferenceSetsRequestName";

    public static final String PROPERTY_DATA_REFERENCE_SETS_REQUEST_NAME = "dataReferenceSetsRequestName";

    private static final long serialVersionUID = 1L;

    protected final Class<R> type;

    protected final R object;

    /**
     * Le nom de la requète à utiliser pour récupérer les ensembles de référentiels utilisés par ce formulaire.
     */
    protected final String referentialReferenceSetsRequestName;

    /**
     * Le nom de la requète à utiliser pour récupérer les ensembles de données utilisés par ce formulaire.
     */
    protected final String dataReferenceSetsRequestName;

    public static <R extends IdDto> Form<R> newFormDto(Class<R> type,
                                                       R object,
                                                       String referentialReferenceSetsRequestName,
                                                       String dataReferenceSetsRequestName) {
        return new Form<>(type, object, referentialReferenceSetsRequestName, dataReferenceSetsRequestName);
    }

    protected Form(Class<R> type, R object, String referentialReferenceSetsRequestName, String dataReferenceSetsRequestName) {
        this.type = type;
        this.object = object;
        this.referentialReferenceSetsRequestName = referentialReferenceSetsRequestName;
        this.dataReferenceSetsRequestName = dataReferenceSetsRequestName;
    }

    public R getObject() {
        return object;
    }

    public Class<R> getType() {
        return type;
    }

    public String getReferentialReferenceSetsRequestName() {
        return referentialReferenceSetsRequestName;
    }

    public String getDataReferenceSetsRequestName() {
        return dataReferenceSetsRequestName;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add(PROPERTY_TYPE, type.getName())
                .add(PROPERTY_REFERENTIAL_REFERENCE_SETS_REQUEST_NAME, referentialReferenceSetsRequestName)
                .add(PROPERTY_DATA_REFERENCE_SETS_REQUEST_NAME, dataReferenceSetsRequestName)
                .toString();
    }

}
