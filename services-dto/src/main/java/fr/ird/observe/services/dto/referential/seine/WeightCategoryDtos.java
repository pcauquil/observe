package fr.ird.observe.services.dto.referential.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class WeightCategoryDtos extends AbstractWeightCategoryDtos {

    /** Logger. */
    private static final Log log = LogFactory.getLog(WeightCategoryDtos.class);

    /**
     * @param speciesId l'identifiant de l'espèce
     * @return la liste de toutes les catégories de poids de l'espèce
     */
    public static  List<ReferentialReference<WeightCategoryDto>> filterSpeciesWeightCategories(Set<ReferentialReference<WeightCategoryDto>> weightCategoryReferences, String speciesId) {

        List<ReferentialReference<WeightCategoryDto>> references = weightCategoryReferences.stream().filter(new WeightCategorySpeciesPredicate(speciesId)).collect(Collectors.toList());

        if (log.isInfoEnabled()) {
            log.info("Found " + references.size() + " categories for species: " + speciesId);
        }
        return references;

    }

    private static class WeightCategorySpeciesPredicate implements Predicate<ReferentialReference<WeightCategoryDto>> {

        private final String speciesId;

        public WeightCategorySpeciesPredicate(String speciesId) {
            this.speciesId = speciesId;
        }

        @Override
        public boolean test(ReferentialReference<WeightCategoryDto> input) {
            String inputSpeciesId = (String) input.getPropertyValue(WeightCategoryDto.PROPERTY_SPECIES);
            return speciesId.equals(inputSpeciesId);
        }

    }
}
