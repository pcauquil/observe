package fr.ird.observe.services.dto.referential.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialReference;

public class VesselActivitySeineDtos extends AbstractVesselActivitySeineDtos {

    private static final String VESSEL_ACTIVITY_ID_FOR_SET = "fr.ird.observe.entities.referentiel.seine.VesselActivitySeine#1239832675369#0.12552908048322586";

    public static final String VESSEL_ACTIVITY_ID_CHANGED_ZONE = "fr.ird.observe.entities.referentiel.seine.VesselActivitySeine#1379684416896#0.38648073770690594";

    public static boolean isSetOpreration(String id) {
        return VESSEL_ACTIVITY_ID_FOR_SET.equals(id);
    }

    public static boolean isSetOperation(VesselActivitySeineDto vesselActivitySeineDto) {
        return vesselActivitySeineDto != null && isSetOpreration(vesselActivitySeineDto.getId());
    }

    public static boolean isSetOperation(ReferentialReference<VesselActivitySeineDto> vesselActivitySeineRef) {
        return vesselActivitySeineRef != null && isSetOpreration(vesselActivitySeineRef.getId());
    }

    public static boolean isChangedZoneOperation(String id) {
        return VESSEL_ACTIVITY_ID_CHANGED_ZONE.equals(id);
    }

    public static boolean isChangedZoneOperation(VesselActivitySeineDto vesselActivitySeineDto) {
        return vesselActivitySeineDto != null && isChangedZoneOperation(vesselActivitySeineDto.getId());
    }

    public static boolean isChangedZoneOperation(ReferentialReference<VesselActivitySeineDto> vesselActivitySeineRef) {
        return vesselActivitySeineRef != null && isChangedZoneOperation(vesselActivitySeineRef.getId());
    }

} //VesselActivitySeineDtos
