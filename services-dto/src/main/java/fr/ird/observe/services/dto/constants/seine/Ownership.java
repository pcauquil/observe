/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.dto.constants.seine;

/**
 * An enum to define the ownership of a DCP.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public enum Ownership implements Comparable<Ownership> {

    /** inconnue */
    unknown,

    /** appartient a ce vessel */
    ceVessel,

    /** appartien a un autre vessel */
    autreVessel;

    public static Ownership valueOf(int ordinal)
            throws IllegalArgumentException {
        for (Ownership o : values()) {
            if (o.ordinal() == ordinal) {
                return o;
            }
        }
        throw new IllegalArgumentException(
                "could not find a " + Ownership.class.getSimpleName() +
                " value for ordinal " + ordinal);
    }
}
