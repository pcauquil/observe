package fr.ird.observe.services.dto.reference;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.reflections.Reflections;

import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Pour récupérer une instance d'un {@link ReferenceBinderEngine}.
 *
 * On utilise un {@link Reflections} pour récupérer une instance car l'implantation n'est pas définie au niveau de
 * l'API mais dans le service ToPIA, au même niveau que le BinderEngine (et ceci pour ne pas avoir plusieurs endroits
 * de définition des binders.
 *
 * Created on 28/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferenceBinderEngineSupplier implements Supplier<ReferenceBinderEngine> {

    private static final ReferenceBinderEngine INSTANCE = loadReferenceBinderEngine();

    @Override
    public ReferenceBinderEngine get() {
        return INSTANCE;
    }

    private static ReferenceBinderEngine loadReferenceBinderEngine() {

        Set<Class<? extends ReferenceBinderEngineSupplier>> impls = new Reflections("fr.ird.observe.services").getSubTypesOf(ReferenceBinderEngineSupplier.class);

        if (impls.isEmpty()) {
            throw new ExceptionInInitializerError("No reference binder engine supplier found.");
        }
        Optional<Class<? extends ReferenceBinderEngineSupplier>> optional = impls.stream().filter(impl -> !ReferenceBinderEngineSupplier.class.equals(impl)).findAny();
        if (optional.isPresent()) {
            try {
                return optional.get().newInstance().get();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new ExceptionInInitializerError(e);
            }
        }
        throw new ExceptionInInitializerError("Can't get a reference binder engine supplier");

    }

}
