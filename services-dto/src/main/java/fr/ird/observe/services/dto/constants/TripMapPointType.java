package fr.ird.observe.services.dto.constants;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * <b>Attention : les noms des constantes sont utilisées dans la feuille de style de rendu. Si on change les noms ici,
 * il faut réimpacter dans le fichier style.xml</b>
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public enum TripMapPointType {

    seineDepartureHarbour,
    seineLandingHarbour,
    seineActivity,
    seineActivityInHarbour,
    seineActivityWithFreeSchoolType,
    seineActivityWithObjectSchoolType,

    longlineActivity,
    longlineDepartureHarbour,
    longlineLandingHarbour,
    longlineActivityInHarbour,
    longlineActivityWithSettingStart,
    longlineActivityWithSettingEnd,
    longlineActivityWithHaulingStart,
    longlineActivityWithHaulingEnd,
    longlineActivityWithInteraction,
    longlineActivityWithStation

}
