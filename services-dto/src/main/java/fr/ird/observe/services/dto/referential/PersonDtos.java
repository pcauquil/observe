package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PersonDtos extends AbstractPersonDtos {

    public static List<ReferentialReference<PersonDto>> filterCaptainReferences(Collection<ReferentialReference<PersonDto>> incoming) {
        return incoming.stream().filter(newCaptainReferencePredicate()).collect(Collectors.toList());
    }

    public static List<ReferentialReference<PersonDto>> filterObserverReferences(Collection<ReferentialReference<PersonDto>> incoming) {
        return incoming.stream().filter(newObserverReferencePredicate()).collect(Collectors.toList());
    }

    public static List<ReferentialReference<PersonDto>> filterDataEntryOperatorReferences(Collection<ReferentialReference<PersonDto>> incoming) {
        return incoming.stream().filter(newDataEntryOperatorReferencePredicate()).collect(Collectors.toList());
    }

    public static Predicate<ReferentialReference<PersonDto>> newCaptainReferencePredicate() {
        return input -> input.getPropertyNames().contains(PersonDto.PROPERTY_CAPTAIN)
               && Objects.equals(true, input.getPropertyValue(PersonDto.PROPERTY_CAPTAIN));

    }

    public static Predicate<ReferentialReference<PersonDto>> newDataEntryOperatorReferencePredicate() {
        return input -> input.getPropertyNames().contains(PersonDto.PROPERTY_DATA_ENTRY_OPERATOR)
               && Objects.equals(true, input.getPropertyValue(PersonDto.PROPERTY_DATA_ENTRY_OPERATOR));

    }

    public static Predicate<ReferentialReference<PersonDto>> newObserverReferencePredicate() {
        return input -> input.getPropertyNames().contains(PersonDto.PROPERTY_OBSERVER)
               && Objects.equals(true, input.getPropertyValue(PersonDto.PROPERTY_OBSERVER));

    }

    public static String getNames(PersonDto person) {
        return person.getFirstName() + " " + person.getLastName();
    }

    public static String getNames(ReferentialReference<?> personrRef) {
        return personrRef.getPropertyValue(PersonDto.PROPERTY_FIRST_NAME) + " " + personrRef.getPropertyValue(PersonDto.PROPERTY_LAST_NAME);
    }

}
