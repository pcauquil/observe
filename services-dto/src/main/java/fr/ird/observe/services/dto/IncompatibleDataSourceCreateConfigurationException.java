package fr.ird.observe.services.dto;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 23/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IncompatibleDataSourceCreateConfigurationException extends Exception {

    private static final long serialVersionUID = 1L;

    protected final DataSourceCreateConfigurationDto configuration;

    public IncompatibleDataSourceCreateConfigurationException(DataSourceCreateConfigurationDto configuration) {
        this.configuration = configuration;
    }

    public DataSourceCreateConfigurationDto getConfiguration() {
        return configuration;
    }

}
