package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class VesselDtos extends AbstractVesselDtos {


    public static List<ReferentialReference<VesselDto>> filterVesselReferencesByVesselTypeIds(Collection<ReferentialReference<VesselDto>> incoming, Set<String> vesselTypeIds) {
        return incoming.stream().filter(newVesselReferenceByVesselTypeIdPredicate(vesselTypeIds)).collect(Collectors.toList());
    }

    public static Predicate<ReferentialReference<VesselDto>> newVesselReferenceByVesselTypeIdPredicate(Set<String> vesselTypeIds) {

        return new VesselByVesselTypeIdPredicate(vesselTypeIds);

    }

    private static class VesselByVesselTypeIdPredicate implements Predicate<ReferentialReference<VesselDto>> {

        final Set<String> vesselTypeIds;

        private VesselByVesselTypeIdPredicate(Set<String> vesselTypeIds) {
            this.vesselTypeIds = vesselTypeIds;
        }

        @Override
        public boolean test(ReferentialReference<VesselDto> input) {
            String vesselTypId = (String) input.getPropertyValue(VesselDto.PROPERTY_VESSEL_TYPE);
            return vesselTypeIds.contains(vesselTypId);
        }
    }

}
