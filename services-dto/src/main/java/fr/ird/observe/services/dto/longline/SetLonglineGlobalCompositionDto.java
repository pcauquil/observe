package fr.ird.observe.services.dto.longline;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class SetLonglineGlobalCompositionDto extends AbstractSetLonglineGlobalCompositionDto {

    private static final long serialVersionUID = 3473510292736074595L;

    public static final String PROPERTY_FLOATLINES_COMPOSITION_PROPORTION_SUM = "floatlinesCompositionProportionSum";

    public static final String PROPERTY_BRANCHLINES_COMPOSITION_PROPORTION_SUM = "branchlinesCompositionProportionSum";

    public static final String PROPERTY_HOOKS_COMPOSITION_PROPORTION_SUM = "hooksCompositionProportionSum";

    public static final String PROPERTY_BAITS_COMPOSITION_PROPORTION_SUM = "baitsCompositionProportionSum";

    public int getFloatlinesCompositionProportionSum() {
        int sum = 0;
        if (!isFloatlinesCompositionEmpty()) {
            for (FloatlinesCompositionDto composition : floatlinesComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    public int getBranchlinesCompositionProportionSum() {
        int sum = 0;
        if (!isBranchlinesCompositionEmpty()) {
            for (BranchlinesCompositionDto composition : branchlinesComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    public int getHooksCompositionProportionSum() {
        int sum = 0;
        if (!isHooksCompositionEmpty()) {
            for (HooksCompositionDto composition : hooksComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    public int getBaitsCompositionProportionSum() {
        int sum = 0;
        if (!isBaitsCompositionEmpty()) {
            for (BaitsCompositionDto composition : baitsComposition) {
                if (composition.getProportion() != null) {
                    sum += composition.getProportion();
                }
            }
        }
        return sum;
    }

    public void setFloatlinesCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        firePropertyChange(PROPERTY_FLOATLINES_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

    public void setBranchlinesCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        firePropertyChange(PROPERTY_BRANCHLINES_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

    public void setHooksCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        firePropertyChange(PROPERTY_HOOKS_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

    public void setBaitsCompositionProportionSum(int sum) {
        // not used but required by validation
        // just fire the given value
        firePropertyChange(PROPERTY_BAITS_COMPOSITION_PROPORTION_SUM, 0, sum);
    }

}
