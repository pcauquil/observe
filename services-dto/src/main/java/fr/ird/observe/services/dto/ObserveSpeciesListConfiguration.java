package fr.ird.observe.services.dto;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Contient la configuration des listes de favoris d'espèces à utiliser.
 *
 * Created on 18/10/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveSpeciesListConfiguration {

    /**
     * La valeur par défaut pour obtenir la liste des espèces capture du modèle palangre.
     */
    public static final String DEFAULT_SPECIES_LIST_LONGLINE_CATCH_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.3"; // catch

    /**
     * La valeur par défaut pour obtenir la liste des espèces rencontrées du modèle palangre.
     */
    public static final String DEFAULT_SPECIES_LIST_LONGLINE_ENCOUNTER_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.4"; // encounter

    /**
     * La valeur par défaut pour obtenir la liste des espèces de déprédation capture du modèle palangre.
     */
    public static final String DEFAULT_SPECIES_LIST_LONGLINE_DEPREDATOR_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.5"; // depradator

    /**
     * La valeur par défaut pour obtenir la liste des espèces d'estimation du modèle seine.
     */
    public static final String DEFAULT_SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.1"; // espece thons

    /**
     * La valeur par défaut pour obtenir la liste des espèces non cible du modèle seine.
     */
    public static final String DEFAULT_SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.2"; // espece faune

    /**
     * La valeur par défaut pour obtenir la liste des espèces estimées d'objet du modèle seine.
     */
    public static final String DEFAULT_SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.1"; // espece thons

    /**
     * La valeur par défaut pour obtenir la liste des espèces observées sur objet du modèle seine.
     */
    public static final String DEFAULT_SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.2"; // espece faune

    /**
     * La valeur par défaut pour obtenir la liste des espèces capture du modèle seine.
     */
    public static final String DEFAULT_SPECIES_LIST_SEINE_TARGET_CATCH_ID = "fr.ird.observe.entities.referentiel.SpeciesList#1239832675370#0.1"; // espece thons

    protected String speciesListLonglineCatchId;

    protected String speciesListLonglineDepredatorId;

    protected String speciesListLonglineEncounterId;

    protected String speciesListSeineNonTargetCatchId;

    protected String speciesListSeineSchoolEstimateId;

    protected String speciesListSeineObjectSchoolEstimateId;

    protected String speciesListSeineObjectObservedSpeciesId;

    protected String speciesListSeineTargetCatchId;

    /**
     * @return une configuration avec les valeurs par défaut.
     */
    public static ObserveSpeciesListConfiguration newDefaultConfiguration() {

        ObserveSpeciesListConfiguration speciesListConfiguration = new ObserveSpeciesListConfiguration();
        speciesListConfiguration.setSpeciesListLonglineCatchId(DEFAULT_SPECIES_LIST_LONGLINE_CATCH_ID);
        speciesListConfiguration.setSpeciesListLonglineDepredatorId(DEFAULT_SPECIES_LIST_LONGLINE_DEPREDATOR_ID);
        speciesListConfiguration.setSpeciesListLonglineEncounterId(DEFAULT_SPECIES_LIST_LONGLINE_ENCOUNTER_ID);
        speciesListConfiguration.setSpeciesListSeineNonTargetCatchId(DEFAULT_SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID);
        speciesListConfiguration.setSpeciesListSeineObjectObservedSpeciesId(DEFAULT_SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID);
        speciesListConfiguration.setSpeciesListSeineObjectSchoolEstimateId(DEFAULT_SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID);
        speciesListConfiguration.setSpeciesListSeineSchoolEstimateId(DEFAULT_SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID);
        speciesListConfiguration.setSpeciesListSeineTargetCatchId(DEFAULT_SPECIES_LIST_SEINE_TARGET_CATCH_ID);
        return speciesListConfiguration;
    }

    public String getSpeciesListLonglineCatchId() {
        return speciesListLonglineCatchId;
    }

    public void setSpeciesListLonglineCatchId(String speciesListLonglineCatchId) {
        this.speciesListLonglineCatchId = speciesListLonglineCatchId;
    }

    public String getSpeciesListLonglineDepredatorId() {
        return speciesListLonglineDepredatorId;
    }

    public void setSpeciesListLonglineDepredatorId(String speciesListLonglineDepredatorId) {
        this.speciesListLonglineDepredatorId = speciesListLonglineDepredatorId;
    }

    public String getSpeciesListLonglineEncounterId() {
        return speciesListLonglineEncounterId;
    }

    public void setSpeciesListLonglineEncounterId(String speciesListLonglineEncounterId) {
        this.speciesListLonglineEncounterId = speciesListLonglineEncounterId;
    }

    public String getSpeciesListSeineNonTargetCatchId() {
        return speciesListSeineNonTargetCatchId;
    }

    public void setSpeciesListSeineNonTargetCatchId(String speciesListSeineNonTargetCatchId) {
        this.speciesListSeineNonTargetCatchId = speciesListSeineNonTargetCatchId;
    }

    public String getSpeciesListSeineSchoolEstimateId() {
        return speciesListSeineSchoolEstimateId;
    }

    public void setSpeciesListSeineSchoolEstimateId(String speciesListSeineSchoolEstimateId) {
        this.speciesListSeineSchoolEstimateId = speciesListSeineSchoolEstimateId;
    }

    public String getSpeciesListSeineObjectSchoolEstimateId() {
        return speciesListSeineObjectSchoolEstimateId;
    }

    public void setSpeciesListSeineObjectSchoolEstimateId(String speciesListSeineObjectSchoolEstimateId) {
        this.speciesListSeineObjectSchoolEstimateId = speciesListSeineObjectSchoolEstimateId;
    }

    public String getSpeciesListSeineObjectObservedSpeciesId() {
        return speciesListSeineObjectObservedSpeciesId;
    }

    public void setSpeciesListSeineObjectObservedSpeciesId(String speciesListSeineObjectObservedSpeciesId) {
        this.speciesListSeineObjectObservedSpeciesId = speciesListSeineObjectObservedSpeciesId;
    }

    public String getSpeciesListSeineTargetCatchId() {
        return speciesListSeineTargetCatchId;
    }

    public void setSpeciesListSeineTargetCatchId(String speciesListSeineTargetCatchId) {
        this.speciesListSeineTargetCatchId = speciesListSeineTargetCatchId;
    }

}
