package fr.ird.observe.services.dto.gson.reference;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.lang.reflect.Type;

/**
 * Cet adapter est destiné à permettre la désérialisation des objets Observe contenant
 * des propriétés de tye AbstractReference.
 *
 * Par exemple : ValidateResultForDto possède une propriété dto de type AbstractReference
 *
 * Au moment de la désérialisation, il faut déterminer le type concret de la référence (DataReference, ReferentialReference..)
 * afin de permettre la désérialisation.
 * (AbstractReference étant une classe abstraite, elle n'est pas instanciable)
 *
 * @author smaisonneuve
 * Created on 19/08/16.
 */
public class UnknownReferenceAdapter<D extends IdDto, R extends AbstractReference<D>> implements JsonDeserializer<R> {

    @Override
    public final R deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        R reference;

        JsonObject jsonObject = json.getAsJsonObject();

        Class<D> dtoType = context.deserialize(jsonObject.get(AbstractReference.PROPERTY_TYPE), Class.class);

        if (ReferentialDto.class.isAssignableFrom(dtoType)) {
            reference = context.deserialize(json, ReferentialReference.class);
        } else {
            reference = context.deserialize(json, DataReference.class);
        }

        return reference;
    }
}
