package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;

import java.util.Set;

public class GearCaracteristicTypeDtos extends AbstractGearCaracteristicTypeDtos {

    private static final Set<String> INTEGER_IDS = Sets.newHashSet(
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.3",
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.5"
    );

    private static final Set<String> FLOAT_IDS = Sets.newHashSet(
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.4",
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.6"
    );

    private static final Set<String> BOOLEAN_IDS = Sets.newHashSet(
            "fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.2"
    );


    public static boolean isBoolean(GearCaracteristicTypeDto type) {
        return isBoolean(type.getId());
    }

    public static boolean isBoolean(String gearCaracteristicTypeId) {
        return BOOLEAN_IDS.contains(gearCaracteristicTypeId);
    }

    public static boolean isInteger(GearCaracteristicTypeDto type) {
        return isInteger(type.getId());
    }

    public static boolean isInteger(String gearCaracteristicTypeId) {
        return INTEGER_IDS.contains(gearCaracteristicTypeId);
    }

    public static boolean isFloat(GearCaracteristicTypeDto type) {
        return isFloat(type.getId());
    }

    public static boolean isFloat(String gearCaracteristicTypeId) {
        return FLOAT_IDS.contains(gearCaracteristicTypeId);
    }


    public static Object getTypeValue(String gearCaracteristicTypeId, Object value) {

        if (value != null && !value.toString().isEmpty()) {

            if (isBoolean(gearCaracteristicTypeId)) {

                value = Boolean.valueOf(value.toString());

            } else if (isInteger(gearCaracteristicTypeId)) {

                value = Float.valueOf(value.toString()).intValue();

            } else if (isFloat(gearCaracteristicTypeId)) {

                value = Float.valueOf(value.toString());

            }
        }

        return value;

    }
}
