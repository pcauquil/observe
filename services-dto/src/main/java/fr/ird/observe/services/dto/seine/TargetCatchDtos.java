package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TargetCatchDtos extends AbstractTargetCatchDtos {

    public static List<TargetCatchDto> filterDiscarded(Collection<TargetCatchDto> source, boolean discardedFilter) {
        return source.stream()
                     .filter(t -> discardedFilter == t.isDiscarded())
                     .collect(Collectors.toList());
    }

    public static <BeanType extends TargetCatchDto> void copyTargetCatchDto(BeanType source, BeanType target) {
        // The reasonForDiscard has to be the last property to be set :
        // Setting species and categoryWeight properties will fire event that might end up
        // in resetting the reasonForDiscard to null into the bean.
        //
        // This is a side effect from the ui (TargetDiscarCatchUi) : changing the selected species reinitializes weightCategory and reasonForDiscard to null
        // This works with events and these events are also unfortunately triggered when selecting a new row in a table
        // and thus changing te value of the TargetCatchDto bean
        //
        // To avoid being trapped with this mechanism we need to ensure that the reasonDorDiscard will be set at the end.
        // If not, you will probably end up with an empty value in the reasonForDiscard field within the ui when you select another row
        // in the table.
        ReferentialReference<ReasonForDiscardDto> reasonForDiscard = source.reasonForDiscard;
        AbstractTargetCatchDtos.copyTargetCatchDto(source, target);
        target.setReasonForDiscard(reasonForDiscard);
    }
}
