package fr.ird.observe.services.dto.longline;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created on 12/12/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class SectionWithTemplates {

    public static List<SectionWithTemplate> getSectionTemplates(Collection<SectionDto> sections) {

        List<SectionWithTemplate> sectionTemplates = new ArrayList<>();

        if (sections != null) {

            for (SectionDto section1 : sections) {
                SectionWithTemplate e = new SectionWithTemplate(section1);
                sectionTemplates.add(e);
            }

        }

        return sectionTemplates;
    }

    public static LinkedHashSet<SectionDto> getSections(List<SectionWithTemplate> data) {
        return Sets.newLinkedHashSet(Iterables.transform(data, SectionWithTemplate::getDelegate));
    }
}
