package fr.ird.observe.services.dto;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import java.io.Serializable;
import java.util.Optional;

public abstract class AbstractReferenceSet<D extends IdDto, R extends AbstractReference<D>> implements ObserveDto, Serializable {

    public static final String PROPERTY_TYPE = "type";

    public static final String PROPERTY_REFERENCES = "references";

    private static final long serialVersionUID = 1L;

    protected final Class<D> type;

    protected final ImmutableSet<R> references;

    protected AbstractReferenceSet(Class<D> type, ImmutableSet<R> references) {
        this.type = type;
        this.references = references;
    }

    public Class<D> getType() {
        return type;
    }

    public R getReferenceById(String id) {
        return tryGetReferenceById(id).orElse(null);
    }

    public Optional<R> tryGetReferenceById(String id) {
        return references.stream().filter(AbstractReference.newIdPredicate(id)).findFirst();
    }

    public R getReferenceByPosition(int index) {
        return Iterables.get(references, index);
    }

    public int sizeReference() {
        return references.size();
    }

    public ImmutableSet<R> getReferences() {
        return references;
    }

}
