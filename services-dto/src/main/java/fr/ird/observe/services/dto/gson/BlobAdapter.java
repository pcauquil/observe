package fr.ird.observe.services.dto.gson;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import javax.sql.rowset.serial.SerialBlob;
import java.lang.reflect.Type;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BlobAdapter implements JsonSerializer<Blob>, JsonDeserializer<Blob> {

    @Override
    public Blob deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        String blob64 = json.getAsString();

        byte[] bytes = Base64.getDecoder().decode(blob64);


        try {

            return new SerialBlob(bytes);

        } catch (SQLException e) {
            throw new JsonParseException("could not create blob ", e);
        }

    }

    @Override
    public JsonElement serialize(Blob src, Type typeOfSrc, JsonSerializationContext context) {
        try {

            byte[] bytes = src.getBytes(1, (int) src.length());

            Base64.Encoder encoder = Base64.getEncoder();

            String blob64 = encoder.encodeToString(bytes);

            return new JsonPrimitive(blob64);

        } catch (SQLException e) {
            throw new JsonParseException("could not read blob ", e);
        }
    }
}
