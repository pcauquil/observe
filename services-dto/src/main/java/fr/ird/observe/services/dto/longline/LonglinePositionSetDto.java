package fr.ird.observe.services.dto.longline;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.DataReference;

import java.util.Collection;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public interface LonglinePositionSetDto {

    DataReference<SectionDto> getSections(int index);

    boolean isSectionsEmpty();

    int sizeSections();

    void addSections(DataReference<SectionDto> sections);

    void addAllSections(Collection<DataReference<SectionDto>> sections);

    boolean removeSections(DataReference<SectionDto> sections);

    boolean removeAllSections(Collection<DataReference<SectionDto>> sections);

    boolean containsSections(DataReference<SectionDto> sections);

    boolean containsAllSections(Collection<DataReference<SectionDto>> sections);

    Collection<DataReference<SectionDto>> getSections();

    void setSections(Collection<DataReference<SectionDto>> sections);

    DataReference<BasketDto> getBaskets(int index);

    boolean isBasketsEmpty();

    int sizeBaskets();

    void addBaskets(DataReference<BasketDto> baskets);

    void addAllBaskets(Collection<DataReference<BasketDto>> baskets);

    boolean removeBaskets(DataReference<BasketDto> baskets);

    boolean removeAllBaskets(Collection<DataReference<BasketDto>> baskets);

    boolean containsBaskets(DataReference<BasketDto> baskets);

    boolean containsAllBaskets(Collection<DataReference<BasketDto>> baskets);

    Collection<DataReference<BasketDto>> getBaskets();

    void setBaskets(Collection<DataReference<BasketDto>> baskets);

    DataReference<BranchlineDto> getBranchlines(int index);

    boolean isBranchlinesEmpty();

    int sizeBranchlines();

    void addBranchlines(DataReference<BranchlineDto> branchlines);

    void addAllBranchlines(Collection<DataReference<BranchlineDto>> branchlines);

    boolean removeBranchlines(DataReference<BranchlineDto> branchlines);

    boolean removeAllBranchlines(Collection<DataReference<BranchlineDto>> branchlines);

    boolean containsBranchlines(DataReference<BranchlineDto> branchlines);

    boolean containsAllBranchlines(Collection<DataReference<BranchlineDto>> branchlines);

    Collection<DataReference<BranchlineDto>> getBranchlines();

    void setBranchlines(Collection<DataReference<BranchlineDto>> branchlines);
}
