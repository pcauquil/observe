package fr.ird.observe.services.dto;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created on 21/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class AbstractReference<D extends IdDto> implements ObserveDto, Serializable {

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_LAST_UPDATE_DATE = "lastUpdateDate";

    public static final String PROPERTY_TYPE = "type";

    public static final String PROPERTY_CREATE_DATE = "createDate";

    public static final String PROPERTY_VERSION = "version";

    public static final String PROPERTY_LABEL_PROPERTY_NAMES = "labelPropertyNames";

    public static final String PROPERTY_LABEL_PROPERTY_VALUES = "labelPropertyValues";

    public static final Function<AbstractReference, String> ID_FUNCTION = AbstractReference::getId;

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractReference.class);

    protected Class<D> type;

    protected String id;

    protected Date lastUpdateDate;

    protected Date createDate;

    protected long version;

    protected List<String> labelPropertyNames;

    protected Serializable[] labelPropertyValues;

    public static <BeanType extends AbstractReference> Predicate<BeanType> newIdPredicate(String id) {
        return input -> Objects.equals(id, input.getId());
    }

    public static <BeanType extends AbstractReference> Predicate<BeanType> newIdsPredicate(Collection<String> ids) {
        return input -> ids.contains(input.getId());
    }

    public static <BeanType extends AbstractReference> Map<String, BeanType> splitById(Collection<BeanType> dtos) {
        return Maps.uniqueIndex(dtos, ID_FUNCTION::apply);
    }

    public static <BeanType extends AbstractReference> Collection<BeanType> filterById(Collection<BeanType> source, String id) {
        return source.stream().filter(newIdPredicate(id)).collect(Collectors.toList());
    }

    public static <BeanType extends AbstractReference> BeanType find(Collection<BeanType> source, String id) {
        return source.stream().filter(newIdPredicate(id)).findFirst().orElse(null);
    }

    public void init(Class<D> type, String[] labelPropertyNames, Serializable... labelPropertyValues) {
        this.type = type;
        this.labelPropertyNames = Arrays.asList(labelPropertyNames);
        this.labelPropertyValues = labelPropertyValues;
    }

    public Class<D> getType() {
        return type;
    }

    public void setType(Class<D> type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Serializable getPropertyValue(String propertyName) {
        int propertyIndex = getPropertyIndex(propertyName);
        return labelPropertyValues[propertyIndex];
    }

    public Serializable[] getLabelPropertyValues() {
        return labelPropertyValues;
    }

    public List<String> getPropertyNames() {
        return labelPropertyNames;
    }

    protected int getPropertyIndex(String propertyName) {
        int index = labelPropertyNames.indexOf(propertyName);
        if (index == -1) {
            throw new ReferenceValueNotFoundException(type, propertyName);
        }
        return index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractReference<?> that = (AbstractReference<?>) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, id);
    }

}
