package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.AbstractReference;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created on 06/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialReferences {

    public static final Function<ReferentialReference, String> ID_FUNCTION = AbstractReference::getId;

    protected static final Predicate<ReferentialReference> IS_ENABLE_PREDICATE = ReferentialReference::isEnabled;

    public static <D extends ReferentialDto> List<ReferentialReference<D>> filterEnabled(Collection<ReferentialReference<D>> references) {

        return references.stream().filter(ReferentialReference::isEnabled).collect(Collectors.toList());

    }

    public static <D extends ReferentialDto> List<ReferentialReference<D>> filterContains(Collection<ReferentialReference<D>> references, Set<String> containsIds) {

        return references.stream().filter(r -> containsIds.contains(r.getId())).collect(Collectors.toList());

    }

    public static <D extends ReferentialDto> List<ReferentialReference<D>> filterNotContains(Collection<ReferentialReference<D>> references, Set<String> containsIds) {

        return references.stream().filter(r -> !containsIds.contains(r.getId())).collect(Collectors.toList());

    }

    public static Function<ReferentialReference, String> getIdFunction() {
        return ID_FUNCTION;

    }

}
