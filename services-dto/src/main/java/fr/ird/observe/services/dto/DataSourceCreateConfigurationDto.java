package fr.ird.observe.services.dto;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;

import java.util.Objects;

/**
 * Created on 22/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DataSourceCreateConfigurationDto extends AbstractObserveDto {

    private static final long serialVersionUID = 1L;

    /**
     * Le contenu de la base à importer (Optionel).
     */
    protected byte[] optionalImportDatabase;

    /**
     * Une configuration de la dataSource à utiliser pour importer le référentiel (Optionel).
     */
    protected ObserveDataSourceConfiguration optionalImportReferentialDataSourceConfiguration;

    /**
     * Une configuration de la dataSource à utiliser pour importer des données (Optionel).
     */
    protected ObserveDataSourceConfiguration optionalImportDataDataSourceConfiguration;

    /**
     * Les données à importer (ids des marées) (Optionel).
     */
    protected ImmutableSet<String> optionalImportDataIds;

    /**
     * Pour autoriser la création d'une base vide (utilisé pour les bases temporaires)
     */
    protected boolean canCreateEmptyDatabase;

    private boolean leaveOpenSource;

    public boolean isCanCreateEmptyDatabase() {
        return canCreateEmptyDatabase;
    }

    public void setCanCreateEmptyDatabase(boolean canCreateEmptyDatabase) {
        this.canCreateEmptyDatabase = canCreateEmptyDatabase;
    }

    public boolean isImportDatabase() {
        return optionalImportDatabase != null;
    }

    public boolean isImportReferential() {
        return optionalImportReferentialDataSourceConfiguration != null;
    }

    public boolean isImportData() {
        return optionalImportDataDataSourceConfiguration != null;
    }

    public boolean isLeaveOpenSource() {
        return leaveOpenSource;
    }

    public void setLeaveOpenSource(boolean leaveOpenSource) {
        this.leaveOpenSource = leaveOpenSource;
    }

    public byte[] getImportDatabase() {
        return optionalImportDatabase;
    }

    public ObserveDataSourceConfiguration getImportReferentialDataSourceConfiguration() {
        return optionalImportReferentialDataSourceConfiguration;
    }

    public ObserveDataSourceConfiguration getImportDataDataSourceConfiguration() {
        return optionalImportDataDataSourceConfiguration;
    }

    public ImmutableSet<String> getImportDataIds() {
        return optionalImportDataIds;
    }

    public void setImportDatabase(byte... importDatabase) {
        Objects.requireNonNull(importDatabase, "'importDatabase' can't be null.");
        this.optionalImportDatabase = importDatabase;
    }

    public void setImportReferentialDataSourceConfiguration(ObserveDataSourceConfiguration importReferentialDataSourceConfiguration) {
        Objects.requireNonNull(importReferentialDataSourceConfiguration, "'importReferentialDataSourceConfiguration' can't be null.");
        this.optionalImportReferentialDataSourceConfiguration = importReferentialDataSourceConfiguration;
    }

    public void setImportDataConfiguration(ObserveDataSourceConfiguration importDataDataSourceConfiguration, ImmutableSet<String> importDataIds) {
        Objects.requireNonNull(importDataDataSourceConfiguration, "'importDataDataSourceConfiguration' can't be null.");
        Objects.requireNonNull(importDataIds, "'importDataIds' can't be null.");
        this.optionalImportDataDataSourceConfiguration = importDataDataSourceConfiguration;
        this.optionalImportDataIds = importDataIds;
    }

    public void validateConfiguration() throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException {

        boolean importDatabase = isImportDatabase();

        if (importDatabase) {

            // on ne peut pas importer autre chose en même
            if (isImportData() || isImportReferential()) {
                throw new IncompatibleDataSourceCreateConfigurationException(this);
            }

        } else {

            if (!isCanCreateEmptyDatabase() && !isImportReferential()) {

                // on n'autorise pas la création d'une base sans référentiel.
                throw new DataSourceCreateWithNoReferentialImportException(this);
            }

        }

    }
}
