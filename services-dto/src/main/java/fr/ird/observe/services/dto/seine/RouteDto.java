package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.services.dto.IdDtos;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.util.Date;
import java.util.Objects;

public class RouteDto extends AbstractRouteDto {

    private static final Log log = LogFactory.getLog(RouteDto.class);

    private static final long serialVersionUID = 1L;

    public boolean isActivityFindDeVeilleFound() {
        int position = Iterables.indexOf(getActivitySeine(),
                                         ActivitySeineStubDtos.newActivityFinDeVeillePredicate(true)::test);
        return position >= 0;

    }

    public boolean isTimeAvailable(String activitySeineId, Date time) {
        Date currentTime = DateUtil.getTime(time, false, false);

        return ! getActivitySeine()
            .stream()
            .filter(activity ->
                Objects.equals(currentTime, activity.getTimeSecond()) 
                && ! Objects.equals(activitySeineId, activity.getId()))
            .findFirst()
            .isPresent();
    }

    public ActivitySeineStubDto getPreviousActivity(String activitySeineId) {

        ActivitySeineStubDto previous = null;

        if (activitySeineId != null) {

            int currentPosition = Iterables.indexOf(getActivitySeine(), IdDtos.newIdPredicate(activitySeineId)::test);
            if (currentPosition >= 1) {
                previous = Iterables.get(getActivitySeine(), currentPosition - 1);
            }
        }

        if (previous != null) {
            if (log.isDebugEnabled()) {
                log.debug("previous activity " +
                        previous.getTime());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("no previous activity for " + activitySeine);
            }
        }

        return previous;
    }
}
