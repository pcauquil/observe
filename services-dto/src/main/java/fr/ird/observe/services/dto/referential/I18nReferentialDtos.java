package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class I18nReferentialDtos extends AbstractI18nReferentialDtos {


    public static <E extends I18nReferentialDto> String decorate(int referenceLocaleOrdinal, E i18nDto) {

        String result = null;

        switch (referenceLocaleOrdinal + 1) {
            case 1:
                result = i18nDto.getLabel1();
                break;
            case 2:
                result = i18nDto.getLabel2();
                break;
            case 3:
                result = i18nDto.getLabel3();
                break;
            case 4:
                result = i18nDto.getLabel4();
                break;
            case 5:
                result = i18nDto.getLabel5();
                break;
            case 6:
                result = i18nDto.getLabel6();
                break;
            case 7:
                result = i18nDto.getLabel7();
                break;
            case 8:
                result = i18nDto.getLabel8();
                break;
        }

        return result;

    }


}
