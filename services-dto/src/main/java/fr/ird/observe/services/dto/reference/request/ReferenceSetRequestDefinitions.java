package fr.ird.observe.services.dto.reference.request;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineObservedSystemDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;

import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.BAIT_HAULING_STATUS;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.BAIT_SETTING_STATUS;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.BAIT_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.CATCH_FATE_LONGLINE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.COUNTRY;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.DETECTION_MODE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.ENCOUNTER_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.FPA_ZONE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.GEAR;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.GEAR_CARACTERISTIC;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.GEAR_CARACTERISTIC_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.HARBOUR;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.HEALTHNESS;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.HOOK_POSITION;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.HOOK_SIZE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.HOOK_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.ITEM_HORIZONTAL_POSITION;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.ITEM_VERTICAL_POSITION;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.LENGTH_WEIGHT_PARAMETER;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.LIGHTSTICKS_COLOR;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.LIGHTSTICKS_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.LINE_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.MATURITY_STATUS;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.MITIGATION_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.OBJECT_FATE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.OBJECT_OPERATION;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.OBJECT_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.OBSERVED_SYSTEM;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.OCEAN;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.ORGANISM;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.PERSON;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.PROGRAM;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.REASON_FOR_DISCARD;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.REASON_FOR_NO_FISHING;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.REASON_FOR_NULL_SET;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SENSOR_BRAND;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SENSOR_DATA_FORMAT;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SENSOR_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SETTING_SHAPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SEX;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SIZE_MEASURE_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SPECIES;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SPECIES_FATE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SPECIES_GROUP;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SPECIES_LIST;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SPECIES_STATUS;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.STOMAC_FULLNESS;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.SURROUNDING_ACTIVITY;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.TRANSMITTING_BUOY_OPERATION;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.TRANSMITTING_BUOY_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.TRIP_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.VESSEL;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.VESSEL_ACTIVITY_LONGLINE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.VESSEL_ACTIVITY_SEINE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.VESSEL_SIZE_CATEGORY;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.VESSEL_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.WEIGHT_CATEGORY;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.WEIGHT_MEASURE_TYPE;
import static fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions.WIND;

/**
 * Contient les définitions de tous les requêtes que les services proposent.
 *
 * Pour chaque type qui correspond à un formulaire, on décrit ici que les référentiels à utiliser, pour les données
 * métier, on gère ça à la main (peu de cas au final).
 *
 * Created on 11/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public enum ReferenceSetRequestDefinitions {

    // LONGLINE DATA

    TRIP_LONGLINE_FORM(
            newBuilder(TripLonglineDto.class)
                    .addKey(TripLonglineDto.PROPERTY_TRIP_TYPE, TRIP_TYPE)
                    .addKey(TripLonglineDto.PROPERTY_OBSERVER, PERSON)
                    .addKey(TripLonglineDto.PROPERTY_VESSEL, VESSEL)
                    .addKey(TripLonglineDto.PROPERTY_CAPTAIN, PERSON)
                    .addKey(TripLonglineDto.PROPERTY_DATA_ENTRY_OPERATOR, PERSON)
                    .addKey(TripLonglineDto.PROPERTY_PROGRAM, PROGRAM)
                    .addKey(TripLonglineDto.PROPERTY_OCEAN, OCEAN)
                    .addKey(TripLonglineDto.PROPERTY_DEPARTURE_HARBOUR, HARBOUR)
                    .addKey(TripLonglineDto.PROPERTY_LANDING_HARBOUR, HARBOUR)),

    TRIP_LONGLINE_GEAR_USE_FORM(
            newBuilder(GearUseFeaturesLonglineDto.class)
                    .addKey(GearUseFeaturesLonglineDto.PROPERTY_GEAR, GEAR)
                    .addKey(GearUseFeaturesMeasurementLonglineDto.PROPERTY_GEAR_CARACTERISTIC, GEAR_CARACTERISTIC)),

    ACTIVITY_LONGLINE_FORM(
            newBuilder(ActivityLonglineDto.class)
                    .addKey(ActivityLonglineDto.PROPERTY_FPA_ZONE, FPA_ZONE)
                    .addKey(ActivityLonglineDto.PROPERTY_VESSEL_ACTIVITY_LONGLINE, VESSEL_ACTIVITY_LONGLINE)),

    ACTIVITY_LONGLINE_SENSOR_USED_FORM(
            newBuilder(SensorUsedDto.class)
                    .addKey(SensorUsedDto.PROPERTY_SENSOR_TYPE, SENSOR_TYPE)
                    .addKey(SensorUsedDto.PROPERTY_SENSOR_BRAND, SENSOR_BRAND)
                    .addKey(SensorUsedDto.PROPERTY_SENSOR_DATA_FORMAT, SENSOR_DATA_FORMAT)),

    ACTIVITY_LONGLINE_ENCOUTER_FORM(
            newBuilder(EncounterDto.class)
                    .addKey(EncounterDto.PROPERTY_ENCOUNTER_TYPE, ENCOUNTER_TYPE)
                    .addKey(EncounterDto.PROPERTY_SPECIES, SPECIES)),

    BRANCHLINE_FORM(
            newBuilder(BranchlineDto.class)
                    .addKey(BranchlineDto.PROPERTY_BAIT_SETTING_STATUS, BAIT_SETTING_STATUS)
                    .addKey(BranchlineDto.PROPERTY_HOOK_SIZE, HOOK_SIZE)
                    .addKey(BranchlineDto.PROPERTY_BAIT_HAULING_STATUS, BAIT_HAULING_STATUS)
                    .addKey(BranchlineDto.PROPERTY_HOOK_TYPE, HOOK_TYPE)
                    .addKey(BranchlineDto.PROPERTY_TRACELINE_TYPE, LINE_TYPE)
                    .addKey(BranchlineDto.PROPERTY_TOP_TYPE, LINE_TYPE)
                    .addKey(BranchlineDto.PROPERTY_BAIT_TYPE, BAIT_TYPE)),

    SET_LONGLINE_GLOBAL_COMPOSITION_FORM(
            newBuilder(SetLonglineGlobalCompositionDto.class)
                    .addKey(SetLonglineGlobalCompositionDto.PROPERTY_MITIGATION_TYPE, MITIGATION_TYPE)
                    .addKey(FloatlinesCompositionDto.PROPERTY_LINE_TYPE, LINE_TYPE)
                    .addKey(BranchlinesCompositionDto.PROPERTY_TOP_TYPE, LINE_TYPE)
                    .addKey(BranchlinesCompositionDto.PROPERTY_TRACELINE_TYPE, LINE_TYPE)
                    .addKey(HooksCompositionDto.PROPERTY_HOOK_TYPE, HOOK_TYPE)
                    .addKey(HooksCompositionDto.PROPERTY_HOOK_SIZE, HOOK_SIZE)
                    .addKey(BaitsCompositionDto.PROPERTY_BAIT_SETTING_STATUS, BAIT_SETTING_STATUS)
                    .addKey(BaitsCompositionDto.PROPERTY_BAIT_TYPE, BAIT_TYPE)),

    SET_LONGLINE_DETAIL_COMPOSITION_FORM(
            newBuilder(SetLonglineDetailCompositionDto.class)
                    .addKey(BranchlineDto.PROPERTY_TOP_TYPE, LINE_TYPE)
                    .addKey(BranchlineDto.PROPERTY_TRACELINE_TYPE, LINE_TYPE)
                    .addKey(BranchlineDto.PROPERTY_HOOK_TYPE, HOOK_TYPE)
                    .addKey(BranchlineDto.PROPERTY_HOOK_SIZE, HOOK_SIZE)
                    .addKey(BranchlineDto.PROPERTY_BAIT_TYPE, BAIT_TYPE)
                    .addKey(BranchlineDto.PROPERTY_BAIT_SETTING_STATUS, BAIT_SETTING_STATUS)
                    .addKey(BranchlineDto.PROPERTY_BAIT_HAULING_STATUS, BAIT_HAULING_STATUS)),

    SET_LONGLINE_FORM(
            newBuilder(SetLonglineDto.class)
                    .addKey(SetLonglineDto.PROPERTY_SETTING_SHAPE, SETTING_SHAPE)
                    .addKey(SetLonglineDto.PROPERTY_LINE_TYPE, LINE_TYPE)
                    .addKey(SetLonglineDto.PROPERTY_LIGHTSTICKS_TYPE, LIGHTSTICKS_TYPE)
                    .addKey(SetLonglineDto.PROPERTY_LIGHTSTICKS_COLOR, LIGHTSTICKS_COLOR)),

    SET_LONGLINE_CATCH_FORM(
            newBuilder(CatchLonglineDto.class)
                    .addKey(CatchLonglineDto.PROPERTY_SPECIES_CATCH, SPECIES)
                    .addKey(CatchLonglineDto.PROPERTY_CATCH_HEALTHNESS, HEALTHNESS)
                    .addKey(CatchLonglineDto.PROPERTY_HOOK_POSITION, HOOK_POSITION)
                    .addKey(CatchLonglineDto.PROPERTY_CATCH_FATE_LONGLINE, CATCH_FATE_LONGLINE)
                    .addKey(CatchLonglineDto.PROPERTY_DISCARD_HEALTHNESS, HEALTHNESS)
                    .addKey(CatchLonglineDto.PROPERTY_PREDATOR, SPECIES)
                    .addKey(CatchLonglineDto.PROPERTY_STOMAC_FULLNESS, STOMAC_FULLNESS)
                    .addKey(CatchLonglineDto.PROPERTY_SEX, SEX)
                    .addKey(CatchLonglineDto.PROPERTY_MATURITY_STATUS, MATURITY_STATUS)
                    .addKey(SizeMeasureDto.PROPERTY_SIZE_MEASURE_TYPE, SIZE_MEASURE_TYPE)
                    .addKey(WeightMeasureDto.PROPERTY_WEIGHT_MEASURE_TYPE, WEIGHT_MEASURE_TYPE)
                    .addKey(BranchlineDto.PROPERTY_BAIT_HAULING_STATUS, BAIT_HAULING_STATUS)),

    SET_LONGLINE_TDR_FORM(
            newBuilder(TdrDto.class)
                    .addKey(TdrDto.PROPERTY_SENSOR_BRAND, SENSOR_BRAND)
                    .addKey(TdrDto.PROPERTY_ITEM_HORIZONTAL_POSITION, ITEM_HORIZONTAL_POSITION)
                    .addKey(TdrDto.PROPERTY_ITEM_VERTICAL_POSITION, ITEM_VERTICAL_POSITION)
                    .addKey(TdrDto.PROPERTY_SPECIES, SPECIES)),

    // SEINE DATA

    TRIP_SEINE_FORM(
            newBuilder(TripSeineDto.class)
                    .addKey(TripSeineDto.PROPERTY_OBSERVER, PERSON)
                    .addKey(TripSeineDto.PROPERTY_CAPTAIN, PERSON)
                    .addKey(TripSeineDto.PROPERTY_DATA_ENTRY_OPERATOR, PERSON)
                    .addKey(TripSeineDto.PROPERTY_VESSEL, VESSEL)
                    .addKey(TripSeineDto.PROPERTY_OCEAN, OCEAN)
                    .addKey(TripSeineDto.PROPERTY_DEPARTURE_HARBOUR, HARBOUR)
                    .addKey(TripSeineDto.PROPERTY_LANDING_HARBOUR, HARBOUR)
                    .addKey(TripSeineDto.PROPERTY_PROGRAM, PROGRAM)),

    TRIP_SEINE_GEAR_USE_FORM(
            newBuilder(GearUseFeaturesSeineDto.class)
                    .addKey(GearUseFeaturesSeineDto.PROPERTY_GEAR, GEAR)
                    .addKey(GearUseFeaturesMeasurementSeineDto.PROPERTY_GEAR_CARACTERISTIC, GEAR_CARACTERISTIC)),

    ACTIVITY_SEINE_FORM(
            newBuilder(ActivitySeineDto.class)
                    .addKey(ActivitySeineDto.PROPERTY_VESSEL_ACTIVITY_SEINE, VESSEL_ACTIVITY_SEINE)
                    .addKey(ActivitySeineDto.PROPERTY_SURROUNDING_ACTIVITY, SURROUNDING_ACTIVITY)
                    .addKey(ActivitySeineDto.PROPERTY_WIND, WIND)
                    .addKey(ActivitySeineDto.PROPERTY_DETECTION_MODE, DETECTION_MODE)
                    .addKey(ActivitySeineDto.PROPERTY_REASON_FOR_NO_FISHING, REASON_FOR_NO_FISHING)
                    .addKey(ActivitySeineDto.PROPERTY_PREVIOUS_FPA_ZONE, FPA_ZONE)
                    .addKey(ActivitySeineDto.PROPERTY_CURRENT_FPA_ZONE, FPA_ZONE)
                    .addKey(ActivitySeineDto.PROPERTY_NEXT_FPA_ZONE, FPA_ZONE)),

    ACTIVITY_SEINE_OBSERVED_SYSTEM_FORM(
            newBuilder(ActivitySeineObservedSystemDto.class)
                    .addKey(ActivitySeineObservedSystemDto.PROPERTY_OBSERVED_SYSTEM, OBSERVED_SYSTEM)),

    SET_SEINE_FORM(
            newBuilder(SetSeineDto.class)
                    .addKey(SetSeineDto.PROPERTY_REASON_FOR_NULL_SET, REASON_FOR_NULL_SET)),

    SET_SEINE_SCHOOL_ESTIMATE_FORM(
            newBuilder(SchoolEstimateDto.class)
                    .addKey(SchoolEstimateDto.PROPERTY_SPECIES, SPECIES)),

    SET_SEINE_TARGET_CATCH_FORM(
            newBuilder(TargetCatchDto.class)
                    .addKey(TargetCatchDto.PROPERTY_SPECIES, SPECIES)
                    .addKey(TargetCatchDto.PROPERTY_REASON_FOR_DISCARD, REASON_FOR_DISCARD)
                    .addKey(TargetCatchDto.PROPERTY_WEIGHT_CATEGORY, WEIGHT_CATEGORY)),

    SET_SEINE_NON_TARGET_CATCH_FORM(
            newBuilder(NonTargetCatchDto.class)
                    .addKey(NonTargetCatchDto.PROPERTY_SPECIES, SPECIES)
                    .addKey(NonTargetCatchDto.PROPERTY_SPECIES_FATE, SPECIES_FATE)
                    .addKey(NonTargetCatchDto.PROPERTY_REASON_FOR_DISCARD, REASON_FOR_DISCARD)),

    TARGET_SAMPLE_FORM(
            newBuilder(TargetLengthDto.class)
                    .addKey(TargetLengthDto.PROPERTY_SPECIES, SPECIES)),

    NON_TARGET_SAMPLE_FORM(
            newBuilder(NonTargetLengthDto.class)
                    .addKey(NonTargetLengthDto.PROPERTY_SPECIES, SPECIES)
                    .addKey(NonTargetLengthDto.PROPERTY_SEX, SEX)),

    FLOATING_OBJECT_FORM(
            newBuilder(FloatingObjectDto.class)
                    .addKey(FloatingObjectDto.PROPERTY_OBJECT_TYPE, OBJECT_TYPE)
                    .addKey(FloatingObjectDto.PROPERTY_OBJECT_OPERATION, OBJECT_OPERATION)
                    .addKey(FloatingObjectDto.PROPERTY_OBJECT_FATE, OBJECT_FATE)),

    FLOATING_OBJECT_OBSERVED_SPECIES_FORM(
            newBuilder(ObjectObservedSpeciesDto.class)
                    .addKey(ObjectObservedSpeciesDto.PROPERTY_SPECIES, SPECIES)
                    .addKey(ObjectObservedSpeciesDto.PROPERTY_SPECIES_STATUS, SPECIES_STATUS)),

    FLOATING_OBJECT_SCHOOL_ESTIMATE_FORM(
            newBuilder(ObjectObservedSpeciesDto.class)
                    .addKey(ObjectObservedSpeciesDto.PROPERTY_SPECIES, SPECIES)),

    FLOATING_OBJECT_TRANSMITTING_BUOY_FORM(
            newBuilder(TransmittingBuoyDto.class)
                    .addKey(TransmittingBuoyDto.PROPERTY_TRANSMITTING_BUOY_OPERATION, TRANSMITTING_BUOY_OPERATION)
                    .addKey(TransmittingBuoyDto.PROPERTY_TRANSMITTING_BUOY_TYPE, TRANSMITTING_BUOY_TYPE)
                    .addKey(TransmittingBuoyDto.PROPERTY_COUNTRY, COUNTRY)),

    // COMMON REFERENTIAL

    GEAR_CARACTERISTIC_FORM(
            newReferentialBuilder(GearCaracteristicDto.class, GEAR_CARACTERISTIC)
                    .addKey(GearCaracteristicDto.PROPERTY_GEAR_CARACTERISTIC_TYPE, GEAR_CARACTERISTIC_TYPE)),

    GEAR_FORM(
            newReferentialBuilder(GearDto.class, GEAR)
                    .addKey(GearDto.PROPERTY_GEAR_CARACTERISTIC, GEAR_CARACTERISTIC)),

    HARBOUR_FORM(
            newReferentialBuilder(HarbourDto.class, HARBOUR)
                    .addKey(HarbourDto.PROPERTY_COUNTRY, COUNTRY)),

    LENGTH_WEIGHT_PARAMETER_FORM(
            newReferentialBuilder(LengthWeightParameterDto.class, LENGTH_WEIGHT_PARAMETER)
                    .addKey(LengthWeightParameterDto.PROPERTY_SPECIES, SPECIES)
                    .addKey(LengthWeightParameterDto.PROPERTY_OCEAN, OCEAN)
                    .addKey(LengthWeightParameterDto.PROPERTY_SEX, SEX)),

    ORGANISM_FORM(
            newReferentialBuilder(OrganismDto.class, ORGANISM)
                    .addKey(OrganismDto.PROPERTY_COUNTRY, COUNTRY)),

    PERSON_FORM(
            newReferentialBuilder(PersonDto.class, PERSON)
                    .addKey(PersonDto.PROPERTY_COUNTRY, COUNTRY)),

    PROGRAM_FORM(
            newReferentialBuilder(ProgramDto.class, PROGRAM)
                    .addKey(ProgramDto.PROPERTY_ORGANISM, ORGANISM)),

    SPECIES_FORM(
            newReferentialBuilder(SpeciesDto.class, SPECIES)
                    .addKey(SpeciesDto.PROPERTY_OCEAN, OCEAN)
                    .addKey(SpeciesDto.PROPERTY_SPECIES_GROUP, SPECIES_GROUP)),

    SPECIES_LIST_FORM(
            newReferentialBuilder(SpeciesListDto.class, SPECIES_LIST)
                    .addKey(SpeciesListDto.PROPERTY_SPECIES, SPECIES)),

    VESSEL_FORM(
            newReferentialBuilder(VesselDto.class, VESSEL)
                    .addKey(VesselDto.PROPERTY_FLAG_COUNTRY, COUNTRY)
                    .addKey(VesselDto.PROPERTY_VESSEL_TYPE, VESSEL_TYPE)
                    .addKey(VesselDto.PROPERTY_VESSEL_SIZE_CATEGORY, VESSEL_SIZE_CATEGORY)),

    COUNTRY_FORM(newReferentialBuilder(CountryDto.class, COUNTRY)),
    FPA_ZONE_FORM(newReferentialBuilder(FpaZoneDto.class, FPA_ZONE)),
    GEAR_CARACTERISTIC_TYPE_FORM(newReferentialBuilder(GearCaracteristicTypeDto.class, GEAR_CARACTERISTIC_TYPE)),
    OCEAN_FORM(newReferentialBuilder(OceanDto.class, OCEAN)),
    SEX_FORM(newReferentialBuilder(SexDto.class, SEX)),
    SPECIES_GROUP_FORM(newReferentialBuilder(SpeciesGroupDto.class, SPECIES_GROUP)),
    VESSEL_SIZE_CATEGORY_FORM(newReferentialBuilder(VesselSizeCategoryDto.class, VESSEL_SIZE_CATEGORY)),
    VESSEL_TYPE_FORM(newReferentialBuilder(VesselTypeDto.class, VESSEL_TYPE)),

    // LONGLINE REFERENTIAL

    BAIT_HAULING_STATUS_FORM(newReferentialBuilder(BaitHaulingStatusDto.class, BAIT_HAULING_STATUS)),
    BAIT_SETTING_STATUS_FORM(newReferentialBuilder(BaitSettingStatusDto.class, BAIT_SETTING_STATUS)),
    BAIT_TYPE_FORM(newReferentialBuilder(BaitTypeDto.class, BAIT_TYPE)),
    CATCH_FATE_LONGLINE_FORM(newReferentialBuilder(CatchFateLonglineDto.class, CATCH_FATE_LONGLINE)),
    ENCOUNTER_TYPE_FORM(newReferentialBuilder(EncounterTypeDto.class, ENCOUNTER_TYPE)),
    HEALTHNESS_FORM(newReferentialBuilder(HealthnessDto.class, HEALTHNESS)),
    HOOK_POSITION_FORM(newReferentialBuilder(HookPositionDto.class, HOOK_POSITION)),
    HOOK_SIZE_FORM(newReferentialBuilder(HookSizeDto.class, HOOK_SIZE)),
    HOOK_TYPE_FORM(newReferentialBuilder(HookTypeDto.class, HOOK_TYPE)),
    ITEM_HORIZONTAL_POSITION_FORM(newReferentialBuilder(ItemHorizontalPositionDto.class, ITEM_HORIZONTAL_POSITION)),
    ITEM_VERTICAL_POSITION_FORM(newReferentialBuilder(ItemVerticalPositionDto.class, ITEM_VERTICAL_POSITION)),
    LIGHTSTICKS_COLOR_FORM(newReferentialBuilder(LightsticksColorDto.class, LIGHTSTICKS_COLOR)),
    LIGHTSTICKS_TYPE_FORM(newReferentialBuilder(LightsticksTypeDto.class, LIGHTSTICKS_TYPE)),
    LINE_TYPE_FORM(newReferentialBuilder(LineTypeDto.class, LINE_TYPE)),
    MATURITY_STATUS_FORM(newReferentialBuilder(MaturityStatusDto.class, MATURITY_STATUS)),
    MITIGATION_TYPE_FORM(newReferentialBuilder(MitigationTypeDto.class, MITIGATION_TYPE)),
    SENSOR_BRAND_FORM(newReferentialBuilder(SensorBrandDto.class, SENSOR_BRAND)),
    SENSOR_DATA_FORMAT_FORM(newReferentialBuilder(SensorDataFormatDto.class, SENSOR_DATA_FORMAT)),
    SENSOR_TYPE_FORM(newReferentialBuilder(SensorTypeDto.class, SENSOR_TYPE)),
    SETTING_SHAPE_FORM(newReferentialBuilder(SettingShapeDto.class, SETTING_SHAPE)),
    SIZE_MEASURE_TYPE_FORM(newReferentialBuilder(SizeMeasureTypeDto.class, SIZE_MEASURE_TYPE)),
    STOMAC_FULLNESS_FORM(newReferentialBuilder(StomacFullnessDto.class, STOMAC_FULLNESS)),
    TRIP_TYPE_FORM(newReferentialBuilder(TripTypeDto.class, TRIP_TYPE)),
    VESSEL_ACTIVITY_LONGLINE_FORM(newReferentialBuilder(VesselActivityLonglineDto.class, VESSEL_ACTIVITY_LONGLINE)),
    WEIGHT_MEASURE_TYPE_FORM(newReferentialBuilder(WeightMeasureTypeDto.class, WEIGHT_MEASURE_TYPE)),

    // SEINE REFERENTIAL

    DETECTION_MODE_FORM(newReferentialBuilder(DetectionModeDto.class, DETECTION_MODE)),
    OBJECT_FATE_FORM(newReferentialBuilder(ObjectFateDto.class, OBJECT_FATE)),
    OBJECT_OPERATION_FORM(newReferentialBuilder(ObjectOperationDto.class, OBJECT_OPERATION)),
    OBJECT_TYPE_FORM(newReferentialBuilder(ObjectTypeDto.class, OBJECT_TYPE)),
    OBSERVED_SYSTEM_FORM(newReferentialBuilder(ObservedSystemDto.class, OBSERVED_SYSTEM)),
    REASON_FOR_DISCARD_FORM(newReferentialBuilder(ReasonForDiscardDto.class, REASON_FOR_DISCARD)),
    REASON_FOR_NO_FISHING_FORM(newReferentialBuilder(ReasonForNoFishingDto.class, REASON_FOR_NO_FISHING)),
    REASON_FOR_NULL_SET_FORM(newReferentialBuilder(ReasonForNullSetDto.class, REASON_FOR_NULL_SET)),
    SPECIES_FATE_FORM(newReferentialBuilder(SpeciesFateDto.class, SPECIES_FATE)),
    SPECIES_STATUS_FORM(newReferentialBuilder(SpeciesStatusDto.class, SPECIES_STATUS)),
    SURROUNDING_ACTIVITY_FORM(newReferentialBuilder(SurroundingActivityDto.class, SURROUNDING_ACTIVITY)),
    TRANSMITTING_BUOY_OPERATION_FORM(newReferentialBuilder(TransmittingBuoyOperationDto.class, TRANSMITTING_BUOY_OPERATION)),
    TRANSMITTING_BUOY_TYPE_FORM(newReferentialBuilder(TransmittingBuoyTypeDto.class, TRANSMITTING_BUOY_TYPE)),
    VESSEL_ACTIVITY_SEINE_FORM(newReferentialBuilder(VesselActivitySeineDto.class, VESSEL_ACTIVITY_SEINE)),
    WEIGHT_CATEGORY_FORM(
            newReferentialBuilder(WeightCategoryDto.class, WEIGHT_CATEGORY)
                    .addKey(WeightCategoryDto.PROPERTY_SPECIES, SPECIES)),
    WIND_FORM(newReferentialBuilder(WindDto.class, WIND));

    private final ReferenceSetRequestDefinition definition;

    private static ReferenceSetRequestDefinition.Builder newBuilder(Class<? extends IdDto> type) {
        return new ReferenceSetRequestDefinition.Builder(type);
    }

    private static ReferenceSetRequestDefinition.Builder newReferentialBuilder(Class<? extends ReferentialDto> type, ReferentialReferenceSetDefinitions propertyDefinition) {
        return new ReferenceSetRequestDefinition.Builder(type).addKey("referentialListHeader", propertyDefinition);
    }

    ReferenceSetRequestDefinitions(ReferenceSetRequestDefinition.Builder definitionBuilder) {
        this.definition = definitionBuilder.build();
    }

    public ReferenceSetRequestDefinition getDefinition() {
        return definition;
    }

    public static ReferenceSetRequestDefinition get(String definitionName) {

        ReferenceSetRequestDefinitions requestDefinitions = valueOf(definitionName);
        if (requestDefinitions == null) {
            throw new IllegalArgumentException("No definition with name " + definitionName + " registred");
        }

        return requestDefinitions.getDefinition();

    }

    public static ReferenceSetRequestDefinitions get(Class<? extends IdDto> type) {

        ReferenceSetRequestDefinitions requestDefinitions = null;

        for (ReferenceSetRequestDefinitions definitions : values()) {
            if (type.equals(definitions.getDefinition().getType())) {
                requestDefinitions = definitions;
                break;
            }
        }

        if (requestDefinitions == null) {
            throw new IllegalArgumentException("No definition with type " + type.getName() + " registred");
        }

        return requestDefinitions;

    }

}
