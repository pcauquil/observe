package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.AbstractReference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;

/**
 * Created on 21/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialReference<D extends ReferentialDto> extends AbstractReference<D> {

    public static final String PROPERTY_NEED_COMMENT = "needComment";

    public static final String PROPERTY_ENABLED = "enabled";

    public static final String PROPERTY_LABEL = "label";

    public static final ImmutableSet<String> I18N_PROPERTY_NAMES = ImmutableSet.of(
            I18nReferentialDto.PROPERTY_LABEL1,
            I18nReferentialDto.PROPERTY_LABEL2,
            I18nReferentialDto.PROPERTY_LABEL3,
            I18nReferentialDto.PROPERTY_LABEL4,
            I18nReferentialDto.PROPERTY_LABEL5,
            I18nReferentialDto.PROPERTY_LABEL6,
            I18nReferentialDto.PROPERTY_LABEL7,
            I18nReferentialDto.PROPERTY_LABEL8
    );

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialReference.class);

    protected boolean needComment;

    protected boolean enabled;

    public boolean isNeedComment() {
        return needComment;
    }

    public void setNeedComment(boolean needComment) {
        this.needComment = needComment;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper toStringHelper = MoreObjects.toStringHelper(this)
                .add(PROPERTY_TYPE, type.getSimpleName())
                .add(PROPERTY_ID, id);

        if (log.isDebugEnabled()) {
            toStringHelper
                    .add(PROPERTY_ENABLED, enabled)
                    .add(PROPERTY_NEED_COMMENT, needComment)
                    .add(PROPERTY_CREATE_DATE, createDate)
                    .add(PROPERTY_LAST_UPDATE_DATE, lastUpdateDate)
                    .add(PROPERTY_VERSION, version)
                    .add(PROPERTY_LABEL_PROPERTY_VALUES, labelPropertyNames);
        }
        return toStringHelper
                .add(PROPERTY_LABEL_PROPERTY_VALUES, Arrays.toString(labelPropertyValues))
                .toString();
    }

}
