package fr.ird.observe.services.dto.gson;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Samuel Maisonneuve - maisonneuve@codelutin.com
 */
public class ImmutableMultimapAdapter implements JsonSerializer<ImmutableMultimap>, JsonDeserializer<ImmutableMultimap> {


    @Override
    public JsonElement serialize(ImmutableMultimap src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.asMap());
    }

    @Override
    public ImmutableMultimap deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        ImmutableMultimap.Builder builder = ImmutableSetMultimap.builder();

        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        Type keyType = actualTypeArguments[0];
        Type valueType = actualTypeArguments[1];

        Type type2 = mapOf(TypeToken.of(keyType), TypeToken.of(valueType)).getType();
        Map map = context.deserialize(json, type2);

        for (Object key : map.keySet()) {
            List multimapValues = (List) map.get(key);

            for (Object multimapValue : multimapValues) {
                builder.put(key, multimapValue);
            }
        }

        return builder.build();

    }


    static <K, V> TypeToken<Map<K, Collection<V>>> mapOf(TypeToken<K> keyType, TypeToken<V> valueType) {
        return new TypeToken<Map<K, Collection<V>>>() {}
                .where(new TypeParameter<K>() {}, keyType)
                .where(new TypeParameter<V>() {}, valueType);
    }

}
