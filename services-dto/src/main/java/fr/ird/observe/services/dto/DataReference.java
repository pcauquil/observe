package fr.ird.observe.services.dto;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Predicate;

/**
 * Created on 21/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DataReference<D extends DataDto> extends AbstractReference<D> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataReference.class);

    public static Predicate<? super DataReference> newLabelValuePredicate(final String propertyName, final Serializable propertyValue) {
        return (Predicate<DataReference>) input -> input.getPropertyNames().contains(propertyName)
               && propertyValue.equals(input.getPropertyValue(propertyName));
    }

    public static Predicate<DataReference> newTripSeinePredicate() {
        return input -> IdDtos.isTripSeineId(input.getId());
    }

    public static <BeanType extends IdDto> java.util.function.Predicate<BeanType> newTripLonglinePredicate() {
        return input -> IdDtos.isTripLonglineId(input.getId());
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper toStringHelper = MoreObjects.toStringHelper(this)
                .add(PROPERTY_TYPE, type.getSimpleName())
                .add(PROPERTY_ID, id)
                .add(PROPERTY_LAST_UPDATE_DATE, lastUpdateDate);

        if (log.isDebugEnabled()) {
            toStringHelper
                    .add(PROPERTY_CREATE_DATE, createDate)
                    .add(PROPERTY_VERSION, version)
                    .add(PROPERTY_LABEL_PROPERTY_VALUES, labelPropertyNames);
        }
        return toStringHelper
                .add(PROPERTY_LABEL_PROPERTY_VALUES, Arrays.toString(labelPropertyValues))
                .toString();
    }

}
