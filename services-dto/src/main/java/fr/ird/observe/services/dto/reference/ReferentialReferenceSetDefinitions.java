package fr.ird.observe.services.dto.reference;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.constants.GearType;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.I18nReferentialDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Contient les définitions de tous les ensembles de références (de type référentiel) reconnus dans l'application.
 *
 * Created on 11/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public enum ReferentialReferenceSetDefinitions {

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL COMMON ---------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    COUNTRY(newDefaultDefinitionBuilder(CountryDto.class)),

    FPA_ZONE(newDefaultDefinitionBuilder(FpaZoneDto.class)),

    GEAR_CARACTERISTIC(newDefaultDefinitionBuilder(GearCaracteristicDto.class)
                               .addProperty(String.class, GearCaracteristicDto.PROPERTY_GEAR_CARACTERISTIC_TYPE)),

    GEAR_CARACTERISTIC_TYPE(newDefaultDefinitionBuilder(GearCaracteristicTypeDto.class)),

    GEAR(newDefaultDefinitionBuilder(GearDto.class)),

    HARBOUR(newDefinitionBuilder(HarbourDto.class)
                    .addProperty(String.class, HarbourDto.PROPERTY_CODE)
                    .addProperty(String.class, HarbourDto.PROPERTY_NAME)
                    .addProperty(String.class, HarbourDto.PROPERTY_LOCODE)),

    LENGTH_WEIGHT_PARAMETER(newDefinitionBuilder(LengthWeightParameterDto.class)
                                    .addProperty(String.class, LengthWeightParameterDto.PROPERTY_CODE)
                                    .addProperty(String.class, LengthWeightParameterDto.PROPERTY_OCEAN)
                                    .addProperty(String.class, LengthWeightParameterDto.PROPERTY_SPECIES)
                                    .addProperty(String.class, LengthWeightParameterDto.PROPERTY_SEX)
                                    .addProperty(String.class, LengthWeightParameterDto.PROPERTY_LENGTH_WEIGHT_FORMULA)
                                    .addProperty(String.class, LengthWeightParameterDto.PROPERTY_WEIGHT_LENGTH_FORMULA)),

    OCEAN(newDefaultDefinitionBuilder(OceanDto.class)),

    ORGANISM(newDefaultDefinitionBuilder(OrganismDto.class)),

    PERSON(newDefinitionBuilder(PersonDto.class)
                   .addProperty(String.class, PersonDto.PROPERTY_FIRST_NAME)
                   .addProperty(String.class, PersonDto.PROPERTY_LAST_NAME)
                   .addProperty(boolean.class, PersonDto.PROPERTY_CAPTAIN)
                   .addProperty(boolean.class, PersonDto.PROPERTY_OBSERVER)
                   .addProperty(boolean.class, PersonDto.PROPERTY_DATA_ENTRY_OPERATOR)),

    PROGRAM(newDefinitionBuilder(ProgramDto.class)
                    .addProperty(GearType.class, ProgramDto.PROPERTY_GEAR_TYPE)
                    .addProperty(String.class, ProgramDto.PROPERTY_GEAR_TYPE_PREFIX)),

    SEX(newDefaultDefinitionBuilder(SexDto.class)),

    SPECIES(newDefinitionBuilder(SpeciesDto.class)
                    .addProperty(String.class, SpeciesDto.PROPERTY_FAO_CODE)
                    .addProperty(String.class, SpeciesDto.PROPERTY_SCIENTIFIC_LABEL)
                    .addProperty(String.class, SpeciesDto.PROPERTY_HOME_ID)
                    .addProperty(String.class, SpeciesDto.PROPERTY_LENGTH_MEASURE_TYPE)),

    SPECIES_GROUP(newDefaultDefinitionBuilder(SpeciesGroupDto.class)),

    SPECIES_LIST(newDefaultDefinitionBuilder(SpeciesListDto.class)),

    VESSEL_SIZE_CATEGORY(newDefinitionBuilder(VesselSizeCategoryDto.class)
                                 .addProperty(String.class, VesselSizeCategoryDto.PROPERTY_CODE)
                                 .addProperty(String.class, VesselSizeCategoryDto.PROPERTY_GAUGE_LABEL)
                                 .addProperty(String.class, VesselSizeCategoryDto.PROPERTY_CAPACITY_LABEL)),

    VESSEL(newDefaultDefinitionBuilder(VesselDto.class)
                   .addProperty(String.class, VesselDto.PROPERTY_VESSEL_TYPE)),

    VESSEL_TYPE(newDefaultDefinitionBuilder(VesselTypeDto.class)),

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL LONGLINE -------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    BAIT_HAULING_STATUS(newDefaultDefinitionBuilder(BaitHaulingStatusDto.class)),

    BAIT_SETTING_STATUS(newDefaultDefinitionBuilder(BaitSettingStatusDto.class)),

    BAIT_TYPE(newDefaultDefinitionBuilder(BaitTypeDto.class)),

    CATCH_FATE_LONGLINE(newDefaultDefinitionBuilder(CatchFateLonglineDto.class)),

    ENCOUNTER_TYPE(newDefaultDefinitionBuilder(EncounterTypeDto.class)),

    HEALTHNESS(newDefaultDefinitionBuilder(HealthnessDto.class)),

    HOOK_POSITION(newDefaultDefinitionBuilder(HookPositionDto.class)),

    HOOK_SIZE(newDefaultDefinitionBuilder(HookSizeDto.class)),

    HOOK_TYPE(newDefaultDefinitionBuilder(HookTypeDto.class)),

    ITEM_HORIZONTAL_POSITION(newDefaultDefinitionBuilder(ItemHorizontalPositionDto.class)),

    ITEM_VERTICAL_POSITION(newDefaultDefinitionBuilder(ItemVerticalPositionDto.class)),

    LIGHTSTICKS_COLOR(newDefaultDefinitionBuilder(LightsticksColorDto.class)),

    LIGHTSTICKS_TYPE(newDefaultDefinitionBuilder(LightsticksTypeDto.class)),

    LINE_TYPE(newDefaultDefinitionBuilder(LineTypeDto.class)),

    MATURITY_STATUS(newDefaultDefinitionBuilder(MaturityStatusDto.class)),

    MITIGATION_TYPE(newDefaultDefinitionBuilder(MitigationTypeDto.class)),

    SENSOR_BRAND(newDefinitionBuilder(SensorBrandDto.class)
                         .addProperty(String.class, SensorBrandDto.PROPERTY_CODE)
                         .addProperty(String.class, SensorBrandDto.PROPERTY_BRAND_NAME)),

    SENSOR_DATA_FORMAT(newDefaultDefinitionBuilder(SensorDataFormatDto.class)),

    SENSOR_TYPE(newDefaultDefinitionBuilder(SensorTypeDto.class)),

    SETTING_SHAPE(newDefaultDefinitionBuilder(SettingShapeDto.class)),

    SIZE_MEASURE_TYPE(newDefaultDefinitionBuilder(SizeMeasureTypeDto.class)),

    STOMAC_FULLNESS(newDefaultDefinitionBuilder(StomacFullnessDto.class)),

    TRIP_TYPE(newDefaultDefinitionBuilder(TripTypeDto.class)),

    VESSEL_ACTIVITY_LONGLINE(newDefaultDefinitionBuilder(VesselActivityLonglineDto.class)),

    WEIGHT_MEASURE_TYPE(newDefaultDefinitionBuilder(WeightMeasureTypeDto.class)),

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL SEINE ----------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    DETECTION_MODE(newDefaultDefinitionBuilder(DetectionModeDto.class)),

    OBJECT_FATE(newDefaultDefinitionBuilder(ObjectFateDto.class)),

    OBJECT_OPERATION(newDefaultDefinitionBuilder(ObjectOperationDto.class)),

    OBJECT_TYPE(newDefaultDefinitionBuilder(ObjectTypeDto.class)),

    OBSERVED_SYSTEM(newDefaultDefinitionBuilder(ObservedSystemDto.class)),

    REASON_FOR_DISCARD(newDefaultDefinitionBuilder(ReasonForDiscardDto.class)),

    REASON_FOR_NO_FISHING(newDefaultDefinitionBuilder(ReasonForNoFishingDto.class)),

    REASON_FOR_NULL_SET(newDefaultDefinitionBuilder(ReasonForNullSetDto.class)),

    SPECIES_FATE(newDefaultDefinitionBuilder(SpeciesFateDto.class)),

    SPECIES_STATUS(newDefaultDefinitionBuilder(SpeciesStatusDto.class)),

    SURROUNDING_ACTIVITY(newDefaultDefinitionBuilder(SurroundingActivityDto.class)),

    TRANSMITTING_BUOY_OPERATION(newDefaultDefinitionBuilder(TransmittingBuoyOperationDto.class)),

    TRANSMITTING_BUOY_TYPE(newDefaultDefinitionBuilder(TransmittingBuoyTypeDto.class)),

    VESSEL_ACTIVITY_SEINE(newDefaultDefinitionBuilder(VesselActivitySeineDto.class)),

    WEIGHT_CATEGORY(newDefinitionBuilder(WeightCategoryDto.class)
                            .addProperty(String.class, WeightCategoryDto.PROPERTY_CODE)
                            .addProperty(String.class, WeightCategoryDto.PROPERTY_SPECIES)),

    WIND(newDefinitionBuilder(WindDto.class)
                 .addProperty(String.class, WindDto.PROPERTY_SPEED_RANGE)
    );

    public static final Map<String, ReferenceSetDefinition> MAPPING = new TreeMap<>();

    static {

        for (ReferentialReferenceSetDefinitions definitions : ReferentialReferenceSetDefinitions.values()) {

            ReferenceSetDefinition<? extends ReferentialDto> definition = definitions.getDefinition();
            MAPPING.put(definition.getType().getName(), definition);

        }
    }

    private final ReferenceSetDefinition<? extends ReferentialDto> definition;

    public static <D extends ReferentialDto> ReferenceSetDefinition<D> getDefinition(Class<D> type) {
        ReferenceSetDefinition<D> referenceSetDefinition = MAPPING.get(type.getName());
        Objects.requireNonNull(referenceSetDefinition, "Could not find definition for type: " + type);
        return referenceSetDefinition;
    }

    public static <D extends ReferentialDto> ReferenceSetDefinition<D> getDefinition(String name) {
        ReferenceSetDefinition<D> referenceSetDefinition = MAPPING.get(name);
        Objects.requireNonNull(referenceSetDefinition, "Could not find definition for type: " + name);
        return referenceSetDefinition;
    }

    protected static <R extends ReferentialDto> ReferenceSetDefinition.Builder newDefinitionBuilder(Class<R> type) {
        ReferenceSetDefinition.Builder<R> builder = ReferenceSetDefinition.builder(type);
        if (I18nReferentialDto.class.isAssignableFrom(type)) {
            builder.addProperty(String.class, I18nReferentialDto.PROPERTY_LABEL);
        }
        return builder;
    }

    protected static <R extends ReferentialDto> ReferenceSetDefinition.Builder newDefaultDefinitionBuilder(Class<R> type) {
        ReferenceSetDefinition.Builder<R> builder = ReferenceSetDefinition
                .builder(type)
                .addProperty(String.class, ReferentialDto.PROPERTY_CODE);
        if (I18nReferentialDto.class.isAssignableFrom(type)) {
            builder.addProperty(String.class, I18nReferentialDto.PROPERTY_LABEL);
        }
        return builder;
    }

    ReferentialReferenceSetDefinitions(ReferenceSetDefinition.Builder definition) {
        this.definition = definition.build();
    }

    public <D extends ReferentialDto> ReferenceSetDefinition<D> getDefinition() {
        return (ReferenceSetDefinition<D>) definition;
    }

}
