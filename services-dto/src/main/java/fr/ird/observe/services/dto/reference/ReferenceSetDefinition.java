package fr.ird.observe.services.dto.reference;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ird.observe.services.dto.IdDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Pour définir ce que l'on doit retrouver dans un ensemble de références d'un même type.
 *
 * Created on 11/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferenceSetDefinition<D extends IdDto> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferenceSetDefinition.class);

    /**
     * Le type du dto qui doit être transformé en références.
     * {@link ReferenceSetDefinition#getType()}.
     */
    private final Class<D> type;

    private final String[] propertyNames;

    private final Class<?>[] propertyTypes;

    public static <D extends IdDto> Builder<D> builder(Class<D> type) {
        return new Builder<>(type);
    }

    protected ReferenceSetDefinition(Class<D> type, Class<?>[] propertyTypes, String... propertyNames) {
        this.type = type;
        this.propertyTypes = propertyTypes;
        this.propertyNames = propertyNames;
    }

    public Class<D> getType() {
        return type;
    }

    public Class<?>[] getPropertyTypes() {
        return propertyTypes;
    }

    public String[] getPropertyNames() {
        return propertyNames;
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper toStringHelper = MoreObjects.toStringHelper(this)
                .add("type", type.getSimpleName());
        if (log.isDebugEnabled()) {
            toStringHelper
                    .add("propertyNames", Arrays.toString(propertyNames))
                    .add("propertyTypes", Arrays.toString(propertyTypes));
        }
        return toStringHelper.toString();
    }

    public static class Builder<D extends IdDto> {

        private final Class<D> type;

        private final LinkedList<Class<?>> propertyTypes;

        private final LinkedList<String> propertyNames;

        public Builder(Class<D> type) {
            this.type = type;
            this.propertyTypes = new LinkedList<>();
            this.propertyNames = new LinkedList<>();
        }

        public <O> Builder<D> addProperty(Class<O> type, String name) {
            propertyTypes.add(type);
            propertyNames.add(name);
            return this;
        }

        public ReferenceSetDefinition<D> build() {
            return new ReferenceSetDefinition<>(type, propertyTypes.toArray(new Class[propertyTypes.size()]), propertyNames.toArray(new String[propertyNames.size()]));
        }
    }

}
