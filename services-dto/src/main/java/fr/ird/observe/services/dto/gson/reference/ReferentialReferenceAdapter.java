package fr.ird.observe.services.dto.gson.reference;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialReferenceAdapter<D extends ReferentialDto> extends AbstractReferenceAdapter<D, ReferentialReference<D>> {

    @Override
    public ReferentialReference<D> deserialize(JsonObject jsonObject, JsonDeserializationContext context, Class<D> dtoType, String[] propertyNames, Class<?>... propertyTypes) {
        ReferentialReference<D> reference = super.deserialize(jsonObject, context, dtoType, propertyNames, propertyTypes);


        JsonElement neddCommentElement = jsonObject.get(ReferentialReference.PROPERTY_NEED_COMMENT);
        if (neddCommentElement != null) {
            boolean neddComment = neddCommentElement.getAsBoolean();
            reference.setNeedComment(neddComment);
        }

        JsonElement enabledElement = jsonObject.get(ReferentialReference.PROPERTY_ENABLED);
        if (enabledElement != null) {
            boolean enabled = enabledElement.getAsBoolean();
            reference.setEnabled(enabled);
        }

        JsonElement lastUpdateElement = jsonObject.get(ReferentialReference.PROPERTY_LAST_UPDATE_DATE);
        if (lastUpdateElement != null) {
            Date lastUpdate = context.deserialize(lastUpdateElement, Date.class);
            reference.setLastUpdateDate(lastUpdate);
        }

        return reference;
    }

    @Override
    public JsonObject serialize(ReferentialReference<D> src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject jsonReference = super.serialize(src, typeOfSrc, context);

        if (src.isNeedComment()) {
            jsonReference.add(ReferentialReference.PROPERTY_NEED_COMMENT, context.serialize(src.isNeedComment()));
        }
        if (src.isEnabled()) {
            jsonReference.add(ReferentialReference.PROPERTY_ENABLED, context.serialize(src.isEnabled()));
        }
        if (src.getLastUpdateDate() != null) {
            jsonReference.add(ReferentialReference.PROPERTY_LAST_UPDATE_DATE, context.serialize(src.getLastUpdateDate()));
        }

        return jsonReference;

    }

    @Override
    protected ReferenceSetDefinition<D> getDefinition(Class<D> dtoType) {
        return ReferentialReferenceSetDefinitions.getDefinition(dtoType);
    }

    @Override
    protected ReferentialReference<D> newReference(Class<D> dtoType, String[] propertyNames, Serializable... propertyValues) {
        ReferentialReference<D> reference = new ReferentialReference<>();
        reference.init(dtoType, propertyNames, propertyValues);
        return reference;
    }

}
