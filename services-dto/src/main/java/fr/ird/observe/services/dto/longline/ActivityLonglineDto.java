package fr.ird.observe.services.dto.longline;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.util.DateUtil;

import java.util.Date;

public class ActivityLonglineDto extends AbstractActivityLonglineDto {

    private static final long serialVersionUID = 3991657324991701860L;

    public static final String PROPERTY_SET_LONGLINE = "setLongline";

    public static final String PROPERTY_DATE = "date";

    public static final String PROPERTY_TIME = "time";

    public Date getDate() {
        return timeStamp == null ? null : DateUtil.getDay(timeStamp);
    }

    public Date getTime() {
        return timeStamp == null ? null : DateUtil.getTime(timeStamp, false, false);
    }

    public void setDate(Date date) {
        if (timeStamp != null) {
            Date dateAndTime = date == null ? timeStamp : DateUtil.getDateAndTime(date, timeStamp, true, false);
            setTimeStamp(dateAndTime);
        }
    }

    public void setTime(Date time) {
        if (timeStamp != null) {
            Date dateAndTime = time == null ? timeStamp : DateUtil.getDateAndTime(timeStamp, time, false, false);
            setTimeStamp(dateAndTime);
        }
    }

    public void setTimeStamp(Date timeStamp) {
        Date oldDate = getDate();
        Date oldTime = getTime();
        super.setTimeStamp(timeStamp);
        firePropertyChange(PROPERTY_DATE, oldDate, getDate());
        firePropertyChange(PROPERTY_TIME, oldTime, getTime());
    }


} //ActivityLonglineDto
