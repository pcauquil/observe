package fr.ird.observe.services.dto.longline;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * Created on 12/12/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class SectionWithTemplate extends SectionDto {

    private static final long serialVersionUID = 1L;

    SectionTemplate sectionTemplate;

    private final SectionDto delegate;

    public SectionWithTemplate() {
        this(new SectionDto());
    }

    public SectionWithTemplate(SectionDto delegate) {
        this.delegate = delegate;
    }

    public SectionDto getDelegate() {
        return delegate;
    }

    public SectionTemplate getSectionTemplate() {
        return sectionTemplate;
    }

    public void setSectionTemplate(SectionTemplate sectionTemplate) {
        this.sectionTemplate = sectionTemplate;
    }

    @Override
    public Integer getSettingIdentifier() {
        return delegate.getSettingIdentifier();
    }

    @Override
    public void setSettingIdentifier(Integer settingIdentifier) {
        delegate.setSettingIdentifier(settingIdentifier);
    }

    @Override
    public Integer getHaulingIdentifier() {
        return delegate.getHaulingIdentifier();
    }

    @Override
    public void setHaulingIdentifier(Integer haulingIdentifier) {
        delegate.setHaulingIdentifier(haulingIdentifier);
    }

    @Override
    public BasketDto getBasket(int index) {
        return delegate.getBasket(index);
    }

    @Override
    public boolean isBasketEmpty() {
        return delegate.isBasketEmpty();
    }

    @Override
    public int sizeBasket() {
        return delegate.sizeBasket();
    }

    @Override
    public void addBasket(BasketDto basket) {
        delegate.addBasket(basket);
    }

    @Override
    public void addAllBasket(Collection<BasketDto> basket) {
        delegate.addAllBasket(basket);
    }

    @Override
    public boolean removeBasket(BasketDto basket) {
        return delegate.removeBasket(basket);
    }

    @Override
    public boolean removeAllBasket(Collection<BasketDto> basket) {
        return delegate.removeAllBasket(basket);
    }

    @Override
    public boolean containsBasket(BasketDto basket) {
        return delegate.containsBasket(basket);
    }

    @Override
    public boolean containsAllBasket(Collection<BasketDto> basket) {
        return delegate.containsAllBasket(basket);
    }

    @Override
    public LinkedHashSet<BasketDto> getBasket() {
        return delegate.getBasket();
    }

    @Override
    public void setBasket(LinkedHashSet<BasketDto> basket) {
        delegate.setBasket(basket);
    }

    @Override
    public boolean isPersisted() {
        return delegate.isPersisted();
    }

    @Override
    public boolean isNotPersisted() {
        return delegate.isNotPersisted();
    }

    @Override
    public boolean equals(Object o) {
        return delegate.equals(o);
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public void setId(String id) {
        delegate.setId(id);
    }
}
