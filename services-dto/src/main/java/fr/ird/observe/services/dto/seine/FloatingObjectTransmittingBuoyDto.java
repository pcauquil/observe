package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.services.dto.constants.seine.TypeTransmittingBuoyOperation;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;

public class FloatingObjectTransmittingBuoyDto extends AbstractFloatingObjectTransmittingBuoyDto {

    private static final long serialVersionUID = 7148959059263172961L;

    /**
     * Récupère le type d'objectOperation effectué sur les balises du DCP.
     *
     * @return l'ordinal de l'énumeration {@link TypeTransmittingBuoyOperation}
     * @see TypeTransmittingBuoyOperation
     */
    public TypeTransmittingBuoyOperation getTypeTransmittingBuoyOperation() {
        if (isTransmittingBuoyEmpty()) {

            // pas de balise lié au dcp.
            return TypeTransmittingBuoyOperation.pasDeBalise;
        }
        if (transmittingBuoy.size() == 1) {

            // une seule balise lue sur le DCP, on peut avoir les cas suivants :
            // - visite simple
            // - récuperation
            // - pose d'une nouvelle balise
            TransmittingBuoyDto balise = Iterables.get(transmittingBuoy, 0);
            ReferentialReference<TransmittingBuoyOperationDto> objectOperation = balise.getTransmittingBuoyOperation();
            String code = (String) objectOperation.getPropertyValue(TransmittingBuoyOperationDto.PROPERTY_CODE);
            switch (Integer.valueOf(code)) {
                case 1:

                    // visite
                    return TypeTransmittingBuoyOperation.visite;
                case 2:

                    // recuperation
                    return TypeTransmittingBuoyOperation.recuperation;
                case 3:

                    // mise a l'eau d'une nouvelle balise
                    return TypeTransmittingBuoyOperation.pose;
                default:
                    throw new IllegalStateException("objectOperation must be between 1 to 3, but was " + code);
            }
        }
        if (transmittingBuoy.size() == 2) {

            // deux balises lues pour le DCP, on peut avoir un seul cas :
            // - récupération et remplacement
            return TypeTransmittingBuoyOperation.recuperationEtRemplacement;
        }
        throw new IllegalStateException("on a dcp, can only have 0, 1 or 2 balise lues");

    }

}
