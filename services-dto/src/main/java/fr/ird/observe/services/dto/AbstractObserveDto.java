package fr.ird.observe.services.dto;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.application.bean.JavaBeanObject;
import org.nuiton.util.CollectionUtil;

import java.util.Collection;

/**
 * Created on 14/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 7.0
 */
public abstract class AbstractObserveDto extends AbstractSerializableBean implements JavaBeanObject {

    private static final long serialVersionUID = 1L;

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }

    protected <B> B getChild(Collection<B> child, int index) {
        return CollectionUtil.getOrNull(child, index);
    }

    @Override
    public void firePropertyChanged(String propertyName,
                                    Object oldValue,
                                    Object newValue) {
        firePropertyChange(propertyName, oldValue, newValue);
    }

}
