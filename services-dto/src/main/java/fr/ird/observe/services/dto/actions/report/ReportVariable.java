/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.dto.actions.report;

import fr.ird.observe.services.dto.ObserveDto;

import java.io.Serializable;
import java.util.Set;

/**
 * La définition d'une variable utilisable dans un report.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.7
 */
public class ReportVariable<V> implements Serializable, ObserveDto {

    public final static String PROPERTY_NAME = "name";

    public final static String PROPERTY_TYPE = "type";

    public final static String PROPERTY_REQUEST = "request";

    public final static String PROPERTY_VALUES = "values";

    public final static String PROPERTY_SELECTED_VALUE = "selectedValue";

    private static final long serialVersionUID = 1L;

    protected final String name;

    protected final Class<V> type;

    protected final String request;

    protected Set<V> values;

    protected V selectedValue;

    public ReportVariable(String name, Class<V> type, String request) {
        this.name = name;
        this.type = type;
        this.request = request;
    }

    public String getName() {
        return name;
    }

    public Class<V> getType() {
        return type;
    }

    public String getRequest() {
        return request;
    }

    public Set<V> getValues() {
        return values;
    }

    public void setValues(Set<V> values) {
        this.values = values;
    }

    public V getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(V selectedValue) {
        this.selectedValue = selectedValue;
    }
}
