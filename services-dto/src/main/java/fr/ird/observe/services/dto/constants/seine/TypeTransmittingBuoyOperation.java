/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.dto.constants.seine;

/** @author Tony Chemit - chemit@codelutin.com */
public enum TypeTransmittingBuoyOperation {
    // pas de balise lue
    pasDeBalise,

    // une balise lue
    visite("1"),
    recuperation("2"),
    pose("3"),

    // deux balises lues
    recuperationEtRemplacement("2", "3");

    /** les codes des opérations sur balise */
    private final String[] codeOperation;

    /** le count de balises lues */
    private final int nbBalises;

    TypeTransmittingBuoyOperation(String... codeOperation) {
        this.codeOperation = codeOperation;
        nbBalises = codeOperation.length;
    }

    public int getNbBalises() {
        return nbBalises;
    }

    public String[] getCodeOperation() {
        return codeOperation;
    }

    public static TypeTransmittingBuoyOperation valueOf(int ordinal)
            throws IllegalArgumentException {
        for (TypeTransmittingBuoyOperation o : values()) {
            if (o.ordinal() == ordinal) {
                return o;
            }
        }
        throw new IllegalArgumentException(
                "could not find a " +
                TypeTransmittingBuoyOperation.class.getSimpleName() +
                " value for ordinal " + ordinal);
    }
}
