/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.dto.actions.report;

import fr.ird.observe.services.dto.ObserveDto;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Dimension;
import java.awt.Point;
import java.io.Serializable;

/**
 * Une matrice de données
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class DataMatrix implements ObserveDto {

    /** Logger */
    private static final Log log = LogFactory.getLog(DataMatrix.class);

    protected Object[][] data;

    protected int width;

    protected int height;

    protected int x;

    protected int y;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Object[][] getData() {
        return data;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void createData() {
        data = new Object[height][width];
    }

    public void setData(Object[][] data) {
        this.data = data;
    }

    public void copyData(DataMatrix incoming) {

        int x = incoming.getX();
        int y = incoming.getY();

        Integer height = incoming.getHeight();
        Integer width = incoming.getWidth();

        if (log.isDebugEnabled()) {
            log.debug("copying incoming matrix (dim: " +
                      incoming.getDimension() + ", location: " +
                      incoming.getLocation() + ")");
        }

        for (int i = 0; i < width; i++) {

            for (int j = 0; j < height; j++) {

                Serializable value = incoming.getValue(i, j);
                setValue(x + i, y + j, value);
            }
        }
    }

    public Dimension getDimension() {
        return new Dimension(width, height);
    }

    public void setDimension(Dimension dim) {
        height = (int) dim.getHeight();
        width = (int) dim.getWidth();
    }

    public Point getLocation() {
        return new Point(x, y);
    }

    public void setLocation(Point location) {
        x = (int) location.getX();
        y = (int) location.getY();
    }

    public Serializable getValue(int x, int y) {
        return data == null ? null : (Serializable) data[y][x];
    }

    public void setValue(int x, int y, Object data) {

        String cellData = data == null ? null : String.valueOf(data);
        if (log.isDebugEnabled()) {
            log.debug("Put data [" + x + "," + y + "] = " + cellData);
        }
        this.data[y][x] = cellData;
    }

    public static Dimension getDimension(DataMatrix... datas) {
        int width = 0;
        int height = 0;

        for (DataMatrix request : datas) {

            int nWidth = request.getX() + request.getWidth();
            int nHeight = request.getY() + request.getHeight();
            if (nWidth > width) {
                width = nWidth;
            }
            if (nHeight > height) {
                height = nHeight;
            }
        }
        return new Dimension(width, height);
    }

    public static DataMatrix merge(DataMatrix... incomings) {
        return merge(-1, -1, incomings);
    }

    public static DataMatrix merge(int rows,
                                   int columns,
                                   DataMatrix... incomings) {
        Dimension dimension = getDimension(incomings);

        if (log.isDebugEnabled()) {
            log.debug("Merge dimension : " + dimension);
        }

        if (rows != -1) {
            int height = (int) dimension.getHeight();
            // on verifie que récupère bien le bon count de lignes
            if (rows != height) {
                if (log.isWarnEnabled()) {
                    log.warn("No matching rows number : should have " + rows +
                             ", but was " + height);
                }
            }
        }

        if (columns != -1) {
            int width = (int) dimension.getWidth();
            // on verifie que récupère bien le bon count de colonnes
            if (columns != width) {
                if (log.isWarnEnabled()) {
                    log.warn("No matching columns number : should have " +
                             columns + ", but was " + width);
                }
            }
        }

        DataMatrix result = new DataMatrix();
        result.setDimension(dimension);
        result.createData();
        for (DataMatrix incoming : incomings) {
            result.copyData(incoming);
        }
        return result;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this);
        builder.append("dimension", getDimension());
        builder.append("location", getLocation());
        return builder.toString();
    }

    public String getClipbordContent(boolean copyRowHeaders,
                                     boolean copyColumnHeaders) {

        if (getWidth() <= 0 || getHeight() <= 0) {
            return "";
        }

        StringBuilder buffer = new StringBuilder();

        char sep = '\t';
        char eol = '\n';

        for (int y = copyColumnHeaders ? 0 : 1, rows = getHeight(); y < rows; y++) {

            Serializable value;

            // nouvell ligne

            int x = copyRowHeaders ? 0 : 1;

            for (int columns = getWidth() - 1; x < columns; x++) {

                // sur chaque cellule (sauf la dernière)                
                value = getValue(x, y);
                buffer.append(value).append(sep);
            }

            // dernière cellule
            value = getValue(x, y);
            buffer.append(value);

            // fin de ligne
            buffer.append(eol);
        }
        return buffer.toString();
    }
}
