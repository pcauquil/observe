package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;

public class ReferentialDtos extends AbstractReferentialDtos {

    public static final ImmutableSet<Class<? extends ReferentialDto>> REFERENCE_COMMON_DTOS = ImmutableSet.of(
            VesselSizeCategoryDto.class,
            CountryDto.class,
            HarbourDto.class,
            VesselTypeDto.class,
            VesselDto.class,
            SpeciesGroupDto.class,
            SpeciesDto.class,
            SexDto.class,
            FpaZoneDto.class,
            SpeciesListDto.class,
            PersonDto.class,
            OceanDto.class,
            OrganismDto.class,
            LengthWeightParameterDto.class,
            ProgramDto.class,
            GearCaracteristicTypeDto.class,
            GearCaracteristicDto.class,
            GearDto.class);

    public static final ImmutableSet<Class<? extends ReferentialDto>> REFERENCE_SEINE_DTOS = ImmutableSet.of(
            VesselActivitySeineDto.class,
            SurroundingActivityDto.class,
            ReasonForNullSetDto.class,
            ReasonForNoFishingDto.class,
            SpeciesFateDto.class,
            ObjectFateDto.class,
            WeightCategoryDto.class,
            DetectionModeDto.class,
            TransmittingBuoyOperationDto.class,
            ObjectOperationDto.class,
            ReasonForDiscardDto.class,
            SpeciesStatusDto.class,
            ObservedSystemDto.class,
            TransmittingBuoyTypeDto.class,
            ObjectTypeDto.class,
            WindDto.class);

    public static final ImmutableSet<Class<? extends ReferentialDto>> REFERENCE_LONGLINE_DTOS = ImmutableSet.of(
            BaitHaulingStatusDto.class,
            BaitSettingStatusDto.class,
            BaitTypeDto.class,
            CatchFateLonglineDto.class,
            EncounterTypeDto.class,
            HealthnessDto.class,
            HookPositionDto.class,
            HookSizeDto.class,
            HookTypeDto.class,
            ItemVerticalPositionDto.class,
            ItemHorizontalPositionDto.class,
            LightsticksColorDto.class,
            LightsticksTypeDto.class,
            LineTypeDto.class,
            MaturityStatusDto.class,
            MitigationTypeDto.class,
            SensorBrandDto.class,
            SensorDataFormatDto.class,
            SensorTypeDto.class,
            SettingShapeDto.class,
            SizeMeasureTypeDto.class,
            StomacFullnessDto.class,
            TripTypeDto.class,
            VesselActivityLonglineDto.class,
            WeightMeasureTypeDto.class);

    public static final ImmutableSet<Class<? extends ReferentialDto>> REFERENCE_DTOS = ImmutableSet.<Class<? extends ReferentialDto>>builder()
            .addAll(REFERENCE_COMMON_DTOS)
            .addAll(REFERENCE_SEINE_DTOS)
            .addAll(REFERENCE_LONGLINE_DTOS)
            .build();

}
