package fr.ird.observe.services.dto.gson.reference;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;

import java.util.Date;

/**
 * Created on 23/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialReferenceSetAdapter<D extends ReferentialDto> extends AbstractReferenceSetAdapter<D, ReferentialReference<D>, ReferentialReferenceSet<D>> {

    protected final ReferentialReferenceAdapter<D> referenceAdapter = new ReferentialReferenceAdapter<>();

    @Override
    protected ReferentialReferenceSet<D> newReferenceSet(Class<D> dtoType, ImmutableSet<ReferentialReference<D>> references, JsonObject jsonObject, JsonDeserializationContext context) {

        JsonElement lastUpdateElement = jsonObject.get(ReferentialReferenceSet.PROPERTY_LAST_UPDATE);
        Date lastUpdate = null;
        if (lastUpdateElement != null) {
            lastUpdate = context.deserialize(lastUpdateElement, Date.class);
        }

        return ReferentialReferenceSet.of(dtoType, references, lastUpdate);
    }

    @Override
    protected ReferentialReference<D> deserializeReference(JsonElement referenceJsonElement, JsonDeserializationContext context, Class<D> dtoType, String[] propertyNames, Class<?>... propertyTypes) {

        return referenceAdapter.deserialize(referenceJsonElement, dtoType, context);

    }

    @Override
    protected ReferenceSetDefinition<D> getDefintion(Class<D> dtoType) {
        return referenceAdapter.getDefinition(dtoType);
    }

}
