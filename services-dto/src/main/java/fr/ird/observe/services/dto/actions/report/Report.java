/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.dto.actions.report;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.ObserveDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

public class Report implements Serializable, ObserveDto {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Report.class);

    /** l'id du report. */
    protected final String id;

    /** le libellé court du report. */
    protected final String name;

    /** la description du report. */
    protected final String description;

    /** les libellés des colonnes du report. */
    protected final String[] columnHeaders;

    /** les libellés des lignes du report. */
    protected final String[] rowHeaders;

    /** la liste des requêtes à jouer. */
    protected final ReportRequest[] requests;

    /** la liste des opérations à jouer. */
    protected final ReportOperation[] operations;

    /** la liste des variables du report. */
    protected final ReportVariable[] variables;

    /** la liste des variables de type repeat du report */
    protected final ReportVariable[] repeatVariables;

    private static final long serialVersionUID = 1L;

    public Report(String id,
                  String name,
                  String description,
                  String[] rowHeaders,
                  String[] columnHeaders,
                  ReportOperation[] operations,
                  ReportVariable[] variables,
                  ReportVariable[] repeatVariables,
                  ReportRequest... requests) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.rowHeaders = rowHeaders;
        this.columnHeaders = columnHeaders;
        this.requests = requests;
        this.operations = operations;
        this.variables = variables;
        this.repeatVariables = repeatVariables;
        if (log.isDebugEnabled()) {
            log.debug("New report [" + id + ":" + name + "], nb requests : " +
                      requests.length + ", nb objectOperations : " +
                      operations.length + ", nb variables : " +
                      variables.length + ", nb repeat variables : " +
                      repeatVariables.length);
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getRows() {
        return rowHeaders == null ? -1 : rowHeaders.length;
    }

    public int getColumns() {
        return columnHeaders == null ? -1 : columnHeaders.length;
    }

    public String[] getColumnHeaders() {
        return columnHeaders;
    }

    public String[] getRowHeaders() {
        return rowHeaders;
    }

    public ReportRequest[] getRequests() {
        return requests;
    }

    public ReportOperation[] getOperations() {
        return operations;
    }

    public ReportVariable[] getVariables() {
        return variables;
    }

    public ReportVariable[] getRepeatVariables() {
        return repeatVariables;
    }

    public boolean isVariableRequired() {
        return variables.length > 0;
    }

    @Override
    public String toString() {
        return t(name);
    }

    public ReportVariable getRepeatVariable(final String name) {
        return Iterables.find(Lists.newArrayList(repeatVariables), reportVariable -> name.equals(reportVariable.getName()));
    }

    public boolean canExecute() {

        for (ReportVariable variable : getVariables()) {

            // on verifie qu'on a bien cette variable
            String name = variable.getName();
            if (variable.getSelectedValue() == null) {

                if (log.isInfoEnabled()) {
                    log.info("variable " + name + " is missing");
                }
                return false;
            }
        }

        // le report peut-être executé
        return true;
    }

    public boolean canExecute(Map<String, Object> variables) {

        for (ReportVariable variable : getVariables()) {

            // on verifie qu'on a bien cette variable
            String name = variable.getName();
            if (variables.get(name) == null) {

                if (log.isInfoEnabled()) {
                    log.info("variable " + name + " is missing");
                }
                return false;
            }
        }

        // le report peut-être executé
        return true;
    }

}
