/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.dto.actions.report;

import com.google.common.collect.Maps;
import fr.ird.observe.services.dto.ObserveDto;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Pour caractériser une requète à lancer dans un report.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ReportRequest implements Serializable, ObserveDto {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReportRequest.class);

    public static final String TRIP_ID_VARIABLE = "tripId";

    /** le layout de la requete. */
    public enum RequestLayout {
        /** lorsque les résultats de la requète sont les lignes du résultat. */
        row,
        /** lorsque les résultats de la requète sont les colonnes du résultat. */
        column
    }

    /** Un repeater optionnel sur la requête. */
    public static class RequestRepeat implements Serializable, ObserveDto {

        protected final String variableName;

        protected final RequestLayout layout;

        private static final long serialVersionUID = 1L;

        public RequestRepeat(String variableName, RequestLayout layout) {
            this.variableName = variableName;
            this.layout = layout;
        }

        public String getVariableName() {
            return variableName;
        }

        public RequestLayout getLayout() {
            return layout;
        }

        @Override
        public String toString() {
            ToStringBuilder builder = new ToStringBuilder(this);
            builder.append("variableName", getVariableName());
            builder.append("layout", getLayout());
            return builder.toString();
        }
    }

    /** layout de la requète. */
    protected final RequestLayout layout;

    /** la requète à exécuter. */
    protected final String request;

    /** la position de la requète. */
    protected final Point location;

    /** le repeater optionel. */
    protected final RequestRepeat repeat;

    public ReportRequest(RequestLayout layout,
                         int x,
                         int y,
                         String request,
                         RequestRepeat repeat) {
        this.layout = layout;
        this.request = request;
        this.repeat = repeat;
        location = new Point(x, y);
    }

    public RequestLayout getLayout() {
        return layout;
    }

    public int getX() {
        return (int) location.getX();
    }

    public int getY() {
        return (int) location.getY();
    }

    public Point getLocation() {
        return location;
    }

    public String getRequest() {
        return request;
    }

    public RequestRepeat getRepeat() {
        return repeat;
    }

    public static Map<String, Object> extractParams(Report report, String tripId) {

        Map<String, Object> params = Maps.newHashMap();

        params.put(TRIP_ID_VARIABLE, tripId);

        for (ReportVariable variable : report.getVariables()) {

            String name = variable.getName();

            Object value = variable.getSelectedValue();

            params.put(name, value);

        }
        return params;

    }

    public static Object[] getParams(String request, Map<String, Object> params) {

        // on parcourt la liste de tous les paramètres pour savoir si on doit
        // les inclure pour la requête donnée :
        List<String> namesToUsed = new ArrayList<>();
        for (String paramName : params.keySet()) {
            if (request.contains(":" + paramName)) {
                namesToUsed.add(paramName);
            }
        }
        Object[] datas = new Object[namesToUsed.size() * 2];
        int index = 0;
        for (String name : namesToUsed) {
            Object value = params.get(name);
            datas[index * 2] = name;
            datas[index * 2 + 1] = value;
            index++;
        }
        namesToUsed.clear();
        return datas;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this);
        builder.append("request", getRequest());
        builder.append("layout", getLayout());
        builder.append("location", getLocation());
        builder.append("repeatVariable", getRepeat());
        return builder.toString();
    }
}
