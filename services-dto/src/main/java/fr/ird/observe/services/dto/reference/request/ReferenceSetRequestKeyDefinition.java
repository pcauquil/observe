package fr.ird.observe.services.dto.reference.request;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created on 10/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferenceSetRequestKeyDefinition<D extends IdDto> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferenceSetRequestKeyDefinition.class);

    private final ReferenceSetDefinition<D> referenceSetDefinition;

    private final String name;

    public ReferenceSetRequestKeyDefinition(ReferenceSetDefinition<D> referenceSetDefinition, String name) {
        this.referenceSetDefinition = referenceSetDefinition;
        this.name = name;
    }

    public Class<D> getType() {
        return referenceSetDefinition.getType();
    }

    public String getName() {
        return name;
    }

    public ReferenceSetDefinition<D> getDefinition() {
        return referenceSetDefinition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReferenceSetRequestKeyDefinition)) return false;
        ReferenceSetRequestKeyDefinition that = (ReferenceSetRequestKeyDefinition) o;
        return Objects.equals(referenceSetDefinition.getType(), that.referenceSetDefinition.getType()) &&
               Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(referenceSetDefinition.getType(), name);
    }

    public boolean isReferential() {
        Class<?> type = referenceSetDefinition.getType();
        return ReferentialDto.class.isAssignableFrom(type);
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper toStringHelper = MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("type", referenceSetDefinition.getType().getSimpleName());
        if (log.isDebugEnabled()) {
            toStringHelper
                    .add("propertyNames", Arrays.toString(referenceSetDefinition.getPropertyNames()))
                    .add("propertyTypes", Arrays.toString(referenceSetDefinition.getPropertyTypes()));
        }
        return toStringHelper.toString();
    }
}
