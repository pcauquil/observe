package fr.ird.observe.services.dto.gson.reference;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.AbstractReferenceSet;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;

import java.lang.reflect.Type;

/**
 * Note: Pour une référence, on ne sérialize pas les méta-données (noms et types des propriétés), on les récupère à la
 * désérialisation via {@link ReferenceSetDefinition}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class AbstractReferenceSetAdapter<D extends IdDto, R extends AbstractReference<D>, S extends AbstractReferenceSet<D, R>> implements JsonDeserializer<S> {

    @Override
    public final S deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        Class<D> dtoType = context.deserialize(jsonObject.get(AbstractReferenceSet.PROPERTY_TYPE), Class.class);

        ReferenceSetDefinition<D> definition = getDefintion(dtoType);

        String[] propertyNames = definition.getPropertyNames();
        Class<?>[] propertyTypes = definition.getPropertyTypes();

        JsonArray jsonPropertyValues = jsonObject.get(AbstractReferenceSet.PROPERTY_REFERENCES).getAsJsonArray();

        int referencesSize = jsonPropertyValues.size();

        ImmutableSet.Builder<R> references = ImmutableSet.builder();

        for (int i = 0; i < referencesSize; i++) {

            R reference = deserializeReference(jsonPropertyValues.get(i), context, dtoType, propertyNames, propertyTypes);
            references.add(reference);

        }

        return newReferenceSet(dtoType, references.build(), jsonObject, context);

    }

    protected abstract ReferenceSetDefinition<D> getDefintion(Class<D> dtoType);

    protected abstract R deserializeReference(JsonElement referenceJsonElement, JsonDeserializationContext context, Class<D> dtoType, String[] propertyNames, Class<?>... propertyTypes);

    protected abstract S newReferenceSet(Class<D> dtoType, ImmutableSet<R> references, JsonObject jsonObject, JsonDeserializationContext context);

}
