package fr.ird.observe.services.dto.referential;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.SetMultimap;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * @author smaisonneuve
 * Created on 17/08/16.
 */
public class ReferentialMultimap<K extends ReferentialDto> implements SetMultimap<Class<K>, K> {

    private ImmutableSetMultimap<Class<K>, K> immutableSetMultimap;

    public static <K extends ReferentialDto, V> ReferentialMultimap<K> of() {
        return new ReferentialMultimap();
    }

    public static <K extends ReferentialDto> ReferentialMultimap<K> of(Class<K> k1, K v1) {
        return new ReferentialMultimap<K>(k1, v1);
    }

    public static <K extends ReferentialDto> ReferentialMultimap<K> copyOf(Multimap<Class<K>, K> multimap) {
        return new ReferentialMultimap(multimap);
    }

    public static <K extends ReferentialDto> Builder<K> builder() {
        return new Builder<>();
    }

    public static class Builder<K extends ReferentialDto> {
        private final ImmutableSetMultimap.Builder builder =  ImmutableSetMultimap.builder();

        public <K extends ReferentialDto> Builder add(K referentialDto) {
            Objects.requireNonNull(referentialDto, "Can't add a null object in ReferentialMultimap");
            builder.put(referentialDto.getClass(), referentialDto);
            return this;
        }

        public <K extends ReferentialDto> Builder put(Class<K> dtoType, K referentialDto) {
            builder.putAll(dtoType, referentialDto);
            return this;
        }

        public <K extends ReferentialDto> Builder putAll(Class<K> dtoType, Collection<K> values) {
            builder.putAll(dtoType, values);
            return this;
        }

        public ReferentialMultimap<K> build() {
            return copyOf(builder.build());
        }
    }

    private ReferentialMultimap() {
        immutableSetMultimap = ImmutableSetMultimap.<Class<K>, K>builder().build();
    }

    private ReferentialMultimap(Class<K> k1, K v1) {
        immutableSetMultimap = ImmutableSetMultimap.of(k1, v1);
    }

    private ReferentialMultimap(Multimap<Class<K>, K> multimap) {
        immutableSetMultimap = ImmutableSetMultimap.copyOf(multimap);
    }

    @Override
    public ImmutableSet<K> get(Class<K> key) {
        return immutableSetMultimap.get(key);
    }

    @Override
    public ImmutableSet<Class<K>> keySet() {
        return immutableSetMultimap.keySet();
    }

    @Override
    public Multiset<Class<K>> keys() {
        return immutableSetMultimap.keys();
    }

    @Override
    public Collection<K> values() {
        return immutableSetMultimap.values();
    }

    @Override
    public ImmutableSet<K> removeAll(Object key) {
        return immutableSetMultimap.removeAll(key);
    }

    @Override
    public void clear() {
        immutableSetMultimap.clear();
    }

    @Override
    public int size() {
        return immutableSetMultimap.size();
    }

    @Override
    public boolean isEmpty() {
        return immutableSetMultimap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return immutableSetMultimap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return immutableSetMultimap.containsValue(value);
    }

    @Override
    public boolean containsEntry(Object key, Object value) {
        return immutableSetMultimap.containsEntry(key, value);
    }

    @Override
    public boolean put(Class<K> key, K value) {
        return immutableSetMultimap.put(key, value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return immutableSetMultimap.remove(key, value);
    }

    @Override
    public boolean putAll(Class<K> key, Iterable<? extends K> values) {
        return immutableSetMultimap.putAll(key, values);
    }

    @Override
    public boolean putAll(Multimap<? extends Class<K>, ? extends K> multimap) {
        return immutableSetMultimap.putAll(multimap);
    }

    @Override
    public ImmutableSet<K> replaceValues(Class<K> key, Iterable<? extends K> values) {
        return immutableSetMultimap.replaceValues(key, values);
    }

    @Override
    public ImmutableSet<Map.Entry<Class<K>, K>> entries() {
        return immutableSetMultimap.entries();
    }

    @Override
    public ImmutableMap<Class<K>, Collection<K>> asMap() {
        return immutableSetMultimap.asMap();
    }
}
