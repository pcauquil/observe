package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import org.nuiton.util.DateUtil;

import java.util.Date;

public class ActivitySeineDto extends AbstractActivitySeineDto {

    public static final String ACTIVITY_FIN_DE_VEILLE = "16";

    public static final String ACTIVITY_DEBUT_DE_PECHE = "7";

    public static final String ACTIVITY_FIN_DE_PECHE = "6";

    public static final String PROPERTY_SET_SEINE = "setSeine";

    private static final long serialVersionUID = 3846974823980413495L;

    public boolean isActivityFinDeVeille() {
        return vesselActivitySeine != null
                && ACTIVITY_FIN_DE_VEILLE.equals(
                    vesselActivitySeine.getPropertyValue(VesselActivitySeineDto.PROPERTY_CODE));
    }

    public Date getTimeSecond() {
        return DateUtil.getTime(time, false, false);
    }
}
