package fr.ird.observe.services.dto.referential;

/*
 * #%L
 * ObServe :: Services model
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.AbstractReferenceSet;

import java.util.Date;

public class ReferentialReferenceSet<R extends ReferentialDto> extends AbstractReferenceSet<R, ReferentialReference<R>> {

    public static final String PROPERTY_LAST_UPDATE = "lastUpdate";

    private static final long serialVersionUID = 1L;

    protected Date lastUpdate;

    public static <R extends ReferentialDto> ReferentialReferenceSet<R> of(Class<R> type,
                                                                           ImmutableSet<ReferentialReference<R>> references,
                                                                           Date lastUpdate) {

        return new ReferentialReferenceSet<>(type, references, lastUpdate);

    }

    protected ReferentialReferenceSet(Class<R> type, ImmutableSet<ReferentialReference<R>> references, Date lastUpdate) {
        super(type, references);
        this.lastUpdate = lastUpdate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add(PROPERTY_TYPE, type.getName())
                .add(PROPERTY_LAST_UPDATE, lastUpdate)
                .toString();
    }
}
