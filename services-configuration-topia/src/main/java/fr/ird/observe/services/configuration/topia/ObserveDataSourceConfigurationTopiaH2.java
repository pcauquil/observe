package fr.ird.observe.services.configuration.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Configuration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created on 19/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveDataSourceConfigurationTopiaH2 extends ObserveDataSourceConfigurationTopiaSupport {

    private static final long serialVersionUID = 1L;

    /**
     * Le répertoire où se trouve la base (le nom de la base est {@code obstuna}).
     */
    private File directory;

    /**
     * Le nom de la base à ouvrir.
     */
    private String dbName;

    @Override
    public boolean isH2Database() {
        return true;
    }

    @Override
    public boolean isPostgresDatabase() {
        return false;
    }

    public File getDatabaseFile() {
        return new File(directory, dbName + ".h2.db");
    }

    public File getLockFile() {
        return new File(directory, dbName + ".lock.db");
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public File getDirectory() {
        return directory;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObserveDataSourceConfigurationTopiaH2)) return false;
        ObserveDataSourceConfigurationTopiaH2 that = (ObserveDataSourceConfigurationTopiaH2) o;
        return Objects.equals(directory, that.directory) &&
                Objects.equals(dbName, that.dbName) &&
                Objects.equals(getUsername(), that.getUsername()) &&
                Arrays.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(directory, dbName, getUsername(), getPassword());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("directory", directory)
                          .add("dbName", dbName)
                          .add("username", getUsername())
                          .add("password", "***")
                          .toString();
    }

    @Override
    public ObserveDataSourceConfigurationTopiaH2 clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfigurationTopiaH2) super.clone();
    }

}
