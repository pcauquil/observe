package fr.ird.observe.test.spi;

/*-
 * #%L
 * ObServe :: Test
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 04/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public enum DatabaseClassifier {

    /**
     * classifier par défault (si aucun classifier n'est défini de manière explicite).
     */
    DEFAULT,
    /**
     * classifier utilisé lors des tests de synchronisation où une seconde source de données est requies.
     */
    CENTRAL
}
