package fr.ird.observe.test;

/*-
 * #%L
 * ObServe :: Test
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.net.MalformedURLException;
import java.net.URL;

import static fr.ird.observe.test.ObserveTestResources.getTestProperties;

/**
 * Created on 29/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveTestConfiguration {

    public static final String MODEL_VERSION = "observetest.model.version";
    public static final String PREVIOUS_VERSION = "observetest.previous.model.version";
    public static final String FIRST_VERSION = "observetest.first.model.version";
    public static final String FIRST_VERSION_FOR_REF_SYNCHRO = "observetest.first.model.for.ref.synchro.version";
    public static final String H2_LOGIN = "observetest.h2.login";
    public static final String H2_PASSWORD = "observetest.h2.password";
    public static final String WEB_LOGIN = "observetest.web.login";
    public static final String WEB_PASSWORD = "observetest.web.password";
    public static final String WEB_URL = "observetest.web.url";
    public static final String OBSTUNA_URL = "observetest.obstuna.url";
    public static final String OBSTUNA_LOGIN = "observetest.obstuna.login";
    public static final String OBSTUNA_PASSWORD = "observetest.obstuna.password";
    public static final String OBSTUNA_ADMIN_LOGIN = "observetest.obstuna.admin.login";
    public static final String OBSTUNA_TECHNICIEN_LOGIN = "observetest.obstuna.technicien.login";
    public static final String OBSTUNA_UTILISATEUR_LOGIN = "observetest.obstuna.utilisateur.login";
    public static final String OBSTUNA_REFERENTIEL_LOGIN = "observetest.obstuna.referentiel.login";

    public static Version getModelVersion() {
        return getVersion(MODEL_VERSION);
    }

    public static Version getPreviousModelVersion() {
        return getVersion(PREVIOUS_VERSION);
    }

    public static Version getFirstModelVersion() {
        return getVersion(FIRST_VERSION);
    }

    public static Version getVersion(String propertyName) {
        return Versions.valueOf(getTestProperties().getProperty(propertyName));
    }

    public static String getLogin(String propertyName) {
        return getTestProperties().getProperty(propertyName);
    }

    public static URL getUrl(String propertyName) {
        String property = null;
        try {
            property = getTestProperties().getProperty(propertyName);
            return new URL(property);
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Can't init with url: " + property);
        }
    }

    public static char[] getPassword(String propertyName) {
        return getTestProperties().getProperty(propertyName).toCharArray();
    }

//    public static DatabaseNameConfiguration getDatabaseNameConfigurationAnnotation(Method testClassMethod, DatabaseClassifier classifier) {
//
//        DatabaseNameConfiguration[] annotations = testClassMethod.getAnnotationsByType(DatabaseNameConfiguration.class);
//
//        for (DatabaseNameConfiguration annotation : annotations) {
//            if (classifier.equals(annotation.classifier())) {
//                return annotation;
//            }
//        }
//        return null;
//    }

//    public static DatabaseVersionConfiguration getDatabaseVersionConfigurationAnnotation(Method testClassMethod, DatabaseClassifier classifier) {
//
//        DatabaseVersionConfiguration[] annotations = testClassMethod.getAnnotationsByType(DatabaseVersionConfiguration.class);
//
//        for (DatabaseVersionConfiguration annotation : annotations) {
//            if (classifier.equals(annotation.classifier())) {
//                return annotation;
//            }
//        }
//        return null;
//    }
//
//    public static CopyDatabaseConfiguration getCopyDatabaseConfigurationAnnotation(Method testClassMethod, DatabaseClassifier classifier) {
//
//        CopyDatabaseConfiguration[] annotations = testClassMethod.getAnnotationsByType(CopyDatabaseConfiguration.class);
//
//        for (CopyDatabaseConfiguration annotation : annotations) {
//            if (classifier.equals(annotation.classifier())) {
//                return annotation;
//            }
//        }
//        return null;
//    }

    public static DatabaseUrlConfiguration getDatabaseUrlConfigurationAnnotation(AnnotatedElement annotatedElement) {

        return getAnnotation(annotatedElement, DatabaseUrlConfiguration.class);

    }

    public static DatabaseLoginConfiguration getDatabaseLoginConfigurationAnnotation(AnnotatedElement annotatedElement) {

        return getAnnotation(annotatedElement, DatabaseLoginConfiguration.class);

    }

    public static DatabasePasswordConfiguration getDatabasePasswordConfigurationAnnotation(AnnotatedElement annotatedElement) {

        return getAnnotation(annotatedElement, DatabasePasswordConfiguration.class);

    }

    public static DatabaseNameConfiguration getDatabaseNameConfigurationAnnotation(AnnotatedElement testClass, DatabaseClassifier classifier) {

        DatabaseNameConfiguration[] annotations = getAnnotations(testClass, DatabaseNameConfiguration.class);

        for (DatabaseNameConfiguration annotation : annotations) {
            if (classifier.equals(annotation.classifier())) {
                return annotation;
            }
        }
        return null;
    }

    public static DatabaseVersionConfiguration getDatabaseVersionConfigurationAnnotation(AnnotatedElement testClass, DatabaseClassifier classifier) {

        DatabaseVersionConfiguration[] annotations = getAnnotations(testClass, DatabaseVersionConfiguration.class);

        for (DatabaseVersionConfiguration annotation : annotations) {
            if (classifier.equals(annotation.classifier())) {
                return annotation;
            }
        }
        return null;
    }

    public static CopyDatabaseConfiguration getCopyDatabaseConfigurationAnnotation(AnnotatedElement testClass, DatabaseClassifier classifier) {

        CopyDatabaseConfiguration[] annotations = getAnnotations(testClass, CopyDatabaseConfiguration.class);

        for (CopyDatabaseConfiguration annotation : annotations) {
            if (classifier.equals(annotation.classifier())) {
                return annotation;
            }
        }
        return null;
    }

    private static <E extends Annotation> E[] getAnnotations(AnnotatedElement clazz, Class<E> annotationType) {

        E[] annotations = clazz.getAnnotationsByType(annotationType);

        if (annotations.length == 0 && clazz instanceof Class && ((Class) clazz).getSuperclass() != null) {
            annotations = getAnnotations(((Class) clazz).getSuperclass(), annotationType);
        }
        return annotations;
    }

    private static <E extends Annotation> E getAnnotation(AnnotatedElement clazz, Class<E> annotationType) {

        E annotation = clazz.getAnnotation(annotationType);

        if (annotation == null && clazz instanceof Class && ((Class) clazz).getSuperclass() != null) {
            annotation = getAnnotation(((Class) clazz).getSuperclass(), annotationType);
        }
        return annotation;
    }
}
