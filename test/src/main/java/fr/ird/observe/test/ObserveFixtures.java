package fr.ird.observe.test;

/*-
 * #%L
 * ObServe :: Test
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.mutable.MutableLong;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 29/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveFixtures {

    public static final String TRIP_SEINE_ID_1 = "fr.ird.observe.entities.seine.TripSeine#1359167789871#0.6765335978809843";
    public static final String TRIP_SEINE_ID_2 = "fr.ird.observe.entities.seine.TripSeine#1359280279156#0.41771067982188215";
    public static final String TRIP_SEINE_ID_3 = "fr.ird.observe.entities.seine.TripSeine#1360156698296#0.6097793743126777";

    public static final ImmutableSet<String> TRIP_SEINE_IDS = ImmutableSet.of(
            TRIP_SEINE_ID_1,
            TRIP_SEINE_ID_2,
            TRIP_SEINE_ID_3
    );

    public static final String PROGRAM_ID = "fr.ird.observe.entities.referentiel.Program#1239832686262#0.31033946454061234";

    public static final String TRIP_LONGLINE_ID_1 = "fr.ird.observe.entities.longline.TripLongline#1429538088091#0.763886003987864";

    public static final String TRIP_LONGLINE_ID_2 = "fr.ird.observe.entities.longline.TripLongline#1429540363472#0.517173705156893";

    public static final String TRIP_LONGLINE_ID_3 = "fr.ird.observe.entities.longline.TripLongline#1429537115030#0.493692863034084";

    public static final ImmutableSet<String> TRIP_LONGLINE_IDS = ImmutableSet.of(
            TRIP_LONGLINE_ID_1,
            TRIP_LONGLINE_ID_2,
            TRIP_LONGLINE_ID_3
    );

    public static final String SET_LONGLINE_ID_1 = "fr.ird.observe.entities.longline.SetLongline#1429538714446#0.0876020351424813";

    public static final ImmutableMap<String, Long> REFERENTIAL_COMMON_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_common.country", 46L)
            .put("observe_common.fpazone", 41L)
            .put("observe_common.gear", 26L)
            .put("observe_common.gear_gearcaracteristic", 29L)
            .put("observe_common.gearcaracteristic", 22L)
            .put("observe_common.gearcaracteristictype", 6L)
            .put("observe_common.harbour", 72L)
            .put("observe_common.lastupdatedate", 95L)
            .put("observe_common.lengthweightparameter", 352L)
            .put("observe_common.ocean", 3L)
            .put("observe_common.ocean_species", 359L)
            .put("observe_common.organism", 6L)
            .put("observe_common.person", 344L)
            .put("observe_common.program", 23L)
            .put("observe_common.sex", 5L)
            .put("observe_common.species", 211L)
            .put("observe_common.species_specieslist", 255L)
            .put("observe_common.speciesgroup", 8L)
            .put("observe_common.specieslist", 5L)
            .put("observe_common.vessel", 906L)
            .put("observe_common.vesselsizecategory", 13L)
            .put("observe_common.vesseltype", 14L)
            .build();

    public static final ImmutableMap<String, Long> REFERENTIAL_SEINE_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_seine.detectionmode", 10L)
            .put("observe_seine.objectfate", 9L)
            .put("observe_seine.objectoperation", 4L)
            .put("observe_seine.objecttype", 23L)
            .put("observe_seine.observedsystem", 21L)
            .put("observe_seine.reasonfordiscard", 5L)
            .put("observe_seine.reasonfornofishing", 13L)
            .put("observe_seine.reasonfornullset", 10L)
            .put("observe_seine.speciesfate", 9L)
            .put("observe_seine.speciesstatus", 3L)
            .put("observe_seine.surroundingactivity", 8L)
            .put("observe_seine.transmittingbuoyoperation", 3L)
            .put("observe_seine.transmittingbuoytype", 12L)
            .put("observe_seine.vesselactivity", 23L)
            .put("observe_seine.weightcategory", 101L)
            .put("observe_seine.wind", 13L)
            .build();

    public static final ImmutableMap<String, Long> REFERENTIAL_LONGLINE_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_longline.baithaulingstatus", 4L)
            .put("observe_longline.baitsettingstatus", 4L)
            .put("observe_longline.baittype", 10L)
            .put("observe_longline.catchfate", 6L)
            .put("observe_longline.encountertype", 12L)
            .put("observe_longline.healthness", 6L)
            .put("observe_longline.hookposition", 13L)
            .put("observe_longline.hooksize", 21L)
            .put("observe_longline.hooktype", 12L)
            .put("observe_longline.itemverticalposition", 3L)
            .put("observe_longline.itemhorizontalposition", 3L)
            .put("observe_longline.lightstickscolor", 6L)
            .put("observe_longline.lightstickstype", 2L)
            .put("observe_longline.linetype", 8L)
            .put("observe_longline.maturitystatus", 12L)
            .put("observe_longline.mitigationtype", 15L)
            .put("observe_longline.sensorbrand", 4L)
            .put("observe_longline.sensordataformat", 2L)
            .put("observe_longline.sensortype", 4L)
            .put("observe_longline.settingshape", 5L)
            .put("observe_longline.sizemeasuretype", 18L)
            .put("observe_longline.stomacfullness", 6L)
            .put("observe_longline.triptype", 3L)
            .put("observe_longline.vesselactivity", 5L)
            .put("observe_longline.weightmeasuretype", 3L)
            .build();

    public static final ImmutableMap<String, Long> REFERENTIAL_TABLES_COUNT = mergeResults(
            REFERENTIAL_COMMON_TABLES_COUNT,
            REFERENTIAL_LONGLINE_TABLES_COUNT,
            REFERENTIAL_SEINE_TABLES_COUNT
    );

    public static final ImmutableMap<String, Long> TRIP_SEINE_1_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_seine.trip", 1L)
            .put("observe_seine.gearusefeatures", 2L)
            .put("observe_seine.gearusefeaturesmeasurement", 4L)
            .put("observe_seine.route", 34L)
            .put("observe_seine.set", 35L)
            .put("observe_seine.activity", 862L)
            .put("observe_seine.floatingobject", 54L)
            .put("observe_seine.schoolestimate", 27L)
            .put("observe_seine.nontargetcatch", 109L)
            .put("observe_seine.nontargetsample", 20L)
            .put("observe_seine.nontargetlength", 767L)
            .put("observe_seine.targetcatch", 73L)
            .put("observe_seine.targetsample", 10L)
            .put("observe_seine.targetlength", 85L)
            .put("observe_seine.objectobservedspecies", 3L)
            .put("observe_seine.objectschoolestimate", 1L)
            .put("observe_seine.transmittingbuoy", 62L)
            .put("observe_seine.activity_observedsystem", 1182L)
            .build();

    public static final ImmutableMap<String, Long> TRIP_SEINE_2_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_seine.trip", 1L)
            .put("observe_seine.gearusefeatures", 0L)
            .put("observe_seine.gearusefeaturesmeasurement", 0L)
            .put("observe_seine.route", 46L)
            .put("observe_seine.set", 42L)
            .put("observe_seine.activity", 657L)
            .put("observe_seine.activity_observedsystem", 652L)
            .put("observe_seine.floatingobject", 50L)
            .put("observe_seine.schoolestimate", 10L)
            .put("observe_seine.nontargetcatch", 112L)
            .put("observe_seine.nontargetsample", 19L)
            .put("observe_seine.nontargetlength", 242L)
            .put("observe_seine.targetcatch", 73L)
            .put("observe_seine.targetsample", 0L)
            .put("observe_seine.targetlength", 0L)
            .put("observe_seine.objectobservedspecies", 2L)
            .put("observe_seine.objectschoolestimate", 3L)
            .put("observe_seine.transmittingbuoy", 68L)
            .build();

    public static final ImmutableMap<String, Long> TRIP_SEINE_3_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_seine.trip", 1L)
            .put("observe_seine.gearusefeatures", 0L)
            .put("observe_seine.gearusefeaturesmeasurement", 0L)
            .put("observe_seine.route", 45L)
            .put("observe_seine.set", 35L)
            .put("observe_seine.activity", 816L)
            .put("observe_seine.floatingobject", 41L)
            .put("observe_seine.schoolestimate", 35L)
            .put("observe_seine.nontargetcatch", 104L)
            .put("observe_seine.nontargetsample", 16L)
            .put("observe_seine.nontargetlength", 1193L)
            .put("observe_seine.targetcatch", 118L)
            .put("observe_seine.targetsample", 14L)
            .put("observe_seine.targetlength", 53L)
            .put("observe_seine.objectobservedspecies", 0L)
            .put("observe_seine.objectschoolestimate", 24L)
            .put("observe_seine.transmittingbuoy", 39L)
            .put("observe_seine.activity_observedsystem", 321L)
            .build();

    public static final ImmutableMap<String, Long> ALL_TRIP_SEINE_COUNT = mergeResults(
            TRIP_SEINE_1_TABLES_COUNT,
            TRIP_SEINE_2_TABLES_COUNT,
            TRIP_SEINE_3_TABLES_COUNT
    );

    public static final ImmutableMap<String, Long> TRIP_LONGLINE_1_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_longline.trip", 1L)
            .put("observe_longline.set", 13L)
            .put("observe_longline.activity", 13L)
            .put("observe_longline.gearusefeatures", 0L)
            .put("observe_longline.gearusefeaturesmeasurement", 0L)
            .put("observe_longline.encounter", 27L)
            .put("observe_longline.sensorused", 0L)
            .put("observe_longline.tdr", 0L)
            .put("observe_longline.baitscomposition", 13L)
            .put("observe_longline.floatlinescomposition", 65L)
            .put("observe_longline.hookscomposition", 39L)
            .put("observe_longline.branchlinescomposition", 13L)
            .put("observe_longline.section", 234L)
            .put("observe_longline.basket", 3042L)
            .put("observe_longline.branchline", 18252L)
            .put("observe_longline.catch", 435L)
            .put("observe_longline.catch_predator", 49L)
            .put("observe_longline.sizemeasure", 340L)
            .put("observe_longline.weightmeasure", 0L)
            .put("observe_longline.tdrrecord", 0L)
            .put("observe_longline.species_tdr", 0L)
            .put("observe_longline.mitigationtype_set", 0L)
            .build();

    public static final ImmutableMap<String, Long> TRIP_LONGLINE_2_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_longline.trip", 1L)
            .put("observe_longline.set", 5L)
            .put("observe_longline.activity", 5L)
            .put("observe_longline.gearusefeatures", 0L)
            .put("observe_longline.gearusefeaturesmeasurement", 0L)
            .put("observe_longline.encounter", 3L)
            .put("observe_longline.sensorused", 0L)
            .put("observe_longline.tdr", 0L)
            .put("observe_longline.baitscomposition", 10L)
            .put("observe_longline.floatlinescomposition", 0L)
            .put("observe_longline.hookscomposition", 10L)
            .put("observe_longline.branchlinescomposition", 2L)
            .put("observe_longline.section", 95L)
            .put("observe_longline.basket", 1140L)
            .put("observe_longline.branchline", 6840L)
            .put("observe_longline.catch", 152L)
            .put("observe_longline.sizemeasure", 116L)
            .put("observe_longline.weightmeasure", 0L)
            .put("observe_longline.tdrrecord", 0L)
            .put("observe_longline.species_tdr", 0L)
            .put("observe_longline.mitigationtype_set", 0L)
            .put("observe_longline.catch_predator", 9L)
            .build();

    public static final ImmutableMap<String, Long> TRIP_LONGLINE_3_TABLES_COUNT = ImmutableMap
            .<String, Long>builder()
            .put("observe_longline.trip", 1L)
            .put("observe_longline.set", 14L)
            .put("observe_longline.activity", 14L)
            .put("observe_longline.gearusefeatures", 0L)
            .put("observe_longline.gearusefeaturesmeasurement", 0L)
            .put("observe_longline.encounter", 12L)
            .put("observe_longline.sensorused", 0L)
            .put("observe_longline.tdr", 0L)
            .put("observe_longline.baitscomposition", 14L)
            .put("observe_longline.floatlinescomposition", 36L)
            .put("observe_longline.hookscomposition", 42L)
            .put("observe_longline.branchlinescomposition", 14L)
            .put("observe_longline.section", 243L)
            .put("observe_longline.basket", 3229L)
            .put("observe_longline.branchline", 19374L)
            .put("observe_longline.catch", 677L)
            .put("observe_longline.mitigationtype_set", 0L)
            .put("observe_longline.sizemeasure", 647L)
            .put("observe_longline.weightmeasure", 0L)
            .put("observe_longline.catch_predator", 25L)
            .put("observe_longline.tdrrecord", 0L)
            .put("observe_longline.species_tdr", 0L)
            .build();

    public static final ImmutableMap<String, Long> ALL_TRIP_LONGLINE_COUNT = mergeResults(
            TRIP_LONGLINE_1_TABLES_COUNT,
            TRIP_LONGLINE_2_TABLES_COUNT,
            TRIP_LONGLINE_3_TABLES_COUNT
    );

    public static final String TEST_H2_LOGIN = "sa";
    public static final char[] TEST_H2_PASSWORD = "sa".toCharArray();
    public static final String TEST_REMOTE_URL = "jdbc:postgresql://localhost/obstuna-test";
    //public static final String TEST_REMOTE_URL = "jdbc:postgresql://demo.codelutin.com/obstuna";
    public static final String TEST_REMOTE_ADMIN_LOGIN = "admin";
    public static final String TEST_REMOTE_UTILISATEUR_LOGIN = "utilisateur";
    public static final String TEST_REMOTE_REFERENTIEL_LOGIN = "referentiel";
    public static final String[] TEST_REMOTE_TECHNICIENS_LOGIN = {"technicien1", "technicien2", "technicien3"};
    public static final char[] TEST_REMOTE_PASSWORD = "a".toCharArray();
    public static final String GEAR_USE_FEATURES_SEINE_ID = "fr.ird.observe.entities.seine.GearUseFeaturesSeine#1440486230661#0.42614931015885216";
    public static final String GEAR_USE_FEATURES_SEINE_ID_1 = "fr.ird.observe.entities.seine.GearUseFeaturesSeine#1440486230661#0.42614931015885216";
    public static final String GEAR_USE_FEATURES_SEINE_ID_2 = "fr.ird.observe.entities.seine.GearUseFeaturesSeine#1440486387658#0.5629816198069391";
    public static final String SPECIES_GROUP_ID = "fr.ird.observe.entities.referentiel.SpeciesGroup#1239832683689#0.7120116158620075";
    public static String SET_SEINE_ID = "fr.ird.observe.entities.seine.SetSeine#1359573677233#0.016313003525718517";


    @SafeVarargs
    protected static ImmutableMap<String, Long> mergeResults(ImmutableMap<String, Long>... expectedTablesCounts) {
        TreeMap<String, MutableLong> expectedTablesCountTmp = new TreeMap<>();
        for (ImmutableMap<String, Long> tablesCounts : expectedTablesCounts) {

            for (Map.Entry<String, Long> entry : tablesCounts.entrySet()) {
                String key = entry.getKey();
                Long value = entry.getValue();
                MutableLong count = expectedTablesCountTmp.get(key);
                if (count == null) {
                    count = new MutableLong(value);
                    expectedTablesCountTmp.put(key, count);

                } else {
                    count.add(value);
                }
            }
        }
        ImmutableMap.Builder<String, Long> expectedTablesCountBuilder = ImmutableMap.builder();
        for (Map.Entry<String, MutableLong> entry : expectedTablesCountTmp.entrySet()) {
            expectedTablesCountBuilder.put(entry.getKey(), entry.getValue().longValue());
        }
        return expectedTablesCountBuilder.build();
    }

}
