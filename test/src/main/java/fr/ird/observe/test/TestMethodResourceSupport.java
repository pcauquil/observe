package fr.ird.observe.test;

/*
 * #%L
 * ObServe :: Test
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.nuiton.version.Version;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class TestMethodResourceSupport<A extends TestClassResourceSupport> implements TestRule {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TestMethodResourceSupport.class);

    protected final A testClassResource;

    private File testDirectory;
    private String login;
    private char[] password;
    protected Method testClassMethod;
    private URL url;
    private Class<?> testClass;
    private String methodName;
    private String dbName;
    private Version dbVersion;

    protected TestMethodResourceSupport(A testClassResource) {
        this.testClassResource = testClassResource;
    }

    @Override
    public final Statement apply(final Statement base, final Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                before(description);
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    public String getDbName() {
        return dbName;
    }

    public Version getDbVersion() {
        return dbVersion;
    }

    public File getTestDirectory() {
        return testDirectory;
    }

    public String getLogin() {
        return login;
    }

    public char[] getPassword() {
        return password;
    }

    public URL getUrl() {
        return url;
    }

    protected void before(Description description) throws Throwable {

        testClass = description.getTestClass();
        methodName = description.getMethodName();
        if (log.isDebugEnabled()) {
            log.debug("Starts " + testClass.getName() + "::" + methodName);
        }

        testDirectory = testClassResource.newFile(methodName);

        Path temporaryDirectoryRoot = new File(testDirectory, "tmp").toPath();

        Files.createDirectories(temporaryDirectoryRoot);

        testClassResource.setTemporaryDirectoryRoot(temporaryDirectoryRoot);

        DatabaseLoginConfiguration databaseLogin = ObserveTestConfiguration.getDatabaseLoginConfigurationAnnotation(testClass);
        if (databaseLogin == null) {
            login = testClassResource.getLogin();
        } else {
            login = ObserveTestConfiguration.getLogin(databaseLogin.value());
        }

        DatabasePasswordConfiguration databasePassword = ObserveTestConfiguration.getDatabasePasswordConfigurationAnnotation(testClass);
        if (databasePassword == null) {
            password = testClassResource.getPassword();
        } else {
            password = ObserveTestConfiguration.getPassword(databasePassword.value());
        }

        testClassMethod = testClass.getMethod(methodName);

        DatabaseNameConfiguration databaseNameConfiguration = ObserveTestConfiguration.getDatabaseNameConfigurationAnnotation(testClassMethod, testClassResource.getClassifier());
        if (databaseNameConfiguration != null) {
            dbName = databaseNameConfiguration.value().name();
        } else {
            dbName = testClassResource.getDbName();
        }
        if (Strings.isNullOrEmpty(dbName)) {
            dbName = null;
        }

        DatabaseVersionConfiguration databaseVersionConfiguration = ObserveTestConfiguration.getDatabaseVersionConfigurationAnnotation(testClassMethod, testClassResource.getClassifier());
        if (databaseVersionConfiguration != null) {
            dbVersion = ObserveTestConfiguration.getVersion(databaseVersionConfiguration.value());
        } else {
            dbVersion = testClassResource.getDbVersion();
        }

        DatabaseUrlConfiguration databaseUrl = ObserveTestConfiguration.getDatabaseUrlConfigurationAnnotation(testClassMethod);
        if (databaseUrl == null) {
            url = testClassResource.getUrl();
        } else {
            url = ObserveTestConfiguration.getUrl(databaseUrl.value());
        }
    }


    protected void after(Description description) {

        if (log.isDebugEnabled()) {
            log.debug("Ends " + testClass.getName() + "::" + methodName);
        }

        testClassResource.setTemporaryDirectoryRoot(null);

    }

}
