package fr.ird.observe.test;

/*-
 * #%L
 * ObServe :: Test
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;

/**
 * Created on 29/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class TestClassResourceSupport implements TestRule {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TestClassResourceSupport.class);

    private final DatabaseClassifier classifier;
    protected Path temporaryDirectoryRoot;

    private File testDirectory;
    private String login;
    private char[] password;
    private String dbName;
    private Version dbVersion;
    private URL url;

    protected TestClassResourceSupport(DatabaseClassifier classifier) {
        this.classifier = classifier;
    }

    public String getScriptPath(String classifier, Version databaseVersion) {
        return ObserveTestResources.getBackupScript(databaseVersion, classifier);
    }

    public final void setTemporaryDirectoryRoot(Path temporaryDirectoryRoot) {
        this.temporaryDirectoryRoot = temporaryDirectoryRoot;
    }

    public Path getTemporaryDirectoryRoot() {
        return temporaryDirectoryRoot;
    }

    public String getLogin() {
        return login;
    }

    public char[] getPassword() {
        return password;
    }

    public String getDbName() {
        return dbName;
    }

    public Version getDbVersion() {
        return dbVersion;
    }

    public DatabaseClassifier getClassifier() {
        return classifier;
    }

    @Override
    public final Statement apply(final Statement base, final Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                before(description);
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    protected void before(Description description) throws IOException {

        Class<?> testClass = description.getTestClass();
        if (log.isDebugEnabled()) {
            log.debug("Starts " + testClass.getName());
        }

        testDirectory = TestHelper.getTestBasedir(testClass);

        DatabaseLoginConfiguration databaseLogin = ObserveTestConfiguration.getDatabaseLoginConfigurationAnnotation(testClass);
        if (databaseLogin != null) {
            login = ObserveTestConfiguration.getLogin(databaseLogin.value());
        }

        DatabasePasswordConfiguration databasePassword = ObserveTestConfiguration.getDatabasePasswordConfigurationAnnotation(testClass);
        if (databasePassword != null) {
            password = ObserveTestConfiguration.getPassword(databasePassword.value());
        }

        DatabaseNameConfiguration databaseNameConfiguration = ObserveTestConfiguration.getDatabaseNameConfigurationAnnotation(testClass, classifier);
        if (databaseNameConfiguration != null) {
            dbName = databaseNameConfiguration.value().name();
        }

        DatabaseVersionConfiguration databaseVersionConfiguration = ObserveTestConfiguration.getDatabaseVersionConfigurationAnnotation(testClass, classifier);
        if (databaseVersionConfiguration != null) {
            dbVersion = ObserveTestConfiguration.getVersion(databaseVersionConfiguration.value());
        }

        DatabaseUrlConfiguration databaseUrl = ObserveTestConfiguration.getDatabaseUrlConfigurationAnnotation(description.getTestClass());
        if (databaseUrl != null) {
            url = ObserveTestConfiguration.getUrl(databaseUrl.value());
        }

    }

    protected void after(Description description) {

        Class<?> testClass = description.getTestClass();
        if (log.isDebugEnabled()) {
            log.debug("Ends " + testClass.getName());
        }

        setTemporaryDirectoryRoot(null);

    }

    public File createTemporaryFile(String suffix) {
        return temporaryDirectoryRoot.resolve(System.nanoTime() + suffix).toFile();
    }

    public File newFile(String methodName) {
        return new File(testDirectory, methodName);
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

}
