package fr.ird.observe.test;

/*-
 * #%L
 * ObServe :: Test
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.ByteStreams;
import com.google.common.io.Resources;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;
import java.util.Properties;

/**
 * Created on 29/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveTestResources {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveTestResources.class);
    

    private static final String BACKUP_SCRIPT_PATTERN = "/db/%1$s/%2$s.sql.gz";
    private static final String TEST_PROPERTIES_FILE_NAME = "observetest.properties";
    private static Properties testProperties;

    public static String getBackupScript(Version version, String loadScript) {

        String result = String.format(BACKUP_SCRIPT_PATTERN, version.toString(), loadScript);
        if (log.isInfoEnabled()) {
            log.info(result);
        }
        return result;

    }

    protected static Properties getTestProperties() {
        if (testProperties == null) {
            testProperties = new Properties();
            try (InputStream inputStream = ObserveTestResources.class.getClassLoader().getResourceAsStream(TEST_PROPERTIES_FILE_NAME)) {
                testProperties.load(inputStream);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("unable to load test properties", e);
                }
            }
        }
        return testProperties;
    }

    public static byte[] getResourceContent(String scriptPath) throws IOException {

        URL url = ObserveTestResources.class.getResource(scriptPath);
        Objects.requireNonNull(url, "Could not find resource at: " + scriptPath);
        try (InputStream inputStream = Resources.asByteSource(url).openStream()) {
            return ByteStreams.toByteArray(inputStream);
        }
    }
}
