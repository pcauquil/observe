/*
 * #%L
 * ObServe :: Test
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class TestHelper {

    private static File testsBasedir;

    private static File basedir;

    public static File getCommonsDir() {
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        return new File(baseDir, "commons");
    }

    public static File getTestBasedir(Class<?> testClass) {
        return new File(getTestsBasedir(), testClass.getSimpleName());
    }

    private static File getBasedir() {
        if (basedir == null) {
            String tmp = System.getProperty("basedir");
            if (tmp == null) {
                tmp = new File("").getAbsolutePath();
            }
            basedir = new File(tmp);
        }
        return basedir;
    }

    private static File getTestsBasedir() {
        if (testsBasedir == null) {
            testsBasedir = getBasedir().toPath().resolve("target").resolve("surefire-workdir").resolve("tests").resolve(System.nanoTime() + "").toFile();
        }
        return testsBasedir;
    }

}
