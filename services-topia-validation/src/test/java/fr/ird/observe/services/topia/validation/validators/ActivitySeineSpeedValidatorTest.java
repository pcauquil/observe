package fr.ird.observe.services.topia.validation.validators;

/*
 * #%L
 * ObServe :: Services ToPIA validation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.seine.VesselActivitySeine;
import fr.ird.observe.entities.referentiel.seine.VesselActivitySeineImpl;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.ActivitySeineImpl;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.RouteImpl;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.util.DateUtil;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorResult;

import java.util.Date;
import java.util.Locale;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ActivitySeineSpeedValidatorTest {

    NuitonValidatorResult messages;

    static VesselActivitySeine vesselActivitySeine;

    static Locale locale;

    @BeforeClass
    public static void beforeTest() {

        locale = Locale.getDefault();

        Locale.setDefault(Locale.FRENCH);

//        TestHelper.createApplicationContext();

        vesselActivitySeine = new VesselActivitySeineImpl();

        vesselActivitySeine.setLabel1("VesselActivity");
    }

    @After
    public void tearDonw() {
        if (messages != null) {
            messages = null;
        }
    }

    @AfterClass
    public static void afterTest() {
        Locale.setDefault(locale);
    }

    @Test
    public void testValidate() {

        NuitonValidator<Route> validator =
                NuitonValidatorFactory.newValidator(Route.class, "testSpeed");

        // il y a 316 Km (196 Miles) en tre Rouen et Nantes
        ActivitySeine aFromNantes = new ActivitySeineImpl();
        aFromNantes.setVesselActivitySeine(vesselActivitySeine);

        aFromNantes.setLatitude(47.197f);
        aFromNantes.setLongitude(-1.525f);
        Date nantesTime = DateUtil.getTime(DateUtil.createDate(0, 10, 10, 1, 1, 2014), false, false);

        aFromNantes.setTime(nantesTime);

        ActivitySeine aFromRouen = new ActivitySeineImpl();
        aFromRouen.setVesselActivitySeine(vesselActivitySeine);
        aFromRouen.setLatitude(49.447f);
        aFromRouen.setLongitude(1.096f);


        Route r = new RouteImpl();
        r.setDate(DateUtil.createDate(13, 12, 2014));
        r.addActivitySeine(aFromNantes);
        r.addActivitySeine(aFromRouen);

        // en 1 heure, on fait pas plus de 100 miles

        aFromRouen.setTime(DateUtils.addHours(nantesTime, 1));
        messages = validator.validate(r);
        Assert.assertFalse(messages.isValid());

        aFromRouen.setTime(DateUtils.addHours(nantesTime, 2));

        messages = validator.validate(r);
        Assert.assertTrue(messages.isValid());
    }
}
