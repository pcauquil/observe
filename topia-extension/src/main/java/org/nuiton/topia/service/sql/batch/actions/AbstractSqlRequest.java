package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;

import java.io.Writer;

/**
 * Support to create action request.
 * <p>
 * Created on 29/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public abstract class AbstractSqlRequest {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(AbstractSqlRequest.class);

    protected TopiaApplicationContext sourceTopiaApplicationContext;

    protected TopiaApplicationContext targetTopiaApplicationContext;

    protected Writer writer;

    public TopiaApplicationContext getTargetTopiaApplicationContext() {
        return targetTopiaApplicationContext;
    }

    protected void setTargetTopiaApplicationContext(TopiaApplicationContext targetTopiaApplicationContext) {
        this.targetTopiaApplicationContext = targetTopiaApplicationContext;
    }

    public TopiaApplicationContext getSourceTopiaApplicationContext() {
        return sourceTopiaApplicationContext;
    }

    protected void setSourceTopiaApplicationContext(TopiaApplicationContext sourceTopiaApplicationContext) {
        this.sourceTopiaApplicationContext = sourceTopiaApplicationContext;
    }

    public Writer getWriter() {
        return writer;
    }

    protected void setWriter(Writer writer) {
        this.writer = writer;
    }

    public static abstract class AbstractSqlRequestBuilder<B extends AbstractSqlRequestBuilder, R extends AbstractSqlRequest> {

        protected final R request;

        protected AbstractSqlRequestBuilder(R request) {
            this.request = request;
        }

        public B from(TopiaApplicationContext sourceTopiaApplicationContext) {
            request.setSourceTopiaApplicationContext(sourceTopiaApplicationContext);
            return returnThis();
        }

        public B to(TopiaApplicationContext targetTopiaApplicationContext) {
            request.setTargetTopiaApplicationContext(targetTopiaApplicationContext);
            return returnThis();
        }

        public B to(Writer writer) {
            request.setWriter(writer);
            return returnThis();
        }

        public R build() {
            checkParams();
            return request;
        }

        protected void checkParams() {
            Preconditions.checkState(request.getSourceTopiaApplicationContext() != null, "No sourceTopiaApplicationContext defined");
            Preconditions.checkState(request.getWriter() != null || request.getTargetTopiaApplicationContext() != null, "No targetTopiaApplicationContext, nor writer defined");
        }

        protected B returnThis() {
            return (B) this;
        }
    }
}
