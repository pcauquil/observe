package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTable;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;

import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class UpdateTablesAction extends AbstractTablesAction<UpdateTablesRequest> {

    public static final String UPDATE_STATEMENT = "UPDATE %s.%s %s WHERE topiaid='%%s'";
    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(UpdateTablesAction.class);

    public UpdateTablesAction(UpdateTablesRequest request) {
        super(request);
    }

    protected TopiaSqlTables getTables() {
        return request.getTables();
    }

    @Override
    protected void executeOnTable(UpdateTablesRequest request, TopiaSqlTable table, PreparedStatement readStatement) throws SQLException {

        ResultSet readResultSet = readStatement.getResultSet();

        TopiaMetadataEntity metadataEntity = table.getMetadataEntity();

        List<String> columnNames = getColumnNames(metadataEntity, table, readResultSet);

        String topiaIdColumnName = TopiaEntity.PROPERTY_TOPIA_ID.toLowerCase();
        int topiaIdColumnIndex = columnNames.indexOf(topiaIdColumnName);
        columnNames.remove(topiaIdColumnName);

        boolean useOutputWriter = useOutputWriter();
        boolean useOutputDb = useOutputDb();

        PreparedStatement writeStatement = null;

        String updateStatementSql = newUpdateStatementSql(table, columnNames);

        if (useOutputDb) {

            String arguments = generateWildcardArguments(columnNames);
            String sql = String.format(updateStatementSql, arguments, "?");
            writeStatement = targetConnection.prepareStatement(sql);

        }

        int writeBatchSize = request.getWriteBatchSize();
        String tableName = table.getFullyTableName();

        long index = 0;
        while (readResultSet.next()) {

            String topiaId = readResultSet.getString(topiaIdColumnIndex);
            if (log.isTraceEnabled()) {
                log.trace("Update " + readResultSet.getString(1));
            }


//            if (useOutputDb) {
//
//                writeStatement.clearParameters();
//                int i = 1;
//                for (String columnName : columnNames) {
//                    Object object = readResultSet.getObject(columnName);
//                    writeStatement.setObject(i++, object);
//                }
//                writeStatement.setString(i + 1, topiaId);
//                writeStatement.addBatch();
//
//            }

            if (useOutputWriter) {

                try {

                    String arguments = generateArguments(columnNames, readResultSet);
                    String sql = String.format(updateStatementSql, arguments, topiaId);
                    writer.append(sql);

                } catch (IOException e) {
                    throw new RuntimeException("Could not updateRow", e);
                }

            }

            if ((++index % writeBatchSize) == 0) {
                flush(writeStatement, writer, tableName, index);
            }

        }

        flush(writeStatement, writer, tableName, index);

    }

    protected String generateArguments(Iterable<String> columnNames, ResultSet readResultSet) throws SQLException, IOException {

        String statement = "";

        for (String columnName : columnNames) {

            statement += ", SET " + columnName + " = ";
            Object columnValue = readResultSet.getObject(columnName);
            if (columnValue == null) {
                statement += "NULL";
                continue;
            }

            if (columnValue instanceof String) {
                String stringValue = (String) columnValue;
                statement += "'" + stringValue.replaceAll("'", "''") + "'";
                continue;
            }

            if (columnValue instanceof Date) {
                statement += "'" + columnValue + "'";
                continue;
            }

            if (columnValue instanceof Blob) {
                Blob blob = (Blob) columnValue;
                SerialBlob serialBlob = new SerialBlob(blob);
                try (ByteArrayOutputStream stringWriter = new ByteArrayOutputStream((int) serialBlob.length())) {
                    stringWriter.write(serialBlob.getBinaryStream());
                    statement += "'" + new String(stringWriter.toByteArray()) + "'";
                }
                continue;
            }

            statement += columnValue;

        }

        return statement.substring(2);

    }

    protected String newUpdateStatementSql(TopiaSqlTable table, Iterable<String> columnNames) {

        StringBuilder columnNamesBuilder = new StringBuilder();

        columnNames.forEach(columnName -> columnNamesBuilder.append(", SET ").append(columnName).append(" = ?"));

        String sql = String.format(UPDATE_STATEMENT,
                                   table.getSchemaName(),
                                   table.getTableName(),
                                   columnNamesBuilder.substring(2));
        if (log.isDebugEnabled()) {
            log.debug("Insert sql: " + sql);
        }

        return sql;

    }

}
