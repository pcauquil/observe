package org.nuiton.topia.service.sql.batch;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Configuration of the {@link TopiaSqlBatchService}.
 * <p>
 * Created on 05/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class TopiaSqlBatchServiceConfiguration {

    public static final String PROPERTY_READ_FETCH_SIZE = "readFetchSize";
    public static final String PROPERTY_WRITE_BATCH_SIZE = "writeBatchSize";
    public static final int DEFAULT_READ_FETCH_SIZE = 1000;
    public static final int DEFAULT_WRITE_BATCH_SIZE = 1000;

    /**
     * The fetch size used by read statements.
     */
    protected int readFetchSize = DEFAULT_READ_FETCH_SIZE;

    /**
     * The batch size used by write statements.
     */
    protected int writeBatchSize = DEFAULT_WRITE_BATCH_SIZE;

    public int getReadFetchSize() {
        return readFetchSize;
    }

    public int getWriteBatchSize() {
        return writeBatchSize;
    }

    protected void setReadFetchSize(int readFetchSize) {
        this.readFetchSize = readFetchSize;
    }

    protected void setWriteBatchSize(int writeBatchSize) {
        this.writeBatchSize = writeBatchSize;
    }
}
