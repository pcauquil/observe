package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.service.sql.batch.TopiaSqlBatchServiceConfiguration;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;

/**
 * Support to create action request.
 * <p>
 * Created on 29/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public abstract class AbstractTablesRequest extends AbstractSqlRequest {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(AbstractTablesRequest.class);

    protected int readFetchSize;

    protected int writeBatchSize;

    protected TopiaSqlTableSelectArgument selectArgument;

    protected TopiaSqlTables tables;

    public int getReadFetchSize() {
        return readFetchSize;
    }

    public void setReadFetchSize(int readFetchSize) {
        this.readFetchSize = readFetchSize;
    }

    public int getWriteBatchSize() {
        return writeBatchSize;
    }

    public void setWriteBatchSize(int writeBatchSize) {
        this.writeBatchSize = writeBatchSize;
    }

    public TopiaSqlTableSelectArgument getSelectArgument() {
        return selectArgument;
    }

    public void setSelectArgument(TopiaSqlTableSelectArgument selectArgument) {
        this.selectArgument = selectArgument;
    }

    public TopiaSqlTables getTables() {
        return tables;
    }

    public void setTables(TopiaSqlTables tables) {
        this.tables = tables;
    }

    /**
     * Support to create action builder.
     * <p>
     * Created on 29/12/15.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 3.0.1
     */
    public abstract static class AbstractTablesRequestBuilder<B extends AbstractTablesRequestBuilder, R extends AbstractTablesRequest> extends AbstractSqlRequestBuilder<B, R> {

        protected AbstractTablesRequestBuilder(R sqlActionRequest) {
            super(sqlActionRequest);
            setReadFetchSize(TopiaSqlBatchServiceConfiguration.DEFAULT_READ_FETCH_SIZE);
            setWriteBatchSize(TopiaSqlBatchServiceConfiguration.DEFAULT_WRITE_BATCH_SIZE);
        }

        public B setTables(TopiaSqlTables tables) {
            request.setTables(tables);
            return returnThis();
        }

        public B setReadFetchSize(int readFetchSize) {
            request.setReadFetchSize(readFetchSize);
            return returnThis();
        }

        public B setWriteBatchSize(int writeBatchSize) {
            request.setWriteBatchSize(writeBatchSize);
            return returnThis();
        }

        public B setSelectArgument(TopiaSqlTableSelectArgument arg) {
            request.setSelectArgument(arg);
            return returnThis();
        }

    }
}
