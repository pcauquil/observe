package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.output.WriterOutputStream;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.Dialect;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class DropSchemaAction extends AbstractSchemaAction<DropSchemaRequest> {

    public static final String DROP_SCHEMA_STATEMENT = "\nDROP SCHEMA %s;";

    public DropSchemaAction(DropSchemaRequest request) {
        super(request);
    }

    @Override
    protected String produceSql(Class<? extends Dialect> dialectType, Path temporaryDirectory) throws IOException {

        try {

            Path sqlScriptFile = temporaryDirectory.resolve("replicateSchema_" + System.nanoTime() + ".sql");

            Configuration hibernateConfiguration = getSourcePersistenceContext().getHibernateSupport().getHibernateConfiguration();

            Properties properties = new Properties();

            properties.put(Environment.DIALECT, dialectType.getName());

            new SchemaExport(hibernateConfiguration, properties)
                    .setOutputFile(sqlScriptFile.toFile().getAbsolutePath())
                    .setDelimiter(";")
                    .drop(false, false);

            WriterOutputStream out = new WriterOutputStream(writer);
            Files.copy(sqlScriptFile, out);
            out.flush();

            String sqlContent = new String(Files.readAllBytes(sqlScriptFile));
            Files.delete(sqlScriptFile);

            if (request.isDropSchema()) {

                ImmutableSet<String> schemaNames = getSchemaNames();
                for (String schemaName : schemaNames) {
                    sqlContent += String.format(DROP_SCHEMA_STATEMENT, schemaName);
                }
            }

            return sqlContent;

        } catch (HibernateException eee) {
            throw new TopiaException(String.format("Could not create schema for reason: %s", eee.getMessage()), eee);
        }

    }
}
