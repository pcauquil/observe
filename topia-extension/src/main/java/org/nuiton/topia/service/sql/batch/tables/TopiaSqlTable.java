package org.nuiton.topia.service.sql.batch.tables;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;

import java.util.Objects;

/**
 * This object represents a sql table.
 *
 * Created on 30/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class TopiaSqlTable {

    protected final TopiaMetadataEntity metadataEntity;
    protected final TopiaMetadataEntity associationMetadataEntity;

    /**
     * Table schema name.
     */
    protected final String schemaName;

    /**
     * Table name.
     */
    protected final String tableName;

    /**
     * Association table (optional only present for association table).
     */
    private final boolean associationTable;

    /**
     * Join column name (optional only present for association table).
     */
    private final String joinColumnName;

    /**
     * Fully table name (including the schema name).
     */
    protected final String fullyTableName;

    /**
     * From clause.
     */
    protected final String fromClause;

    /**
     * Where clause alias.
     */
    protected final String whereClauseAlias;

    /**
     * Join clauses.
     */
    protected final ImmutableSet<String> joinClauses;

    public TopiaSqlTable(TopiaMetadataEntity metadataEntity,
                         TopiaMetadataEntity associationMetadataEntity,
                         String schemaName,
                         String tableName,
                         String fromClause,
                         String whereClauseAlias,
                         ImmutableSet<String> joinClauses,
                         String joinColumnName) {
        this.metadataEntity = metadataEntity;
        this.associationMetadataEntity = associationMetadataEntity;
        this.schemaName = schemaName.toLowerCase();
        this.tableName = tableName.toLowerCase();
        this.associationTable = associationMetadataEntity != null;
        this.fullyTableName = this.schemaName + "." + this.tableName;
        this.fromClause = fromClause;
        this.whereClauseAlias = whereClauseAlias;
        this.joinClauses = joinClauses;
        this.joinColumnName = joinColumnName;
    }

    public TopiaMetadataEntity getMetadataEntity() {
        return metadataEntity;
    }

    public TopiaMetadataEntity getAssociationMetadataEntity() {
        return associationMetadataEntity;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getFullyTableName() {
        return fullyTableName;
    }

    public String getFromClause() {
        return fromClause;
    }

    public String getWhereClauseAlias() {
        return whereClauseAlias;
    }

    public String getWhereClause(ImmutableSet<String> ids) {
        String result = whereClauseAlias;
        if (ids.size() == 1) {
            result += " = ?";
        } else {
            String in = "";
            for (String ignored : ids) {
                in += ", ?";
            }
            result += " IN (" + in.substring(2) + ")";
        }
        return result;
    }

    public ImmutableSet<String> getJoinClauses() {
        return joinClauses;
    }

    public boolean isAssociationTable() {
        return associationTable;
    }

    public String getJoinColumnName() {
        return joinColumnName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopiaSqlTable that = (TopiaSqlTable) o;
        return Objects.equals(fullyTableName, that.fullyTableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullyTableName);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("fullyTableName", fullyTableName)
                          .add("associationTable", associationTable)
                          .toString();
    }
}
