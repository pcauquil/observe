package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.PostgreSQL9Dialect;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 01/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public abstract class AbstractSchemaRequest extends AbstractSqlRequest {

    protected Class<? extends Dialect> dialect;

    protected Path temporaryPath;

    public Class<? extends Dialect> getDialect() {
        return dialect;
    }

    protected void setDialect(Class<? extends Dialect> dialect) {
        this.dialect = dialect;
    }

    public Path getTemporaryPath() {
        return temporaryPath;
    }

    protected void setTemporaryPath(Path temporaryPath) {
        this.temporaryPath = temporaryPath;
    }

    public abstract static class AbstractSchemaRequestBuilder<R extends AbstractSchemaRequest, B extends AbstractSchemaRequestBuilder<R, B>> extends AbstractSqlRequestBuilder<B, R> {

        protected AbstractSchemaRequestBuilder(R request) {
            super(request);
        }

        public B forH2() {
            setDialect(H2Dialect.class);
            return returnThis();
        }

        public B forPostgres() {
            setDialect(PostgreSQL9Dialect.class);
            return returnThis();
        }

        public B setDialect(Class<? extends Dialect> dialectType) {
            request.setDialect(dialectType);
            return returnThis();
        }

        public B setTemporaryPath(Path temporaryPath) {
            request.setTemporaryPath(temporaryPath);
            return returnThis();
        }

        @Override
        protected void checkParams() {
            super.checkParams();
            Preconditions.checkState(request.getDialect() != null, "No dialect defined");

            if (request.getTemporaryPath() == null) {

                try {
                    Path tempDirectory = Files.createTempDirectory(getClass().getSimpleName());
                    setTemporaryPath(tempDirectory);
                } catch (IOException e) {
                    throw new RuntimeException("Could not create teomporary path");
                }

            }
        }

    }
}
