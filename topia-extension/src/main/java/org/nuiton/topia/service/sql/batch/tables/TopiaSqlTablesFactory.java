package org.nuiton.topia.service.sql.batch.tables;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.TopiaEntityEnumProvider;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModelVisitor;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A factory of {@link TopiaSqlTables}.
 *
 * Created on 04/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class TopiaSqlTablesFactory {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(TopiaSqlTablesFactory.class);

    protected final TopiaMetadataModel model;
    protected final TopiaEntityEnumProvider entityEnumProvider;

    public TopiaSqlTablesFactory(TopiaMetadataModel model, TopiaEntityEnumProvider entityEnumProvider) {
        this.model = model;
        this.entityEnumProvider = entityEnumProvider;
    }

    public TopiaSqlTables newReplicateEntityTables(TopiaSqlTablesPredicate predicate, TopiaEntityEnum... entityEnums) {

        ReplicateTables tablesBuilder = new ReplicateTables(predicate);
        return tablesBuilder.getTables(entityEnums);

    }

    public interface TopiaSqlTablesPredicate {

        boolean acceptEntity(TopiaMetadataEntity metadataEntity);

        boolean acceptAssociation(TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

        boolean acceptReversedAssociation(TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

        boolean acceptNmAssociation(TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

    }

    public class ReplicateTables implements TopiaMetadataModelVisitor {

        protected final TopiaSqlTablesPredicate predicate;
        protected final Set<TopiaMetadataEntity> dones;
        protected TopiaSqlTables.BuilderStepOnTable builder;
        protected TopiaSqlTables tables;

        public ReplicateTables(TopiaSqlTablesPredicate predicate) {
            this.predicate = predicate;
            this.dones = new LinkedHashSet<>();
        }

        public TopiaSqlTables getTables(TopiaEntityEnum... entityEnums) {
            visitModelStart(model);
            for (TopiaEntityEnum entityEnum : entityEnums) {
                TopiaMetadataEntity entity = model.getEntity(entityEnum.name());
                entity.accept(this, model);
            }
            visitModelEnd(model);
            return tables;
        }

        @Override
        public void visitModelStart(TopiaMetadataModel metadataModel) {
        }

        @Override
        public void visitModelEnd(TopiaMetadataModel metadataModel) {
            tables = builder.build();
        }

        @Override
        public void visitEntiyStart(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {

            if (predicate.acceptEntity(metadataEntity)) {

                boolean added = dones.add(metadataEntity);
                if (added) {

                    log.info("E → " + metadataEntity.getType());
                    TopiaEntityEnum entityEnum = entityEnumProvider.getEntityEnum(metadataEntity.getType());

                    builder = (builder == null ? TopiaSqlTables.builder(metadataModel, metadataEntity) : builder).addMainTable(entityEnum);
                }
            }

        }

        @Override
        public void visitEntiyEnd(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {

            if (dones.contains(metadataEntity)) {
                log.info("E ← " + metadataEntity.getType());
            }

        }

        @Override
        public void visitReversedAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

            if (predicate.acceptReversedAssociation(metadataEntity, propertyName, propertyType)) {

                TopiaEntityEnum entityEnum = entityEnumProvider.getEntityEnum(propertyType.getType());
                boolean withShell = propertyType.withShell();
                log.info(metadataEntity.getType() + "/" + propertyName + "→" + propertyType.getType() + " (withShell: " + withShell + ")");

                if (withShell) {
                    builder = builder.addAndEnterReverseJoinTable(entityEnum);
                    visitChild(propertyType);
                } else {
                    builder = builder.addReverseJoinTable(entityEnum);
                }
            }
        }

        @Override
        public void visitOneToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

            if (predicate.acceptAssociation(metadataEntity, propertyName, propertyType)) {

                TopiaEntityEnum entityEnum = entityEnumProvider.getEntityEnum(propertyType.getType());
                boolean withShell = propertyType.withShell();
                log.info(metadataEntity.getType() + "/" + propertyName + "→" + propertyType.getType() + " (withShell: " + withShell + ")");

                if (withShell) {
                    builder = builder.addAndEnterJoinTable(entityEnum);
                    visitChild(propertyType);
                } else {
                    builder = builder.addJoinTable(entityEnum);
                }
            }

        }

        @Override
        public void visitOneToManyAssociationInverse(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            // Rien a faire
        }

        @Override
        public void visitManyToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

            if (predicate.acceptNmAssociation(metadataEntity, propertyName, propertyType)) {

                log.info(metadataEntity.getType() + "/" + propertyName + "→" + propertyType.getType());

                TopiaEntityEnum entityEnum = entityEnumProvider.getEntityEnum(propertyType.getType());
                builder = builder.addAssociationTable(entityEnum, propertyName);
            }

        }

        @Override
        public void visitManyToOneAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
        }

        @Override
        public void visitProperty(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, String propertyType) {

        }

        protected void visitChild(TopiaMetadataEntity propertyType) {
            dones.add(propertyType);
            propertyType.accept(this, model);
            builder = builder.backToParent();
        }

    }
}
