package org.nuiton.topia.service.sql.batch.actions;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.support.TopiaSqlWork;
import org.nuiton.util.TimeLog;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Support to create action.
 * <p>
 * Created on 29/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public abstract class AbstractSqlAction<R extends AbstractSqlRequest> implements Runnable, Closeable {

    protected static final TimeLog TIME_LOG = new TimeLog(AbstractSqlAction.class, 50L, 100L);
    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(AbstractSqlAction.class);
    protected final R request;

    protected final ImmutableSet<Closeable> closeables;

    protected TopiaPersistenceContext sourcePersistenceContext;

    protected Connection targetConnection;

    protected Writer writer;

    protected long startTime;

    protected long endTime;

    private final Set<BlobsContainer.Builder> blobsContainerBuilders = new LinkedHashSet<>();

    protected AbstractSqlAction(R request) {
        this.request = request;

        this.closeables = ImmutableSet.<Closeable>builder()
                .add(() -> {
                    if (sourcePersistenceContext != null) {
                        sourcePersistenceContext.close();
                    }
                })
                .add(() -> {
                    if (targetConnection != null) {
                        try {
                            targetConnection.close();
                        } catch (SQLException e) {
                            throw new TopiaException("Could not close targetConnection", e);
                        }
                    }
                }).build();
    }

    public static void flush(Writer writer) {
        try {
            writer.flush();
        } catch (IOException e) {
            throw new TopiaException("Could not flush writer", e);
        }
    }

    public R getRequest() {
        return request;
    }

    public Optional<Set<BlobsContainer.Builder>> getBlobsContainersBuilder() {

        if (blobsContainerBuilders.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(blobsContainerBuilders);

    }

    public void registerBlobsContainer(BlobsContainer.Builder builder) {
        blobsContainerBuilders.add(builder);
    }

    protected boolean useOutputWriter() {
        return request.getWriter() != null;
    }

    protected boolean useOutputDb() {
        return request.getTargetTopiaApplicationContext() != null;
    }

    protected abstract void execute() throws IOException, SQLException;

    @Override
    public final void run() {

        try {

            before();
            execute();
            after();

        } catch (Exception e) {
            fail(e);
        }

    }

    public void commit() {

        if (useOutputWriter()) {
            flush(writer);
        }

        if (useOutputDb()) {

            if (targetConnection != null) {
                try {
                    targetConnection.commit();
                } catch (SQLException e) {
                    throw new TopiaException("Could not commit", e);
                }
            }

        }

    }

    @Override
    public final void close() {

        Exception error = null;
        for (Closeable closeable : closeables) {
            try {
                closeable.close();
            } catch (Exception e) {
                error = e;
                log.error("Could not close", e);
            }
        }
        if (error != null) {
            throw new RuntimeException("Could not close", error);
        }

    }

    protected final void before() throws SQLException {
        startTime = TimeLog.getTime();

        if (useOutputWriter()) {
            writer = request.getWriter();
        }

        if (useOutputDb()) {
            OpenJdbcHelper jdbcHelper = new OpenJdbcHelper(request.getTargetTopiaApplicationContext().getConfiguration());
            targetConnection = jdbcHelper.openConnection();
        }

    }

    protected void fail(Exception e) {

        endTime = TIME_LOG.log(startTime, "Action failed", getClass().getName());

        throw new TopiaException(e);

    }

    protected final void after() {

        endTime = TIME_LOG.log(startTime, "Action executed", getClass().getName());

    }

    protected void executeSqlWork(TopiaSqlWork sqlWork) {
        getSourcePersistenceContext().getSqlSupport().doSqlWork(sqlWork);
    }

    protected AbstractTopiaPersistenceContext getSourcePersistenceContext() {
        if (sourcePersistenceContext == null) {
            sourcePersistenceContext = request.getSourceTopiaApplicationContext().newPersistenceContext();
        }
        return (AbstractTopiaPersistenceContext) sourcePersistenceContext;
    }

    protected ImmutableSet<String> getSchemaNames() {
        TopiaApplicationContext<?> sourceTopiaApplicationContext = request.getSourceTopiaApplicationContext();
        return sourceTopiaApplicationContext.getSchemaNames();
    }

    protected static class OpenJdbcHelper extends JdbcHelper {

        public OpenJdbcHelper(JdbcConfiguration jdbcConfiguration) {
            super(jdbcConfiguration);
        }

        @Override
        public Connection openConnection() throws SQLException {
            return super.openConnection();
        }
    }

}
