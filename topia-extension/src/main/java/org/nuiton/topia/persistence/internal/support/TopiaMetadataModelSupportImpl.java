package org.nuiton.topia.persistence.internal.support;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;
import org.nuiton.topia.persistence.support.TopiaMetadataModelSupport;

import java.io.IOException;

/**
 * Created on 04/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class TopiaMetadataModelSupportImpl implements TopiaMetadataModelSupport {

    private final String metadataModelPath;

    protected TopiaMetadataModel metadataModel;

    public <K extends TopiaPersistenceContext> TopiaMetadataModelSupportImpl(String packageName, String modelName) {
        this.metadataModelPath = String.format("/%s/%sTopiaMetadataModel.json", packageName.replace(".", "/"), modelName);
    }

    @Override
    public TopiaMetadataModel getMetadataModel() {
        if (metadataModel == null) {
            try {
                metadataModel = TopiaMetadataModel.load(getClass().getResource(metadataModelPath));
            } catch (IOException e) {
                throw new TopiaException(e);
            }
        }
        return metadataModel;
    }

}
