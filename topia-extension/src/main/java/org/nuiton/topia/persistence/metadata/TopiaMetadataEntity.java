package org.nuiton.topia.persistence.metadata;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.sql.Blob;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created on 03/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class TopiaMetadataEntity {

    private static final Log log = LogFactory.getLog(TopiaMetadataEntity.class);

    /**
     * L'entité parent (optionnelle).
     */
    protected final String parent;
    /**
     * Le nom simple de l'entité (correspond au nom de l'énumération qui caractérise cette entité).
     */
    protected final String type;
    /**
     * Le nom du schéma qui contient la table correspondant à l'entité.
     */
    protected final String dbSchemaName;
    /**
     * Le nom de la table qui correspond à l'entité.
     */
    protected final String dbTableName;
    /**
     * Le dictionnaire des associations simples (multiplicitié 1→n) (la clef est le nom de la propriété, la valeur son type).
     */
    protected final Map<String, String> oneToManyAssociations = new LinkedHashMap<>();
    /**
     * La liste des terminaisons des associations simples.
     *
     * Uniquement utilisé pour connaitre les colonnes physiques de la table sql.
     */
    protected final Set<String> oneToManyAssociationInverses = new TreeSet<>();
    /**
     * Le dictionnaire des associations inversées (la clef est le nom de la propriété, la valeur son type).
     */
    protected final Map<String, String> reversedAssociations = new LinkedHashMap<>();
    /**
     * Le dictionnaire des associations nm (multiplicitié n→m) (la clef est le nom de la propriété, la valeur son type).
     */
    protected final Map<String, String> manyToManyAssociations = new LinkedHashMap<>();
    /**
     * Le dictionnaire des compositions simples vers des entitées (la clef est le nom de la propriété, la valeur son type).
     */
    protected final Map<String, String> manyToOneAssociations = new LinkedHashMap<>();
    /**
     * Le dictionnaire des propriétés qui ne sont pas des entités (la clef est le nom de la propriété, la valeur son type).
     */
    protected final Map<String, String> properties = new LinkedHashMap<>();
    /**
     * Le dictionnaire des propriétés de type {@link Blob}
     */
    protected final Set<String> blobProperties = new HashSet<>();
    /**
     * Le nom des colunnes correspondants aux propriétés de l'entité.
     * <b>Note: </b> On ne conserve que les correspondances qui diffèrent du nom de la propriété.
     *
     * @see #getDbColumnName(String)
     */
    protected final Map<String, String> dbColumnsName = new LinkedHashMap<>();
    /**
     * Le nom des tables utilisées pour les associations nm.
     */
    protected final Map<String, String> dbManyToManyAssociationsTableName = new LinkedHashMap<>();
    protected Set<String> allDbColumnNames;


    public TopiaMetadataEntity(String parent, String type, String dbSchemaName, String dbTableName) {
        this.parent = parent;
        this.type = type;
        this.dbSchemaName = dbSchemaName;
        this.dbTableName = dbTableName;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopiaMetadataEntity that = (TopiaMetadataEntity) o;
        return Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("type", type)
                          .add("dbName", dbSchemaName + "." + dbTableName)
                          .toString();
    }

    public boolean withParent() {
        return parent != null;
    }

    public String getParent() {
        return parent;
    }

    public String getDbSchemaName() {
        return dbSchemaName;
    }

    public String getDbTableName() {
        return dbTableName;
    }

    public Set<String> getAllDbColumnNames() {
        if (allDbColumnNames == null) {
            allDbColumnNames = getProperties().keySet().stream()
                                              .map(this::getDbColumnName)
                                              .map(String::toLowerCase)
                                              .collect(Collectors.toSet());
            allDbColumnNames.addAll(getManyToOneAssociations().keySet().stream()
                                                              .map(this::getDbColumnName)
                                                              .map(String::toLowerCase)
                                                              .collect(Collectors.toSet()));
            allDbColumnNames.addAll(getReversedAssociations().keySet().stream()
                                                             .map(this::getDbColumnName)
                                                             .map(String::toLowerCase)
                                                             .collect(Collectors.toSet()));
            allDbColumnNames.addAll(getOneToManyAssociationInverses().stream()
                                                                     .map(this::getDbColumnName)
                                                                     .map(String::toLowerCase)
                                                                     .collect(Collectors.toSet()));
            allDbColumnNames.add(TopiaEntity.PROPERTY_TOPIA_ID.toLowerCase());
            allDbColumnNames.add(TopiaEntity.PROPERTY_TOPIA_CREATE_DATE.toLowerCase());
            allDbColumnNames.add(TopiaEntity.PROPERTY_TOPIA_VERSION.toLowerCase());
        }
        return allDbColumnNames;
    }

    public Map<String, String> getReversedAssociations() {
        return reversedAssociations;
    }

    public Map<String, String> getManyToManyAssociations() {
        return manyToManyAssociations;
    }

    public Map<String, String> getOneToManyAssociations() {
        return oneToManyAssociations;
    }

    public Map<String, String> getManyToOneAssociations() {
        return manyToOneAssociations;
    }

    public Set<String> getOneToManyAssociationInverses() {
        return oneToManyAssociationInverses;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public static final Set<String> PRIMITIVE_TYPES = ImmutableSet.of("byte", "boolean", "char", "int", "long", "float", "double");

    public Set<String> getPrimitivePropertyNames() {
        Set<String> names = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String propertyType = entry.getValue();
            if (PRIMITIVE_TYPES.contains(propertyType)) {
                names.add(entry.getKey());
            }
        }
        return names;
    }

    public Set<String> getPrimitivePropertyNames(String primitiveType) {
        Set<String> names = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String propertyType = entry.getValue();
            if (primitiveType.equals(propertyType)) {
                names.add(entry.getKey());
            }
        }
        return names;
    }

    public boolean withBlob() {
        return !blobProperties.isEmpty();
    }

    public Set<String> getBlobProperties() {
        return blobProperties;
    }

    public Map<String, String> getDbColumnsName() {
        return dbColumnsName;
    }

    public String getDbColumnName(String propertyName) {
        String dbColumnName = dbColumnsName.get(propertyName);
        if (dbColumnName == null) {
            dbColumnName = propertyName;
        }
        return dbColumnName;
    }

    public Map<String, String> getDbManyToManyAssociationsTableName() {
        return dbManyToManyAssociationsTableName;
    }

    public String getBdManyToManyAssociationTableName(String propertyName) {
        return dbManyToManyAssociationsTableName.get(propertyName);
    }

    public void addOneToManyAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.info(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        oneToManyAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
    }

    public void addOneToManyAssociationInverse(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.info(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        oneToManyAssociationInverses.add(name);
        addDbColumnName(name, dbColumnName);
    }

    public void addReversedAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.info(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        reversedAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
    }

    public void addManyToManyAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName, String dbManyToManyAssociationTableName) {
        log.info(getType() + "/" + name + "(" + dbManyToManyAssociationTableName + ") →" + associationClazz.getType());
        manyToManyAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
        dbManyToManyAssociationsTableName.put(name, dbManyToManyAssociationTableName);
    }

    public void addManyToOneAssociation(TopiaMetadataEntity associationClazz, String name, String dbColumnName) {
        log.info(getType() + "/" + name + "(" + dbColumnName + ") →" + associationClazz.getType());
        manyToOneAssociations.put(name, associationClazz.getType());
        addDbColumnName(name, dbColumnName);
    }

    public void addProperty(String name, String type, String dbColumnName) {
        log.info(getType() + "/" + name + "(" + dbColumnName + ") →" + type);
        properties.put(name, type);
        if (Blob.class.getName().equals(type)) {
            blobProperties.add(name);
        }
        addDbColumnName(name, dbColumnName);
    }

    public TopiaMetadataEntity copy() {
        TopiaMetadataEntity copy = new TopiaMetadataEntity(parent, type, dbSchemaName, dbTableName);
        copy.oneToManyAssociations.putAll(oneToManyAssociations);
        copy.oneToManyAssociationInverses.addAll(oneToManyAssociationInverses);
        copy.reversedAssociations.putAll(reversedAssociations);
        copy.manyToManyAssociations.putAll(manyToManyAssociations);
        copy.manyToOneAssociations.putAll(manyToOneAssociations);
        copy.properties.putAll(properties);
        copy.dbColumnsName.putAll(dbColumnsName);
        copy.dbManyToManyAssociationsTableName.putAll(dbManyToManyAssociationsTableName);
        return copy;
    }

    public void putAll(TopiaMetadataEntity metadataEntity) {
        metadataEntity.getOneToManyAssociations().putAll(getOneToManyAssociations());
        metadataEntity.getOneToManyAssociationInverses().addAll(getOneToManyAssociationInverses());
        metadataEntity.getReversedAssociations().putAll(getReversedAssociations());
        metadataEntity.getManyToManyAssociations().putAll(getManyToManyAssociations());
        metadataEntity.getManyToOneAssociations().putAll(getManyToOneAssociations());
        metadataEntity.getProperties().putAll(getProperties());
        metadataEntity.getDbColumnsName().putAll(getDbColumnsName());
        metadataEntity.getDbColumnsName().putAll(getDbManyToManyAssociationsTableName());
    }

    public void accept(TopiaMetadataModelVisitor visitor, TopiaMetadataModel metadataModel) {
        visitor.visitEntiyStart(metadataModel, this);
        for (Map.Entry<String, String> entry : reversedAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitReversedAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : oneToManyAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitOneToManyAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (String propertyType : oneToManyAssociationInverses) {
            visitor.visitOneToManyAssociationInverse(metadataModel, this, propertyType, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : manyToManyAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitManyToManyAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : manyToOneAssociations.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitManyToOneAssociation(metadataModel, this, propertyName, metadataModel.getEntity(propertyType));
        }
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String propertyName = entry.getKey();
            String propertyType = entry.getValue();
            visitor.visitProperty(metadataModel, this, propertyName, propertyType);
        }
        visitor.visitEntiyEnd(metadataModel, this);
    }

    public boolean withShell() {
        return !(reversedAssociations.isEmpty() && oneToManyAssociations.isEmpty() && manyToManyAssociations.isEmpty());
    }

    private void addDbColumnName(String name, String dbColumnName) {
        if (!name.equals(dbColumnName)) {
            dbColumnsName.put(name, dbColumnName);
        }
    }

}
