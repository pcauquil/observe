package org.nuiton.topia.persistence.metadata;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 04/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public interface TopiaMetadataModelVisitor {

    void visitModelStart(TopiaMetadataModel metadataModel);

    void visitModelEnd(TopiaMetadataModel metadataModel);

    void visitEntiyStart(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity);

    void visitEntiyEnd(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity);

    void visitReversedAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

    void visitOneToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

    void visitOneToManyAssociationInverse(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

    void visitManyToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

    void visitManyToOneAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType);

    void visitProperty(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, String propertyType);

    class TopiaMetadataModelVisitorAdapter implements TopiaMetadataModelVisitor {

        @Override
        public void visitModelStart(TopiaMetadataModel metadataModel) {

        }

        @Override
        public void visitModelEnd(TopiaMetadataModel metadataModel) {

        }

        @Override
        public void visitEntiyStart(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {

        }

        @Override
        public void visitEntiyEnd(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {

        }

        @Override
        public void visitReversedAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

        }

        @Override
        public void visitOneToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

        }

        @Override
        public void visitManyToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

        }

        @Override
        public void visitManyToOneAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

        }

        @Override
        public void visitProperty(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, String propertyType) {

        }

        @Override
        public void visitOneToManyAssociationInverse(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

        }
    }

    class PrintVisitor implements TopiaMetadataModelVisitor {

        private final Log log = LogFactory.getLog(TopiaMetadataModelVisitor.class);
        protected final boolean deepVisit;
        protected final String eol;
        protected final StringBuilder builder;

        protected String prefix = "";

        class DeepVisitor extends TopiaMetadataModelVisitorAdapter {

            String prefix;
            Set<String> visited = new LinkedHashSet<>();

            public DeepVisitor(String prefix) {
                this.prefix = prefix;
            }

            @Override
            public void visitEntiyStart(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {
                prefix += "  ";
                appendPrefix("E → ").append(metadataEntity).append(eol);
            }

            @Override
            public void visitEntiyEnd(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {
                appendPrefix("E ← ").append(metadataEntity).append(eol);
                prefix = prefix.substring(2);
            }

            @Override
            public void visitReversedAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
                appendProperty("ReversedAssociation: ", metadataEntity, propertyName, propertyType.getType());
                if (visited.add(propertyType.getType())) {
                    propertyType.accept(this, metadataModel);
                }
            }

            @Override
            public void visitOneToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
                appendProperty("OneToManyAssociation: ", metadataEntity, propertyName, propertyType.getType());
                if (visited.add(propertyType.getType())) {
                    propertyType.accept(this, metadataModel);
                }
            }

            @Override
            public void visitOneToManyAssociationInverse(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
                appendProperty("OneToManyAssociationInverse: ", metadataEntity, propertyName, propertyType.getType());
            }

            @Override
            public void visitManyToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
                appendProperty("ManyToManyAssociation: ", metadataEntity, propertyName, propertyType.getType());
                if (visited.add(propertyType.getType())) {
                    propertyType.accept(this, metadataModel);
                }
            }

            @Override
            public void visitManyToOneAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
                appendProperty("ManyToOneAssociation: ", metadataEntity, propertyName, propertyType.getType());
                if (visited.add(propertyType.getType())) {
                    propertyType.accept(this, metadataModel);
                }
            }

            private StringBuilder appendPrefix(String prefix) {
                return builder.append(this.prefix).append(prefix);
            }

            private StringBuilder appendProperty(String prefix, TopiaMetadataEntity metadataEntity, String propertyName, String propertyType) {
                return appendPrefix(prefix).append(metadataEntity).append("/").append(propertyName).append("(").append(metadataEntity.getDbColumnName(propertyName)).append(")→").append(propertyType).append(eol);
            }
        }

        public PrintVisitor(boolean deepVisit, String eol) {
            this.deepVisit = deepVisit;
            this.eol = eol;
            this.builder = new StringBuilder();
        }

        @Override
        public void visitModelStart(TopiaMetadataModel metadataModel) {
            appendPrefix("M → ").append(metadataModel).append(eol);
        }

        @Override
        public void visitModelEnd(TopiaMetadataModel metadataModel) {
            appendPrefix("M ← ").append(metadataModel).append(eol);

        }

        @Override
        public void visitEntiyStart(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {

            if (deepVisit) {
                metadataEntity.accept(new DeepVisitor(prefix), metadataModel);
            } else {
                prefix += "  ";
                appendPrefix("E → ").append(metadataEntity).append(eol);
            }
        }

        @Override
        public void visitEntiyEnd(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {
            if (!deepVisit) {
                appendPrefix("E ← ").append(metadataEntity).append(eol);
                prefix = prefix.substring(2);
            }
        }

        @Override
        public void visitReversedAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            if (!deepVisit) {
                appendProperty("ReversedAssociation: ", metadataEntity, propertyName, propertyType.getType());
            }
        }

        @Override
        public void visitOneToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            if (!deepVisit) {
                appendProperty("Association: ", metadataEntity, propertyName, propertyType.getType());
            }
        }

        @Override
        public void visitManyToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            if (!deepVisit) {
                appendProperty("NmAssociation: ", metadataEntity, propertyName, propertyType.getType());
            }
        }

        @Override
        public void visitManyToOneAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            if (!deepVisit) {
                appendProperty("Required: ", metadataEntity, propertyName, propertyType.getType());
            }
        }

        @Override
        public void visitOneToManyAssociationInverse(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            if (!deepVisit) {
                appendProperty("OneToManyAssociationInverse: ", metadataEntity, propertyName, propertyType.getType());
            }
        }

        @Override
        public void visitProperty(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, String propertyType) {
            appendProperty("Property: ", metadataEntity, propertyName, propertyType);
        }

        @Override
        public String toString() {
            return builder.toString();
        }

        private StringBuilder appendProperty(String prefix, TopiaMetadataEntity metadataEntity, String propertyName, String propertyType) {
            return appendPrefix(prefix).append(metadataEntity).append("/").append(propertyName).append("(").append(metadataEntity.getDbColumnName(propertyName)).append(")→").append(propertyType).append(eol);
        }

        private  StringBuilder appendPrefix(String prefix) {
            return builder.append(this.prefix).append(prefix);
        }

    }

}
