package org.nuiton.topia.persistence.metadata;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Méta-modèle topia simplifié qui contient des informations utile pour des algorithmes générique sur les entités.
 *
 * Ce méta-modèle est juste un conteneur de méta-modèle d'entités.
 *
 * Created on 03/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class TopiaMetadataModel implements Iterable<TopiaMetadataEntity> {

    private static final Log log = LogFactory.getLog(TopiaMetadataModel.class);

    protected final Map<String, TopiaMetadataEntity> entities = new LinkedHashMap<>();

    public TopiaMetadataEntity getEntity(String type) {
        return entities.get(type);
    }

    public Optional<TopiaMetadataEntity> getOptionalEntity(String type) {
        return Optional.ofNullable(getEntity(type));
    }

    public void accept(TopiaMetadataModelVisitor visitor) {
        visitor.visitModelStart(this);
        for (TopiaMetadataEntity entity : entities.values()) {
            entity.accept(visitor, this);
        }
        visitor.visitModelEnd(this);
    }

    public TopiaMetadataEntity newEntity(String parent, String type, String dbSchemaName, String dbTableName) {
        Preconditions.checkState(!entities.containsKey(type), type + " already in cache");
        TopiaMetadataEntity clazz = new TopiaMetadataEntity(parent, type, dbSchemaName, dbTableName);
        entities.put(type, clazz);
        log.info(clazz.getType());
        return clazz;
    }

    public static TopiaMetadataModel load(URL url) throws IOException {
        try (Reader reader = new InputStreamReader(url.openStream())) {

            Gson gson = new GsonBuilder().create();
            return gson.fromJson(reader, TopiaMetadataModel.class);

        }
    }

    @Override
    public Iterator<TopiaMetadataEntity> iterator() {
        return entities.values().iterator();
    }

}
