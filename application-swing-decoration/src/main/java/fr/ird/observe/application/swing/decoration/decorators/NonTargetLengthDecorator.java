/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration.decorators;

import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import org.apache.commons.jxpath.JXPathContext;

/**
 * Decorator of {@link NonTargetLengthDto}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class NonTargetLengthDecorator extends ObserveDecorator<NonTargetLengthDto> {

    private static final long serialVersionUID = 1L;

    public NonTargetLengthDecorator() throws IllegalArgumentException, NullPointerException {
        super(NonTargetLengthDto.class,
              "${species/scientificLabel}$s##${length}$f##${gender}$s"
        );
    }

    @SuppressWarnings({"unchecked"})
    @Override
    protected Comparable<Comparable<?>> getTokenValue(
            JXPathContext jxcontext, String token) {
        Comparable<?> value =
                super.getTokenValue(jxcontext, token);
        if (token.equals("species/scientificLabel") &&
            "xx".equals(value)) {
            value = super.getTokenValue(jxcontext, "species/label");
        }
        if ("sex".equals(token)) {
            value = super.getTokenValue(jxcontext, "sex/label");
        }
        return (Comparable<Comparable<?>>) value;
    }


}
