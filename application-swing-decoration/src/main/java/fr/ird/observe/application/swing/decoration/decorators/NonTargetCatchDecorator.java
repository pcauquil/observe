/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration.decorators;

import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import org.apache.commons.jxpath.JXPathContext;

/**
 * Decorator of {@link NonTargetCatchDto}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class NonTargetCatchDecorator extends ObserveDecorator<NonTargetCatchDto> {

    private static final long serialVersionUID = 1L;

    public NonTargetCatchDecorator() {
        super(NonTargetCatchDto.class,
              "${species/scientificLabel}$s##${speciesFate/label}$s"
        );
    }

    @Override
    protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxcontext,
                                                      String token) {
        Comparable<Comparable<?>> value = super.getTokenValue(jxcontext, token);
        if (token.equals("species/scientificLabel") &&
            "xx".equals(String.valueOf(value))) {
            value = super.getTokenValue(jxcontext, "species/label");
        }
        return value;
    }
}
