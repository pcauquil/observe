package fr.ird.observe.application.swing.decoration.decorators;

/*-
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.decorator.JXPathDecorator;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class ReferentialReferenceDecorator<O extends ReferentialDto> extends ObserveDecorator<ReferentialReference<O>> implements Cloneable {

    private static final long serialVersionUID = 1L;

    public ReferentialReferenceDecorator(String expression) {
        super((Class) ReferentialReference.class, expression);
        for (int i = 0; i < nbToken; i++) {
            String property = getProperty(i);
            if (ReferentialDto.PROPERTY_CODE.equals(property)) {

                // On change le comparateur pour simuler le tri numérique si possible.
                contexts[i].setComparator(new JXPathComparator<ReferentialReference<O>>(property) {

                    @Override
                    public void init(JXPathDecorator<ReferentialReference<O>> decorator, List<ReferentialReference<O>> datas) {
                        clear();
                        for (ReferentialReference<O> data : datas) {
                            JXPathContext jxcontext = JXPathContext.newContext(data);
                            Comparable<Comparable<?>> key = getTokenValue(jxcontext, ReferentialDto.PROPERTY_CODE);
                            // on passe en 000000x comme ça si on a des nombres, ils seront bien triés
                            String value = key == null ? "" : StringUtils.leftPad(key.toString(), 6, "0");
                            valueCache.put(data, (Comparable) value);
                        }
                    }
                });
                break;
            }
        }
    }

    @Override
    protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxcontext, String token) {


        // assume all values are comparable
        Comparable<Comparable<?>> value;

        Object contextBean = jxcontext.getContextBean();

        if (contextBean instanceof ReferentialReference) {

            String[] tokens = token.split("/");

            value = getValueFromReference(tokens, (ReferentialReference) contextBean, 0);

            if (value == null) {
                value = (Comparable<Comparable<?>>) getDefaultNullValue(tokens[0]);
            }

        } else {
            value = super.getTokenValue(jxcontext, token);
        }

        return value;

    }

    @Override
    protected Comparable<?> getDefaultNullValue(String token) {
        return ReferentialDto.PROPERTY_CODE.equals(token) ? t("observe.common.nocode") : super.getDefaultNullValue(token);
    }
}
