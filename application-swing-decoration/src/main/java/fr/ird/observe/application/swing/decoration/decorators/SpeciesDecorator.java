/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration.decorators;

import fr.ird.observe.services.dto.referential.SpeciesDto;
import org.apache.commons.jxpath.JXPathContext;

import static org.nuiton.i18n.I18n.n;

/**
 * Decorator of {@link SpeciesDto}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SpeciesDecorator extends ObserveDecorator<SpeciesDto> {

    private static final long serialVersionUID = 1L;

    static {

        // pour avoir les traduction sur le nom de la propriété

        n("observe.common.faoCode");
        n("observe.common.species/scientificLabel");
        n("observe.common.scientificLabel");
        n("observe.common.homeId");
    }


    public SpeciesDecorator() {
        super(SpeciesDto.class,
              "${faoCode}$s##${scientificLabel}$s"
        );
    }

    @Override
    protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxcontext, String token) {
        Comparable<?> value;
        value = super.getTokenValue(jxcontext, token);
        if (token.equals(SpeciesDto.PROPERTY_SCIENTIFIC_LABEL) && "xx".equals(value)) {
            value = super.getTokenValue(jxcontext, "label");
        }
        return (Comparable<Comparable<?>>) value;
    }

}
