package fr.ird.observe.application.swing.decoration;

/*-
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.AbstractLoadingCache;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.util.ObserveUtil;
import org.apache.commons.lang3.StringUtils;
import org.atteo.evo.inflector.English;

import java.beans.Introspector;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 03/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ObserveI18nDecoratorHelper {

    private static final String OBSERVE_COMMON_PREFIX = "observe.common.";
    private static final String OBSERVE_TYPE_PREFIX = "observe.type.";

    public static List<Class> sortTypes(Collection<Class> types) {
        return ObserveUtil.sortTypes(types, klass -> t(ObserveI18nDecoratorHelper.getTypeI18nKey(klass)));
    }

    public static String getPropertyName(Class key) {
        String name = key.getSimpleName();
        name = StringUtils.removeEnd(name, "Dto");
        return Introspector.decapitalize(name);
    }

    public static String getPluralPropertyName(Class key) {
        return English.plural(getPropertyName(key), 2);
    }

    public static String getTypeI18nKey(Class key) {
        return TYPE_CACHE.get(key);
    }

    public static String getTypePluralI18nKey(String name) {
        name = StringUtils.removeEnd(name, "Dto");
        String propertyName = Introspector.decapitalize(name);
        return OBSERVE_TYPE_PREFIX + English.plural(propertyName);
    }

    public static String getTypePluralI18nKey(Class entityClass) {
        return TYPE_PLURAL_CACHE.get(entityClass);
    }

    public static String getPropertyI18nKey(String propertyName) {
        return getPropertyI18nKey(OBSERVE_COMMON_PREFIX, propertyName);
    }

    private static String getPropertyI18nKey(String prefix, String propertyName) {
        return prefix + Introspector.decapitalize(propertyName);
    }

    private static final ClassToI18nKeyCache TYPE_CACHE = new ClassToI18nKeyCache(type -> OBSERVE_TYPE_PREFIX + getPropertyName(type));

    private static final ClassToI18nKeyCache TYPE_PLURAL_CACHE = new ClassToI18nKeyCache(type -> OBSERVE_TYPE_PREFIX + getPluralPropertyName(type));

    private static class ClassToI18nKeyCache extends AbstractLoadingCache<Class, String> {

        private final Function<Class, String> function;

        private ClassToI18nKeyCache(Function<Class, String> function) {
            this.function = function;
        }

        @Override
        public String get(Class key) {
            if (!ObserveDto.class.isAssignableFrom(key)) {
                throw new IllegalArgumentException(key + " is not an Observe DTO");
            }
            return function.apply(key);
        }

        @Override
        public String getIfPresent(Object key) {
            return getUnchecked((Class) key);
        }
    }
}
