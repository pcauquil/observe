/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration.decorators;

import fr.ird.observe.services.dto.longline.TripLonglineDto;

/**
 * Decorator of {@link TripLonglineDto}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class TripLonglineDecorator extends ObserveDecorator<TripLonglineDto> {

    private static final long serialVersionUID = 1L;

    public TripLonglineDecorator() throws IllegalArgumentException, NullPointerException {
        super(TripLonglineDto.class,
              "${startDate}$td/%1$tm/%1$tY##${endDate}$td/%2$tm/%2$tY##${vessel/label}$s##${observerLabel}$s"
        );
    }

}
