/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration.decorators;

import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import org.apache.commons.jxpath.JXPathContext;

import static org.nuiton.i18n.I18n.t;

/**
 * Decorator of {@link LengthWeightParameterDto}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class LengthWeightParameterDecorator extends ObserveDecorator<LengthWeightParameterDto> {

    private static final long serialVersionUID = 1L;

    public LengthWeightParameterDecorator() throws IllegalArgumentException, NullPointerException {
        super(LengthWeightParameterDto.class,
              "${sex}$s##${ocean/label}$s##${species/scientificLabel}$s##" +
              t("observe.common.lengthWeightFormula") + " ${lengthWeightFormula}$s"
        );
    }

    @SuppressWarnings({"unchecked"})
    @Override
    protected Comparable<Comparable<?>> getTokenValue(
            JXPathContext jxcontext, String token) {
        Object value;
//        if (token.equals("ocean/" + libelle)) {
//            try {
//                value = super.getTokenValue(jxcontext,
//                                            token);
//            } catch (JXPathNotFoundException e) {
//                value = t("observe.common.inconnu");
//            }
//            value = t("observe.common.ocean") + ' ' + value;
//        } else {

        value = super.getTokenValue(jxcontext, token);
        if (token.equals("species/scientificLabel")
            && "xx".equals(value)) {
            value = super.getTokenValue(jxcontext, "species/label");
        }
        if (token.equals("sex")) {
            value = super.getTokenValue(jxcontext,
                                        "sex/label");
            value = t("observe.common.sex") + ' ' +
                    value;
        }
//        }
        return (Comparable<Comparable<?>>) value;
    }

    @Override
    protected Comparable<?> getDefaultUndefinedValue(String token) {
        if (token.startsWith("ocean")) {
            return t("observe.common.inconnu");
        }
        return super.getDefaultUndefinedValue(token);
    }
}
