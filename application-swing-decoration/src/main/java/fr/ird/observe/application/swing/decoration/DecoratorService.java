/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration;

import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.LengthWeightParameterDecorator;
import fr.ird.observe.application.swing.decoration.decorators.NonTargetCatchDecorator;
import fr.ird.observe.application.swing.decoration.decorators.NonTargetLengthDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ObjectObservedSpeciesDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ObserveDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.SpeciesDecorator;
import fr.ird.observe.application.swing.decoration.decorators.TargetCatchDecorator;
import fr.ird.observe.application.swing.decoration.decorators.TripLonglineDecorator;
import fr.ird.observe.application.swing.decoration.decorators.TripSeineDecorator;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SectionTemplate;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineObservedSystemDto;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import fr.ird.observe.util.GPSPoint;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;


/**
 * Le service de décoration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class DecoratorService extends DecoratorProvider {

//    /** Logger */
//    private static final Log log = LogFactory.getLog(DecoratorService.class);

//    /** Le pattern pour utiliser les clef i18n generees dans les entites */
//    private static final Pattern LABEL_PATTERN = Pattern.compile("observe\\.common\\.(.+)");

    public static final String HAULING_IDENTIFIER = "haulingIdentifier";

    public static final String TRIP_CONTEXT = "Trip";

    /** la locale du referentiel. */
    private ReferentialLocale referentialLocale;

    public DecoratorService(ReferentialLocale referentialLocale) {
        this.referentialLocale = referentialLocale;
        loadDecorators();
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public void setReferentialLocale(ReferentialLocale referentialLocale) {
        this.referentialLocale = referentialLocale;
    }

//    public Matcher getPropertyMatch(String regex) {
//        return LABEL_PATTERN.matcher(regex);
//    }

    public String decorate(Object o) {
        return decorate(null, o);

    }

    private String decorate(String context, Object o) {
        //FIXME
        if (o == null) {
            return null;
        }
        Decorator<Object> decorator = getDecorator(o, context);
        String result = "";
        if (decorator != null) {
            result = decorator.toString(o);
        }
        return result;
    }

    static {
        // pour avoir les traduction sur le nom de la propriété
        n("observe.common.vesselActivitySeine/label1");
        n("observe.common.vesselActivitySeine/label2");
        n("observe.common.vesselActivitySeine/label3");
        n("observe.common.vesselActivitySeine/label4");
        n("observe.common.vesselActivitySeine/label5");
        n("observe.common.vesselActivitySeine/label6");
        n("observe.common.vesselActivitySeine/label7");
        n("observe.common.vesselActivitySeine/label8");

        n("observe.common.vessel/label1");
        n("observe.common.vessel/label2");
        n("observe.common.vessel/label3");
        n("observe.common.vessel/label4");
        n("observe.common.vessel/label5");
        n("observe.common.vessel/label6");
        n("observe.common.vessel/label7");
        n("observe.common.vessel/label8");

        n("observe.common.ocean/label1");
        n("observe.common.ocean/label2");
        n("observe.common.ocean/label3");
        n("observe.common.ocean/label4");
        n("observe.common.ocean/label5");
        n("observe.common.ocean/label6");
        n("observe.common.ocean/label7");
        n("observe.common.ocean/label8");

        n("observe.common.species/scientificLabel");
        n("observe.common.observerLabel");
        n("observe.common.label");


//        n("observe.common.targetSample");
//        n("observe.common.targetLength");
//        n("observe.common.nonTargetSample");
//        n("observe.common.nonTargetLength");
//        n("observe.common.nonTargetCatch");
//        n("observe.common.setSeine");
//        n("observe.common.targetCatch");
//        n("observe.common.transmittingBuoy");
//        n("observe.common.objectObservedSpecies");
//        n("observe.common.schoolEstimate");
//        n("observe.common.objectSchoolEstimate");
//        n("observe.common.floatingObject");
//        n("observe.common.activitySeine");
//        n("observe.common.route");
//        n("observe.common.tripSeine");

//        n("observe.common.activityLongline");
//        n("observe.common.baitsComposition");
//        n("observe.common.basket");
//        n("observe.common.branchline");
//        n("observe.common.branchlinesComposition");
//        n("observe.common.catchLongline");
//        n("observe.common.encounter");
//        n("observe.common.setLongline");
//        n("observe.common.time");
//        n("observe.common.floatlinesComposition");
//        n("observe.common.hooksComposition");
//        n("observe.common.section");
//        n("observe.common.sensorUsed");
//        n("observe.common.sizeMeasure");
//        n("observe.common.tdr");
//        n("observe.common.tdrRecord");
//        n("observe.common.tripLongline");
//        n("observe.common.weightMeasure");
//        n("observe.common.gearUseFeaturesSeine");
//        n("observe.common.gearUseFeaturesLongline");
//        n("observe.common.longlineDetailComposition");

//        n("observe.common.vesselSizeCategory");
//        n("observe.common.harbour");
//        n("observe.common.country");
//        n("observe.common.vesselType");
//        n("observe.common.vessel");
//        n("observe.common.speciesGroup");
//        n("observe.common.species");
//        n("observe.common.sex");
//        n("observe.common.fpaZone");
//        n("observe.common.speciesList");
//        n("observe.common.person");
//        n("observe.common.ocean");
//        n("observe.common.organism");
//        n("observe.common.lengthWeightParameter");
//        n("observe.common.program");

//        n("observe.common.vesselActivitySeine");
//        n("observe.common.surroundingActivity");
//        n("observe.common.reasonForNullSet");
//        n("observe.common.reasonForNoFishing");
//        n("observe.common.speciesFate");
//        n("observe.common.objectFate");
//        n("observe.common.weightCategory");
//        n("observe.common.detectionMode");
//        n("observe.common.transmittingBuoyOperation");
//        n("observe.common.objectOperation");
//        n("observe.common.reasonForDiscard");
//        n("observe.common.speciesStatus");
//        n("observe.common.observedSystem");
//        n("observe.common.transmittingBuoyType");
//        n("observe.common.objectType");
//        n("observe.common.wind");
//
//        n("observe.common.baitHaulingStatus");
//        n("observe.common.baitSettingStatus");
//        n("observe.common.baitType");
//        n("observe.common.catchFateLongline");
//        n("observe.common.encounterType");
//        n("observe.common.healthness");
//        n("observe.common.hookPosition");
//        n("observe.common.hookSize");
//        n("observe.common.hookType");
//        n("observe.common.itemVerticalPosition");
//        n("observe.common.itemHorizontalPosition");
//        n("observe.common.lightsticksColor");
//        n("observe.common.lightsticksType");
//        n("observe.common.lineType");
//        n("observe.common.maturityStatus");
//        n("observe.common.mitigationType");
//        n("observe.common.sensorBrand");
//        n("observe.common.sensorDataFormat");
//        n("observe.common.sensorPosition");
//        n("observe.common.sensorType");
//        n("observe.common.settingShape");
//        n("observe.common.sizeMeasureType");
//        n("observe.common.stomacFullness");
//        n("observe.common.tripType");
//        n("observe.common.vesselActivityLongline");
//        n("observe.common.weightMeasureType");
//        n("observe.common.id");
//        n("observe.common.floatlineLengths");
//        n("observe.common.locode");
//        n("observe.common.name");
//        n("observe.common.gearCaracteristicType");
//        n("observe.common.gearCaracteristic");
//        n("observe.common.gear");
//        n("observe.common.gender");

//        n("observe.common.floatline1Length");
//        n("observe.common.floatline2Length");
//        n("observe.common.branchlineLength");
//        n("observe.common.tracelineLength");

//        n("observe.common.gearUseFeaturesMeasurementSeine");
//        n("observe.common.gearUseFeaturesMeasurementLongline");

    }

    @Override
    protected void loadDecorators() {
        if (referentialLocale == null) {
            // on n'enregistre pas les décorateur tant que la locale n'est pas
            // positionnée
            return;
        }

        Locale locale = referentialLocale.getLocale();

        String libelle = referentialLocale.getLibelle();

        registerReferentialAndReferentialReferenceDecorator(ProgramDto.class,
                                                            "[${gearTypePrefix}$s] " + t("observe.common.program") + " ${" + libelle + "}$s",
                                                            "[${gearTypePrefix}$s] " + t("observe.common.program") + " ${label}$s");
        registerReferentialAndReferentialReferenceDecorator(PersonDto.class, "${lastName}$s##${firstName}$s");
        registerReferentialAndReferentialReferenceDecorator(VesselSizeCategoryDto.class, "${code}$s##${gaugeLabel}$s##${capacityLabel}$s");
        registerReferentialAndReferentialReferenceDecorator(WindDto.class, "${code}$s##${label}$s##${speedRange}$s");

        registerReferentialAndReferentialReferenceDecorator(SensorBrandDto.class, "${code}$s##${brandName}$s");
        registerReferentialAndReferentialReferenceDecorator(HarbourDto.class, "${code}$s##${name}$s##${locode}$s");
        // Species decorator
        registerDecorator(new SpeciesDecorator());
        registerReferentialReferenceDecorator(SpeciesDto.class, "${faoCode}$s##${scientificLabel}$s");

        // LengthWeightParameter decorator
        registerDecorator(new LengthWeightParameterDecorator());
        registerReferentialReferenceDecorator(LengthWeightParameterDto.class,
                                              "${sex}$s##${ocean}$s##${species}$s##" + t("observe.common.lengthWeightFormula") + " ${lengthWeightFormula}$s");

        registerDefaultReferentialAndReferentialReferenceDecorator(SexDto.class, libelle);
        //FIXME Use startDate - endDate
        registerDefaultReferentialAndReferentialReferenceDecorator(FpaZoneDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(OceanDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(DetectionModeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(VesselDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(CountryDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(VesselTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(VesselActivitySeineDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SurroundingActivityDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ObservedSystemDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ObjectFateDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(OrganismDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ObjectTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ObjectOperationDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesStatusDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(TransmittingBuoyTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(TransmittingBuoyOperationDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ReasonForNullSetDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ReasonForNoFishingDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(WeightCategoryDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ReasonForDiscardDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesFateDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesGroupDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SpeciesListDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(BaitHaulingStatusDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(BaitSettingStatusDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(BaitTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(CatchFateLonglineDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(EncounterTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(HealthnessDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(HookPositionDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(HookSizeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(HookTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ItemVerticalPositionDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(ItemHorizontalPositionDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(LightsticksColorDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(LightsticksTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(LineTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(MaturityStatusDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(MitigationTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SensorDataFormatDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SensorTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SettingShapeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(SizeMeasureTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(StomacFullnessDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(TripTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(VesselActivityLonglineDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(WeightMeasureTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(GearCaracteristicTypeDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(GearCaracteristicDto.class, libelle);
        registerDefaultReferentialAndReferentialReferenceDecorator(GearDto.class, libelle);

        // Data decorators

        registerDataAndDataReferenceDecorator(RouteDto.class, "${date}$td/%1$tm/%1$tY");
        registerDataAndDataReferenceDecorator(ActivitySeineDto.class,
                                              "${time}$tH:%1$tM##${vesselActivitySeine/label}$s",
                                              "${time}$tH:%1$tM##${vesselActivitySeine}$s",
                                              " - ");
        registerDataAndDataReferenceDecorator(ActivitySeineStubDto.class, "${time}$tH:%1$tM");

        registerDataAndDataReferenceDecorator(ActivityLonglineDto.class,
                                              "${timeStamp}$td/%1$tm/%1$tY %1$tH:%1$tM##${vesselActivityLongline/label}$s",
                                              "${timeStamp}$td/%1$tm/%1$tY %1$tH:%1$tM##${vesselActivityLongline}$s",
                                              " - ");
        registerDataAndDataReferenceDecorator(SetSeineDto.class, t("observe.type.setSeine"));
        registerDataAndDataReferenceDecorator(SetLonglineDto.class, t("observe.type.setLongline"));
        registerDataAndDataReferenceDecorator(TripSeineGearUseDto.class, t("observe.type.tripSeineGearUse"));
        registerDataAndDataReferenceDecorator(TripLonglineGearUseDto.class, t("observe.type.tripLonglineGearUse"));
        registerDataAndDataReferenceDecorator(ActivitySeineObservedSystemDto.class, t("observe.type.activitySeineObservedSystem"));
        registerDataAndDataReferenceDecorator(SetSeineSchoolEstimateDto.class, t("observe.type.setSeineSchoolEstimate"));
        registerDataAndDataReferenceDecorator(SetSeineTargetCatchDto.class, t("observe.type.setSeineTargetCatch"));
        registerDataAndDataReferenceDecorator(SetSeineNonTargetCatchDto.class, t("observe.type.setSeineNonTargetCatch"));
        registerDataAndDataReferenceDecorator(NonTargetSampleDto.class, t("observe.type.nonTargetSample"));
        registerDataAndDataReferenceDecorator(TargetSampleDto.class, t("observe.type.targetSample"));
        registerDataAndDataReferenceDecorator(FloatingObjectTransmittingBuoyDto.class, t("observe.type.floatingObjectTransmittingBuoy"));
        registerDataAndDataReferenceDecorator(FloatingObjectSchoolEstimateDto.class, t("observe.type.floatingObjectSchoolEstimate"));
        registerDataAndDataReferenceDecorator(FloatingObjectObservedSpeciesDto.class, t("observe.type.floatingObjectObservedSpecies"));

        registerDataAndDataReferenceDecorator(ObjectSchoolEstimateDto.class,
                                              "${species/scientificLabel}$s##${weight}$d",
                                              "${species}$s##${weight}$d");
        registerDataAndDataReferenceDecorator(SchoolEstimateDto.class,
                                              "${species/scientificLabel}$s##${totalWeight}$d##${meanWeight}$d",
                                              "${species}$s##${totalWeight}$d##${meanWeight}$d");

        registerDataAndDataReferenceDecorator(TargetLengthDto.class,
                                              "${species/scientificLabel}$s##${length}$f##${count}$d",
                                              "${species}$s##${length}$f##${count}$d");

        registerDataAndDataReferenceDecorator(TransmittingBuoyDto.class,
                                              "${transmittingBuoyType/label}$s##${transmittingBuoyOperation/label}$s##${code}$s");

        registerDataAndDataReferenceDecorator(FloatingObjectDto.class,
                                              "DCP ${objectType/label}$s", "DCP ${objectType}$s", " - ");

        String settingIdentifier = l(locale, "observe.common.settingIdentifier");
        String haulingIdentifier = l(locale, "observe.common.haulingIdentifier");

        registerDataAndDataReferenceDecorator(SectionDto.class,
                                              " ${settingIdentifier}$s (" + settingIdentifier + ")##${haulingIdentifier}$s (" + haulingIdentifier + ")");

        registerDataAndDataReferenceDecorator(BasketDto.class,
                                              " ${settingIdentifier}$s (" + settingIdentifier + ")##${haulingIdentifier}$s (" + haulingIdentifier + ")");

        registerDataAndDataReferenceDecorator(BranchlineDto.class,
                                              " ${settingIdentifier}$s (" + settingIdentifier + ")##${haulingIdentifier}$s (" + haulingIdentifier + ")");

        registerDataAndDataReferenceDecorator(HAULING_IDENTIFIER, SectionDto.class, "${haulingIdentifier}$s");
        registerDataAndDataReferenceDecorator(HAULING_IDENTIFIER, BasketDto.class, "${haulingIdentifier}$s");
        registerDataAndDataReferenceDecorator(HAULING_IDENTIFIER, BranchlineDto.class, "${haulingIdentifier}$s");

        // TripSeine decorator
        registerDecorator(new TripSeineDecorator());
        registerDataReferenceDecorator(TripSeineDto.class,
                                       "${startDate}$td/%1$tm/%1$tY##${endDate}$td/%2$tm/%2$tY##${vessel}$s##${observer}$s");

        // TripLongline decorator
        registerDecorator(new TripLonglineDecorator());
        registerDataReferenceDecorator(TripLonglineDto.class,
                                       "${startDate}$td/%1$tm/%1$tY##${endDate}$td/%2$tm/%2$tY##${vessel}$s##${observer}$s");

        // Trip commun decorator
        registerDecorator(TRIP_CONTEXT,
                          new DataReferenceDecorator("${startDate}$td/%1$tm/%1$tY##${endDate}$td/%2$tm/%2$tY##${vessel}$s##${observer}$s"));

        // ObjectObservedSpecies decorator
        registerDecorator(new ObjectObservedSpeciesDecorator());

        // TargetLength decorator
        registerDecorator(new TargetCatchDecorator());

        // NonTargetCatch decorator
        registerDecorator(new NonTargetCatchDecorator());
        registerDataReferenceDecorator(NonTargetCatchDto.class, "${species}$s##${speciesFate}$s");

        // NonTargetLength decorator
        registerDecorator(new NonTargetLengthDecorator());

        registerDataAndDataReferenceDecorator(CatchLonglineDto.class, "${homeId}$s");

        registerDataAndDataReferenceDecorator(TdrDto.class, "${homeId}$s");

        //FIXME how to decorate ?
        registerDataAndDataReferenceDecorator(GearUseFeaturesMeasurementSeineDto.class, "${id}$s");

        registerDataAndDataReferenceDecorator(EncounterDto.class,
                                              "${encounterType/label}$s##${species/label}$s",
                                              "${encounterType}$s##${species}$s",
                                              " - ");

        registerDataAndDataReferenceDecorator(SensorUsedDto.class,
                "${sensorType/label}$s",
                "${sensorType}$s");


        // gps decorators
        registerObserveDecorator("activity-gps",
                                 ActivitySeineDto.class,
                                 t("observe.common.gps.activity"));
        registerObserveDecorator("gpsPoint-gps",
                                 GPSPoint.class,
                                 t("observe.common.gps.gpsPoint"));

        registerObserveDecorator(GPSPoint.class,
                                 "${time}$td/%1$tm/%1$tY %1$tH:%1$tM##${latitude}$s##${longitude}$s",
                                 " ");

        registerObserveDecorator(SectionTemplate.class, "${id}$s##${floatlineLengths}$s", " ");

    }

    public <T extends ReferentialDto> void sort(Class<T> type, List<ReferentialReference<T>> data) {
        new ReferentialReferenceComparator<>(type).sort(data);
    }

    public <T extends ReferentialDto> ReferentialReferenceDecorator<T> getReferentialReferenceDecorator(Class<T> referenceType) {
        return (ReferentialReferenceDecorator<T>) (ReferentialReferenceDecorator) getDecoratorByType(ReferentialReference.class, referenceType.getSimpleName());
    }

    public <T extends DataDto> DataReferenceDecorator<T> getDataReferenceDecorator(Class<T> referenceType) {
        return (DataReferenceDecorator<T>) (DataReferenceDecorator) getDecoratorByType(DataReference.class, referenceType.getSimpleName());
    }

    public <T extends DataDto> DataReferenceDecorator<T> getDataReferenceDecorator(Class<T> referenceType, String context) {
        return (DataReferenceDecorator<T>) (DataReferenceDecorator) getDecoratorByType(DataReference.class, referenceType.getSimpleName() + context);
    }

    public Decorator getReferenceDecorator(Class referenceType) {
        Decorator decorator;

        if (DataDto.class.isAssignableFrom(referenceType)) {
            decorator = this.getDataReferenceDecorator(referenceType);
        } else {
            decorator = this.getReferentialReferenceDecorator(referenceType);
        }

        return decorator;
    }

    public DataReferenceDecorator getTripReferenceDecorator(DataReference tripDto) {
        DataReferenceDecorator decorator;
        if (tripDto.getType().isAssignableFrom(TripSeineDto.class)) {
            decorator = (DataReferenceDecorator) getDataReferenceDecorator(TripSeineDto.class);
        } else {
            decorator = (DataReferenceDecorator) getDataReferenceDecorator(TripLonglineDto.class);
        }
        return decorator;
    }

    private <T extends ReferentialDto> void registerDefaultReferentialAndReferentialReferenceDecorator(Class<T> referenceType, String libelle) {
        registerReferentialReferenceDecorator(referenceType, "${code}$s##${label}$s");
        registerObserveDecorator(referenceType, "${code}$s##${" + libelle + "}$s", " ");
    }

    private <T extends ReferentialDto> void registerReferentialAndReferentialReferenceDecorator(Class<T> referenceType, String expression) {
        registerReferentialReferenceDecorator(referenceType, expression);
        registerObserveDecorator(referenceType, expression);
    }

    private <T extends ReferentialDto> void registerReferentialAndReferentialReferenceDecorator(Class<T> referenceType, String expression, String referenceExpression) {
        registerReferentialReferenceDecorator(referenceType, referenceExpression);
        registerObserveDecorator(referenceType, expression);
    }

    private <T extends ReferentialDto> void registerReferentialReferenceDecorator(Class<T> referenceType, String referenceExpression) {
        registerDecorator(referenceType.getSimpleName(), new ReferentialReferenceDecorator<>(referenceExpression));
    }

    private <T extends IdDto> void registerDataAndDataReferenceDecorator(String context, Class<T> referenceType, String expression) {
        registerDataReferenceDecorator(context, referenceType, expression);
        registerObserveDecorator(context, referenceType, expression, " ");
    }

    private <T extends IdDto> void registerDataAndDataReferenceDecorator(Class<T> referenceType, String expression) {
        registerDataReferenceDecorator(referenceType, expression);
        registerObserveDecorator(referenceType, expression, " ");
    }

    private <T extends IdDto> void registerDataAndDataReferenceDecorator(Class<T> referenceType, String expression, String referenceExpression) {
        registerDataReferenceDecorator(referenceType, referenceExpression);
        registerObserveDecorator(referenceType, expression, " ");
    }

    private <T extends IdDto> void registerDataAndDataReferenceDecorator(Class<T> referenceType, String expression, String referenceExpression, String separator) {
        registerDataReferenceDecorator(referenceType, referenceExpression);
        registerObserveDecorator(referenceType, expression, separator);
    }

    private <T extends IdDto> void registerDataReferenceDecorator(Class<T> referenceType, String referenceExpression) {
        registerDataReferenceDecorator(null, referenceType, referenceExpression);
    }

    private <T extends IdDto> void registerDataReferenceDecorator(String context, Class<T> referenceType, String referenceExpression) {
        registerDecorator(referenceType.getSimpleName() + (context == null ? "" : context), new DataReferenceDecorator<>(referenceExpression));
    }

    private <T> void registerObserveDecorator(Class<T> referenceType, String expression) {
        registerDecorator(new ObserveDecorator<>(referenceType, expression));
    }

    private <T> void registerObserveDecorator(Class<T> referenceType, String expression, String separator) {
        registerDecorator(new ObserveDecorator<>(referenceType, expression, separator));
    }

    private <T> void registerObserveDecorator(String context, Class<T> referenceType, String expression) {
        registerDecorator(context, new ObserveDecorator<>(referenceType, expression));
    }

    private <T> void registerObserveDecorator(String context, Class<T> referenceType, String expression, String separator) {
        registerDecorator(context, new ObserveDecorator<>(referenceType, expression, separator));
    }

    private class ReferentialReferenceComparator<T extends ReferentialDto> implements Comparator<ReferentialReference<T>> {

        private final ReferentialReferenceDecorator<T> decorator;

        private final Map<ReferentialReference<T>, String> cache = new HashMap<>();

        private ReferentialReferenceComparator(Class<T> type) {
            decorator = getReferentialReferenceDecorator(type);
        }

        private String get(ReferentialReference<T> id) {
            String value = cache.get(id);
            if (value == null) {
                value = decorator.toString(id);
                cache.put(id, value);
            }
            return value;
        }

        @Override
        public int compare(ReferentialReference<T> o1, ReferentialReference<T> o2) {
            return get(o1).compareTo(get(o2));
        }

        public void sort(List<ReferentialReference<T>> data) {
            Collections.sort(data, this);
            cache.clear();
        }
    }
}
