package fr.ird.observe.application.swing.validators;

/*
 * #%L
 * ObServe :: Application Swing Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineStubDto;
import fr.ird.observe.services.dto.longline.SetLonglineStubDtos;

import java.util.Optional;

/**
 * Created on 12/7/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class SetLonglineUniqueNumberDtoValidator extends FieldValidatorSupport {

    @Override
    public void validate(Object object) throws ValidationException {

        SetLonglineDto setLongline = (SetLonglineDto) object;
        Integer number = setLongline.getNumber();

        if (number != null) {

            Optional<SetLonglineStubDto> sameNumberSetLonglineOptional = setLongline.getOtherSets()
                                                                                    .stream()
                                                                                    .filter(SetLonglineStubDtos.newNumberPredicate(number))
                                                                                    .findFirst();

            if (sameNumberSetLonglineOptional.isPresent()) {

                DataReference<ActivityLonglineDto> activityLonglineRef = sameNumberSetLonglineOptional.get().getActivityLongline();

                DecoratorService decoratorService = (DecoratorService) stack.findValue("decoratorService");
                DataReferenceDecorator<ActivityLonglineDto> decorator = decoratorService.getDataReferenceDecorator(ActivityLonglineDto.class);

                stack.set("duplicatedActivity", decorator.toString(activityLonglineRef));

                addFieldError(getFieldName(), object);

            }

        }

    }

    @Override
    public String getValidatorType() {
        return "setLonglineUniqueNumber";
    }
}
