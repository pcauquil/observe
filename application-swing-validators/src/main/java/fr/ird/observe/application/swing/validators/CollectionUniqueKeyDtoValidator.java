package fr.ird.observe.application.swing.validators;

/*
 * #%L
 * ObServe :: Application Swing Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ird.observe.services.dto.IdDto;
import org.nuiton.validator.xwork2.field.CollectionUniqueKeyValidator;

/**
 * Created on 1/23/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class CollectionUniqueKeyDtoValidator extends CollectionUniqueKeyValidator {

    @Override
    protected Integer getUniqueKeyHashCode(Object o) throws ValidationException {
        if (o instanceof IdDto) {
            o = ((IdDto) o).getId();
        }
        return super.getUniqueKeyHashCode(o);
    }

    @Override
    public String getValidatorType() {
        return "collectionUniqueKeyDto";
    }

}
