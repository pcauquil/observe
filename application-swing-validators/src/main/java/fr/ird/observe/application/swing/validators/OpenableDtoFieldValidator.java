package fr.ird.observe.application.swing.validators;

/*
 * #%L
 * ObServe :: Application Swing Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.OpenableDto;
import org.nuiton.decorator.Decorator;

import java.util.Collection;

/**
 * <!-- START SNIPPET: javadoc --> OpenableFieldValidator verifie qu'un
 * objet ou qu'une collection d'objet cloturable est bien
 * fermée.
 * <p/>
 * <!-- END SNIPPET: javadoc -->
 * <p/>
 * <p/>
 * <!-- START SNIPPET: parameters --> <ul> <li>fieldName - The field name this
 * validator is validating. Required if using Plain-Validator Syntax otherwise
 * not required</li> </ul> <!-- END SNIPPET: parameters -->
 * <p/>
 * <p/>
 * <pre>
 * <!-- START SNIPPET: examples -->
 *     &lt;validators&gt;
 *         &lt;!-- Plain-Validator Syntax --&gt;
 *         &lt;validator type="openable"&gt;
 *             &lt;param name="fieldName"&gt;route&lt;/param&gt;
 *             &lt;message&gt;existing unclosed routes&lt;/message&gt;
 *         &lt;/validator&gt;
 * <p/>
 *         &lt;!-- Field-Validator Syntax --&gt;
 *         &lt;field name="route"&gt;
 *         	  &lt;field-validator type="openable"&gt;
 *                 &lt;message&gt;existing unclosed routes&lt;/message&gt;
 *            &lt;/field-validator&gt;
 *         &lt;/field&gt;
 *     &lt;/validators&gt;
 * <!-- END SNIPPET: examples -->
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class OpenableDtoFieldValidator extends FieldValidatorSupport {

    String openValueAsString;

    public String getOpenValueAsString() {
        return openValueAsString;
    }

    @Override
    public void validate(Object object) throws ValidationException {
        String fieldName = getFieldName();

        Object value = getFieldValue(fieldName, object);

        boolean result = true;

        DataDto dataValue = null;

        if (value != null) {
            if (value instanceof Collection<?>) {
                // on est sur une collection,
                // on regarde si l'un des objets est ouvert
                for (Object o : (Collection<?>) value) {
                    dataValue = (DataDto) o;
                    String id = dataValue.getId();

                    result = (boolean) stack.findValue("!openDataManager.isOpen(\"" + dataValue.getId() + "\")");

                    if (!result) {
                        // on objet ouvert a été trouvé
                        // on peut arréter le parcours
                        break;
                    }
                }
            } else if (value instanceof OpenableDto) {
                dataValue = (DataDto) value;
                result = (boolean) stack.findValue("!openDataManager.isOpen(\"" + dataValue.getId() + "\")");
            } else {
                // pas un type connu pour ce validateur
                throw new ValidationException(
                        "le type " + value.getClass().getName() +
                        " n'est pas pris en charge pas ce validateur");
            }
        }


        if (!result) {

            DecoratorService decoratorService = (DecoratorService) stack.findValue("decoratorService");
            Decorator<?> decorator = decoratorService.getDecorator(dataValue);

            if (log.isDebugEnabled()) {
                log.debug("decorator to use : " + decorator);
            }
            if (decorator != null) {
                openValueAsString = decorator.toString(dataValue);
                log.debug("decorator message " + openValueAsString);
            }
            try {
                // on a decouvert au moins un objet non fermé
                addFieldError(fieldName, dataValue);
            } finally {
                openValueAsString = null;
            }
        }
    }

    @Override
    public String getValidatorType() {
        return "openable";
    }
}
