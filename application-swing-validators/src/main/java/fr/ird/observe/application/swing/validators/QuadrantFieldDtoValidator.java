package fr.ird.observe.application.swing.validators;

/*-
 * #%L
 * ObServe :: Application Swing Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

/**
 * Created on 02/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class QuadrantFieldDtoValidator extends FieldValidatorSupport {

    private String ocean;

    public void setOcean(String ocean) {
        this.ocean = ocean;
    }

    @Override
    public void validate(Object object) throws ValidationException {

        String fieldName = getFieldName();
        if (fieldName == null) {
            throw new ValidationException("No parameter 'fieldName' filled");
        }

        ReferentialReference<OceanDto> ocean = (ReferentialReference<OceanDto>) getFieldValue(this.ocean, object);
        if (ocean == null) {
            throw new ValidationException("could not find trip named: " + this.ocean);
        }

        Integer quadrant = (Integer) getFieldValue(fieldName, object);
        if (quadrant == null) {
            return;
        }

        Integer code = Integer.valueOf((String) ocean.getPropertyValue(OceanDto.PROPERTY_CODE));
        boolean valid = code == 3
                || (code == 1 && (quadrant == 1 || quadrant == 2 || quadrant == 3 || quadrant == 4))
                || (code == 2 && (quadrant == 1 || quadrant == 2));

        if (!valid) {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "quadrantDto";
    }

}
