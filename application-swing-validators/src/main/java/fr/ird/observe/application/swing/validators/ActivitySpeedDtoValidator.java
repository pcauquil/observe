package fr.ird.observe.application.swing.validators;

/*
 * #%L
 * ObServe :: Application Swing Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.util.GPSPoint;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.xwork2.field.CollectionFieldExpressionValidator;

/**
 * <!-- START SNIPPET: javadoc --> ActivityspeedValidator vérifie que
 * la cohérence de vitesses entre toutes les activités d'une route. <!-- END SNIPPET: javadoc
 * -->
 *
 *
 * <!-- START SNIPPET: parameters --> <ul> <li>fieldName - The field name this
 * validator is validating. Required if using Plain-Validator Syntax otherwise
 * not required</li> </ul> <!-- END SNIPPET: parameters -->
 *
 * <pre>
 * <!-- START SNIPPET: examples -->
 *     &lt;validators&gt;
 *         &lt;!-- Plain-Validator Syntax --&gt;
 *         &lt;validator type="invalidLochMatin"&gt;
 *             &lt;param name="fieldName"&gt;startLogValue&lt;/param&gt;
 *             &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *         &lt;/validator&gt;
 *
 *         &lt;!-- Field-Validator Syntax --&gt;
 *         &lt;field name="startLogValue"&gt;
 *         	  &lt;field-validator type="invalidLochMatin"&gt;
 *                 &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *            &lt;/field-validator&gt;
 *         &lt;/field&gt;
 *     &lt;/validators&gt;
 * <!-- END SNIPPET: examples -->
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ActivitySpeedDtoValidator extends FieldValidatorSupport {

    /** Logger. */
    private static final Log LOG = LogFactory.getLog(ActivitySimpleSpeedDtoValidator.class);

    private CollectionFieldExpressionValidator delegate;

    private Float speed;

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    protected String invalidActivity;

    public String getInvalidActivity() {
        return invalidActivity;
    }

    protected String decorate(ActivitySeineStubDto activitySeine) {
        DecoratorService decoratorService = (DecoratorService) stack.findValue("decoratorService");
        return decoratorService.getDecoratorByType(ActivitySeineStubDto.class).toString(activitySeine);
    }

    protected String decorate(GPSPoint currentPoint) {
        DecoratorService decoratorService = (DecoratorService) stack.findValue("decoratorService");
        return decoratorService.getDecoratorByType(GPSPoint.class).toString(currentPoint);
    }

    public CollectionFieldExpressionValidator getDelegate(final RouteDto route) {
        if (delegate == null) {
            delegate = new CollectionFieldExpressionValidator() {

                @Override
                protected boolean validateOneEntry(Object object) {

                    c.addCurrent(object);

                    ActivitySeineStubDto previousActivity = (ActivitySeineStubDto) c.getPrevious();
                    ActivitySeineStubDto currentActivity = (ActivitySeineStubDto) c.getCurrent();

                    if (previousActivity == null) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("No previous activity for current activity : " + decorate(currentActivity) + ", skip speed computation");
                        }
                        return true;
                    }

                    if (previousActivity.getLatitude() == null || previousActivity.getLongitude() == null) {
                        // cas limite (pas de précédent ou position non renseigne)

                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Missing latitude or longitude on previous activity : " + decorate(previousActivity) + ", skip speed computation");
                        }

                        return true;
                    }

                    if (currentActivity.getLongitude() == null || currentActivity.getLatitude() == null) {
                        // cas limite (pas de précédent ou position non renseigne)
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Missing latitude or longitude on current activity : " + decorate(currentActivity) + ", skip speed computation");
                        }
                        return true;
                    }

                    GPSPoint previousPoint = GPSPoint.newPoint(route, previousActivity);
                    GPSPoint currentPoint = GPSPoint.newPoint(route, currentActivity);

                    float computedSpeed = previousPoint.getSpeed(currentPoint);

                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Speed computed between previous activity point " + decorate(previousPoint) + " to current activity point " + decorate(currentPoint) + ", speed is : " + computedSpeed);
                    }

                    boolean valid = computedSpeed <= speed;

                    if (!valid) {
                        stack.set("foundSpeed", computedSpeed);

                        invalidActivity = decorate(currentActivity);

                        if (LOG.isInfoEnabled()) {
                            LOG.info("Speed from " +
                                             decorate(previousActivity) +
                                             " to " + invalidActivity +
                                             " is " + computedSpeed +
                                             " which is more thant authorized one " +
                                             speed);
                        }
                    }
                    return valid;
                }

                @Override
                public String getMessage(Object object) {
                    boolean pop = false;
                    if (!stack.getRoot().contains(ActivitySpeedDtoValidator.this)) {
                        stack.push(ActivitySpeedDtoValidator.this);
                        pop = true;
                    }
                    try {
                        return super.getMessage(object);
                    } finally {
                        if (pop) {
                            stack.pop();
                        }
                    }
                }
            };
            delegate.setCollectionFieldName(RouteDto.PROPERTY_ACTIVITY_SEINE);
            delegate.setMode(CollectionFieldExpressionValidator.Mode.ALL);
            delegate.setValueStack(stack);
            delegate.setUseSensitiveContext(true);
            delegate.setExpressionForFirst(null);
            delegate.setExpressionForLast(null);
            delegate.setFieldName(getFieldName());
            delegate.setExpression("true");
            delegate.setMessageKey(getMessageKey());
            delegate.setDefaultMessage(getDefaultMessage());
            delegate.setValidatorContext(getValidatorContext());
        }
        return delegate;
    }

    @Override
    public void validate(Object object) throws ValidationException {

        if (speed == null) {
            throw new ValidationException("le parametre speed est obligatoire");
        }

        invalidActivity = null;

        getDelegate((RouteDto) object).validate(object);
    }

    @Override
    public String getValidatorType() {
        return "activitySpeed";
    }
}
