package fr.ird.observe.services.service.actions.report;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportOperation;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import fr.ird.observe.services.dto.actions.report.ReportVariable;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ReportBuilderTest {

    ReportBuilder reportBuilder;

    @Before
    public void beforeTest() {
        reportBuilder = new ReportBuilder();
    }


    @Test
    public void loadTest() throws Exception {
        URL reportFileUrl = getClass().getResource("observe-reports-test.properties");

        List<Report> reports = reportBuilder.load(reportFileUrl);

        Assert.assertNotNull(reports);
        Assert.assertEquals(2, reports.size());

        /*
        Report 1
         */

        Report report1 = reports.get(0);

        Assert.assertNotNull(report1);
        Assert.assertEquals("reportOne", report1.getId());
        Assert.assertEquals("Rapport numéro 1", report1.getName());
        Assert.assertEquals("Description du rapport numéro 1", report1.getDescription());
        Assert.assertEquals(4, report1.getColumns());
        Assert.assertEquals("Colonne 1", report1.getColumnHeaders()[0]);
        Assert.assertEquals("Colonne 2", report1.getColumnHeaders()[1]);
        Assert.assertEquals("Colonne 3", report1.getColumnHeaders()[2]);
        Assert.assertEquals("Dernière colonne", report1.getColumnHeaders()[3]);

        Assert.assertEquals(0, report1.getVariables().length);
        Assert.assertEquals(0, report1.getRepeatVariables().length);
        Assert.assertEquals(0, report1.getOperations().length);
        Assert.assertEquals(1, report1.getRequests().length);

        ReportRequest request1_1 = report1.getRequests()[0];
        Assert.assertNotNull(request1_1);
        Assert.assertEquals(0, request1_1.getX());
        Assert.assertEquals(0, request1_1.getY());
        Assert.assertEquals("Select * from uneTable;", request1_1.getRequest());

        /*
        Report 2
         */

        Report report2 = reports.get(1);

        Assert.assertNotNull(report2);
        Assert.assertEquals("reportTwo", report2.getId());
        Assert.assertEquals("Rapport numéro 2", report2.getName());
        Assert.assertEquals("Description du rapport numéro 2", report2.getDescription());
        Assert.assertEquals(5, report2.getColumns());
        Assert.assertEquals("Colonne 1", report2.getColumnHeaders()[0]);
        Assert.assertEquals("Colonne 2", report2.getColumnHeaders()[1]);
        Assert.assertEquals("Colonne 3", report2.getColumnHeaders()[2]);
        Assert.assertEquals("Avant dernière colonne", report2.getColumnHeaders()[3]);
        Assert.assertEquals("Total", report2.getColumnHeaders()[4]);

        Assert.assertEquals(1, report2.getVariables().length);
        ReportVariable variable = report2.getVariables()[0];
        Assert.assertNotNull(variable);
        Assert.assertEquals("groupId", variable.getName());
        Assert.assertEquals(SpeciesGroupDto.class, variable.getType());
        Assert.assertEquals("From SpeciesGroupImpl ge Order By ge.code", variable.getRequest());
        Assert.assertNull(variable.getValues());
        Assert.assertNull(variable.getSelectedValue());

        Assert.assertEquals(1, report2.getRepeatVariables().length);
        ReportVariable repeatVariable = report2.getRepeatVariables()[0];
        Assert.assertNotNull(repeatVariable);
        Assert.assertEquals("typeObjetId", repeatVariable.getName());
        Assert.assertEquals(String.class, repeatVariable.getType());
        Assert.assertEquals("Select id from uneTable where group = :groupId order By code;", repeatVariable.getRequest());
        Assert.assertNull(repeatVariable.getValues());
        Assert.assertNull(repeatVariable.getSelectedValue());


        Assert.assertEquals(1, report2.getOperations().length);
        ReportOperation operation = report2.getOperations()[0];
        Assert.assertEquals(ReportOperation.SumRow, operation);


        Assert.assertEquals(2, report2.getRequests().length);

        ReportRequest request2_1 = report2.getRequests()[0];
        Assert.assertNotNull(request2_1);
        Assert.assertEquals(0, request2_1.getX());
        Assert.assertEquals(0, request2_1.getY());
        Assert.assertEquals("Select * from uneTable where id = :typeObjectId;", request2_1.getRequest());
        ReportRequest.RequestRepeat repeat1 = request2_1.getRepeat();
        Assert.assertNotNull(repeat1);
        Assert.assertEquals("typeObjetId", repeat1.getVariableName());
        Assert.assertEquals(ReportRequest.RequestLayout.column, repeat1.getLayout());

        ReportRequest request2_2 = report2.getRequests()[0];
        Assert.assertNotNull(request2_2);
        Assert.assertEquals(0, request2_2.getX());
        Assert.assertEquals(0, request2_2.getY());
        Assert.assertEquals("Select * from uneTable where id = :typeObjectId;", request2_2.getRequest());
        ReportRequest.RequestRepeat repeat2 = request2_2.getRepeat();
        Assert.assertNotNull(repeat2);
        Assert.assertEquals("typeObjetId", repeat2.getVariableName());
        Assert.assertEquals(ReportRequest.RequestLayout.column, repeat2.getLayout());

    }

}
