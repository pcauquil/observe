package fr.ird.observe.services.service.trip;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.services.service.ObserveBlobsContainer;

/**
 * Created on 27/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ImportTripRequest implements ObserveDto {

    private final String programId;
    private final String tripId;
    private final byte[] sqlContent;
    private final ImmutableSet<ObserveBlobsContainer> blobsContainers;

    public ImportTripRequest(ExportTripResult exportTripResult) {
        this.programId = exportTripResult.getProgramId();
        this.tripId = exportTripResult.getTripId();
        this.sqlContent = exportTripResult.getSqlContent();
        this.blobsContainers = exportTripResult.getBlobsContainers();
    }

    public String getProgramId() {
        return programId;
    }

    public String getTripId() {
        return tripId;
    }

    public byte[] getSqlContent() {
        return sqlContent;
    }

    public ImmutableSet<ObserveBlobsContainer> getBlobsContainers() {
        return blobsContainers;
    }

}
