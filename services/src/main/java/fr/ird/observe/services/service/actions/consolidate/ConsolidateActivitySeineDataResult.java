package fr.ird.observe.services.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.services.dto.constants.seine.SchoolType;

import java.io.Serializable;

/**
 * Pour retourner le résultat de la consolidation d'une activité de type Seine.
 *
 * Un tel objet est créé uniquement si des modifications on été effectuée sur l'activité.
 *
 * Created on 28/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ConsolidateActivitySeineDataResult implements Serializable,ObserveDto {

    private static final long serialVersionUID = 1L;

    /**
     * L'indentifiant de l'activité.
     */
    protected String activitySeineId;

    /**
     * Le libellé de l'activité.
     */
    protected String activitySeineLabel;

//    /**
//     * L'ancien type de banc (si le type de banc a été modifié).
//     */
//    protected String oldSchoolType;
//
//    /**
//     * Le nouveau type de banc (s'il a été modifié).
//     */
//    protected String newSchoolType;

    /**
     * L'ensemble des modifications sur les échantillons cibles.
     */
    protected ImmutableSet<TargetLengthModification> targetLengthModifications;

    /**
     * L'ensemble des modifications sur les échantillons non cibles.
     */
    protected ImmutableSet<NonTargetLengthModification> nonTargetLengthModifications;

    /**
     * L'ensemble des modifications sur les captures non cibles.
     */
    protected ImmutableSet<NonTargetCatchModification> nonTargetCatchModifications;
    private SchoolType oldSchoolType;
    private SchoolType newSchoolType;

    public String getActivitySeineId() {
        return activitySeineId;
    }

    public String getActivitySeineLabel() {
        return activitySeineLabel;
    }

    public ImmutableSet<TargetLengthModification> getTargetLengthModifications() {
        return targetLengthModifications;
    }

    public ImmutableSet<NonTargetLengthModification> getNonTargetLengthModifications() {
        return nonTargetLengthModifications;
    }

    public ImmutableSet<NonTargetCatchModification> getNonTargetCatchModifications() {
        return nonTargetCatchModifications;
    }

    public SchoolType getOldSchoolType() {
        return oldSchoolType;
    }

    public SchoolType getNewSchoolType() {
        return newSchoolType;
    }

    public static class TargetLengthModification implements Serializable {

        private static final long serialVersionUID = 1L;

        protected String targetLengthId;

        protected String speciesLabel;

        protected String propertyName;

        protected Float newValue;

        public String getTargetLengthId() {
            return targetLengthId;
        }

        public String getSpeciesLabel() {
            return speciesLabel;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public Float getNewValue() {
            return newValue;
        }

        public void setTargetLengthId(String targetLengthId) {
            this.targetLengthId = targetLengthId;
        }

        public void setSpeciesLabel(String speciesLabel) {
            this.speciesLabel = speciesLabel;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public void setNewValue(Float newValue) {
            this.newValue = newValue;
        }
    }

    public static class NonTargetLengthModification implements Serializable {

        private static final long serialVersionUID = 1L;

        protected String nonTargetLengthId;

        protected String speciesLabel;

        protected String propertyName;

        protected Float newValue;

        public String getNonTargetLengthId() {
            return nonTargetLengthId;
        }

        public String getSpeciesLabel() {
            return speciesLabel;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public Float getNewValue() {
            return newValue;
        }

        public void setNonTargetLengthId(String nonTargetLengthId) {
            this.nonTargetLengthId = nonTargetLengthId;
        }

        public void setSpeciesLabel(String speciesLabel) {
            this.speciesLabel = speciesLabel;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public void setNewValue(Float newValue) {
            this.newValue = newValue;
        }
    }

    public static class NonTargetCatchModification implements Serializable {

        private static final long serialVersionUID = 1L;

        protected String nonTargetCatchId;

        protected String speciesLabel;

        protected String propertyName;

        protected Number newValue;

        public String getNonTargetCatchId() {
            return nonTargetCatchId;
        }

        public String getSpeciesLabel() {
            return speciesLabel;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public Number getNewValue() {
            return newValue;
        }

        public void setNonTargetCatchId(String nonTargetCatchId) {
            this.nonTargetCatchId = nonTargetCatchId;
        }

        public void setSpeciesLabel(String speciesLabel) {
            this.speciesLabel = speciesLabel;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public void setNewValue(Number newValue) {
            this.newValue = newValue;
        }
    }

    public void setActivitySeineId(String activitySeineId) {
        this.activitySeineId = activitySeineId;
    }

    public void setActivitySeineLabel(String activitySeineLabel) {
        this.activitySeineLabel = activitySeineLabel;
    }

    public void setSchoolTypeChanged(SchoolType oldSchoolType, SchoolType newSchoolType) {
        this.oldSchoolType = oldSchoolType;
        this.newSchoolType = newSchoolType;
    }

    public void setTargetLengthModifications(ImmutableSet<TargetLengthModification> targetLengthModifications) {
        this.targetLengthModifications = targetLengthModifications;
    }

    public void setNonTargetLengthModifications(ImmutableSet<NonTargetLengthModification> nonTargetLengthModifications) {
        this.nonTargetLengthModifications = nonTargetLengthModifications;
    }

    public void setNonTargetCatchModifications(ImmutableSet<NonTargetCatchModification> nonTargetCatchModifications) {
        this.nonTargetCatchModifications = nonTargetCatchModifications;
    }

}
