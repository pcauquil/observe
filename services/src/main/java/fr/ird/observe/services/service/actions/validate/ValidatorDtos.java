package fr.ird.observe.services.service.actions.validate;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.nuiton.validator.NuitonValidatorScope;

import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ValidatorDtos {

    public static Set<ValidatorDto> filter(Set<ValidatorDto> validators,
                                           boolean validateData,
                                           boolean validateReferential,
                                           EnumSet<NuitonValidatorScope> scopes,
                                           String context) {

        return validators.stream().filter(input ->
                                                  (validateData && !ReferentialDto.class.isAssignableFrom(input.getType())
                                                          || validateReferential && ReferentialDto.class.isAssignableFrom(input.getType()))
                                                          && scopes.contains(input.getScope())
                                                          && context.equals(input.getContext())).
                                 collect(Collectors.toSet());

    }


}
