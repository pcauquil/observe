package fr.ird.observe.services.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.util.Date;
import java.util.Objects;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeDiffState implements Comparable<ReferentialSynchronizeDiffState> {

    private final String id;
    private final Date lastUpdateDate;
    private final long version;
    private final boolean disabled;
    public static final int EQUALS = 0;
    public static final int BEFORE = -1;
    public static final int AFTER = 1;

    ReferentialSynchronizeDiffState(String id, Date lastUpdateDate, long version, boolean disabled) {
        this.id = id;
        this.lastUpdateDate = lastUpdateDate;
        this.version = version;
        this.disabled = disabled;
    }

    public String getId() {
        return id;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public boolean isEnabled() {
        return !disabled;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public long getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("id", id)
                          .add("lastUpdateDate", lastUpdateDate)
                          .add("version", version)
                          .add("disabled", disabled)
                          .toString();
    }

    @Override
    public int compareTo(ReferentialSynchronizeDiffState o) {
        if (Objects.equals(lastUpdateDate.getTime(), o.lastUpdateDate.getTime()) && Objects.equals(version, o.getVersion())) {
            return EQUALS;
        }
        return lastUpdateDate.before(o.getLastUpdateDate()) || version < o.getVersion() ? BEFORE : AFTER;
    }
}
