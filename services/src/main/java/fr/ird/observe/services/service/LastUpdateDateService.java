package fr.ird.observe.services.service;

import fr.ird.observe.services.ObserveService;

/**
 * Created on 07/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public interface LastUpdateDateService extends ObserveService {

    void updateReferentialLastUpdateDates();

    void updateDataLastUpdateDates();
}
