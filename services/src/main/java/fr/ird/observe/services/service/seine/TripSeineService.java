package fr.ird.observe.services.service.seine;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.spi.DeleteRequest;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteDataPermission;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public interface TripSeineService  extends ObserveService {

    @ReadDataPermission
    DataReferenceSet<TripSeineDto> getAllTripSeine();

    @ReadDataPermission
    DataReferenceSet<TripSeineDto> getTripSeineByProgram(String programId);

    @ReadDataPermission
    int getTripSeinePositionInProgram(String programId, String tripSeineId);

    @ReadDataPermission
    TripMapDto getTripSeineMap(String tripSeineId);

    @ReadDataPermission
    Form<TripSeineDto> loadForm(String tripSeineId);

    @ReadDataPermission
    TripSeineDto loadDto(String tripSeineId);

    @ReadDataPermission
    DataReference<TripSeineDto> loadReferenceToRead(String tripSeineId);

    @ReadDataPermission
    boolean exists(String tripSeineId);

    @WriteDataPermission
    Form<TripSeineDto> preCreate(String programId);

    @Write
    @WriteDataPermission
    @PostRequest
    SaveResultDto save(TripSeineDto dto);

    @Write
    @WriteDataPermission
    @DeleteRequest
    void delete(String tripSeineId);

    @Write
    @WriteDataPermission
    @PostRequest
    int moveTripSeineToProgram(String tripSeineId, String programId);

    @Write
    @WriteDataPermission
    @PostRequest
    List<Integer> moveTripSeinesToProgram(List<String> tripSeineIds, String programId);

    @ReadDataPermission
    ImmutableList<ReferentialReference<SpeciesDto>> getSpeciesByListAndTrip(String tripSeineId, String speciesListId);
}
