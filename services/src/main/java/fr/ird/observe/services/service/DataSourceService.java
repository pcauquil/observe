package fr.ird.observe.services.service;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.spi.NoDataAccess;

import java.io.Closeable;
import java.io.File;
import java.util.Set;

/**
 * Created on 21/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface DataSourceService extends ObserveService, Closeable {

    @NoDataAccess
    ObserveDataSourceInformation checkCanConnect(ObserveDataSourceConfiguration dataSourceConfiguration) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException;

    @NoDataAccess
    ObserveDataSourceConnection create(ObserveDataSourceConfiguration dataSourceConfiguration, DataSourceCreateConfigurationDto dataSourceCreateConfiguration)
            throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException,
            BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException;

    @NoDataAccess
    ObserveDataSourceConnection open(ObserveDataSourceConfiguration dataSourceConfiguration) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException;

    @NoDataAccess
    Set<ObserveDbUserDto> getUsers(ObserveDataSourceConfiguration dataSourceConfiguration);

    @NoDataAccess
    void applySecurity(ObserveDataSourceConfiguration dataSourceConfiguration, Set<ObserveDbUserDto> users);

    @NoDataAccess
    void migrateData(ObserveDataSourceConfiguration dataSourceConfiguration);

    Set<Class<? extends ReferentialDto>> getReferentialTypesInShell();

    @Override
    void close();

    void destroy() throws DatabaseDestroyNotAuthorizedException;

    void backup(File backupFile);
}
