package fr.ird.observe.services.service.actions.validate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.ObserveDto;

/**
 * Le résultat d'une validation d'un type de dto donné.
 *
 * Created on 02/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ValidateResultForDtoType<D extends IdDto> implements ObserveDto {

    /**
     * Les messages regroupés pour chaque dto.
     */
    protected final ImmutableSet<ValidateResultForDto<D>> validateResultForDto;

    public ValidateResultForDtoType(ImmutableSet<ValidateResultForDto<D>> validateResultForDto) {
        this.validateResultForDto = validateResultForDto;
    }

    public ImmutableSet<ValidateResultForDto<D>> getValidateResultForDto() {
        return validateResultForDto;
    }

    public ValidateResultForDto<D> getValidateResult(final AbstractReference<D> referenceDto) {
        return validateResultForDto.stream()
                                   .filter(input -> referenceDto.equals(input.getDto()))
                                   .findFirst()
                                   .orElse(null);
    }

}
