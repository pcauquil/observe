package fr.ird.observe.services.service;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.ReferenceMap;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.spi.DeleteRequest;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.ReadReferentialPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteReferentialPermission;

import java.util.Collection;
import java.util.Date;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface ReferentialService extends ObserveService {

    @ReadReferentialPermission
    <R extends ReferentialDto> ReferentialReferenceSet<R> getReferenceSet(Class<R> type, Date lastUpdateDate);

    @ReadReferentialPermission
     ImmutableSet<ReferentialReferenceSet<?>> getReferentialReferenceSets(ReferenceSetsRequest request);

    //FIXME charger cela dans le cache côté applicatif
    @ReadReferentialPermission
    SpeciesDto loadSpecies(String id);

    @ReadReferentialPermission
    <R extends ReferentialDto> Form<R> loadForm(Class<R> type, String id) throws DataNotFoundException;

    @ReadReferentialPermission
    <R extends ReferentialDto> ReferentialReference<R> loadReference(Class<R> type, String id) throws DataNotFoundException;

    @WriteReferentialPermission
    <R extends ReferentialDto> Form<R> preCreate(Class<R> type);

    @WriteReferentialPermission
    @Write
    @PostRequest
    <R extends ReferentialDto> SaveResultDto save(R bean);

    @WriteReferentialPermission
    @Write
    @DeleteRequest
    <R extends ReferentialDto> void delete(Class<R> type, String id) throws DataNotFoundException;

    @WriteReferentialPermission
    @Write
    @DeleteRequest
    <R extends ReferentialDto> void delete(Class<R> type, Collection<String> ids) throws DataNotFoundException;

    @ReadReferentialPermission
    @ReadDataPermission
    <R extends ReferentialDto> ReferenceMap findAllUsages(R bean) throws DataNotFoundException;

    @ReadReferentialPermission
    @ReadDataPermission
    <R extends ReferentialDto> boolean exists(Class<R> type, String id);
}
