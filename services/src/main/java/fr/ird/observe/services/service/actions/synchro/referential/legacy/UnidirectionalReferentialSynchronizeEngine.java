package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffState;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffs;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffsEngine;
import org.apache.commons.collections4.CollectionUtils;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 28/06/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeEngine {

    private final ReferentialSynchronizeDiffsEngine diffsEngine;

    public UnidirectionalReferentialSynchronizeEngine(ReferentialSynchronizeDiffsEngine diffsEngine) {
        this.diffsEngine = diffsEngine;
    }

    public UnidirectionalReferentialSynchronizeContext prepareContext(UnidirectionalReferentialSynchronizeLocalService localService) {

        ReferentialSynchronizeDiffs synchronizeDiffs = diffsEngine.build();

        ImmutableSet.Builder<UnidirectionalReferentialSynchronizeRequest.Builder<?>> referentialSynchronizeRequestBuilders = ImmutableSet.builder();

        UnidirectionalReferentialSynchronizeCallbackRequests callbackRequests = new UnidirectionalReferentialSynchronizeCallbackRequests();

        // Première étape pour construire les builder de requète et calculer au passage les ids à faire remplacer
        // par l'utilisateur

        for (Class<? extends ReferentialDto> referentialName : synchronizeDiffs.getReferentialNames()) {

            UnidirectionalReferentialSynchronizeRequest.Builder<?> builder = computeReferentialSynchronizeRequestBuilder(
                    localService,
                    referentialName, synchronizeDiffs, callbackRequests);

            referentialSynchronizeRequestBuilders.add(builder);

        }

        return new UnidirectionalReferentialSynchronizeContext(
                referentialSynchronizeRequestBuilders.build(),
                callbackRequests
        );

    }

    public UnidirectionalReferentialSynchronizeResult prepareResult(UnidirectionalReferentialSynchronizeLocalService localService,UnidirectionalReferentialSynchronizeContext context, UnidirectionalReferentialSynchronizeCallbackResults callbackResults) {

        UnidirectionalReferentialSynchronizeResult result = new UnidirectionalReferentialSynchronizeResult();

        Set<String> insertSqlRequests = new LinkedHashSet<>();
        Set<String> updateSqlRequests = new LinkedHashSet<>();
        Set<String> deleteSqlRequests = new LinkedHashSet<>();

        Set<String> sqlRequests = new LinkedHashSet<>();

        // Second étape pour terminer la construction des builders de requètes et générer le code sql

        boolean needCallback = callbackResults != null;

        for (UnidirectionalReferentialSynchronizeRequest.Builder<?> referentialSynchronizeRequestBuilder : context.getReferentialSynchronizeRequestBuilders()) {

            Class<? extends ReferentialDto> referentialName = referentialSynchronizeRequestBuilder.getReferentialName();

            if (needCallback && callbackResults.containsReferentialName(referentialName)) {

                // l'utilisateur a agit sur ce référentiel
                UnidirectionalReferentialSynchronizeCallbackResult callbackResult = callbackResults.getCallbackResult(referentialName);

                Map<String, String> ids = callbackResult.getIds();

                for (Map.Entry<String, String> entry : ids.entrySet()) {
                    referentialSynchronizeRequestBuilder.entityToReplace(entry.getKey(), entry.getValue());
                    referentialSynchronizeRequestBuilder.entityToRemove(entry.getKey());
                }

            }

            UnidirectionalReferentialSynchronizeRequest<?> referentialSynchronizeRequest = referentialSynchronizeRequestBuilder.build();

            result.flushRequest(referentialSynchronizeRequest);

            Set<String> sqlRequestsforReferential = localService.generateSqlRequests(referentialSynchronizeRequest);
            for (String sqlStatement : sqlRequestsforReferential) {

                if (sqlStatement.startsWith("INSERT")) {
                    insertSqlRequests.add(sqlStatement);
                }if (sqlStatement.startsWith("UPDATE")) {
                    updateSqlRequests.add(sqlStatement);
                } else {
                    deleteSqlRequests.add(sqlStatement);
                }
            }

        }
        sqlRequests.addAll(insertSqlRequests);
        sqlRequests.addAll(updateSqlRequests);
        sqlRequests.addAll(deleteSqlRequests);

        context.setSqlRequests(sqlRequests);

        return result;

    }

    public void finish(UnidirectionalReferentialSynchronizeLocalService localService, UnidirectionalReferentialSynchronizeContext context) {

        Set<String> sqlRequests = context.getSqlRequests();
        localService.applySqlRequests(sqlRequests);

        localService.updateLastUpdateDates();
    }

    private <R extends ReferentialDto> UnidirectionalReferentialSynchronizeRequest.Builder<R> computeReferentialSynchronizeRequestBuilder(UnidirectionalReferentialSynchronizeLocalService localService,
                                                                                                                                          Class<R> referentialName,
                                                                                                                                          ReferentialSynchronizeDiffs diffs,
                                                                                                                                          UnidirectionalReferentialSynchronizeCallbackRequests callbackRequests) {

        UnidirectionalReferentialSynchronizeRequest.Builder<R> builder = UnidirectionalReferentialSynchronizeRequest.builder(referentialName);

        // Tous les référentiels ajoutés sur la base centrale doivent être copié en local
        Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalAddDiffStates = diffs.getRightAddedReferentials(referentialName);
        if (optionalAddDiffStates.isPresent()) {

            ImmutableSet<ReferentialSynchronizeDiffState> referentialSynchronizeDiffStates = optionalAddDiffStates.get();
            ImmutableSet<R> referentials = diffsEngine.getRightReferentials(referentialName, referentialSynchronizeDiffStates);
            referentials.forEach(builder::entityToAdd);
        }

        // Tous les référentiels mises à jour dans la base centrale doivent être copié en local
        Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalUpdateDiffStates = diffs.getRightUpdatedReferentials(referentialName);
        if (optionalUpdateDiffStates.isPresent()) {

            ImmutableSet<ReferentialSynchronizeDiffState> referentialSynchronizeDiffStates = optionalUpdateDiffStates.get();
            ImmutableSet<R> referentials = diffsEngine.getRightReferentials(referentialName, referentialSynchronizeDiffStates);
            referentials.forEach(builder::entityToUpdate);
        }

        // Tous les référentiels non présents dans la base centrale  doivent être supprimés en local
        Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalRemoveDiffStates = diffs.getLeftAddedReferentials(referentialName);
        if (optionalRemoveDiffStates.isPresent()) {

            ImmutableSet<ReferentialSynchronizeDiffState> referentialSynchronizeDiffStates = optionalRemoveDiffStates.get();

            Set<String> idsToRemove = referentialSynchronizeDiffStates.stream().map(ReferentialSynchronizeDiffState::getId).collect(Collectors.toSet());
            Set<String> blockingIdsToRemove = null;

            if (CollectionUtils.isNotEmpty(idsToRemove)) {

                Set<String> blockingIdsToRemoveFromLocal = localService.filterIdsUsedInLocalSource(referentialName, idsToRemove);

                blockingIdsToRemove = idsToRemove
                        .stream()
                        .filter(blockingIdsToRemoveFromLocal::contains)
                        .collect(Collectors.toSet());

                idsToRemove.removeAll(blockingIdsToRemove);
                idsToRemove.forEach(builder::entityToRemove);

            }

            boolean needCallback = CollectionUtils.isNotEmpty(blockingIdsToRemove);

            if (needCallback) {

                ReferentialReferenceSet<R> availableReferenceSet = diffsEngine.getRightEnabledReferentialReferenceSet(referentialName);
                Set<ReferentialReference<R>> availableReferentials = availableReferenceSet.getReferences();

                Set<ReferentialReference<R>> blockingReferentialsToRemove = localService.getLocalSourceReferentialToDelete(referentialName, blockingIdsToRemove);

                callbackRequests.addCallbackRequest(referentialName, blockingReferentialsToRemove, availableReferentials);

            }

            referentialSynchronizeDiffStates.stream().map(ReferentialSynchronizeDiffState::getId).forEach(builder::entityToRemove);
        }

        return builder;

    }

}
