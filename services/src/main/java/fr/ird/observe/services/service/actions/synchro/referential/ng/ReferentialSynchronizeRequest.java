package fr.ird.observe.services.service.actions.synchro.referential.ng;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.actions.synchro.referential.ng.task.ReferentialSynchronizeTask;
import fr.ird.observe.services.service.actions.synchro.referential.ng.task.ReferentialSynchronizeTaskType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 08/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchronizeRequest implements ObserveDto {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialSynchronizeRequest.class);

    private final ImmutableMultimap<ReferentialSynchronizeTaskType, ReferentialSynchronizeTask<?>> tasks;
    private final ImmutableSet<Class<? extends ReferentialDto>> types;
    private final ArrayListMultimap<Class<? extends ReferentialDto>, String> idsOnlyExistingOnThisSide;

    private ReferentialSynchronizeRequest(ImmutableMultimap<ReferentialSynchronizeTaskType, ReferentialSynchronizeTask<?>> tasks,
                                          ImmutableSet<Class<? extends ReferentialDto>> types,
                                          ArrayListMultimap<Class<? extends ReferentialDto>, String> idsOnlyExistingOnThisSide) {
        this.tasks = tasks;
        this.types = types;
        this.idsOnlyExistingOnThisSide = idsOnlyExistingOnThisSide;
    }

    public static Builder builder(ImmutableSetMultimap<Class<? extends ReferentialDto>, String> idsOnlyExistingOnThisSide) {
        return new Builder(idsOnlyExistingOnThisSide);
    }

    public boolean isNotEmpty() {
        return !tasks.isEmpty();
    }

    public Multimap<Class<? extends ReferentialDto>, String> getIdsOnlyExistingOnThisSide() {
        return idsOnlyExistingOnThisSide;
    }

    public ImmutableMultimap<ReferentialSynchronizeTaskType, ReferentialSynchronizeTask<?>> getTasks() {
        return tasks;
    }

    public <R extends ReferentialDto> Set<ReferentialSynchronizeTask<R>> getAddTasks(Class<R> referentialName) {
        return getTasks0(ReferentialSynchronizeTaskType.ADD, referentialName);
    }

    public <R extends ReferentialDto> Set<ReferentialSynchronizeTask<R>> getUpdateTasks(Class<R> referentialName) {
        return getTasks0(ReferentialSynchronizeTaskType.UPDATE, referentialName);
    }

    public <R extends ReferentialDto> Set<ReferentialSynchronizeTask<R>> getRevertTasks(Class<R> referentialName) {
        return getTasks0(ReferentialSynchronizeTaskType.REVERT, referentialName);
    }

    public <R extends ReferentialDto> Set<ReferentialSynchronizeTask<R>> getDesactivateTasks(Class<R> referentialName) {
        return getTasks0(ReferentialSynchronizeTaskType.DESACTIVATE, referentialName);
    }

    public <R extends ReferentialDto> Set<ReferentialSynchronizeTask<R>> getDeleteTasks(Class<R> referentialName) {
        return getTasks0(ReferentialSynchronizeTaskType.DELETE, referentialName);
    }

    public ImmutableSet<Class<? extends ReferentialDto>> getTypes() {
        return types;
    }

    private <R extends ReferentialDto> Set<ReferentialSynchronizeTask<R>> getTasks0(ReferentialSynchronizeTaskType type, Class<R> referentialName) {
        Set<ReferentialSynchronizeTask<?>> collect = tasks.get(type).stream().filter(t -> referentialName.equals(t.getReferentialType())).collect(Collectors.toSet());
        return (Set) collect;
    }

    public static class Builder {

        private final ImmutableMultimap.Builder<ReferentialSynchronizeTaskType, ReferentialSynchronizeTask<?>> tasksBuilder = ImmutableMultimap.builder();
        private final ImmutableSet.Builder<Class<? extends ReferentialDto>> typesBuilder = ImmutableSet.builder();
        private final Multimap<Class<? extends ReferentialDto>, String> idsOnlyExistingOnThisSide;

        public Builder(ImmutableSetMultimap<Class<? extends ReferentialDto>, String> idsOnlyExistingOnThisSide) {
            this.idsOnlyExistingOnThisSide = ArrayListMultimap.create(idsOnlyExistingOnThisSide);
        }

        public ReferentialSynchronizeRequest build() {
            return new ReferentialSynchronizeRequest(tasksBuilder.build(), typesBuilder.build(), ArrayListMultimap.create(idsOnlyExistingOnThisSide));
        }

        public <R extends ReferentialDto> Builder addTask(ReferentialSynchronizeTaskType taskType, Class<R> type, String referentialId, String replaceReferentialId) {
            if (log.isInfoEnabled()) {
                log.info("Add " + taskType + " task: " + type.getName() + " / " + referentialId);
            }
            tasksBuilder.put(taskType, new ReferentialSynchronizeTask<>(type, referentialId, replaceReferentialId));
            typesBuilder.add(type);
            return this;
        }


        public <R extends ReferentialDto> void removeIdOnlyExistOnThisSide(Class<R> type, String id) {
            idsOnlyExistingOnThisSide.remove(type, id);
        }

    }

}
