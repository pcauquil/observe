package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;

import java.util.stream.Collectors;

/**
 * Created on 31/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DeleteSqlScriptProducerRequest {

    protected boolean deleteData;
    protected ImmutableSet<String> dataIds;

    public static DeleteSqlScriptProducerRequest builder() {
        return new DeleteSqlScriptProducerRequest();
    }

    public static DeleteSqlScriptProducerRequest forH2() {
        return new DeleteSqlScriptProducerRequest();
    }

    public static DeleteSqlScriptProducerRequest forPostgres() {
        return new DeleteSqlScriptProducerRequest();
    }

    public DeleteSqlScriptProducerRequest deleteAllData() {
        deleteData = true;
        return this;
    }

    public DeleteSqlScriptProducerRequest dataIdsToDelete(ImmutableSet<String> dataIds) {
        deleteData = true;
        this.dataIds = dataIds;
        return this;
    }

    protected DeleteSqlScriptProducerRequest() {
    }

    public boolean isDeleteData() {
        return deleteData;
    }

    public ImmutableSet<String> getDataIds() {
        return dataIds;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("deleteData", deleteData)
                .add("dataIds", dataIds == null ? "" : "[" + dataIds.stream().collect(Collectors.joining(", ")) + "]")
                .toString();
    }
}
