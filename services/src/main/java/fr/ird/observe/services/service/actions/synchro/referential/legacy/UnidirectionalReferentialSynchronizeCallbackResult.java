package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.Map;
import java.util.TreeMap;

/**
 * Contient les résultats du call back utilisateur pour un type de référentiel donné.
 *
 * Created on 12/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeCallbackResult<R extends ReferentialDto> {

    private final Class<R> referentialName;
    private final Map<String, String> ids = new TreeMap<>();

    public Class<R> getReferentialName() {
        return referentialName;
    }

    public Map<String, String> getIds() {
        return ids;
    }

    void addId(String idToReplace, String replaceId) {
        ids.put(idToReplace, replaceId);
    }

    UnidirectionalReferentialSynchronizeCallbackResult(Class<R> referentialName) {
        this.referentialName = referentialName;
    }



}
