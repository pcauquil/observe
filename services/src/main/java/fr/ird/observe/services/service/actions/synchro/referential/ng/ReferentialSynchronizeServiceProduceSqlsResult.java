package fr.ird.observe.services.service.actions.synchro.referential.ng;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.ObserveDto;

/**
 * Created on 15/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeServiceProduceSqlsResult implements ObserveDto {

    protected final ReferentialSynchronizeSqlsRequest leftRequest;
    protected final ReferentialSynchronizeSqlsRequest rightRequest;

    public ReferentialSynchronizeServiceProduceSqlsResult(ReferentialSynchronizeSqlsRequest leftRequest, ReferentialSynchronizeSqlsRequest rightRequest) {
        this.leftRequest = leftRequest;
        this.rightRequest = rightRequest;
    }

    public ReferentialSynchronizeSqlsRequest getLeftRequest() {
        return leftRequest;
    }

    public ReferentialSynchronizeSqlsRequest getRightRequest() {
        return rightRequest;
    }

}
