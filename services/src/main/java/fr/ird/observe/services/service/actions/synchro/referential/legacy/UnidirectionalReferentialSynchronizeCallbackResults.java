package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Contient l'ensemble des résultats du call back utilisateur.
 *
 * Created on 12/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeCallbackResults {

    private final Map<Class<? extends ReferentialDto>, UnidirectionalReferentialSynchronizeCallbackResult> callbackResults;

    public UnidirectionalReferentialSynchronizeCallbackResults() {
        callbackResults = new LinkedHashMap<>();
    }

    public <R extends ReferentialDto> void addCallbackResult(Class<R> referentialName, String idtoReplace, String replaceId) {
        UnidirectionalReferentialSynchronizeCallbackResult<R> callbackResult = getCallbackResult(referentialName);
        if (callbackResult == null) {
            callbackResult = new UnidirectionalReferentialSynchronizeCallbackResult<>(referentialName);
            callbackResults.put(referentialName, callbackResult);
        }
        callbackResult.addId(idtoReplace, replaceId);
    }

    public <R extends ReferentialDto> UnidirectionalReferentialSynchronizeCallbackResult<R> getCallbackResult(Class<R> referentialName) {
        return callbackResults.get(referentialName);
    }

    public <R extends ReferentialDto> boolean containsReferentialName(Class<R> referentialName) {
        return callbackResults.containsKey(referentialName);
    }
}
