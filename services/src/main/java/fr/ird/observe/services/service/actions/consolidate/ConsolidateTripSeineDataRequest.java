package fr.ird.observe.services.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.ObserveDto;

import java.io.Serializable;

/**
 * Pour configurer l'opération de consolidation de données sur les marées de type Seine.
 *
 * Created on 28/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ConsolidateTripSeineDataRequest implements Serializable, ObserveDto {

    private static final long serialVersionUID = 1L;

    /**
     * Pour générer une exception si une relation RTP n'est pas trouvée.
     */
    protected boolean failIfLenghtWeightParameterNotFound;

    /**
     * Les identifiants des marées à consolider.
     */
    protected ImmutableSet<String> tripSeineIds;

    public boolean isFailIfLenghtWeightParameterNotFound() {
        return failIfLenghtWeightParameterNotFound;
    }

    public void setFailIfLenghtWeightParameterNotFound(boolean failIfLenghtWeightParameterNotFound) {
        this.failIfLenghtWeightParameterNotFound = failIfLenghtWeightParameterNotFound;
    }

    public ImmutableSet<String> getTripSeineIds() {
        return tripSeineIds;
    }

    public void setTripSeineIds(ImmutableSet<String> tripSeineIds) {
        this.tripSeineIds = tripSeineIds;
    }

}
