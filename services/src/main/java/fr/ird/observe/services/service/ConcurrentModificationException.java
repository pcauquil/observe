package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ConcurrentModificationException extends RuntimeException {

    private static final long serialVersionUID = -4964630055605654415L;

    /**
     * Entity id.
     */
    protected final String id;
    /**
     * Last update date stored in database.
     */
    protected final Date lastUpdateDate;

    /**
     * Last update date coming from entity to store.
     */
    protected final Date currentUpdateDate;

    public ConcurrentModificationException(String id, Date lastUpdateDate, Date currentUpdateDate) {
        this.id = id;
        this.lastUpdateDate = lastUpdateDate;
        this.currentUpdateDate = currentUpdateDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public Date getCurrentUpdateDate() {
        return currentUpdateDate;
    }
}
