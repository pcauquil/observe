package fr.ird.observe.services.http;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveHttpError {

    public static final String PROPERTY_HTTP_CODE = "httpCode";

    public static final String PROPERTY_EXECPTION_TYPE = "exceptionType";

    public static final String PROPERTY_MESSAGE = "message";

    public static final String PROPERTY_EXCEPTION = "exception";

    protected final Integer httpCode;

    protected final Class<?> exceptionType;

    protected final String message;

    protected final Throwable exception;

    public ObserveHttpError(Integer httpCode, Class<?> exceptionType, String message, Throwable exception) {
        this.httpCode = httpCode;
        this.exceptionType = exceptionType;
        this.message = message;
        this.exception = exception;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public Class<?> getExceptionType() {
        return exceptionType;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getException() {
        return exception;
    }
}
