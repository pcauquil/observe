package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;

import java.util.Date;

/**
 * Pour demander plusieurs ensembles de références dans un même appel.
 *
 * Created on 10/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see ReferentialService#getReferentialReferenceSets(ReferenceSetsRequest)
 */
public class ReferenceSetsRequest {

    private String requestName;

    private ImmutableMap<Class<?>, Date> lastUpdateDates;

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public ImmutableMap<Class<?>, Date> getLastUpdateDates() {
        return lastUpdateDates;
    }

    public void setLastUpdateDates(ImmutableMap<Class<?>, Date> lastUpdateDates) {
        this.lastUpdateDates = lastUpdateDates;
    }

}
