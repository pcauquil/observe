package fr.ird.observe.services.service.actions.validate;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.gson.ObserveDtoGsonSupplier;

import java.io.InputStreamReader;
import java.io.Reader;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ValidateServiceUtils {

    private static final String RESOURCE_VALIDATORS = "/META-INF/validators/services-topia-validation.json";

    public static ImmutableSet<ValidatorDto> getValidators() {

        ObserveDtoGsonSupplier gsonSupplier = new ObserveDtoGsonSupplier(false);

        Gson gson = gsonSupplier.get();

        try (Reader reader = new InputStreamReader(ValidateServiceUtils.class.getResourceAsStream(RESOURCE_VALIDATORS))) {

            ValidatorDto[] validators = gson.fromJson(reader, ValidatorDto[].class);

            ImmutableSet.Builder<ValidatorDto> builder = ImmutableSet.builder();

            for (ValidatorDto validator : validators) {
                String entityClassName = validator.getType().getName();
                String dtoClassName = entityClassName.replace(".entities.", ".services.dto.").replace(".referentiel.", ".referential.") + "Dto";
                Class<? extends IdDto> dtoClass = (Class<? extends IdDto>) Class.forName(dtoClassName);
                ValidatorDto v2 = new ValidatorDto(dtoClass, validator.getScope(), validator.getContext(), validator.getFields());
                builder.add(v2);
            }

            return builder.build();

        } catch (Exception e) {
            throw new ValidatorInitializationException(e);
        }

    }

}
