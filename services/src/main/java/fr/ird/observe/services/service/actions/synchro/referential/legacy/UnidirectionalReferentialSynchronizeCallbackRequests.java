package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Représente l'ensemble des demandes au call back utilisateur.
 *
 * Created on 12/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeCallbackRequests implements Iterable<UnidirectionalReferentialSynchronizeCallbackRequest<?>> {

    private final Map<Class<? extends ReferentialDto>, UnidirectionalReferentialSynchronizeCallbackRequest<?>> callbackRequests;

    public UnidirectionalReferentialSynchronizeCallbackRequests() {
        this.callbackRequests = new HashMap<>();
    }

    public <R extends ReferentialDto> void addCallbackRequest(Class<R> referentialName,
                                                              Collection<ReferentialReference<R>> referentialsToReplace,
                                                              Collection<ReferentialReference<R>> availableReferentials) {

        UnidirectionalReferentialSynchronizeCallbackRequest<R> callbackRequest = new UnidirectionalReferentialSynchronizeCallbackRequest<>(referentialName, referentialsToReplace, availableReferentials);
        callbackRequests.put(referentialName, callbackRequest);
    }

    public <R extends ReferentialDto> UnidirectionalReferentialSynchronizeCallbackRequest<R> getCallbackRequest(Class<R> referentialName) {
        return (UnidirectionalReferentialSynchronizeCallbackRequest) callbackRequests.get(referentialName);
    }

    public boolean isNotEmpty() {
        return !callbackRequests.isEmpty();
    }

    @Override
    public Iterator<UnidirectionalReferentialSynchronizeCallbackRequest<?>> iterator() {
        return callbackRequests.values().iterator();
    }
}
