package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadReferentialPermission;
import fr.ird.observe.services.spi.Write;

import java.util.Set;

/**
 * Service pour effectuer une synchronisation de référentiel unidirectionnelle du côte de la source centrale.
 *
 * Created on 27/06/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public interface UnidirectionalReferentialSynchronizeLocalService extends ObserveService {

    /**
     * Pour un référentiel d'un type donné (son nom est donné), détecte les référentiels dont
     * les identifiants sont passés en paramètres qui sont réellement utilisés dans la source locale.
     *
     * @param referentialName le nom du référentiel
     * @param ids             les identifiants du référentiel dont on recherche le nombre d'utilisation
     * @return les identifiants des référentiels passés en paramètres qui sont réellement utilisés dans la source locale.
     */
    @ReadReferentialPermission
    <R extends ReferentialDto> Set<String> filterIdsUsedInLocalSource(Class<R> referentialName, Set<String> ids);

    /**
     * Pour récupérer les références sur les référentiels d'un certain type à supprimer,
     * cet ensemble servira à les afficher dans l'interface graphique pour effectuer le remplacement.
     *
     * @param <R>             type des référentiels à récupérer
     * @param referentialName le nom du référentiel à récupérer
     * @param ids             les identifiants des référentiels à supprimer
     * @return l'ensemble des références de référentiel à supprimer.
     */
    @ReadReferentialPermission
    <R extends ReferentialDto> Set<ReferentialReference<R>> getLocalSourceReferentialToDelete(Class<R> referentialName, Set<String> ids);

    /**
     * Pour produire le code sql à partir de la demande pour un référentiel donné.
     *
     * @param request la demande des actions à produire pour un référentiel donné
     * @param <R>     type des référentiels à traiter
     * @return l'ensemble des requètes sql à appliquer.
     */
    @ReadReferentialPermission
    <R extends ReferentialDto> Set<String> generateSqlRequests(UnidirectionalReferentialSynchronizeRequest<R> request);

    /**
     * Pour appliquer les requètes sql de mise à jour du réferentiel.
     *
     * @param sqlRequests les requètes sql à appliquer
     */
    @ReadReferentialPermission
    //tc-20160713 On ne met pas cette permission, car il s'agit d'appliquer un script sql, sans avoir besoin des droits applicatifs
//    @WriteReferentialPermission
    @Write
    @PostRequest
    void applySqlRequests(Set<String> sqlRequests);

    @ReadReferentialPermission
    @Write
    @PostRequest
    void updateLastUpdateDates();
}
