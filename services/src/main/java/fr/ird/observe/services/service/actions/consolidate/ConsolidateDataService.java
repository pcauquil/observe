package fr.ird.observe.services.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteDataPermission;

/**
 * Le service pour effectuer les consolidations de données, i.e
 * de pouvoir effectuer remplir certains champs à partir de champs observés.
 *
 * Created on 28/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface ConsolidateDataService extends ObserveService{

    /**
     * Effectue la consolitation à partir de la demande passé en paramètre et retourne les résultats pour les marées
     * modifiées.
     *
     * @param consolidateTripSeineDataRequest la demande de consolidation
     * @return le résultat de la consolidation de données pour chaque marée qui a été modifée.
     */
    @ReadDataPermission
    @WriteDataPermission
    @PostRequest
    @Write
    ImmutableSet<ConsolidateTripSeineDataResult> consolidateTripSeines(ConsolidateTripSeineDataRequest consolidateTripSeineDataRequest);

}
