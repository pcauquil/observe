package fr.ird.observe.services.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.Optional;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeDiff {

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, ReferentialSynchronizeDiffState> addedReferentialsBuilder;

        private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, ReferentialSynchronizeDiffState> updatedReferentialsBuilder;
        private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, String> existingIdsBuilder;

        public Builder() {

            addedReferentialsBuilder = ImmutableSetMultimap.builder();
            updatedReferentialsBuilder = ImmutableSetMultimap.builder();
            existingIdsBuilder = ImmutableSetMultimap.builder();
        }

        <R extends ReferentialDto> Builder addAddedReferential(Class<R> referentialName, ReferentialSynchronizeDiffState referentialDto) {
            addedReferentialsBuilder.put(referentialName, referentialDto);
            return this;
        }

        <R extends ReferentialDto> Builder addUpdatedReferential(Class<R> referentialName, ReferentialSynchronizeDiffState referentialDto) {
            updatedReferentialsBuilder.put(referentialName, referentialDto);
            return this;
        }

        public <R extends ReferentialDto> Builder addExistingId(Class<R> referentialName, String id) {
            existingIdsBuilder.put(referentialName, id);
            return this;
        }

        public ReferentialSynchronizeDiff build() {
            return new ReferentialSynchronizeDiff(addedReferentialsBuilder.build(), updatedReferentialsBuilder.build(), existingIdsBuilder.build());
        }
    }

    public static <K> Optional<ImmutableSet<K>> optionalOfEmptySet(ImmutableSet<K> set) {
        return Optional.ofNullable(set.isEmpty() ? null : set);
    }

    /**
     * Tous les types de référentiels décrits pour cette source.
     */
    private final ImmutableSet<Class<? extends ReferentialDto>> referentialNames;

    /**
     * Les référentiels ajoutés dans cette source (indexé par type de référentiel).
     */
    private final ImmutableSetMultimap<Class<? extends ReferentialDto>, ReferentialSynchronizeDiffState> addedReferentials;
    /**
     * Les référentiels mises à jour dans cette source (indexé par type de référentiel).
     */
    private final ImmutableSetMultimap<Class<? extends ReferentialDto>, ReferentialSynchronizeDiffState> updatedReferentials;

    private final ImmutableSetMultimap<Class<? extends ReferentialDto>, String> existingIds;

    public ImmutableSet<Class<? extends ReferentialDto>> getReferentialNames() {
        return referentialNames;
    }

    public <R extends ReferentialDto> Optional<ImmutableSet<ReferentialSynchronizeDiffState>> getAddedReferentials(Class<R> referentialName) {
        return optionalOfEmptySet(addedReferentials.get(referentialName));
    }

    public <R extends ReferentialDto> Optional<ImmutableSet<ReferentialSynchronizeDiffState>> getUpdatedReferentials(Class<R> referentialName) {
        return optionalOfEmptySet(updatedReferentials.get(referentialName));
    }

    public ImmutableSetMultimap<Class<? extends ReferentialDto>, String> getExistingIds() {
        return existingIds;
    }

    private ReferentialSynchronizeDiff(ImmutableSetMultimap<Class<? extends ReferentialDto>, ReferentialSynchronizeDiffState> addedReferentials,
                                       ImmutableSetMultimap<Class<? extends ReferentialDto>, ReferentialSynchronizeDiffState> updatedReferentials,
                                       ImmutableSetMultimap<Class<? extends ReferentialDto>, String> existingIds) {
        this.addedReferentials = addedReferentials;
        this.updatedReferentials = updatedReferentials;
        this.existingIds = existingIds;
        this.referentialNames = ImmutableSet.<Class<? extends ReferentialDto>>builder()
                .addAll(addedReferentials.keySet())
                .addAll(updatedReferentials.keySet())
                .build();

    }

}
