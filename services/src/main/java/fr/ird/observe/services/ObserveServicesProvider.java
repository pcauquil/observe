package fr.ird.observe.services;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.LastUpdateDateService;
import fr.ird.observe.services.service.PingService;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.service.SqlScriptProducerService;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.report.ReportService;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeLocalService;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeService;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import fr.ird.observe.services.service.longline.ActivityLongLineEncounterService;
import fr.ird.observe.services.service.longline.ActivityLongLineSensorUsedService;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import fr.ird.observe.services.service.longline.BranchlineService;
import fr.ird.observe.services.service.longline.SetLonglineCatchService;
import fr.ird.observe.services.service.longline.SetLonglineDetailCompositionService;
import fr.ird.observe.services.service.longline.SetLonglineGlobalCompositionService;
import fr.ird.observe.services.service.longline.SetLonglineService;
import fr.ird.observe.services.service.longline.TdrService;
import fr.ird.observe.services.service.longline.TripLonglineGearUseService;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.services.service.seine.ActivitySeineObservedSystemService;
import fr.ird.observe.services.service.seine.ActivitySeineService;
import fr.ird.observe.services.service.seine.FloatingObjectService;
import fr.ird.observe.services.service.seine.NonTargetCatchService;
import fr.ird.observe.services.service.seine.NonTargetSampleService;
import fr.ird.observe.services.service.seine.ObjectObservedSpeciesService;
import fr.ird.observe.services.service.seine.ObjectSchoolEstimateService;
import fr.ird.observe.services.service.seine.RouteService;
import fr.ird.observe.services.service.seine.SchoolEstimateService;
import fr.ird.observe.services.service.seine.SetSeineService;
import fr.ird.observe.services.service.seine.TargetCatchService;
import fr.ird.observe.services.service.seine.TargetSampleService;
import fr.ird.observe.services.service.seine.TransmittingBuoyOperationService;
import fr.ird.observe.services.service.seine.TripSeineGearUseService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.services.service.trip.TripManagementService;

/**
 * Un provider de services typés.
 *
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public interface ObserveServicesProvider {

    LastUpdateDateService newLastUpdateDateService();

    SqlScriptProducerService newSqlScriptProducerService();

    ValidateService newValidateService();

    ReportService newReportService();

    PingService newPingService();

    TripManagementService newTripManagementService();

    ConsolidateDataService newConsolidateDataService();

    UnidirectionalReferentialSynchronizeLocalService newUnidirectionalReferentialSynchronizeLocalService();

    ReferentialSynchronizeService newReferentialSynchronizeService();

    ReferentialSynchronizeDiffService newReferentialSynchronizeDiffService();

    DataSourceService newDataSourceService();

    ReferentialService newReferentialService();

    TripSeineService newTripSeineService();

    RouteService newRouteService();

    FloatingObjectService newFloatingObjectService();

    ActivitySeineService newActivitySeineService();

    SetSeineService newSetSeineService();

    TripLonglineService newTripLonglineService();

    ActivityLonglineService newActivityLonglineService();

    SetLonglineService newSetLonglineService();

    ActivitySeineObservedSystemService newActivitySeineObservedSystemService();

    SetLonglineGlobalCompositionService newSetLonglineGlobalCompositionService();

    SetLonglineDetailCompositionService newSetLonglineDetailCompositionService();

    TransmittingBuoyOperationService newTransmittingBuoyOperationService();

    BranchlineService newBranchlineService();

    SetLonglineCatchService newSetLonglineCatchService();

    ActivityLongLineEncounterService newActivityLongLineEncounterService();

    TripLonglineGearUseService newTripLonglineGearUseService();

    ActivityLongLineSensorUsedService newActivityLongLineSensorUsedService();

    TdrService newTdrService();

    TripSeineGearUseService newTripSeineGearUseService();

    NonTargetCatchService newNonTargetCatchService();

    NonTargetSampleService newNonTargetSampleService();

    ObjectObservedSpeciesService newObjectObservedSpeciesService();

    ObjectSchoolEstimateService newObjectSchoolEstimateService();

    SchoolEstimateService newSchoolEstimateService();

    TargetCatchService newTargetCatchService();

    TargetSampleService newTargetSampleService();
}
