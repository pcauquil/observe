package fr.ird.observe.services.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialDataSourceState<R extends ReferentialDto> {

    private final Class<R> type;

    private final Set<ReferentialSynchronizeDiffState> diffStates;

    public ReferentialDataSourceState(Class<R> type) {
        this.type = type;
        this.diffStates = new LinkedHashSet<>();
    }

    public Class<R> getType() {
        return type;
    }

    public Set<ReferentialSynchronizeDiffState> getDiffStates() {
        return diffStates;
    }

    public ImmutableMap<String, ReferentialSynchronizeDiffState> getReferentialStatesById() {
        return Maps.uniqueIndex(diffStates, ReferentialSynchronizeDiffState::getId);
    }

    public ReferentialSynchronizeDiffState getLatestReferentialDiffState() {
        ReferentialSynchronizeDiffState result = null;
        for (ReferentialSynchronizeDiffState diffState : diffStates) {
            if (result == null) {
                result = diffState;
                continue;
            }
            if (result.getLastUpdateDate().before(diffState.getLastUpdateDate()) || result.getVersion() < diffState.getVersion()) {
                result = diffState;
            }
        }
        return result;
    }

    /**
     * Pour ajouter un référentiel.
     *
     * @param id             l'identifiant du référentiel à ajouter
     * @param lastUpdateDate la date de dernière mise à jour du référentiel à ajouter
     * @param version        la version du référentiel à ajouter
     * @param disabled       {@code true} si le référentiel est désactivé
     */
    public void addReferentialVersion(String id, Date lastUpdateDate, long version, boolean disabled) {
        diffStates.add(new ReferentialSynchronizeDiffState(id, lastUpdateDate, version, disabled));
    }
}
