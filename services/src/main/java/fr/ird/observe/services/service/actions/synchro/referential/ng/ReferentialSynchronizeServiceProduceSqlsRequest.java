package fr.ird.observe.services.service.actions.synchro.referential.ng;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSetMultimap;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.actions.synchro.referential.ng.task.ReferentialSynchronizeTaskType;

import java.util.Set;

/**
 * Created on 15/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeServiceProduceSqlsRequest implements ObserveDto {

    private final ReferentialSynchronizeRequest leftRequest;
    private final ReferentialSynchronizeRequest rightRequest;
    private final Set<Class<? extends ReferentialDto>> referentialTypesInShell;

    private ReferentialSynchronizeServiceProduceSqlsRequest(ReferentialSynchronizeRequest leftRequest,
                                                            ReferentialSynchronizeRequest rightRequest,
                                                            Set<Class<? extends ReferentialDto>> referentialTypesInShell) {
        this.leftRequest = leftRequest;
        this.rightRequest = rightRequest;
        this.referentialTypesInShell = referentialTypesInShell;
    }

    public static Builder builder(ImmutableSetMultimap<Class<? extends ReferentialDto>, String> leftExistingIds,
                                  ImmutableSetMultimap<Class<? extends ReferentialDto>, String> rightExistingIds,
                                  Set<Class<? extends ReferentialDto>> referentialTypesInShell) {
        return new Builder(leftExistingIds, rightExistingIds, referentialTypesInShell);
    }

    public ReferentialSynchronizeRequest getLeftRequest() {
        return leftRequest;
    }

    public ReferentialSynchronizeRequest getRightRequest() {
        return rightRequest;
    }

    public Set<Class<? extends ReferentialDto>> getReferentialTypesInShell() {
        return referentialTypesInShell;
    }

    public static class Builder {

        private final ReferentialSynchronizeRequest.Builder leftRequestBuilder;
        private final ReferentialSynchronizeRequest.Builder rightRequestBuilder;
        private final Set<Class<? extends ReferentialDto>> referentialTypesInShell;

        private Builder(ImmutableSetMultimap<Class<? extends ReferentialDto>, String> leftIdsOnlyExistingOnThisSide,
                        ImmutableSetMultimap<Class<? extends ReferentialDto>, String> rightIdsOnlyExistingOnThisSide,
                        Set<Class<? extends ReferentialDto>> referentialTypesInShell) {
            leftRequestBuilder = ReferentialSynchronizeRequest.builder(leftIdsOnlyExistingOnThisSide);
            rightRequestBuilder = ReferentialSynchronizeRequest.builder(rightIdsOnlyExistingOnThisSide);
            this.referentialTypesInShell = referentialTypesInShell;
        }

        public ReferentialSynchronizeServiceProduceSqlsRequest build() {

            return new ReferentialSynchronizeServiceProduceSqlsRequest(leftRequestBuilder.build(),
                                                                       rightRequestBuilder.build(),
                                                                       referentialTypesInShell);

        }

        public <R extends ReferentialDto> Builder addTask(boolean left,
                                                          ReferentialSynchronizeTaskType taskType,
                                                          Class<R> type,
                                                          String id,
                                                          String replaceId) {

            ReferentialSynchronizeRequest.Builder requestBuilder = getRequestBuilder(left);
            requestBuilder.addTask(taskType, type, id, replaceId);

            if (ReferentialSynchronizeTaskType.ADD == taskType) {

                // le referentiel est à ajouter, il sera donc disponible dans les deux sources
                requestBuilder.removeIdOnlyExistOnThisSide(type, id);

            } else if (ReferentialSynchronizeTaskType.DELETE == taskType) {

                // le référentiel est supprimer, il ne sera donc plus disponible du tout
                getRequestBuilder(!left).removeIdOnlyExistOnThisSide(type, id);

            }
            return this;
        }

        private ReferentialSynchronizeRequest.Builder getRequestBuilder(boolean left) {
            return left ? leftRequestBuilder : rightRequestBuilder;
        }

    }
}
