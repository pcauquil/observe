package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Contient toutes les opérations réalisées lors de la synchronisation des référentiels.
 *
 * Created on 05/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeResult implements ObserveDto {

    private final Set<Class<? extends ReferentialDto>> referentialNames;
    private final Multimap<Class<? extends ReferentialDto>, String> referentialAdded;
    private final Multimap<Class<? extends ReferentialDto>, String> referentialUpdated;
    private final Multimap<Class<? extends ReferentialDto>, Pair<String, String>> referentialReplaced;
    private final Multimap<Class<? extends ReferentialDto>, String> referentialRemoved;

    public UnidirectionalReferentialSynchronizeResult() {
        this.referentialNames = new LinkedHashSet<>();
        this.referentialAdded = ArrayListMultimap.create();
        this.referentialUpdated = ArrayListMultimap.create();
        this.referentialReplaced = ArrayListMultimap.create();
        this.referentialRemoved = ArrayListMultimap.create();
    }

    public boolean isEmpty() {
        return referentialNames.isEmpty();
    }

    public Set<Class<? extends ReferentialDto>> getReferentialNames() {
        return referentialNames;
    }

    public Collection<String> getReferentialAdded(Class<? extends ReferentialDto> referentialName) {
        return referentialAdded.get(referentialName);
    }

    public Collection<String> getReferentialUpdated(Class<? extends ReferentialDto> referentialName) {
        return referentialUpdated.get(referentialName);
    }

    public Collection<Pair<String, String>> getReferentialReplaced(Class<? extends ReferentialDto> referentialName) {
        return referentialReplaced.get(referentialName);
    }

    public Collection<String> getReferentialRemoved(Class<? extends ReferentialDto> referentialName) {
        return referentialRemoved.get(referentialName);
    }

    void flushRequest(UnidirectionalReferentialSynchronizeRequest<?> referentialSynchronizeRequest) {

        Class<? extends ReferentialDto> referentialName = referentialSynchronizeRequest.getReferentialName();

        if (referentialSynchronizeRequest.withReferentialToAdd()) {

            for (ReferentialDto referentialDto : referentialSynchronizeRequest.getReferentialToAdd()) {
                addReferentialAdded(referentialName, referentialDto.getId());
            }

        }

        if (referentialSynchronizeRequest.withReferentialToUpdate()) {

            for (ReferentialDto referentialDto : referentialSynchronizeRequest.getReferentialToUpdate()) {
                addReferentialUpdated(referentialName, referentialDto.getId());
            }

        }

        if (referentialSynchronizeRequest.withReferentialToRemove()) {

            for (String id : referentialSynchronizeRequest.getReferentialToRemove()) {
                addReferentialRemoved(referentialName, id);
            }

        }

        if (referentialSynchronizeRequest.withReferentialToReplace()) {

            for (Map.Entry<String, String> entry : referentialSynchronizeRequest.getReferentialToReplace().entrySet()) {
                addReferentialReplaced(referentialName, entry.getKey(), entry.getValue());
            }

        }

    }

    private void addReferentialAdded(Class<? extends ReferentialDto> referentialName, String id) {
        referentialNames.add(referentialName);
        referentialAdded.put(referentialName, id);
    }

    private void addReferentialUpdated(Class<? extends ReferentialDto> referentialName, String id) {
        referentialNames.add(referentialName);
        referentialUpdated.put(referentialName, id);
    }

    private void addReferentialRemoved(Class<? extends ReferentialDto> referentialName, String id) {
        referentialNames.add(referentialName);
        referentialRemoved.put(referentialName, id);
    }

    private void addReferentialReplaced(Class<? extends ReferentialDto> referentialName, String idToReplace, String replaceId) {
        referentialNames.add(referentialName);
        referentialReplaced.put(referentialName, Pair.of(idToReplace, replaceId));
    }
}
