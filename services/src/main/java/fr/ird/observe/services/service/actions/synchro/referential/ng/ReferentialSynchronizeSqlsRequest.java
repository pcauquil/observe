package fr.ird.observe.services.service.actions.synchro.referential.ng;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.ObserveDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 08/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchronizeSqlsRequest implements ObserveDto {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialSynchronizeSqlsRequest.class);

    private final byte[] insertSqlCode;
    private final byte[] updateSqlCode;
    private final byte[] deleteSqlCode;
    private final byte[] desactivateSqlCode;

    private ReferentialSynchronizeSqlsRequest(String insertSqlCode,
                                              String updateSqlCode,
                                              String deleteSqlCode,
                                              String desactivateSqlCode) {
        this.insertSqlCode = insertSqlCode.getBytes();
        this.updateSqlCode = updateSqlCode.getBytes();
        this.deleteSqlCode = deleteSqlCode.getBytes();
        this.desactivateSqlCode = desactivateSqlCode.getBytes();
    }

    public static Builder builder() {
        return new Builder();
    }

    public byte[] getInsertSqlCode() {
        return insertSqlCode;
    }

    public byte[] getUpdateSqlCode() {
        return updateSqlCode;
    }

    public byte[] getDeleteSqlCode() {
        return deleteSqlCode;
    }

    public byte[] getDesactivateSqlCode() {
        return desactivateSqlCode;
    }

    public static class Builder {

        private final StringBuilder addTasksBuilder = new StringBuilder();
        private final StringBuilder updateTasksBuilder = new StringBuilder();
        private final StringBuilder deleteTasksBuilder = new StringBuilder();
        private final StringBuilder desactivateTasksBuilder = new StringBuilder();

        public ReferentialSynchronizeSqlsRequest build() {
            return new ReferentialSynchronizeSqlsRequest(addTasksBuilder.toString(),
                                                         updateTasksBuilder.toString(),
                                                         deleteTasksBuilder.toString(),
                                                         desactivateTasksBuilder.toString());
        }

        public Builder addInsertStatement(String sql) {
            if (log.isInfoEnabled()) {
                log.info("Add add sql: " + sql);
            }
            addTasksBuilder.append(sql);
            return this;
        }

        public Builder addUpdateStatement(String sql) {
            if (log.isInfoEnabled()) {
                log.info("Add update sql: " + sql);
            }
            updateTasksBuilder.append(sql);
            return this;
        }

        public Builder addDeleteStatement(String sql) {

            if (log.isInfoEnabled()) {
                log.info("Add delete sql: " + sql);
            }
            deleteTasksBuilder.append(sql);
            return this;
        }

        public Builder addDesactivateStatement(String sql) {

            if (log.isInfoEnabled()) {
                log.info("Add desactivate sql: " + sql);
            }
            desactivateTasksBuilder.append(sql);
            return this;
        }

    }
}
