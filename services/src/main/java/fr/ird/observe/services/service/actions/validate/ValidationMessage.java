package fr.ird.observe.services.service.actions.validate;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ird.observe.services.dto.ObserveDto;
import org.nuiton.validator.NuitonValidatorScope;

import java.io.Serializable;

/**
 * Représente un message de validation.
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ValidationMessage implements ObserveDto, Serializable {

    private static final long serialVersionUID = 1L;

    protected final NuitonValidatorScope scope;

    protected final String fieldName;

    protected final String message;

    public ValidationMessage(NuitonValidatorScope scope, String fieldName, String message) {
        this.scope = scope;
        this.fieldName = fieldName;
        this.message = message;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("scope", scope)
                .add("fieldName", fieldName)
                .add("message", message)
                .toString();
    }
}
