package fr.ird.observe.services;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfigurationAndConnection;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.ObserveSpeciesListConfiguration;
import fr.ird.observe.services.dto.constants.ReferentialLocale;

import java.io.File;
import java.util.Locale;

/**
 * Objet contentant les informations nécessaire pour créer un nouveau service.
 *
 * Ces informations viennent en général du context applicatif appelant.
 *
 * Created on 31/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveServiceInitializer {

    public static ObserveServiceInitializer create(Locale applicationLocale,
                                                   ReferentialLocale referentialLocale,
                                                   File temporaryDirectoryRoot,
                                                   ObserveSpeciesListConfiguration speciesListConfiguration,
                                                   ObserveDataSourceConnection dataSourceConnection) {

        ObserveServiceInitializer observeServiceInitializer = new ObserveServiceInitializer();
        observeServiceInitializer.setApplicationLocale(applicationLocale);
        observeServiceInitializer.setReferentialLocale(referentialLocale);
        observeServiceInitializer.setTemporaryDirectoryRoot(temporaryDirectoryRoot);
        observeServiceInitializer.setSpeciesListConfiguration(speciesListConfiguration);
        observeServiceInitializer.setDataSourceConfiguration(null);
        observeServiceInitializer.setDataSourceConnection(dataSourceConnection);
        return observeServiceInitializer;

    }

    public static ObserveServiceInitializer create(Locale applicationLocale,
                                                   ReferentialLocale referentialLocale,
                                                   File temporaryDirectoryRoot,
                                                   ObserveSpeciesListConfiguration speciesListConfiguration,
                                                   ObserveDataSourceConfiguration dataSourceConfiguration) {

        ObserveServiceInitializer observeServiceInitializer = new ObserveServiceInitializer();
        observeServiceInitializer.setApplicationLocale(applicationLocale);
        observeServiceInitializer.setReferentialLocale(referentialLocale);
        observeServiceInitializer.setTemporaryDirectoryRoot(temporaryDirectoryRoot);
        observeServiceInitializer.setSpeciesListConfiguration(speciesListConfiguration);
        observeServiceInitializer.setDataSourceConfiguration(dataSourceConfiguration);
        observeServiceInitializer.setDataSourceConnection(null);
        return observeServiceInitializer;

    }

    public static ObserveServiceInitializer create(Locale applicationLocale,
                                                   ReferentialLocale referentialLocale,
                                                   File temporaryDirectoryRoot,
                                                   ObserveSpeciesListConfiguration speciesListConfiguration,
                                                   ObserveDataSourceConfigurationAndConnection configurationAndConnection) {

        ObserveServiceInitializer observeServiceInitializer = new ObserveServiceInitializer();
        observeServiceInitializer.setApplicationLocale(applicationLocale);
        observeServiceInitializer.setReferentialLocale(referentialLocale);
        observeServiceInitializer.setTemporaryDirectoryRoot(temporaryDirectoryRoot);
        observeServiceInitializer.setSpeciesListConfiguration(speciesListConfiguration);
        observeServiceInitializer.setDataSourceConfiguration(null);
        observeServiceInitializer.setDataSourceConnection(configurationAndConnection.getConnection());
        observeServiceInitializer.setDataSourceConfiguration(configurationAndConnection.getConfiguration());
        return observeServiceInitializer;

    }

    public static ObserveServiceInitializer create(ObserveServiceInitializer otherObserveServiceInitializer) {

        ObserveServiceInitializer observeServiceInitializer;
        if (otherObserveServiceInitializer.withDataSourceConnection()) {
            observeServiceInitializer = create(
                    otherObserveServiceInitializer.getApplicationLocale(),
                    otherObserveServiceInitializer.getReferentialLocale(),
                    otherObserveServiceInitializer.getTemporaryDirectoryRoot(),
                    otherObserveServiceInitializer.getSpeciesListConfiguration(),
                    otherObserveServiceInitializer.getDataSourceConnection());
        } else {
            observeServiceInitializer = create(
                    otherObserveServiceInitializer.getApplicationLocale(),
                    otherObserveServiceInitializer.getReferentialLocale(),
                    otherObserveServiceInitializer.getTemporaryDirectoryRoot(),
                    otherObserveServiceInitializer.getSpeciesListConfiguration(),
                    otherObserveServiceInitializer.getDataSourceConfiguration());
        }

        return observeServiceInitializer;

    }

    /**
     * La locale à utiliser pour faire des traductions.
     */
    private Locale applicationLocale;

    /**
     * La locale à utiliser pour la source de données.
     */
    private ReferentialLocale referentialLocale;

    /**
     * La connexion à la source de données (peut être null si on a pas encore de connexion).
     */
    private ObserveDataSourceConnection dataSourceConnection;

    /**
     * La configuration à la source de données quand on a pas encore de connexion.
     */
    private ObserveDataSourceConfiguration dataSourceConfiguration;

    /**
     * Le répertoire où créer les répertoires temporaires.
     */
    private File temporaryDirectoryRoot;

    /**
     * La configuration des listes d'espèces (utilisé pour filtrer les listes d'espèces).
     */
    private ObserveSpeciesListConfiguration speciesListConfiguration;

    public Locale getApplicationLocale() {
        return applicationLocale;
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public ObserveDataSourceConnection getDataSourceConnection() {
        return dataSourceConnection;
    }

    public ObserveDataSourceConfiguration getDataSourceConfiguration() {
        return dataSourceConfiguration;
    }

    public File getTemporaryDirectoryRoot() {
        return temporaryDirectoryRoot;
    }

    public ObserveSpeciesListConfiguration getSpeciesListConfiguration() {
        return speciesListConfiguration;
    }

    public boolean withDataSourceConnection() {
        return dataSourceConnection != null;
    }

    public boolean withDataSourceConfiguration() {
        return dataSourceConfiguration != null;
    }

    protected void setApplicationLocale(Locale applicationLocale) {
        this.applicationLocale = applicationLocale;
    }

    protected void setReferentialLocale(ReferentialLocale referentialLocale) {
        this.referentialLocale = referentialLocale;
    }

    public void setDataSourceConnection(ObserveDataSourceConnection dataSourceConnection) {
        this.dataSourceConnection = dataSourceConnection;
    }

    public void setDataSourceConfiguration(ObserveDataSourceConfiguration dataSourceConfiguration) {
        this.dataSourceConfiguration = dataSourceConfiguration;
    }

    protected void setTemporaryDirectoryRoot(File temporaryDirectoryRoot) {
        this.temporaryDirectoryRoot = temporaryDirectoryRoot;
    }

    protected void setSpeciesListConfiguration(ObserveSpeciesListConfiguration speciesListConfiguration) {
        this.speciesListConfiguration = speciesListConfiguration;
    }

    protected ObserveServiceInitializer() {
    }
}
