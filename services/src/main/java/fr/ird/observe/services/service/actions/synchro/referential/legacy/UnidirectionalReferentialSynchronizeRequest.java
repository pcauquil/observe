package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multiset;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialMultimap;

import java.util.Objects;

/**
 * Pour un référentiel de type donné, l'ensemble des opérations à effectuer.
 *
 * À partir de cette demande, on génèrera ensuite le code sql correspondant.
 *
 * Created on 28/06/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeRequest<R extends ReferentialDto> implements ObserveDto {

    public static <R extends ReferentialDto> Builder<R> builder(Class<R> referentialName) {
        Objects.nonNull(referentialName);
        return new Builder<>(referentialName);
    }

    private final Class<R> referentialName;
    private final ReferentialMultimap<R> referentialToAdd;
    private final ReferentialMultimap<R> referentialToUpdate;
    private final ImmutableSet<String> referentialToRemove;
    private final ImmutableMap<String, String> referentialToReplace;

    public Class<R> getReferentialName() {
        return referentialName;
    }

    public ImmutableSet<R> getReferentialToAdd() {
        ImmutableSet<R> result = new ImmutableSet.Builder().build();

        Multiset<Class<R>> entries = referentialToAdd.keys();
        if (!entries.isEmpty()) {
            result = referentialToAdd.get(entries.iterator().next());
        }

        return result;
    }

    public ImmutableSet<R> getReferentialToUpdate() {
        ImmutableSet<R> result = new ImmutableSet.Builder().build();

        Multiset<Class<R>> entries = referentialToUpdate.keys();
        if (!entries.isEmpty()) {
            result = referentialToUpdate.get(entries.iterator().next());
        }

        return result;
    }

    public ImmutableSet<String> getReferentialToRemove() {
        return referentialToRemove;
    }

    public ImmutableMap<String, String> getReferentialToReplace() {
        return referentialToReplace;
    }

    public boolean withReferentialToAdd() {
        return !referentialToAdd.isEmpty();
    }

    public boolean withReferentialToUpdate() {
        return !referentialToUpdate.isEmpty();
    }

    public boolean withReferentialToRemove() {
        return !referentialToRemove.isEmpty();
    }

    public boolean withReferentialToReplace() {
        return !referentialToReplace.isEmpty();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("referentialName", referentialName.getName())
                          .add("referentialToAdd", referentialToAdd.size())
                          .add("referentialToUpdate", referentialToUpdate.size())
                          .add("referentialToRemove", referentialToRemove.size())
                          .add("referentialToReplace", referentialToReplace.size())
                          .toString();
    }

    private UnidirectionalReferentialSynchronizeRequest(Class<R> referentialName,
                                                        ReferentialMultimap<R> referentialToAdd,
                                                        ReferentialMultimap<R> referentialToUpdate,
                                                        ImmutableSet<String> referentialToRemove,
                                                        ImmutableMap<String, String> toReplace) {
        this.referentialName = referentialName;
        this.referentialToAdd = referentialToAdd;
        this.referentialToUpdate = referentialToUpdate;
        this.referentialToRemove = referentialToRemove;
        this.referentialToReplace = toReplace;
    }

    public static class Builder<R extends ReferentialDto> {

        private final Class<R> referentialName;
        private final ReferentialMultimap.Builder<R> toAddBuilder = ReferentialMultimap.builder();
        private final ReferentialMultimap.Builder<R> toUpdateBuilder = ReferentialMultimap.builder();
        private final ImmutableSet.Builder<String> toRemoveBuilder = ImmutableSet.builder();
        private final ImmutableMap.Builder<String, String> toReplaceBuilder = ImmutableMap.builder();

        public Builder entityToAdd(R referentialDto) {
            toAddBuilder.add(referentialDto);
            return this;
        }

        public Builder entityToUpdate(R referentialDto) {
            toUpdateBuilder.add(referentialDto);
            return this;
        }

        public Builder entityToRemove(String id) {
            toRemoveBuilder.add(id);
            return this;
        }

        public Builder entityToReplace(String sourceId, String targetId) {
            toReplaceBuilder.put(sourceId, targetId);
            return this;
        }

        public Class<R> getReferentialName() {
            return referentialName;
        }

        public UnidirectionalReferentialSynchronizeRequest<R> build() {

            return new UnidirectionalReferentialSynchronizeRequest<>(
                    referentialName,
                    toAddBuilder.build(),
                    toUpdateBuilder.build(),
                    toRemoveBuilder.build(),
                    toReplaceBuilder.build()
            );

        }

        private Builder(Class<R> referentialName) {
            this.referentialName = referentialName;
        }

    }
}
