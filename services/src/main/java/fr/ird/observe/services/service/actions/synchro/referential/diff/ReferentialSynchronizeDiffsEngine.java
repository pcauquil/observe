package fr.ird.observe.services.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialMultimap;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchronizeDiffsEngine {

    private final ReferentialSynchronizeDiffService leftDiffService;
    private final ReferentialSynchronizeDiffService rightDiffService;

    public ReferentialSynchronizeDiffsEngine(ReferentialSynchronizeDiffService leftDiffService,
                                             ReferentialSynchronizeDiffService rightDiffService) {
        this.leftDiffService = leftDiffService;
        this.rightDiffService = rightDiffService;
    }

    public ReferentialSynchronizeDiffs build() {

        ReferentialSynchronizeDiffs.Builder result = ReferentialSynchronizeDiffs.builder();

        ReferentialDataSourceStates leftSourceReferentialStates = leftDiffService.getSourceReferentialStates();
        ReferentialDataSourceStates rightSourceReferentialStates = rightDiffService.getSourceReferentialStates();

        ImmutableSet<Class<? extends ReferentialDto>> leftReferentialTypes = leftSourceReferentialStates.getReferentialTypes();
        ImmutableSet<Class<? extends ReferentialDto>> rightReferentialTypes = rightSourceReferentialStates.getReferentialTypes();
        ImmutableSet<Class<? extends ReferentialDto>> allReferentialTypes = ImmutableSet.<Class<? extends ReferentialDto>>builder().addAll(leftReferentialTypes).addAll(rightReferentialTypes).build();

        for (Class referentialType : allReferentialTypes) {

            build0(referentialType, result, leftSourceReferentialStates.getReferentialVersions(referentialType), rightSourceReferentialStates.getReferentialVersions(referentialType));
        }

        return result.build();
    }

    public <R extends ReferentialDto> ReferentialReferenceSet<R> getLeftReferentialReferenceSet(Class<R> referentialName, ImmutableSet<ReferentialSynchronizeDiffState> diffStates) {
        Set<String> ids = diffStates.stream().map(ReferentialSynchronizeDiffState::getId).collect(Collectors.toSet());
        return leftDiffService.getReferentialReferenceSet(referentialName, ImmutableSet.copyOf(ids));
    }

    public <R extends ReferentialDto> ReferentialReferenceSet<R> getRightReferentialReferenceSet(Class<R> referentialName, ImmutableSet<ReferentialSynchronizeDiffState> diffStates) {
        Set<String> ids = diffStates.stream().map(ReferentialSynchronizeDiffState::getId).collect(Collectors.toSet());
        return rightDiffService.getReferentialReferenceSet(referentialName, ImmutableSet.copyOf(ids));
    }

    public <R extends ReferentialDto> ImmutableSet<R> getLeftReferentials(Class<R> referentialName, ImmutableSet<ReferentialSynchronizeDiffState> diffStates) {
        Set<String> ids = diffStates.stream().map(ReferentialSynchronizeDiffState::getId).collect(Collectors.toSet());
        ReferentialMultimap<R> referentials = leftDiffService.getReferentials(referentialName, ImmutableSet.copyOf(ids));
        return referentials.get(referentialName);
    }

    public <R extends ReferentialDto> ImmutableSet<R> getRightReferentials(Class<R> referentialName, ImmutableSet<ReferentialSynchronizeDiffState> diffStates) {
        Set<String> ids = diffStates.stream().map(ReferentialSynchronizeDiffState::getId).collect(Collectors.toSet());
        ReferentialMultimap<R> referentials = rightDiffService.getReferentials(referentialName, ImmutableSet.copyOf(ids));
        return referentials.get(referentialName);
    }

    public <R extends ReferentialDto> ReferentialReferenceSet<R> getLeftEnabledReferentialReferenceSet(Class<R> referentialName) {
        return leftDiffService.getEnabledReferentialReferenceSet(referentialName);
    }

    public <R extends ReferentialDto> ReferentialReferenceSet<R> getRightEnabledReferentialReferenceSet(Class<R> referentialName) {
        return rightDiffService.getEnabledReferentialReferenceSet(referentialName);
    }

    private <R extends ReferentialDto> void build0(Class<R> referentialName, ReferentialSynchronizeDiffs.Builder result, ReferentialDataSourceState<R> leftStates, ReferentialDataSourceState<R> rightStates) {

        ImmutableMap<String, ReferentialSynchronizeDiffState> leftLatestReferentialDiffState = leftStates.getReferentialStatesById();
        ImmutableMap<String, ReferentialSynchronizeDiffState> rightLatestReferentialDiffState = rightStates.getReferentialStatesById();

        ImmutableSet<String> allIds = ImmutableSet.<String>builder().addAll(leftLatestReferentialDiffState.keySet()).addAll(rightLatestReferentialDiffState.keySet()).build();

        for (String id : allIds) {

            ReferentialSynchronizeDiffState leftReferentialState = leftLatestReferentialDiffState.get(id);
            ReferentialSynchronizeDiffState rightReferentialState = rightLatestReferentialDiffState.get(id);

            if (leftReferentialState == null) {

                // ajouté à droite
                result.addRightAddedReferential(referentialName, rightReferentialState);
                continue;
            }

            if (rightReferentialState == null) {

                // ajouté à gauche
                result.addLeftAddedReferential(referentialName, leftReferentialState);
                continue;
            }

            int order = leftReferentialState.compareTo(rightReferentialState);
            switch (order) {

                case ReferentialSynchronizeDiffState.BEFORE:

                    // la droite est plus récente
                    result.addRightUpdatedReferential(referentialName, rightReferentialState);
                    break;
                case ReferentialSynchronizeDiffState.AFTER:

                    // la gauche est plus récente
                    result.addLeftUpdatedReferential(referentialName, leftReferentialState);
                    break;
            }

        }

    }

}
