package fr.ird.observe.services.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Created on 12/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeContext {

    private final ImmutableSet<UnidirectionalReferentialSynchronizeRequest.Builder<?>> referentialSynchronizeRequestBuilders;
    private final UnidirectionalReferentialSynchronizeCallbackRequests callbackRequests;
    private Set<String> sqlRequests;

    public UnidirectionalReferentialSynchronizeContext(ImmutableSet<UnidirectionalReferentialSynchronizeRequest.Builder<?>> referentialSynchronizeRequestBuilders, UnidirectionalReferentialSynchronizeCallbackRequests callbackRequests) {
        this.referentialSynchronizeRequestBuilders = referentialSynchronizeRequestBuilders;
        this.callbackRequests = callbackRequests;
    }


    public ImmutableSet<UnidirectionalReferentialSynchronizeRequest.Builder<?>> getReferentialSynchronizeRequestBuilders() {
        return referentialSynchronizeRequestBuilders;
    }

    public UnidirectionalReferentialSynchronizeCallbackRequests getCallbackRequests() {
        return callbackRequests;
    }

    public boolean isNeedCallback() {
        return callbackRequests.isNotEmpty();
    }

    public void setSqlRequests(Set<String> sqlRequests) {
        this.sqlRequests = sqlRequests;
    }

    public Set<String> getSqlRequests() {
        return sqlRequests;
    }
}
