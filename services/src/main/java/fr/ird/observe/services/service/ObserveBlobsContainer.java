package fr.ird.observe.services.service;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;

/**
 * Pour contenir des blobs pour une colonne de type blob sur une table donnée.
 *
 * Created on 24/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ObserveBlobsContainer {

    private final String tableName;
    private final String columnName;
    private final ImmutableMap<String, byte[]> blobsById;

    public ObserveBlobsContainer(String tableName, String columnName, ImmutableMap<String, byte[]> blobsById) {
        this.tableName = tableName;
        this.columnName = columnName;
        this.blobsById = blobsById;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public ImmutableMap<String, byte[]> getBlobsById() {
        return blobsById;
    }

}
