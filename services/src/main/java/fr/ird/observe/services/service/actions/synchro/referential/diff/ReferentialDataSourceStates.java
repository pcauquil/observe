package fr.ird.observe.services.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialDataSourceStates {

    /**
     * Contient pour chaque type de référentiel la version de chaque référentiel indexé par son nom.
     */
    private final Map<Class<? extends ReferentialDto>, ReferentialDataSourceState> diffStatesByType;

    public ReferentialDataSourceStates() {
        this.diffStatesByType = new LinkedHashMap<>();
    }

    /**
     * @param referentialName le nom de référentiel
     * @return la collection des versions de référentiel du nom demandé.
     */
    public <R extends ReferentialDto> ReferentialDataSourceState<R> getReferentialVersions(Class<R> referentialName) {
        return diffStatesByType.get(referentialName);
    }

    public ImmutableSet<Class<? extends ReferentialDto>> getReferentialTypes() {
        return ImmutableSet.copyOf(diffStatesByType.keySet());
    }

    /**
     * Pour ajouter un référentiel.
     *
     * @param referentialName le nom du référentiel
     * @param sourceState     les états pour ce type de référentiel
     */
    public <R extends ReferentialDto> void addReferentialVersion(Class<R> referentialName, ReferentialDataSourceState<R> sourceState) {
        diffStatesByType.put(referentialName, sourceState);
    }
}
