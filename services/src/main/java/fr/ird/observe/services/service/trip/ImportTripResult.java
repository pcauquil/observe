package fr.ird.observe.services.service.trip;

/*-
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.ObserveDto;

/**
 * Created on 27/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportTripResult implements ObserveDto {

    private final String programId;
    private final String tripId;

    private final boolean imported;
    private final boolean deleted;

    private final long deleteTime;
    private final long importTime;

    public ImportTripResult(ImportTripRequest request, boolean imported, long importTime, boolean deleted, long deleteTime) {
        this.programId = request.getProgramId();
        this.tripId = request.getTripId();
        this.imported = imported;
        this.deleted = deleted;
        this.importTime = importTime;
        this.deleteTime = deleteTime;
    }

    public String getProgramId() {
        return programId;
    }

    public String getTripId() {
        return tripId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public boolean isImported() {
        return imported;
    }

    public long getImportTime() {
        return importTime;
    }

    public long getDeleteTime() {
        return deleteTime;
    }
}
