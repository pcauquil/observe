package fr.ird.observe.services.service.longline;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.spi.DeleteRequest;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteDataPermission;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public interface ActivityLonglineService extends ObserveService {

    @ReadDataPermission
    DataReferenceSet<ActivityLonglineDto> getActivityLonglineByTripLongline(String tripLonglineId);

    @ReadDataPermission
    int getActivityLonglinePositionInTripLongline(String tripLonglineId, String activityLonglineId);

    @ReadDataPermission
    Form<ActivityLonglineDto> loadForm(String activityLonglineId);

    @ReadDataPermission
    ActivityLonglineDto loadDto(String activityLonglineId);

    @ReadDataPermission
    DataReference<ActivityLonglineDto> loadReferenceToRead(String activityLonglineId);

    @ReadDataPermission
    boolean exists(String activityLonglineId);

    @WriteDataPermission
    Form<ActivityLonglineDto> preCreate(String tripLonglineId);

    @Write
    @WriteDataPermission
    @PostRequest
    TripChildSaveResultDto save(String tripLonglineId, ActivityLonglineDto dto);

    /**
     * @return true if the trip end date has been updated
     */
    @Write
    @WriteDataPermission
    @DeleteRequest
    boolean delete(String tripLonglineId, String activityLonglineId);

    @Write
    @WriteDataPermission
    @PostRequest
    int moveActivityLonglineToTripLongline(String activityLonglineId, String tripLonglineId);

    @Write
    @WriteDataPermission
    @PostRequest
    List<Integer> moveActivityLonglinesToTripLongline(List<String> activityLonglineIds, String tripLonglineId);
}
