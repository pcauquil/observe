package fr.ird.observe.services.service.longline;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.spi.DeleteRequest;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteDataPermission;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public interface SetLonglineService extends ObserveService {

    @ReadDataPermission
    Form<SetLonglineDto> loadForm(String setLonglineId);

    @ReadDataPermission
    SetLonglineDto loadDto(String setLonglineId);

    @ReadDataPermission
    DataReference<SetLonglineDto> loadReferenceToRead(String setLonglineId);

    @ReadDataPermission
    boolean exists(String setLonglineId);

    @WriteDataPermission
    Form<SetLonglineDto> preCreate(String activityLonglineId);

    @WriteDataPermission
    @Write
    @PostRequest
    SaveResultDto save(String activityLonglineId, SetLonglineDto dto);

    @Write
    @WriteDataPermission
    @DeleteRequest
    void delete(String activityLonglineId, String setLonglineId);

}
