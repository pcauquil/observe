package fr.ird.observe.services.service.longline;

/*
 * #%L
 * ObServe :: Services API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.spi.DeleteRequest;
import fr.ird.observe.services.spi.PostRequest;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteDataPermission;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public interface TripLonglineService extends ObserveService {

    @ReadDataPermission
    DataReferenceSet<TripLonglineDto> getAllTripLongline();

    @ReadDataPermission
    DataReferenceSet<TripLonglineDto> getTripLonglineByProgram(String programId);

    @ReadDataPermission
    int getTripLonglinePositionInProgram(String programId, String tripLonglineId);

    @ReadDataPermission
    TripMapDto getTripLonglineMap(String tripLonglineId);

    @ReadDataPermission
    Form<TripLonglineDto> loadForm(String tripLonglineId);

    @ReadDataPermission
    TripLonglineDto loadDto(String tripLonglineId);

    @ReadDataPermission
    DataReference<TripLonglineDto> loadReferenceToRead(String tripLonglineId);

    @ReadDataPermission
    boolean exists(String tripLonglineId);

    @WriteDataPermission
    Form<TripLonglineDto> preCreate(String programId);

    @Write
    @WriteDataPermission
    @PostRequest
    SaveResultDto save(TripLonglineDto dto);

    @Write
    @WriteDataPermission
    @DeleteRequest
    void delete(String tripLonglineId);

    @Write
    @WriteDataPermission
    @PostRequest
    int moveTripLonglineToProgram(String tripLonglineId, String programId);

    @Write
    @WriteDataPermission
    @PostRequest
    List<Integer> moveTripLonglinesToProgram(List<String> tripLonglineIds, String programId);

    @ReadDataPermission
    ImmutableList<ReferentialReference<SpeciesDto>> getSpeciesByListAndTrip(String tripLonglineId, String speciesListId);
}
