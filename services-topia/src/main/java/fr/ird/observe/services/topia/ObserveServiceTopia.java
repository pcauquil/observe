package fr.ird.observe.services.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.ObserveEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.topia.binder.BinderEngine;
import fr.ird.observe.services.topia.binder.data.DataBinderSupport;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataFileDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.result.SaveResultDtos;
import fr.ird.observe.services.service.ConcurrentModificationException;
import fr.ird.observe.services.service.DataNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaNoResultException;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

/**
 * Support pour toute implantation d'un service ToPIA.
 * <p>
 * Created on 16/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class ObserveServiceTopia implements ObserveService {

    protected static final BinderEngine BINDER_ENGINE = BinderEngine.get();
    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ObserveServiceTopia.class);
    protected ObserveServiceContextTopia serviceContext;

    public static <D extends DataDto, E extends ObserveDataEntity> Class<E> getDataEntityType(Class<D> dtoType) {
        return BINDER_ENGINE.getDataEntityType(dtoType);
    }

    public static <D extends ReferentialDto, E extends ObserveReferentialEntity> Class<E> getReferentialEntityType(Class<D> dtoType) {
        return BINDER_ENGINE.getReferentialEntityType(dtoType);
    }

    public static ImmutableSet<Class<? extends ReferentialDto>> getReferentialDtoTypes() {
        return BINDER_ENGINE.getReferentialDtoTypes();
    }

    public void setServiceContext(ObserveServiceContextTopia serviceContext) {
        Objects.requireNonNull(serviceContext, "serviceContext can't be null.");
        this.serviceContext = serviceContext;
    }

    public ReferentialLocale getReferentialLocale() {
        return serviceContext.getReferentialLocale();
    }

    public Locale getApplicationLocale() {
        return serviceContext.getApplicationLocale();
    }

    public <D extends IdDto, E extends ObserveEntity> E loadEntity(Class<D> dtoType, String id) {
        if (log.isInfoEnabled()) {
            log.info("Load entity: " + id);
        }
        try {
            return getTopiaPersistenceContext().findByTopiaId(id);
        } catch (TopiaNoResultException e) {
            throw new DataNotFoundException(dtoType, id);
        }
    }

    public <E extends ObserveEntity> E newEntity(Class<E> entityType) {
        ObserveTopiaPersistenceContext persistenceContext = getTopiaPersistenceContext();
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        return dao.newInstance();
    }

    public ObserveTopiaPersistenceContext getTopiaPersistenceContext() {
        return serviceContext.getTopiaPersistenceContext();
    }

    protected <E extends ObserveEntity> List<E> loadEntities(Class<E> entityType) {
        ObserveTopiaPersistenceContext persistenceContext = getTopiaPersistenceContext();
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        return dao.findAll();
    }

    public <E extends ObserveEntity> List<E> loadEntities(Class<E> entityType, Collection<String> ids) {
        ObserveTopiaPersistenceContext persistenceContext = getTopiaPersistenceContext();
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        return dao.forTopiaIdIn(ids).findAll();
    }

    protected <E extends ObserveEntity> Optional<Date> getLastUpdate(Class<E> entityType) {

        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();
        Date lastUpdateDate = persistenceContext.getLastUpdateDate(entityType);

        return Optional.ofNullable(lastUpdateDate);
    }

    protected <D extends IdDto, E extends ObserveEntity> void deleteEntity(Class<D> dtoType, Class<E> entityType, Iterable<String> ids) {
        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        for (String id : ids) {
            E entity = dao.forTopiaIdEquals(id).findUniqueOrNull();
            if (entity == null) {
                throw new DataNotFoundException(dtoType, id);
            }
            dao.delete(entity);
        }

        persistenceContext.updateLastUpdateDate(entityType, now());

    }

    protected <E extends ObserveEntity> SaveResultDto saveEntity(E entity) {

        Date lastUpdateDate = now();

        getTopiaPersistenceContext().updateLastUpdateDate(entity, lastUpdateDate);

        return SaveResultDtos.of(entity.getTopiaId(), lastUpdateDate);

    }

    protected <P extends ObserveEntity, E extends ObserveEntity> SaveResultDto saveEntity(P parenEntity, E entity) {

        Date lastUpdateDate = now();

        ObserveTopiaPersistenceContext persistenceContext = getTopiaPersistenceContext();

        persistenceContext.updateLastUpdateDate(parenEntity, lastUpdateDate);
        persistenceContext.updateLastUpdateDate(entity, lastUpdateDate);

        return SaveResultDtos.of(entity.getTopiaId(), lastUpdateDate);

    }

    protected <E extends ObserveDataEntity, D extends DataDto> Form<D> dataEntityToForm(Class<D> dtoType,
                                                                                        E entity,
                                                                                        ReferenceSetRequestDefinitions referentialRequestDefinition) {

        D dto = BINDER_ENGINE.transformEntityToDataDto(serviceContext.getReferentialLocale(), dtoType, entity);

        return Form.newFormDto(dtoType, dto, referentialRequestDefinition == null ? null :
                referentialRequestDefinition.name(), null);

    }

    protected <E extends ObserveReferentialEntity, D extends ReferentialDto> Form<D> referentialEntityToForm(Class<D> dtoType,
                                                                                                             E entity,
                                                                                                             ReferenceSetRequestDefinitions referentialRequestDefinition) {

        D dto = BINDER_ENGINE.transformEntityToReferentialDto(serviceContext.getReferentialLocale(), entity);

        return Form.newFormDto(dtoType, dto, referentialRequestDefinition == null ? null :
                referentialRequestDefinition.name(), null);

    }

    protected <E extends ObserveDataEntity, D extends DataDto> void copyDataDtoToEntity(D dto, E entity) {

        BINDER_ENGINE.copyDataDtoToEntity(serviceContext.getReferentialLocale(), dto, entity);

    }

    protected <E extends ObserveReferentialEntity, D extends ReferentialDto> void copyReferentialDtoToEntity(D dto, E entity) {

        BINDER_ENGINE.copyReferentialDtoToEntity(serviceContext.getReferentialLocale(), dto, entity);

    }

    protected <E extends ObserveDataEntity, D extends DataDto> E loadOrCreateEntityFromDataDto(D dto) {

        Class<D> dtoType = (Class<D>) dto.getClass();

        Class<E> entityType = getDataEntityType(dtoType);

        E entity;
        if (dto.isPersisted()) {
            entity = loadEntity(dtoType, dto.getId());
        } else {
            entity = newEntity(entityType);
        }
        return entity;
    }

    protected <E extends ObserveReferentialEntity, D extends ReferentialDto> E loadOrCreateEntityFromReferentialDto(D dto) {

        Class<D> dtoType = (Class<D>) dto.getClass();

        Class<E> entityType = getReferentialEntityType(dtoType);

        E entity;
        if (dto.isPersisted()) {
            entity = loadEntity(dtoType, dto.getId());
        } else {
            entity = newEntity(entityType);
        }
        return entity;
    }

    protected <E extends ObserveEntity, D extends IdDto> void checkLastUpdateDate(E entity, D dto) {

        if (entity.isPersisted()) {

            Date lastUpdateDate = entity.getLastUpdateDate();

            Date currentUpdateDate = dto.getLastUpdateDate();

            if (lastUpdateDate.after(currentUpdateDate)) {

                throw new ConcurrentModificationException(entity.getTopiaId(), lastUpdateDate, currentUpdateDate);

            }

        }

    }

    protected Date now() {
        return serviceContext.now();
    }

    protected <D extends DataDto, E extends ObserveDataEntity> D loadEntityToDataDto(Class<D> dtoType, String id) {

        E entity = loadEntity(dtoType, id);
        DataBinderSupport<E, D> binder = getDataBinder(dtoType);
        return binder.toData(getReferentialLocale(), entity);

    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> D loadEntityToReferentialDto(Class<D> dtoType, String id) {

        E entity = loadEntity(dtoType, id);
        ReferentialBinderSupport<E, D> binder = getReferentialBinder(dtoType);
        return binder.toDto(getReferentialLocale(), entity);

    }

    protected <E extends ObserveEntity> boolean existsEntity(Class<E> entityType, String id) {
        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        return dao.forTopiaIdEquals(id).exists();
    }

    protected <D extends ReferentialDto> ReferentialReference<D> toReference(ObserveReferentialEntity entity) {

        return BINDER_ENGINE.transformEntityToReferentialReferenceDto(getReferentialLocale(), entity);

    }

    protected <D extends DataDto> DataReference<D> toReference(ObserveDataEntity entity) {

        return BINDER_ENGINE.transformEntityToDataReferenceDto(getReferentialLocale(), entity);

    }


    protected <D extends DataDto, E extends ObserveDataEntity> DataReferenceSet<D> toDataReferenceSet(Class<D> dtoType, List<E> allStubByTripId) {

        DataBinderSupport<E, D> binder = getDataBinder(dtoType);

        ReferentialLocale referentialLocale = getReferentialLocale();

        ImmutableSet.Builder<DataReference<D>> references = ImmutableSet.builder();
        for (E activitySeine : allStubByTripId) {

            DataReference<D> reference = binder.toDataReference(referentialLocale, activitySeine);
            references.add(reference);

        }

        return DataReferenceSet.of(dtoType, references.build());

    }

    protected <D extends ReferentialDto, E extends ObserveReferentialEntity> ReferentialReferenceSet<D> toReferentialReferenceSet(Class<D> dtoType, List<E> allStubByTripId, Date lastUpdate) {

        ReferentialBinderSupport<E, D> binder = getReferentialBinder(dtoType);

        ReferentialLocale referentialLocale = getReferentialLocale();

        ImmutableSet.Builder<ReferentialReference<D>> references = ImmutableSet.builder();
        for (E activitySeine : allStubByTripId) {

            ReferentialReference<D> reference = binder.toReferentialReference(referentialLocale, activitySeine);
            references.add(reference);

        }

        return ReferentialReferenceSet.of(dtoType, references.build(), lastUpdate);

    }

    protected <D extends DataDto, E extends ObserveDataEntity> DataBinderSupport<E, D> getDataBinder(Class<D> dtoType) {

        return BINDER_ENGINE.getDataBinder(dtoType);

    }

    protected <D extends ReferentialDto, E extends ObserveReferentialEntity> ReferentialBinderSupport<E, D> getReferentialBinder(Class<D> dtoType) {

        return BINDER_ENGINE.getReferentialBinder(dtoType);

    }

    protected DataFileDto newDataFileDto(Blob data, String dataFilename) {
        DataFileDto dto = new DataFileDto();
        try {
            // on copie le blob pour supprimer le lien entre la blob venant de la base et la connexion à la base
            int length = (int) data.length();
            byte[] bytes = data.getBytes(1, length);
            dto.setContent(bytes);
        } catch (SQLException e) {
            if (log.isErrorEnabled()) {
                log.error("unable to read blod from " + dataFilename, e);
            }
        }
        dto.setName(dataFilename);

        return dto;
    }
}


