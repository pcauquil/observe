package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.seine.ReasonForDiscard;
import fr.ird.observe.entities.referentiel.seine.SpeciesFate;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class NonTargetCatchBinder extends DataBinderSupport<NonTargetCatch, NonTargetCatchDto> {

    public NonTargetCatchBinder() {
        super(NonTargetCatch.class, NonTargetCatchDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, NonTargetCatchDto dto, NonTargetCatch entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setCatchWeight(dto.getCatchWeight());
        entity.setMeanWeight(dto.getMeanWeight());
        entity.setMeanLength(dto.getMeanLength());
        entity.setTotalCount(dto.getTotalCount());
        entity.setCatchWeightComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_ENTITY.apply(dto.getCatchWeightComputedSource()));
        entity.setMeanWeightComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_ENTITY.apply(dto.getMeanWeightComputedSource()));
        entity.setMeanLengthComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_ENTITY.apply(dto.getMeanLengthComputedSource()));
        entity.setTotalCountComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_ENTITY.apply(dto.getTotalCountComputedSource()));
        entity.setSpeciesFate(toEntity(dto.getSpeciesFate(), SpeciesFate.class));
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setReasonForDiscard(toEntity(dto.getReasonForDiscard(), ReasonForDiscard.class));


    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, NonTargetCatch entity, NonTargetCatchDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setCatchWeight(entity.getCatchWeight());
        dto.setMeanWeight(entity.getMeanWeight());
        dto.setMeanLength(entity.getMeanLength());
        dto.setTotalCount(entity.getTotalCount());
        dto.setCatchWeightComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_DTO.apply(entity.getCatchWeightComputedSource()));
        dto.setMeanWeightComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_DTO.apply(entity.getMeanWeightComputedSource()));
        dto.setMeanLengthComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_DTO.apply(entity.getMeanLengthComputedSource()));
        dto.setTotalCountComputedSource(NON_TARGET_CATCH_COMPUTED_VALUE_SOURCE_TO_DTO.apply(entity.getTotalCountComputedSource()));

        dto.setSpeciesFate(toReferentialReference(referentialLocale, entity.getSpeciesFate(), SpeciesFateDto.class));
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setReasonForDiscard(toReferentialReference(referentialLocale, entity.getReasonForDiscard(), ReasonForDiscardDto.class));

    }

    @Override
    public DataReference<NonTargetCatchDto> toDataReference(ReferentialLocale referentialLocale, NonTargetCatch entity) {

        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getSpecies()),
                               getLabel(referentialLocale, entity.getSpeciesFate()));

    }

    @Override
    public DataReference<NonTargetCatchDto> toDataReference(ReferentialLocale referentialLocale, NonTargetCatchDto dto) {

        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getSpecies()),
                               getLabel(referentialLocale, dto.getSpeciesFate()));

    }
}
