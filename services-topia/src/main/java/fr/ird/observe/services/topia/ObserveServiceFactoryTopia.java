package fr.ird.observe.services.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.Reflection;
import fr.ird.observe.ObserveTopiaApplicationContext;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceFactorySupport;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaSupport;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConnectionTopia;
import fr.ird.observe.services.dto.UnauthorizedException;
import fr.ird.observe.services.spi.NoDataAccess;
import fr.ird.observe.services.spi.ReadDataPermission;
import fr.ird.observe.services.spi.ReadReferentialPermission;
import fr.ird.observe.services.spi.Write;
import fr.ird.observe.services.spi.WriteDataPermission;
import fr.ird.observe.services.spi.WriteReferentialPermission;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.TimeLog;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveServiceFactoryTopia extends ObserveServiceFactorySupport {

    protected static final LoadingCache<Class<?>, Class<?>> serviceTypeCache = newServiceImplementationTypesCache("Topia");
    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveServiceFactoryTopia.class);
    private static final TimeLog TIME_LOG = new TimeLog(ObserveServiceFactoryTopia.class, 100, 1000);

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {

        Objects.requireNonNull(dataSourceConfiguration, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return dataSourceConfiguration instanceof ObserveDataSourceConfigurationTopiaSupport;

    }

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {

        Objects.requireNonNull(dataSourceConnection, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return dataSourceConnection instanceof ObserveDataSourceConnectionTopia;

    }

    @Override
    public <S extends ObserveService> S newService(ObserveServiceInitializer observeServiceInitializer, Class<S> serviceType) {

        Objects.requireNonNull(observeServiceInitializer, "observeServiceInitializerContext can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");

        if (observeServiceInitializer.withDataSourceConnection()) {

            ObserveDataSourceConnection dataSourceConnection = observeServiceInitializer.getDataSourceConnection();
            Preconditions.checkArgument(dataSourceConnection instanceof ObserveDataSourceConnectionTopia, "dataSourceConnection must be of type " + ObserveDataSourceConnectionTopia.class.getName());

        } else {

            ObserveDataSourceConfiguration dataSourceConfiguration = observeServiceInitializer.getDataSourceConfiguration();
            Objects.requireNonNull(dataSourceConfiguration, "dataSourceConfiguration can't be null.");
            Preconditions.checkArgument(dataSourceConfiguration instanceof ObserveDataSourceConfigurationTopiaSupport, "dataSourceConfiguration must be of type " + ObserveDataSourceConfigurationTopiaSupport.class.getName());

        }

        Class<S> serviceTypeImpl = getServiceClassType(serviceTypeCache, serviceType);
        Objects.requireNonNull(serviceTypeImpl, "serviceTypeImpl not found for : " + serviceType.getName());

        ObserveServiceContextTopia serviceContext = createServiceContext(observeServiceInitializer);

        S service = newServiceInstance(serviceTypeImpl, serviceContext);
        service = newServiceTransactionalProxy(serviceType, service, serviceContext);
        return service;

    }

    protected ObserveServiceContextTopia createServiceContext(ObserveServiceInitializer observeServiceInitializer) {

        return new ObserveServiceContextTopia(observeServiceInitializer, mainServiceFactory, this);

    }

    protected <S extends ObserveService> S newServiceInstance(Class<S> serviceTypeImpl, ObserveServiceContextTopia serviceContext) {
        try {
            S service = serviceTypeImpl.newInstance();
            ((ObserveServiceTopia) service).setServiceContext(serviceContext);
            return service;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException("Could not create service", e);
        }
    }

    protected <S extends ObserveService> S newServiceTransactionalProxy(Class<S> serviceType, S service, ObserveServiceContextTopia serviceContext) {

        ObserveServiceInvocationHandler invocationHandler = new ObserveServiceInvocationHandler(serviceContext, service);
        return Reflection.newProxy(serviceType, invocationHandler);

    }

    @Override
    public void close() {

        ObserveTopiaApplicationContextFactory.close();

    }

    protected static class ObserveServiceInvocationHandler implements InvocationHandler {

        private final ObserveServiceContextTopia serviceContext;

        private final ObserveService target;

        private final Set<String> methodNamesToByPass;

        protected ObserveServiceInvocationHandler(ObserveServiceContextTopia serviceContext, ObserveService target) {

            this.serviceContext = serviceContext;
            this.target = target;
            this.methodNamesToByPass = ImmutableSet.of("equals",
                                                       "hashCode",
                                                       "finalize",
                                                       "toString",
                                                       "clone",
                                                       "getClass",
                                                       "close",
                                                       "destroy");

        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

            Object result;

            if (methodNamesToByPass.contains(method.getName()) || method.isAnnotationPresent(NoDataAccess.class)) {

                result = invokeMethod(method, args);

            } else {

                if (serviceContext.getTopiaApplicationContext() == null) {

                    ObserveTopiaApplicationContext topiaApplicationContext;
                    ObserveDataSourceConfigurationTopiaSupport dataSourceConfiguration;

                    if (serviceContext.withDataSourceConnection()) {

                        String authenticationToken = serviceContext.getDataSourceConnection().getAuthenticationToken();
                        topiaApplicationContext = ObserveTopiaApplicationContextFactory.getTopiaApplicationContext(authenticationToken);

                    } else {

                        dataSourceConfiguration = serviceContext.getDataSourceConfiguration();
                        topiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext(dataSourceConfiguration);

                    }

                    serviceContext.setTopiaApplicationContext(topiaApplicationContext);

                }

                boolean readDataPermissionPresent = method.isAnnotationPresent(ReadDataPermission.class);
                boolean writeDataPermissionPresent = method.isAnnotationPresent(WriteDataPermission.class);
                boolean readReferentialPermissionPresent = method.isAnnotationPresent(ReadReferentialPermission.class);
                boolean writeReferentialPermissionPresent = method.isAnnotationPresent(WriteReferentialPermission.class);
                if (readDataPermissionPresent
                        || writeDataPermissionPresent
                        || readReferentialPermissionPresent
                        || writeReferentialPermissionPresent) {

                    if (serviceContext.withDataSourceConnection()) {

                        ObserveDataSourceConnectionTopia dataSourceConnection = serviceContext.getDataSourceConnection();

                        if (readDataPermissionPresent && !dataSourceConnection.canReadData()
                                || writeDataPermissionPresent && !dataSourceConnection.canWriteData()
                                || readReferentialPermissionPresent && !dataSourceConnection.canReadReferential()
                                || writeReferentialPermissionPresent && !dataSourceConnection.canWriteReferential()) {

                            throw new UnauthorizedException(method.getClass().getCanonicalName(), method.getName());

                        }

                    } else {

                        throw new UnauthorizedException(method.getClass().getCanonicalName(), method.getName());

                    }

                }

                result = invokeMethodWithTransaction(method, args);

            }

            return result;

        }

        protected Object invokeMethod(Method method, Object... args) throws Throwable {

            long t0 = TimeLog.getTime();
            try {
                return method.invoke(target, args);
            } catch (InvocationTargetException e) {
                if (log.isErrorEnabled()) {
                    log.error("Error in method " + method.getName(), e);
                }
                throw e.getCause();
            } finally {

                TIME_LOG.log(t0, "invokeMethod", method.getName());

            }
        }

        protected Object invokeMethodWithTransaction(Method method, Object... args) throws Throwable {

            ObserveTopiaApplicationContext source = serviceContext.getTopiaApplicationContext();

            long t0 = TimeLog.getTime();
            try (ObserveTopiaPersistenceContext topiaPersistenceContext = source.newPersistenceContext()) {


                serviceContext.setTopiaPersistenceContext(topiaPersistenceContext);

                Object invoke = invokeMethod(method, args);

                if (method.isAnnotationPresent(Write.class)) {

                    // do commit
                    topiaPersistenceContext.commit();

                }

                return invoke;

            } finally {

                serviceContext.setTopiaPersistenceContext(null);

                TIME_LOG.log(t0, "invokeMethodWithTransaction", method.getName());
            }


        }

    }

}
