package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.referentiel.Harbour;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Person;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.referentiel.Vessel;
import fr.ird.observe.entities.referentiel.longline.TripType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.TripLonglineActivityDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.PersonDtos;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TripLonglineBinder extends DataBinderSupport<TripLongline, TripLonglineDto> {

    public TripLonglineBinder() {
        super(TripLongline.class, TripLonglineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TripLonglineDto dto, TripLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        entity.setTotalFishingOperationsNumber(dto.getTotalFishingOperationsNumber());
        entity.setHomeId(dto.getHomeId());
        entity.setTripType(toEntity(dto.getTripType(), TripType.class));
        entity.setOcean(toEntity(dto.getOcean(), Ocean.class));
        entity.setVessel(toEntity(dto.getVessel(), Vessel.class));
        entity.setProgram(toEntity(dto.getProgram(), Program.class));
        entity.setObserver(toEntity(dto.getObserver(), Person.class));
        entity.setCaptain(toEntity(dto.getCaptain(), Person.class));
        entity.setDataEntryOperator(toEntity(dto.getDataEntryOperator(), Person.class));
        entity.setDepartureHarbour(toEntity(dto.getDepartureHarbour(), Harbour.class));
        entity.setLandingHarbour(toEntity(dto.getLandingHarbour(), Harbour.class));
        entity.setActivityLongline(toEntitySet(referentialLocale, dto.getActivityLongline(), ActivityLongline.class, entity.getActivityLongline()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, TripLongline entity, TripLonglineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setStartDate(entity.getStartDate());
        dto.setEndDate(entity.getEndDate());
        dto.setTotalFishingOperationsNumber(entity.getTotalFishingOperationsNumber());
        dto.setHomeId(entity.getHomeId());
        dto.setTripType(toReferentialReference(referentialLocale, entity.getTripType(), TripTypeDto.class));
        dto.setOcean(toReferentialReference(referentialLocale, entity.getOcean(), OceanDto.class));
        dto.setVessel(toReferentialReference(referentialLocale, entity.getVessel(), VesselDto.class));
        dto.setProgram(toReferentialReference(referentialLocale, entity.getProgram(), ProgramDto.class));
        dto.setObserver(toReferentialReference(referentialLocale, entity.getObserver(), PersonDto.class));
        dto.setCaptain(toReferentialReference(referentialLocale, entity.getCaptain(), PersonDto.class));
        dto.setDataEntryOperator(toReferentialReference(referentialLocale, entity.getDataEntryOperator(), PersonDto.class));
        dto.setDepartureHarbour(toReferentialReference(referentialLocale, entity.getDepartureHarbour(), HarbourDto.class));
        dto.setLandingHarbour(toReferentialReference(referentialLocale, entity.getLandingHarbour(), HarbourDto.class));
        dto.setActivityLongline(toLinkedHashSetData(referentialLocale, entity.getActivityLongline(), TripLonglineActivityDto.class));

    }

    @Override
    public DataReference<TripLonglineDto> toDataReference(ReferentialLocale referentialLocale, TripLongline entity) {

        return toDataReference(entity,
                               entity.getStartDate(),
                               entity.getEndDate(),
                               getLabel(referentialLocale, entity.getVessel()),
                               entity.getObserverLabel());

    }

    @Override
    public DataReference<TripLonglineDto> toDataReference(ReferentialLocale referentialLocale, TripLonglineDto dto) {

        return toDataReference(dto,
                               dto.getStartDate(),
                               dto.getEndDate(),
                               getLabel(referentialLocale, dto.getVessel()),
                               PersonDtos.getNames(dto.getObserver()));

    }
}
