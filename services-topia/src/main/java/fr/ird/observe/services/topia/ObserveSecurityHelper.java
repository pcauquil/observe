package fr.ird.observe.services.topia;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.ObserveDbUserDtos;
import fr.ird.observe.services.dto.constants.ObserveDbRole;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.migration.mappings.TMSVersionHibernateDao;
import org.nuiton.topia.persistence.TopiaEntityEnum;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveSecurityHelper {

    public static final String OBSERVE_COMMON_SCHEMA_NAME = "observe_common";
    public static final String OBSERVE_SEINE_SCHEMA_NAME = "observe_seine";
    public static final String OBSERVE_LONGLINE_SCHEMA_NAME = "observe_longline";
    public static final Function<String, String> ESCAPE_STRING = input -> "\"" + input + "\"";
    protected static final String DROP_TABLE_PATTERN = "DROP TABLE IF EXISTS %s.%s CASCADE;\n";
    protected static final String DROP_SCHEMA_PATTERN = "DROP SCHEMA IF EXISTS %s CASCADE;\n";
    protected static final String REVOKE_ON_TABLE_ALL_PATTERN = "REVOKE ALL ON %s.%s FROM %s CASCADE;\n";
    protected static final String SET_ON_TABLE_OWNER_PATTERN = "ALTER TABLE %s.%s OWNER TO %s;\n";
    protected static final String GRANT_ON_TABLE_READ_PATTERN = "GRANT SELECT ON %s.%s TO %s;\n";
    protected static final String GRANT_ON_TABLE_ALL_PATTERN = "GRANT ALL ON %s.%s TO %s;\n";
    protected static final String GRANT_ON_FUNCTION_PATTERN = "GRANT EXECUTE ON FUNCTION %s TO %s;\n";
    protected static final String REVOKE_ON_SCHEMA_ALL_PATTERN = "REVOKE ALL ON SCHEMA %s FROM %s CASCADE;\n";
    protected static final String REVOKE_ON_FUNCTIONS_PATTERN = "REVOKE EXECUTE ON FUNCTION %s FROM %s CASCADE;\n";
    protected static final String GRANT_ON_SCHEMA_ALL_PATTERN = "GRANT USAGE ON SCHEMA %s TO %s;\n";
    protected static final Set<String> EXTRA_TABLES = Sets.newHashSet(
            TMSVersionHibernateDao.TABLE_NAME,
            TMSVersionHibernateDao.LEGACY_TABLE_NAME);
    protected static final Set<String> FUNCTION_NAMES_PREFIXS = Sets.newHashSet("ST_MakePoint",
                                                                                "ST_SetSRID",
                                                                                "sync_",
                                                                                "tr_sync",
                                                                                "ot_enhanced_school_type",
                                                                                "observe_");
    protected static final String SCHEMA_PUBLIC = "public";
    protected static final Set<String> SCHEMAS = Sets.newHashSet(SCHEMA_PUBLIC,
                                                                 OBSERVE_COMMON_SCHEMA_NAME,
                                                                 OBSERVE_SEINE_SCHEMA_NAME,
                                                                 OBSERVE_LONGLINE_SCHEMA_NAME);
    private static final Log log = LogFactory.getLog(ObserveSecurityHelper.class);
    protected final ObserveJdbcHelper jdbcHelper;
    protected final JdbcConfiguration jdbcConfiguration;


    public ObserveSecurityHelper(JdbcConfiguration jdbcConfiguration) {
        this.jdbcConfiguration = jdbcConfiguration;
        this.jdbcHelper = new ObserveJdbcHelper(jdbcConfiguration);
    }

    public void applySecurity(Set<ObserveDbUserDto> users, boolean showSql) {
        if (users == null) {
            throw new NullPointerException("users can not be null");
        }

        String script = createSecurityScript(users);

        if (showSql && log.isInfoEnabled()) {
            log.info("SQL to execute :\n" + script);
        }

        jdbcHelper.loadScript(script);

    }

    protected String createSecurityScript(Set<ObserveDbUserDto> users) {

        List<Pair<String, String>> tables = jdbcHelper.getTables(SCHEMAS, EXTRA_TABLES);

        if (tables.isEmpty()) {
            // no tables
            return "";
        }

        String administratorName = Iterables.get(getUserNamesByRole(users, ObserveDbRole.ADMINISTRATOR), 0);
        List<String> technicalNames = getUserNamesByRole(users, ObserveDbRole.TECHNICAL);
        List<String> usersNames = getUserNamesByRole(users, ObserveDbRole.USER);
        List<String> referentialNames = getUserNamesByRole(users, ObserveDbRole.REFERENTIAL);
        List<String> unusedNames = getUserNamesByRole(users, ObserveDbRole.UNUSED);


        if (log.isInfoEnabled()) {
            log.info("Will apply security on " + tables.size() + " table(s).");
            log.info(" - administrateur : " + administratorName);
            log.info(" - techniciens    : " + technicalNames);
            log.info(" - utilisateurs   : " + usersNames);
            log.info(" - referentiels   : " + referentialNames);
        }

        List<Pair<String, String>> referentielTables = getReferentielTables(tables);

        getDataTables(tables, referentielTables);

        Set<String> allPostgisFunctions = new LinkedHashSet<>();
        for (String postgisFunction : FUNCTION_NAMES_PREFIXS) {
            Set<String> postgisFunctions = jdbcHelper.getPostgisFunctions(postgisFunction);
            allPostgisFunctions.addAll(postgisFunctions);
        }

        StringBuilder builder = new StringBuilder();

        String administratorEscapedName = ESCAPE_STRING.apply(administratorName);
        Set<String> technicalEscapedNames = escapedNames(technicalNames);
        Set<String> usersEscapedNames = escapedNames(usersNames);
        Set<String> referentialEscapedNames = escapedNames(referentialNames);
        Set<String> unusedEscapedNames = escapedNames(unusedNames);


        // suppression de tous les droits
        {
            Set<String> privateRoles = new HashSet<>();
            privateRoles.add("public");
            privateRoles.addAll(referentialEscapedNames);
            privateRoles.addAll(usersEscapedNames);
            privateRoles.addAll(unusedEscapedNames);

            String roles = StringUtil.join(privateRoles, ",", true);

            addOnTablesForRole(REVOKE_ON_TABLE_ALL_PATTERN, builder, tables, roles);
            addOnSchemaForRole(REVOKE_ON_SCHEMA_ALL_PATTERN, builder, SCHEMAS, roles);
            addOnFunctionForRole(REVOKE_ON_FUNCTIONS_PATTERN, builder, allPostgisFunctions, roles);

        }

        // ajout propriétaire
        addOnTablesForRole(SET_ON_TABLE_OWNER_PATTERN, builder, tables, administratorEscapedName);
        addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, builder, SCHEMAS, administratorEscapedName);
        addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, builder, allPostgisFunctions, administratorEscapedName);

        // ajout administrateurs
        if (!technicalEscapedNames.isEmpty()) {
            String roles = StringUtil.join(technicalEscapedNames, ",", true);
            addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, builder, tables, roles);
            addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, builder, SCHEMAS, roles);
            addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, builder, allPostgisFunctions, roles);
        }

        // ajout utilisateur
        if (!usersEscapedNames.isEmpty()) {
            String roles = StringUtil.join(usersEscapedNames, ",", true);
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, builder, tables, roles);
            addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, builder, SCHEMAS, roles);
            addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, builder, allPostgisFunctions, roles);
        }

        // ajout referentiel
        if (!referentialEscapedNames.isEmpty()) {
            String roles = StringUtil.join(referentialEscapedNames, ",", true);
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, builder, referentielTables, roles);
            addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, builder, SCHEMAS, roles);
            addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, builder, allPostgisFunctions, roles);
        }

        String result = builder.toString();

        if (log.isInfoEnabled()) {
            log.info("Security script :\n" + result);
        }
        return result;


    }

    protected List<String> getUserNamesByRole(Set<ObserveDbUserDto> users, ObserveDbRole role) {
        return users.stream().filter(ObserveDbUserDtos.newRolePredicate(role)).map(ObserveDbUserDtos.NAME_FUNCTION).collect(Collectors.toList());
//        return Iterables.transform(Iterables.filter(users, ObserveDbUserDtos.newRolePredicate(role)), ObserveDbUserDtos.NAME_FUNCTION::apply);
    }

    protected Set<String> escapedNames(List<String> names) {
        return names.stream().map(ESCAPE_STRING).collect(Collectors.toSet());
    }

    protected List<Pair<String, String>> getReferentielTables(Iterable<Pair<String, String>> tables) {
        Set<TopiaEntityEnum> types = new HashSet<>();
        types.addAll(Arrays.asList(Entities.REFERENCE_ENTITIES));

        List<Pair<String, String>> result = getTables(tables, types, EXTRA_TABLES);
        if (log.isInfoEnabled()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Detected ").append(result.size()).append(" referentiel tables :");
            for (Pair<String, String> s : result) {
                sb.append("\n - ").append(s);
            }
            log.info(sb.toString());
        }
        return result;
    }

    protected List<Pair<String, String>> getDataTables(Collection<Pair<String, String>> tables,
                                                       Collection<Pair<String, String>> referentielTables) {
        List<Pair<String, String>> result = new ArrayList<>(tables);
        result.removeAll(referentielTables);

        if (log.isInfoEnabled()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Detected ").append(result.size()).append(" data tables :");
            for (Pair<String, String> s : result) {
                sb.append("\n - ").append(s);
            }
            log.info(sb.toString());
        }
        return result;
    }

    protected List<Pair<String, String>> getTables(Iterable<Pair<String, String>> tables,
                                                   Set<TopiaEntityEnum> types,
                                                   Set<String> extraTypes) {
        List<Pair<String, String>> result = new ArrayList<>();
        for (Pair<String, String> t : tables) {
            String table = t.getRight();
            String detectedType = null;
            for (TopiaEntityEnum type : types) {
                String name = type.dbTableName();
                if (table.equalsIgnoreCase(name) || table.startsWith(name + "_")) {
                    detectedType = name;
                    break;
                }
            }
            if (detectedType == null) {
                for (String extraType : extraTypes) {
                    if (table.equalsIgnoreCase(extraType)) {
                        detectedType = extraType;
                        break;
                    }
                }
            }
            if (detectedType != null && !result.contains(t)) {
                result.add(t);
            }
        }
        Collections.sort(result);
        return result;
    }

    protected void addOnTablesForRole(String pattern,
                                      StringBuilder builder,
                                      Iterable<Pair<String, String>> tables,
                                      String role) {

        for (Pair<String, String> t : tables) {
            builder.append(String.format(pattern, t.getLeft(), t.getRight(), role));
        }
    }

    protected void addOnSchemaForRole(String pattern,
                                      StringBuilder builder,
                                      Set<String> schemas,
                                      String role) {

        for (String t : schemas) {
            builder.append(String.format(pattern, t, role));
        }
    }

    protected void addOnFunctionForRole(String pattern,
                                        StringBuilder builder,
                                        Set<String> functions,
                                        String role) {

        for (String t : functions) {
            builder.append(String.format(pattern, t, role));
        }
    }


}
