package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.VesselSizeCategory;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class VesselSizeCategoryBinder extends ReferentialBinderSupport<VesselSizeCategory, VesselSizeCategoryDto> {

    public VesselSizeCategoryBinder() {
        super(VesselSizeCategory.class, VesselSizeCategoryDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, VesselSizeCategoryDto dto, VesselSizeCategory entity) {


        copyDtoReferentialFieldsToEntity(dto, entity);
        entity.setGaugeLabel(dto.getGaugeLabel());
        entity.setCapacityLabel(dto.getCapacityLabel());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, VesselSizeCategory entity, VesselSizeCategoryDto dto) {


        copyEntityReferentialFieldsToDto(entity, dto);
        dto.setGaugeLabel(entity.getGaugeLabel());
        dto.setCapacityLabel(entity.getCapacityLabel());

    }

    @Override
    public ReferentialReference<VesselSizeCategoryDto> toReferentialReference(ReferentialLocale referentialLocale, VesselSizeCategory entity) {

        return toReferentialReference(entity,
                                      entity.getCode(),
                                      entity.getGaugeLabel(),
                                      entity.getCapacityLabel());

    }

    @Override
    public ReferentialReference<VesselSizeCategoryDto> toReferentialReference(ReferentialLocale referentialLocale, VesselSizeCategoryDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(),
                                      dto.getGaugeLabel(),
                                      dto.getCapacityLabel());

    }
}
