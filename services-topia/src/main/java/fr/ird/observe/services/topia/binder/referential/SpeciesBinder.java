package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.SpeciesGroup;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SpeciesBinder extends ReferentialBinderSupport<Species, SpeciesDto> {

    public SpeciesBinder() {
        super(Species.class, SpeciesDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SpeciesDto dto, Species entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);

        entity.setSpeciesGroup(toEntity(dto.getSpeciesGroup(), SpeciesGroup.class));
        entity.setFaoCode(dto.getFaoCode());
        entity.setScientificLabel(dto.getScientificLabel());
        entity.setHomeId(dto.getHomeId());
        entity.setWormsId(dto.getWormsId());
        entity.setLengthMeasureType(dto.getLengthMeasureType());
        entity.setMinLength(dto.getMinLength());
        entity.setMaxLength(dto.getMaxLength());
        entity.setMinWeight(dto.getMinWeight());
        entity.setMaxWeight(dto.getMaxWeight());
        entity.setOcean(toEntitySet(dto.getOcean(), Ocean.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Species entity, SpeciesDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);

        dto.setSpeciesGroup(toReferentialReference(referentialLocale, entity.getSpeciesGroup(), SpeciesGroupDto.class));
        dto.setFaoCode(entity.getFaoCode());
        dto.setScientificLabel(entity.getScientificLabel());
        dto.setHomeId(entity.getHomeId());
        dto.setWormsId(entity.getWormsId());
        dto.setLengthMeasureType(entity.getLengthMeasureType());
        dto.setMinLength(entity.getMinLength());
        dto.setMaxLength(entity.getMaxLength());
        dto.setMinWeight(entity.getMinWeight());
        dto.setMaxWeight(entity.getMaxWeight());
        dto.setOcean(toReferentialReferenceList(referentialLocale, entity.getOcean(), OceanDto.class));

    }

    @Override
    public ReferentialReference<SpeciesDto> toReferentialReference(ReferentialLocale referentialLocale, Species entity) {

        return toReferentialReference(entity,
                                      getLabel(referentialLocale, entity),
                                      entity.getFaoCode(),
                                      entity.getScientificLabel(),
                                      entity.getHomeId(),
                                      entity.getLengthMeasureType());

    }

    @Override
    public ReferentialReference<SpeciesDto> toReferentialReference(ReferentialLocale referentialLocale, SpeciesDto dto) {

        return toReferentialReference(dto,
                                      getLabel(referentialLocale, dto),
                                      dto.getFaoCode(),
                                      dto.getScientificLabel(),
                                      dto.getHomeId(),
                                      dto.getLengthMeasureType());

    }
}
