package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.SensorUsed;
import fr.ird.observe.entities.referentiel.longline.SensorBrand;
import fr.ird.observe.entities.referentiel.longline.SensorDataFormat;
import fr.ird.observe.entities.referentiel.longline.SensorType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SensorUsedBinder extends DataBinderSupport<SensorUsed, SensorUsedDto> {

    public SensorUsedBinder() {
        super(SensorUsed.class, SensorUsedDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SensorUsedDto dto, SensorUsed entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        // on enregistre le fichier uniquement si il a changer
        if (dto.isHasData()) {
            if (dto.getData() != null) {
                entity.setData(byteArrayToBlob(dto.getData().getContent()));
                entity.setDataFilename(dto.getData().getName());
            }
        } else {
            entity.setData(null);
            entity.setDataFilename(null);
        }
        entity.setDataLocation(dto.getDataLocation());
        entity.setSensorSerialNo(dto.getSensorSerialNo());
        entity.setSensorType(toEntity(dto.getSensorType(), SensorType.class));
        entity.setSensorBrand(toEntity(dto.getSensorBrand(), SensorBrand.class));
        entity.setSensorDataFormat(toEntity(dto.getSensorDataFormat(), SensorDataFormat.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SensorUsed entity, SensorUsedDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        // on envoie pas le fichier de donné
        dto.setHasData(entity.getData() != null);
        // le champ data ne sert que pour l'upload du fichier si il a changé
        dto.setData(null);
        dto.setDataLocation(entity.getDataLocation());
        dto.setSensorSerialNo(entity.getSensorSerialNo());
        dto.setSensorType(toReferentialReference(referentialLocale, entity.getSensorType(), SensorTypeDto.class));
        dto.setSensorBrand(toReferentialReference(referentialLocale, entity.getSensorBrand(), SensorBrandDto.class));
        dto.setSensorDataFormat(toReferentialReference(referentialLocale, entity.getSensorDataFormat(), SensorDataFormatDto.class));

    }

    @Override
    public DataReference<SensorUsedDto> toDataReference(ReferentialLocale referentialLocale, SensorUsed entity) {

        return toDataReference(entity,
                getLabel(referentialLocale, entity.getSensorType()));

    }

    @Override
    public DataReference<SensorUsedDto> toDataReference(ReferentialLocale referentialLocale, SensorUsedDto dto) {

        return toDataReference(dto,
                getLabel(referentialLocale, dto.getSensorType()));

    }
}
