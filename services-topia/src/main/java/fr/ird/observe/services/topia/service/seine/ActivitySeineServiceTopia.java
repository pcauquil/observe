package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import fr.ird.observe.entities.referentiel.FpaZone;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.ActivitySeineTopiaDao;
import fr.ird.observe.entities.seine.ActivitySeines;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.service.DataNotFoundException;
import fr.ird.observe.services.service.seine.ActivitySeineService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ActivitySeineServiceTopia extends ObserveServiceTopia implements ActivitySeineService {

    private static final Log log = LogFactory.getLog(ActivitySeineServiceTopia.class);

    @Override
    public DataReferenceSet<ActivitySeineDto> getActivitySeineByRoute(String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("getActivitySeineByRoute(" + routeId + ")");
        }

        ReferentialLocale referenceLocale = getReferentialLocale();

        ActivitySeineTopiaDao dao = getTopiaPersistenceContext().getActivitySeineDao();
        List<ActivitySeine> allStubByTripId = dao.findAllStubByRouteId(routeId, referenceLocale.ordinal());

        return toDataReferenceSet(ActivitySeineDto.class, allStubByTripId);

    }


    @Override
    public int getActivitySeinePositionInRoute(String routeId, String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("getActivitySeinePositionInRoute(" + routeId + ", " + activitySeineId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);

        ActivitySeine activitySeine = route.getActivitySeineByTopiaId(activitySeineId);

        return getActivitySeinePositionInRoute(route, activitySeine);
    }

    @Override
    public DataReference<ActivitySeineDto> loadReferenceToRead(String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + activitySeineId + ")");
        }

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        return toReference(activitySeine);

    }

    @Override
    public ActivitySeineDto loadDto(String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + activitySeineId + ")");
        }

        return loadEntityToDataDto(ActivitySeineDto.class, activitySeineId);
    }

    @Override
    public boolean exists(String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + activitySeineId + ")");
        }

        return existsEntity(ActivitySeine.class, activitySeineId);

    }

    @Override
    public Form<ActivitySeineDto> loadForm(String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + activitySeineId + ")");
        }

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        Form<ActivitySeineDto> form = dataEntityToForm(ActivitySeineDto.class,
                                                       activitySeine,
                                                       ReferenceSetRequestDefinitions.ACTIVITY_SEINE_FORM);

        form.getObject().setObservedSystemEmpty(activitySeine.isObservedSystemEmpty());
        form.getObject().setFloatingObjectEmpty(activitySeine.isFloatingObjectEmpty());

        return form;

    }

    @Override
    public Form<ActivitySeineDto> preCreate(String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + routeId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);

        ActivitySeine preCreated = newEntity(ActivitySeine.class);

        ActivitySeine lastActivitySeine = Iterables.getLast(route.getActivitySeine(), null);

        Date time;

        FpaZone currentFpaZone = null;

        if (lastActivitySeine == null) {

            // première activité, on utilise l'heure courante
            time = serviceContext.now();

        } else {

            // on reprend l'heure de la dernière activité
            time = lastActivitySeine.getTime();

            // utilisation des zones fpa de la dernière activité
            currentFpaZone = lastActivitySeine.getNextFpaZone();
            if (currentFpaZone == null) {
                currentFpaZone = lastActivitySeine.getCurrentFpaZone();
            }

        }

        preCreated.setTime(DateUtil.getTime(time, false, false));
        preCreated.setCurrentFpaZone(currentFpaZone);

        Form<ActivitySeineDto> form = dataEntityToForm(ActivitySeineDto.class,
                                                       preCreated,
                                                       ReferenceSetRequestDefinitions.ACTIVITY_SEINE_FORM);

        form.getObject().setObservedSystemEmpty(preCreated.isObservedSystemEmpty());
        form.getObject().setFloatingObjectEmpty(preCreated.isFloatingObjectEmpty());

        return form;

    }

    @Override
    public SaveResultDto save(String routeId, ActivitySeineDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + routeId + ", " + dto.getId() + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);

        ActivitySeine entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto result = saveEntity(entity);

        if (dto.isNotPersisted()) {
            route.addActivitySeine(entity);
        }

        return result;

    }

    @Override
    public void delete(String routeId, String activitySeineId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + routeId + ", " + activitySeineId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);

        if (!route.containsActivitySeine(activitySeine)) {
            throw new DataNotFoundException(ActivitySeineDto.class, activitySeineId);
        }

        route.removeActivitySeine(activitySeine);

    }

    @Override
    public int moveActivitySeineToRoute(String activitySeineId, String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("moveActivitySeineToRoute(" + activitySeineId + ", " + routeId + ")");
        }

        ActivitySeine activitySeine = loadEntity(ActivitySeineDto.class, activitySeineId);
        Route route = loadEntity(RouteDto.class, routeId);

        route.addActivitySeine(activitySeine);
        saveEntity(route);

        return getActivitySeinePositionInRoute(route, activitySeine);

    }

    @Override
    public List<Integer> moveActivitySeinesToRoute(List<String> activitySeineIds, String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("moveActivitySeinesToRoute([" + Joiner.on(", ").join(activitySeineIds) + "]" + routeId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);

        List<Integer> result = new ArrayList<>();

        for (String activityId : activitySeineIds) {
            ActivitySeine activity = loadEntity(ActivitySeineDto.class, activityId);
            route.addActivitySeine(activity);

            result.add(getActivitySeinePositionInRoute(routeId, activityId));
        }

        saveEntity(route);

        return result;

    }

    protected int getActivitySeinePositionInRoute(Route route, ActivitySeine activitySeine) {
        return (int) route.getActivitySeine()
                          .stream()
                          .filter(ActivitySeines.newDateBeforePredicate(activitySeine.getTime()))
                          .count();
    }
}
