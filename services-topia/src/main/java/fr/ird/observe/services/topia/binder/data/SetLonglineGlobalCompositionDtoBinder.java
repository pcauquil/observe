package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.BaitsComposition;
import fr.ird.observe.entities.longline.BranchlinesComposition;
import fr.ird.observe.entities.longline.FloatlinesComposition;
import fr.ird.observe.entities.longline.HooksComposition;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.referentiel.longline.MitigationType;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SetLonglineGlobalCompositionDtoBinder extends DataBinderSupport<SetLongline, SetLonglineGlobalCompositionDto> {

    public SetLonglineGlobalCompositionDtoBinder() {
        super(SetLongline.class, SetLonglineGlobalCompositionDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SetLonglineGlobalCompositionDto dto, SetLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setFloatlinesComposition(toEntityCollection(referentialLocale, dto.getFloatlinesComposition(), FloatlinesComposition.class, entity.getFloatlinesComposition()));
        entity.setBranchlinesComposition(toEntityCollection(referentialLocale, dto.getBranchlinesComposition(), BranchlinesComposition.class, entity.getBranchlinesComposition()));
        entity.setHooksComposition(toEntityCollection(referentialLocale, dto.getHooksComposition(), HooksComposition.class, entity.getHooksComposition()));
        entity.setBaitsComposition(toEntityCollection(referentialLocale, dto.getBaitsComposition(), BaitsComposition.class, entity.getBaitsComposition()));
        entity.setMitigationType(toEntitySet(dto.getMitigationType(), MitigationType.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SetLongline entity, SetLonglineGlobalCompositionDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setFloatlinesComposition(toLinkedHashSetData(referentialLocale, entity.getFloatlinesComposition(), FloatlinesCompositionDto.class));
        dto.setBranchlinesComposition(toLinkedHashSetData(referentialLocale, entity.getBranchlinesComposition(), BranchlinesCompositionDto.class));
        dto.setHooksComposition(toLinkedHashSetData(referentialLocale, entity.getHooksComposition(), HooksCompositionDto.class));
        dto.setBaitsComposition(toLinkedHashSetData(referentialLocale, entity.getBaitsComposition(), BaitsCompositionDto.class));
        dto.setMitigationType(toReferentialReferenceList(referentialLocale, entity.getMitigationType(), MitigationTypeDto.class));

    }

}
