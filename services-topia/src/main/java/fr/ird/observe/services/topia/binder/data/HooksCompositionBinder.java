package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.HooksComposition;
import fr.ird.observe.entities.referentiel.longline.HookSize;
import fr.ird.observe.entities.referentiel.longline.HookType;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class HooksCompositionBinder extends DataBinderSupport<HooksComposition, HooksCompositionDto> {

    public HooksCompositionBinder() {
        super(HooksComposition.class, HooksCompositionDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, HooksCompositionDto dto, HooksComposition entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setProportion(dto.getProportion());
        entity.setHookOffset(dto.getHookOffset());
        entity.setHookSize(toEntity(dto.getHookSize(), HookSize.class));
        entity.setHookType(toEntity(dto.getHookType(), HookType.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, HooksComposition entity, HooksCompositionDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setProportion(entity.getProportion());
        dto.setHookOffset(entity.getHookOffset());
        dto.setHookSize(toReferentialReference(referentialLocale, entity.getHookSize(), HookSizeDto.class));
        dto.setHookType(toReferentialReference(referentialLocale, entity.getHookType(), HookTypeDto.class));

    }

}
