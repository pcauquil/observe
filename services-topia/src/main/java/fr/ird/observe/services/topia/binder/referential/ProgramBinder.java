package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Organism;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ProgramBinder extends ReferentialBinderSupport<Program, ProgramDto> {

    public ProgramBinder() {
        super(Program.class, ProgramDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, ProgramDto dto, Program entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);

        entity.setOrganism(toEntity(dto.getOrganism(), Organism.class));

        entity.setEndDate(dto.getEndDate());
        entity.setGearType(GEAR_TYPE_TO_ENTITY.apply(dto.getGearType()));
        entity.setNonTargetObservation(dto.getNonTargetObservation());
        entity.setTargetDiscardsObservation(dto.getTargetDiscardsObservation());
        entity.setSamplesObservation(dto.getSamplesObservation());
        entity.setObjectsObservation(dto.getObjectsObservation());
        entity.setDetailledActivitiesObservation(dto.getDetailledActivitiesObservation());
        entity.setMammalsObservation(dto.getMammalsObservation());
        entity.setBirdsObservation(dto.getBirdsObservation());
        entity.setBaitObservation(dto.getBaitObservation());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        entity.setComment(dto.getComment());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Program entity, ProgramDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);

        dto.setOrganism(toReferentialReference(referentialLocale, entity.getOrganism(), OrganismDto.class));

        dto.setGearType(GEAR_TYPE_TO_DTO.apply(entity.getGearType()));
        dto.setGearTypePrefix(entity.getGearTypePrefix());
        dto.setNonTargetObservation(entity.getNonTargetObservation());
        dto.setTargetDiscardsObservation(entity.getTargetDiscardsObservation());
        dto.setSamplesObservation(entity.getSamplesObservation());
        dto.setObjectsObservation(entity.getObjectsObservation());
        dto.setDetailledActivitiesObservation(entity.getDetailledActivitiesObservation());
        dto.setMammalsObservation(entity.getMammalsObservation());
        dto.setBirdsObservation(entity.getBirdsObservation());
        dto.setBaitObservation(entity.getBaitObservation());
        dto.setStartDate(entity.getStartDate());
        dto.setEndDate(entity.getEndDate());
        dto.setComment(entity.getComment());

    }

    @Override
    public ReferentialReference<ProgramDto> toReferentialReference(ReferentialLocale referentialLocale, Program entity) {

        return toReferentialReference(entity,
                                      getLabel(referentialLocale, entity),
                                      GEAR_TYPE_TO_DTO.apply(entity.getGearType()),
                                      entity.getGearTypePrefix());

    }

    @Override
    public ReferentialReference<ProgramDto> toReferentialReference(ReferentialLocale referentialLocale, ProgramDto dto) {

        return toReferentialReference(dto,
                                      getLabel(referentialLocale, dto),
                                      dto.getGearType(),
                                      dto.getGearTypePrefix());

    }


}
