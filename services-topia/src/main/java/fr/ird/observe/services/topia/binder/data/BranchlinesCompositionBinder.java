package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.BranchlinesComposition;
import fr.ird.observe.entities.referentiel.longline.LineType;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BranchlinesCompositionBinder extends DataBinderSupport<BranchlinesComposition, BranchlinesCompositionDto> {

    public BranchlinesCompositionBinder() {
        super(BranchlinesComposition.class, BranchlinesCompositionDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, BranchlinesCompositionDto dto, BranchlinesComposition entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setLength(dto.getLength());
        entity.setProportion(dto.getProportion());
        entity.setTopType(toEntity(dto.getTopType(), LineType.class));
        entity.setTracelineType(toEntity(dto.getTracelineType(), LineType.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, BranchlinesComposition entity, BranchlinesCompositionDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setLength(entity.getLength());
        dto.setProportion(entity.getProportion());
        dto.setTopType(toReferentialReference(referentialLocale, entity.getTopType(), LineTypeDto.class));
        dto.setTracelineType(toReferentialReference(referentialLocale, entity.getTracelineType(), LineTypeDto.class));

    }

}
