package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.longline.BaitHaulingStatus;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BaitHaulingStatusBinder extends ReferentialBinderSupport<BaitHaulingStatus, BaitHaulingStatusDto> {

    public BaitHaulingStatusBinder() {
        super(BaitHaulingStatus.class, BaitHaulingStatusDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, BaitHaulingStatusDto dto, BaitHaulingStatus entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, BaitHaulingStatus entity, BaitHaulingStatusDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);

    }

    @Override
    public ReferentialReference<BaitHaulingStatusDto> toReferentialReference(ReferentialLocale referentialLocale, BaitHaulingStatus entity) {

        return toReferentialReference(entity,
                                      entity.getCode(), getLabel(referentialLocale, entity));

    }

    @Override
    public ReferentialReference<BaitHaulingStatusDto> toReferentialReference(ReferentialLocale referentialLocale, BaitHaulingStatusDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(), getLabel(referentialLocale, dto));

    }
}
