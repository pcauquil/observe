package fr.ird.observe.services.topia.service.longline;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.services.service.longline.SetLonglineDetailCompositionService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SetLonglineDetailCompositionServiceTopia extends ObserveServiceTopia implements SetLonglineDetailCompositionService {

    private static final Log log = LogFactory.getLog(SetLonglineDetailCompositionServiceTopia.class);

    @Override
    public Form<SetLonglineDetailCompositionDto> loadForm(String setLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setLonglineId + ")");
        }

        SetLongline setLongline = loadEntity(SetLonglineDetailCompositionDto.class, setLonglineId);

        return dataEntityToForm(
                SetLonglineDetailCompositionDto.class,
                setLongline,
                ReferenceSetRequestDefinitions.SET_LONGLINE_DETAIL_COMPOSITION_FORM);

    }

    @Override
    public boolean canDeleteSection(String sectionId) {
        if (log.isTraceEnabled()) {
            log.trace("canDeleteSection(" + sectionId + ")");
        }

        Section section = loadEntity(SectionDto.class, sectionId);

        boolean sectionUsed = getTopiaPersistenceContext().getSectionDao().isUsed(section);

        // il ne doit pas être nécessaire de parcourir les paniers de las section
        // car si un des paniers de la section est utilisé alors la section est aussi utilisé.

        return !sectionUsed;

    }

    @Override
    public boolean canDeleteBasket(String basketId) {
        if (log.isTraceEnabled()) {
            log.trace("canDeleteBasket(" + basketId + ")");
        }

        Basket basket = loadEntity(BasketDto.class, basketId);

        boolean basketUsed = getTopiaPersistenceContext().getBasketDao().isUsed(basket);

        // il ne doit pas être nécessaire de parcourir les avançons du panier
        // car si un des avançons du panier est utilisé alors le panier est aussi utilisé.

        return !basketUsed;

    }

    @Override
    public boolean canDeleteBranchline(String branchlineId) {
        if (log.isTraceEnabled()) {
            log.trace("canDeleteBranchline(" + branchlineId + ")");
        }

        Branchline branchline = loadEntity(BranchlineDto.class, branchlineId);

        boolean branchlineUsed = getTopiaPersistenceContext().getBranchlineDao().isUsed(branchline);

        return !branchlineUsed;

    }

    @Override
    public SaveResultDto save(SetLonglineDetailCompositionDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        SetLongline entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto result = saveEntity(entity);

        for (Section section : entity.getSection()) {
            section.setSetLongline(entity);
        }

        return result;

    }

}
