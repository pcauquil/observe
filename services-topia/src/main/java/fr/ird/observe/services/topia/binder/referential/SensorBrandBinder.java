package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.longline.SensorBrand;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SensorBrandBinder extends ReferentialBinderSupport<SensorBrand, SensorBrandDto> {

    public SensorBrandBinder() {
        super(SensorBrand.class, SensorBrandDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SensorBrandDto dto, SensorBrand entity) {


        copyDtoReferentialFieldsToEntity(dto, entity);
        entity.setBrandName(dto.getBrandName());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SensorBrand entity, SensorBrandDto dto) {


        copyEntityReferentialFieldsToDto(entity, dto);
        dto.setBrandName(entity.getBrandName());

    }

    @Override
    public ReferentialReference<SensorBrandDto> toReferentialReference(ReferentialLocale referentialLocale, SensorBrand entity) {

        return toReferentialReference(entity,
                                      entity.getCode(),
                                      entity.getBrandName());

    }

    @Override
    public ReferentialReference<SensorBrandDto> toReferentialReference(ReferentialLocale referentialLocale, SensorBrandDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(),
                                      dto.getBrandName());

    }
}
