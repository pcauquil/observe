package fr.ird.observe.services.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.observe.ObserveTopiaApplicationContext;
import fr.ird.observe.ObserveTopiaConfiguration;
import fr.ird.observe.ObserveTopiaConfigurationFactory;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaApplicationContext;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Usine de contexte applicatif ToPIA.
 *
 * Created on 23/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveTopiaApplicationContextFactory {

    protected static final Map<ObserveDataSourceConfigurationTopiaSupport, ObserveTopiaApplicationContext> TOPIA_APPLICATION_CONTEXT_CACHE = new HashMap<>();
    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveTopiaApplicationContextFactory.class);

    public static ObserveTopiaApplicationContext getOrCreateTopiaApplicationContext(ObserveDataSourceConfigurationTopiaSupport dataSourceConfiguration) {

        Optional<ObserveTopiaApplicationContext> optionalTopiaApplicationContext = getTopiaApplicationContextIfPresent(dataSourceConfiguration);
        ObserveTopiaApplicationContext topiaApplicationContext;
        if (optionalTopiaApplicationContext.isPresent()) {
            topiaApplicationContext = optionalTopiaApplicationContext.get();
        } else {
            // Si on demande de créer ici une base, on ne fait rien sur le schéma
            topiaApplicationContext = createTopiaApplicationContext(dataSourceConfiguration, false);
        }
        return topiaApplicationContext;

    }

    public static ObserveTopiaApplicationContext createTopiaApplicationContext(ObserveDataSourceConfigurationTopiaSupport configuration, boolean initSchema) {

        Optional<ObserveTopiaApplicationContext> topiaApplicationContextIfPresent = getTopiaApplicationContextIfPresent(configuration);
        Preconditions.checkState(!topiaApplicationContextIfPresent.isPresent(), "There is already a TopiaApplicationContext for configuration: " + configuration);

        ObserveTopiaApplicationContext topiaApplicationContext = null;

        if (configuration instanceof ObserveDataSourceConfigurationTopiaH2) {
            topiaApplicationContext = createTopiaApplicationContext((ObserveDataSourceConfigurationTopiaH2) configuration, initSchema);
        } else if (configuration instanceof ObserveDataSourceConfigurationTopiaPG) {
            topiaApplicationContext = createTopiaApplicationContext((ObserveDataSourceConfigurationTopiaPG) configuration, initSchema);
        }
        Objects.requireNonNull(topiaApplicationContext, "Did not find how to create ObserveTopiaApplicationContext from: " + configuration);
        return topiaApplicationContext;

    }

    public static Optional<ObserveTopiaApplicationContext> getTopiaApplicationContextIfPresent(ObserveDataSourceConfigurationTopiaSupport dataSourceConfiguration) {

        ObserveTopiaApplicationContext topiaApplicationContext = TOPIA_APPLICATION_CONTEXT_CACHE.get(dataSourceConfiguration);
        return Optional.ofNullable(topiaApplicationContext);

    }

    public static ObserveTopiaApplicationContext getTopiaApplicationContext(String authenticationToken) {

        ObserveTopiaApplicationContext result = null;
        for (ObserveTopiaApplicationContext topiaApplicationContext : TOPIA_APPLICATION_CONTEXT_CACHE.values()) {
            if (authenticationToken.equals(topiaApplicationContext.getAuthenticationToken())) {
                result = topiaApplicationContext;
                break;
            }
        }
        Objects.requireNonNull(result, "Did not find how to create ObserveTopiaApplicationContext from: " + authenticationToken);
        return result;

    }

    public static void close() {

        for (TopiaApplicationContext topiaApplicationContext : new LinkedHashSet<>(TOPIA_APPLICATION_CONTEXT_CACHE.values())) {

            if (!topiaApplicationContext.isClosed()) {
                try {
                    topiaApplicationContext.close();
                } catch (Exception e) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not close topiaApplicationContext: " + topiaApplicationContext, e);
                    }
                }
            }
        }

        TOPIA_APPLICATION_CONTEXT_CACHE.clear();

    }

    public static ObserveTopiaConfiguration createTopiaConfiguration(ObserveDataSourceConfigurationTopiaPG configuration, boolean initSchema) {

        ObserveTopiaConfiguration topiaConfiguration = ObserveTopiaConfigurationFactory.forPostgresqlDatabase(
                configuration.getJdbcUrl(),
                configuration.getUsername(),
                String.valueOf(configuration.getPassword()),
                initSchema,
                configuration.isShowMigrationSql(),
                configuration.isShowMigrationProgression()
        );

        if (log.isInfoEnabled()) {
            log.info("PG Topia configuration: " + topiaConfiguration.getJdbcConnectionUrl());
        }
        if (log.isDebugEnabled()) {
            log.debug("PG Topia configuration: " + topiaConfiguration);
        }
        return topiaConfiguration;
    }


    protected static ObserveTopiaApplicationContext createTopiaApplicationContext(ObserveDataSourceConfigurationTopiaPG configuration, boolean initSchema) {

        ObserveTopiaConfiguration topiaConfiguration = createTopiaConfiguration(configuration, initSchema);

        return new MyObserveTopiaApplicationContext(topiaConfiguration, configuration);

    }

    protected static ObserveTopiaApplicationContext createTopiaApplicationContext(ObserveDataSourceConfigurationTopiaH2 configuration, boolean initSchema) {

        ObserveTopiaConfiguration topiaConfiguration = ObserveTopiaConfigurationFactory.forH2Database(
                configuration.getDirectory(),
                configuration.getDbName(),
                configuration.getUsername(),
                String.valueOf(configuration.getPassword()),
                initSchema,
                configuration.isShowMigrationSql(),
                configuration.isShowMigrationProgression()
        );

        if (log.isInfoEnabled()) {
            log.info("H2 Topia configuration: " + topiaConfiguration.getJdbcConnectionUrl());
        }
        if (log.isDebugEnabled()) {
            log.debug("H2 Topia configuration: " + topiaConfiguration);
        }
        return new MyObserveTopiaApplicationContext(topiaConfiguration, configuration);

    }

    protected static class MyObserveTopiaApplicationContext extends ObserveTopiaApplicationContext {

        private final ObserveDataSourceConfigurationTopiaSupport observeDataSourceConfiguration;

        public MyObserveTopiaApplicationContext(ObserveTopiaConfiguration topiaConfiguration, ObserveDataSourceConfigurationTopiaSupport observeDataSourceConfiguration) {
            super(topiaConfiguration);
            this.observeDataSourceConfiguration = observeDataSourceConfiguration;
            TOPIA_APPLICATION_CONTEXT_CACHE.put(observeDataSourceConfiguration, this);
        }

        @Override
        public void close() {
            try {
                super.close();
            } finally {
                // remove from cache
                TOPIA_APPLICATION_CONTEXT_CACHE.remove(observeDataSourceConfiguration);
            }
        }

    }
}
