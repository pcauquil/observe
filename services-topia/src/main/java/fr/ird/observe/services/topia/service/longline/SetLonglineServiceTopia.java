package fr.ird.observe.services.topia.service.longline;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.ActivityLonglineTopiaDao;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.longline.TripLonglineTopiaDao;
import fr.ird.observe.services.service.longline.SetLonglineService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.data.DataBinderSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineStubDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.DataNotFoundException;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SetLonglineServiceTopia extends ObserveServiceTopia implements SetLonglineService {

    private static final Log log = LogFactory.getLog(SetLonglineServiceTopia.class);

    @Override
    public Form<SetLonglineDto> loadForm(String setLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setLonglineId + ")");
        }

        SetLongline setLongline = loadEntity(SetLonglineDto.class, setLonglineId);

        Form<SetLonglineDto> form = dataEntityToForm(SetLonglineDto.class,
                                                     setLongline,
                                                     ReferenceSetRequestDefinitions.SET_LONGLINE_FORM);

        ActivityLonglineTopiaDao activityLonglineDao = getTopiaPersistenceContext().getActivityLonglineDao();

        ActivityLongline activityLongline = activityLonglineDao.forSetLonglineEquals(setLongline).findUnique();

        form.getObject().setOtherSets(getOtherSetLonglineDtos(activityLongline, setLongline));

        return form;

    }

    @Override
    public DataReference<SetLonglineDto> loadReferenceToRead(String setLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + setLonglineId + ")");
        }

        SetLongline setLongline = loadEntity(SetLonglineDto.class, setLonglineId);

        return toReference(setLongline);

    }

    @Override
    public SetLonglineDto loadDto(String setLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + setLonglineId + ")");
        }

        return loadEntityToDataDto(SetLonglineDto.class, setLonglineId);

    }

    @Override
    public boolean exists(String setLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + setLonglineId + ")");
        }

        return existsEntity(SetLongline.class, setLonglineId);

    }

    @Override
    public Form<SetLonglineDto> preCreate(String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + activityLonglineId + ")");
        }

        ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityLonglineId);

        SetLongline preCreated = newEntity(SetLongline.class);

        // on utilise la date - heure de l'activité pour initialiser les horodatages
        // de l'opération de peche
        Date timeStamp = activityLongline.getTimeStamp();
        preCreated.setSettingStartTimeStamp(timeStamp);
        preCreated.setSettingEndTimeStamp(DateUtils.addHours(timeStamp, 1));
        preCreated.setHaulingStartTimeStamp(DateUtils.addHours(timeStamp, 2));
        preCreated.setHaulingEndTimeStamp(DateUtils.addHours(timeStamp, 3));

        // on reporte la position de l'activité pour la position de début de filage
        Float latitude = activityLongline.getLatitude();
        Float longitude = activityLongline.getLongitude();

        // On enregistre deux fois les coordonnées car la première fois on perd le signe à cause de l'éditeur
        preCreated.setSettingStartLatitude(latitude);
        preCreated.setSettingStartLongitude(longitude);
        preCreated.setSettingStartLatitude(latitude);
        preCreated.setSettingStartLongitude(longitude);

        Form<SetLonglineDto> form = dataEntityToForm(SetLonglineDto.class,
                                                     preCreated,
                                                     ReferenceSetRequestDefinitions.SET_LONGLINE_FORM);

        form.getObject().setOtherSets(getOtherSetLonglineDtos(activityLongline, preCreated));

        return form;

    }

    @Override
    public SaveResultDto save(String activityLonglineId, SetLonglineDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + activityLonglineId + ", " + dto.getId() + ")");
        }

        ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityLonglineId);

        SetLongline entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto result = saveEntity(entity);

        if (dto.isNotPersisted()) {
            activityLongline.setSetLongline(entity);
        }

        return result;

    }

    @Override
    public void delete(String activityLonglineId, String setLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + activityLonglineId + ", " + setLonglineId + ")");
        }

        ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityLonglineId);

        SetLongline setLongline = loadEntity(SetLonglineDto.class, setLonglineId);

        if (!activityLongline.getSetLongline().equals(setLongline)) {

            throw new DataNotFoundException(SetLonglineDto.class, setLonglineId);

        }

        activityLongline.setSetLongline(null);

    }

    protected Set<SetLonglineStubDto> getOtherSetLonglineDtos(ActivityLongline currentActivityLongline, SetLongline setLongline) {

        Set<SetLonglineStubDto> otherSetLonglineDtos = Sets.newHashSet();

        TripLonglineTopiaDao tripLonglineDao = getTopiaPersistenceContext().getTripLonglineDao();

        TripLongline tripLongline = tripLonglineDao.forActivityLonglineContains(currentActivityLongline).findUnique();

        DataBinderSupport<ActivityLongline, ActivityLonglineDto> binder = getDataBinder(ActivityLonglineDto.class);

        for (ActivityLongline activityLongline : tripLongline.getActivityLongline()) {

            SetLongline otherSetLongline = activityLongline.getSetLongline();

            if (otherSetLongline != null && !setLongline.equals(otherSetLongline)) {

                SetLonglineStubDto otherSetDto = new SetLonglineStubDto();
                otherSetDto.setId(otherSetLongline.getTopiaId());
                otherSetDto.setHomeId(otherSetLongline.getHomeId());
                otherSetDto.setNumber(otherSetLongline.getNumber());

                DataReference<ActivityLonglineDto> reference = binder.toDataReference(getReferentialLocale(), activityLongline);
                otherSetDto.setActivityLongline(reference);
                otherSetLonglineDtos.add(otherSetDto);

            }

        }

        return otherSetLonglineDtos;

    }

}
