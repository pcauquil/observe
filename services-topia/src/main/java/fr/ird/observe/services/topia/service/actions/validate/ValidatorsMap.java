/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.validate;

import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Un dictionnaire de validateurs ordonnees par le type de leur bean.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.1
 */
public class ValidatorsMap {

    private final Map<Class<?>, SimpleBeanValidator<?>> delegate;

    public ValidatorsMap() {
        delegate = new HashMap<>();
    }

    public NuitonValidatorScope[] getScopes() {
        EnumSet<NuitonValidatorScope> result =
                EnumSet.noneOf(NuitonValidatorScope.class);
        for (SimpleBeanValidator<?> b : delegate.values()) {
            result.addAll(b.getScopes());
        }
        return result.toArray(new NuitonValidatorScope[result.size()]);
    }

    public <X> SimpleBeanValidator<X> getValidator(Class<X> klass) {
        return (SimpleBeanValidator) get(klass);
    }

    public int size() {
        return delegate.size();
    }

    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    public boolean containsKey(Class<?> key) {
        return delegate.containsKey(key);
    }

    public boolean containsValue(SimpleBeanValidator<?> value) {
        return delegate.containsValue(value);
    }

    public SimpleBeanValidator<?> get(Class<?> key) {
        return delegate.get(key);
    }

    public SimpleBeanValidator<?> put(Class<?> key, SimpleBeanValidator<?> value) {
        return delegate.put(key, value);
    }

    public Collection<SimpleBeanValidator<?>> values() {
        return delegate.values();
    }
}
