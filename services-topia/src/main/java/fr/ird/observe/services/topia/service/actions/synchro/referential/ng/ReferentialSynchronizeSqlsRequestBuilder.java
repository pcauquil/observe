package fr.ird.observe.services.topia.service.actions.synchro.referential.ng;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeRequest;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeSqlsRequest;
import fr.ird.observe.services.topia.binder.BinderEngine;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.actions.synchro.referential.ng.task.ReferentialSynchronizeTask;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.DeleteSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.DesactivateSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.InsertSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.InsertSqlWithCascadeStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.ReplaceSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.UpdateSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.UpdateSqlWithCascadeStatementGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 14/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeSqlsRequestBuilder {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialSynchronizeSqlsRequestBuilder.class);

    private final BinderEngine binderEngine;
    private final ReferentialSynchronizeServiceTopia service;
    private final ReferentialLocale referentialLocale;
    private final TopiaMetadataModel metadataModel;
    private final ReferentialSynchronizeRequest request;
    private final ReferentialSynchronizeSqlsRequest.Builder resultBuilder;

    private ReferentialSynchronizeSqlsRequestBuilder(ReferentialSynchronizeServiceTopia service,
                                                     TopiaMetadataModel metadataModel,
                                                     ReferentialSynchronizeRequest request) {
        this.metadataModel = metadataModel;
        this.request = request;
        this.binderEngine = BinderEngine.get();
        this.service = service;
        this.referentialLocale = service.getReferentialLocale();
        this.resultBuilder = new ReferentialSynchronizeSqlsRequest.Builder();
    }

    public static ReferentialSynchronizeSqlsRequestBuilder builder(ReferentialSynchronizeServiceTopia service,
                                                                   TopiaMetadataModel metadataModel,
                                                                   ReferentialSynchronizeRequest request) {

        return new ReferentialSynchronizeSqlsRequestBuilder(service, metadataModel, request);
    }

    public ReferentialSynchronizeSqlsRequest build() {

        request.getTypes().forEach(this::build0);
        return resultBuilder.build();

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> void build0(Class<R> type) {

        Class<E> entityType = binderEngine.getReferentialEntityType(type);
        ObserveEntityEnum entityEnum = ObserveEntityEnum.valueOf(entityType);
        String referentialName = entityEnum.name();

        Set<ReferentialSynchronizeTask<R>> addTasks = request.getAddTasks(type);
        if (log.isInfoEnabled()) {
            log.info("Add: " + addTasks.size());
        }
        onAdd(referentialName, type, addTasks);

        Set<ReferentialSynchronizeTask<R>> updateTasks = request.getUpdateTasks(type);
        if (log.isInfoEnabled()) {
            log.info("Update: " + updateTasks.size());
        }
        onUpdate(referentialName, type, updateTasks);

        Set<ReferentialSynchronizeTask<R>> revertTasks = request.getRevertTasks(type);
        if (log.isInfoEnabled()) {
            log.info("Revert: " + revertTasks.size());
        }
        onRevert(referentialName, type, revertTasks);

        Set<ReferentialSynchronizeTask<R>> deleteTasks = request.getDeleteTasks(type);
        if (log.isInfoEnabled()) {
            log.info("Delete: " + deleteTasks.size());
        }
        onDelete(referentialName, type, deleteTasks);

        Set<ReferentialSynchronizeTask<R>> desactivateTasks = request.getDesactivateTasks(type);
        if (log.isInfoEnabled()) {
            log.info("Desactivate: " + desactivateTasks.size());
        }
        onDesactivate(referentialName, type, desactivateTasks);

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> void onAdd(String referentialName, Class<R> type, Set<ReferentialSynchronizeTask<R>> tasks) {

        ReferentialBinderSupport<E, R> binder = binderEngine.getReferentialBinder(type);
        Class<E> entityType = binderEngine.getReferentialEntityType(type);
        TopiaMetadataEntity metadata = metadataModel.getEntity(referentialName);
        List<E> insertEntities = loadEntities(entityType, tasks);
        if (metadata.withShell()) {

            InsertSqlWithCascadeStatementGenerator<R> insertGenerator = new InsertSqlWithCascadeStatementGenerator<R>(metadata, type, request.getIdsOnlyExistingOnThisSide()) {

                @Override
                protected <D extends ReferentialDto> String insertMissingReferential(Class<D> referentialType, String id) {
                    return addExtraInsertStatement(referentialType, id);
                }
            };

            for (E entity : insertEntities) {
                R referential = binder.toDto(referentialLocale, entity);
                String sql = insertGenerator.generateSql(referential);

                if (log.isInfoEnabled()) {
                    log.info("Insert referential: " + type.getName() + " / " + referential.getId() + " -- " + sql);
                }
                resultBuilder.addInsertStatement(sql);
            }

        } else {

            InsertSqlStatementGenerator<R> insertGenerator = new InsertSqlStatementGenerator<>(metadata, type);
            for (E entity : insertEntities) {
                R referential = binder.toDto(referentialLocale, entity);
                String sql = insertGenerator.generateSql(referential);

                if (log.isInfoEnabled()) {
                    log.info("Insert referential: " + type.getName() + " / " + referential.getId() + " -- " + sql);
                }
                resultBuilder.addInsertStatement(sql);
            }
        }

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> void onUpdate(String referentialName, Class<R> type, Set<ReferentialSynchronizeTask<R>> tasks) {

        ReferentialBinderSupport<E, R> binder = binderEngine.getReferentialBinder(type);
        TopiaMetadataEntity metadata = metadataModel.getEntity(referentialName);
        Class<E> entityType = binderEngine.getReferentialEntityType(type);
        List<E> updateEntities = loadEntities(entityType, tasks);

        if (metadata.withShell()) {

            UpdateSqlWithCascadeStatementGenerator<R> updateGenerator = new UpdateSqlWithCascadeStatementGenerator<R>(metadata, type, request.getIdsOnlyExistingOnThisSide()) {
                @Override
                protected <D extends ReferentialDto> String insertMissingReferential(Class<D> referentialType, String id) {
                    return addExtraInsertStatement(referentialType, id);
                }
            };
            for (E entity : updateEntities) {
                R referential = binder.toDto(referentialLocale, entity);
                String sql = updateGenerator.generateSql(referential);

                if (log.isInfoEnabled()) {
                    log.info("Update referential: " + type.getName() + " / " + referential.getId() + " -- " + sql);
                }
                resultBuilder.addUpdateStatement(sql);
            }

        } else {

            UpdateSqlStatementGenerator<R> updateGenerator = new UpdateSqlStatementGenerator<>(metadata, type);
            for (E entity : updateEntities) {
                R referential = binder.toDto(referentialLocale, entity);
                String sql = updateGenerator.generateSql(referential);

                if (log.isInfoEnabled()) {
                    log.info("Update referential: " + type.getName() + " / " + referential.getId() + " -- " + sql);
                }
                resultBuilder.addUpdateStatement(sql);
            }
        }


    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> void onRevert(String referentialName, Class<R> type, Set<ReferentialSynchronizeTask<R>> tasks) {

        ReferentialBinderSupport<E, R> binder = binderEngine.getReferentialBinder(type);
        TopiaMetadataEntity metadata = metadataModel.getEntity(referentialName);
        Class<E> entityType = binderEngine.getReferentialEntityType(type);

        UpdateSqlStatementGenerator<R> updateGenerator = new UpdateSqlStatementGenerator<>(metadata, type);
        List<E> updateEntities = loadEntities(entityType, tasks);
        for (E entity : updateEntities) {
            R referential = binder.toDto(referentialLocale, entity);
            String sql = updateGenerator.generateSql(referential);

            if (log.isInfoEnabled()) {
                log.info("Revert referential: " + type.getName() + " / " + referential.getId() + " -- " + sql);
            }
            resultBuilder.addUpdateStatement(sql);
        }

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> void onDelete(String referentialName, Class<R> type, Set<ReferentialSynchronizeTask<R>> tasks) {

        TopiaMetadataEntity metadata = metadataModel.getEntity(referentialName);

        ReplaceSqlStatementGenerator<R> replaceGenerator = new ReplaceSqlStatementGenerator<>(metadataModel, referentialName);
        DeleteSqlStatementGenerator<R> deleteGenerator = new DeleteSqlStatementGenerator<>(metadata);

        for (ReferentialSynchronizeTask<R> task : tasks) {

            String referentialId = task.getReferentialId();
            Optional<String> replaceReferentialId = task.getOptionalReplaceReferentialId();
            if (replaceReferentialId.isPresent()) {
                String sql = computeReplaceRequestSql(type, replaceGenerator, referentialId, replaceReferentialId.get());
                resultBuilder.addDeleteStatement(sql);
            }
            String sql = deleteGenerator.generateSql(referentialId);

            if (log.isInfoEnabled()) {
                log.info("Delete referential: " + type.getName() + " / " + referentialId + " -- " + sql);
            }
            resultBuilder.addDeleteStatement(sql);
        }

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> void onDesactivate(String referentialName,
                                                                                              Class<R> type,
                                                                                              Set<ReferentialSynchronizeTask<R>> tasks) {

        TopiaMetadataEntity metadata = metadataModel.getEntity(referentialName);

        ReplaceSqlStatementGenerator<R> replaceGenerator = new ReplaceSqlStatementGenerator<>(metadataModel, referentialName);
        DesactivateSqlStatementGenerator<R> desactivateGenerator = new DesactivateSqlStatementGenerator<>(metadata);

        for (ReferentialSynchronizeTask<R> task : tasks) {
            String referentialId = task.getReferentialId();
            Optional<String> replaceReferentialId = task.getOptionalReplaceReferentialId();
            if (replaceReferentialId.isPresent()) {
                String sql = computeReplaceRequestSql(type, replaceGenerator, referentialId, replaceReferentialId.get());
                resultBuilder.addDesactivateStatement(sql);
            }
            String sql = desactivateGenerator.generateSql(referentialId);

            if (log.isInfoEnabled()) {
                log.info("Desactivate referential: " + type.getName() + " / " + referentialId + " -- " + sql);
            }
            resultBuilder.addDesactivateStatement(sql);
        }

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> String computeReplaceRequestSql(Class<R> type, ReplaceSqlStatementGenerator replaceSqlStatementGenerator, String referentialId, String replaceReferentialId) {

        String sql = replaceSqlStatementGenerator.generateSql(referentialId, replaceReferentialId);

        if (log.isInfoEnabled()) {
            log.info("Replace referential: " + type.getName() + " / " + referentialId + " -- " + sql);
        }

        return sql;

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> String addExtraInsertStatement(Class<R> referentialType, String id) {

        Class<E> entityType = binderEngine.getReferentialEntityType(referentialType);
        String referentialName = ObserveEntityEnum.valueOf(entityType).name();
        TopiaMetadataEntity metadata = metadataModel.getEntity(referentialName);

        InsertSqlStatementGenerator<R> insertGenerator = new InsertSqlStatementGenerator<>(metadata, referentialType);
        R referential = service.loadEntityToReferentialDto(referentialType, id);

        return insertGenerator.generateSql(referential);

    }

    private <R extends ReferentialDto, E extends ObserveReferentialEntity> List<E> loadEntities(Class<E> entityType, Set<ReferentialSynchronizeTask<R>> tasks) {

        Set<String> ids = tasks.stream().map(ReferentialSynchronizeTask::getReferentialId).collect(Collectors.toSet());
        return service.loadEntities(entityType, ids);

    }

}
