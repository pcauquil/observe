package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SetLongline;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BasketEntitiesExtractor extends EntitiesExtractor<Basket> {

    protected BasketEntitiesExtractor() {
        super(Basket.class, null);
    }

    @Override
    protected Collection<Basket> getEntitiesSetBase(ObserveTopiaPersistenceContext persistenceContext, Map<String, Object> requestContext) {

        SetLongline setLongline = (SetLongline) requestContext.get(Section.PROPERTY_SET_LONGLINE);

        LinkedHashSet<Basket> baskets = Sets.newLinkedHashSet();

        for (Section section : setLongline.getSection()) {

            baskets.addAll(section.getBasket());

        }

        return baskets;
    }
}
