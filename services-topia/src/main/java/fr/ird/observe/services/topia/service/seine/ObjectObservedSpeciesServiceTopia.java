package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.FloatingObject;
import fr.ird.observe.services.service.seine.ObjectObservedSpeciesService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObjectObservedSpeciesServiceTopia extends ObserveServiceTopia implements ObjectObservedSpeciesService {

    private static final Log log = LogFactory.getLog(ObjectObservedSpeciesServiceTopia.class);

    @Override
    public Form<FloatingObjectObservedSpeciesDto> loadForm(String floatingObjectId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + floatingObjectId + ")");
        }

        FloatingObject floatingObject =
                loadEntity(FloatingObjectObservedSpeciesDto.class, floatingObjectId);

        return dataEntityToForm(
                FloatingObjectObservedSpeciesDto.class,
                floatingObject,
                ReferenceSetRequestDefinitions.FLOATING_OBJECT_OBSERVED_SPECIES_FORM);
    }

    @Override
    public SaveResultDto save(FloatingObjectObservedSpeciesDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        FloatingObject entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);
        return saveEntity(entity);

    }
}
