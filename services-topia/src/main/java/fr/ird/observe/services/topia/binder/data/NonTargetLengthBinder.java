package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class NonTargetLengthBinder extends DataBinderSupport<NonTargetLength, NonTargetLengthDto> {

    public NonTargetLengthBinder() {
        super(NonTargetLength.class, NonTargetLengthDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, NonTargetLengthDto dto, NonTargetLength entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setLength(dto.getLength());
        entity.setWeight(dto.getWeight());
        entity.setLengthSource(dto.isLengthSource());
        entity.setWeightSource(dto.isWeightSource());
        entity.setCount(dto.getCount());
        entity.setAcquisitionMode(dto.getAcquisitionMode());
        entity.setPicturesReferences(dto.getPicturesReferences());
        entity.setLength(dto.getLength());
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setSex(toEntity(dto.getSex(), Sex.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, NonTargetLength entity, NonTargetLengthDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setLength(entity.getLength());
        dto.setWeight(entity.getWeight());
        dto.setLengthSource(entity.isLengthSource());
        dto.setWeightSource(entity.isWeightSource());
        dto.setCount(entity.getCount());
        dto.setAcquisitionMode(entity.getAcquisitionMode());
        dto.setPicturesReferences(entity.getPicturesReferences());
        dto.setLength(entity.getLength());
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setSex(toReferentialReference(referentialLocale, entity.getSex(), SexDto.class));

    }
}
