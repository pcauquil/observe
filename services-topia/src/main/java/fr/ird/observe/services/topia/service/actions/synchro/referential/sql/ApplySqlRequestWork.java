package fr.ird.observe.services.topia.service.actions.synchro.referential.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.support.TopiaSqlWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created on 14/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ApplySqlRequestWork implements TopiaSqlWork {

    private static final int BATCH_SIZE = 100;

    private final Iterable<String> sqlRequests;

    public ApplySqlRequestWork(Iterable<String> sqlRequests) {
        this.sqlRequests = sqlRequests;
    }

    @Override
    public void execute(Connection connection) throws SQLException {

        Statement statement = connection.createStatement();

        int count = 0;
        for (String sqlRequest : sqlRequests) {
            statement.addBatch(sqlRequest);
            if ((count % BATCH_SIZE) == 0) {
                flush(statement);
            }
        }
        flush(statement);

    }

    private void flush(Statement statement) throws SQLException {
        statement.executeBatch();
        statement.clearBatch();
    }
}
