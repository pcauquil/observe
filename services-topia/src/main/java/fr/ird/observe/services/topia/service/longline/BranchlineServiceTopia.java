package fr.ird.observe.services.topia.service.longline;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.services.service.longline.BranchlineService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class BranchlineServiceTopia extends ObserveServiceTopia implements BranchlineService {

    @Override
    public Form<BranchlineDto> loadForm(String setLonglineId, String id) {

        SetLongline setLongline = loadEntity(SetLonglineDto.class, setLonglineId);

        Branchline entity = loadEntity(BranchlineDto.class, id);

        Form<BranchlineDto> form = dataEntityToForm(BranchlineDto.class, entity, ReferenceSetRequestDefinitions.BRANCHLINE_FORM);
        form.getObject().setLastUpdateDate(setLongline.getLastUpdateDate());

        return form;

    }

    @Override
    public SaveResultDto save(String setLonglineId, BranchlineDto dto) {

        SetLongline setLongline = loadEntity(SetLonglineDto.class, setLonglineId);
        checkLastUpdateDate(setLongline, dto);

        Branchline entity = loadOrCreateEntityFromDataDto(dto);

        copyDataDtoToEntity(dto, entity);

        return saveEntity(setLongline, entity);

    }
}
