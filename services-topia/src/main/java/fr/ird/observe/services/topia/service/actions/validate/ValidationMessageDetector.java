/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.validate;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.FloatingObject;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.service.actions.validate.ValidationMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorEvent;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import static org.nuiton.i18n.I18n.l;

/**
 * Le detecteur de messages de validation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ValidationMessageDetector {

    /** Logger */
    private static final Log log = LogFactory.getLog(ValidationMessageDetector.class);

    /**
     * Le dictionnaire des messages détectés pour chaque entité visitée.
     */
    protected final Multimap<TopiaEntity, ValidationMessage> detectedMessages;

    /**
     * Le visiteur d'entités pour lancer la validation sur l'enveloppe d'une entité.
     */
    protected final MyTopiaEntityVisitor entityVisitor;

    protected final ReferentialLocale referenceLocale;

    public ValidationMessageDetector(ValidatorsMap validators, ValidationDataContext validationDataContext, ReferentialLocale referenceLocale) {
        this.detectedMessages = HashMultimap.create();
        this.entityVisitor = new MyTopiaEntityVisitor(validators, validationDataContext);
        this.referenceLocale = referenceLocale;
    }

    public void detectValidationMessages(TopiaEntity entity) {

        if (log.isDebugEnabled()) {
            log.debug("start for entity " + entity);
        }

        // détection sur l'entité e
        entity.accept(entityVisitor);

        // on vide le visiteur
        entityVisitor.clear();

    }

    public Set<Class<? extends TopiaEntity>> getDetectedEntityTypes() {

        // Première passe où l'on récupère les types de toutes les entités détectées
        Set<Class<? extends TopiaEntity>> contractAndImplementationTypes = new LinkedHashSet<>();
        for (TopiaEntity topiaEntity : detectedMessages.keySet()) {
            contractAndImplementationTypes.add(topiaEntity.getClass());
        }

        // Seconde passe pour ne conserver que les contrats
        Set<Class<? extends TopiaEntity>> result = new LinkedHashSet<>();
        for (Class<? extends TopiaEntity> type : contractAndImplementationTypes) {
            Class<? extends TopiaEntity> contract = ObserveEntityEnum.valueOf(type).getContract();
            result.add(contract);
        }

        return result;

    }

    public <E extends TopiaEntity> Map<TopiaEntity, Collection<ValidationMessage>> getDetectedMessages(final Class<E> entityType) {
        return Multimaps.filterEntries(detectedMessages, input -> entityType.isAssignableFrom(input.getKey().getClass())).asMap();
    }

    public String translateMessage(String message) {
        String text;
        if (!message.contains("##")) {
            text = l(referenceLocale.getLocale(), message);
        } else {
            StringTokenizer stk = new StringTokenizer(message, "##");
            String errorName = stk.nextToken();
            List<String> args = new ArrayList<>();
            while (stk.hasMoreTokens()) {
                args.add(stk.nextToken());
            }
            text = l(referenceLocale.getLocale(), errorName, args.toArray());
        }

        return text;
    }

    protected class MyTopiaEntityVisitor implements TopiaEntityVisitor, SimpleBeanValidatorListener {

        /**
         * Le dictionnaire des validateurs utilisables.
         */
        protected final ValidatorsMap validators;

        /**
         * Le contexte de données injecté dans le contexte de validation.
         */
        protected final ValidationDataContext validationDataContext;

        /**
         * La pile d'entités en cours de validation (le haut est l'entitié courante).
         */
        protected final Deque<TopiaEntity> path;

        /**
         * La liste des entités déjà visitées (pour éviter de les parcourir plusieurs fois).
         */
        protected final Set<TopiaEntity> explored;

        protected TopiaEntity currentEntity;

        protected MyTopiaEntityVisitor(ValidatorsMap validators, ValidationDataContext validationDataContext) {
            this.validators = validators;
            this.validationDataContext = validationDataContext;
            this.path = new LinkedList<>();
            this.explored = new HashSet<>();
        }

        @Override
        public void start(TopiaEntity e) {
            if (path.isEmpty()) {
                // start come in start method since last clear method invocation
                addPath(e);
            }
            explored.add(e);
            SimpleBeanValidator validator = getValidator(e.getClass());
            if (validator != null) {

                validator.addSimpleBeanValidatorListener(this);
                addEntityInContext(e);
                try {
                    validator.setBean(e);
                } finally {
                    validator.removeSimpleBeanValidatorListener(this);
                    validator.setBean(null);
                }
            }
        }

        @Override
        public void end(TopiaEntity e) {
            if (path.size() == 1) {
                // global visit is done
                removePath();
            }
            removeEntityFromContext(e);
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> type, Object value) {
            TopiaEntity e1 = getTopiaValue(value);
            if (e1 != null) {
                addPath(e1);
                try {
                    e1.accept(this);
                } finally {
                    removePath();
                }
            }
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> collectionType, Class<?> type, int index, Object value) {
            TopiaEntity e1 = getTopiaValue(value);
            if (e1 != null) {
                addPath(e1);
                try {
                    e1.accept(this);
                } finally {
                    removePath();
                }
            }
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> collectionType, Class<?> type, Object value) {
            Collection<?> cValue = (Collection<?>) value;
            if (TopiaEntity.class.isAssignableFrom(type) &&
                    cValue != null && !cValue.isEmpty()) {
                int i = 0;
                for (Object currentValue : cValue) {
                    visit(e, name, collectionType, type, i++, currentValue);
                }
            }
        }

        @Override
        public void clear() {
            explored.clear();
            validationDataContext.clear();

            // on reinitialise tous les validateurs
            for (SimpleBeanValidator<?> v : validators.values()) {
                v.setBean(null);
            }
        }

        @Override
        public void onFieldChanged(SimpleBeanValidatorEvent event) {

            // on ne traite que les messages a ajouter
            String[] messagesToAdd = event.getMessagesToAdd();

            if (messagesToAdd != null) {

                for (String messageToAdd : messagesToAdd) {

                    String translateMessage = translateMessage(messageToAdd);
                    ValidationMessage validationMessage = new ValidationMessage(event.getScope(), event.getField(), translateMessage);

                    if (!alreadyContains(detectedMessages.get(currentEntity), validationMessage)) {
                        detectedMessages.put(currentEntity, validationMessage);
                    }

                    if (log.isDebugEnabled()) {
                        log.debug(String.format("On entity %s add message %s", currentEntity.getTopiaId(), validationMessage));
                    }
                }

            }

        }

        protected boolean alreadyContains(Collection<ValidationMessage> currentMessages, ValidationMessage validationMessage) {
            return currentMessages
                    .stream()
                    .filter(
                            message -> message.getScope().equals(validationMessage.getScope())
                                    && message.getFieldName().equals(validationMessage.getFieldName())
                                    && message.getMessage().equals(validationMessage.getMessage())
                    )
                    .findFirst()
                    .isPresent();
        }

        protected TopiaEntity getTopiaValue(Object value) {
            TopiaEntity entity = (TopiaEntity) (value != null && value instanceof TopiaEntity ? value : null);
            if (entity != null && explored.contains(entity)) {
                // entite deja visitee
                entity = null;
            }
            return entity;
        }

        protected void addPath(TopiaEntity entity) {
            path.add(entity);
            currentEntity = entity;
        }

        protected void removePath() {
            path.removeLast();
            if (!path.isEmpty()) {
                currentEntity = path.peek();
            }
        }

        protected <T extends TopiaEntity> SimpleBeanValidator<T> getValidator(Class<T> entityType) {
            return validators.getValidator(entityType);
        }

        protected void addEntityInContext(TopiaEntity entity) {

            ObserveEntityEnum anEnum = ObserveEntityEnum.valueOf(entity);
            switch (anEnum) {
                case Route:
                    validationDataContext.setRouteEntity((Route) entity);
                    break;
                case TripSeine:
                    validationDataContext.setTripSeineEntity((TripSeine) entity);
                    break;
                case ActivitySeine:
                    validationDataContext.setActivitySeineEntity((ActivitySeine) entity);
                    break;
                case SetSeine:
                    validationDataContext.setSetSeineEntity((SetSeine) entity);
                    break;
                case TripLongline:
                    validationDataContext.setTripLonglineEntity((TripLongline) entity);
                    break;
                case ActivityLongline:
                    validationDataContext.setActivityLonglineEntity((ActivityLongline) entity);
                    break;
                case SetLongline:
                    validationDataContext.setSetLonglineEntity((SetLongline) entity);
                    break;
                case FloatingObject:
                    validationDataContext.setFloatingObjectEntity((FloatingObject) entity);
                    break;
                case NonTargetLength:
                    break;
                case TargetLength:
                    break;
                case TransmittingBuoy:
                    break;
                case NonTargetCatch:
                    break;
                case TargetCatch:
                    break;
                case NonTargetSample:
                    break;
                case TargetSample:
                    break;
                case ObjectObservedSpecies:
                    break;
                case SchoolEstimate:
                    break;
                case ObjectSchoolEstimate:
                    break;
                case VesselActivitySeine:
                    break;
                case SurroundingActivity:
                    break;
                case Vessel:
                    break;
                case VesselSizeCategory:
                    break;
                case WeightCategory:
                    break;
                case ReasonForNullSet:
                    break;
                case ReasonForNoFishing:
                    break;
                case SpeciesFate:
                    break;
                case ObjectFate:
                    break;
                case Species:
                    break;
                case SpeciesGroup:
                    break;
                case DetectionMode:
                    break;
                case Person:
                    break;
                case Ocean:
                    break;
                case TransmittingBuoyOperation:
                    break;
                case ObjectOperation:
                    break;
                case Organism:
                    break;
                case LengthWeightParameter:
                    break;
                case Country:
                    break;
                case Program:
                    break;
                case ReasonForDiscard:
                    break;
                case SpeciesStatus:
                    break;
                case ObservedSystem:
                    break;
                case TransmittingBuoyType:
                    break;
                case VesselType:
                    break;
                case ObjectType:
                    break;
                case Wind:
                    break;
            }
        }


        protected void removeEntityFromContext(TopiaEntity entity) {

            ObserveEntityEnum anEnum = ObserveEntityEnum.valueOf(entity);
            switch (anEnum) {
                case Route:
                    validationDataContext.setRouteEntity(null);
                    break;
                case TripSeine:
                    validationDataContext.setTripSeineEntity(null);
                    break;
                case ActivitySeine:
                    validationDataContext.setActivitySeineEntity(null);
                    break;
                case SetSeine:
                    validationDataContext.setSetSeineEntity(null);
                    break;
                case TripLongline:
                    validationDataContext.setTripLonglineEntity(null);
                    break;
                case ActivityLongline:
                    validationDataContext.setActivityLonglineEntity(null);
                    break;
                case SetLongline:
                    validationDataContext.setSetLonglineEntity(null);
                    break;
                case FloatingObject:
                    validationDataContext.setFloatingObjectEntity(null);
                    break;
                case NonTargetLength:
                    break;
                case TargetLength:
                    break;
                case TransmittingBuoy:
                    break;
                case NonTargetCatch:
                    break;
                case TargetCatch:
                    break;
                case NonTargetSample:
                    break;
                case TargetSample:
                    break;
                case ObjectObservedSpecies:
                    break;
                case SchoolEstimate:
                    break;
                case ObjectSchoolEstimate:
                    break;
                case VesselActivitySeine:
                    break;
                case SurroundingActivity:
                    break;
                case Vessel:
                    break;
                case VesselSizeCategory:
                    break;
                case WeightCategory:
                    break;
                case ReasonForNullSet:
                    break;
                case ReasonForNoFishing:
                    break;
                case SpeciesFate:
                    break;
                case ObjectFate:
                    break;
                case Species:
                    break;
                case SpeciesGroup:
                    break;
                case DetectionMode:
                    break;
                case Person:
                    break;
                case Ocean:
                    break;
                case TransmittingBuoyOperation:
                    break;
                case ObjectOperation:
                    break;
                case Organism:
                    break;
                case LengthWeightParameter:
                    break;
                case Country:
                    break;
                case Program:
                    break;
                case ReasonForDiscard:
                    break;
                case SpeciesStatus:
                    break;
                case ObservedSystem:
                    break;
                case TransmittingBuoyType:
                    break;
                case VesselType:
                    break;
                case ObjectType:
                    break;
                case Wind:
                    break;
            }
        }

    }

}
