package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.referentiel.longline.BaitHaulingStatus;
import fr.ird.observe.entities.referentiel.longline.BaitSettingStatus;
import fr.ird.observe.entities.referentiel.longline.BaitType;
import fr.ird.observe.entities.referentiel.longline.HookSize;
import fr.ird.observe.entities.referentiel.longline.HookType;
import fr.ird.observe.entities.referentiel.longline.LineType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BranchlineBinder extends DataBinderSupport<Branchline, BranchlineDto> {

    public BranchlineBinder() {
        super(Branchline.class, BranchlineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, BranchlineDto dto, Branchline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSettingIdentifier(dto.getSettingIdentifier());
        entity.setHaulingIdentifier(dto.getHaulingIdentifier());

        entity.setDepthRecorder(dto.getDepthRecorder());
        entity.setTimer(dto.getTimer());
        entity.setTimeSinceContact(dto.getTimeSinceContact());
        entity.setHookOffset(dto.getHookOffset());
        entity.setBranchlineLength(dto.getBranchlineLength());
        entity.setWeightedSwivel(dto.getWeightedSwivel());
        entity.setTimerTimeOnBoard(dto.getTimerTimeOnBoard());
        entity.setWeightedSnap(dto.getWeightedSnap());
        entity.setSwivelWeight(dto.getSwivelWeight());
        entity.setSnapWeight(dto.getSnapWeight());
        entity.setTracelineLength(dto.getTracelineLength());
        entity.setHookLost(dto.getHookLost());
        entity.setTraceCutOff(dto.getTraceCutOff());
        entity.setHookType(toEntity(dto.getHookType(), HookType.class));
        entity.setBaitType(toEntity(dto.getBaitType(), BaitType.class));
        entity.setTopType(toEntity(dto.getTopType(), LineType.class));
        entity.setTracelineType(toEntity(dto.getTracelineType(), LineType.class));
        entity.setBaitSettingStatus(toEntity(dto.getBaitSettingStatus(), BaitSettingStatus.class));
        entity.setBaitHaulingStatus(toEntity(dto.getBaitHaulingStatus(), BaitHaulingStatus.class));
        entity.setHookSize(toEntity(dto.getHookSize(), HookSize.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Branchline entity, BranchlineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSettingIdentifier(entity.getSettingIdentifier());
        dto.setHaulingIdentifier(entity.getHaulingIdentifier());

        dto.setDepthRecorder(entity.getDepthRecorder());
        dto.setTimer(entity.getTimer());
        dto.setTimeSinceContact(entity.getTimeSinceContact());
        dto.setHookOffset(entity.getHookOffset());
        dto.setBranchlineLength(entity.getBranchlineLength());
        dto.setWeightedSwivel(entity.getWeightedSwivel());
        dto.setTimerTimeOnBoard(entity.getTimerTimeOnBoard());
        dto.setWeightedSnap(entity.getWeightedSnap());
        dto.setSwivelWeight(entity.getSwivelWeight());
        dto.setSnapWeight(entity.getSnapWeight());
        dto.setTracelineLength(entity.getTracelineLength());
        dto.setHookLost(entity.getHookLost());
        dto.setTraceCutOff(entity.getTraceCutOff());
        dto.setHookType(toReferentialReference(referentialLocale, entity.getHookType(), HookTypeDto.class));
        dto.setBaitType(toReferentialReference(referentialLocale, entity.getBaitType(), BaitTypeDto.class));
        dto.setTopType(toReferentialReference(referentialLocale, entity.getTopType(), LineTypeDto.class));
        dto.setTracelineType(toReferentialReference(referentialLocale, entity.getTracelineType(), LineTypeDto.class));
        dto.setBaitSettingStatus(toReferentialReference(referentialLocale, entity.getBaitSettingStatus(), BaitSettingStatusDto.class));
        dto.setBaitHaulingStatus(toReferentialReference(referentialLocale, entity.getBaitHaulingStatus(), BaitHaulingStatusDto.class));
        dto.setHookSize(toReferentialReference(referentialLocale, entity.getHookSize(), HookSizeDto.class));

    }

    @Override
    public DataReference<BranchlineDto> toDataReference(ReferentialLocale referentialLocale, Branchline entity) {

        return toDataReference(entity, entity.getHaulingIdentifier(), entity.getSettingIdentifier());

    }

    @Override
    public DataReference<BranchlineDto> toDataReference(ReferentialLocale referentialLocale, BranchlineDto dto) {

        return toDataReference(dto, dto.getHaulingIdentifier(), dto.getSettingIdentifier());

    }
}
