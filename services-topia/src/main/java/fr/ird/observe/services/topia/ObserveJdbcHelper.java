package fr.ird.observe.services.topia;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.constants.ObserveDbRole;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveJdbcHelper extends JdbcHelper {

    protected static final Set<String> POSTGIS_TABLES = Sets.newHashSet("geometry_columns", "spatial_ref_sys");
    private static final Log log = LogFactory.getLog(ObserveJdbcHelper.class);


    public ObserveJdbcHelper(JdbcConfiguration jdbcConfiguration) {
        super(jdbcConfiguration);
    }

    public Set<String> getTablePrivileges(String schema, String tableName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = openConnection();
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet observerDataPrivilege = metaData.getTablePrivileges(null, schema, tableName);

            Set<String> tablePrivileges = Sets.newHashSet();

            while (observerDataPrivilege.next()) {
                String security = observerDataPrivilege.getString("PRIVILEGE");
                String grantee = observerDataPrivilege.getString("GRANTEE");
                // Il se peut que le login soit echappe sous la forme \"login\"
                grantee = grantee.replaceAll("\\\\\"", "");
                if (log.isDebugEnabled()) {
                    log.debug(String.format("(security %s) - grantee (%s)", security, grantee));
                }
                if (grantee.equals(jdbcConfiguration.getJdbcConnectionUser())) {
                    if (log.isDebugEnabled()) {
                        log.debug("for " + tableName + " table " + grantee + '/' + security);
                    }
                    tablePrivileges.add(security);
                }
            }

            return tablePrivileges;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(preparedStatement);
            closeQuietly(connection);
        }

    }

    public Version getVersion() {
        Version version = Version.VZERO;

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = openConnection();
            // la connexion est reussie, on recherche les droits de
            // récupération de la version de la base
            preparedStatement = connection.prepareStatement("SELECT version FROM tms_version;");
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String versionStr = resultSet.getString(1);
                version = Versions.valueOf(versionStr);
            }

        } catch (SQLException e) {
            if (log.isErrorEnabled()) {
                log.error(e);
            }
        } finally {
            closeQuietly(preparedStatement);
            closeQuietly(connection);
        }

        return version;

    }

    public List<ObserveDbUserDto> getUsers() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<ObserveDbUserDto> users = Lists.newLinkedList();

        try {
            connection = openConnection();
            // la connexion est reussie, on recherche les droits de
            // récupération de la version de la base
            preparedStatement = connection.prepareStatement("SELECT rolname FROM pg_catalog.pg_roles;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString(1);
                ObserveDbUserDto user = new ObserveDbUserDto();
                user.setName(name);
                if (jdbcConfiguration.getJdbcConnectionUser().equals(name)) {
                    user.setRole(ObserveDbRole.ADMINISTRATOR);
                } else {
                    user.setRole(ObserveDbRole.UNUSED);
                }

                users.add(user);
            }

            return users;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(preparedStatement);
            closeQuietly(connection);
        }
    }

    public List<Pair<String, String>> getTables(Set<String> schemas, Set<String> extraTables) {
        List<Pair<String, String>> result = new ArrayList<>();

        Connection connection = null;
        ResultSet tables = null;

        try {
            // recuperation des tables sur la base
            connection = openConnection();
            DatabaseMetaData data = connection.getMetaData();
            tables = data.getTables(null,
                                    null,
                                    null,
                                    new String[]{"TABLE"}
            );

            int columnCount = tables.getMetaData().getColumnCount();

            if (log.isDebugEnabled()) {
                StringBuilder builder = new StringBuilder();
                builder.append("\nheader");
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = tables.getMetaData().getColumnName(i);
                    builder.append("\n [").append(i).append("] :").append(columnName);
                }
                log.debug(builder.toString());
                log.debug("fetchSize : " + tables.getFetchSize());
            }
            while (tables.next()) {
                String schemaName = tables.getString(2);
                String tableName = tables.getString(3);
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Discover table named %s", tables));
                }
                if (!extraTables.contains(tableName)) {

                    if (POSTGIS_TABLES.contains(tableName)) {
                        continue;
                    }

                    if (schemaName == null || !schemas.contains(schemaName.toLowerCase())) {
                        continue;
                    }

                }

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Keep table: %s", tables));
                }
                result.add(Pair.of(schemaName, tableName));
            }

            Collections.sort(result);
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(connection);
            closeQuietly(tables);
        }
    }

    public Set<String> getPostgisFunctions(String functionPattern) {

        final Set<String> result = new LinkedHashSet<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        String sql = String.format("SELECT ns.nspname::text || '.' ||  p.proname::text || '(' || oidvectortypes(p.proargtypes)::text || ')'" +
                                           " FROM pg_proc p INNER JOIN pg_namespace ns ON (p.pronamespace = ns.oid)" +
                                           " WHERE ns.nspname = 'public' AND p.proname ILIKE '%s%%';", functionPattern);
        try {
            connection = openConnection();
            preparedStatement = connection.prepareStatement(sql);
            ResultSet set = preparedStatement.executeQuery();

            while (set.next()) {
                String functionPrototype = set.getString(1);
                result.add(functionPrototype);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(connection);
            closeQuietly(preparedStatement);
        }
        return result;

    }

    public void loadScript(String scriptContent) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = openConnection();

            preparedStatement = connection.prepareStatement(scriptContent);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(connection);
            closeQuietly(preparedStatement);
        }
    }


}
