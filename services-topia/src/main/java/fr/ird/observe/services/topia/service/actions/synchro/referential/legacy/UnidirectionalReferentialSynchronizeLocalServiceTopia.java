package fr.ird.observe.services.topia.service.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.LastUpdateDateService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeLocalService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeRequest;
import fr.ird.observe.services.topia.ObserveServiceContextTopia;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.BinderEngine;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.ApplySqlRequestWork;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.DeleteSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.InsertSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.ReplaceSqlStatementGenerator;
import fr.ird.observe.services.topia.service.actions.synchro.referential.sql.UpdateSqlStatementGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;
import org.nuiton.topia.persistence.support.TopiaSqlWork;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 27/06/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class UnidirectionalReferentialSynchronizeLocalServiceTopia extends ObserveServiceTopia implements UnidirectionalReferentialSynchronizeLocalService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UnidirectionalReferentialSynchronizeLocalServiceTopia.class);

    private LastUpdateDateService lastUpdateDateService;

    @Override
    public void setServiceContext(ObserveServiceContextTopia serviceContext) {
        super.setServiceContext(serviceContext);
        lastUpdateDateService = serviceContext.newService(LastUpdateDateService.class);
    }

    @Override
    public <R extends ReferentialDto> Set<String> filterIdsUsedInLocalSource(Class<R> referentialName, Set<String> ids) {

        if (log.isTraceEnabled()) {
            log.trace("filterIdsUsedInLocalSource(" + referentialName + ", " + ids + ")");
        }
        Class entityType = BinderEngine.get().getReferentialEntityType(referentialName);
        Set<String> result = new LinkedHashSet<>();
        for (String id : ids) {
            int count = countUsage0(entityType, id);
            if (count > 0) {
                result.add(id);
            }
        }
        return result;

    }

    @Override
    public <R extends ReferentialDto> Set<ReferentialReference<R>> getLocalSourceReferentialToDelete(Class<R> referentialName, Set<String> ids) {

        Class<? extends ObserveReferentialEntity> entityType = BinderEngine.get().getReferentialEntityType(referentialName);
        return getLocalSourceReferentialToDelete0(entityType, referentialName, ids);

    }

    @Override
    public <R extends ReferentialDto> Set<String> generateSqlRequests(UnidirectionalReferentialSynchronizeRequest<R> request) {

        if (log.isTraceEnabled()) {
            log.trace("generateSqlRequests(" + request + ")");
        }

        Set<String> result = new LinkedHashSet<>();

        Class<R> dtoType = request.getReferentialName();
        Class<ObserveReferentialEntity> entityType = BinderEngine.get().getReferentialEntityType(dtoType);
        ObserveEntityEnum entityEnum = ObserveEntityEnum.valueOf(entityType);
        String referentielName = entityEnum.name();

        TopiaMetadataModel metadataModel = serviceContext.getTopiaApplicationContext().getMetadataModel();
        TopiaMetadataEntity metadataEntity = metadataModel.getEntity(referentielName);
        if (request.withReferentialToAdd()) {

            InsertSqlStatementGenerator<R> sqlStatementGenerator = new InsertSqlStatementGenerator<>(metadataEntity, dtoType);
            for (R referentialDto : request.getReferentialToAdd()) {
                String sql = sqlStatementGenerator.generateSql(referentialDto);
                result.add(sql);
            }
        }

        if (request.withReferentialToUpdate()) {

            UpdateSqlStatementGenerator<R> sqlStatementGenerator = new UpdateSqlStatementGenerator<>(metadataEntity, dtoType);
            for (R referentialDto : request.getReferentialToUpdate()) {
                String sql = sqlStatementGenerator.generateSql(referentialDto);
                result.add(sql);
            }

        }

        if (request.withReferentialToReplace()) {

            ReplaceSqlStatementGenerator sqlStatementGenerator = new ReplaceSqlStatementGenerator(metadataModel, referentielName);
            for (Map.Entry<String, String> entry : request.getReferentialToReplace().entrySet()) {
                String sql = sqlStatementGenerator.generateSql(entry.getKey(), entry.getValue());
                result.add(sql);
            }

        }
        if (request.withReferentialToRemove()) {

            DeleteSqlStatementGenerator sqlStatementGenerator = new DeleteSqlStatementGenerator(metadataEntity);
            for (String id : request.getReferentialToRemove()) {
                String sql = sqlStatementGenerator.generateSql(id);
                result.add(sql);
            }

        }


        return result;

    }

    @Override
    public void applySqlRequests(Set<String> sqlRequests) {

        if (log.isTraceEnabled()) {
            log.trace("applySqlRequests(" + sqlRequests + ")");
        }

        TopiaSqlWork applySqlWork = new ApplySqlRequestWork(sqlRequests);
        getTopiaPersistenceContext().getSqlSupport().doSqlWork(applySqlWork);

    }

    @Override
    public void updateLastUpdateDates() {

        lastUpdateDateService.updateReferentialLastUpdateDates();

    }

    private <E extends ObserveReferentialEntity> int countUsage0(Class<E> entityType, String id) {

        TopiaDao<E> dao = getTopiaPersistenceContext().getDao(entityType);
        E e = dao.forTopiaIdEquals(id).findUnique();
        Map<Class<? extends TopiaEntity>, List<? extends TopiaEntity>> allUsages = dao.findAllUsages(e);
        int count = 0;
        for (List<? extends TopiaEntity> entities : allUsages.values()) {
            count += entities.size();
        }
        return count;

    }

    private <E extends ObserveReferentialEntity, R extends ReferentialDto> Set<ReferentialReference<R>> getLocalSourceReferentialToDelete0(Class<E> entityType, Class<R> dtoType, Set<String> ids) {

        TopiaDao<E> dao = getTopiaPersistenceContext().getDao(entityType);
        List<E> entities = dao.forTopiaIdIn(ids).findAll();
        ReferentialBinderSupport<E, R> referentialBinder = BinderEngine.get().getReferentialBinder(dtoType);
        Set<ReferentialReference<R>> result = new LinkedHashSet<>();
        for (E entity : entities) {
            ReferentialReference<R> rReferentialReference = referentialBinder.toReferentialReference(getReferentialLocale(), entity);
            result.add(rReferentialReference);
        }
        return result;

    }

}
