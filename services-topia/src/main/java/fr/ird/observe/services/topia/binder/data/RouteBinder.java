package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.RouteDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class RouteBinder extends DataBinderSupport<Route, RouteDto> {

    public RouteBinder() {
        super(Route.class, RouteDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, RouteDto dto, Route entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setDate(dto.getDate());
        entity.setStartLogValue(dto.getStartLogValue());
        entity.setEndLogValue(dto.getEndLogValue());
        entity.setCheckLevel(dto.getCheckLevel());
        entity.setLastUpdateDate(dto.getLastUpdateDate());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Route entity, RouteDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setDate(entity.getDate());
        dto.setStartLogValue(entity.getStartLogValue());
        dto.setEndLogValue(entity.getEndLogValue());
        dto.setCheckLevel(entity.getCheckLevel());

        dto.setActivitySeine(toLinkedHashSetData(referentialLocale, entity.getActivitySeine(), ActivitySeineStubDto.class));

    }

    @Override
    public DataReference<RouteDto> toDataReference(ReferentialLocale referentialLocale, Route entity) {

        return toDataReference(entity, entity.getDate(), entity.getComment());

    }

    @Override
    public DataReference<RouteDto> toDataReference(ReferentialLocale referentialLocale, RouteDto dto) {

        return toDataReference(dto, dto.getDate(), dto.getComment());

    }
}
