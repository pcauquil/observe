package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Country;
import fr.ird.observe.entities.referentiel.Person;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class PersonBinder extends ReferentialBinderSupport<Person, PersonDto> {

    public PersonBinder() {
        super(Person.class, PersonDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, PersonDto dto, Person entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        entity.setCode(dto.getCode());
        entity.setLastName(dto.getLastName());
        entity.setFirstName(dto.getFirstName());
        entity.setCaptain(dto.isCaptain());
        entity.setObserver(dto.isObserver());
        entity.setDataEntryOperator(dto.isDataEntryOperator());
        entity.setCountry(toEntity(dto.getCountry(), Country.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Person entity, PersonDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        dto.setCode(entity.getCode());
        dto.setLastName(entity.getLastName());
        dto.setFirstName(entity.getFirstName());
        dto.setCaptain(entity.isCaptain());
        dto.setObserver(entity.isObserver());
        dto.setDataEntryOperator(entity.isDataEntryOperator());
        dto.setCountry(toReferentialReference(referentialLocale, entity.getCountry(), CountryDto.class));

    }

    @Override
    public ReferentialReference<PersonDto> toReferentialReference(ReferentialLocale referentialLocale, Person entity) {

        return toReferentialReference(entity,
                                      entity.getFirstName(),
                                      entity.getLastName(),
                                      entity.isCaptain(),
                                      entity.isObserver(),
                                      entity.isDataEntryOperator());

    }

    @Override
    public ReferentialReference<PersonDto> toReferentialReference(ReferentialLocale referentialLocale, PersonDto dto) {

        return toReferentialReference(dto,
                                      dto.getFirstName(),
                                      dto.getLastName(),
                                      dto.isCaptain(),
                                      dto.isObserver(),
                                      dto.isDataEntryOperator());

    }
}
