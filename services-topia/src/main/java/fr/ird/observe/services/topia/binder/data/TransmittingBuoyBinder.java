package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Country;
import fr.ird.observe.entities.referentiel.seine.TransmittingBuoyOperation;
import fr.ird.observe.entities.referentiel.seine.TransmittingBuoyType;
import fr.ird.observe.entities.seine.TransmittingBuoy;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TransmittingBuoyBinder extends DataBinderSupport<TransmittingBuoy, TransmittingBuoyDto> {

    public TransmittingBuoyBinder() {
        super(TransmittingBuoy.class, TransmittingBuoyDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TransmittingBuoyDto dto, TransmittingBuoy entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setCode(dto.getCode());
        entity.setBrand(dto.getBrand());
        entity.setOwnership(OWNERSHIP_TO_ENTITY.apply(dto.getOwnership()));
        entity.setTransmittingBuoyOperation(toEntity(dto.getTransmittingBuoyOperation(), TransmittingBuoyOperation.class));
        entity.setTransmittingBuoyType(toEntity(dto.getTransmittingBuoyType(), TransmittingBuoyType.class));
        entity.setCountry(toEntity(dto.getCountry(), Country.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, TransmittingBuoy entity, TransmittingBuoyDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setCode(entity.getCode());
        dto.setBrand(entity.getBrand());
        dto.setOwnership(OWNERSHIP_TO_DTO.apply(entity.getOwnership()));
        dto.setTransmittingBuoyOperation(toReferentialReference(referentialLocale, entity.getTransmittingBuoyOperation(), TransmittingBuoyOperationDto.class));
        dto.setTransmittingBuoyType(toReferentialReference(referentialLocale, entity.getTransmittingBuoyType(), TransmittingBuoyTypeDto.class));
        dto.setCountry(toReferentialReference(referentialLocale, entity.getCountry(), CountryDto.class));

    }

    @Override
    public DataReference<TransmittingBuoyDto> toDataReference(ReferentialLocale referentialLocale, TransmittingBuoy entity) {
        return toDataReference(entity,
                               entity.getCode(),
                               entity.getBrand(),
                               toReferentialReference(referentialLocale, entity.getTransmittingBuoyType(), TransmittingBuoyTypeDto.class),
                               toReferentialReference(referentialLocale, entity.getTransmittingBuoyOperation(), TransmittingBuoyOperationDto.class));
    }

    @Override
    public DataReference<TransmittingBuoyDto> toDataReference(ReferentialLocale referentialLocale, TransmittingBuoyDto dto) {
        return toDataReference(dto, dto.getCode(), dto.getBrand(), dto.getTransmittingBuoyType(), dto.getTransmittingBuoyOperation());
    }

}
