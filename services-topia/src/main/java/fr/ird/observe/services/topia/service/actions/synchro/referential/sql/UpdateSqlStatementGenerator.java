package fr.ird.observe.services.topia.service.actions.synchro.referential.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Pour générer une requète sql de mise à jour à partir d'un référentiel donné.
 *
 * Created on 29/06/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class UpdateSqlStatementGenerator<R extends ReferentialDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UpdateSqlStatementGenerator.class);

    private static final String UPDATE_STATEMENT = "UPDATE %s.%s SET %s WHERE topiaId ='%s';\n";
    private static final String MANY_TO_MANY_ASSOCIATION_DELETE_STATEMENT = "DELETE FROM %s.%s WHERE %s = '%s';\n";
    private static final String MANY_TO_MANY_ASSOCIATION_INSERT_STATEMENT = "INSERT INTO %s.%s(%s, %s) VALUES ('%s', '%s');\n";

    private final Map<String, String> columnNames;
    private final String schemaName;
    private final String tableName;
    private final Binder<R, R> binder;
    private final String[] simplePropertyNames;
    private final String[] manyToOneAssociationNames;
    private final Set<ManyToManyAssociationStruct> manyToManyAssociations;
    private final Set<String> primitiveBooleanPropertyNames;
    private final Set<String> primitiveIntegerPropertyNames;
    private final Set<String> primitiveLongPropertyNames;
    private final Set<String> primitiveFloatPropertyNames;

    public UpdateSqlStatementGenerator(TopiaMetadataEntity metadataEntity, Class<R> dtoType) {
        this.schemaName = metadataEntity.getDbSchemaName();
        this.tableName = metadataEntity.getDbTableName();

        Set<String> simplePropertyNamesSet = metadataEntity.getProperties().keySet();
        this.simplePropertyNames = simplePropertyNamesSet.toArray(new String[simplePropertyNamesSet.size()]);
        this.primitiveBooleanPropertyNames = metadataEntity.getPrimitivePropertyNames("boolean");
        this.primitiveIntegerPropertyNames = metadataEntity.getPrimitivePropertyNames("int");
        this.primitiveLongPropertyNames = metadataEntity.getPrimitivePropertyNames("long");
        this.primitiveFloatPropertyNames = metadataEntity.getPrimitivePropertyNames("float");

        Set<String> manyToOneAssociationNamesSet = metadataEntity.getManyToOneAssociations().keySet();
        this.manyToOneAssociationNames = manyToOneAssociationNamesSet.toArray(new String[manyToOneAssociationNamesSet.size()]);

        Map<String, String> manyToManyAssociationsMap = metadataEntity.getManyToManyAssociations();
        this.manyToManyAssociations = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : manyToManyAssociationsMap.entrySet()) {
            String propertyName = entry.getKey();
            String dbColumnName = metadataEntity.getDbColumnName(propertyName);
            String tableName = metadataEntity.getBdManyToManyAssociationTableName(propertyName);
            ManyToManyAssociationStruct manyToManyAssociation = new ManyToManyAssociationStruct(propertyName, dbColumnName, tableName);
            manyToManyAssociations.add(manyToManyAssociation);
        }
        this.columnNames = computeColumnNames(metadataEntity, simplePropertyNames, manyToOneAssociationNames);
        this.binder = BinderFactory.newBinder(dtoType);
    }

    public String generateSql(R referentialDto) {

        StringBuilder parameters = new StringBuilder();

        addOtherTypeParameter(TopiaEntity.PROPERTY_TOPIA_VERSION, referentialDto.getVersion(), parameters);
        addTimestampParameter(TopiaEntity.PROPERTY_TOPIA_CREATE_DATE, referentialDto.getCreateDate(), parameters);

        if (simplePropertyNames.length > 0) {

            Map<String, Object> simpleProperties = binder.obtainProperties(referentialDto, true, simplePropertyNames);
            for (Map.Entry<String, Object> entry : simpleProperties.entrySet()) {

                String simplePropertyName = entry.getKey();
                String columnName = columnNames.get(simplePropertyName);
                Object simplePropertyValue = entry.getValue();

                if (primitiveBooleanPropertyNames.contains(simplePropertyName)) {
                    addPrimitiveBooleanParameter(columnName, (Boolean) simplePropertyValue, parameters);
                    continue;
                }
                if (primitiveIntegerPropertyNames.contains(simplePropertyName)) {
                    addPrimitiveIntegerParameter(columnName, (Integer) simplePropertyValue, parameters);
                    continue;
                }
                if (primitiveFloatPropertyNames.contains(simplePropertyName)) {
                    addPrimitiveFloatParameter(columnName, (Float) simplePropertyValue, parameters);
                    continue;
                }
                if (primitiveLongPropertyNames.contains(simplePropertyName)) {
                    addPrimitiveLongParameter(columnName, (Long) simplePropertyValue, parameters);
                    continue;
                }

                if (simplePropertyValue == null) {
                    addNullParameter(columnName, parameters);
                    continue;
                }
                if (simplePropertyValue instanceof String) {
                    addStringParameter(columnName, (String) simplePropertyValue, parameters);
                    continue;
                }
                if (simplePropertyValue instanceof Date) {
                    addDateParameter(columnName, (Date) simplePropertyValue, parameters);
                    continue;
                }
                if (simplePropertyValue instanceof Enum) {
                    addEnumParameter(columnName, (Enum) simplePropertyValue, parameters);
                    continue;
                }

                addOtherTypeParameter(columnName, simplePropertyValue, parameters);

            }

        }

        if (manyToOneAssociationNames.length > 0) {

            Map<String, Object> manyToOneAssociations = binder.obtainProperties(referentialDto, true, manyToOneAssociationNames);
            for (Map.Entry<String, Object> entry : manyToOneAssociations.entrySet()) {

                String manyToOneAssociationName = entry.getKey();
                String columnName = columnNames.get(manyToOneAssociationName);
                Object manyToOneAssociationValue = entry.getValue();

                if (manyToOneAssociationValue == null) {
                    addNullParameter(columnName, parameters);
                    continue;
                }
                if (manyToOneAssociationValue instanceof ReferentialDto) {
                    addReferentialDtoParameter(columnName, (ReferentialDto) manyToOneAssociationValue, parameters);
                    continue;
                }
                if (manyToOneAssociationValue instanceof ReferentialReference) {
                    addReferentialReferenceParameter(columnName, (ReferentialReference) manyToOneAssociationValue, parameters);
                }

            }

        }

        StringBuilder result = new StringBuilder();

        String sql = String.format(UPDATE_STATEMENT,
                                   schemaName,
                                   tableName,
                                   parameters.substring(2),
                                   referentialDto.getId());
        result.append(sql);
        if (log.isDebugEnabled()) {
            log.debug("sql: " + sql);
        }

        for (ManyToManyAssociationStruct manyToManyAssociation : manyToManyAssociations) {
            String manyToManyAssociationSql = generateManyToManyAssociationSql(referentialDto, manyToManyAssociation);
            result.append(manyToManyAssociationSql);
        }

        return result.toString();

    }

    private String generateManyToManyAssociationSql(R referentialDto, ManyToManyAssociationStruct manyToManyAssociation) {

        StringBuilder builder = new StringBuilder();

        String referentialDtoId = referentialDto.getId();
        String manyToManyAssociationTableName = manyToManyAssociation.tableName;

        // On commence toujours par supprimer toutes les anciennes associations, elles seront ré-ajoutées juste après
        String deleteSql = String.format(MANY_TO_MANY_ASSOCIATION_DELETE_STATEMENT,
                                         schemaName,
                                         manyToManyAssociationTableName,
                                         tableName,
                                         referentialDtoId);
        builder.append(deleteSql);
        if (log.isDebugEnabled()) {
            log.debug("sql: " + deleteSql);
        }

        Collection<ReferentialReference<?>> manyToManyAssociationValues = binder.obtainSourceProperty(referentialDto, manyToManyAssociation.propertyName);
        if (CollectionUtils.isNotEmpty(manyToManyAssociationValues)) {


            String manyToManyAssociationDbColumnName = manyToManyAssociation.dbColumnName;


            for (ReferentialReference<?> manyToManyAssociationValue : manyToManyAssociationValues) {

                addMnAssociation(
                        manyToManyAssociationTableName,
                        manyToManyAssociationDbColumnName,
                        referentialDtoId,
                        manyToManyAssociationValue.getType(),
                        manyToManyAssociationValue.getId(),
                        builder
                );
            }
        }

        return builder.toString();

    }

    protected <D extends ReferentialDto> void addMnAssociation(String nmAssociationTableName,
                                                               String nmAssociationDbColumnName,
                                                               String referentialDtoId,
                                                               Class<D> associationType,
                                                               String associationId,
                                                               StringBuilder builder) {

        String sql = String.format(MANY_TO_MANY_ASSOCIATION_INSERT_STATEMENT,
                                   schemaName,
                                   nmAssociationTableName,
                                   this.tableName,
                                   nmAssociationDbColumnName,
                                   referentialDtoId,
                                   associationId);
        if (log.isDebugEnabled()) {
            log.debug("sql: " + sql);
        }
        builder.append(sql);

    }

    private Map<String, String> computeColumnNames(TopiaMetadataEntity metadataEntity,
                                                   String[] simplePropertyNames,
                                                   String[] compositionPropertyNames) {
        Map<String, String> columnNames = new TreeMap<>();

        for (String propertyName : simplePropertyNames) {
            columnNames.put(propertyName, metadataEntity.getDbColumnName(propertyName));
        }
        for (String propertyName : compositionPropertyNames) {
            columnNames.put(propertyName, metadataEntity.getDbColumnName(propertyName));
        }
        return columnNames;
    }

    private void addNullParameter(String columnName, StringBuilder parameters) {
        addParameter0(columnName, "NULL", parameters);
    }

    private void addStringParameter(String columnName, String parameter, StringBuilder parameters) {
        addParameter0(columnName, "'" + parameter.replaceAll("'", "''") + "'", parameters);
    }

    private void addDateParameter(String columnName, Date parameter, StringBuilder parameters) {
        addParameter0(columnName, "'" + new Timestamp(parameter.getTime()) + "'", parameters);
    }

    private void addTimestampParameter(String columnName, Date parameter, StringBuilder parameters) {
        addParameter0(columnName, "'" + new Timestamp(parameter.getTime()) + "'::timestamp", parameters);
    }

    private void addEnumParameter(String columnName, Enum parameter, StringBuilder parameters) {
        addParameter0(columnName, "" + parameter.ordinal(), parameters);
    }

    private void addOtherTypeParameter(String columnName, Object parameter, StringBuilder parameters) {
        addParameter0(columnName, "" + parameter, parameters);
    }

    protected void addReferentialReferenceParameter(String columnName, ReferentialReference parameter, StringBuilder parameters) {
        addStringParameter(columnName, parameter.getId(), parameters);
    }

    protected void addReferentialDtoParameter(String columnName, ReferentialDto parameter, StringBuilder parameters) {
        addStringParameter(columnName, parameter.getId(), parameters);
    }

    private void addParameter0(String columnName, String value, StringBuilder parameters) {
        parameters.append(", ").append(columnName).append(" = ").append(value);
    }

    private void addPrimitiveBooleanParameter(String columnName, Boolean parameter, StringBuilder parameters) {
        addParameter0(columnName, "" + (parameter != null && parameter), parameters);
    }

    private void addPrimitiveIntegerParameter(String columnName, Integer parameter, StringBuilder parameters) {
        addParameter0(columnName, "" + (parameter == null ? 0 : parameter), parameters);
    }

    private void addPrimitiveLongParameter(String columnName, Long parameter, StringBuilder parameters) {
        addParameter0(columnName, "" + (parameter == null ? 0 : parameter), parameters);
    }

    private void addPrimitiveFloatParameter(String columnName, Float parameter, StringBuilder parameters) {
        addParameter0(columnName, "" + (parameter == null ? 0f : parameter), parameters);
    }

    /**
     * Pour décrire une association nm.
     */
    private static class ManyToManyAssociationStruct {

        /**
         * Le nom de la propriété dans l'objet.
         */
        private final String propertyName;
        /**
         * Le nom de la colonne de l'association dans la table d'association.
         */
        private final String dbColumnName;
        /**
         * Le nom de la table d'association.
         */
        private final String tableName;

        private ManyToManyAssociationStruct(String propertyName, String dbColumnName, String tableName) {
            this.propertyName = propertyName;
            this.dbColumnName = dbColumnName;
            this.tableName = tableName;
        }

    }
}
