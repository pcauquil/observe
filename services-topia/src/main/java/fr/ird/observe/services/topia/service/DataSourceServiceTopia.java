package fr.ird.observe.services.topia.service;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.ObserveTopiaApplicationContext;
import fr.ird.observe.ObserveTopiaConfiguration;
import fr.ird.observe.ObserveTopiaConfigurationFactory;
import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProvider;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaSupport;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConnectionTopia;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.ObserveDbUserDtos;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseDestroyNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.SqlScriptProducerService;
import fr.ird.observe.services.topia.ObserveJdbcHelper;
import fr.ird.observe.services.topia.ObserveSecurityHelper;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.ObserveTopiaApplicationContextFactory;
import fr.ird.observe.services.topia.binder.BinderEngine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModelVisitor;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * Created on 21/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DataSourceServiceTopia extends ObserveServiceTopia implements DataSourceService {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(DataSourceServiceTopia.class);

    protected static boolean canWrite(Set<?> privileges) {
        return privileges != null &&
                privileges.contains("DELETE") &&
                privileges.contains("UPDATE") &&
                privileges.contains("INSERT");
    }

    protected static boolean canRead(Set<?> privileges) {
        // seul les utilisateurs avec au moins un droit sur les donnes observer peut les lire
        return privileges != null && !privileges.isEmpty();
    }

    @Override
    public ObserveDataSourceInformation checkCanConnect(ObserveDataSourceConfiguration dataSourceConfiguration) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException {
        if (log.isTraceEnabled()) {
            log.trace("checkCanConnect(" + dataSourceConfiguration + ")");
        }

        Preconditions.checkState(dataSourceConfiguration instanceof ObserveDataSourceConfigurationTopiaSupport);
        ObserveDataSourceConfigurationTopiaSupport dataSourceConfigurationTopiaSupport = (ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration;

        ObserveDataSourceInformation dataSourceInformation;

        if (dataSourceConfigurationTopiaSupport.isH2Database()) {

            ObserveDataSourceConfigurationTopiaH2 h2DataSourceConfiguration = (ObserveDataSourceConfigurationTopiaH2) dataSourceConfigurationTopiaSupport;

            // On vérifie que le fichier de la base existe
            File databaseFile = h2DataSourceConfiguration.getDatabaseFile();

            if (!databaseFile.exists()) {

                String message = l(getApplicationLocale(), "observe.services.topia.error.h2.database.notFound");
                throw new DatabaseNotFoundException(message, dataSourceConfiguration);

            }

            // On vérifier que la base n'est pas déjà en cours d'utilisation
            File databaseLockFile = h2DataSourceConfiguration.getLockFile();
            if (databaseLockFile.exists()) {

                String message = l(getApplicationLocale(), "observe.services.topia.error.h2.database.locked");
                if (log.isWarnEnabled()) {
                    log.warn(message);
                }
                // FIXME even if file is lock we still authorize to connect,
                //throw new DatabaseConnexionNotAuthorizedException(message, dataSourceConfiguration);
            }

            // On tente une connection à la base
            ObserveTopiaConfiguration topiaConfiguration = ObserveTopiaConfigurationFactory.forH2Database(h2DataSourceConfiguration.getDirectory(),
                                                                                                          h2DataSourceConfiguration.getDbName(),
                                                                                                          h2DataSourceConfiguration.getUsername(),
                                                                                                          new String(h2DataSourceConfiguration.getPassword()),
                                                                                                          false,
                                                                                                          false,
                                                                                                          false);


            try {
                new JdbcHelper(topiaConfiguration).runSelectOnString("SELECT 1;");
            } catch (Exception e) {

                // Authentification refusée
                String message = l(getApplicationLocale(), "observe.services.topia.error.h2.database.badAuthentication");
                throw new DatabaseConnexionNotAuthorizedException(message, e, dataSourceConfiguration);

            }

            dataSourceInformation = getDataSourceInformation(h2DataSourceConfiguration, topiaConfiguration);


        } else {

            ObserveDataSourceConfigurationTopiaPG pgDataSourceConfiguration = (ObserveDataSourceConfigurationTopiaPG) dataSourceConfigurationTopiaSupport;
            // On tente une connexion au serveur
            ObserveTopiaConfiguration topiaConfiguration = ObserveTopiaConfigurationFactory.forPostgresqlDatabase(pgDataSourceConfiguration.getJdbcUrl(),
                                                                                                                  pgDataSourceConfiguration.getUsername(),
                                                                                                                  new String(pgDataSourceConfiguration.getPassword()),
                                                                                                                  false,
                                                                                                                  false,
                                                                                                                  false);

            try {
                new JdbcHelper(topiaConfiguration).runSelectOnString("SELECT 1;");
            } catch (Exception e) {

                throw new DatabaseConnexionNotAuthorizedException(e.getMessage(), e, dataSourceConfiguration);

            }

            dataSourceInformation = getDataSourceInformation(pgDataSourceConfiguration, topiaConfiguration);

        }

        return dataSourceInformation;

    }

    @Override
    public ObserveDataSourceConnectionTopia create(ObserveDataSourceConfiguration dataSourceConfiguration, DataSourceCreateConfigurationDto dataSourceCreateConfiguration)
            throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException,
            BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        if (log.isTraceEnabled()) {
            log.trace("create(" + dataSourceConfiguration + ", " + dataSourceCreateConfiguration + ")");
        }

        dataSourceCreateConfiguration.validateConfiguration();

        boolean initSchema = !dataSourceCreateConfiguration.isImportDatabase();

        ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.createTopiaApplicationContext((ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration, initSchema);

        if (log.isDebugEnabled()) {
            log.debug("Create topia application context: " + topiaApplicationContext);
        }

        if (dataSourceCreateConfiguration.isImportDatabase()) {

            if (log.isInfoEnabled()) {
                log.info("Create new database from a script.");
            }
            byte[] importDatabase = dataSourceCreateConfiguration.getImportDatabase();

            if (((ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration).isH2Database()) {
                topiaApplicationContext.executeSqlStatements(importDatabase);
                topiaApplicationContext.getMigrationService().createSchemaIfNotExist();
                topiaApplicationContext.getMigrationService().runSchemaMigration();
            } else { // base postgre

                // on realise les import dans un base H2 temporaire
                // FIXME il faut obtenir le repertoit temporaire d'observe plutot que celui du système
                File tmpDir;
                try {
                    tmpDir = Files.createTempDirectory("obstuna").toFile();
                } catch (IOException e) {
                    throw new IllegalStateException("could not create temporary directory ", e);
                }

                ObserveDataSourceConfigurationTopiaH2 temporaryConfiguration = createTemporaryConfiguration(tmpDir, dataSourceConfiguration.getModelVersion());

                ObserveTopiaApplicationContext temporaryTopiaApplicationContext = ObserveTopiaApplicationContextFactory.createTopiaApplicationContext(temporaryConfiguration, false);
                temporaryTopiaApplicationContext.executeSqlStatements(importDatabase);
                temporaryTopiaApplicationContext.getMigrationService().createSchemaIfNotExist();
                temporaryTopiaApplicationContext.getMigrationService().runSchemaMigration();

                SqlScriptProducerService dumpProducerService = serviceContext.newService(temporaryConfiguration, SqlScriptProducerService.class);
                AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forPostgres().addSchema().addReferential().addAllData();
                byte[] dump = dumpProducerService.produceAddSqlScript(request).getSqlCode();
                topiaApplicationContext.executeSqlStatements(dump);
                topiaApplicationContext.getMigrationService().createSchemaIfNotExist();
                topiaApplicationContext.getMigrationService().runSchemaMigration();

                temporaryTopiaApplicationContext.close();
                File databaseFile = temporaryConfiguration.getDatabaseFile();

                if (!databaseFile.delete()) {
                    throw new IllegalStateException("could not delete " + databaseFile);
                }


            }

        } else {

            boolean importReferential = dataSourceCreateConfiguration.isImportReferential();
            boolean importData = dataSourceCreateConfiguration.isImportData();

            // si le referentiel n'est pas importé on aliment la table lasteUpdateDate
            if (!importReferential) {
                topiaApplicationContext.insertLastUpdateDate();
            }


            boolean importStandaloneReferantial = importReferential;
            if (importReferential && importData) {

                // Si on est sur la même source de données pour les deux imports, on fait tout sur la même base temporaire

                ObserveDataSourceConfiguration importReferentialDataSourceConfiguration = dataSourceCreateConfiguration.getImportReferentialDataSourceConfiguration();
                ObserveDataSourceConfiguration importDataSourceConfiguration = dataSourceCreateConfiguration.getImportDataDataSourceConfiguration();

                importStandaloneReferantial = !importReferentialDataSourceConfiguration.equals(importDataSourceConfiguration);

            }

            boolean referantialImported = false;

            if (importStandaloneReferantial) {

                if (log.isInfoEnabled()) {
                    log.info("Import referential.");
                }

                ObserveDataSourceConfiguration importDataSourceConfiguration = dataSourceCreateConfiguration.getImportReferentialDataSourceConfiguration();

                AddSqlScriptProducerRequest request;
                if (((ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration).isH2Database()) {
                    request = AddSqlScriptProducerRequest.forH2();
                } else {
                    request = AddSqlScriptProducerRequest.forPostgres();
                }
                request.addReferential();


                byte[] referentialDump;

                DataSourceService dataSourceService = serviceContext.newService(importDataSourceConfiguration, DataSourceService.class);
                try {
                    ObserveDataSourceConnection importDataSourceConnection = dataSourceService.open(importDataSourceConfiguration);

                    SqlScriptProducerService dumpProducerService = serviceContext.newService(importDataSourceConnection, SqlScriptProducerService.class);
                    referentialDump = dumpProducerService.produceAddSqlScript(request).getSqlCode();

                    topiaApplicationContext.executeSqlStatements(referentialDump);
                } finally {

                    if (!dataSourceCreateConfiguration.isLeaveOpenSource()) {
                        dataSourceService.close();
                    }

                }

                referantialImported = true;

            }


            if (importData) {

                ObserveDataSourceConfiguration importDataSourceConfiguration = dataSourceCreateConfiguration.getImportDataDataSourceConfiguration();


                AddSqlScriptProducerRequest request;
                if (((ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration).isH2Database()) {
                    request = AddSqlScriptProducerRequest.forH2();
                } else {
                    request = AddSqlScriptProducerRequest.forPostgres();
                }

                if (!referantialImported) {
                    if (log.isInfoEnabled()) {
                        log.info("Get referential.");
                    }

                    request.addReferential();
                }

                // Récupération du dump qui contient les données
                ImmutableSet<String> importDataIds = dataSourceCreateConfiguration.getImportDataIds();

                if (log.isInfoEnabled()) {
                    log.info("Get data: " + importDataIds);
                }

                request.dataIdsToAdd(importDataIds);


                byte[] dataDump;
                DataSourceService dataSourceService = serviceContext.newService(importDataSourceConfiguration, DataSourceService.class);

                try {

                    ObserveDataSourceConnection importDataSourceConnection = dataSourceService.open(importDataSourceConfiguration);
                    SqlScriptProducerService dumpProducerService = serviceContext.newService(importDataSourceConnection, SqlScriptProducerService.class);
                    dataDump = dumpProducerService.produceAddSqlScript(request).getSqlCode();

                } finally {
                    if (!dataSourceCreateConfiguration.isLeaveOpenSource()) {
                        dataSourceService.close();
                    }
                }


                if (log.isInfoEnabled()) {
                    log.info("Import data" + (request.isAddReferential() ? " and referential." : "."));
                }

                topiaApplicationContext.executeSqlStatements(dataDump);

            }

        }

        ObserveDataSourceInformation dataSourceInformation = getDataSourceInformation((ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration, topiaApplicationContext.getConfiguration());

        return createDataSourceConnection(dataSourceInformation, topiaApplicationContext.getAuthenticationToken());

    }

    protected ObserveDataSourceConfigurationTopiaH2 createTemporaryConfiguration(File tmpDirectory, Version version) {

        File dbDirectory = new File(tmpDirectory, "obstuna" + UUID.randomUUID().toString());

        ObserveDataSourceConfigurationTopiaH2 configuration = new ObserveDataSourceConfigurationTopiaH2();
        configuration.setLabel("obtunaTmp");
        configuration.setDbName("obstuna");
        configuration.setUsername("sa");
        configuration.setPassword("sa".toCharArray());
        configuration.setDirectory(dbDirectory);
        configuration.setShowMigrationProgression(true);
        configuration.setShowMigrationSql(true);
        configuration.setModelVersion(version);

        return configuration;
    }

    @Override
    public ObserveDataSourceConnectionTopia open(ObserveDataSourceConfiguration dataSourceConfiguration) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {
        if (log.isTraceEnabled()) {
            log.trace("open(" + dataSourceConfiguration + ")");
        }

        ObserveDataSourceInformation dataSourceInformation = checkCanConnect(dataSourceConfiguration);

        Version dbVersion = dataSourceInformation.getVersion();
        Version requestVersion = dataSourceConfiguration.getModelVersion();

        if (!dataSourceConfiguration.isAutoMigrate() && !dbVersion.equals(requestVersion)) {

            String message = l(getApplicationLocale(), "observe.services.topia.error.database.badModelVersion", requestVersion, dbVersion);

            throw new BabModelVersionException(message, requestVersion, dbVersion);

        }

        ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext((ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration);
        return createDataSourceConnection(dataSourceInformation, topiaApplicationContext.getAuthenticationToken());

    }

    @Override
    public void close() {
        if (log.isTraceEnabled()) {
            log.trace("close()");
        }

        ObserveDataSourceConfigurationTopiaSupport dataSourceConfiguration = serviceContext.getDataSourceConfiguration();
        Optional<ObserveTopiaApplicationContext> optionalTopiaApplicationContext = ObserveTopiaApplicationContextFactory.getTopiaApplicationContextIfPresent(dataSourceConfiguration);

        if (optionalTopiaApplicationContext.isPresent()) {

            ObserveTopiaApplicationContext topiaApplicationContext = optionalTopiaApplicationContext.get();
            if (log.isInfoEnabled()) {
                log.info("Closing topia application context: " + dataSourceConfiguration);
            }
            topiaApplicationContext.close();

        }

    }

    @Override
    public void destroy() throws DatabaseDestroyNotAuthorizedException {
        if (log.isTraceEnabled()) {
            log.trace("destroy()");
        }

        ObserveDataSourceConfigurationTopiaSupport dataSourceConfiguration = serviceContext.getDataSourceConfiguration();

        if (!dataSourceConfiguration.isH2Database()) {

            throw new DatabaseDestroyNotAuthorizedException(dataSourceConfiguration);

        }

        Optional<ObserveTopiaApplicationContext> optionalTopiaApplicationContext = ObserveTopiaApplicationContextFactory.getTopiaApplicationContextIfPresent(dataSourceConfiguration);

        if (optionalTopiaApplicationContext.isPresent()) {

            ObserveTopiaApplicationContext topiaApplicationContext = optionalTopiaApplicationContext.get();
            if (log.isInfoEnabled()) {
                log.info("Closing topia application context: " + dataSourceConfiguration);
            }
            topiaApplicationContext.close();

        }

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfigurationH2 = (ObserveDataSourceConfigurationTopiaH2) dataSourceConfiguration;

        File databaseFile = dataSourceConfigurationH2.getDatabaseFile();

        if (!databaseFile.delete()) {
            throw new IllegalStateException("could not delete " + databaseFile);
        }

    }

    @Override
    public void backup(File backupFile) {

        if (!serviceContext.getTopiaApplicationContext().getConfiguration().isH2Configuration()) {

            throw new IllegalStateException("Cant backup a none H2 database.");
        }

        getTopiaPersistenceContext().getSqlSupport().executeSql("SCRIPT NOPASSWORDS NOSETTINGS BLOCKSIZE 2048 TO '" + backupFile.getAbsolutePath() + "' COMPRESSION GZIP CHARSET 'UTF-8';");

    }

    @Override
    public Set<ObserveDbUserDto> getUsers(ObserveDataSourceConfiguration dataSourceConfiguration) {
        if (log.isTraceEnabled()) {
            log.trace("getUsers(" + dataSourceConfiguration + ")");
        }

        Set<ObserveDbUserDto> users = Sets.newHashSet();

        // pas d'user pour les bases autres que postgresql
        if (dataSourceConfiguration instanceof ObserveDataSourceConfigurationTopiaPG) {

            ObserveDataSourceConfigurationTopiaPG sourceConfiguration = (ObserveDataSourceConfigurationTopiaPG) dataSourceConfiguration;

            ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext(sourceConfiguration);

            ObserveTopiaConfiguration topiaConfiguration = topiaApplicationContext.getConfiguration();

            ObserveJdbcHelper observeJdbcHelper = new ObserveJdbcHelper(topiaConfiguration);

            users.addAll(observeJdbcHelper.getUsers());

            topiaApplicationContext.close();

        }

        return users;
    }

    @Override
    public void applySecurity(ObserveDataSourceConfiguration dataSourceConfiguration, Set<ObserveDbUserDto> users) {
        if (log.isTraceEnabled()) {
            log.trace("applySecurity(" + dataSourceConfiguration + ", [" + Joiner.on(", ").join(users.stream().map(ObserveDbUserDtos.NAME_FUNCTION).collect(Collectors.toList())) + "])");
        }

        // pas de securité  pour les bases autres que postgresql
        if (dataSourceConfiguration instanceof ObserveDataSourceConfigurationTopiaPG) {

            ObserveDataSourceConfigurationTopiaPG sourceConfiguration = (ObserveDataSourceConfigurationTopiaPG) dataSourceConfiguration;

            ObserveTopiaApplicationContext optionalTopiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext(sourceConfiguration);

            ObserveTopiaConfiguration topiaConfiguration = optionalTopiaApplicationContext.getConfiguration();

            ObserveSecurityHelper securityHelper = new ObserveSecurityHelper(topiaConfiguration);

            securityHelper.applySecurity(users, sourceConfiguration.isShowMigrationSql());

        }

    }

    @Override
    public void migrateData(ObserveDataSourceConfiguration dataSourceConfiguration) {
        if (log.isTraceEnabled()) {
            log.trace("migrateData(" + dataSourceConfiguration + ")");
        }

        ObserveTopiaApplicationContext topiaApplicationContext = ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext((ObserveDataSourceConfigurationTopiaSupport) dataSourceConfiguration);

        topiaApplicationContext.getMigrationService().runSchemaMigration();

    }

    @Override
    public Set<Class<? extends ReferentialDto>> getReferentialTypesInShell() {
        ObserveTopiaApplicationContext topiaApplicationContext = serviceContext.getTopiaApplicationContext();
        TopiaMetadataModel metadataModel = topiaApplicationContext.getMetadataModel();

        DetectReferentialTypesInShellBuilder visitor = new DetectReferentialTypesInShellBuilder();
        metadataModel.accept(visitor);
        return visitor.build();
    }

    protected ObserveDataSourceConnectionTopia createDataSourceConnection(ObserveDataSourceInformation dataSourceInformation, String authenticationToken) {

        return new ObserveDataSourceConnectionTopia(
                authenticationToken,
                dataSourceInformation.canReadReferential(),
                dataSourceInformation.canWriteReferential(),
                dataSourceInformation.canReadData(),
                dataSourceInformation.canWriteData(),
                dataSourceInformation.getVersion());
    }

    protected ObserveDataSourceInformation getDataSourceInformation(ObserveDataSourceConfigurationTopiaSupport dataSourceConfiguration, ObserveTopiaConfiguration topiaConfiguration) {

        boolean readReferential;
        boolean writeReferential;
        boolean readData;
        boolean writeData;

        Version version;

        ObserveJdbcHelper observeJdbcHelper = new ObserveJdbcHelper(topiaConfiguration);

        version = observeJdbcHelper.getVersion();

        if (dataSourceConfiguration.isH2Database()) {

            // Sur une base H2, on a le droit de tout lire, mais uniquement d'écrire les données
            readReferential = true;
            writeReferential = false;
            readData = true;
            writeData = true;

        } else {

            // on recherche les droits de l'utilisateur sur cette base

            //TODO chemit 2010-10-28 : il vaudrait mieux utiliser des fonctions postgres adéquates qui elle sont fiables...
            //FIXME la recuperation des meta-donnees n'est pas fiable!
            //FIXME en effet, sur un simple lecteur, on voit apparaître aussi
            //FIXME des privileges INSERT ou UPDATE...

            // on pourrait utiliser une requete specifique postgres :

            // select count(*) from information_schema.table_privileges where
            // grantee='ROLE' and table_name='maree' and
            // privilege_type='INSERT';

            // cependant cela n'est pas mieux car la requete peut ne pas etre
            // extacte si l'utilisateur n'a pas les bons droits....

            // la meilleure solution serait je pense de poser 2 fonctions
            // stockées dans pg canWriteData, canWrite pour etre sur du resultat

            // recherche des droits sur les données observers
            Set<String> dataPrivileges = observeJdbcHelper.getTablePrivileges(ObserveSecurityHelper.OBSERVE_SEINE_SCHEMA_NAME, "trip");

            readData = canRead(dataPrivileges);
            writeData = canWrite(dataPrivileges);


            // recherche des droits sur le referentiel
            Set<String> referentielPrivileges = observeJdbcHelper.getTablePrivileges(ObserveSecurityHelper.OBSERVE_COMMON_SCHEMA_NAME, "vessel");

            // Sur une base PG, on regarde en base ce que l'utilisateur peut lire/écrire
            readReferential = true;
            writeReferential = canWrite(referentielPrivileges);

        }

        if (log.isDebugEnabled()) {
            log.debug("User can read refererential : " + readReferential + ", " +
                              "write referential : " + writeReferential + ", " +
                              "read data : " + readData + ", " +
                              "write data : " + writeData + ".");
        }

        ObserveMigrationConfigurationProvider observeMigrationConfigurationProvider = ObserveMigrationConfigurationProvider.get();
        return new ObserveDataSourceInformation(
                readReferential,
                writeReferential,
                readData,
                writeData,
                observeMigrationConfigurationProvider.getMinimumVersion(),
                version,
                observeMigrationConfigurationProvider.getVersionsAfter(version));
    }

    private static class DetectReferentialTypesInShellBuilder implements TopiaMetadataModelVisitor {

        private final ImmutableSet.Builder<Class<? extends ReferentialDto>> typesInShellBuilder = ImmutableSet.builder();

        @Override
        public void visitModelStart(TopiaMetadataModel metadataModel) {

        }

        @Override
        public void visitModelEnd(TopiaMetadataModel metadataModel) {

        }

        @Override
        public void visitEntiyStart(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {

        }

        @Override
        public void visitEntiyEnd(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity) {

        }

        @Override
        public void visitReversedAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

        }

        @Override
        public void visitOneToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            addTypeInShell(metadataEntity, propertyType);
        }

        @Override
        public void visitManyToManyAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            addTypeInShell(metadataEntity, propertyType);
        }

        @Override
        public void visitManyToOneAssociation(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {
            addTypeInShell(metadataEntity, propertyType);
        }

        @Override
        public void visitOneToManyAssociationInverse(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, TopiaMetadataEntity propertyType) {

        }

        @Override
        public void visitProperty(TopiaMetadataModel metadataModel, TopiaMetadataEntity metadataEntity, String propertyName, String propertyType) {

        }

        private void addTypeInShell(TopiaMetadataEntity containerType, TopiaMetadataEntity propertyType) {

            ObserveEntityEnum parentObserveEntityEnum = ObserveEntityEnum.valueOf(containerType.getType());
            Class parentEntityType = parentObserveEntityEnum.getContract();
            if (ObserveReferentialEntity.class.isAssignableFrom(parentEntityType)) {

                String type = propertyType.getType();

                ObserveEntityEnum observeEntityEnum = ObserveEntityEnum.valueOf(type);
                Class<? extends ReferentialDto> dtoType = BinderEngine.get().getReferentialDtoType(observeEntityEnum);
                typesInShellBuilder.add(dtoType);
                if (log.isInfoEnabled()) {
                    log.info("For container type:" + parentObserveEntityEnum + ", add to shell: " + observeEntityEnum);
                }
            }

        }

        public Set<Class<? extends ReferentialDto>> build() {
            return typesInShellBuilder.build();
        }
    }
}
