package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.Species2;

import java.util.Map;
import java.util.function.Predicate;

/**
 * Extraction des list d'espèces definit dans le référentiel "liste d'èspéce" et présent dans un océan
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SpeciesListOceanEntitiesExtractor extends SpeciesListEntitiesExtractor {

    public SpeciesListOceanEntitiesExtractor(String speciesListId) {
        super(speciesListId);
    }

    @Override
    protected Predicate<Species> getFilter(Map<String, Object> requestContext) {

        Predicate<Species> filter = super.getFilter(requestContext);

        Ocean ocean = (Ocean) requestContext.get(Species.PROPERTY_OCEAN);

        if (ocean != null) {

            filter = filter.and(Species2.newSpeciesByOceanPredicate(ocean));

        }

        return filter;
    }
}
