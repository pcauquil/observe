package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.seine.WeightCategory;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class WeightCategoryBinder extends ReferentialBinderSupport<WeightCategory, WeightCategoryDto> {

    public WeightCategoryBinder() {
        super(WeightCategory.class, WeightCategoryDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, WeightCategoryDto dto, WeightCategory entity) {


        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, WeightCategory entity, WeightCategoryDto dto) {


        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));

    }

    @Override
    public ReferentialReference<WeightCategoryDto> toReferentialReference(ReferentialLocale referentialLocale, WeightCategory entity) {

        return toReferentialReference(entity,
                                      getLabel(referentialLocale, entity),
                                      entity.getCode(),
                                      entity.getSpecies().getTopiaId());

    }

    @Override
    public ReferentialReference<WeightCategoryDto> toReferentialReference(ReferentialLocale referentialLocale, WeightCategoryDto dto) {

        return toReferentialReference(dto,
                                      getLabel(referentialLocale, dto),
                                      dto.getCode(),
                                      dto.getSpecies().getId());

    }
}
