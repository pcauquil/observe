package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.SpeciesList;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SpeciesListBinder extends ReferentialBinderSupport<SpeciesList, SpeciesListDto> {

    public SpeciesListBinder() {
        super(SpeciesList.class, SpeciesListDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SpeciesListDto dto, SpeciesList entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);
        entity.setSpecies(toEntitySet(dto.getSpecies(), Species.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SpeciesList entity, SpeciesListDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);
        dto.setSpecies(toReferentialReferenceList(referentialLocale, entity.getSpecies(), SpeciesDto.class));

    }

    @Override
    public ReferentialReference<SpeciesListDto> toReferentialReference(ReferentialLocale referentialLocale, SpeciesList entity) {

        return toReferentialReference(entity,
                                      entity.getCode(), getLabel(referentialLocale, entity));

    }

    @Override
    public ReferentialReference<SpeciesListDto> toReferentialReference(ReferentialLocale referentialLocale, SpeciesListDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(), getLabel(referentialLocale, dto));

    }
}
