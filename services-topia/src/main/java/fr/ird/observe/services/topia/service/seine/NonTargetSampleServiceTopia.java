package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.entities.seine.NonTargetSample;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.services.service.seine.NonTargetSampleService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class NonTargetSampleServiceTopia extends ObserveServiceTopia implements NonTargetSampleService {

    private static final Log log = LogFactory.getLog(NonTargetSampleServiceTopia.class);

    @Override
    public boolean canUseNonTargetSample(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("canUseNonTargetSample(" + setSeineId + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        return setSeine.canUseNonTargetSample();
    }

    protected NonTargetSample getNonTargetSample(SetSeine setSeine) {

        NonTargetSample nonTargetSample;

        if (setSeine.isNonTargetSampleEmpty()) {

            nonTargetSample = newEntity(NonTargetSample.class);

        } else {

            nonTargetSample = Iterables.get(setSeine.getNonTargetSample(), 0);
        }

        return nonTargetSample;


    }

    @Override
    public Form<NonTargetSampleDto> loadForm(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setSeineId + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        NonTargetSample nonTargetSample = getNonTargetSample(setSeine);

        return dataEntityToForm(
                NonTargetSampleDto.class,
                nonTargetSample,
                ReferenceSetRequestDefinitions.NON_TARGET_SAMPLE_FORM);
    }

    @Override
    public Collection<ReferentialReference<SpeciesDto>> getSampleSpecies(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("getSampleSpecies(" + setSeineId + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        ReferentialBinderSupport<ObserveReferentialEntity, SpeciesDto> speciesBinder = getReferentialBinder(SpeciesDto.class);

        ReferentialLocale referentialLocale = getReferentialLocale();

        // on recupere la liste des espèces thon cible

        return setSeine.getNonTargetCatch().stream()
                       .map(NonTargetCatch::getSpecies)
                       .distinct()
                       .map(s -> speciesBinder.toReferentialReference(referentialLocale, s))
                       .collect(Collectors.toList());
    }

    @Override
    public SaveResultDto save(String setSeineId, NonTargetSampleDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + setSeineId + ", " + dto.getId() + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineDto.class, setSeineId);

        NonTargetSample entity = loadOrCreateEntityFromDataDto(dto);

        checkLastUpdateDate(entity, dto);

        copyDataDtoToEntity(dto, entity);

        if (dto.isNotPersisted()) {

            setSeine.addNonTargetSample(entity);

        }

        return saveEntity(setSeine, entity);

    }
}
