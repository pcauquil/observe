package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.SchoolEstimate;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SchoolEstimateBinder extends DataBinderSupport<SchoolEstimate, SchoolEstimateDto> {

    public SchoolEstimateBinder() {
        super(SchoolEstimate.class, SchoolEstimateDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SchoolEstimateDto dto, SchoolEstimate entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setTotalWeight(dto.getTotalWeight());
        entity.setMeanWeight(dto.getMeanWeight());
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SchoolEstimate entity, SchoolEstimateDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setTotalWeight(entity.getTotalWeight());
        dto.setMeanWeight(entity.getMeanWeight());
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));

    }

    @Override
    public DataReference<SchoolEstimateDto> toDataReference(ReferentialLocale referentialLocale, SchoolEstimate entity) {

        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getSpecies()),
                               entity.getMeanWeight(),
                               entity.getTotalWeight());

    }

    @Override
    public DataReference<SchoolEstimateDto> toDataReference(ReferentialLocale referentialLocale, SchoolEstimateDto dto) {

        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getSpecies()),
                               dto.getMeanWeight(),
                               dto.getTotalWeight());

    }
}
