package fr.ird.observe.services.topia.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveTopiaDaoSupplier;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.LengthWeightComputable;
import fr.ird.observe.entities.constants.seine.NonTargetCatchComputedValueSourcePersist;
import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import fr.ird.observe.entities.referentiel.LengthWeightParameter;
import fr.ird.observe.entities.referentiel.LengthWeightParameterNotFoundException;
import fr.ird.observe.entities.referentiel.LengthWeightParameters;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.entities.seine.NonTargetCatches;
import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.entities.seine.NonTargetSample;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.entities.seine.SetSeines;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.entities.seine.TargetSample;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.entities.seine.TripSeines;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateActivitySeineDataResult;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataRequest;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataResult;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.BeanMonitor;

import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ConsolidateDataServiceTopia extends ObserveServiceTopia implements ConsolidateDataService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ConsolidateDataServiceTopia.class);

    @Override
    public ImmutableSet<ConsolidateTripSeineDataResult> consolidateTripSeines(ConsolidateTripSeineDataRequest consolidateTripSeineDataRequest) {

        ReferentialLocale referenceLocale = serviceContext.getReferentialLocale();
        boolean failIfLenghtWeightParameterNotFound = consolidateTripSeineDataRequest.isFailIfLenghtWeightParameterNotFound();
        ImmutableSet.Builder<ConsolidateTripSeineDataResult> resultBuilder = new ImmutableSet.Builder<>();
        for (String tripSeineId : consolidateTripSeineDataRequest.getTripSeineIds()) {

            Optional<ConsolidateTripSeineDataResult> consolidateTripSeineDataResult = consolidateTripSeine(referenceLocale, tripSeineId, failIfLenghtWeightParameterNotFound);

            if (consolidateTripSeineDataResult.isPresent()) {
                resultBuilder.add(consolidateTripSeineDataResult.get());
            }

        }
        return resultBuilder.build();

    }

    protected Optional<ConsolidateTripSeineDataResult> consolidateTripSeine(ReferentialLocale referenceLocale, String tripSeineId, boolean failIfLenghtWeightParameterNotFound) {

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        BeanMonitor targetLengthMonitor = new BeanMonitor(
                TargetLength.PROPERTY_LENGTH,
                TargetLength.PROPERTY_LENGTH_SOURCE,
                TargetLength.PROPERTY_WEIGHT,
                TargetLength.PROPERTY_WEIGHT_SOURCE);

        BeanMonitor nonTargetSampleMonitor = new BeanMonitor(
                NonTargetLength.PROPERTY_LENGTH,
                NonTargetLength.PROPERTY_LENGTH_SOURCE,
                NonTargetLength.PROPERTY_WEIGHT,
                NonTargetLength.PROPERTY_WEIGHT_SOURCE);

        BeanMonitor nonTargetCatchMonitor = new BeanMonitor(
                NonTargetCatch.PROPERTY_MEAN_LENGTH,
                NonTargetCatch.PROPERTY_MEAN_LENGTH_COMPUTED_SOURCE,
                NonTargetCatch.PROPERTY_MEAN_WEIGHT,
                NonTargetCatch.PROPERTY_MEAN_WEIGHT_COMPUTED_SOURCE,
                NonTargetCatch.PROPERTY_CATCH_WEIGHT,
                NonTargetCatch.PROPERTY_CATCH_WEIGHT_COMPUTED_SOURCE,
                NonTargetCatch.PROPERTY_TOTAL_COUNT,
                NonTargetCatch.PROPERTY_TOTAL_COUNT_COMPUTED_SOURCE
        );

        Locale applicationLocale = serviceContext.getApplicationLocale();

        ObserveTopiaPersistenceContext persistenceContext = getTopiaPersistenceContext();

        ImmutableSet.Builder<ConsolidateActivitySeineDataResult> actititiesResultBuilder = new ImmutableSet.Builder<>();

        for (Route route : tripSeine.getRoute()) {

            for (ActivitySeine activitySeine : route.getActivitySeine()) {

                if (!activitySeine.isSetOperation()) {
                    if (log.isDebugEnabled()) {
                        log.debug("No set on activity " + activitySeine);
                    }
                    continue;
                }

                ConsolidateActivitySeineDataResultBuilder resultBuilder = ConsolidateActivitySeineDataResultBuilder.create(applicationLocale, referenceLocale, activitySeine);

                ConsolidationActivitySeineDataContext activityContext = new ConsolidationActivitySeineDataContext();
                activityContext.locale = applicationLocale;
                activityContext.failIfLenghtWeightParameterNotFound = failIfLenghtWeightParameterNotFound;
                activityContext.daoSupplier = persistenceContext;
                activityContext.resultBuilder = resultBuilder;
                activityContext.tripSeine = tripSeine;
                activityContext.route = route;
                activityContext.activity = activitySeine;
                activityContext.targetLengthMonitor = targetLengthMonitor;
                activityContext.nonTargetLengthMonitor = nonTargetSampleMonitor;
                activityContext.nonTargetCatchMonitor = nonTargetCatchMonitor;

                consolidateActivitySeine(activityContext);

                Optional<ConsolidateActivitySeineDataResult> optionalConsolidateActivitySeineDataResult = resultBuilder.build();
                if (optionalConsolidateActivitySeineDataResult.isPresent()) {

                    // Des modifications ont été enregistrées sur l'activité
                    ConsolidateActivitySeineDataResult consolidateActivitySeineDataResult = optionalConsolidateActivitySeineDataResult.get();
                    if (log.isInfoEnabled()) {
                        log.info("Found some modifications on activity: " + consolidateActivitySeineDataResult.getActivitySeineLabel());
                    }
                    actititiesResultBuilder.add(consolidateActivitySeineDataResult);

                }

            }

        }

        ImmutableSet<ConsolidateActivitySeineDataResult> consolidateActivitySeineDataResults = actititiesResultBuilder.build();

        ConsolidateTripSeineDataResult consolidateTripSeineDataResult;

        if (consolidateActivitySeineDataResults.isEmpty()) {
            consolidateTripSeineDataResult = null;
        } else {

            consolidateTripSeineDataResult = new ConsolidateTripSeineDataResult(tripSeineId, TripSeines.decorate(referenceLocale.ordinal(), tripSeine), consolidateActivitySeineDataResults);

            if (log.isInfoEnabled()) {
                log.info("Found some modifications on trip: " + consolidateTripSeineDataResult.getTripSeineLabel());
            }
        }
        return Optional.ofNullable(consolidateTripSeineDataResult);

    }

    protected void consolidateActivitySeine(ConsolidationActivitySeineDataContext activityContext) {

        ActivitySeine activity = activityContext.activity;

        if (log.isDebugEnabled()) {
            log.debug("Start consolidate activity: " + activity.getTopiaId());
        }

        SetSeine setSeine = activity.getSetSeine();

        if (setSeine == null) {
            if (log.isInfoEnabled()) {
                log.info("No set found for activity: " + activity.getTopiaId());
            }
            return;
        }
        SchoolTypePersist oldSchoolType = setSeine.getSchoolType();
        SchoolTypePersist newSchoolType = activity.getSchoolType();
        if (oldSchoolType == null || oldSchoolType != newSchoolType) {

            // le type de banc a changé, on doit sauver l'activité
            setSeine.setSchoolType(newSchoolType);
            activityContext.setSchoolTypeChanged(oldSchoolType, newSchoolType);

        }

        if (!setSeine.isTargetSampleEmpty()) {

            // des echantillons thons trouves
            for (TargetSample targetSample : setSeine.getTargetSample()) {
                if (!targetSample.isTargetLengthEmpty()) {
                    for (TargetLength targetLength : targetSample.getTargetLength()) {

                        activityContext.watchTargetLength(targetLength);

                        updateLengthWeightAble(activityContext,
                                               targetLength.getSpecies(),
                                               null, /* pas de sexe precise */
                                               targetLength);

                        activityContext.flushTargetLength();

                    }
                }
            }
        }

        if (!setSeine.isNonTargetSampleEmpty()) {

            // des echantillons faunes trouves
            for (NonTargetSample nonTargetSample : setSeine.getNonTargetSample()) {

                if (!nonTargetSample.isNonTargetLengthEmpty()) {
                    for (NonTargetLength nonTargetLength : nonTargetSample.getNonTargetLength()) {

                        activityContext.watchNonTargetLenght(nonTargetLength);

                        updateLengthWeightAble(activityContext,
                                               nonTargetLength.getSpecies(),
                                               nonTargetLength.getSex(),
                                               nonTargetLength);

                        activityContext.flushNonTargetLength();

                    }
                }
            }
        }

        if (!setSeine.isNonTargetCatchEmpty()) {

            // des captures (ou rejets) faunes trouves
            for (NonTargetCatch nonTargetCatch : setSeine.getNonTargetCatch()) {

                activityContext.watchNonTargetCatch(nonTargetCatch);

                updateNonTargetCatch(activityContext, nonTargetCatch);

                activityContext.flushNonTargetCatch();

            }
        }

    }

    protected void updateNonTargetCatch(ConsolidationActivitySeineDataContext activityContext, NonTargetCatch nonTargetCatch) {

        Species species = nonTargetCatch.getSpecies();

        // récupération du référentiel
        LengthWeightParameter lengthWeightParameter = activityContext.findLengthWeightParameter(species, null /* pas de sexe spécifié*/);

        // -- Cas n°1 (calcul uniquement à partir des relations taille - poids)
        updateNonTargetCatchByLengthWeightRelation(nonTargetCatch, lengthWeightParameter);

        if (NonTargetCatches.allNonTargetCatchDataFilled(nonTargetCatch)) {

            // tout est rempli, plus rien à faire
            return;
        }

        // répération des échantillon de cette espèce sur les calée

        Collection<NonTargetLength> nonTargetLengths = SetSeines.getNonTargetLengths(activityContext.getSetSeine(), species);

        if (nonTargetCatch.getCatchWeight() != null || nonTargetCatch.getTotalCount() != null) {

            // -- Cas n°2 (pas de taille / poids moyen mais au moins un des deux taille / poids)
            computeNonTargetCatchMeanLength(nonTargetCatch, nonTargetLengths, lengthWeightParameter);
        }

        if (NonTargetCatches.allNonTargetCatchDataFilled(nonTargetCatch)) {

            // tout est rempli, plus rien à faire
            return;
        }

        // -- Cas n°3 (pas de nombre estimé)

        if (nonTargetCatch.getTotalCount() == null) {

            computeNonTargetCatchNombreEstime(nonTargetCatch, nonTargetLengths, lengthWeightParameter);

        }

        if (NonTargetCatches.allNonTargetCatchDataFilled(nonTargetCatch)) {

            // tout est rempli, plus rien à faire
            return;
        }

        // -- Cas n°4 (pas de poids moyen, taille moyenne)

        computeNonTargetCatchMeanValues(nonTargetCatch, lengthWeightParameter);
    }

    protected void updateNonTargetCatchByLengthWeightRelation(NonTargetCatch nonTargetCatch,
                                                              LengthWeightParameter lengthWeightParameter) {

        // calcul via le paramétrage taille - poids
        updateLengthWeightAble(nonTargetCatch, lengthWeightParameter);

        // calcule l'un des trois champs poids estimé - nbEstime - poids moyen
        updateNonTargetCatchPoidsEstimeNbEstimePoidsMoyen(nonTargetCatch);

        // on ressaye d'appliquer la relation taille - poids au cas où une des
        // trois valeurs précédentes a été calculée, on pourrait peut-être
        // ainsi en déduire via le paramétrage la taille moyenne
        updateLengthWeightAble(nonTargetCatch, lengthWeightParameter);

    }

    protected void computeNonTargetCatchMeanLength(NonTargetCatch nonTargetCatch,
                                                   Collection<NonTargetLength> samples,
                                                   LengthWeightParameter lengthWeightParameter) {

        Float meanLength = nonTargetCatch.getMeanLength();

        if (meanLength == null) {

            // on essaye de calculer la taille moyenne à partir des échantillons

            NonTargetCatchComputedValueSourcePersist computedSource = null;

            if (CollectionUtils.isNotEmpty(samples)) {

                // on calcul la taille moyenne à partir des échantillons
                float totalLength = 0f;
                int totalCount = 0;
                for (NonTargetLength sample : samples) {

                    Integer count = sample.getCount();
                    Float length = sample.getLength();

                    if (count != null && length != null) {
                        totalCount += count;
                        totalLength += length * count;
                    }
                }

                if (totalCount != 0) {

                    meanLength = totalLength / totalCount;

                    computedSource = NonTargetCatchComputedValueSourcePersist.fromSample;
                }
            }

            if (meanLength == null && lengthWeightParameter != null) {

                // on prend directement la valeur fournie par le référentiel

                meanLength = lengthWeightParameter.getMeanLength();
                computedSource = NonTargetCatchComputedValueSourcePersist.fromReferentiel;
            }

            if (meanLength != null) {

                // la taille moyenne a pu etre calculee, on la pousse alors
                nonTargetCatch.setMeanLength(meanLength);
                nonTargetCatch.setMeanLengthComputedSource(computedSource);

            }

        }

        if (meanLength != null) {

            // on peut aussi relancer la calcul du cas n°1
            updateNonTargetCatchByLengthWeightRelation(nonTargetCatch, lengthWeightParameter);

        }

    }

    protected void computeNonTargetCatchNombreEstime(NonTargetCatch nonTargetCatch,
                                                     Collection<NonTargetLength> samples,
                                                     LengthWeightParameter lengthWeightParameter) {


        Integer totalCount = nonTargetCatch.getTotalCount();

        if (totalCount == null) {

            if (CollectionUtils.isNotEmpty(samples)) {

                // on calcul la nombre d'individus à partir des échantillons
                totalCount = 0;
                for (NonTargetLength sample : samples) {

                    Integer count = sample.getCount();

                    if (count != null) {
                        totalCount += count;
                    }
                }

                if (totalCount != 0) {

                    nonTargetCatch.setTotalCount(totalCount);
                    nonTargetCatch.setTotalCountComputedSource(NonTargetCatchComputedValueSourcePersist.fromSample);

                }
            }

        }

        if (nonTargetCatch.getMeanWeight() != null || nonTargetCatch.getMeanLength() != null) {

            // on peut aussi relancer la calcul du cas n°1
            updateNonTargetCatchByLengthWeightRelation(nonTargetCatch, lengthWeightParameter);
        }

    }

    protected void computeNonTargetCatchMeanValues(NonTargetCatch nonTargetCatch, LengthWeightParameter lengthWeightParameter) {

        if (lengthWeightParameter != null) {

            if (nonTargetCatch.getMeanLength() == null) {

                nonTargetCatch.setMeanLength(lengthWeightParameter.getMeanLength());
                nonTargetCatch.setMeanLengthComputedSource(NonTargetCatchComputedValueSourcePersist.fromReferentiel);

            }

            if (nonTargetCatch.getMeanWeight() == null) {

                nonTargetCatch.setMeanWeight(lengthWeightParameter.getMeanWeight());
                nonTargetCatch.setMeanWeightComputedSource(NonTargetCatchComputedValueSourcePersist.fromReferentiel);

            }

            if (nonTargetCatch.getMeanWeight() != null || nonTargetCatch.getMeanLength() != null) {

                // on peut aussi relancer la calcul du cas n°1
                updateNonTargetCatchByLengthWeightRelation(nonTargetCatch, lengthWeightParameter);

            }

        }
    }

    protected void updateNonTargetCatchPoidsEstimeNbEstimePoidsMoyen(NonTargetCatch nonTargetCatch) {

        Float meanWeight = nonTargetCatch.getMeanWeight();
        Float catchWeight = nonTargetCatch.getCatchWeight();
        Integer totalCount = nonTargetCatch.getTotalCount();

        if (catchWeight == null && totalCount != null && meanWeight != null) {

            // calcul le weight poids à partir de nb estime et du poids moyen
            catchWeight = meanWeight * (float) totalCount / 1000;
            nonTargetCatch.setCatchWeight(catchWeight);
            nonTargetCatch.setCatchWeightComputedSource(NonTargetCatchComputedValueSourcePersist.fromData);

        }

        if (totalCount == null && catchWeight != null && meanWeight != null) {

            // calcul le nb estime à partir du poids estime et du poids moyen
            totalCount = (int) ((float) 1000 * catchWeight / meanWeight);
            nonTargetCatch.setTotalCount(totalCount);
            nonTargetCatch.setTotalCountComputedSource(NonTargetCatchComputedValueSourcePersist.fromData);

        }

        if (meanWeight == null && totalCount != null && totalCount != 0 && catchWeight != null &&
                !NonTargetCatchComputedValueSourcePersist.fromSample.equals(nonTargetCatch.getTotalCountComputedSource())) {

            // calcul le poids moyen à partir de nb estime et du poids estime
            // uniquement si le nombre estimé ne vient pas des échantillons (voir http://forge.codelutin.com/issues/4670)

            meanWeight = catchWeight * (float) 1000 / (float) totalCount;
            nonTargetCatch.setMeanWeight(meanWeight);
            nonTargetCatch.setMeanWeightComputedSource(NonTargetCatchComputedValueSourcePersist.fromData);

        }
    }

    protected void updateLengthWeightAble(ConsolidationActivitySeineDataContext activityContext,
                                          Species species,
                                          Sex sex,
                                          LengthWeightComputable lengthWeightComputable) {

        Float weight = lengthWeightComputable.getWeight();
        Float length = lengthWeightComputable.getLength();

        boolean computeWeight = false;
        boolean computeLength = false;

        if (weight == null && length != null) {

            // on essaye de calculer le poids
            computeWeight = true;
        }

        if (length == null && weight != null) {

            // on essaye de calcule la taille
            computeLength = true;
        }

        if (!computeLength && !computeWeight) {

            // rien a calculer
            return;
        }

        // recherche du parametrage adequate
        LengthWeightParameter lengthWeightParameter = activityContext.findLengthWeightParameter(species, sex);

        if (lengthWeightParameter == null) {

            // aucun parametrage connu

            return;
        }

        if (computeLength) {
            Float newLength = lengthWeightParameter.computeLength(weight);
            if (newLength != null) {

                lengthWeightComputable.setLength(newLength);
                lengthWeightComputable.setLengthSource(true);
                return;

            }

            // la taille n'a pas ete changee, on peut quitter car il est impossible
            // de calculer et la taille et le poids...
            return;

        }

        // on cherche obligatoirement a calculer le poids
        Float newWeight = lengthWeightParameter.computeWeight(length);
        if (newWeight != null) {

            lengthWeightComputable.setWeight(newWeight);
            lengthWeightComputable.setWeightSource(true);

        }
    }

    protected void updateLengthWeightAble(LengthWeightComputable lengthWeightComputable,
                                          LengthWeightParameter lengthWeightParameter) {

        Float weight = lengthWeightComputable.getWeight();
        boolean computeWeight = false;
        boolean computeLength = false;
        Float length = lengthWeightComputable.getLength();

        if (weight == null && length != null) {

            // on essaye de calculer le poids
            computeWeight = true;

        }

        if (length == null && weight != null) {

            // on essaye de calcule la taille
            computeLength = true;

        }

        if (!computeLength && !computeWeight) {

            // rien a calculer
            return;
        }

        if (lengthWeightParameter == null) {

            // aucun parametrage connu

            return;
        }

        if (computeLength) {
            Float newLength = lengthWeightParameter.computeLength(weight);
            if (newLength != null) {

                lengthWeightComputable.setLength(newLength);
                lengthWeightComputable.setLengthSource(true);
                return;
            }

            // la taille n'a pas ete changee, on peut quitter car il est impossible
            // de calculer et la taille et le poids...
            return;
        }

        // on cherche obligatoirement a calculer le poids
        Float newWeight = lengthWeightParameter.computeWeight(length);
        if (newWeight != null) {

            lengthWeightComputable.setWeight(newWeight);
            lengthWeightComputable.setWeightSource(true);

        }
    }

    protected static class ConsolidationActivitySeineDataContext {

        protected ObserveTopiaDaoSupplier daoSupplier;

        protected Locale locale;

        protected boolean failIfLenghtWeightParameterNotFound;

        protected TripSeine tripSeine;

        protected Route route;

        protected ActivitySeine activity;

        protected BeanMonitor targetLengthMonitor;

        protected BeanMonitor nonTargetLengthMonitor;

        protected BeanMonitor nonTargetCatchMonitor;

        protected ConsolidateActivitySeineDataResultBuilder resultBuilder;

        public Ocean getOcean() {
            return tripSeine.getOcean();
        }

        public Date getRouteDate() {
            return route.getDate();
        }

        public SetSeine getSetSeine() {
            return activity.getSetSeine();
        }

        public LengthWeightParameter findLengthWeightParameter(Species species, Sex sex) {
            Ocean ocean = getOcean();
            Date routeDate = getRouteDate();
            LengthWeightParameter lengthWeightParameter = LengthWeightParameters.findLengthWeightParameter(daoSupplier, species, ocean, sex, routeDate);
            if (lengthWeightParameter == null) {

                if (failIfLenghtWeightParameterNotFound) {
                    throw new LengthWeightParameterNotFoundException(species, ocean, sex, routeDate);
                }

                resultBuilder.registerLengthWeightParameterNotFound(species, ocean, sex, routeDate);

            }
            return lengthWeightParameter;
        }

        public void setSchoolTypeChanged(SchoolTypePersist oldSchoolType, SchoolTypePersist newSchoolType) {
            resultBuilder.setSchoolTypeChanged(oldSchoolType, newSchoolType);
        }

        public void watchTargetLength(TargetLength targetLength) {
            targetLengthMonitor.setBean(targetLength);
            if (targetLength.isLengthSource()) {
                // Reset de la valeur calculée
                targetLength.setLength(null);
                targetLength.setLengthSource(false);
            }
            if (targetLength.isWeightSource()) {
                // Reset de la valeur calculée
                targetLength.setWeight(null);
                targetLength.setWeightSource(false);
            }
        }

        public void watchNonTargetLenght(NonTargetLength nonTargetLength) {
            nonTargetLengthMonitor.setBean(nonTargetLength);
            if (nonTargetLength.isLengthSource()) {
                // Reset de la valeur calculée
                nonTargetLength.setLength(null);
                nonTargetLength.setLengthSource(false);
            }
            if (nonTargetLength.isWeightSource()) {
                // Reset de la valeur calculée
                nonTargetLength.setWeight(null);
                nonTargetLength.setWeightSource(false);
            }
        }

        public void flushTargetLength() {
            if (targetLengthMonitor.wasModified()) {
                TargetLength targetLength = (TargetLength) targetLengthMonitor.getBean();
                resultBuilder.flushTargetLengthModification(targetLength, targetLengthMonitor.getModifiedProperties());
            }
            targetLengthMonitor.setBean(null);
        }

        public void flushNonTargetLength() {
            if (nonTargetLengthMonitor.wasModified()) {
                NonTargetLength nonTargetLength = (NonTargetLength) nonTargetLengthMonitor.getBean();
                resultBuilder.flushNonTargetLengthModification(nonTargetLength, nonTargetLengthMonitor.getModifiedProperties());
            }
            nonTargetLengthMonitor.setBean(null);
        }

        public void watchNonTargetCatch(NonTargetCatch nonTargetCatch) {
            nonTargetCatchMonitor.setBean(nonTargetCatch);
            if (nonTargetCatch.isCatchWeightComputed()) {
                nonTargetCatch.setCatchWeight(null);
                nonTargetCatch.setCatchWeightComputedSource(null);
            }
            if (nonTargetCatch.isTotalCountComputed()) {
                nonTargetCatch.setTotalCount(null);
                nonTargetCatch.setTotalCountComputedSource(null);
            }
            if (nonTargetCatch.isMeanWeightComputed()) {
                nonTargetCatch.setMeanWeight(null);
                nonTargetCatch.setMeanWeightComputedSource(null);
            }
            if (nonTargetCatch.isMeanLengthComputed()) {
                nonTargetCatch.setMeanLength(null);
                nonTargetCatch.setMeanLengthComputedSource(null);
            }
        }

        public void flushNonTargetCatch() {
            if (nonTargetCatchMonitor.wasModified()) {
                NonTargetCatch nonTargetCatch = (NonTargetCatch) nonTargetCatchMonitor.getBean();
                resultBuilder.flushNonTargetCatchModification(nonTargetCatch, nonTargetCatchMonitor.getModifiedProperties());
            }
            nonTargetCatchMonitor.setBean(null);
        }

    }

}
