package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.seine.ObjectFate;
import fr.ird.observe.entities.referentiel.seine.ObjectOperation;
import fr.ird.observe.entities.referentiel.seine.ObjectType;
import fr.ird.observe.entities.seine.FloatingObject;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class FloatingObjectBinder extends DataBinderSupport<FloatingObject, FloatingObjectDto> {

    public FloatingObjectBinder() {
        super(FloatingObject.class, FloatingObjectDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, FloatingObjectDto dto, FloatingObject entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setDaysAtSeaCount(dto.getDaysAtSeaCount());
        entity.setSupportVesselName(dto.getSupportVesselName());
        entity.setObjectFate(toEntity(dto.getObjectFate(), ObjectFate.class));
        entity.setObjectOperation(toEntity(dto.getObjectOperation(), ObjectOperation.class));
        entity.setObjectType(toEntity(dto.getObjectType(), ObjectType.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, FloatingObject entity, FloatingObjectDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setDaysAtSeaCount(entity.getDaysAtSeaCount());
        dto.setSupportVesselName(entity.getSupportVesselName());
        dto.setObjectFate(toReferentialReference(referentialLocale, entity.getObjectFate(), ObjectFateDto.class));
        dto.setObjectOperation(toReferentialReference(referentialLocale, entity.getObjectOperation(), ObjectOperationDto.class));
        dto.setObjectType(toReferentialReference(referentialLocale, entity.getObjectType(), ObjectTypeDto.class));

    }


    @Override
    public DataReference<FloatingObjectDto> toDataReference(ReferentialLocale referentialLocale, FloatingObject entity) {

        return toDataReference(entity, getLabel(referentialLocale, entity.getObjectType()));

    }

    @Override
    public DataReference<FloatingObjectDto> toDataReference(ReferentialLocale referentialLocale, FloatingObjectDto dto) {

        return toDataReference(dto, getLabel(referentialLocale, dto.getObjectType()));

    }
}
