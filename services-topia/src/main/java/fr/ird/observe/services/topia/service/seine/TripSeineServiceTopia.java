package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import fr.ird.observe.entities.TripMapPoint;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.Species2;
import fr.ird.observe.entities.referentiel.SpeciesList;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.entities.seine.TripSeineTopiaDao;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.topia.entity.TripMapDtoFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripSeineServiceTopia extends ObserveServiceTopia implements TripSeineService {

    private static final Log log = LogFactory.getLog(TripSeineServiceTopia.class);

    protected TripSeineTopiaDao getDao() {
        return (TripSeineTopiaDao) serviceContext.getTopiaPersistenceContext().getDao(TripSeine.class);
    }

    @Override
    public DataReferenceSet<TripSeineDto> getAllTripSeine() {
        if (log.isTraceEnabled()) {
            log.trace("getAllTripSeine()");
        }

        List<TripSeine> tripSeines = loadEntities(TripSeine.class);

        return toDataReferenceSet(TripSeineDto.class, tripSeines);

    }

    @Override
    public DataReferenceSet<TripSeineDto> getTripSeineByProgram(String programId) {
        if (log.isTraceEnabled()) {
            log.trace("getTripSeineByProgram(" + programId + ")");
        }

        ReferentialLocale referenceLocale = getReferentialLocale();

        // pour verifier l'existance du programme
        loadEntity(ProgramDto.class, programId);

        List<TripSeine> tripSeines = getDao().findAllStubByProgramId(programId, referenceLocale.ordinal());

        return toDataReferenceSet(TripSeineDto.class, tripSeines);

    }

    @Override
    public int getTripSeinePositionInProgram(String programId, String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("getTripSeinePositionInProgram(" + programId + ", " + tripSeineId + ")");
        }

        return getDao().findPositionByProgramId(programId, tripSeineId);
    }

    @Override
    public Form<TripSeineDto> loadForm(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + tripSeineId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        Form<TripSeineDto> form = dataEntityToForm(TripSeineDto.class,
                                                   tripSeine,
                                                   ReferenceSetRequestDefinitions.TRIP_SEINE_FORM);

        TripSeineDto tripSeineDto = form.getObject();

        if (tripSeineDto.getEndDate() == null) {
            Date date = DateUtil.getEndOfDay(now());
            tripSeineDto.setEndDate(date);
        }

        return form;
    }

    @Override
    public TripSeineDto loadDto(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + tripSeineId + ")");
        }

        return loadEntityToDataDto(TripSeineDto.class, tripSeineId);
    }

    @Override
    public TripMapDto getTripSeineMap(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("getTripSeineMap(" + tripSeineId + ")");
        }

        LinkedHashSet<TripMapPoint> points = getDao().extractTripMapActivityPoints(tripSeineId);

        return TripMapDtoFactory.newTripMapDto(tripSeineId, points);

    }

    @Override
    public DataReference<TripSeineDto> loadReferenceToRead(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + tripSeineId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        return toReference(tripSeine);

    }

    @Override
    public boolean exists(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + tripSeineId + ")");
        }

        return existsEntity(TripSeine.class, tripSeineId);

    }

    @Override
    public Form<TripSeineDto> preCreate(String programId) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + programId + ")");
        }

        TripSeine tripSeine = newEntity(TripSeine.class);

        Program program = loadEntity(ProgramDto.class, programId);

        Date date = DateUtil.getDay(now());

        tripSeine.setStartDate(date);

        tripSeine.setEndDate(date);

        tripSeine.setProgram(program);

        return dataEntityToForm(TripSeineDto.class,
                                tripSeine,
                                ReferenceSetRequestDefinitions.TRIP_SEINE_FORM);

    }

    @Override
    public SaveResultDto save(TripSeineDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        TripSeine entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);

        copyDataDtoToEntity(dto, entity);

        SaveResultDto result = saveEntity(entity);

        TripSeineTopiaDao dao = getTopiaPersistenceContext().getTripSeineDao();
        dao.updateEndDate(entity);

        return result;

    }

    @Override
    public void delete(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + tripSeineId + ")");
        }

        deleteEntity(TripSeineDto.class, TripSeine.class, Collections.singleton(tripSeineId));

    }

    @Override
    public int moveTripSeineToProgram(String tripSeineId, String programId) {
        if (log.isTraceEnabled()) {
            log.trace("moveTripSeineToProgram(" + tripSeineId + ", " + programId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);
        Program program = loadEntity(ProgramDto.class, programId);
        tripSeine.setProgram(program);
        saveEntity(tripSeine);

        getTopiaPersistenceContext().flush();

        return getTripSeinePositionInProgram(programId, tripSeineId);

    }

    @Override
    public List<Integer> moveTripSeinesToProgram(List<String> tripSeineIds, String programId) {
        if (log.isTraceEnabled()) {
            log.trace("moveTripSeinesToProgram([" + Joiner.on(", ").join(tripSeineIds) + "], " + programId + ")");
        }

        Program program = loadEntity(ProgramDto.class, programId);

        List<Integer> result = new ArrayList<>();

        for (String tripSeineId : tripSeineIds) {

            TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);
            tripSeine.setProgram(program);

            saveEntity(tripSeine);
            getTopiaPersistenceContext().flush();

            result.add(getTripSeinePositionInProgram(programId, tripSeineId));
        }

        return result;

    }

    @Override
    public ImmutableList<ReferentialReference<SpeciesDto>> getSpeciesByListAndTrip(String tripSeineId, String speciesListId) {
        if (log.isTraceEnabled()) {
            log.trace("getSpeciesByListAndTrip(" + tripSeineId + ", " + speciesListId + ")");
        }

        // find Ocean
        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        Ocean ocean = tripSeine.getOcean();

        SpeciesList speciesList = loadEntity(SpeciesListDto.class, speciesListId);

        List<Species> species2 = Species2.filterByOcean(speciesList.getSpecies(), ocean);

        ReferentialBinderSupport<Species, SpeciesDto> binder = getReferentialBinder(SpeciesDto.class);

        ReferentialLocale referentialLocale = getReferentialLocale();

        ImmutableList.Builder<ReferentialReference<SpeciesDto>> references = ImmutableList.builder();

        for (Species species : species2) {

            ReferentialReference<SpeciesDto> reference = binder.toReferentialReference(referentialLocale, species);
            references.add(reference);

        }

        return references.build();
    }
}
