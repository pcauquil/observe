package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine;
import fr.ird.observe.entities.seine.GearUseFeaturesSeine;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.service.seine.TripSeineGearUseService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripSeineGearUseServiceTopia extends ObserveServiceTopia implements TripSeineGearUseService {

    private static final Log log = LogFactory.getLog(TripSeineGearUseServiceTopia.class);

    @Override
    public Form<TripSeineGearUseDto> loadForm(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + tripSeineId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineGearUseDto.class, tripSeineId);

        return dataEntityToForm(
                TripSeineGearUseDto.class,
                tripSeine,
                ReferenceSetRequestDefinitions.TRIP_SEINE_GEAR_USE_FORM);

    }

    @Override
    public SaveResultDto save(TripSeineGearUseDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        TripSeine entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);
        return saveEntity(entity);

    }

    protected SaveResultDto saveEntity(TripSeine entity) {
        SaveResultDto saveResultDto = super.saveEntity(entity);
        Date lastUpdateDate = saveResultDto.getLastUpdateDate();

        // propagate lastUpdateDate to every gear uses
        Collection<GearUseFeaturesSeine> gearUseFeaturess = entity.getGearUseFeaturesSeine();
        for (GearUseFeaturesSeine gearUseFeaturesSeine : gearUseFeaturess) {
            gearUseFeaturesSeine.setLastUpdateDate(lastUpdateDate);
            for (GearUseFeaturesMeasurementSeine gearUseFeaturesMeasurementSeine : gearUseFeaturesSeine.getGearUseFeaturesMeasurement()) {
                gearUseFeaturesMeasurementSeine.setLastUpdateDate(lastUpdateDate);
            }
        }
        return saveResultDto;
    }

}
