package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.seine.ReasonForDiscard;
import fr.ird.observe.entities.referentiel.seine.WeightCategory;
import fr.ird.observe.entities.seine.TargetCatch;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TargetCatchBinder extends DataBinderSupport<TargetCatch, TargetCatchDto> {

    public TargetCatchBinder() {
        super(TargetCatch.class, TargetCatchDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TargetCatchDto dto, TargetCatch entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setCatchWeight(dto.getCatchWeight());
        entity.setWell(dto.getWell());
        entity.setBroughtOnDeck(dto.getBroughtOnDeck());
        entity.setDiscarded(dto.isDiscarded());
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setReasonForDiscard(toEntity(dto.getReasonForDiscard(), ReasonForDiscard.class));
        entity.setWeightCategory(toEntity(dto.getWeightCategory(), WeightCategory.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, TargetCatch entity, TargetCatchDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setCatchWeight(entity.getCatchWeight());
        dto.setWell(entity.getWell());
        dto.setBroughtOnDeck(entity.getBroughtOnDeck());
        dto.setDiscarded(entity.isDiscarded());
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setReasonForDiscard(toReferentialReference(referentialLocale, entity.getReasonForDiscard(), ReasonForDiscardDto.class));
        dto.setWeightCategory(toReferentialReference(referentialLocale, entity.getWeightCategory(), WeightCategoryDto.class));

    }
}
