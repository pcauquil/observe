package fr.ird.observe.services.topia.service;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.service.trip.DeleteTripRequest;
import fr.ird.observe.services.service.trip.DeleteTripResult;
import fr.ird.observe.services.service.trip.ExportTripRequest;
import fr.ird.observe.services.service.trip.ExportTripResult;
import fr.ird.observe.services.service.trip.ImportTripRequest;
import fr.ird.observe.services.service.trip.ImportTripResult;
import fr.ird.observe.services.service.trip.TripManagementService;
import fr.ird.observe.services.topia.ObserveServiceContextTopia;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.IdDtos;
import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.AddSqlScriptProducerResult;
import fr.ird.observe.services.service.DeleteSqlScriptProducerRequest;
import fr.ird.observe.services.service.ObserveBlobsContainer;
import fr.ird.observe.services.service.SqlScriptProducerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 20/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class TripManagementServiceTopia extends ObserveServiceTopia implements TripManagementService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TripManagementServiceTopia.class);

    private SqlScriptProducerService sqlScriptProducerService;

    @Override
    public void setServiceContext(ObserveServiceContextTopia serviceContext) {
        super.setServiceContext(serviceContext);
        sqlScriptProducerService = serviceContext.newService(SqlScriptProducerService.class);
    }

    @Override
    public ExportTripResult exportTrip(ExportTripRequest exportRequest) {

        SqlScriptProducerService sqlScriptProducerService = serviceContext.newService(SqlScriptProducerService.class);

        long t0 = System.nanoTime();

        String tripId = exportRequest.getTripId();

        if (log.isInfoEnabled()) {
            log.info("Start export of trip: " + tripId);
        }

        AddSqlScriptProducerRequest request;
        request = (exportRequest.isForPG() ? AddSqlScriptProducerRequest.forPostgres() : AddSqlScriptProducerRequest.forH2())
                .dataIdsToAdd(ImmutableSet.of(tripId));
        AddSqlScriptProducerResult producerResult = sqlScriptProducerService.produceAddSqlScript(request);
        byte[] tripContent = producerResult.getSqlCode();
        ImmutableSet<ObserveBlobsContainer> blobsContainers = producerResult.getBlobsContainers();

        long time = System.nanoTime() - t0;

        return new ExportTripResult(exportRequest, tripContent, blobsContainers, time);

    }

    @Override
    public DeleteTripResult deleteTrip(DeleteTripRequest request) {

        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();

        Set<String> tripSeineIds = new TreeSet<>(persistenceContext.getTripSeineDao().findAllIds());
        Set<String> tripLonglineIds = new TreeSet<>(persistenceContext.getTripLonglineDao().findAllIds());

        String tripId = request.getTripId();

        long t0 = System.nanoTime();

        boolean deleted = deleteTrip(persistenceContext, tripId, tripSeineIds, tripLonglineIds);

        if (deleted) {

            long time = System.nanoTime() - t0;
            if (log.isInfoEnabled()) {
                log.info("Delete of trip: " + tripId + " done in " + StringUtil.convertTime(time));
            }

            return new DeleteTripResult(request, time);

        }

        return null;

    }

    @Override
    public ImportTripResult importTrip(ImportTripRequest request) {

        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();

        Set<String> tripSeineIds = new TreeSet<>(persistenceContext.getTripSeineDao().findAllIds());
        Set<String> tripLonglineIds = new TreeSet<>(persistenceContext.getTripLonglineDao().findAllIds());

        String tripId = request.getTripId();

        long t0 = System.nanoTime();

        boolean deleted = deleteTrip(persistenceContext, tripId, tripSeineIds, tripLonglineIds);

        long t1 = System.nanoTime();

        long deleteTime = 0;
        if (deleted) {

            deleteTime = System.nanoTime() - t0;
            if (log.isInfoEnabled()) {
                log.info("Delete of trip: " + tripId + " done in " + StringUtil.convertTime(deleteTime));
            }

        }

        if (log.isInfoEnabled()) {
            log.info("Start import of trip: " + request.getTripId());
        }

        ImmutableSet<ObserveBlobsContainer> blobsContainers = request.getBlobsContainers();

        if (blobsContainers.isEmpty()) {

            persistenceContext.executeSqlScript(request.getSqlContent());

        } else {

            persistenceContext.getSqlSupport().doSqlWork(new ImportTripScriptTopiaSqlWork(1000, request.getSqlContent(), blobsContainers));

        }

        persistenceContext.commit();

        long t2 = System.nanoTime();

        if (log.isInfoEnabled()) {
            log.info("Import of trip: " + tripId + " done in " + StringUtil.convertTime(t1, t2));
        }

        return new ImportTripResult(request, true, t2 - t1, deleted, deleteTime);

    }

    private boolean deleteTrip(ObserveTopiaPersistenceContext persistenceContext, String tripId, Set<String> tripSeineIds, Set<String> tripLonglineIds) {

        long t0 = System.nanoTime();

        boolean isTripSeineId = IdDtos.isTripSeineId(tripId);
        boolean deleted = (isTripSeineId && tripSeineIds.contains(tripId)) || tripLonglineIds.contains(tripId);

        if (deleted) {

            if (log.isInfoEnabled()) {
                log.info("Start delete of trip: " + tripId);
            }

            DeleteSqlScriptProducerRequest sqlRequest = DeleteSqlScriptProducerRequest.builder()
                                                                                      .dataIdsToDelete(ImmutableSet.of(tripId));
            byte[] sqlScript = sqlScriptProducerService.produceDeleteSqlScript(sqlRequest);

            persistenceContext.executeSqlScript(sqlScript);

            if (log.isInfoEnabled()) {
                log.info("Delete of trip: " + tripId + " done in " + StringUtil.convertTime(t0, System.nanoTime()));
            }

            if (isTripSeineId) {
                tripSeineIds.remove(tripId);
            } else {
                tripLonglineIds.remove(tripId);
            }

            persistenceContext.commit();
        }

        return deleted;

    }

}
