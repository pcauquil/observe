package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Gear;
import fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine;
import fr.ird.observe.entities.seine.GearUseFeaturesSeine;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class GearUseFeaturesSeineBinder extends DataBinderSupport<GearUseFeaturesSeine, GearUseFeaturesSeineDto> {

    public GearUseFeaturesSeineBinder() {
        super(GearUseFeaturesSeine.class, GearUseFeaturesSeineDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, GearUseFeaturesSeineDto dto, GearUseFeaturesSeine entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setNumber(dto.getNumber());
        entity.setUsedInTrip(dto.getUsedInTrip());
        entity.setGear(toEntity(dto.getGear(), Gear.class));
        entity.setGearUseFeaturesMeasurement(toEntityCollection(referentialLocale, dto.getGearUseFeaturesMeasurement(), GearUseFeaturesMeasurementSeine.class, entity.getGearUseFeaturesMeasurement()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, GearUseFeaturesSeine entity, GearUseFeaturesSeineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setNumber(entity.getNumber());
        dto.setUsedInTrip(entity.getUsedInTrip());
        dto.setGear(toReferentialReference(referentialLocale, entity.getGear(), GearDto.class));
        dto.setGearUseFeaturesMeasurement(toLinkedHashSetData(referentialLocale, entity.getGearUseFeaturesMeasurement(), GearUseFeaturesMeasurementSeineDto.class));

    }
}
