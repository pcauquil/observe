package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.FpaZone;
import fr.ird.observe.entities.referentiel.seine.DetectionMode;
import fr.ird.observe.entities.referentiel.seine.ReasonForNoFishing;
import fr.ird.observe.entities.referentiel.seine.SurroundingActivity;
import fr.ird.observe.entities.referentiel.seine.VesselActivitySeine;
import fr.ird.observe.entities.referentiel.seine.Wind;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ActivitySeineBinder extends DataBinderSupport<ActivitySeine, ActivitySeineDto> {

    public ActivitySeineBinder() {
        super(ActivitySeine.class, ActivitySeineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, ActivitySeineDto dto, ActivitySeine entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setTime(dto.getTime());
        entity.setLatitude(dto.getLatitude());
        entity.setLongitude(dto.getLongitude());
        entity.setVesselSpeed(dto.getVesselSpeed());
        entity.setSeaSurfaceTemperature(dto.getSeaSurfaceTemperature());
        entity.setObservedSystemDistance(dto.getObservedSystemDistance());
        entity.setErsId(dto.getErsId());
        entity.setVesselActivitySeine(toEntity(dto.getVesselActivitySeine(), VesselActivitySeine.class));
        entity.setSurroundingActivity(toEntity(dto.getSurroundingActivity(), SurroundingActivity.class));
        entity.setWind(toEntity(dto.getWind(), Wind.class));
        entity.setDetectionMode(toEntity(dto.getDetectionMode(), DetectionMode.class));
        entity.setReasonForNoFishing(toEntity(dto.getReasonForNoFishing(), ReasonForNoFishing.class));
        entity.setCurrentFpaZone(toEntity(dto.getCurrentFpaZone(), FpaZone.class));
        entity.setPreviousFpaZone(toEntity(dto.getPreviousFpaZone(), FpaZone.class));
        entity.setNextFpaZone(toEntity(dto.getNextFpaZone(), FpaZone.class));


    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, ActivitySeine entity, ActivitySeineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setTime(entity.getTime());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        dto.setQuadrant(entity.getQuadrant());
        dto.setVesselSpeed(entity.getVesselSpeed());
        dto.setSeaSurfaceTemperature(entity.getSeaSurfaceTemperature());
        dto.setObservedSystemDistance(entity.getObservedSystemDistance());
        dto.setErsId(entity.getErsId());
        dto.setVesselActivitySeine(toReferentialReference(referentialLocale, entity.getVesselActivitySeine(), VesselActivitySeineDto.class));
        dto.setSurroundingActivity(toReferentialReference(referentialLocale, entity.getSurroundingActivity(), SurroundingActivityDto.class));
        dto.setWind(toReferentialReference(referentialLocale, entity.getWind(), WindDto.class));
        dto.setDetectionMode(toReferentialReference(referentialLocale, entity.getDetectionMode(), DetectionModeDto.class));
        dto.setReasonForNoFishing(toReferentialReference(referentialLocale, entity.getReasonForNoFishing(), ReasonForNoFishingDto.class));
        dto.setCurrentFpaZone(toReferentialReference(referentialLocale, entity.getCurrentFpaZone(), FpaZoneDto.class));
        dto.setPreviousFpaZone(toReferentialReference(referentialLocale, entity.getPreviousFpaZone(), FpaZoneDto.class));
        dto.setNextFpaZone(toReferentialReference(referentialLocale, entity.getNextFpaZone(), FpaZoneDto.class));
        dto.setSetSeine(toDataReference(referentialLocale, entity.getSetSeine(), SetSeineDto.class));

    }

    @Override
    public DataReference<ActivitySeineDto> toDataReference(ReferentialLocale referentialLocale, ActivitySeine entity) {

        return toDataReference(entity,
                               entity.getTime(),
                               getLabel(referentialLocale, entity.getVesselActivitySeine()),
                               toDataReference(referentialLocale, entity.getSetSeine(), SetSeineDto.class));

    }

    @Override
    public DataReference<ActivitySeineDto> toDataReference(ReferentialLocale referentialLocale, ActivitySeineDto dto) {

        return toDataReference(dto,
                               dto.getTime(),
                               getLabel(referentialLocale, dto.getVesselActivitySeine()),
                               dto.getSetSeine());

    }
}
