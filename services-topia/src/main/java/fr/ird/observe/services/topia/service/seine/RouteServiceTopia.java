package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.ActivitySeineImpl;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.RouteTopiaDao;
import fr.ird.observe.entities.seine.Routes;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.entities.seine.TripSeineTopiaDao;
import fr.ird.observe.services.service.seine.RouteService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.data.DataBinderSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDtos;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.DataNotFoundException;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class RouteServiceTopia extends ObserveServiceTopia implements RouteService {

    private static final Log log = LogFactory.getLog(RouteServiceTopia.class);

    @Override
    public DataReferenceSet<RouteDto> getRouteByTripSeine(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("getRouteByTripSeine(" + tripSeineId + ")");
        }

        RouteTopiaDao dao = getTopiaPersistenceContext().getRouteDao();
        List<Route> allStubByTripId = dao.findAllStubByTripId(tripSeineId);

        ReferentialLocale referenceLocale = getReferentialLocale();

        DataBinderSupport<Route, RouteDto> binder = getDataBinder(RouteDto.class);

        ImmutableSet.Builder<DataReference<RouteDto>> references = ImmutableSet.builder();

        for (Route route : allStubByTripId) {

            DataReference<RouteDto> reference = binder.toDataReference(referenceLocale, route);
            references.add(reference);

        }
        return DataReferenceSet.of(RouteDto.class, references.build());

    }

    @Override
    public int getRoutePositionInTripSeine(String tripSeineId, String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("getRoutePositionInTripSeine(" + tripSeineId + ", " + routeId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        Route route = tripSeine.getRouteByTopiaId(routeId);

        return getRoutePositionInTripSeine(tripSeine, route);

    }

    protected void bindFindeVeilleProperties(Set<ActivitySeine> activitySeines, LinkedHashSet<ActivitySeineStubDto> activitySeineDtos) {

        for (ActivitySeineStubDto activitySeineDto : activitySeineDtos) {

            ActivitySeine activitySeine = Iterables.find(
                    activitySeines,
                    TopiaEntities.entityHasId(activitySeineDto.getId()));

            boolean isFindeVeille = activitySeine.getVesselActivitySeine() != null
                    && ActivitySeineImpl.ACTIVITY_FIN_DE_VEILLE.equals(activitySeine.getVesselActivitySeine().getCode());

            activitySeineDto.setActivityFinDeVeille(isFindeVeille);

        }

    }

    @Override
    public DataReference<RouteDto> loadReferenceToRead(String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + routeId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);
        return toReference(route);
    }

    @Override
    public RouteDto loadDto(String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + routeId + ")");
        }

        return loadEntityToDataDto(RouteDto.class, routeId);
    }

    @Override
    public boolean exists(String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + routeId + ")");
        }

        return existsEntity(Route.class, routeId);
    }

    @Override
    public Form<RouteDto> loadForm(String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + routeId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);

        Form<RouteDto> form = dataEntityToForm(RouteDto.class, route, null);

        bindFindeVeilleProperties(route.getActivitySeine(), form.getObject().getActivitySeine());

        return form;
    }

    @Override
    public Form<RouteDto> preCreate(String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("precreate(" + tripSeineId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        Route preCreated = newEntity(Route.class);

        Route lastRoute = Iterables.getLast(tripSeine.getRoute(), null);

        Date date;
        if (lastRoute == null) {

            // aucune route defini, on utilise la date courante
            date = now();

        } else {

            // une route precedente est definie sur la maree
            // le jour d'observation est le jour suivant celui de la
            // derniere route
            date = DateUtils.addDays(lastRoute.getDate(), 1);

            // le loch du matin est le loch du soir de la derniere route
            preCreated.setStartLogValue(lastRoute.getEndLogValue());
        }

        preCreated.setDate(DateUtil.getDay(date));

        return dataEntityToForm(RouteDto.class, preCreated, null);
    }

    @Override
    public TripChildSaveResultDto save(String tripSeineId, RouteDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + tripSeineId + ", " + dto.getId() + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        Date oldRouteDate;
        if (dto.isPersisted()) {
            Route route = loadEntity(RouteDto.class, dto.getId());
            oldRouteDate = route.getDate();

        } else {
            oldRouteDate = dto.getDate();
        }

        Route entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto saveResultDto = saveEntity(entity);

        TripChildSaveResultDto result = TripChildSaveResultDtos.of(saveResultDto);

        if (dto.isNotPersisted()) {

            tripSeine.addRoute(entity);

            saveEntity(tripSeine);

        } else {
            Date oldDate = DateUtil.getDay(oldRouteDate);
            // si le jour a change, il faut mettre à jour les dates des activitéset des sets
            boolean dateHasChanged = !oldDate.equals(dto.getDate());

            if (dateHasChanged) {
                RouteTopiaDao dao = getTopiaPersistenceContext().getRouteDao();
                dao.updateActivitiesDate(entity.getTopiaId());
            }
        }

        TripSeineTopiaDao tripSeineTopiaDao = getTopiaPersistenceContext().getTripSeineDao();
        boolean wasEndDateUpdated = tripSeineTopiaDao.updateEndDate(tripSeine);

        result.setTripEndDateUpdated(wasEndDateUpdated);

        return result;
    }

    @Override
    public boolean delete(String tripSeineId, String routeId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + tripSeineId + ", " + routeId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        Route route = loadEntity(RouteDto.class, routeId);

        if (!tripSeine.containsRoute(route)) {

            throw new DataNotFoundException(RouteDto.class, routeId);

        }

        tripSeine.removeRoute(route);

        TripSeineTopiaDao tripSeineTopiaDao = getTopiaPersistenceContext().getTripSeineDao();

        return tripSeineTopiaDao.updateEndDate(tripSeine);

    }

    @Override
    public int moveRouteToTripSeine(String routeId, String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("moveRouteToTripSeine(" + routeId + ", " + tripSeineId + ")");
        }

        Route route = loadEntity(RouteDto.class, routeId);
        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);
        tripSeine.addRoute(route);
        saveEntity(tripSeine);

        ObserveTopiaPersistenceContext persistenceContext = serviceContext.getTopiaPersistenceContext();
        persistenceContext.flush();

        return getRoutePositionInTripSeine(tripSeineId, routeId);
    }

    @Override
    public List<Integer> moveRoutesToTripSeine(List<String> routeIds, String tripSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("moveRoutesToTripSeine([" + Joiner.on(", ").join(routeIds) + ", " + tripSeineId + ")");
        }

        TripSeine tripSeine = loadEntity(TripSeineDto.class, tripSeineId);

        List<Integer> result = new ArrayList<>();

        for (String routeId : routeIds) {
            Route route = loadEntity(RouteDto.class, routeId);
            tripSeine.addRoute(route);

            result.add(getRoutePositionInTripSeine(tripSeine, route));
        }

        saveEntity(tripSeine);

        return result;
    }

    protected int getRoutePositionInTripSeine(TripSeine tripSeine, Route route) {
        return (int) tripSeine.getRoute().stream()
                              .filter(Routes.newDateBeforePredicate(route.getDate()))
                              .count();
    }
}
