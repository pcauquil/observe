package fr.ird.observe.services.topia.service;

import com.google.common.base.Optional;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.LastUpdateDate;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.ObserveEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.service.LastUpdateDateService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created on 07/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class LastUpdateDateServiceTopia extends ObserveServiceTopia implements LastUpdateDateService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LastUpdateDateServiceTopia.class);

    @Override
    public void updateReferentialLastUpdateDates() {

        for (Class<? extends ObserveReferentialEntity> entityType : BINDER_ENGINE.getReferentialEntityToDtoTypes().keySet()) {
            updateLastUpdateDate(entityType);
        }

    }

    @Override
    public void updateDataLastUpdateDates() {

        for (Class<? extends ObserveDataEntity> entityType : BINDER_ENGINE.getDataEntityToDtoTypes().keySet()) {
            updateLastUpdateDate(entityType);
        }
    }

    private <E extends ObserveEntity> void updateLastUpdateDate(Class<E> entityType) {

        Optional<LastUpdateDate> optionalLastUpdateDate = getTopiaPersistenceContext().getLastUpdateDateDao()
                                                                                      .forTypeEquals(entityType.getName())
                                                                                      .setOrderByArguments(LastUpdateDate.PROPERTY_LAST_UPDATE_DATE + " DESC")
                                                                                      .tryFindFirst();

        if (!optionalLastUpdateDate.isPresent()) {
            return;
        }

        LastUpdateDate lastUpdateDate = optionalLastUpdateDate.get();

        ObserveEntityEnum entityEnum = serviceContext.getTopiaApplicationContext().getEntityEnum(entityType);

        String schemaName = entityEnum.dbSchemaName();
        String tableName = entityEnum.dbTableName();


        Timestamp maxLastUpdateDate = getTopiaPersistenceContext().getSqlSupport().findSingleResult(new TopiaSqlQuery<Timestamp>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {

                return connection.prepareStatement("SELECT max(lastUpdateDate)::TIMESTAMP FROM " + schemaName + "." + tableName);
            }

            @Override
            public Timestamp prepareResult(ResultSet set) throws SQLException {
                return set.getTimestamp(1);
            }
        });

        if (lastUpdateDate.getLastUpdateDate().before(maxLastUpdateDate)) {

            if (log.isInfoEnabled()) {
                log.info("Update LastUpdateDate for " + entityType.getName() + " with value: " + maxLastUpdateDate);
            }
            lastUpdateDate.setLastUpdateDate(maxLastUpdateDate);
        }
    }
}
