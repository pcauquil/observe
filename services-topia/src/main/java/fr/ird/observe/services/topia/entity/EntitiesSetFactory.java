package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.ObserveEntity;
import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.CatchLongline;
import fr.ird.observe.entities.longline.Encounter;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.Tdr;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntities;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.entities.referentiel.Person;
import fr.ird.observe.entities.referentiel.Persons;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.Vessel;
import fr.ird.observe.entities.referentiel.Vessels;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.entities.seine.ObjectObservedSpecies;
import fr.ird.observe.entities.seine.ObjectSchoolEstimate;
import fr.ird.observe.entities.seine.SchoolEstimate;
import fr.ird.observe.entities.seine.TargetCatch;
import fr.ird.observe.entities.seine.TargetCatchImpl;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.dto.ObserveSpeciesListConfiguration;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Une usine de {@link EntitiesExtractor}.
 *
 * Created on 18/10/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class EntitiesSetFactory {

    protected final ImmutableMap<String, EntitiesExtractor> cache;

    public EntitiesSetFactory(ObserveSpeciesListConfiguration speciesListConfiguration) {

        ImmutableMap.Builder<String, EntitiesExtractor> builder = ImmutableMap.builder();

        // TripSeine
        registerReferentialEntitiesExtractor(builder,
                                             TripSeine.class,
                                             TripSeine.PROPERTY_CAPTAIN,
                                             Person.class,
                                             Persons.CAPTAIN_PREDICATE);
        registerReferentialEntitiesExtractor(builder,
                                             TripSeine.class,
                                             TripSeine.PROPERTY_OBSERVER,
                                             Person.class,
                                             Persons.OBSERVER_PREDICATE);
        registerReferentialEntitiesExtractor(builder,
                                             TripSeine.class,
                                             TripSeine.PROPERTY_DATA_ENTRY_OPERATOR,
                                             Person.class,
                                             Persons.DATA_ENTRY_OPERATOR_PREDICATE);
        registerReferentialEntitiesExtractor(builder,
                                             TripSeine.class,
                                             TripSeine.PROPERTY_VESSEL,
                                             Vessel.class,
                                             Vessels.newVesselSeinePredicate());

        // TripLongLine
        registerReferentialEntitiesExtractor(builder,
                                             TripLongline.class,
                                             TripLongline.PROPERTY_CAPTAIN,
                                             Person.class,
                                             Persons.CAPTAIN_PREDICATE);
        registerReferentialEntitiesExtractor(builder,
                                             TripLongline.class,
                                             TripLongline.PROPERTY_OBSERVER,
                                             Person.class,
                                             Persons.OBSERVER_PREDICATE);
        registerReferentialEntitiesExtractor(builder,
                                             TripLongline.class,
                                             TripLongline.PROPERTY_DATA_ENTRY_OPERATOR,
                                             Person.class,
                                             Persons.DATA_ENTRY_OPERATOR_PREDICATE);
        registerReferentialEntitiesExtractor(builder,
                                             TripLongline.class,
                                             TripLongline.PROPERTY_VESSEL,
                                             Vessel.class,
                                             Vessels.newVesselLonglinePredicate());

        // ObjectSchoolEstimate
        registerSpeciesListEntitiesExtractor(builder,
                                             ObjectSchoolEstimate.class,
                                             ObjectSchoolEstimate.PROPERTY_SPECIES,
                                             speciesListConfiguration.getSpeciesListSeineObjectSchoolEstimateId());

        // ObjectObservedSpecies
        registerSpeciesListEntitiesExtractor(builder,
                                             ObjectObservedSpecies.class,
                                             ObjectObservedSpecies.PROPERTY_SPECIES,
                                             speciesListConfiguration.getSpeciesListSeineObjectObservedSpeciesId());

        // SchoolEstimate
        registerSpeciesListEntitiesExtractor(builder,
                                             SchoolEstimate.class,
                                             SchoolEstimate.PROPERTY_SPECIES,
                                             speciesListConfiguration.getSpeciesListSeineSchoolEstimateId());

        // TargetCatch
        registerSpeciesListOceanEntitiesExtractor(builder,
                                                  TargetCatch.class,
                                                  TargetCatchImpl.PROPERTY_SPECIES,
                                                  speciesListConfiguration.getSpeciesListSeineTargetCatchId());

        // NonTargetCatch
        registerSpeciesListEntitiesExtractor(builder,
                                             NonTargetCatch.class,
                                             NonTargetCatch.PROPERTY_SPECIES,
                                             speciesListConfiguration.getSpeciesListSeineNonTargetCatchId());

        // Encounter
        registerSpeciesListEntitiesExtractor(builder,
                                             Encounter.class,
                                             Encounter.PROPERTY_SPECIES,
                                             speciesListConfiguration.getSpeciesListLonglineEncounterId());

        // CatchLongline
        registerSpeciesListEntitiesExtractor(builder,
                                             CatchLongline.class,
                                             CatchLongline.PROPERTY_SPECIES_CATCH,
                                             speciesListConfiguration.getSpeciesListLonglineCatchId());

        registerSpeciesListEntitiesExtractor(builder,
                                             CatchLongline.class,
                                             CatchLongline.PROPERTY_PREDATOR,
                                             speciesListConfiguration.getSpeciesListLonglineDepredatorId());

        registerSectionEntitiesExtractor(builder,
                                         CatchLongline.class,
                                         CatchLongline.PROPERTY_SECTION);

        registerBasketEntitiesExtractor(builder,
                                        CatchLongline.class,
                                        CatchLongline.PROPERTY_BASKET);

        registerBranchlineEntitiesExtractor(builder,
                                            CatchLongline.class,
                                            CatchLongline.PROPERTY_BRANCHLINE);

        // CatchLongline
        registerSpeciesListEntitiesExtractor(builder,
                                             Tdr.class,
                                             Tdr.PROPERTY_SPECIES,
                                             speciesListConfiguration.getSpeciesListLonglineCatchId());

        registerSectionEntitiesExtractor(builder,
                                         Tdr.class,
                                         Tdr.PROPERTY_SECTION);

        registerBasketEntitiesExtractor(builder,
                                        Tdr.class,
                                        Tdr.PROPERTY_BASKET);

        registerBranchlineEntitiesExtractor(builder,
                                            Tdr.class,
                                            Tdr.PROPERTY_BRANCHLINE);


        this.cache = builder.build();

    }

    public <P extends ObserveEntity, C extends ObserveEntity> EntitiesExtractor<C> newEntitiesSet(Class<P> parentType,
                                                                                                  Class<C> propertyType,
                                                                                                  String propertyName) {

        Objects.requireNonNull(parentType, "Parent type is required");
        Objects.requireNonNull(propertyName, "Property name is required");
        Objects.requireNonNull(propertyType, "Property type is required");

        String key = buildKey(parentType, propertyName);
        EntitiesExtractor<C> entitiesExtractor = cache.get(key);

        if (entitiesExtractor == null) {

            // Pas de filtre spécifique trouvé, on en construit un générique

            if (Entities.isReferentielClass(propertyType)) {

                // On veut tous les référentiels sauf ceux qui sont désactivés
                entitiesExtractor = new EntitiesExtractor<>(propertyType, (Predicate) ObserveReferentialEntities.IS_ACTIF_PREDICATE);

            } else {

                // On veut toutes les entités
                entitiesExtractor = new EntitiesExtractor<>(propertyType, null);

            }

        }

        return entitiesExtractor;

    }

    protected <P extends ObserveEntity> String buildKey(Class<P> parentType, String propertyName) {
        return parentType.getName() + "#" + propertyName;
    }

    protected <P extends ObserveEntity, C extends ObserveReferentialEntity> void registerReferentialEntitiesExtractor(
            ImmutableMap.Builder<String, EntitiesExtractor> builder,
            Class<P> parentType,
            String propertyName,
            Class<C> propertyType,
            Predicate<C> filter) {

        Predicate<C> predicate = (Predicate) ObserveReferentialEntities.IS_ACTIF_PREDICATE;
        Predicate<C> newFilter = f -> filter.test(f) && predicate.test(f);

        EntitiesExtractor<C> entitiesExtractor = new EntitiesExtractor<>(propertyType, newFilter);
        registerEntitiesExtractor(builder, parentType, propertyName, entitiesExtractor);

    }

    protected <P extends ObserveEntity> void registerSpeciesListEntitiesExtractor(
            ImmutableMap.Builder<String, EntitiesExtractor> builder,
            Class<P> parentType,
            String propertyName,
            String speciesListId) {

        EntitiesExtractor<Species> entitiesExtractor = new SpeciesListEntitiesExtractor(speciesListId);
        registerEntitiesExtractor(builder, parentType, propertyName, entitiesExtractor);

    }

    protected <P extends ObserveEntity> void registerSpeciesListOceanEntitiesExtractor(
            ImmutableMap.Builder<String, EntitiesExtractor> builder,
            Class<P> parentType,
            String propertyName,
            String speciesListId) {

        EntitiesExtractor<Species> entitiesExtractor = new SpeciesListOceanEntitiesExtractor(speciesListId);
        registerEntitiesExtractor(builder, parentType, propertyName, entitiesExtractor);

    }

    protected <P extends ObserveEntity> void registerSectionEntitiesExtractor(
            ImmutableMap.Builder<String, EntitiesExtractor> builder,
            Class<P> parentType,
            String propertyName) {

        EntitiesExtractor<Section> entitiesExtractor = new SectionEntitiesExtractor();
        registerEntitiesExtractor(builder, parentType, propertyName, entitiesExtractor);

    }

    protected <P extends ObserveEntity> void registerBasketEntitiesExtractor(
            ImmutableMap.Builder<String, EntitiesExtractor> builder,
            Class<P> parentType,
            String propertyName) {

        EntitiesExtractor<Basket> entitiesExtractor = new BasketEntitiesExtractor();
        registerEntitiesExtractor(builder, parentType, propertyName, entitiesExtractor);

    }

    protected <P extends ObserveEntity> void registerBranchlineEntitiesExtractor(
            ImmutableMap.Builder<String, EntitiesExtractor> builder,
            Class<P> parentType,
            String propertyName) {

        EntitiesExtractor<Branchline> entitiesExtractor = new BranchlineEntitiesExtractor();
        registerEntitiesExtractor(builder, parentType, propertyName, entitiesExtractor);

    }

    protected <P extends ObserveEntity, C extends ObserveEntity> void registerEntitiesExtractor(
            ImmutableMap.Builder<String, EntitiesExtractor> builder,
            Class<P> parentType,
            String propertyName,
            EntitiesExtractor<C> entitiesExtractor) {

        String key = buildKey(parentType, propertyName);
        builder.put(key, entitiesExtractor);

    }

}
