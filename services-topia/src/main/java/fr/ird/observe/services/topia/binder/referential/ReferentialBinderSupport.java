package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.I18nReferentialEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.topia.binder.BinderSupport;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.dto.reference.ReferentialReferenceSetDefinitions;
import fr.ird.observe.services.dto.referential.I18nReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.io.Serializable;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class ReferentialBinderSupport<E extends ObserveReferentialEntity, D extends ReferentialDto> extends BinderSupport<E, D> {

    protected final ReferenceSetDefinition<D> definition;

    protected ReferentialBinderSupport(Class<E> entityType, Class<D> dtoType) {
        super(entityType, dtoType);
        this.definition = ReferentialReferenceSetDefinitions.getDefinition(this.dtoType);
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- ENTITY → REFERENTIAL REFERENCE ---------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public abstract ReferentialReference<D> toReferentialReference(ReferentialLocale locale, E entity);

    protected ReferentialReference<D> toReferentialReference(E entity, Serializable... values) {

        ReferentialReference<D> reference = new ReferentialReference<>();

        reference.setId(entity.getTopiaId());
        reference.setCreateDate(entity.getTopiaCreateDate());
        reference.setVersion(entity.getTopiaVersion());

        reference.setLastUpdateDate(entity.getLastUpdateDate());
        reference.setEnabled(entity.isEnabled());
        reference.setNeedComment(entity.isNeedComment());

        reference.init(definition.getType(), definition.getPropertyNames(), values);

        return reference;
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL → REFERENTIAL REFERENCE ----------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public abstract ReferentialReference<D> toReferentialReference(ReferentialLocale locale, D dto);

    protected ReferentialReference<D> toReferentialReference(D dto, Serializable... values) {

        ReferentialReference<D> reference = new ReferentialReference<>();

        reference.setId(dto.getId());
        reference.setCreateDate(dto.getCreateDate());
        reference.setVersion(dto.getVersion());

        reference.setLastUpdateDate(dto.getLastUpdateDate());
        reference.setEnabled(dto.isEnabled());
        reference.setNeedComment(dto.isNeedComment());

        reference.init(definition.getType(), definition.getPropertyNames(), values);

        return reference;
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL REFERENCE → ENTITY ---------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public E toEntity(ReferentialReference<D> reference) {
        E entity = newEntity();
        entity.setTopiaId(reference.getId());
        return entity;
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- ENTITY → REFERENTIAL--------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public D toDto(ReferentialLocale referentialLocale, E entity) {

        D dto = newDto();
        copyToDto(referentialLocale, entity, dto);
        return dto;

    }

    protected void copyDtoI18nFieldsToEntity(I18nReferentialDto dto, I18nReferentialEntity entity) {

        entity.setLabel1(dto.getLabel1());
        entity.setLabel2(dto.getLabel2());
        entity.setLabel3(dto.getLabel3());
        entity.setLabel4(dto.getLabel4());
        entity.setLabel5(dto.getLabel5());
        entity.setLabel6(dto.getLabel6());
        entity.setLabel7(dto.getLabel7());
        entity.setLabel8(dto.getLabel8());

    }

    protected void copyDtoReferentialFieldsToEntity(ReferentialDto dto, ObserveReferentialEntity entity) {

        entity.setTopiaId(dto.getId());
        entity.setStatus(REFERENCE_STATUS_TO_ENTITY.apply(dto.getStatus()));
        entity.setNeedComment(dto.isNeedComment());
        entity.setLastUpdateDate(dto.getLastUpdateDate());
        entity.setTopiaVersion(dto.getVersion());
        entity.setTopiaCreateDate(dto.getCreateDate());
        entity.setCode(dto.getCode());
        entity.setUri(dto.getUri());

    }

    protected void copyEntityReferentialFieldsToDto(ObserveReferentialEntity entity, ReferentialDto dto) {

        dto.setId(entity.getTopiaId());
        dto.setStatus(REFERENCE_STATUS_TO_DTO.apply(entity.getStatus()));
        dto.setNeedComment(entity.isNeedComment());
        dto.setLastUpdateDate(entity.getLastUpdateDate());
        dto.setVersion(entity.getTopiaVersion());
        dto.setCreateDate(entity.getTopiaCreateDate());
        dto.setCode(entity.getCode());
        dto.setUri(entity.getUri());

    }

    protected void copyEntityI18nFieldsToDto(I18nReferentialEntity entity, I18nReferentialDto dto) {

        dto.setLabel1(entity.getLabel1());
        dto.setLabel2(entity.getLabel2());
        dto.setLabel3(entity.getLabel3());
        dto.setLabel4(entity.getLabel4());
        dto.setLabel5(entity.getLabel5());
        dto.setLabel6(entity.getLabel6());
        dto.setLabel7(entity.getLabel7());
        dto.setLabel8(entity.getLabel8());

    }

}
