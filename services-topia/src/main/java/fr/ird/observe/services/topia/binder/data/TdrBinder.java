package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.Tdr;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.longline.ItemHorizontalPosition;
import fr.ird.observe.entities.referentiel.longline.ItemVerticalPosition;
import fr.ird.observe.entities.referentiel.longline.SensorBrand;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TdrBinder extends DataBinderSupport<Tdr, TdrDto> {

    public TdrBinder() {
        super(Tdr.class, TdrDto.class, true);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TdrDto dto, Tdr entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setHomeId(dto.getHomeId());
        entity.setFloatline1Length(dto.getFloatline1Length());
        entity.setFloatline2Length(dto.getFloatline2Length());
        entity.setSerialNo(dto.getSerialNo());
        // on enregistre le fichier uniquement si il a changer
        if (dto.isHasData()) {
            if (dto.getData() != null) {
                entity.setData(byteArrayToBlob(dto.getData().getContent()));
                entity.setDataFilename(dto.getData().getName());
            }
        } else {
            entity.setData(null);
            entity.setDataFilename(null);
        }
        entity.setDataLocation(dto.getDataLocation());
        entity.setDeployementStart(dto.getDeployementStart());
        entity.setDeployementEnd(dto.getDeployementEnd());
        entity.setFishingStart(dto.getFishingStart());
        entity.setFishingEnd(dto.getFishingEnd());
        entity.setFishingStartDepth(dto.getFishingStartDepth());
        entity.setFishingEndDepth(dto.getFishingEndDepth());
        entity.setMeanDeployementDepth(dto.getMeanDeployementDepth());
        entity.setMedianDeployementDepth(dto.getMedianDeployementDepth());
        entity.setMinFishingDepth(dto.getMinFishingDepth());
        entity.setMaxFishingDepth(dto.getMaxFishingDepth());
        entity.setMeanFishingDepth(dto.getMeanFishingDepth());
        entity.setMedianFishingDepth(dto.getMedianFishingDepth());
        entity.setSensorBrand(toEntity(dto.getSensorBrand(), SensorBrand.class));
        entity.setItemHorizontalPosition(toEntity(dto.getItemHorizontalPosition(), ItemHorizontalPosition.class));
        entity.setItemVerticalPosition(toEntity(dto.getItemVerticalPosition(), ItemVerticalPosition.class));
        entity.setSpecies(toEntitySet(dto.getSpecies(), Species.class));
        entity.setBranchline(toEntity(dto.getBranchline(), Branchline.class));
        entity.setSection(toEntity(dto.getSection(), Section.class));
        entity.setBasket(toEntity(dto.getBasket(), Basket.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Tdr entity, TdrDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setHomeId(entity.getHomeId());
        dto.setFloatline1Length(entity.getFloatline1Length());
        dto.setFloatline2Length(entity.getFloatline2Length());
        dto.setSerialNo(entity.getSerialNo());
        // on envoie pas le fichier de donné
        dto.setHasData(entity.getData() != null);
        // le champ data ne sert que pour l'upload du fichier si il a changé
        dto.setData(null);
        dto.setDataLocation(entity.getDataLocation());
        dto.setDeployementStart(entity.getDeployementStart());
        dto.setDeployementEnd(entity.getDeployementEnd());
        dto.setFishingStart(entity.getFishingStart());
        dto.setFishingEnd(entity.getFishingEnd());
        dto.setFishingStartDepth(entity.getFishingStartDepth());
        dto.setFishingEndDepth(entity.getFishingEndDepth());
        dto.setMeanDeployementDepth(entity.getMeanDeployementDepth());
        dto.setMedianDeployementDepth(entity.getMedianDeployementDepth());
        dto.setMinFishingDepth(entity.getMinFishingDepth());
        dto.setMaxFishingDepth(entity.getMaxFishingDepth());
        dto.setMeanFishingDepth(entity.getMeanFishingDepth());
        dto.setMedianFishingDepth(entity.getMedianFishingDepth());
        dto.setSensorBrand(toReferentialReference(referentialLocale, entity.getSensorBrand(), SensorBrandDto.class));
        dto.setItemHorizontalPosition(toReferentialReference(referentialLocale, entity.getItemHorizontalPosition(), ItemHorizontalPositionDto.class));
        dto.setItemVerticalPosition(toReferentialReference(referentialLocale, entity.getItemVerticalPosition(), ItemVerticalPositionDto.class));
        dto.setSpecies(toReferentialReferenceList(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setBranchline(toDataReference(referentialLocale, entity.getBranchline(), BranchlineDto.class));
        dto.setSection(toDataReference(referentialLocale, entity.getSection(), SectionDto.class));
        dto.setBasket(toDataReference(referentialLocale, entity.getBasket(), BasketDto.class));

    }

    @Override
    public DataReference<TdrDto> toDataReference(ReferentialLocale referentialLocale, Tdr entity) {
        return toDataReference(entity, entity.getHomeId());
    }

    @Override
    public DataReference<TdrDto> toDataReference(ReferentialLocale referentialLocale, TdrDto dto) {
        return toDataReference(dto, dto.getHomeId());
    }

}
