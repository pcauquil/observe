package fr.ird.observe.services.topia.binder;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.topia.binder.data.DataBinderSupport;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.ObserveModelInitializerRunner;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.ReferenceBinderEngine;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.util.Map;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BinderEngine implements ReferenceBinderEngine {

    private static final BinderEngine INSTANCE = new BinderEngine();

    protected final ImmutableMap<Class<? extends ReferentialDto>, BinderSupport> referentialBinders;

    protected final ImmutableMap<Class<? extends DataDto>, BinderSupport> dataBinders;

    protected final ImmutableMap<Class<? extends ReferentialDto>, Class<? extends ObserveReferentialEntity>> referentialDtoToEntityTypes;

    protected final ImmutableMap<Class<? extends DataDto>, Class<? extends ObserveDataEntity>> dataDtoToEntityTypes;

    protected final ImmutableMap<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>> referentialEntityToDtoTypes;

    protected final ImmutableMap<Class<? extends ObserveDataEntity>, Class<? extends DataDto>> dataEntityToDtoTypes;

    protected BinderEngine() {

        BinderEngineInitializer initializer = new BinderEngineInitializer();
        ObserveModelInitializerRunner.init(initializer);

        dataBinders = initializer.dataBinders;
        referentialBinders = initializer.referentialBinders;

        dataDtoToEntityTypes = initializer.dataDtoToEntityTypes;
        referentialDtoToEntityTypes = initializer.referentialDtoToEntityTypes;

        referentialEntityToDtoTypes = initializer.referentialEntityToDtoTypes;
        dataEntityToDtoTypes = initializer.dataEntityToDtoTypes;

    }

    public static BinderEngine get() {
        return INSTANCE;
    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> ReferentialBinderSupport<E, D> getReferentialBinder(Class<D> dtoType) {
        return (ReferentialBinderSupport) referentialBinders.get(dtoType);
    }

    public <D extends DataDto, E extends ObserveDataEntity> DataBinderSupport<E, D> getDataBinder(Class<D> dtoType) {
        return (DataBinderSupport) dataBinders.get(dtoType);
    }

    public <D extends DataDto, E extends ObserveDataEntity> DataBinderSupport<E, D> getDataBinder(D dtoType) {
        return (DataBinderSupport) dataBinders.get(dtoType.getClass());
    }

    public <D extends DataDto, E extends ObserveDataEntity> Class<E> getDataEntityType(Class<D> dtoType) {
        return (Class<E>) dataDtoToEntityTypes.get(dtoType);
    }

    public <D extends DataDto, E extends ObserveDataEntity> Class<D> getDataDtoType(Class<E> entityType) {
        Class<D> result = (Class<D>) dataEntityToDtoTypes.get(entityType);

        if (result == null) {
            // Pour gérer le cas des proxy hibernate qui ne sont pas égale à la classe proxifiée
            //FIXME Trouver une meilleure solution
            String name = entityType.getName();
            for (Map.Entry<Class<? extends ObserveDataEntity>, Class<? extends DataDto>> entry : dataEntityToDtoTypes.entrySet()) {
                if (name.startsWith(entry.getKey().getName())) {
                    result = (Class<D>) entry.getValue();
                    break;
                }
            }
        }
        return result;

    }

    public <D extends ReferentialDto> Class<D> getReferentialDtoType(ObserveEntityEnum entityEnum) {
        return BinderEngine.get().getReferentialDtoType((Class) entityEnum.getContract());
    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> Class<D> getReferentialDtoType(Class<E> entityType) {
        Class<D> result = (Class<D>) referentialEntityToDtoTypes.get(entityType);
        if (result == null) {
            // Pour gérer le cas des proxy hibernate qui ne sont pas égale à la classe proxifiée
            //FIXME Trouver une meilleure solution
            String name = entityType.getName();
            for (Map.Entry<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>> entry : referentialEntityToDtoTypes.entrySet()) {
                if (name.startsWith(entry.getKey().getName())) {
                    result = (Class<D>) entry.getValue();
                    break;
                }
            }
        }
        return result;
    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> Class<E> getReferentialEntityType(Class<D> dtoType) {
        return (Class<E>) referentialDtoToEntityTypes.get(dtoType);
    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> D transformEntityToReferentialDto(ReferentialLocale referentialLocale, E entity) {

        Class<D> dtoType = getReferentialDtoType(entity.getClass());
        ReferentialBinderSupport<E, D> binder = getReferentialBinder(dtoType);

        D dto = binder.newDto();
        binder.copyToDto(referentialLocale, entity, dto);

        return dto;

    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> E transformReferentialDtoToEntity(ReferentialLocale referentialLocale, D dto) {

        Class<D> dtoType = (Class<D>) dto.getClass();
        ReferentialBinderSupport<E, D> binder = getReferentialBinder(dtoType);

        E entity = binder.newEntity();
        binder.copyToEntity(referentialLocale, dto, entity);

        return entity;

    }

    @Override
    public <D extends ReferentialDto> ReferentialReference<D> transformReferentialDtoToReference(ReferentialLocale referentialLocale, D dto) {

        Class<D> dtoType = (Class<D>) dto.getClass();
        ReferentialBinderSupport<?, D> binder = getReferentialBinder(dtoType);

        return binder.toReferentialReference(referentialLocale, dto);

    }

    @Override
    public <D extends DataDto> DataReference<D> transformDataDtoToReference(ReferentialLocale referentialLocale, D dto) {

        Class<D> dtoType = (Class<D>) dto.getClass();
        DataBinderSupport<?, D> binder = getDataBinder((Class) dtoType);

        return binder.toDataReference(referentialLocale, dto);

    }

    public <D extends DataDto, E extends ObserveDataEntity> D transformEntityToDataDto(ReferentialLocale referentialLocale, Class<D> dtoType, E entity) {

        DataBinderSupport<E, D> binder = getDataBinder(dtoType);

        D dto = binder.newDto();
        binder.copyToDto(referentialLocale, entity, dto);

        return dto;

    }

    public <D extends DataDto, E extends ObserveDataEntity> DataReference<D> transformEntityToDataReferenceDto(ReferentialLocale referentialLocale, E entity) {

        Class<D> dtoType = getDataDtoType(entity.getClass());
        DataBinderSupport<E, D> binder = getDataBinder(dtoType);

        return binder.toDataReference(referentialLocale, entity);

    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> ReferentialReference<D> transformEntityToReferentialReferenceDto(ReferentialLocale referentialLocale, E entity) {

        Class<D> dtoType = getReferentialDtoType(entity.getClass());
        ReferentialBinderSupport<E, D> binder = getReferentialBinder(dtoType);

        return binder.toReferentialReference(referentialLocale, entity, dtoType);

    }

    public <D extends DataDto, E extends ObserveDataEntity> void copyDataDtoToEntity(ReferentialLocale referentialLocale, D dto, E entity) {

        Class<D> dtoType = (Class<D>) dto.getClass();
        DataBinderSupport<E, D> binder = getDataBinder(dtoType);

        binder.copyToEntity(referentialLocale, dto, entity);

    }

    public <D extends ReferentialDto, E extends ObserveReferentialEntity> void copyReferentialDtoToEntity(ReferentialLocale referentialLocale, D dto, E entity) {

        Class<D> dtoType = (Class<D>) dto.getClass();
        ReferentialBinderSupport<E, D> binder = getReferentialBinder(dtoType);

        binder.copyToEntity(referentialLocale, dto, entity);

    }

    public ImmutableSet<Class<? extends ReferentialDto>> getReferentialDtoTypes() {
        return ImmutableSet.copyOf(getReferentialDtoToEntityTypes().keySet());
    }

    public ImmutableMap<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>> getReferentialEntityToDtoTypes() {
        return referentialEntityToDtoTypes;
    }

    public ImmutableMap<Class<? extends ObserveDataEntity>, Class<? extends DataDto>> getDataEntityToDtoTypes() {
        return dataEntityToDtoTypes;
    }

    protected ImmutableMap<Class<? extends DataDto>, Class<? extends ObserveDataEntity>> getDataDtoToEntityTypes() {
        return dataDtoToEntityTypes;
    }

    protected ImmutableMap<Class<? extends ReferentialDto>, Class<? extends ObserveReferentialEntity>> getReferentialDtoToEntityTypes() {
        return referentialDtoToEntityTypes;
    }
}
