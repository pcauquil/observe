package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.entities.seine.NonTargetSample;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class NonTargetSampleBinder extends DataBinderSupport<NonTargetSample, NonTargetSampleDto> {

    public NonTargetSampleBinder() {
        super(NonTargetSample.class, NonTargetSampleDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, NonTargetSampleDto dto, NonTargetSample entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setNonTargetLength(toEntitySet(referentialLocale, dto.getNonTargetLength(), NonTargetLength.class, entity.getNonTargetLength()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, NonTargetSample entity, NonTargetSampleDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setNonTargetLength(toLinkedHashSetData(referentialLocale, entity.getNonTargetLength(), NonTargetLengthDto.class));

    }
}
