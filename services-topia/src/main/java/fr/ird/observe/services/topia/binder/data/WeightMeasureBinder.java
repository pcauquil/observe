package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.WeightMeasure;
import fr.ird.observe.entities.referentiel.longline.WeightMeasureType;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class WeightMeasureBinder extends DataBinderSupport<WeightMeasure, WeightMeasureDto> {

    public WeightMeasureBinder() {
        super(WeightMeasure.class, WeightMeasureDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, WeightMeasureDto dto, WeightMeasure entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setWeight(dto.getWeight());
        entity.setWeightMeasureType(toEntity(dto.getWeightMeasureType(), WeightMeasureType.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, WeightMeasure entity, WeightMeasureDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setWeight(entity.getWeight());
        dto.setWeightMeasureType(toReferentialReference(referentialLocale, entity.getWeightMeasureType(), WeightMeasureTypeDto.class));

    }
}
