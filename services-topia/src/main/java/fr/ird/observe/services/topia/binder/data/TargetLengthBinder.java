package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TargetLengthBinder extends DataBinderSupport<TargetLength, TargetLengthDto> {

    public TargetLengthBinder() {
        super(TargetLength.class, TargetLengthDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TargetLengthDto dto, TargetLength entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setMeasureType(dto.getMeasureType());
        entity.setLength(dto.getLength());
        entity.setLengthSource(dto.isLengthSource());
        entity.setCount(dto.getCount());
        entity.setWeight(dto.getWeight());
        entity.setWeightSource(dto.isWeightSource());
        entity.setAcquisitionMode(dto.getAcquisitionMode());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, TargetLength entity, TargetLengthDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setMeasureType(entity.getMeasureType());
        dto.setLength(entity.getLength());
        dto.setLengthSource(entity.isLengthSource());
        dto.setCount(entity.getCount());
        dto.setWeight(entity.getWeight());
        dto.setWeightSource(entity.isWeightSource());
        dto.setAcquisitionMode(entity.getAcquisitionMode());

    }

    @Override
    public DataReference<TargetLengthDto> toDataReference(ReferentialLocale referentialLocale, TargetLength entity) {

        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getSpecies()),
                               entity.getLength(),
                               entity.getCount());

    }

    @Override
    public DataReference<TargetLengthDto> toDataReference(ReferentialLocale referentialLocale, TargetLengthDto dto) {

        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getSpecies()),
                               dto.getLength(),
                               dto.getCount());

    }
}
