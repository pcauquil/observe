package fr.ird.observe.services.topia.service.longline;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.ActivityLonglineTopiaDao;
import fr.ird.observe.entities.longline.ActivityLonglines;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.longline.TripLonglineTopiaDao;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.dto.result.TripChildSaveResultDtos;
import fr.ird.observe.services.service.DataNotFoundException;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ActivityLonglineServiceTopia extends ObserveServiceTopia implements ActivityLonglineService {

    private static final Log log = LogFactory.getLog(ActivityLonglineServiceTopia.class);

    @Override
    public DataReferenceSet<ActivityLonglineDto> getActivityLonglineByTripLongline(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("getActivityLonglineByTripLongline(" + tripLonglineId + ")");
        }

        ActivityLonglineTopiaDao dao = getTopiaPersistenceContext().getActivityLonglineDao();
        List<ActivityLongline> allStubByTripId = dao.findAllStubByTripId(tripLonglineId, getReferentialLocale().ordinal());

        return toDataReferenceSet(ActivityLonglineDto.class, allStubByTripId);

    }

    @Override
    public int getActivityLonglinePositionInTripLongline(String tripLonglineId, String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("getActivityLonglinePositionInTripLongline(" + tripLonglineId + ", " + activityLonglineId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);
        ActivityLongline activityLongline = tripLongline.getActivityLonglineByTopiaId(activityLonglineId);

        return getActivityLonglinePositionInTripLongline(tripLongline, activityLongline);

    }

    @Override
    public DataReference<ActivityLonglineDto> loadReferenceToRead(String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadReferenceToRead(" + activityLonglineId + ")");
        }

        ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityLonglineId);

        return toReference(activityLongline);

    }

    @Override
    public ActivityLonglineDto loadDto(String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadDto(" + activityLonglineId + ")");
        }

        return loadEntityToDataDto(ActivityLonglineDto.class, activityLonglineId);

    }

    @Override
    public boolean exists(String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("exists(" + activityLonglineId + ")");
        }

        return existsEntity(ActivityLongline.class, activityLonglineId);

    }

    @Override
    public Form<ActivityLonglineDto> loadForm(String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + activityLonglineId + ")");
        }

        ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityLonglineId);

        Form<ActivityLonglineDto> form = dataEntityToForm(ActivityLonglineDto.class,
                                                          activityLongline,
                                                          ReferenceSetRequestDefinitions.ACTIVITY_LONGLINE_FORM);

        form.getObject().setHasSetLongline(activityLongline.getSetLongline() != null);

        return form;

    }

    @Override
    public Form<ActivityLonglineDto> preCreate(String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("preCreate(" + tripLonglineId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        ActivityLongline preCreated = newEntity(ActivityLongline.class);

        ActivityLongline lastActivityLongline = Iterables.getLast(tripLongline.getActivityLongline(), null);

        Date timestamp;

        if (lastActivityLongline == null) {

            // première activité, on utilise la date de début de marée (voir http://forge.codelutin.com/issues/6777)
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(tripLongline.getStartDate());
            timestamp = calendar.getTime();

        } else {

            // on reprend la date et l'heure de la dernière activité
            timestamp = lastActivityLongline.getTimeStamp();

        }

        preCreated.setTimeStamp(timestamp);

        return dataEntityToForm(ActivityLonglineDto.class,
                                preCreated,
                                ReferenceSetRequestDefinitions.ACTIVITY_LONGLINE_FORM);

    }

    @Override
    public TripChildSaveResultDto save(String tripLonglineId, ActivityLonglineDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + tripLonglineId + ", " + dto.getId() + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        ActivityLongline entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        SaveResultDto saveResultDto = saveEntity(entity);

        TripChildSaveResultDto result = TripChildSaveResultDtos.of(saveResultDto);

        if (dto.isNotPersisted()) {

            tripLongline.addActivityLongline(entity);

        }

        TripLonglineTopiaDao tripLonglineTopiaDao = getTopiaPersistenceContext().getTripLonglineDao();
        boolean wasEndDateUpdated = tripLonglineTopiaDao.updateEndDate(tripLongline);

        result.setTripEndDateUpdated(wasEndDateUpdated);

        return result;

    }

    @Override
    public boolean delete(String tripLonglineId, String activityLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("delete(" + tripLonglineId + ", " + activityLonglineId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityLonglineId);

        if (!tripLongline.containsActivityLongline(activityLongline)) {

            throw new DataNotFoundException(ActivityLonglineDto.class, activityLonglineId);

        }

        tripLongline.removeActivityLongline(activityLongline);

        TripLonglineTopiaDao tripLonglineTopiaDao = getTopiaPersistenceContext().getTripLonglineDao();

        return tripLonglineTopiaDao.updateEndDate(tripLongline);
    }

    @Override
    public int moveActivityLonglineToTripLongline(String activityLonglineId, String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("moveActivityLonglineToTripLongline(" + activityLonglineId + ", " + tripLonglineId + ")");
        }

        ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityLonglineId);
        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        tripLongline.addActivityLongline(activityLongline);
        saveEntity(tripLongline);

        return getActivityLonglinePositionInTripLongline(tripLongline, activityLongline);

    }

    @Override
    public List<Integer> moveActivityLonglinesToTripLongline(List<String> activityLonglineIds, String tripLonglineId) {
        if (log.isTraceEnabled()) {
            log.trace("moveActivityLonglinesToTripLongline([" + Joiner.on(", ").join(activityLonglineIds) + "], " + tripLonglineId + ")");
        }

        TripLongline tripLongline = loadEntity(TripLonglineDto.class, tripLonglineId);

        List<Integer> result = new ArrayList<>();

        for (String activityId : activityLonglineIds) {
            ActivityLongline activityLongline = loadEntity(ActivityLonglineDto.class, activityId);
            tripLongline.addActivityLongline(activityLongline);

            result.add(getActivityLonglinePositionInTripLongline(tripLongline, activityLongline));
        }

        saveEntity(tripLongline);

        return result;

    }

    protected int getActivityLonglinePositionInTripLongline(TripLongline tripLongline, ActivityLongline activityLongline) {
        return (int) tripLongline.getActivityLongline().stream()
                                 .filter(ActivityLonglines.newTimeStampBeforePredicate(activityLongline.getTimeStamp()))
                                 .count();
    }
}
