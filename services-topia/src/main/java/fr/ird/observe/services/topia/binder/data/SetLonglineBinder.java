package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.referentiel.longline.LightsticksColor;
import fr.ird.observe.entities.referentiel.longline.LightsticksType;
import fr.ird.observe.entities.referentiel.longline.LineType;
import fr.ird.observe.entities.referentiel.longline.SettingShape;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SetLonglineBinder extends DataBinderSupport<SetLongline, SetLonglineDto> {

    public SetLonglineBinder() {
        super(SetLongline.class, SetLonglineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SetLonglineDto dto, SetLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setHomeId(dto.getHomeId());
        entity.setNumber(dto.getNumber());
        entity.setBasketsPerSectionCount(dto.getBasketsPerSectionCount());
        entity.setBranchlinesPerBasketCount(dto.getBranchlinesPerBasketCount());
        entity.setTotalSectionsCount(dto.getTotalSectionsCount());
        entity.setTotalBasketsCount(dto.getTotalBasketsCount());
        entity.setTotalHooksCount(dto.getTotalHooksCount());
        entity.setWeightedSnap(dto.getWeightedSnap());
        entity.setSnapWeight(dto.getSnapWeight());
        entity.setWeightedSwivel(dto.getWeightedSwivel());
        entity.setSwivelWeight(dto.getSwivelWeight());
        entity.setLightsticksPerBasketCount(dto.getLightsticksPerBasketCount());
        entity.setTimeBetweenHooks(dto.getTimeBetweenHooks());
        entity.setShooterUsed(dto.getShooterUsed());
        entity.setShooterSpeed(dto.getShooterSpeed());
        entity.setMaxDepthTargeted(dto.getMaxDepthTargeted());
        entity.setSettingStartTimeStamp(dto.getSettingStartTimeStamp());
        entity.setSettingStartLatitude(dto.getSettingStartLatitude());
        entity.setSettingStartLongitude(dto.getSettingStartLongitude());
        entity.setSettingEndTimeStamp(dto.getSettingEndTimeStamp());
        entity.setSettingEndLatitude(dto.getSettingEndLatitude());
        entity.setSettingEndLongitude(dto.getSettingEndLongitude());
        entity.setSettingVesselSpeed(dto.getSettingVesselSpeed());
        entity.setHaulingDirectionSameAsSetting(dto.getHaulingDirectionSameAsSetting());
        entity.setHaulingStartTimeStamp(dto.getHaulingStartTimeStamp());
        entity.setHaulingStartLatitude(dto.getHaulingStartLatitude());
        entity.setHaulingStartLongitude(dto.getHaulingStartLongitude());
        entity.setHaulingEndTimeStamp(dto.getHaulingEndTimeStamp());
        entity.setHaulingEndLatitude(dto.getHaulingEndLatitude());
        entity.setHaulingEndLongitude(dto.getHaulingEndLongitude());
        entity.setHaulingBreaks(dto.getHaulingBreaks());
        entity.setMonitored(dto.getMonitored());
        entity.setLastUpdateDate(dto.getLastUpdateDate());
        entity.setSettingShape(toEntity(dto.getSettingShape(), SettingShape.class));
        entity.setLineType(toEntity(dto.getLineType(), LineType.class));
        entity.setLightsticksType(toEntity(dto.getLightsticksType(), LightsticksType.class));
        entity.setLightsticksColor(toEntity(dto.getLightsticksColor(), LightsticksColor.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SetLongline entity, SetLonglineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setHomeId(entity.getHomeId());
        dto.setNumber(entity.getNumber());
        dto.setBasketsPerSectionCount(entity.getBasketsPerSectionCount());
        dto.setBranchlinesPerBasketCount(entity.getBranchlinesPerBasketCount());
        dto.setTotalSectionsCount(entity.getTotalSectionsCount());
        dto.setTotalBasketsCount(entity.getTotalBasketsCount());
        dto.setTotalHooksCount(entity.getTotalHooksCount());
        dto.setWeightedSnap(entity.getWeightedSnap());
        dto.setSnapWeight(entity.getSnapWeight());
        dto.setWeightedSwivel(entity.getWeightedSwivel());
        dto.setSwivelWeight(entity.getSwivelWeight());
        dto.setLightsticksPerBasketCount(entity.getLightsticksPerBasketCount());
        dto.setTimeBetweenHooks(entity.getTimeBetweenHooks());
        dto.setShooterUsed(entity.getShooterUsed());
        dto.setShooterSpeed(entity.getShooterSpeed());
        dto.setMaxDepthTargeted(entity.getMaxDepthTargeted());
        dto.setSettingStartTimeStamp(entity.getSettingStartTimeStamp());
        dto.setSettingStartLatitude(entity.getSettingStartLatitude());
        dto.setSettingStartLongitude(entity.getSettingStartLongitude());
        dto.setSettingEndTimeStamp(entity.getSettingEndTimeStamp());
        dto.setSettingEndLatitude(entity.getSettingEndLatitude());
        dto.setSettingEndLongitude(entity.getSettingEndLongitude());
        dto.setSettingVesselSpeed(entity.getSettingVesselSpeed());
        dto.setHaulingDirectionSameAsSetting(entity.getHaulingDirectionSameAsSetting());
        dto.setHaulingStartTimeStamp(entity.getHaulingStartTimeStamp());
        dto.setHaulingStartLatitude(entity.getHaulingStartLatitude());
        dto.setHaulingStartLongitude(entity.getHaulingStartLongitude());
        dto.setHaulingEndTimeStamp(entity.getHaulingEndTimeStamp());
        dto.setHaulingEndLatitude(entity.getHaulingEndLatitude());
        dto.setHaulingEndLongitude(entity.getHaulingEndLongitude());
        dto.setHaulingBreaks(entity.getHaulingBreaks());
        dto.setMonitored(entity.getMonitored());

        dto.setSettingShape(toReferentialReference(referentialLocale, entity.getSettingShape(), SettingShapeDto.class));
        dto.setLineType(toReferentialReference(referentialLocale, entity.getLineType(), LineTypeDto.class));
        dto.setLightsticksType(toReferentialReference(referentialLocale, entity.getLightsticksType(), LightsticksTypeDto.class));
        dto.setLightsticksColor(toReferentialReference(referentialLocale, entity.getLightsticksColor(), LightsticksColorDto.class));

    }

    @Override
    public DataReference<SetLonglineDto> toDataReference(ReferentialLocale referentialLocale, SetLongline entity) {

        return toDataReference(entity, entity.getHomeId());

    }

    @Override
    public DataReference<SetLonglineDto> toDataReference(ReferentialLocale referentialLocale, SetLonglineDto dto) {

        return toDataReference(dto, dto.getHomeId());

    }

}
