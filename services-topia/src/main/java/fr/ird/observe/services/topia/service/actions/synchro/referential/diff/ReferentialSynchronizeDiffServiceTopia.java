package fr.ird.observe.services.topia.service.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.constants.ReferenceStatusPersist;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialDataSourceState;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialDataSourceStates;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.BinderEngine;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialMultimap;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.util.TimeLog;

import java.util.List;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchronizeDiffServiceTopia extends ObserveServiceTopia implements ReferentialSynchronizeDiffService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialSynchronizeDiffServiceTopia.class);

    private static final TimeLog timeLog = new TimeLog(ReferentialSynchronizeDiffServiceTopia.class);

    @Override
    public <R extends ReferentialDto> ReferentialReferenceSet<R> getEnabledReferentialReferenceSet(Class<R> referentialName) {

        if (log.isTraceEnabled()) {
            log.trace("getEnabledReferentialReferenceSet(" + referentialName + ")");
        }

        Class<? extends ObserveReferentialEntity> entityType = BinderEngine.get().getReferentialEntityType(referentialName);
        ObserveEntityEnum entityEnum = ObserveEntityEnum.valueOf(entityType);
        return getEnabledReferenceSet0(entityType, referentialName);
    }

    @Override
    public <R extends ReferentialDto> ReferentialReferenceSet<R> getReferentialReferenceSet(Class<R> referentialName, ImmutableSet<String> ids) {

        if (log.isTraceEnabled()) {
            log.trace("getReferentialReferenceSet(" + referentialName + ", " + ids.size() + " element(s)");
        }
        Class<? extends ObserveReferentialEntity> entityType = BinderEngine.get().getReferentialEntityType(referentialName);
        ObserveEntityEnum entityEnum = ObserveEntityEnum.valueOf(entityType);
        return getReferenceSet0(entityType, referentialName, ids);
    }

    @Override
    public <R extends ReferentialDto> ReferentialMultimap<R> getReferentials(Class<R> referentialName, ImmutableSet<String> ids) {
        Class<? extends ObserveReferentialEntity> entityType = BinderEngine.get().getReferentialEntityType(referentialName);
        return getReferentials0(referentialName, entityType, ids);
    }

    private <E extends ObserveReferentialEntity, R extends ReferentialDto> ReferentialMultimap<R> getReferentials0(Class<R> dtoType, Class<E> entityType, ImmutableSet<String> ids) {
        ReferentialMultimap.Builder<R> result = ReferentialMultimap.builder();
        ReferentialLocale referentialLocale = getReferentialLocale();
        List<E> entities = loadEntities(entityType, ids);
        for (E entity : entities) {
            ReferentialBinderSupport<ObserveReferentialEntity, R> binder = BinderEngine.get().getReferentialBinder(dtoType);
            result.put(dtoType, binder.toDto(referentialLocale, entity));
        }

        return result.build();
    }

    @Override
    public ReferentialDataSourceStates getSourceReferentialStates() {

        if (log.isTraceEnabled()) {
            log.trace("getSourceReferentialStates()");
        }

        ReferentialDataSourceStates referentialStates = new ReferentialDataSourceStates();
        for (ObserveEntityEnum referenceEntity : Entities.REFERENCE_ENTITIES) {
            if (ObserveEntityEnum.LastUpdateDate.equals(referenceEntity)) {
                continue;
            }
            Class entityType = referenceEntity.getContract();
            Class dtoType = BinderEngine.get().getReferentialDtoType(entityType);
            getLocalSourceReferentialVersions0(dtoType, entityType, referentialStates);
        }
        return referentialStates;
    }

    private <E extends ObserveReferentialEntity, R extends ReferentialDto> ReferentialReferenceSet<R> getReferenceSet0(Class<E> entityType, Class<R> referentialName, ImmutableSet<String> ids) {

        long startTime = TimeLog.getTime();

        if (log.isDebugEnabled()) {
            log.debug("Loading referential references for " + referentialName.getName());
        }

        ObserveTopiaPersistenceContext persistenceContext = getTopiaPersistenceContext();
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        List<E> entities = dao.forTopiaIdIn(ids).findAll();
        ReferentialReferenceSet<R> result = toReferentialReferenceSet(referentialName, entities, null);

        timeLog.log(startTime, "getReferenceSet0", referentialName.getName());

        return result;

    }

    private <E extends ObserveReferentialEntity, R extends ReferentialDto> ReferentialReferenceSet<R> getEnabledReferenceSet0(Class<E> entityType, Class<R> referentialName) {

        long startTime = TimeLog.getTime();

        if (log.isDebugEnabled()) {
            log.debug("Loading enabled referential references for " + referentialName.getName());
        }

        ObserveTopiaPersistenceContext persistenceContext = getTopiaPersistenceContext();
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        List<E> entities = dao.forEquals(ObserveReferentialEntity.PROPERTY_STATUS, ReferenceStatusPersist.enabled).findAll();
        ReferentialReferenceSet<R> result = toReferentialReferenceSet(referentialName, entities, null);

        timeLog.log(startTime, "getEnabledReferenceSet0", referentialName.getName());

        return result;

    }

    private <E extends ObserveReferentialEntity, R extends ReferentialDto> void getLocalSourceReferentialVersions0(Class<R> referentialName, Class<E> entityType, ReferentialDataSourceStates localReferentialStates) {

        long startTime = TimeLog.getTime();

        if (log.isDebugEnabled()) {
            log.debug("Loading referential states for " + referentialName.getName());
        }

        ReferentialDataSourceState<R> referentialDataSourceState = new ReferentialDataSourceState<>(referentialName);
        TopiaDao<E> dao = getTopiaPersistenceContext().getDao(entityType);
        for (E e : dao) {

            referentialDataSourceState.addReferentialVersion(e.getTopiaId(), e.getLastUpdateDate(), e.getTopiaVersion(), e.isDisabled());


        }
        localReferentialStates.addReferentialVersion(referentialName, referentialDataSourceState);

        timeLog.log(startTime, "getLocalSourceReferentialVersions0", referentialName.getName());

    }

}
