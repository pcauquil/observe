package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.entities.seine.TargetSample;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TargetSampleBinder extends DataBinderSupport<TargetSample, TargetSampleDto> {

    public TargetSampleBinder() {
        super(TargetSample.class, TargetSampleDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TargetSampleDto dto, TargetSample entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setNature(dto.getNature());
        entity.setDiscarded(dto.getDiscarded());
        entity.setTargetLength(toEntitySet(referentialLocale, dto.getTargetLength(), TargetLength.class, entity.getTargetLength()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, TargetSample entity, TargetSampleDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setNature(entity.getNature());
        dto.setDiscarded(entity.getDiscarded());
        dto.setTargetLength(toLinkedHashSetData(referentialLocale, entity.getTargetLength(), TargetLengthDto.class));

    }

}
