package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.LengthWeightParameter;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class LengthWeightParameterBinder extends ReferentialBinderSupport<LengthWeightParameter, LengthWeightParameterDto> {

    public LengthWeightParameterBinder() {
        super(LengthWeightParameter.class, LengthWeightParameterDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, LengthWeightParameterDto dto, LengthWeightParameter entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        entity.setLengthWeightFormula(dto.getLengthWeightFormula());
        entity.setWeightLengthFormula(dto.getWeightLengthFormula());
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setOcean(toEntity(dto.getOcean(), Ocean.class));
        entity.setSex(toEntity(dto.getSex(), Sex.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, LengthWeightParameter entity, LengthWeightParameterDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        dto.setLengthWeightFormula(entity.getLengthWeightFormula());
        dto.setWeightLengthFormula(entity.getWeightLengthFormula());
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setOcean(toReferentialReference(referentialLocale, entity.getOcean(), OceanDto.class));
        dto.setSex(toReferentialReference(referentialLocale, entity.getSex(), SexDto.class));

    }

    @Override
    public ReferentialReference<LengthWeightParameterDto> toReferentialReference(ReferentialLocale referentialLocale, LengthWeightParameter entity) {

        return toReferentialReference(entity,
                                      entity.getCode(),
                                      getLabel(referentialLocale, entity.getOcean()),
                                      getLabel(referentialLocale, entity.getSpecies()),
                                      getLabel(referentialLocale, entity.getSex()),
                                      entity.getLengthWeightFormula(),
                                      entity.getWeightLengthFormula());

    }

    @Override
    public ReferentialReference<LengthWeightParameterDto> toReferentialReference(ReferentialLocale referentialLocale, LengthWeightParameterDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(),
                                      getLabel(referentialLocale, dto.getOcean()),
                                      getLabel(referentialLocale, dto.getSpecies()),
                                      getLabel(referentialLocale, dto.getSex()),
                                      dto.getLengthWeightFormula(),
                                      dto.getWeightLengthFormula());

    }
}
