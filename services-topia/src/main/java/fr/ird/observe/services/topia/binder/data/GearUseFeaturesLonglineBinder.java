package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.GearUseFeaturesLongline;
import fr.ird.observe.entities.longline.GearUseFeaturesMeasurementLongline;
import fr.ird.observe.entities.referentiel.Gear;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.referential.GearDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class GearUseFeaturesLonglineBinder extends DataBinderSupport<GearUseFeaturesLongline, GearUseFeaturesLonglineDto> {

    public GearUseFeaturesLonglineBinder() {
        super(GearUseFeaturesLongline.class, GearUseFeaturesLonglineDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, GearUseFeaturesLonglineDto dto, GearUseFeaturesLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setNumber(dto.getNumber());
        entity.setUsedInTrip(dto.getUsedInTrip());
        entity.setGear(toEntity(dto.getGear(), Gear.class));
        entity.setGearUseFeaturesMeasurement(toEntitySet(referentialLocale, dto.getGearUseFeaturesMeasurement(), GearUseFeaturesMeasurementLongline.class, entity.getGearUseFeaturesMeasurement()));
    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, GearUseFeaturesLongline entity, GearUseFeaturesLonglineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setNumber(entity.getNumber());
        dto.setUsedInTrip(entity.getUsedInTrip());
        dto.setGear(toReferentialReference(referentialLocale, entity.getGear(), GearDto.class));
        dto.setGearUseFeaturesMeasurement(toLinkedHashSetData(referentialLocale, entity.getGearUseFeaturesMeasurement(), GearUseFeaturesMeasurementLonglineDto.class));

    }
}
