package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.ObserveEntity;
import org.nuiton.topia.persistence.TopiaDao;

import java.util.Collection;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Cette classe permet d'extraire dynamiquement un ensemble d'entitiées de la base de données.
 * cet emsenble est definit par un ensemble extrait de la base, definit dans getEntitiesSetBase
 * Sur cet ensemble on applique le filtre definit l'attribut optionalFilter
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class EntitiesExtractor<E extends ObserveEntity> {

    /**
     * Le type de l'entité dont on cherche des éléments.
     */
    protected final Class<E> entityType;

    /**
     * Un prédicat de filtrage optionnel.
     */
    protected final Predicate<E> optionalFilter;

    protected EntitiesExtractor(Class<E> entityType, Predicate<E> optionalFilter) {
        this.entityType = entityType;
        this.optionalFilter = optionalFilter;
    }

    public Iterable<E> getEntities(ObserveTopiaPersistenceContext persistenceContext, Map<String, Object> requestContext) {
        Collection<E> result = getEntitiesSetBase(persistenceContext, requestContext);
        Predicate<E> filter = getFilter(requestContext);
        if (filter != null) {
            result = result.stream().filter(filter).collect(Collectors.toSet());
        }
        return result;
    }

    protected Collection<E> getEntitiesSetBase(ObserveTopiaPersistenceContext persistenceContext, Map<String, Object> requestContext) {
        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        return dao.findAll();
    }

    protected Predicate<E> getFilter(Map<String, Object> requestContext) {
        return optionalFilter == null ? null : optionalFilter;
    }

}
