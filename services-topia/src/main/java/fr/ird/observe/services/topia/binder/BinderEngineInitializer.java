package fr.ird.observe.services.topia.binder;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.BaitsComposition;
import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.BranchlinesComposition;
import fr.ird.observe.entities.longline.CatchLongline;
import fr.ird.observe.entities.longline.Encounter;
import fr.ird.observe.entities.longline.FloatlinesComposition;
import fr.ird.observe.entities.longline.GearUseFeaturesLongline;
import fr.ird.observe.entities.longline.GearUseFeaturesMeasurementLongline;
import fr.ird.observe.entities.longline.HooksComposition;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SensorUsed;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.longline.SizeMeasure;
import fr.ird.observe.entities.longline.Tdr;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.longline.WeightMeasure;
import fr.ird.observe.entities.referentiel.Country;
import fr.ird.observe.entities.referentiel.FpaZone;
import fr.ird.observe.entities.referentiel.Gear;
import fr.ird.observe.entities.referentiel.GearCaracteristic;
import fr.ird.observe.entities.referentiel.GearCaracteristicType;
import fr.ird.observe.entities.referentiel.Harbour;
import fr.ird.observe.entities.referentiel.LengthWeightParameter;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Organism;
import fr.ird.observe.entities.referentiel.Person;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.SpeciesGroup;
import fr.ird.observe.entities.referentiel.SpeciesList;
import fr.ird.observe.entities.referentiel.Vessel;
import fr.ird.observe.entities.referentiel.VesselSizeCategory;
import fr.ird.observe.entities.referentiel.VesselType;
import fr.ird.observe.entities.referentiel.longline.BaitHaulingStatus;
import fr.ird.observe.entities.referentiel.longline.BaitSettingStatus;
import fr.ird.observe.entities.referentiel.longline.BaitType;
import fr.ird.observe.entities.referentiel.longline.CatchFateLongline;
import fr.ird.observe.entities.referentiel.longline.EncounterType;
import fr.ird.observe.entities.referentiel.longline.Healthness;
import fr.ird.observe.entities.referentiel.longline.HookPosition;
import fr.ird.observe.entities.referentiel.longline.HookSize;
import fr.ird.observe.entities.referentiel.longline.HookType;
import fr.ird.observe.entities.referentiel.longline.ItemHorizontalPosition;
import fr.ird.observe.entities.referentiel.longline.ItemVerticalPosition;
import fr.ird.observe.entities.referentiel.longline.LightsticksColor;
import fr.ird.observe.entities.referentiel.longline.LightsticksType;
import fr.ird.observe.entities.referentiel.longline.LineType;
import fr.ird.observe.entities.referentiel.longline.MaturityStatus;
import fr.ird.observe.entities.referentiel.longline.MitigationType;
import fr.ird.observe.entities.referentiel.longline.SensorBrand;
import fr.ird.observe.entities.referentiel.longline.SensorDataFormat;
import fr.ird.observe.entities.referentiel.longline.SensorType;
import fr.ird.observe.entities.referentiel.longline.SettingShape;
import fr.ird.observe.entities.referentiel.longline.SizeMeasureType;
import fr.ird.observe.entities.referentiel.longline.StomacFullness;
import fr.ird.observe.entities.referentiel.longline.TripType;
import fr.ird.observe.entities.referentiel.longline.VesselActivityLongline;
import fr.ird.observe.entities.referentiel.longline.WeightMeasureType;
import fr.ird.observe.entities.referentiel.seine.DetectionMode;
import fr.ird.observe.entities.referentiel.seine.ObjectFate;
import fr.ird.observe.entities.referentiel.seine.ObjectOperation;
import fr.ird.observe.entities.referentiel.seine.ObjectType;
import fr.ird.observe.entities.referentiel.seine.ObservedSystem;
import fr.ird.observe.entities.referentiel.seine.ReasonForDiscard;
import fr.ird.observe.entities.referentiel.seine.ReasonForNoFishing;
import fr.ird.observe.entities.referentiel.seine.ReasonForNullSet;
import fr.ird.observe.entities.referentiel.seine.SpeciesFate;
import fr.ird.observe.entities.referentiel.seine.SpeciesStatus;
import fr.ird.observe.entities.referentiel.seine.SurroundingActivity;
import fr.ird.observe.entities.referentiel.seine.TransmittingBuoyOperation;
import fr.ird.observe.entities.referentiel.seine.TransmittingBuoyType;
import fr.ird.observe.entities.referentiel.seine.VesselActivitySeine;
import fr.ird.observe.entities.referentiel.seine.WeightCategory;
import fr.ird.observe.entities.referentiel.seine.Wind;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.FloatingObject;
import fr.ird.observe.entities.seine.GearUseFeaturesMeasurementSeine;
import fr.ird.observe.entities.seine.GearUseFeaturesSeine;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.entities.seine.NonTargetSample;
import fr.ird.observe.entities.seine.ObjectObservedSpecies;
import fr.ird.observe.entities.seine.ObjectSchoolEstimate;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.SchoolEstimate;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.entities.seine.TargetCatch;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.entities.seine.TargetSample;
import fr.ird.observe.entities.seine.TransmittingBuoy;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.topia.binder.data.ActivityLonglineBinder;
import fr.ird.observe.services.topia.binder.data.ActivityLonglineEncouterDtoBinder;
import fr.ird.observe.services.topia.binder.data.ActivityLonglineSensorUsedDtoBinder;
import fr.ird.observe.services.topia.binder.data.ActivityLonglineStubDtoBinder;
import fr.ird.observe.services.topia.binder.data.ActivitySeineBinder;
import fr.ird.observe.services.topia.binder.data.ActivitySeineObservedSystemDtoBinder;
import fr.ird.observe.services.topia.binder.data.ActivitySeineStubDtoBinder;
import fr.ird.observe.services.topia.binder.data.BaitsCompositionBinder;
import fr.ird.observe.services.topia.binder.data.BasketBinder;
import fr.ird.observe.services.topia.binder.data.BranchlineBinder;
import fr.ird.observe.services.topia.binder.data.BranchlinesCompositionBinder;
import fr.ird.observe.services.topia.binder.data.CatchLonglineBinder;
import fr.ird.observe.services.topia.binder.data.EncounterBinder;
import fr.ird.observe.services.topia.binder.data.FloatingObjectBinder;
import fr.ird.observe.services.topia.binder.data.FloatingObjectObservedSpeciesDtoBinder;
import fr.ird.observe.services.topia.binder.data.FloatingObjectSchoolEstimateDtoBinder;
import fr.ird.observe.services.topia.binder.data.FloatingObjectTransmittingBuoyDtoBinder;
import fr.ird.observe.services.topia.binder.data.FloatlinesCompositionBinder;
import fr.ird.observe.services.topia.binder.data.GearUseFeaturesLonglineBinder;
import fr.ird.observe.services.topia.binder.data.GearUseFeaturesMeasurementLonglineBinder;
import fr.ird.observe.services.topia.binder.data.GearUseFeaturesMeasurementSeineBinder;
import fr.ird.observe.services.topia.binder.data.GearUseFeaturesSeineBinder;
import fr.ird.observe.services.topia.binder.data.HooksCompositionBinder;
import fr.ird.observe.services.topia.binder.data.NonTargetCatchBinder;
import fr.ird.observe.services.topia.binder.data.NonTargetLengthBinder;
import fr.ird.observe.services.topia.binder.data.NonTargetSampleBinder;
import fr.ird.observe.services.topia.binder.data.ObjectObservedSpeciesBinder;
import fr.ird.observe.services.topia.binder.data.ObjectSchoolEstimateBinder;
import fr.ird.observe.services.topia.binder.data.RouteBinder;
import fr.ird.observe.services.topia.binder.data.RouteStubDtoBinder;
import fr.ird.observe.services.topia.binder.data.SchoolEstimateBinder;
import fr.ird.observe.services.topia.binder.data.SectionBinder;
import fr.ird.observe.services.topia.binder.data.SensorUsedBinder;
import fr.ird.observe.services.topia.binder.data.SetLonglineBinder;
import fr.ird.observe.services.topia.binder.data.SetLonglineCatchDtoBinder;
import fr.ird.observe.services.topia.binder.data.SetLonglineDetailCompositionDtoBinder;
import fr.ird.observe.services.topia.binder.data.SetLonglineGlobalCompositionDtoBinder;
import fr.ird.observe.services.topia.binder.data.SetLonglineStubDtoBinder;
import fr.ird.observe.services.topia.binder.data.SetLonglineTdrDtoBinder;
import fr.ird.observe.services.topia.binder.data.SetSeineBinder;
import fr.ird.observe.services.topia.binder.data.SetSeineNonTargetCatchDtoBinder;
import fr.ird.observe.services.topia.binder.data.SetSeineSchoolEstimateDtoBinder;
import fr.ird.observe.services.topia.binder.data.SetSeineTargetCatchDtoBinder;
import fr.ird.observe.services.topia.binder.data.SizeMeasureBinder;
import fr.ird.observe.services.topia.binder.data.TargetCatchBinder;
import fr.ird.observe.services.topia.binder.data.TargetLengthBinder;
import fr.ird.observe.services.topia.binder.data.TargetSampleBinder;
import fr.ird.observe.services.topia.binder.data.TdrBinder;
import fr.ird.observe.services.topia.binder.data.TransmittingBuoyBinder;
import fr.ird.observe.services.topia.binder.data.TripLonglineActivityDtoBinder;
import fr.ird.observe.services.topia.binder.data.TripLonglineBinder;
import fr.ird.observe.services.topia.binder.data.TripLonglineGearUseDtoBinder;
import fr.ird.observe.services.topia.binder.data.TripSeineBinder;
import fr.ird.observe.services.topia.binder.data.TripSeineGearUseDtoBinder;
import fr.ird.observe.services.topia.binder.data.WeightMeasureBinder;
import fr.ird.observe.services.topia.binder.referential.BaitHaulingStatusBinder;
import fr.ird.observe.services.topia.binder.referential.BaitSettingStatusBinder;
import fr.ird.observe.services.topia.binder.referential.BaitTypeBinder;
import fr.ird.observe.services.topia.binder.referential.CatchFateLonglineBinder;
import fr.ird.observe.services.topia.binder.referential.CountryBinder;
import fr.ird.observe.services.topia.binder.referential.DetectionModeBinder;
import fr.ird.observe.services.topia.binder.referential.EncounterTypeBinder;
import fr.ird.observe.services.topia.binder.referential.FpaZoneBinder;
import fr.ird.observe.services.topia.binder.referential.GearBinder;
import fr.ird.observe.services.topia.binder.referential.GearCaracteristicBinder;
import fr.ird.observe.services.topia.binder.referential.GearCaracteristicTypeBinder;
import fr.ird.observe.services.topia.binder.referential.HarbourBinder;
import fr.ird.observe.services.topia.binder.referential.HealthnessBinder;
import fr.ird.observe.services.topia.binder.referential.HookPositionBinder;
import fr.ird.observe.services.topia.binder.referential.HookSizeBinder;
import fr.ird.observe.services.topia.binder.referential.HookTypeBinder;
import fr.ird.observe.services.topia.binder.referential.ItemHorizontalPositionBinder;
import fr.ird.observe.services.topia.binder.referential.ItemVerticalPositionBinder;
import fr.ird.observe.services.topia.binder.referential.LengthWeightParameterBinder;
import fr.ird.observe.services.topia.binder.referential.LightsticksColorBinder;
import fr.ird.observe.services.topia.binder.referential.LightsticksTypeBinder;
import fr.ird.observe.services.topia.binder.referential.LineTypeBinder;
import fr.ird.observe.services.topia.binder.referential.MaturityStatusBinder;
import fr.ird.observe.services.topia.binder.referential.MitigationTypeBinder;
import fr.ird.observe.services.topia.binder.referential.ObjectFateBinder;
import fr.ird.observe.services.topia.binder.referential.ObjectOperationBinder;
import fr.ird.observe.services.topia.binder.referential.ObjectTypeBinder;
import fr.ird.observe.services.topia.binder.referential.ObservedSystemBinder;
import fr.ird.observe.services.topia.binder.referential.OceanBinder;
import fr.ird.observe.services.topia.binder.referential.OrganismBinder;
import fr.ird.observe.services.topia.binder.referential.PersonBinder;
import fr.ird.observe.services.topia.binder.referential.ProgramBinder;
import fr.ird.observe.services.topia.binder.referential.ReasonForDiscardBinder;
import fr.ird.observe.services.topia.binder.referential.ReasonForNoFishingBinder;
import fr.ird.observe.services.topia.binder.referential.ReasonForNullSetBinder;
import fr.ird.observe.services.topia.binder.referential.SensorBrandBinder;
import fr.ird.observe.services.topia.binder.referential.SensorDataFormatBinder;
import fr.ird.observe.services.topia.binder.referential.SensorTypeBinder;
import fr.ird.observe.services.topia.binder.referential.SettingShapeBinder;
import fr.ird.observe.services.topia.binder.referential.SexBinder;
import fr.ird.observe.services.topia.binder.referential.SizeMeasureTypeBinder;
import fr.ird.observe.services.topia.binder.referential.SpeciesBinder;
import fr.ird.observe.services.topia.binder.referential.SpeciesFateBinder;
import fr.ird.observe.services.topia.binder.referential.SpeciesGroupBinder;
import fr.ird.observe.services.topia.binder.referential.SpeciesListBinder;
import fr.ird.observe.services.topia.binder.referential.SpeciesStatusBinder;
import fr.ird.observe.services.topia.binder.referential.StomacFullnessBinder;
import fr.ird.observe.services.topia.binder.referential.SurroundingActivityBinder;
import fr.ird.observe.services.topia.binder.referential.TransmittingBuoyOperationBinder;
import fr.ird.observe.services.topia.binder.referential.TransmittingBuoyTypeBinder;
import fr.ird.observe.services.topia.binder.referential.TripTypeBinder;
import fr.ird.observe.services.topia.binder.referential.VesselActivityLonglineBinder;
import fr.ird.observe.services.topia.binder.referential.VesselActivitySeineBinder;
import fr.ird.observe.services.topia.binder.referential.VesselBinder;
import fr.ird.observe.services.topia.binder.referential.VesselSizeCategoryBinder;
import fr.ird.observe.services.topia.binder.referential.VesselTypeBinder;
import fr.ird.observe.services.topia.binder.referential.WeightCategoryBinder;
import fr.ird.observe.services.topia.binder.referential.WeightMeasureTypeBinder;
import fr.ird.observe.services.topia.binder.referential.WindBinder;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.ObserveModelInitializer;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineEncounterDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineStubDto;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BasketWithSectionIdDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlineWithBasketIdDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineStubDto;
import fr.ird.observe.services.dto.longline.SetLonglineTdrDto;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TripLonglineActivityDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineObservedSystemDto;
import fr.ird.observe.services.dto.seine.ActivitySeineStubDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.FloatingObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.RouteStubDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;

import java.util.Map;

class BinderEngineInitializer implements ObserveModelInitializer {

    ImmutableMap.Builder<Class<? extends DataDto>, Class<? extends ObserveDataEntity>> dataDtoToEntityTypeBuilder;

    ImmutableMap.Builder<Class<? extends ObserveDataEntity>, Class<? extends DataDto>> dataEntityToDtoTypeBuilder;

    ImmutableMap.Builder<Class<? extends DataDto>, BinderSupport> dataBinderBuilder;

    ImmutableMap.Builder<Class<? extends ReferentialDto>, Class<? extends ObserveReferentialEntity>> referentialDtoToEntityTypeBuilder;

    ImmutableMap.Builder<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>> referentialEntityToDtoTypeBuilder;

    ImmutableMap.Builder<Class<? extends ReferentialDto>, BinderSupport> referentialBinderBuilder;

    ImmutableMap<Class<? extends DataDto>, Class<? extends ObserveDataEntity>> dataDtoToEntityTypes;

    ImmutableMap<Class<? extends ObserveDataEntity>, Class<? extends DataDto>> dataEntityToDtoTypes;

    ImmutableMap<Class<? extends ReferentialDto>, Class<? extends ObserveReferentialEntity>> referentialDtoToEntityTypes;

    ImmutableMap<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>> referentialEntityToDtoTypes;

    ImmutableMap<Class<? extends DataDto>, BinderSupport> dataBinders;

    ImmutableMap<Class<? extends ReferentialDto>, BinderSupport> referentialBinders;

    @Override
    public void start() {

        dataDtoToEntityTypeBuilder = ImmutableMap.builder();
        referentialDtoToEntityTypeBuilder = ImmutableMap.builder();

        dataEntityToDtoTypeBuilder = ImmutableMap.builder();
        referentialEntityToDtoTypeBuilder = ImmutableMap.builder();

        dataBinderBuilder = ImmutableMap.builder();
        referentialBinderBuilder = ImmutableMap.builder();

    }

    @Override
    public void end() {
        dataDtoToEntityTypes = dataDtoToEntityTypeBuilder.build();
        referentialDtoToEntityTypes = referentialDtoToEntityTypeBuilder.build();

        dataBinders = dataBinderBuilder.build();
        referentialBinders = referentialBinderBuilder.build();

        for (Map.Entry<Class<? extends DataDto>, Class<? extends ObserveDataEntity>> entry : dataDtoToEntityTypes.entrySet()) {

            Class<? extends DataDto> dtoType = entry.getKey();
            Class<? extends ObserveDataEntity> entityType = entry.getValue();

            if ((entityType.getSimpleName() + "Dto").equals(dtoType.getSimpleName())) {
                ObserveEntityEnum observeEntityEnum = ObserveEntityEnum.valueOf(entityType);
                dataEntityToDtoTypeBuilder.put(entityType, dtoType);
                dataEntityToDtoTypeBuilder.put((Class<? extends ObserveDataEntity>) observeEntityEnum.getImplementation(), dtoType);
            }
        }

        for (Map.Entry<Class<? extends ReferentialDto>, Class<? extends ObserveReferentialEntity>> entry : referentialDtoToEntityTypes.entrySet()) {

            Class<? extends ReferentialDto> dtoType = entry.getKey();
            Class<? extends ObserveReferentialEntity> entityType = entry.getValue();

            if ((entityType.getSimpleName() + "Dto").equals(dtoType.getSimpleName())) {
                ObserveEntityEnum observeEntityEnum = ObserveEntityEnum.valueOf(entityType);
                referentialEntityToDtoTypeBuilder.put(entityType, dtoType);
                referentialEntityToDtoTypeBuilder.put((Class<? extends ObserveReferentialEntity>) observeEntityEnum.getImplementation(), dtoType);
            }
        }
        dataEntityToDtoTypes = dataEntityToDtoTypeBuilder.build();
        referentialEntityToDtoTypes = referentialEntityToDtoTypeBuilder.build();
    }

    @Override
    public void initCommentableDto() {

    }

    @Override
    public void initDataDto() {

    }

    @Override
    public void initDataFileDto() {

    }

    @Override
    public void initIdDto() {

    }

    @Override
    public void initObserveDbUserDto() {

    }

    @Override
    public void initOpenableDto() {

    }

    @Override
    public void initTripMapDto() {

    }

    @Override
    public void initTripMapPointDto() {

    }

    @Override
    public void initReferentialDto() {

    }

    @Override
    public void initI18nReferentialDto() {

    }

    @Override
    public void initSaveResultDto() {

    }

    @Override
    public void initTripChildSaveResultDto() {

    }

    @Override
    public void initActivityLonglineDto() {
        dataDtoToEntityTypeBuilder.put(ActivityLonglineDto.class, ActivityLongline.class);
        dataBinderBuilder.put(ActivityLonglineDto.class, new ActivityLonglineBinder());
    }

    @Override
    public void initActivityLonglineEncounterDto() {
        dataDtoToEntityTypeBuilder.put(ActivityLonglineEncounterDto.class, ActivityLongline.class);
        dataBinderBuilder.put(ActivityLonglineEncounterDto.class, new ActivityLonglineEncouterDtoBinder());
    }

    @Override
    public void initActivityLonglineSensorUsedDto() {
        dataDtoToEntityTypeBuilder.put(ActivityLonglineSensorUsedDto.class, ActivityLongline.class);
        dataBinderBuilder.put(ActivityLonglineSensorUsedDto.class, new ActivityLonglineSensorUsedDtoBinder());
    }

    @Override
    public void initActivityLonglineStubDto() {
        dataDtoToEntityTypeBuilder.put(ActivityLonglineStubDto.class, ActivityLongline.class);
        dataBinderBuilder.put(ActivityLonglineStubDto.class, new ActivityLonglineStubDtoBinder());
    }

    @Override
    public void initBaitsCompositionDto() {
        dataDtoToEntityTypeBuilder.put(BaitsCompositionDto.class, BaitsComposition.class);
        dataBinderBuilder.put(BaitsCompositionDto.class, new BaitsCompositionBinder());
    }

    @Override
    public void initBasketDto() {
        dataDtoToEntityTypeBuilder.put(BasketDto.class, Basket.class);
        dataBinderBuilder.put(BasketDto.class, new BasketBinder());
        dataDtoToEntityTypeBuilder.put(BasketWithSectionIdDto.class, Basket.class);
        dataBinderBuilder.put(BasketWithSectionIdDto.class, new BasketBinder());
    }

    @Override
    public void initBranchlineDto() {
        dataDtoToEntityTypeBuilder.put(BranchlineDto.class, Branchline.class);
        dataBinderBuilder.put(BranchlineDto.class, new BranchlineBinder());
        dataDtoToEntityTypeBuilder.put(BranchlineWithBasketIdDto.class, Branchline.class);
        dataBinderBuilder.put(BranchlineWithBasketIdDto.class, new BranchlineBinder());
    }

    @Override
    public void initBranchlinesCompositionDto() {
        dataDtoToEntityTypeBuilder.put(BranchlinesCompositionDto.class, BranchlinesComposition.class);
        dataBinderBuilder.put(BranchlinesCompositionDto.class, new BranchlinesCompositionBinder());
    }

    @Override
    public void initCatchLonglineDto() {
        dataDtoToEntityTypeBuilder.put(CatchLonglineDto.class, CatchLongline.class);
        dataBinderBuilder.put(CatchLonglineDto.class, new CatchLonglineBinder());
    }

    @Override
    public void initEncounterDto() {
        dataDtoToEntityTypeBuilder.put(EncounterDto.class, Encounter.class);
        dataBinderBuilder.put(EncounterDto.class, new EncounterBinder());
    }

    @Override
    public void initFloatlinesCompositionDto() {
        dataDtoToEntityTypeBuilder.put(FloatlinesCompositionDto.class, FloatlinesComposition.class);
        dataBinderBuilder.put(FloatlinesCompositionDto.class, new FloatlinesCompositionBinder());
    }

    @Override
    public void initGearUseFeaturesLonglineDto() {
        dataDtoToEntityTypeBuilder.put(GearUseFeaturesLonglineDto.class, GearUseFeaturesLongline.class);
        dataBinderBuilder.put(GearUseFeaturesLonglineDto.class, new GearUseFeaturesLonglineBinder());
    }

    @Override
    public void initGearUseFeaturesMeasurementLonglineDto() {
        dataDtoToEntityTypeBuilder.put(GearUseFeaturesMeasurementLonglineDto.class, GearUseFeaturesMeasurementLongline.class);
        dataBinderBuilder.put(GearUseFeaturesMeasurementLonglineDto.class, new GearUseFeaturesMeasurementLonglineBinder());
    }

    @Override
    public void initHooksCompositionDto() {
        dataDtoToEntityTypeBuilder.put(HooksCompositionDto.class, HooksComposition.class);
        dataBinderBuilder.put(HooksCompositionDto.class, new HooksCompositionBinder());
    }

    @Override
    public void initSectionDto() {
        dataDtoToEntityTypeBuilder.put(SectionDto.class, Section.class);
        dataBinderBuilder.put(SectionDto.class, new SectionBinder());
    }

    @Override
    public void initSensorUsedDto() {
        dataDtoToEntityTypeBuilder.put(SensorUsedDto.class, SensorUsed.class);
        dataBinderBuilder.put(SensorUsedDto.class, new SensorUsedBinder());
    }

    @Override
    public void initSetLonglineDto() {
        dataDtoToEntityTypeBuilder.put(SetLonglineDto.class, SetLongline.class);
        dataBinderBuilder.put(SetLonglineDto.class, new SetLonglineBinder());
    }

    @Override
    public void initSetLonglineCatchDto() {
        dataDtoToEntityTypeBuilder.put(SetLonglineCatchDto.class, SetLongline.class);
        dataBinderBuilder.put(SetLonglineCatchDto.class, new SetLonglineCatchDtoBinder());
    }

    @Override
    public void initSetLonglineDetailCompositionDto() {
        dataDtoToEntityTypeBuilder.put(SetLonglineDetailCompositionDto.class, SetLongline.class);
        dataBinderBuilder.put(SetLonglineDetailCompositionDto.class, new SetLonglineDetailCompositionDtoBinder());
    }

    @Override
    public void initSetLonglineGlobalCompositionDto() {
        dataDtoToEntityTypeBuilder.put(SetLonglineGlobalCompositionDto.class, SetLongline.class);
        dataBinderBuilder.put(SetLonglineGlobalCompositionDto.class, new SetLonglineGlobalCompositionDtoBinder());
    }

    @Override
    public void initSetLonglineStubDto() {
        dataDtoToEntityTypeBuilder.put(SetLonglineStubDto.class, SetLongline.class);
        dataBinderBuilder.put(SetLonglineStubDto.class, new SetLonglineStubDtoBinder());
    }

    @Override
    public void initSetLonglineTdrDto() {
        dataDtoToEntityTypeBuilder.put(SetLonglineTdrDto.class, SetLongline.class);
        dataBinderBuilder.put(SetLonglineTdrDto.class, new SetLonglineTdrDtoBinder());
    }

    @Override
    public void initSizeMeasureDto() {
        dataDtoToEntityTypeBuilder.put(SizeMeasureDto.class, SizeMeasure.class);
        dataBinderBuilder.put(SizeMeasureDto.class, new SizeMeasureBinder());
    }

    @Override
    public void initTdrDto() {
        dataDtoToEntityTypeBuilder.put(TdrDto.class, Tdr.class);
        dataBinderBuilder.put(TdrDto.class, new TdrBinder());
    }

    @Override
    public void initTripLonglineDto() {
        dataDtoToEntityTypeBuilder.put(TripLonglineDto.class, TripLongline.class);
        dataBinderBuilder.put(TripLonglineDto.class, new TripLonglineBinder());
    }

    @Override
    public void initTripLonglineActivityDto() {
        dataDtoToEntityTypeBuilder.put(TripLonglineActivityDto.class, ActivityLongline.class);
        dataBinderBuilder.put(TripLonglineActivityDto.class, new TripLonglineActivityDtoBinder());
    }

    @Override
    public void initTripLonglineGearUseDto() {
        dataDtoToEntityTypeBuilder.put(TripLonglineGearUseDto.class, TripLongline.class);
        dataBinderBuilder.put(TripLonglineGearUseDto.class, new TripLonglineGearUseDtoBinder());
    }

    @Override
    public void initWeightMeasureDto() {
        dataDtoToEntityTypeBuilder.put(WeightMeasureDto.class, WeightMeasure.class);
        dataBinderBuilder.put(WeightMeasureDto.class, new WeightMeasureBinder());
    }

    @Override
    public void initActivitySeineDto() {
        dataDtoToEntityTypeBuilder.put(ActivitySeineDto.class, ActivitySeine.class);
        dataBinderBuilder.put(ActivitySeineDto.class, new ActivitySeineBinder());
    }

    @Override
    public void initActivitySeineObservedSystemDto() {
        dataDtoToEntityTypeBuilder.put(ActivitySeineObservedSystemDto.class, ActivitySeine.class);
        dataBinderBuilder.put(ActivitySeineObservedSystemDto.class, new ActivitySeineObservedSystemDtoBinder());
    }

    @Override
    public void initActivitySeineStubDto() {
        dataDtoToEntityTypeBuilder.put(ActivitySeineStubDto.class, ActivitySeine.class);
        dataBinderBuilder.put(ActivitySeineStubDto.class, new ActivitySeineStubDtoBinder());
    }

    @Override
    public void initFloatingObjectDto() {
        dataDtoToEntityTypeBuilder.put(FloatingObjectDto.class, FloatingObject.class);
        dataBinderBuilder.put(FloatingObjectDto.class, new FloatingObjectBinder());
    }

    @Override
    public void initFloatingObjectObservedSpeciesDto() {
        dataDtoToEntityTypeBuilder.put(FloatingObjectObservedSpeciesDto.class, FloatingObject.class);
        dataBinderBuilder.put(FloatingObjectObservedSpeciesDto.class, new FloatingObjectObservedSpeciesDtoBinder());
    }

    @Override
    public void initFloatingObjectSchoolEstimateDto() {
        dataDtoToEntityTypeBuilder.put(FloatingObjectSchoolEstimateDto.class, FloatingObject.class);
        dataBinderBuilder.put(FloatingObjectSchoolEstimateDto.class, new FloatingObjectSchoolEstimateDtoBinder());
    }

    @Override
    public void initFloatingObjectTransmittingBuoyDto() {
        dataDtoToEntityTypeBuilder.put(FloatingObjectTransmittingBuoyDto.class, FloatingObject.class);
        dataBinderBuilder.put(FloatingObjectTransmittingBuoyDto.class, new FloatingObjectTransmittingBuoyDtoBinder());
    }

    @Override
    public void initGearUseFeaturesMeasurementSeineDto() {
        dataDtoToEntityTypeBuilder.put(GearUseFeaturesMeasurementSeineDto.class, GearUseFeaturesMeasurementSeine.class);
        dataBinderBuilder.put(GearUseFeaturesMeasurementSeineDto.class, new GearUseFeaturesMeasurementSeineBinder());
    }

    @Override
    public void initGearUseFeaturesSeineDto() {
        dataDtoToEntityTypeBuilder.put(GearUseFeaturesSeineDto.class, GearUseFeaturesSeine.class);
        dataBinderBuilder.put(GearUseFeaturesSeineDto.class, new GearUseFeaturesSeineBinder());
    }

    @Override
    public void initNonTargetCatchDto() {
        dataDtoToEntityTypeBuilder.put(NonTargetCatchDto.class, NonTargetCatch.class);
        dataBinderBuilder.put(NonTargetCatchDto.class, new NonTargetCatchBinder());
    }

    @Override
    public void initNonTargetLengthDto() {
        dataDtoToEntityTypeBuilder.put(NonTargetLengthDto.class, NonTargetLength.class);
        dataBinderBuilder.put(NonTargetLengthDto.class, new NonTargetLengthBinder());
    }

    @Override
    public void initNonTargetSampleDto() {
        dataDtoToEntityTypeBuilder.put(NonTargetSampleDto.class, NonTargetSample.class);
        dataBinderBuilder.put(NonTargetSampleDto.class, new NonTargetSampleBinder());
    }

    @Override
    public void initObjectObservedSpeciesDto() {
        dataDtoToEntityTypeBuilder.put(ObjectObservedSpeciesDto.class, ObjectObservedSpecies.class);
        dataBinderBuilder.put(ObjectObservedSpeciesDto.class, new ObjectObservedSpeciesBinder());
    }

    @Override
    public void initObjectSchoolEstimateDto() {
        dataDtoToEntityTypeBuilder.put(ObjectSchoolEstimateDto.class, ObjectSchoolEstimate.class);
        dataBinderBuilder.put(ObjectSchoolEstimateDto.class, new ObjectSchoolEstimateBinder());
    }

    @Override
    public void initRouteDto() {
        dataDtoToEntityTypeBuilder.put(RouteDto.class, Route.class);
        dataBinderBuilder.put(RouteDto.class, new RouteBinder());
    }

    @Override
    public void initRouteStubDto() {
        dataDtoToEntityTypeBuilder.put(RouteStubDto.class, Route.class);
        dataBinderBuilder.put(RouteStubDto.class, new RouteStubDtoBinder());
    }

    @Override
    public void initSchoolEstimateDto() {
        dataDtoToEntityTypeBuilder.put(SchoolEstimateDto.class, SchoolEstimate.class);
        dataBinderBuilder.put(SchoolEstimateDto.class, new SchoolEstimateBinder());
    }

    @Override
    public void initSetSeineDto() {
        dataDtoToEntityTypeBuilder.put(SetSeineDto.class, SetSeine.class);
        dataBinderBuilder.put(SetSeineDto.class, new SetSeineBinder());
    }

    @Override
    public void initSetSeineNonTargetCatchDto() {
        dataDtoToEntityTypeBuilder.put(SetSeineNonTargetCatchDto.class, SetSeine.class);
        dataBinderBuilder.put(SetSeineNonTargetCatchDto.class, new SetSeineNonTargetCatchDtoBinder());
    }

    @Override
    public void initSetSeineSchoolEstimateDto() {
        dataDtoToEntityTypeBuilder.put(SetSeineSchoolEstimateDto.class, SetSeine.class);
        dataBinderBuilder.put(SetSeineSchoolEstimateDto.class, new SetSeineSchoolEstimateDtoBinder());
    }

    @Override
    public void initSetSeineTargetCatchDto() {
        dataDtoToEntityTypeBuilder.put(SetSeineTargetCatchDto.class, SetSeine.class);
        dataBinderBuilder.put(SetSeineTargetCatchDto.class, new SetSeineTargetCatchDtoBinder());
    }

    @Override
    public void initTargetCatchDto() {
        dataDtoToEntityTypeBuilder.put(TargetCatchDto.class, TargetCatch.class);
        dataBinderBuilder.put(TargetCatchDto.class, new TargetCatchBinder());
    }

    @Override
    public void initTargetLengthDto() {
        dataDtoToEntityTypeBuilder.put(TargetLengthDto.class, TargetLength.class);
        dataBinderBuilder.put(TargetLengthDto.class, new TargetLengthBinder());
    }

    @Override
    public void initTargetSampleDto() {
        dataDtoToEntityTypeBuilder.put(TargetSampleDto.class, TargetSample.class);
        dataBinderBuilder.put(TargetSampleDto.class, new TargetSampleBinder());
    }

    @Override
    public void initTransmittingBuoyDto() {
        dataDtoToEntityTypeBuilder.put(TransmittingBuoyDto.class, TransmittingBuoy.class);
        dataBinderBuilder.put(TransmittingBuoyDto.class, new TransmittingBuoyBinder());
    }

    @Override
    public void initTripSeineDto() {
        dataDtoToEntityTypeBuilder.put(TripSeineDto.class, TripSeine.class);
        dataBinderBuilder.put(TripSeineDto.class, new TripSeineBinder());
    }

    @Override
    public void initTripSeineGearUseDto() {
        dataDtoToEntityTypeBuilder.put(TripSeineGearUseDto.class, TripSeine.class);
        dataBinderBuilder.put(TripSeineGearUseDto.class, new TripSeineGearUseDtoBinder());
    }

    @Override
    public void initCountryDto() {
        referentialDtoToEntityTypeBuilder.put(CountryDto.class, Country.class);
        referentialBinderBuilder.put(CountryDto.class, new CountryBinder());
    }

    @Override
    public void initFpaZoneDto() {
        referentialDtoToEntityTypeBuilder.put(FpaZoneDto.class, FpaZone.class);
        referentialBinderBuilder.put(FpaZoneDto.class, new FpaZoneBinder());
    }

    @Override
    public void initGearDto() {
        referentialDtoToEntityTypeBuilder.put(GearDto.class, Gear.class);
        referentialBinderBuilder.put(GearDto.class, new GearBinder());
    }

    @Override
    public void initGearCaracteristicDto() {
        referentialDtoToEntityTypeBuilder.put(GearCaracteristicDto.class, GearCaracteristic.class);
        referentialBinderBuilder.put(GearCaracteristicDto.class, new GearCaracteristicBinder());
    }

    @Override
    public void initGearCaracteristicTypeDto() {
        referentialDtoToEntityTypeBuilder.put(GearCaracteristicTypeDto.class, GearCaracteristicType.class);
        referentialBinderBuilder.put(GearCaracteristicTypeDto.class, new GearCaracteristicTypeBinder());
    }

    @Override
    public void initHarbourDto() {
        referentialDtoToEntityTypeBuilder.put(HarbourDto.class, Harbour.class);
        referentialBinderBuilder.put(HarbourDto.class, new HarbourBinder());
    }

    @Override
    public void initLengthWeightParameterDto() {
        referentialDtoToEntityTypeBuilder.put(LengthWeightParameterDto.class, LengthWeightParameter.class);
        referentialBinderBuilder.put(LengthWeightParameterDto.class, new LengthWeightParameterBinder());
    }

    @Override
    public void initOceanDto() {
        referentialDtoToEntityTypeBuilder.put(OceanDto.class, Ocean.class);
        referentialBinderBuilder.put(OceanDto.class, new OceanBinder());
    }

    @Override
    public void initOrganismDto() {
        referentialDtoToEntityTypeBuilder.put(OrganismDto.class, Organism.class);
        referentialBinderBuilder.put(OrganismDto.class, new OrganismBinder());
    }

    @Override
    public void initPersonDto() {
        referentialDtoToEntityTypeBuilder.put(PersonDto.class, Person.class);
        referentialBinderBuilder.put(PersonDto.class, new PersonBinder());
    }

    @Override
    public void initProgramDto() {
        referentialDtoToEntityTypeBuilder.put(ProgramDto.class, Program.class);
        referentialBinderBuilder.put(ProgramDto.class, new ProgramBinder());
    }

    @Override
    public void initSexDto() {
        referentialDtoToEntityTypeBuilder.put(SexDto.class, Sex.class);
        referentialBinderBuilder.put(SexDto.class, new SexBinder());
    }

    @Override
    public void initSpeciesDto() {
        referentialDtoToEntityTypeBuilder.put(SpeciesDto.class, Species.class);
        referentialBinderBuilder.put(SpeciesDto.class, new SpeciesBinder());
    }

    @Override
    public void initSpeciesGroupDto() {
        referentialDtoToEntityTypeBuilder.put(SpeciesGroupDto.class, SpeciesGroup.class);
        referentialBinderBuilder.put(SpeciesGroupDto.class, new SpeciesGroupBinder());
    }

    @Override
    public void initSpeciesListDto() {
        referentialDtoToEntityTypeBuilder.put(SpeciesListDto.class, SpeciesList.class);
        referentialBinderBuilder.put(SpeciesListDto.class, new SpeciesListBinder());
    }

    @Override
    public void initVesselDto() {
        referentialDtoToEntityTypeBuilder.put(VesselDto.class, Vessel.class);
        referentialBinderBuilder.put(VesselDto.class, new VesselBinder());
    }

    @Override
    public void initVesselSizeCategoryDto() {
        referentialDtoToEntityTypeBuilder.put(VesselSizeCategoryDto.class, VesselSizeCategory.class);
        referentialBinderBuilder.put(VesselSizeCategoryDto.class, new VesselSizeCategoryBinder());
    }

    @Override
    public void initVesselTypeDto() {
        referentialDtoToEntityTypeBuilder.put(VesselTypeDto.class, VesselType.class);
        referentialBinderBuilder.put(VesselTypeDto.class, new VesselTypeBinder());
    }

    @Override
    public void initBaitHaulingStatusDto() {
        referentialDtoToEntityTypeBuilder.put(BaitHaulingStatusDto.class, BaitHaulingStatus.class);
        referentialBinderBuilder.put(BaitHaulingStatusDto.class, new BaitHaulingStatusBinder());
    }

    @Override
    public void initBaitSettingStatusDto() {
        referentialDtoToEntityTypeBuilder.put(BaitSettingStatusDto.class, BaitSettingStatus.class);
        referentialBinderBuilder.put(BaitSettingStatusDto.class, new BaitSettingStatusBinder());
    }

    @Override
    public void initBaitTypeDto() {
        referentialDtoToEntityTypeBuilder.put(BaitTypeDto.class, BaitType.class);
        referentialBinderBuilder.put(BaitTypeDto.class, new BaitTypeBinder());
    }

    @Override
    public void initCatchFateLonglineDto() {
        referentialDtoToEntityTypeBuilder.put(CatchFateLonglineDto.class, CatchFateLongline.class);
        referentialBinderBuilder.put(CatchFateLonglineDto.class, new CatchFateLonglineBinder());
    }

    @Override
    public void initEncounterTypeDto() {
        referentialDtoToEntityTypeBuilder.put(EncounterTypeDto.class, EncounterType.class);
        referentialBinderBuilder.put(EncounterTypeDto.class, new EncounterTypeBinder());
    }

    @Override
    public void initHealthnessDto() {
        referentialDtoToEntityTypeBuilder.put(HealthnessDto.class, Healthness.class);
        referentialBinderBuilder.put(HealthnessDto.class, new HealthnessBinder());
    }

    @Override
    public void initHookPositionDto() {
        referentialDtoToEntityTypeBuilder.put(HookPositionDto.class, HookPosition.class);
        referentialBinderBuilder.put(HookPositionDto.class, new HookPositionBinder());
    }

    @Override
    public void initHookSizeDto() {
        referentialDtoToEntityTypeBuilder.put(HookSizeDto.class, HookSize.class);
        referentialBinderBuilder.put(HookSizeDto.class, new HookSizeBinder());
    }

    @Override
    public void initHookTypeDto() {
        referentialDtoToEntityTypeBuilder.put(HookTypeDto.class, HookType.class);
        referentialBinderBuilder.put(HookTypeDto.class, new HookTypeBinder());
    }

    @Override
    public void initItemHorizontalPositionDto() {
        referentialDtoToEntityTypeBuilder.put(ItemHorizontalPositionDto.class, ItemHorizontalPosition.class);
        referentialBinderBuilder.put(ItemHorizontalPositionDto.class, new ItemHorizontalPositionBinder());
    }

    @Override
    public void initItemVerticalPositionDto() {
        referentialDtoToEntityTypeBuilder.put(ItemVerticalPositionDto.class, ItemVerticalPosition.class);
        referentialBinderBuilder.put(ItemVerticalPositionDto.class, new ItemVerticalPositionBinder());
    }

    @Override
    public void initLightsticksColorDto() {
        referentialDtoToEntityTypeBuilder.put(LightsticksColorDto.class, LightsticksColor.class);
        referentialBinderBuilder.put(LightsticksColorDto.class, new LightsticksColorBinder());
    }

    @Override
    public void initLightsticksTypeDto() {
        referentialDtoToEntityTypeBuilder.put(LightsticksTypeDto.class, LightsticksType.class);
        referentialBinderBuilder.put(LightsticksTypeDto.class, new LightsticksTypeBinder());
    }

    @Override
    public void initLineTypeDto() {
        referentialDtoToEntityTypeBuilder.put(LineTypeDto.class, LineType.class);
        referentialBinderBuilder.put(LineTypeDto.class, new LineTypeBinder());
    }

    @Override
    public void initMaturityStatusDto() {
        referentialDtoToEntityTypeBuilder.put(MaturityStatusDto.class, MaturityStatus.class);
        referentialBinderBuilder.put(MaturityStatusDto.class, new MaturityStatusBinder());
    }

    @Override
    public void initMitigationTypeDto() {
        referentialDtoToEntityTypeBuilder.put(MitigationTypeDto.class, MitigationType.class);
        referentialBinderBuilder.put(MitigationTypeDto.class, new MitigationTypeBinder());
    }

    @Override
    public void initSensorBrandDto() {
        referentialDtoToEntityTypeBuilder.put(SensorBrandDto.class, SensorBrand.class);
        referentialBinderBuilder.put(SensorBrandDto.class, new SensorBrandBinder());
    }

    @Override
    public void initSensorDataFormatDto() {
        referentialDtoToEntityTypeBuilder.put(SensorDataFormatDto.class, SensorDataFormat.class);
        referentialBinderBuilder.put(SensorDataFormatDto.class, new SensorDataFormatBinder());
    }

    @Override
    public void initSensorTypeDto() {
        referentialDtoToEntityTypeBuilder.put(SensorTypeDto.class, SensorType.class);
        referentialBinderBuilder.put(SensorTypeDto.class, new SensorTypeBinder());
    }

    @Override
    public void initSettingShapeDto() {
        referentialDtoToEntityTypeBuilder.put(SettingShapeDto.class, SettingShape.class);
        referentialBinderBuilder.put(SettingShapeDto.class, new SettingShapeBinder());
    }

    @Override
    public void initSizeMeasureTypeDto() {
        referentialDtoToEntityTypeBuilder.put(SizeMeasureTypeDto.class, SizeMeasureType.class);
        referentialBinderBuilder.put(SizeMeasureTypeDto.class, new SizeMeasureTypeBinder());
    }

    @Override
    public void initStomacFullnessDto() {
        referentialDtoToEntityTypeBuilder.put(StomacFullnessDto.class, StomacFullness.class);
        referentialBinderBuilder.put(StomacFullnessDto.class, new StomacFullnessBinder());
    }

    @Override
    public void initTripTypeDto() {
        referentialDtoToEntityTypeBuilder.put(TripTypeDto.class, TripType.class);
        referentialBinderBuilder.put(TripTypeDto.class, new TripTypeBinder());
    }

    @Override
    public void initVesselActivityLonglineDto() {
        referentialDtoToEntityTypeBuilder.put(VesselActivityLonglineDto.class, VesselActivityLongline.class);
        referentialBinderBuilder.put(VesselActivityLonglineDto.class, new VesselActivityLonglineBinder());
    }

    @Override
    public void initWeightMeasureTypeDto() {
        referentialDtoToEntityTypeBuilder.put(WeightMeasureTypeDto.class, WeightMeasureType.class);
        referentialBinderBuilder.put(WeightMeasureTypeDto.class, new WeightMeasureTypeBinder());
    }

    @Override
    public void initDetectionModeDto() {
        referentialDtoToEntityTypeBuilder.put(DetectionModeDto.class, DetectionMode.class);
        referentialBinderBuilder.put(DetectionModeDto.class, new DetectionModeBinder());
    }

    @Override
    public void initObjectFateDto() {
        referentialDtoToEntityTypeBuilder.put(ObjectFateDto.class, ObjectFate.class);
        referentialBinderBuilder.put(ObjectFateDto.class, new ObjectFateBinder());
    }

    @Override
    public void initObjectOperationDto() {
        referentialDtoToEntityTypeBuilder.put(ObjectOperationDto.class, ObjectOperation.class);
        referentialBinderBuilder.put(ObjectOperationDto.class, new ObjectOperationBinder());
    }

    @Override
    public void initObjectTypeDto() {
        referentialDtoToEntityTypeBuilder.put(ObjectTypeDto.class, ObjectType.class);
        referentialBinderBuilder.put(ObjectTypeDto.class, new ObjectTypeBinder());
    }

    @Override
    public void initObservedSystemDto() {
        referentialDtoToEntityTypeBuilder.put(ObservedSystemDto.class, ObservedSystem.class);
        referentialBinderBuilder.put(ObservedSystemDto.class, new ObservedSystemBinder());
    }

    @Override
    public void initReasonForDiscardDto() {
        referentialDtoToEntityTypeBuilder.put(ReasonForDiscardDto.class, ReasonForDiscard.class);
        referentialBinderBuilder.put(ReasonForDiscardDto.class, new ReasonForDiscardBinder());
    }

    @Override
    public void initReasonForNoFishingDto() {
        referentialDtoToEntityTypeBuilder.put(ReasonForNoFishingDto.class, ReasonForNoFishing.class);
        referentialBinderBuilder.put(ReasonForNoFishingDto.class, new ReasonForNoFishingBinder());
    }

    @Override
    public void initReasonForNullSetDto() {
        referentialDtoToEntityTypeBuilder.put(ReasonForNullSetDto.class, ReasonForNullSet.class);
        referentialBinderBuilder.put(ReasonForNullSetDto.class, new ReasonForNullSetBinder());
    }

    @Override
    public void initSpeciesFateDto() {
        referentialDtoToEntityTypeBuilder.put(SpeciesFateDto.class, SpeciesFate.class);
        referentialBinderBuilder.put(SpeciesFateDto.class, new SpeciesFateBinder());
    }

    @Override
    public void initSpeciesStatusDto() {
        referentialDtoToEntityTypeBuilder.put(SpeciesStatusDto.class, SpeciesStatus.class);
        referentialBinderBuilder.put(SpeciesStatusDto.class, new SpeciesStatusBinder());
    }

    @Override
    public void initSurroundingActivityDto() {
        referentialDtoToEntityTypeBuilder.put(SurroundingActivityDto.class, SurroundingActivity.class);
        referentialBinderBuilder.put(SurroundingActivityDto.class, new SurroundingActivityBinder());
    }

    @Override
    public void initTransmittingBuoyOperationDto() {
        referentialDtoToEntityTypeBuilder.put(TransmittingBuoyOperationDto.class, TransmittingBuoyOperation.class);
        referentialBinderBuilder.put(TransmittingBuoyOperationDto.class, new TransmittingBuoyOperationBinder());
    }

    @Override
    public void initTransmittingBuoyTypeDto() {
        referentialDtoToEntityTypeBuilder.put(TransmittingBuoyTypeDto.class, TransmittingBuoyType.class);
        referentialBinderBuilder.put(TransmittingBuoyTypeDto.class, new TransmittingBuoyTypeBinder());
    }

    @Override
    public void initVesselActivitySeineDto() {
        referentialDtoToEntityTypeBuilder.put(VesselActivitySeineDto.class, VesselActivitySeine.class);
        referentialBinderBuilder.put(VesselActivitySeineDto.class, new VesselActivitySeineBinder());
    }

    @Override
    public void initWeightCategoryDto() {
        referentialDtoToEntityTypeBuilder.put(WeightCategoryDto.class, WeightCategory.class);
        referentialBinderBuilder.put(WeightCategoryDto.class, new WeightCategoryBinder());
    }

    @Override
    public void initWindDto() {
        referentialDtoToEntityTypeBuilder.put(WindDto.class, Wind.class);
        referentialBinderBuilder.put(WindDto.class, new WindBinder());
    }

}
