package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.GearUseFeaturesSeine;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TripSeineGearUseDtoBinder extends DataBinderSupport<TripSeine, TripSeineGearUseDto> {

    public TripSeineGearUseDtoBinder() {
        super(TripSeine.class, TripSeineGearUseDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TripSeineGearUseDto dto, TripSeine entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setGearUseFeaturesSeine(toEntityCollection(referentialLocale, dto.getGearUseFeaturesSeine(), GearUseFeaturesSeine.class, entity.getGearUseFeaturesSeine()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, TripSeine entity, TripSeineGearUseDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setGearUseFeaturesSeine(toLinkedHashSetData(referentialLocale, entity.getGearUseFeaturesSeine(), GearUseFeaturesSeineDto.class));

    }
}
