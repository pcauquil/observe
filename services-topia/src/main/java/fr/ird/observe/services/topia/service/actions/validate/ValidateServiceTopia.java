package fr.ird.observe.services.topia.service.actions.validate;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.Trip;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.actions.validate.ValidateDataRequest;
import fr.ird.observe.services.service.actions.validate.ValidateDataResult;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsRequest;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsResult;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDto;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtoType;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import fr.ird.observe.services.service.actions.validate.ValidationMessage;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;
import org.nuiton.validator.xwork2.XWork2ValidatorUtil;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ValidateServiceTopia extends ObserveServiceTopia implements ValidateService {

    private static final Log log = LogFactory.getLog(ValidateServiceTopia.class);
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);

    @Override
    public ValidateReferentialsResult validateReferentials(ValidateReferentialsRequest request) {

        Future<ValidateReferentialsResult> future = EXECUTOR_SERVICE.submit(() -> {

            ValidationDataContext validationDataContext = new ValidationDataContext();

            initValidationEngine(validationDataContext);

            ValidatorsMap validators = getValidators(
                    request.getValidationContext(),
                    request.getScopes(),
                    Entities.REFERENCE_ENTITIES);

            ReferentialLocale referenceLocale = getReferentialLocale();

            ValidationMessageDetector detector = new ValidationMessageDetector(validators, validationDataContext, referenceLocale);

            for (Class<? extends ReferentialDto> referentialType : request.getReferentialTypes()) {

                validateReferential(referentialType, detector);

            }

            ImmutableMap validateResultForDtoTypeMap = buildResultForDtoTypes(detector);
            return new ValidateReferentialsResult(request, validateResultForDtoTypeMap);
        });

        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ServiceValidationException(e);
        }
    }

    @Override
    public ValidateDataResult validateData(ValidateDataRequest request) {

        Future<ValidateDataResult> future = EXECUTOR_SERVICE.submit(() -> {

            ValidationDataContext validationDataContext = new ValidationDataContext();
            initValidationEngine(validationDataContext);

            ValidatorsMap validators = getValidators(
                    request.getValidationContext(),
                    request.getScopes(),
                    Entities.DATA_ENTITIES);
            
            ReferentialLocale referenceLocale = getReferentialLocale();

            initValidationEngine(validationDataContext);

            ValidationMessageDetector detector = new ValidationMessageDetector(validators, validationDataContext, referenceLocale);

            for (String dataId : request.getDataIds()) {

                validateData(dataId, detector);

            }

            ImmutableMap<Class<? extends IdDto>, ValidateResultForDtoType> validateResultForDtoTypeMap = buildResultForDtoTypes(detector);
            return new ValidateDataResult(request, validateResultForDtoTypeMap);
        });

        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ServiceValidationException(e);
        }

    }

    private ImmutableMap<Class<? extends IdDto>, ValidateResultForDtoType> buildResultForDtoTypes(ValidationMessageDetector detector) {

        ImmutableMap.Builder<Class<? extends IdDto>, ValidateResultForDtoType> forDtoTypeBuilder = new ImmutableMap.Builder<>();

        for (Class<? extends TopiaEntity> entityType : detector.getDetectedEntityTypes()) {

            if (ObserveReferentialEntity.class.isAssignableFrom(entityType)) {
                buildResultForReferentialDtoType((Class) entityType, detector, forDtoTypeBuilder);
            } else {
                buildResultForDataDtoType((Class) entityType, detector, forDtoTypeBuilder);
            }

        }

        return forDtoTypeBuilder.build();

    }

    private <E extends ObserveReferentialEntity, D extends ReferentialDto> void buildResultForReferentialDtoType(Class<E> entityType,
                                                                                                                 ValidationMessageDetector detector,
                                                                                                                 ImmutableMap.Builder<Class<? extends IdDto>, ValidateResultForDtoType> forDtoTypeBuilder) {

        ImmutableSet.Builder<ValidateResultForDto<D>> validateResultForDtoMapBuilder = new ImmutableSet.Builder<>();

        Map<TopiaEntity, Collection<ValidationMessage>> detectedMessages = detector.getDetectedMessages(entityType);
        for (Map.Entry<TopiaEntity, Collection<ValidationMessage>> entry : detectedMessages.entrySet()) {

            TopiaEntity entity = entry.getKey();
            ReferentialReference<D> referenceDto = BINDER_ENGINE.transformEntityToReferentialReferenceDto(serviceContext.getReferentialLocale(), (ObserveReferentialEntity) entity);
            Collection<ValidationMessage> validationMessages = entry.getValue();
            ValidateResultForDto<D> validateResultForDto = new ValidateResultForDto<>(referenceDto, ImmutableSet.copyOf(validationMessages));
            validateResultForDtoMapBuilder.add(validateResultForDto);

        }

        ValidateResultForDtoType<D> result = new ValidateResultForDtoType<>(validateResultForDtoMapBuilder.build());

        Class<D> dtoType = BINDER_ENGINE.getReferentialDtoType(entityType);
        forDtoTypeBuilder.put(dtoType, result);

    }

    private <E extends ObserveDataEntity, D extends DataDto> void buildResultForDataDtoType(Class<E> entityType,
                                                                                            ValidationMessageDetector detector,
                                                                                            ImmutableMap.Builder<Class<? extends IdDto>, ValidateResultForDtoType> forDtoTypeBuilder) {

        ImmutableSet.Builder<ValidateResultForDto<D>> validateResultForDtoMapBuilder = new ImmutableSet.Builder<>();

        Map<TopiaEntity, Collection<ValidationMessage>> detectedMessages = detector.getDetectedMessages(entityType);
        for (Map.Entry<TopiaEntity, Collection<ValidationMessage>> entry : detectedMessages.entrySet()) {

            ObserveDataEntity entity = (ObserveDataEntity) entry.getKey();
            DataReference<D> referenceDto = BINDER_ENGINE.transformEntityToDataReferenceDto(serviceContext.getReferentialLocale(), entity);
            Collection<ValidationMessage> validationMessages = entry.getValue();
            ValidateResultForDto<D> validateResultForDto = new ValidateResultForDto<>(referenceDto, ImmutableSet.copyOf(validationMessages));
            validateResultForDtoMapBuilder.add(validateResultForDto);

        }

        ValidateResultForDtoType<D> result = new ValidateResultForDtoType<>(validateResultForDtoMapBuilder.build());

        Class<D> dtoType = BINDER_ENGINE.getDataDtoType(entityType);
        forDtoTypeBuilder.put(dtoType, result);

    }

    private <D extends ReferentialDto, E extends ObserveReferentialEntity> void validateReferential(Class<D> referentialDtoType, ValidationMessageDetector detector) {

        Class<E> referentialType = getReferentialEntityType(referentialDtoType);

        if (log.isInfoEnabled()) {
            log.info("Validate referential type: " + referentialType.getName());
        }

        for (TopiaEntity entity : loadEntities(referentialType)) {

            detector.detectValidationMessages(entity);

        }

    }

    private void validateData(String dataId, ValidationMessageDetector detector) {

        Trip trip;

        if (Entities.isSeineId(dataId)) {

            trip = loadEntity(TripSeineDto.class, dataId);

        } else {

            trip = loadEntity(TripLonglineDto.class, dataId);

        }

        detector.detectValidationMessages(trip);

    }

    /**
     * Obtenir le dictionnaire des validateurs pour les types d'entités donnés.
     *
     * @param context   le context de validation
     * @param scopes    les scopes autorisés
     * @param beanTypes types des entités
     * @return le dictionnaire des validateurs par type d'entité.
     */
    private ValidatorsMap getValidators(String context, Set<NuitonValidatorScope> scopes, ObserveEntityEnum... beanTypes) {

        ValidatorsMap result = new ValidatorsMap();

        for (ObserveEntityEnum type : beanTypes) {
            // on cherche le validateur
            SimpleBeanValidator<?> validator = getValidator(context, scopes, type.getContract());
            if (validator != null) {
                // on enregistre le validateur pour le contrat
                result.put(type.getContract(), validator);
                // mais aussi pour l'implantation afin de ne pas à avoir à toujours revenir sur le contrat
                result.put(type.getImplementation(), validator);
            }
        }
        return result;

    }

    /**
     * Obtenir le validateur d'un type objet
     *
     * @param <B>     type de l'objet à valider
     * @param context le nom du context de validation
     * @param scopes  les scopes autorisés
     * @param klass   type de l'objet à valider
     * @return le validateur trouvé ou {@code null}
     */
    private <B extends TopiaEntity> SimpleBeanValidator<B> getValidator(String context,
                                                                        Set<NuitonValidatorScope> scopes,
                                                                        Class<B> klass) {

        SimpleBeanValidator<B> valitator = SimpleBeanValidator.newValidator(klass,
                                                                            context,
                                                                            Iterables.toArray(scopes, NuitonValidatorScope.class));

        Set<NuitonValidatorScope> effectiveScopes = valitator.getEffectiveScopes();
        if (effectiveScopes.isEmpty()) {
            valitator = null;
            if (log.isDebugEnabled()) {
                log.debug(klass + " : validator skip (no scopes detected)");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug(klass + " : keep validator " + valitator);
            }
        }
        return valitator;

    }

    private void initValidationEngine(ValidationDataContext validationDataContext) {

        ValueStack valueStack = XWork2ValidatorUtil.createValuestack();
        valueStack.push(validationDataContext);
        ActionContext context = new ActionContext(valueStack.getContext());
        ActionContext.setContext(context);

    }
}
