package fr.ird.observe.services.topia.service.longline;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.LonglinePositionAwareDto;
import fr.ird.observe.services.dto.longline.LonglinePositionSetDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.reference.DataReferenceSetDefinitions;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class LonglinePositionSetDtoHelper {

    // constitution des références des position sur la ligne
    public static void loadPositionSet(SetLongline setLongline, LonglinePositionSetDto target) {

        for (Section section : setLongline.getSection()) {

            DataReference<SectionDto> sectionRef = toSectionReference(section);

            target.addSections(sectionRef);

            for (Basket basket : section.getBasket()) {

                DataReference<BasketDto> basketRef = toBasketReference(basket, section);

                target.addBaskets(basketRef);

                for (Branchline branchline : basket.getBranchline()) {

                    DataReference<BranchlineDto> branchlineRef = toBranchlineReference(branchline, basket);

                    target.addBranchlines(branchlineRef);

                }

            }

        }

    }

    // remplacement des références de position  avec ces référence complété avec l'id du parent
    public static void updatePosition(LonglinePositionSetDto positionSetDto, LonglinePositionAwareDto positionDto) {
        DataReference<BasketDto> basketRef = positionDto.getBasket();

        if (basketRef != null) {

            DataReference<BasketDto> basketRefWithSection = positionSetDto.getBaskets().stream()
                                                                          .filter(b -> basketRef.getId().equals(b.getId()))
                                                                          .findFirst()
                                                                          .get();

            positionDto.setBasket(basketRefWithSection);

        }

        DataReference<BranchlineDto> branchlineRef = positionDto.getBranchline();

        if (branchlineRef != null) {

            DataReference<BranchlineDto> branchlineRefWithSection = positionSetDto.getBranchlines().stream()
                                                                                  .filter(b -> branchlineRef.getId().equals(b.getId()))
                                                                                  .findFirst()
                                                                                  .get();

            positionDto.setBranchline(branchlineRefWithSection);

        }
    }

    static protected DataReference<SectionDto> toSectionReference(Section section) {

        DataReference<SectionDto> reference = new DataReference<>();

        reference.setId(section.getTopiaId());
        reference.setCreateDate(section.getTopiaCreateDate());
        reference.setVersion(section.getTopiaVersion());

        ReferenceSetDefinition<SectionDto> definition = DataReferenceSetDefinitions.SECTION.getDefinition();
        reference.init(definition.getType(),
                       definition.getPropertyNames(),
                       section.getSettingIdentifier(),
                       section.getHaulingIdentifier());

        return reference;
    }

    static protected DataReference<BasketDto> toBasketReference(Basket basket, Section section) {

        DataReference<BasketDto> reference = new DataReference<>();

        reference.setId(basket.getTopiaId());
        reference.setCreateDate(basket.getTopiaCreateDate());
        reference.setVersion(basket.getTopiaVersion());

        ReferenceSetDefinition<BasketDto> definition = DataReferenceSetDefinitions.BASKET_WITH_SECTION.getDefinition();
        reference.init(definition.getType(),
                       definition.getPropertyNames(),
                       basket.getSettingIdentifier(),
                       basket.getHaulingIdentifier(),
                       section.getTopiaId());

        return reference;
    }

    static protected DataReference<BranchlineDto> toBranchlineReference(Branchline branchline, Basket basket) {

        DataReference<BranchlineDto> reference = new DataReference<>();

        reference.setId(branchline.getTopiaId());
        reference.setCreateDate(branchline.getTopiaCreateDate());
        reference.setVersion(branchline.getTopiaVersion());

        ReferenceSetDefinition<BranchlineDto> definition = DataReferenceSetDefinitions.BRANCHLINE_WITH_BASKET.getDefinition();
        reference.init(definition.getType(),
                       definition.getPropertyNames(),
                       branchline.getSettingIdentifier(),
                       branchline.getHaulingIdentifier(),
                       basket.getTopiaId());

        return reference;
    }

}
