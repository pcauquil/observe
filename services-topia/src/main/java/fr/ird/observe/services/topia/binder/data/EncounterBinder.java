package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Encounter;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.longline.EncounterType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class EncounterBinder extends DataBinderSupport<Encounter, EncounterDto> {

    public EncounterBinder() {
        super(Encounter.class, EncounterDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, EncounterDto dto, Encounter entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setEncounterType(toEntity(dto.getEncounterType(), EncounterType.class));
        entity.setDistance(dto.getDistance());
        entity.setCount(dto.getCount());


    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Encounter entity, EncounterDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setEncounterType(toReferentialReference(referentialLocale, entity.getEncounterType(), EncounterTypeDto.class));
        dto.setDistance(entity.getDistance());
        dto.setCount(entity.getCount());

    }

    @Override
    public DataReference<EncounterDto> toDataReference(ReferentialLocale referentialLocale, Encounter entity) {

        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getSpecies()),
                               getLabel(referentialLocale, entity.getEncounterType()));

    }

    @Override
    public DataReference<EncounterDto> toDataReference(ReferentialLocale referentialLocale, EncounterDto dto) {

        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getSpecies()),
                               getLabel(referentialLocale, dto.getEncounterType()));

    }
}
