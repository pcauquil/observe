package fr.ird.observe.services.topia.service.actions.synchro.referential.ng;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeRequest;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeService;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeSqlsRequest;
import fr.ird.observe.services.topia.ObserveServiceContextTopia;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.TimeLog;

/**
 * Created on 08/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchronizeServiceTopia extends ObserveServiceTopia implements ReferentialSynchronizeService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialSynchronizeServiceTopia.class);
    private static final TimeLog timeLog = new TimeLog(ReferentialSynchronizeServiceTopia.class);

    @Override
    public void setServiceContext(ObserveServiceContextTopia serviceContext) {
        super.setServiceContext(serviceContext);
    }

    @Override
    public ReferentialSynchronizeSqlsRequest produceSqlsRequest(ReferentialSynchronizeRequest request) {

        long startTime = TimeLog.getTime();

        if (log.isTraceEnabled()) {
            log.trace("produceSqlsRequest(" + request + ")");
        }
        ReferentialSynchronizeSqlsRequest result = ReferentialSynchronizeSqlsRequestBuilder
                .builder(this, serviceContext.getTopiaApplicationContext().getMetadataModel(), request)
                .build();

        timeLog.log(startTime, "produceSqlsRequest");
        return result;

    }

    @Override
    public void executeSqlsRequests(ReferentialSynchronizeSqlsRequest request, ReferentialSynchronizeSqlsRequest oppositeSqlsRequest) {

        long startTime = TimeLog.getTime();

        if (log.isTraceEnabled()) {
            log.trace("executeSqlsRequests(" + request + ")");
        }

        byte[] insertSqlCode = oppositeSqlsRequest.getInsertSqlCode();
        byte[] updateSqlCode = oppositeSqlsRequest.getUpdateSqlCode();
        byte[] deleteSqlCode = request.getDeleteSqlCode();
        byte[] desactivateSqlCode = request.getDesactivateSqlCode();

        getTopiaPersistenceContext().executeSqlScripts(insertSqlCode, updateSqlCode, desactivateSqlCode, deleteSqlCode);

        timeLog.log(startTime, "produceSqlsRequest");

    }


}
