package fr.ird.observe.services.topia.binder.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Country;
import fr.ird.observe.entities.referentiel.Harbour;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class HarbourBinder extends ReferentialBinderSupport<Harbour, HarbourDto> {

    public HarbourBinder() {
        super(Harbour.class, HarbourDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, HarbourDto dto, Harbour entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        entity.setName(dto.getName());
        entity.setLocode(dto.getLocode());
        entity.setLatitude(dto.getLatitude());
        entity.setLongitude(dto.getLongitude());
        entity.setQuadrant(dto.getQuadrant());
        entity.setCountry(toEntity(dto.getCountry(), Country.class));


    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Harbour entity, HarbourDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        dto.setName(entity.getName());
        dto.setLocode(entity.getLocode());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        dto.setQuadrant(entity.getQuadrant());
        dto.setCountry(toReferentialReference(referentialLocale, entity.getCountry(), CountryDto.class));

    }

    @Override
    public ReferentialReference<HarbourDto> toReferentialReference(ReferentialLocale referentialLocale, Harbour entity) {

        return toReferentialReference(entity,
                                      entity.getCode(), entity.getName(), entity.getLocode());

    }

    @Override
    public ReferentialReference<HarbourDto> toReferentialReference(ReferentialLocale referentialLocale, HarbourDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(), dto.getName(), dto.getLocode());
    }
}
