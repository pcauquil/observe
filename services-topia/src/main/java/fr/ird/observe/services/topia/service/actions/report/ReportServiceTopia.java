package fr.ird.observe.services.topia.service.actions.report;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.AbstractObserveTopiaDao;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.ObserveEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.service.actions.report.ReportService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.topia.binder.data.DataBinderSupport;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportOperation;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import fr.ird.observe.services.dto.actions.report.ReportVariable;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.awt.Dimension;
import java.awt.Point;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ReportServiceTopia extends ObserveServiceTopia implements ReportService {

    private static final Log log = LogFactory.getLog(ReportServiceTopia.class);

    @Override
    public Report populateVariables(Report report, String tripId) {

        Map<String, Object> vars = new TreeMap<>();

        vars.put(ReportRequest.TRIP_ID_VARIABLE, tripId);

        for (ReportVariable variable : report.getVariables()) {

            LinkedHashSet<Object> values = new LinkedHashSet<>();

            String hql = variable.getRequest();

            Class<?> type = variable.getType();

            if (ReferentialDto.class.isAssignableFrom(type)) {

                Class<? extends ReferentialDto> dtoType = (Class<? extends ReferentialDto>) type;

                ReferentialBinderSupport<ObserveReferentialEntity, ? extends ReferentialDto> binder = getReferentialBinder(dtoType);
                List<ObserveEntity> universe = executeRequest(hql, vars);

                for (TopiaEntity entity : universe) {

                    ReferentialReference<? extends ReferentialDto> reference = binder.toReferentialReference(getReferentialLocale(), (ObserveReferentialEntity) entity);
                    values.add(reference);

                }

            } else if (DataDto.class.isAssignableFrom(type)) {

                Class<? extends DataDto> dtoType = (Class<? extends DataDto>) type;

                DataBinderSupport<ObserveDataEntity, ? extends DataDto> binder = getDataBinder(dtoType);
                List<ObserveEntity> universe = executeRequest(hql, vars);

                for (ObserveEntity entity : universe) {

                    DataReference<? extends DataDto> reference = binder.toDataReference(getReferentialLocale(), (ObserveDataEntity) entity);
                    values.add(reference);

                }

            } else {

                List<Object> universe = executeRequest(hql, vars);

                values.addAll(universe);

            }

            variable.setValues(values);
        }

        return report;

    }

    @Override
    public DataMatrix executeReport(Report report, String tripId) {

        if (report == null) {

            // pas de report selectionne, donc pas de résultat
            return null;
        }

        if (!report.canExecute()) {

            // la requete n'est pas exécutable
            return null;
        }

        // remplissage des variables de répétition si nécessaire
        doPopulateRepeatVariables(report, tripId);

        if (log.isDebugEnabled()) {
            log.debug("Build result for report [" + report.getName() +
                              "] on " + tripId);
        }

        int rows = report.getRows();
        int columns = report.getColumns();

        if (log.isDebugEnabled()) {
            log.debug("Dimension : [" + rows + "," + columns + "]");
        }


        // lancement de la première opération et la matrice de resultat
        DataMatrix result = executeReportOperation(
                ReportOperation.ExecuteRequests,
                report,
                tripId,
                new DataMatrix());

        // lancement des opérations supplémentaires
        for (ReportOperation objectOperation : report.getOperations()) {

            DataMatrix tmp = result;
            if (log.isDebugEnabled()) {
                log.debug("Apply objectOperation " + objectOperation + " to " + tmp);
            }
            result = executeReportOperation(
                    objectOperation,
                    report,
                    tripId,
                    tmp);
        }
        if (log.isDebugEnabled()) {
            log.debug("Final result : " + result);
        }
        return result;
    }

    private DataMatrix executeReportOperation(ReportOperation executeRequests, Report report, String tripId, DataMatrix incoming) {

        DataMatrix result = incoming;

        switch (executeRequests) {
            case ExecuteRequests: {

                ReportRequest[] requests = report.getRequests();

                DataMatrix[] requestResults = new DataMatrix[requests.length];

                int i = 0;
                for (ReportRequest request : requests) {

                    ReportRequest.RequestRepeat repeatVariable = request.getRepeat();

                    if (repeatVariable == null) {

                        // requete simple sans repetition
                        result = executeReportRequest(request, report, tripId);
                    } else {

                        // on a une requete avec repetition
                        String repeatVariableName = repeatVariable.getVariableName();
                        ReportVariable repeat = report.getRepeatVariable(repeatVariableName);


                        result = executeReportRequest(request, report, tripId, repeat);


                    }

                    requestResults[i++] = result;
                }

                int rows = report.getRows();
                int columns = report.getColumns();

                result = DataMatrix.merge(rows, columns, requestResults);
            }
            break;

            case GroupByLength: {
                // Première passe pour grouper par classe de taille
                Map<String, MutableInt> data = new LinkedHashMap<>();
                for (int row = 0, nbRows = incoming.getHeight(); row < nbRows; row++) {

                    String length = (String) incoming.getValue(0, row);
                    Integer count = Integer.valueOf(incoming.getValue(1, row).toString());

                    MutableInt mutableInt = data.get(length);
                    if (mutableInt == null) {
                        mutableInt = new MutableInt();
                        data.put(length, mutableInt);
                    }
                    mutableInt.add(count);

                }

                // Deuxième passe pour remplir la matrice
                Set<String> lengths = new HashSet<>();
                for (int row = 0; row < incoming.getHeight(); row++) {
                    lengths.add((String) incoming.getValue(0, row));
                }

                result = createTmpMatrix(0, incoming.getHeight(), incoming.getWidth(), lengths.size());
                int row = 0;
                for (Map.Entry<String, MutableInt> entry : data.entrySet()) {
                    String length = entry.getKey();
                    MutableInt mutableInt = entry.getValue();
                    result.setValue(0, row, length);
                    result.setValue(1, row, mutableInt.intValue());
                    row++;
                }
            }
            break;

            case SumColumn:
            case SumIntColumn: {

                DataMatrix tmpMatrix = createTmpMatrix(0, incoming.getHeight(), incoming.getWidth(), 1);

                for (int column = 0, nbColumns = incoming.getWidth(); column < nbColumns; column++) {

                    Object sumColumn = getSumColumn(column, incoming);
                    if (ReportOperation.SumIntColumn.equals(executeRequests) && sumColumn instanceof Number) {
                        sumColumn = ((Number) sumColumn).intValue();
                    }
                    tmpMatrix.setValue(column, 0, sumColumn);
                    if (log.isDebugEnabled()) {
                        log.debug("objectOperation [" + column + ",0] = " + sumColumn);
                    }
                }

                result = DataMatrix.merge(incoming, tmpMatrix);
            }
            break;
            case SumRow:
            case SumIntRow: {

                DataMatrix tmpMatrix = createTmpMatrix(incoming.getWidth(), 0, 1, incoming.getHeight());

                for (int row = 0, nbRows = incoming.getHeight(); row < nbRows; row++) {

                    Object sumRow = getSumRow(row, incoming);
                    if (ReportOperation.SumIntRow.equals(executeRequests) && sumRow instanceof Number) {
                        sumRow = ((Number) sumRow).intValue();
                    }
                    tmpMatrix.setValue(0, row, sumRow);
                    if (log.isDebugEnabled()) {
                        log.debug("objectOperation [0, " + row + "] = " + sumRow);
                    }
                }

                result = DataMatrix.merge(incoming, tmpMatrix);

            }
            break;
        }

        return result;
    }

    protected Object getSumColumn(int column, DataMatrix incoming) {
        Double result = 0d;

        int nbRows = incoming.getHeight();

        for (int row = 0; row < nbRows; row++) {
            Serializable o = incoming.getValue(column, row);

            if (o == null || "null".equals(o)) {
                o = 0;
            }
            Double d;
            try {
                d = Double.valueOf(o.toString());
            } catch (NumberFormatException e) {
                // une des données de la colonne n'est pas un count
                // on sort directement
                if (log.isDebugEnabled()) {
                    log.debug("Could not convert " + o + " to number", e);
                }
                return "-";
            }
            result += d;
        }

        return result;
    }

    protected Object getSumRow(int row, DataMatrix incoming) {

        Double result = 0d;

        int nbColumns = incoming.getWidth();

        for (int col = 0; col < nbColumns; col++) {
            Serializable o = incoming.getValue(col, row);

            if (o == null || "null".equals(o)) {
                o = 0;
            }
            Double d;
            try {
                d = Double.valueOf(o.toString());
            } catch (NumberFormatException e) {
                // une des données de la colonne n'est pas un count
                // on sort directement
                if (log.isDebugEnabled()) {
                    log.debug("Could not convert " + o + " to number", e);
                }
                return "-";
            }
            result += d;
        }

        return result;
    }


    private DataMatrix createTmpMatrix(int positionX, int positionY, int width, int height) {

        DataMatrix result = new DataMatrix();

        // calcul de la position des résultats de l'opération
        Point location = new Point(positionX, positionY);
        result.setLocation(location);

        // calcul de la taille des résultats de l'opération
        Dimension dim = new Dimension(width, height);
        result.setDimension(dim);

        // creation de la matrice
        result.createData();

        return result;
    }

    public DataMatrix executeReportRequest(ReportRequest request, Report report, String tripId, ReportVariable repeatValues) {

        DataMatrix result = new DataMatrix();

        Map<String, Object> params = ReportRequest.extractParams(report, tripId);

        for (Object repeatValue : repeatValues.getValues()) {
            params.put(request.getRepeat().getVariableName(), repeatValue);
            DataMatrix tmp = executeReportRequest(request, params);
            if (result == null) {

                // premiere requete executee
                result = tmp;
                result.setX(0);
                result.setY(0);
            } else {
                switch (request.getRepeat().getLayout()) {

                    case row:

                        // on ajoute le resultat a droite de celui deja present
                        tmp.setX(result.getX() + result.getWidth());
                        tmp.setY(result.getY());
                        break;

                    case column:

                        // on ajoute le resultat en dessous de celui deja present
                        tmp.setX(result.getX());
                        tmp.setY(result.getY() + result.getHeight());
                        break;
                }
                result = DataMatrix.merge(result, tmp);
            }
        }
        result.setX(request.getX());
        result.setY(request.getY());
        if (log.isDebugEnabled()) {
            log.debug("Result location  : " + result.getLocation());
            log.debug("Result data      :\n" + result.getClipbordContent(true, true));
        }
        return result;
    }

    public DataMatrix executeReportRequest(ReportRequest request, Report report, String tripId) {
        Map<String, Object> params = ReportRequest.extractParams(report, tripId);

        return executeReportRequest(request, params);

    }

    protected DataMatrix executeReportRequest(ReportRequest reportRequest, Map<String, Object> params) {

        // création des paramètres : couples (key, value)
        if (log.isDebugEnabled()) {
            log.debug("Request          : " + reportRequest.getRequest());
            log.debug("Available params : " + params.keySet());
            log.debug("Params to use    : " + params);
        }

        // lancement de la requête
        List<?> list = executeRequest(reportRequest.getRequest(), params);
        if (log.isDebugEnabled()) {
            log.debug("Result size      : " + list.size());
        }

        // determination des dimensions du résultat
        Dimension dimension = computeDimension(reportRequest, list);
        if (log.isDebugEnabled()) {
            log.debug("Result dimension : " + dimension);
        }

        // construction du resultat
        DataMatrix result = computeResult(reportRequest, dimension, list);
        if (log.isDebugEnabled()) {
            log.debug("Result location  : " + result.getLocation());
            log.debug("Result data      :\n" + result.getClipbordContent(true, true));
        }
        return result;
    }

    protected DataMatrix computeResult(ReportRequest request, Dimension dimension, List<?> list) {

        DataMatrix result = new DataMatrix();
        result.setDimension(dimension);
        result.createData();

        // le seul cas différent est le n-* (une ligne correspond à une colonne)

        int y = 0;
        int x = 0;

        switch (request.getLayout()) {

            case row:

                boolean uniqueColumn = result.getWidth() == 1;

                // les lignes du résultat sont les lignes du tableau

                for (Object row : list) {

                    if (uniqueColumn) {

                        // une seule colonne
                        result.setValue(0, y, row);
                    } else {

                        x = 0;
                        Object[] cells = (Object[]) row;
                        for (Object cell : cells) {
                            result.setValue(x++, y, cell);
                        }
                    }

                    // on passage a la ligne suivante
                    y++;
                }
                break;
            case column:

                // les lignes du résultat sont les colonnes du tableau

                boolean uniqueRow = result.getHeight() == 1;

                for (Object col : list) {

                    if (uniqueRow) {

                        // une seule ligne
                        result.setValue(x, 0, col);
                    } else {

                        y = 0;
                        Object[] cells = (Object[]) col;
                        for (Object cell : cells) {
                            result.setValue(x, y++, cell);
                        }
                    }

                    // on passage a la colonne suivante
                    x++;
                }
                break;
        }

        // on pousse la position du résultat
        result.setX(request.getX());
        result.setY(request.getY());
        return result;
    }

    protected Dimension computeDimension(ReportRequest request, List<?> list) {

        int height = 0;
        int width = 0;

        switch (request.getLayout()) {

            case row:

                // le count de result est le nombre de lignes
                height = list.size();

                if (list.isEmpty()) {

                    // vu qu'il n'y a pas de résultat, on ne peut rien dire
                    width = 0;
                } else {

                    Object o = list.get(0);

                    if (o == null || !o.getClass().isArray()) {

                        // une seule colonne
                        width = 1;
                    } else {
                        width = ((Object[]) o).length;
                    }
                }

                break;
            case column:

                // le count de result est le nombre de colonnes
                width = list.size();

                if (list.isEmpty()) {

                    // vu qu'il n'y a pas de résultat, on ne peut rien dire
                    height = 0;
                } else {

                    Object o = list.get(0);

                    if (o == null || !o.getClass().isArray()) {

                        // une seule ligne
                        height = 1;
                    } else {
                        height = ((Object[]) o).length;
                    }
                }

                break;
        }
        return new Dimension(width, height);
    }

    protected <R> List<R> executeRequest(String request, Map<String, Object> params) {

        // si il y a des references de DTO dans les valeurs des paramètres on les remplace par leur id
        // on copie pour ne pas modifier le paramètre de la méthode
        Map<String, Object> paramsFixes = Maps.newHashMap();

        for (Map.Entry<String, Object> entry : params.entrySet()) {

            String name = entry.getKey();

            if (request.contains(":" + name)) {

                Object value = entry.getValue();

                if (value instanceof AbstractReference) {
                    AbstractReference referenceDto = (AbstractReference) value;

                    value = referenceDto.getId();

                }

                paramsFixes.put(name, value);
            }

        }

        AbstractObserveTopiaDao dao = (AbstractObserveTopiaDao) getTopiaPersistenceContext().getDao(TripSeine.class);

        return dao.findAllFromHql(request, paramsFixes);
    }

    protected void doPopulateRepeatVariables(Report report, String tripId) {

        Map<String, Object> vars = ReportRequest.extractParams(report, tripId);

        for (ReportVariable variable : report.getRepeatVariables()) {

            String hql = variable.getRequest();

            List<Object> universe = executeRequest(hql, vars);

            LinkedHashSet<Object> values = new LinkedHashSet<>(universe);

            variable.setValues(values);
        }
    }

}
