/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.validate;

import fr.ird.observe.entities.Activity;
import fr.ird.observe.entities.ObserveSet;
import fr.ird.observe.entities.Trip;
import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.longline.TripLongline;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.FloatingObject;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.entities.seine.TripSeine;
import org.nuiton.util.DateUtil;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Contient les objets en cours de validation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ValidationDataContext {

    private List<?> editingReferentielListEntity;

    private Program programEntity;

    private TripSeine tripSeineEntity;

    private TripLongline tripLonglineEntity;

    private Route routeEntity;

    private ActivitySeine activitySeineEntity;

    private ActivityLongline activityLonglineEntity;

    private SetSeine setSeineEntity;

    private SetLongline setLonglineEntity;

    private FloatingObject floatingObjectEntity;

    public void clear() {
        editingReferentielListEntity = null;
        programEntity = null;
        tripSeineEntity = null;
        routeEntity = null;
        activitySeineEntity = null;
        activityLonglineEntity = null;
        setSeineEntity = null;
        setLonglineEntity = null;
        floatingObjectEntity = null;
    }

    public Program getProgramEntity() {
        return programEntity;
    }

    public Trip getTripEntity() {
        Trip result = getTripSeineEntity();
        if (result == null) {
            result = getTripLonglineEntity();
        }
        return result;
    }

    public TripSeine getTripSeineEntity() {
        return tripSeineEntity;
    }

    void setTripSeineEntity(TripSeine tripSeineEntity) {
        this.tripSeineEntity = tripSeineEntity;
        this.programEntity = tripSeineEntity == null ? null : tripSeineEntity.getProgram();
    }

    public TripLongline getTripLonglineEntity() {
        return tripLonglineEntity;
    }

    void setTripLonglineEntity(TripLongline tripLonglineEntity) {
        this.tripLonglineEntity = tripLonglineEntity;
        this.programEntity = tripLonglineEntity == null ? null : tripLonglineEntity.getProgram();
    }

    public Route getRouteEntity() {
        return routeEntity;
    }

    void setRouteEntity(Route routeEntity) {
        this.routeEntity = routeEntity;
    }

    public Activity getActivityEntity() {
        Activity result = getActivitySeineEntity();
        if (result == null) {
            result = getActivityLonglineEntity();
        }
        return result;
    }

    public ActivitySeine getActivitySeineEntity() {
        return activitySeineEntity;
    }

    void setActivitySeineEntity(ActivitySeine activitySeineEntity) {
        this.activitySeineEntity = activitySeineEntity;
    }

    public ActivityLongline getActivityLonglineEntity() {
        return activityLonglineEntity;
    }

    void setActivityLonglineEntity(ActivityLongline activityLonglineEntity) {
        this.activityLonglineEntity = activityLonglineEntity;
    }

    public ObserveSet getSetEntity() {
        ObserveSet result = getSetSeineEntity();
        if (result == null) {
            result = getSetLonglineEntity();
        }
        return result;
    }

    public SetSeine getSetSeineEntity() {
        return setSeineEntity;
    }

    void setSetSeineEntity(SetSeine setSeineEntity) {
        this.setSeineEntity = setSeineEntity;
    }

    public SetLongline getSetLonglineEntity() {
        return setLonglineEntity;
    }

    void setSetLonglineEntity(SetLongline setLonglineEntity) {
        this.setLonglineEntity = setLonglineEntity;
    }

    public FloatingObject getFloatingObjectEntity() {
        return floatingObjectEntity;
    }

    void setFloatingObjectEntity(FloatingObject floatingObjectEntity) {
        this.floatingObjectEntity = floatingObjectEntity;
    }

    public List<?> getEditingReferentielListEntity() {
        return editingReferentielListEntity;
    }

    void setEditingReferentielListEntity(List<?> editingReferentielListEntity) {
        this.editingReferentielListEntity = editingReferentielListEntity;
    }

    // Ne pas supprimer, utilise dans la validation
    public boolean isTimeAvailable(TripLongline trip, String activityId, Date time) {
        boolean result = true;
        if (trip.isActivityLonglineNotEmpty()) {

            Optional<ActivityLongline> first = trip.getActivityLongline().stream().filter(r -> !r.getTopiaId().equals(activityId) && DateUtil.getTime(time, true, false).equals(r.getTime())).findFirst();
            result = !first.isPresent();
        }
        return result;
    }

    // Ne pas supprimer, utilise dans la validation
    public boolean isDateAvailable(TripSeine tripSeine, String routeId, Date date) {
        boolean result = true;
        if (tripSeine.isRouteNotEmpty()) {

            Optional<Route> first = tripSeine.getRoute().stream().filter(r -> !r.getTopiaId().equals(routeId) && DateUtil.getDay(date).equals(r.getDate())).findFirst();
            result = !first.isPresent();
        }
        return result;
    }

    // Ne pas supprimer, utilise dans la validation
    public boolean isTimeAvailable(Route route, String activityId, Date time) {
        boolean result = true;
        if (route.isActivitySeineNotEmpty()) {

            Optional<ActivitySeine> first = route.getActivitySeine().stream().filter(r -> !r.getTopiaId().equals(activityId) && DateUtil.getTime(time, true, false).equals(r.getTime())).findFirst();
            result = !first.isPresent();
        }
        return result;
    }

    public float abs(Float f) {
        return Math.abs(f);
    }

    public float abs(Long f) {
        return Math.abs(f);
    }


}
