package fr.ird.observe.services.topia.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.entities.constants.seine.SchoolTypePersist;
import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.Species2;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.ActivitySeines;
import fr.ird.observe.entities.seine.NonTargetCatch;
import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.constants.seine.SchoolType;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateActivitySeineDataResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import static fr.ird.observe.services.service.actions.consolidate.ConsolidateActivitySeineDataResult.NonTargetCatchModification;
import static fr.ird.observe.services.service.actions.consolidate.ConsolidateActivitySeineDataResult.NonTargetLengthModification;
import static fr.ird.observe.services.service.actions.consolidate.ConsolidateActivitySeineDataResult.TargetLengthModification;
import static org.nuiton.i18n.I18n.l;

/**
 * Un constructeur de résultat de l'opération de consolidation des données de type Seine.
 *
 * Created on 29/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ConsolidateActivitySeineDataResultBuilder {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ConsolidateActivitySeineDataResultBuilder.class);
    protected final ReferentialLocale referenceLocale;
    protected final Locale locale;
    protected final ActivitySeine activitySeine;
    protected final ImmutableSet.Builder<String> lengthWeightParameterNotFoundBuilder = new ImmutableSet.Builder<>();
    protected final ImmutableSet.Builder<TargetLengthModification> targetLengthModificationBuilder = new ImmutableSet.Builder<>();
    protected final ImmutableSet.Builder<NonTargetLengthModification> nonTargetLengthModificationBuilder = new ImmutableSet.Builder<>();
    protected final ImmutableSet.Builder<NonTargetCatchModification> nonTargetCatchModificationBuilder = new ImmutableSet.Builder<>();
    protected SchoolType oldSchoolType;
    protected SchoolType newSchoolType;

    protected ConsolidateActivitySeineDataResultBuilder(Locale locale, ReferentialLocale referenceLocale, ActivitySeine activitySeine) {
        this.locale = locale;
        this.referenceLocale = referenceLocale;
        this.activitySeine = activitySeine;
    }

    public static ConsolidateActivitySeineDataResultBuilder create(Locale locale, ReferentialLocale referenceLocale, ActivitySeine activitySeine) {
        return new ConsolidateActivitySeineDataResultBuilder(locale, referenceLocale, activitySeine);
    }

    public void flushTargetLengthModification(TargetLength targetLength, String... propertyNamesModified) {

        Species species = targetLength.getSpecies();
        String speciesLabel = Species2.decorate(locale, species);
        for (String modifiedProperty : propertyNamesModified) {
            if (modifiedProperty.equals(TargetLength.PROPERTY_LENGTH)) {

                TargetLengthModification element = new TargetLengthModification();
                element.setTargetLengthId(targetLength.getTopiaId());
                element.setSpeciesLabel(speciesLabel);
                element.setPropertyName(TargetLength.PROPERTY_LENGTH);
                element.setNewValue(targetLength.getLength());
                targetLengthModificationBuilder.add(element);

            } else if (modifiedProperty.equals(TargetLength.PROPERTY_WEIGHT)) {

                TargetLengthModification element = new TargetLengthModification();
                element.setTargetLengthId(targetLength.getTopiaId());
                element.setSpeciesLabel(speciesLabel);
                element.setPropertyName(TargetLength.PROPERTY_LENGTH);
                element.setNewValue(targetLength.getLength());
                targetLengthModificationBuilder.add(element);

            }
        }

    }

    public void flushNonTargetLengthModification(NonTargetLength nonTargetLength, String... propertyNamesModified) {

        Species species = nonTargetLength.getSpecies();
        String speciesLabel = Species2.decorate(locale, species);
        for (String modifiedProperty : propertyNamesModified) {
            if (modifiedProperty.equals(NonTargetLength.PROPERTY_LENGTH)) {

                NonTargetLengthModification element = new NonTargetLengthModification();
                element.setNonTargetLengthId(nonTargetLength.getTopiaId());
                element.setSpeciesLabel(speciesLabel);
                element.setPropertyName(NonTargetLength.PROPERTY_LENGTH);
                element.setNewValue(nonTargetLength.getLength());
                nonTargetLengthModificationBuilder.add(element);

            } else if (modifiedProperty.equals(NonTargetLength.PROPERTY_WEIGHT)) {

                NonTargetLengthModification element = new NonTargetLengthModification();
                element.setNonTargetLengthId(nonTargetLength.getTopiaId());
                element.setSpeciesLabel(speciesLabel);
                element.setPropertyName(NonTargetLength.PROPERTY_LENGTH);
                element.setNewValue(nonTargetLength.getLength());
                nonTargetLengthModificationBuilder.add(element);

            }
        }

    }

    public void flushNonTargetCatchModification(NonTargetCatch nonTargetCatch, String... propertyNamesModified) {

        Species species = nonTargetCatch.getSpecies();
        String speciesLabel = Species2.decorate(locale, species);
        for (String modifiedProperty : propertyNamesModified) {
            switch (modifiedProperty) {
                case NonTargetCatch.PROPERTY_MEAN_LENGTH: {

                    NonTargetCatchModification element = new NonTargetCatchModification();
                    element.setNonTargetCatchId(nonTargetCatch.getTopiaId());
                    element.setSpeciesLabel(speciesLabel);
                    element.setPropertyName(NonTargetCatch.PROPERTY_MEAN_LENGTH);
                    element.setNewValue(nonTargetCatch.getMeanLength());
//                    element.computeValueSource = I18nEnumUtil.getLabel(locale, nonTargetCatch.getMeanLengthComputedSource());
                    nonTargetCatchModificationBuilder.add(element);

                    break;
                }
                case NonTargetCatch.PROPERTY_MEAN_WEIGHT: {

                    NonTargetCatchModification element = new NonTargetCatchModification();
                    element.setNonTargetCatchId(nonTargetCatch.getTopiaId());
                    element.setSpeciesLabel(speciesLabel);
                    element.setPropertyName(NonTargetCatch.PROPERTY_MEAN_WEIGHT);
                    element.setNewValue(nonTargetCatch.getMeanWeight());
//                    element.computeValueSource = I18nEnumUtil.getLabel(locale, nonTargetCatch.getMeanWeightComputedSource());
                    nonTargetCatchModificationBuilder.add(element);

                    break;
                }
                case NonTargetCatch.PROPERTY_CATCH_WEIGHT: {

                    NonTargetCatchModification element = new NonTargetCatchModification();
                    element.setNonTargetCatchId(nonTargetCatch.getTopiaId());
                    element.setSpeciesLabel(speciesLabel);
                    element.setPropertyName(NonTargetCatch.PROPERTY_CATCH_WEIGHT);
                    element.setNewValue(nonTargetCatch.getCatchWeight());
//                    element.computeValueSource = I18nEnumUtil.getLabel(locale, nonTargetCatch.getCatchWeightComputedSource());
                    nonTargetCatchModificationBuilder.add(element);

                    break;
                }
                case NonTargetCatch.PROPERTY_TOTAL_COUNT: {

                    NonTargetCatchModification element = new NonTargetCatchModification();
                    element.setNonTargetCatchId(nonTargetCatch.getTopiaId());
                    element.setSpeciesLabel(speciesLabel);
                    element.setPropertyName(NonTargetCatch.PROPERTY_TOTAL_COUNT);
                    element.setNewValue(nonTargetCatch.getTotalCount());
//                    element.computeValueSource = I18nEnumUtil.getLabel(locale, nonTargetCatch.getTotalCountComputedSource());
                    nonTargetCatchModificationBuilder.add(element);

                    break;
                }
            }
        }

    }

    public ConsolidateActivitySeineDataResultBuilder registerLengthWeightParameterNotFound(Species species, Ocean ocean, Sex sex, Date routeDate) {
        String speciesLabel = Species2.decorate(locale, species);
        String oceanLabel = I18nReferenceEntities.getLabel(referenceLocale.ordinal(), ocean);
        String sexLabel = sex == null ? l(locale, "observe.service.actions.consolidate.noSex") : I18nReferenceEntities.getLabel(referenceLocale.ordinal(), sex);
        String message = l(locale, "observe.service.actions.consolidate.lengthWeightParameterNotFound", speciesLabel, oceanLabel, sexLabel, routeDate);
        lengthWeightParameterNotFoundBuilder.add(message);
        if (log.isWarnEnabled()) {
            log.warn(message);
        }
        return this;
    }

    public ConsolidateActivitySeineDataResultBuilder setSchoolTypeChanged(SchoolTypePersist oldSchoolType, SchoolTypePersist newSchoolType) {
        this.oldSchoolType = SchoolType.valueOf(oldSchoolType.name());
        this.newSchoolType = SchoolType.valueOf(newSchoolType.name());
        return this;
    }

    public Optional<ConsolidateActivitySeineDataResult> build() {

        ImmutableSet<TargetLengthModification> targetLengthModifications = targetLengthModificationBuilder.build();
        ImmutableSet<NonTargetLengthModification> nonTargetLengthModifications = nonTargetLengthModificationBuilder.build();
        ImmutableSet<NonTargetCatchModification> nonTargetCatchModifications = nonTargetCatchModificationBuilder.build();

        boolean noModification = (targetLengthModifications.isEmpty()
                && nonTargetLengthModifications.isEmpty()
                && nonTargetCatchModifications.isEmpty()
                && newSchoolType == null);

        ConsolidateActivitySeineDataResult consolidateActivitySeineDataResult;
        if (noModification) {

            consolidateActivitySeineDataResult = null;

        } else {

            consolidateActivitySeineDataResult = new ConsolidateActivitySeineDataResult();
            consolidateActivitySeineDataResult.setActivitySeineId(activitySeine.getTopiaId());
            consolidateActivitySeineDataResult.setActivitySeineLabel(ActivitySeines.decorate(referenceLocale.ordinal(), activitySeine));

            if (newSchoolType != null) {
                consolidateActivitySeineDataResult.setSchoolTypeChanged(oldSchoolType, newSchoolType);
            }
            consolidateActivitySeineDataResult.setTargetLengthModifications(targetLengthModifications);
            consolidateActivitySeineDataResult.setNonTargetLengthModifications(nonTargetLengthModifications);
            consolidateActivitySeineDataResult.setNonTargetCatchModifications(nonTargetCatchModifications);

        }

        return Optional.ofNullable(consolidateActivitySeineDataResult);

    }
}
