package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.seine.SpeciesStatus;
import fr.ird.observe.entities.seine.ObjectObservedSpecies;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObjectObservedSpeciesBinder extends DataBinderSupport<ObjectObservedSpecies, ObjectObservedSpeciesDto> {

    public ObjectObservedSpeciesBinder() {
        super(ObjectObservedSpecies.class, ObjectObservedSpeciesDto.class, false);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, ObjectObservedSpeciesDto dto, ObjectObservedSpecies entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setCount(dto.getCount());
        entity.setStatut(dto.getStatut());
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setSpeciesStatus(toEntity(dto.getSpeciesStatus(), SpeciesStatus.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, ObjectObservedSpecies entity, ObjectObservedSpeciesDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setCount(entity.getCount());
        dto.setStatut(entity.getStatut());
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setSpeciesStatus(toReferentialReference(referentialLocale, entity.getSpeciesStatus(), SpeciesStatusDto.class));

    }
}
