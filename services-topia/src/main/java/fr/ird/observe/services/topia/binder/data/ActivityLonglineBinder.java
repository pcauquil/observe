package fr.ird.observe.services.topia.binder.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.referentiel.FpaZone;
import fr.ird.observe.entities.referentiel.longline.VesselActivityLongline;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ActivityLonglineBinder extends DataBinderSupport<ActivityLongline, ActivityLonglineDto> {

    public ActivityLonglineBinder() {
        super(ActivityLongline.class, ActivityLonglineDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, ActivityLonglineDto dto, ActivityLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setTimeStamp(dto.getTimeStamp());
        entity.setLatitude(dto.getLatitude());
        entity.setLongitude(dto.getLongitude());
        entity.setSeaSurfaceTemperature(dto.getSeaSurfaceTemperature());
        entity.setVesselActivityLongline(toEntity(dto.getVesselActivityLongline(), VesselActivityLongline.class));
        entity.setFpaZone(toEntity(dto.getFpaZone(), FpaZone.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, ActivityLongline entity, ActivityLonglineDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setTimeStamp(entity.getTimeStamp());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        dto.setQuadrant(entity.getQuadrant());
        dto.setSeaSurfaceTemperature(entity.getSeaSurfaceTemperature());
        dto.setVesselActivityLongline(toReferentialReference(referentialLocale, entity.getVesselActivityLongline(), VesselActivityLonglineDto.class));
        dto.setFpaZone(toReferentialReference(referentialLocale, entity.getFpaZone(), FpaZoneDto.class));

    }

    @Override
    public DataReference<ActivityLonglineDto> toDataReference(ReferentialLocale referentialLocale, ActivityLongline entity) {

        return toDataReference(entity,
                               entity.getTimeStamp(),
                               getLabel(referentialLocale, entity.getVesselActivityLongline()),
                               toDataReference(referentialLocale, entity.getSetLongline(), SetLonglineDto.class));

    }

    @Override
    public DataReference<ActivityLonglineDto> toDataReference(ReferentialLocale referentialLocale, ActivityLonglineDto dto) {

        return toDataReference(dto,
                               dto.getTimeStamp(),
                               getLabel(referentialLocale, dto.getVesselActivityLongline()),
                               dto.getSetLongline());
    }
}
