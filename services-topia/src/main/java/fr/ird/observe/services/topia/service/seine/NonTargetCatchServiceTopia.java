package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.NonTargetLength;
import fr.ird.observe.entities.seine.NonTargetSample;
import fr.ird.observe.entities.seine.SetSeine;
import fr.ird.observe.services.service.seine.NonTargetCatchService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class NonTargetCatchServiceTopia extends ObserveServiceTopia implements NonTargetCatchService {

    private static final Log log = LogFactory.getLog(NonTargetCatchServiceTopia.class);

    @Override
    public Form<SetSeineNonTargetCatchDto> loadForm(String setSeineId) {
        if (log.isTraceEnabled()) {
            log.trace("loadForm(" + setSeineId + ")");
        }

        SetSeine setSeine = loadEntity(SetSeineNonTargetCatchDto.class, setSeineId);

        Form<SetSeineNonTargetCatchDto> form = dataEntityToForm(
                SetSeineNonTargetCatchDto.class,
                setSeine,
                ReferenceSetRequestDefinitions.SET_SEINE_NON_TARGET_CATCH_FORM
        );

        SetSeineNonTargetCatchDto setSeineNonTargetCatchDto = form.getObject();

        // on cherche si il y a des échantillons sur les captures
        if (setSeine.isNonTargetSampleNotEmpty()) {

            NonTargetSample nonTargetSample = Iterables.get(setSeine.getNonTargetSample(), 0);

            Set<String> speciesSampleIds = nonTargetSample.getNonTargetLength()
                                                          .stream()
                                                          .map(NonTargetLength::getSpecies)
                                                          .map(Species::getTopiaId)
                                                          .collect(Collectors.toSet());

            for (NonTargetCatchDto nonTargetCatchDto : setSeineNonTargetCatchDto.getNonTargetCatch()) {

                boolean hasSample = speciesSampleIds.contains(nonTargetCatchDto.getSpecies().getId());
                nonTargetCatchDto.setHasSample(hasSample);

            }

        }

        return form;
    }

    @Override
    public SaveResultDto save(SetSeineNonTargetCatchDto dto) {
        if (log.isTraceEnabled()) {
            log.trace("save(" + dto.getId() + ")");
        }

        // On conserve les identifiants des espèces utilisables dans les échantillons
        Set<String> speciesIds = dto.getNonTargetCatch().stream()
                                    .map(NonTargetCatchDto::getSpecies)
                                    .map(ReferentialReference::getId)
                                    .collect(Collectors.toSet());

        SetSeine entity = loadOrCreateEntityFromDataDto(dto);
        checkLastUpdateDate(entity, dto);
        copyDataDtoToEntity(dto, entity);

        // mise a jour de la propriete nonTargetDiscarded sur la calée

        boolean nonTargetDiscarded = entity.getNonTargetCatch().stream()
                                           .anyMatch(nonTargetCatch -> nonTargetCatch.getReasonForDiscard() != null);

        if (log.isInfoEnabled()) {
            log.info("SetSeine " + entity.getTopiaId() + ", nonTargetDiscarded: " + nonTargetDiscarded);
        }
//        for (NonTargetCatch nonTargetCatch : entity.getNonTargetCatch()) {
//            if (nonTargetCatch.getReasonForDiscard() != null) {
//                // on a trouve un rejet de faune
//                nonTargetDiscarded = true;
//                break;
//            }
//        }
        entity.setNonTargetDiscarded(nonTargetDiscarded);

        // on supprime les échantillons dont les espèces ne sont plus dans les captures
        if (entity.isNonTargetSampleNotEmpty()) {

            NonTargetSample nonTargetSample = entity.getNonTargetSample().iterator().next();

            List<NonTargetLength> nonTargetLengthToDelete = nonTargetSample.getNonTargetLength().stream()
                                                                           .filter(nonTargetLength -> !speciesIds.contains(nonTargetLength.getSpecies().getTopiaId()))
                                                                           .collect(Collectors.toList());

            nonTargetLengthToDelete.forEach(nonTargetSample::removeNonTargetLength);

        }

        return saveEntity(entity);

    }
}
