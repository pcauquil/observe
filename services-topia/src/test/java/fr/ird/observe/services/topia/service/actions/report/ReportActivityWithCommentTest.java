/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;

import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import org.junit.Assert;


/**
 * Pour tester le report {@code dailySetAndCapture}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.9
 */
public class ReportActivityWithCommentTest extends AbstractReportServiceTopiaTest {

    @Override
    protected String getReportId() {
        return "activityWithComment";
    }

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Activités avec comment et leurs positions",
                "Afficher les activités avec comment et leurs positions géographiques"
        );

        assertReportDimension(
                report,
                -1,
                5,
                new String[]{"Jour - Heure observation",
                             "Latitude",
                             "Longitude",
                             "Activité",
                             "Commentaire"
                }
        );

        assertReportNbRequests(report, 1);

        ReportRequest[] requests = report.getRequests();

        assertReportRequestDimension(
                requests[0],
                ReportRequest.RequestLayout.row,
                0,
                0
        );
    }

    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 5, 265, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "25/1/2013 19:0", "-19.833473", "57.466805", "Fin de veille", "Pas de fin de veille, toujours en transit 01.");
        assertResultRow(result, row++, "26/1/2013 10:25", "-16.35014", "57.616802", "Transit (route sans recherche)", "");
        assertResultRow(result, row++, "26/1/2013 11:24", "-16.083473", "57.800137", "Transit (route sans recherche)", "");
        assertResultRow(result, row++, "26/1/2013 19:0", "-14.233473", "58.066803", "Autres (à préciser dans les notes)", "Fin de journée toujours en transit.");
        assertResultRow(result, row++, "27/1/2013 7:20", "-11.0", "58.716805", "Route vers des systèmes observés", "Listao trop rapide.");
        assertResultRow(result, row++, "27/1/2013 9:24", "-10.616806", "58.766804", "Route vers des systèmes observés", "Listao trop rapide et petit banc");
        assertResultRow(result, row++, "27/1/2013 15:12", "-9.60014", "58.700138", "Recherche (général)", "albacore de 3 à 4 kg poissons dispersés et trop petits");
        assertResultRow(result, row++, "28/1/2013 6:0", "-9.250139", "56.800137", "Recherche (général)", "Transit la nuit, arrété prés d'une épave mais au petit matin pas de poisson donc nous sommes reparti directement sans toucher à l'épave.");
        assertResultRow(result, row++, "28/1/2013 11:51", "-8.450139", "56.716805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "28/1/2013 16:52", "-7.8501387", "57.53347", "Recherche (général)", "Rencontre d'un thonier espagnol visible à l'oeil nu.");
        assertResultRow(result, row++, "29/1/2013 7:0", "-7.883472", "58.166805", "Recherche (général)", "au moins 5 thoniers visibles au radar (français et espagnols)");
        assertResultRow(result, row++, "29/1/2013 10:5", "-7.7334723", "58.566803", "Route vers des systèmes observés", "marsouins et oiseaux.");
        assertResultRow(result, row++, "29/1/2013 11:5", "-7.650139", "58.633472", "Route vers des systèmes observés", "Pas de calée car plus de marsouins que de thons.\nSuivi d'un nouveaux système: oiseaux à 3.0 milles");
        assertResultRow(result, row++, "29/1/2013 12:5", "-7.650139", "58.800137", "Recherche (général)", "oiseaux mais pas de thons");
        assertResultRow(result, row++, "29/1/2013 17:36", "-7.800139", "58.516804", "Thonier arrivant sur le système détecté", "retour au radeau posé le matin meme");
        assertResultRow(result, row++, "30/1/2013 5:58", "-6.8334723", "57.966805", "Recherche (général)", "Présence de 7 autres navires thoniers français (CFTO) et espagnols");
        assertResultRow(result, row++, "30/1/2013 6:27", "-6.766805", "57.90014", "Thonier arrivant sur le système détecté", "Présence de marsouin, impossible de tourner la senne");
        assertResultRow(result, row++, "30/1/2013 7:16", "-6.783472", "57.716805", "Recherche (général)", "");
        assertResultRow(result, row++, "30/1/2013 9:58", "-6.766805", "57.73347", "Route vers des systèmes observés", "Présence de marsouins, impossible de tourner la senne.");
        assertResultRow(result, row++, "30/1/2013 10:8", "-6.800139", "57.716805", "Recherche (général)", "");
        assertResultRow(result, row++, "30/1/2013 12:33", "-6.483472", "58.200138", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "30/1/2013 12:50", "-6.4668055", "58.200138", "Début de pêche (larguage du skiff)", "Aucunes espèces accessoires, aucuns rejets de thon lors du shiftage le 31.01.2013 sauf erreur de ma part deux listao de 67 et 71 cm!");
        assertResultRow(result, row++, "31/1/2013 6:6", "-7.766805", "58.450138", "Recherche (général)", "");
        assertResultRow(result, row++, "31/1/2013 7:57", "-7.533472", "58.65014", "Recherche (général)", "");
        assertResultRow(result, row++, "31/1/2013 9:28", "-7.4168053", "58.866802", "Route vers des systèmes observés", "présence de marsouins");
        assertResultRow(result, row++, "31/1/2013 9:59", "-7.3668056", "58.966805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "31/1/2013 10:53", "-7.3168054", "59.100136", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "31/1/2013 12:23", "-7.2501388", "58.90014", "Route vers des systèmes observés", "couple de frégattes");
        assertResultRow(result, row++, "31/1/2013 13:0", "-7.2668056", "58.816803", "Recherche (général)", "");
        assertResultRow(result, row++, "31/1/2013 15:15", "-7.2668056", "58.316803", "Recherche (général)", "Shiftage du 30.01.2013 réalisé à 14h. Observation de marsouins dont non calée lors du shiftage.");
        assertResultRow(result, row++, "31/1/2013 17:59", "-7.3668056", "57.766804", "Route vers des systèmes observés", "frégattes\nNon coup de senne : changement de câble de la senne.");
        assertResultRow(result, row++, "31/1/2013 18:25", "-7.283472", "57.73347", "Recherche (général)", "");
        assertResultRow(result, row++, "1/2/2013 14:54", "-6.5668054", "57.53347", "Recherche (général)", "");
        assertResultRow(result, row++, "1/2/2013 15:52", "-6.4501386", "57.53347", "Recherche (général)", "");
        assertResultRow(result, row++, "2/2/2013 5:54", "-6.433472", "57.68347", "Recherche (général)", "7 thoniers autour de nous visibles au radar et à l'oeil nu");
        assertResultRow(result, row++, "2/2/2013 6:47", "-6.4501386", "57.53347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "2/2/2013 8:5", "-6.400139", "57.550137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "2/2/2013 8:26", "-6.433472", "57.53347", "Recherche (général)", "Rencontre avec le DOLOMIEUX, flotte SAPMER, pour récupérer les balises du BERNICA récupérées à MAHE aux Seychelles.");
        assertResultRow(result, row++, "2/2/2013 11:50", "-6.516805", "57.550137", "Fin de pêche (remontée du skiff)", "Coup nul, filet déchiré et emmélé.");
        assertResultRow(result, row++, "2/2/2013 12:48", "-6.433472", "57.58347", "Recherche (général)", "");
        assertResultRow(result, row++, "2/2/2013 13:36", "-6.4168053", "57.750137", "Route vers des systèmes observés", "");
        assertResultRow(result, row++, "2/2/2013 13:41", "-6.433472", "57.766804", "Thonier arrivant sur le système détecté", "Présence marsouin");
        assertResultRow(result, row++, "2/2/2013 14:50", "-6.483472", "57.98347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "2/2/2013 15:20", "-6.533472", "58.08347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "2/2/2013 15:48", "-6.550139", "58.116802", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "2/2/2013 16:53", "-6.433472", "58.28347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "2/2/2013 18:10", "-6.5001388", "58.166805", "Thonier arrivant sur le système détecté", "perte du système");
        assertResultRow(result, row++, "2/2/2013 18:20", "-6.483472", "58.15014", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "2/2/2013 18:50", "-6.5668054", "58.08347", "Fin de veille", "3 bateaux dans la zone. On reste sur zone jusqu'à demain car présence de \"nourriture\" aux sonars");
        assertResultRow(result, row++, "3/2/2013 6:1", "-6.533472", "58.100136", "Recherche (général)", "un autre thonier dans la zone visible au radar.");
        assertResultRow(result, row++, "3/2/2013 6:59", "-6.633472", "58.100136", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "3/2/2013 8:18", "-6.4501386", "58.15014", "Thonier arrivant sur le système détecté", "Haut fond à 292 m, risque de perdre le filet si coup de senne");
        assertResultRow(result, row++, "3/2/2013 11:22", "-6.516805", "58.15014", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "3/2/2013 11:33", "-6.550139", "58.166805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "3/2/2013 11:43", "-6.550139", "58.166805", "Recherche (général)", "");
        assertResultRow(result, row++, "3/2/2013 12:44", "-6.6168056", "58.300137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "3/2/2013 13:22", "-6.6001387", "58.383472", "Route vers des systèmes observés", "Tâche au sonar, ne savent pas ce que c'est!");
        assertResultRow(result, row++, "3/2/2013 13:24", "-6.6001387", "58.383472", "Route vers des systèmes observés", "Balbaya visible à l'oeil nu. Autre système que celui observé à 13h22.");
        assertResultRow(result, row++, "3/2/2013 13:37", "-6.6168056", "58.383472", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "3/2/2013 13:40", "-6.6168056", "58.40014", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "3/2/2013 16:32", "-6.7501388", "58.28347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "3/2/2013 17:20", "-6.800139", "58.28347", "Route vers des systèmes observés", "Présence de marsouins donc non calée.");
        assertResultRow(result, row++, "3/2/2013 17:53", "-6.8668056", "58.216805", "Thonier arrivant sur le système détecté", "Balise appartenant au Glénan, bateau de la SAPMER");
        assertResultRow(result, row++, "3/2/2013 18:1", "-6.883472", "58.18347", "Route vers des systèmes observés", "Banc de listao. Présence d'un palangrier et de thoniers au radar et à l'oeil nu.");
        assertResultRow(result, row++, "3/2/2013 18:15", "-6.883472", "58.18347", "Thonier arrivant sur le système détecté", "Banc de listao, Albacore recherché!");
        assertResultRow(result, row++, "4/2/2013 5:49", "-6.7334723", "58.166805", "Recherche (général)", "2 thoniers visibles au radar.");
        assertResultRow(result, row++, "4/2/2013 7:48", "-6.5834723", "58.016804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 8:0", "-6.5834723", "58.0", "Récupération d'une épave n'appartenant pas au bateau", "");
        assertResultRow(result, row++, "4/2/2013 8:31", "-6.533472", "58.066803", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 9:52", "-6.433472", "58.23347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 10:17", "-6.4501386", "58.28347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 12:32", "-6.550139", "57.966805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 12:50", "-6.5668054", "57.90014", "Thonier arrivant sur le système détecté", "Il ya des thons albacores partout tout autour du navire. Ils sautent, vont dans tous les sens le banc est énorme mais dispersé et il ne garde pas le même cap!");
        assertResultRow(result, row++, "4/2/2013 13:50", "-6.5834723", "57.816803", "Thonier arrivant sur le système détecté", "Même banc que précédemment! dispersé et trop rapide.");
        assertResultRow(result, row++, "4/2/2013 14:17", "-6.650139", "57.816803", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 14:20", "-6.650139", "57.816803", "Route vers des systèmes observés", "Même banc qu'à 12h50 ENORME!");
        assertResultRow(result, row++, "4/2/2013 15:36", "-6.533472", "57.766804", "Thonier arrivant sur le système détecté", "Poissons dispersés et trop rapide. Ne gardent pas le même cap, sautent dans tous les sens!");
        assertResultRow(result, row++, "4/2/2013 16:24", "-6.483472", "57.750137", "Route vers des systèmes observés", "");
        assertResultRow(result, row++, "4/2/2013 16:31", "-6.5001388", "57.766804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 16:39", "-6.483472", "57.766804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 16:59", "-6.4501386", "57.750137", "Thonier arrivant sur le système détecté", "Présence de globicéphales.");
        assertResultRow(result, row++, "4/2/2013 17:12", "-6.4668055", "57.800137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "4/2/2013 17:36", "-6.433472", "57.850136", "Route vers des systèmes observés", "");
        assertResultRow(result, row++, "4/2/2013 19:43", "-6.433472", "57.916805", "Fin de pêche (remontée du skiff)", "Coup Nul.");
        assertResultRow(result, row++, "5/2/2013 5:43", "-6.4501386", "58.18347", "Thonier arrivant sur le système détecté", "Route de nuit vers le DCP mis la veille.");
        assertResultRow(result, row++, "5/2/2013 9:29", "-6.6001387", "58.016804", "Thonier arrivant sur le système détecté", "Présence de marsouins");
        assertResultRow(result, row++, "5/2/2013 10:30", "-6.683472", "57.83347", "Thonier arrivant sur le système détecté", "Présence de Marsouins!");
        assertResultRow(result, row++, "5/2/2013 11:13", "-6.800139", "57.750137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "5/2/2013 16:17", "-7.9501386", "57.516804", "Route vers des systèmes observés", "Listao");
        assertResultRow(result, row++, "5/2/2013 16:28", "-8.0", "57.516804", "Thonier arrivant sur le système détecté", "Listao");
        assertResultRow(result, row++, "5/2/2013 17:42", "-8.316806", "57.43347", "Recherche (général)", "Marsouins observé mais nous les avons laissé au loin sur tribord sans prendre leur direction");
        assertResultRow(result, row++, "6/2/2013 6:31", "-11.766806", "56.53347", "Route vers des systèmes observés", "Banc de listao");
        assertResultRow(result, row++, "6/2/2013 6:37", "-11.800139", "56.53347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "6/2/2013 7:34", "-12.016806", "56.550137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "6/2/2013 7:47", "-12.016806", "56.550137", "Recherche (général)", "Shiftage de la calée du 05.02.13 réalisée à 8H");
        assertResultRow(result, row++, "6/2/2013 8:51", "-12.233473", "56.200138", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "6/2/2013 9:41", "-12.366806", "56.200138", "Route vers des systèmes observés", "Zone avec pleins de petits balbaya. 3 thoniers français sur zone.");
        assertResultRow(result, row++, "6/2/2013 9:43", "-12.366806", "56.18347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "6/2/2013 12:49", "-12.416806", "56.050137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "6/2/2013 13:26", "-12.416806", "56.133472", "Route vers des systèmes observés", "Pleins de petits banc d'YFT et de SKJ");
        assertResultRow(result, row++, "6/2/2013 14:20", "-12.450139", "56.0", "Thonier arrivant sur le système détecté", "listao + baleine");
        assertResultRow(result, row++, "6/2/2013 14:30", "-12.450139", "56.050137", "Route vers des systèmes observés", "");
        assertResultRow(result, row++, "6/2/2013 15:51", "-12.366806", "56.03347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "6/2/2013 16:22", "-12.400139", "56.03347", "Route vers des systèmes observés", "une dizaine de poisson (petit banc).");
        assertResultRow(result, row++, "6/2/2013 16:25", "-12.400139", "56.016804", "Thonier arrivant sur le système détecté", "Plusieurs bancs, dont un plus interessant.");
        assertResultRow(result, row++, "7/2/2013 6:6", "-12.183473", "56.116802", "Route vers des systèmes observés", "Présence de marsouins, nous continuons notre route.");
        assertResultRow(result, row++, "7/2/2013 6:34", "-12.200139", "56.133472", "Thonier arrivant sur le système détecté", "Banc de listao");
        assertResultRow(result, row++, "7/2/2013 7:9", "-12.266806", "56.133472", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "7/2/2013 7:13", "-12.266806", "56.18347", "Route vers des systèmes observés", "Listao + baleines");
        assertResultRow(result, row++, "7/2/2013 7:25", "-12.300139", "56.216805", "Thonier arrivant sur le système détecté", "Listao + 3 baleines");
        assertResultRow(result, row++, "7/2/2013 7:32", "-12.333472", "56.200138", "Route vers des systèmes observés", "");
        assertResultRow(result, row++, "7/2/2013 8:11", "-12.383472", "56.050137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "7/2/2013 11:46", "-12.383472", "55.98347", "Route vers des systèmes observés", "Nombre impressionnant de thons! gros balbaya!");
        assertResultRow(result, row++, "7/2/2013 12:47", "-12.416806", "56.016804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "7/2/2013 13:8", "-12.35014", "56.03347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "7/2/2013 16:43", "-12.266806", "55.966805", "Route vers des systèmes observés", "Banc de Listao");
        assertResultRow(result, row++, "7/2/2013 16:45", "-12.283473", "55.950138", "Thonier arrivant sur le système détecté", "Banc de listao trop rapide, banc de gros albacore visé!");
        assertResultRow(result, row++, "7/2/2013 16:52", "-12.283473", "55.98347", "Route vers des systèmes observés", "Banc de listao vu directement aux jumelles nous continuons donc notre route!");
        assertResultRow(result, row++, "7/2/2013 17:39", "-12.250139", "55.98347", "Route vers des systèmes observés", "Listao + baleine");
        assertResultRow(result, row++, "7/2/2013 18:1", "-12.283473", "55.916805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 6:58", "-12.333472", "55.93347", "Route vers des systèmes observés", "Pleins de \"petits bouillons\" partout tout autour de nous, petits bancs rapides!");
        assertResultRow(result, row++, "8/2/2013 7:17", "-12.316806", "55.950138", "Thonier arrivant sur le système détecté", "Banc de listao + marsouins+ oiseaux");
        assertResultRow(result, row++, "8/2/2013 7:29", "-12.283473", "56.0", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 7:50", "-12.216805", "56.016804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 8:2", "-12.250139", "56.03347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 8:4", "-12.250139", "56.03347", "Route vers des systèmes observés", "");
        assertResultRow(result, row++, "8/2/2013 8:21", "-12.200139", "56.100136", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 8:27", "-12.200139", "56.116802", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 9:15", "-12.250139", "56.016804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 12:30", "-12.183473", "56.066803", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "8/2/2013 12:59", "-12.183473", "56.116802", "Thonier arrivant sur le système détecté", "listao + albacore trop petits");
        assertResultRow(result, row++, "9/2/2013 6:35", "-12.166806", "56.100136", "Route vers des systèmes observés", "Banc de listao, pas interessant, nous continuons notre route sans s'arrêter!");
        assertResultRow(result, row++, "9/2/2013 6:53", "-12.133472", "56.15014", "Thonier arrivant sur le système détecté", "Présence de \"marsouins\" et banc trop petit");
        assertResultRow(result, row++, "9/2/2013 7:12", "-12.150139", "56.116802", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "9/2/2013 7:59", "-12.266806", "56.03347", "Thonier arrivant sur le système détecté", "Banc de listao");
        assertResultRow(result, row++, "9/2/2013 8:24", "-12.366806", "56.050137", "Thonier arrivant sur le système détecté", "Pleins de petits bancs partout");
        assertResultRow(result, row++, "9/2/2013 11:12", "-12.35014", "56.03347", "Thonier arrivant sur le système détecté", "Listao trop rapide et pas interessant pour le capitaine.");
        assertResultRow(result, row++, "9/2/2013 11:31", "-12.316806", "56.03347", "Thonier arrivant sur le système détecté", "Listao + baleine");
        assertResultRow(result, row++, "9/2/2013 11:32", "-12.316806", "56.050137", "Route vers des systèmes observés", "pleins de petits \"bouts\" partout");
        assertResultRow(result, row++, "9/2/2013 15:15", "-12.283473", "55.966805", "Thonier arrivant sur le système détecté", "Shiftage de 14h à 15h.");
        assertResultRow(result, row++, "9/2/2013 15:48", "-12.316806", "56.050137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "10/2/2013 5:55", "-12.316806", "55.916805", "Thonier arrivant sur le système détecté", "épave balisée la veille.");
        assertResultRow(result, row++, "10/2/2013 7:58", "-12.316806", "55.916805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "10/2/2013 11:21", "-12.450139", "55.850136", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "10/2/2013 12:6", "-12.400139", "55.816803", "Route vers des systèmes observés", "Banc de Listao.");
        assertResultRow(result, row++, "10/2/2013 12:16", "-12.383472", "55.850136", "Thonier arrivant sur le système détecté", "Banc de Listao");
        assertResultRow(result, row++, "10/2/2013 14:17", "-12.033473", "55.600136", "Recherche (général)", "Shiftage de 14h à 16h06!");
        assertResultRow(result, row++, "10/2/2013 19:4", "-12.216805", "55.48347", "Fin de veille", "");
        assertResultRow(result, row++, "11/2/2013 6:56", "-12.433473", "55.48347", "Route vers des systèmes observés", "Poissons \"étalés\"");
        assertResultRow(result, row++, "11/2/2013 7:17", "-12.466805", "55.53347", "Thonier arrivant sur le système détecté", "Nous sommes 5 sur la zone avec notamment le Glénan (CFTO), le Dolomieu (SAPMER) et Franche-Terre (SAPMER)");
        assertResultRow(result, row++, "11/2/2013 11:23", "-12.433473", "55.516804", "Début de pêche (larguage du skiff)", "Shiftage du 10.02.2013 de 11h10 à 11h30");
        assertResultRow(result, row++, "11/2/2013 14:8", "-12.416806", "55.53347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "11/2/2013 14:33", "-12.366806", "55.53347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "12/2/2013 5:50", "-12.516806", "55.450138", "Recherche (général)", "Nous sommes à côté d'un épave.");
        assertResultRow(result, row++, "12/2/2013 6:47", "-12.366806", "55.53347", "Thonier arrivant sur le système détecté", "Poissons étalés, Mélange Listao et Albacore");
        assertResultRow(result, row++, "12/2/2013 6:59", "-12.35014", "55.550137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "12/2/2013 7:20", "-12.333472", "55.566803", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "12/2/2013 9:36", "-12.300139", "55.53347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "12/2/2013 15:0", "-11.300139", "55.600136", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "13/2/2013 5:52", "-9.016806", "55.250137", "Thonier arrivant sur le système détecté", "Route de nuit vers ce radeau.");
        assertResultRow(result, row++, "13/2/2013 7:0", "-8.983473", "55.450138", "Recherche (général)", "Présence du Glénan (CFTO)");
        assertResultRow(result, row++, "13/2/2013 7:32", "-8.950139", "55.516804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "13/2/2013 8:35", "-8.950139", "55.68347", "Route vers des systèmes observés", "Nous n'avons pas trouvé le radeau à cause de la pluie, nous ne l'avons pas retrouvé!");
        assertResultRow(result, row++, "13/2/2013 10:20", "-9.016806", "56.016804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "13/2/2013 11:29", "-9.0", "56.23347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "13/2/2013 13:55", "-8.983473", "56.73347", "Opération sur objet flottant (visite, pose, modification, récupération ou pêche)", "");
        assertResultRow(result, row++, "13/2/2013 14:20", "-9.0", "56.766804", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "13/2/2013 16:24", "-9.250139", "57.133472", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "13/2/2013 18:57", "-8.783473", "57.350136", "Fin de veille", "Route jusqu'à 23h.");
        assertResultRow(result, row++, "14/2/2013 6:5", "-7.9501386", "57.68347", "Recherche (général)", "Pluie.");
        assertResultRow(result, row++, "14/2/2013 7:2", "-7.7501388", "57.73347", "Route vers des systèmes observés", "Présence de marsouins, nous n'allons pas vers le système observé, nous reprnons notre cap sans se rendre sur zone. Pluie");
        assertResultRow(result, row++, "14/2/2013 7:5", "-7.7334723", "57.73347", "Recherche (général)", "Pluie");
        assertResultRow(result, row++, "14/2/2013 10:19", "-7.150139", "58.0", "Thonier arrivant sur le système détecté", "\"Balbaya de Marsouins\" pas d'YFT.");
        assertResultRow(result, row++, "14/2/2013 18:23", "-6.633472", "57.48347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "14/2/2013 18:52", "-6.633472", "57.383472", "Fin de veille", "Route toute la nuit.");
        assertResultRow(result, row++, "15/2/2013 16:54", "-5.7334723", "54.33347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "15/2/2013 17:46", "-5.5834723", "54.33347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "15/2/2013 19:1", "-5.433472", "54.166805", "Route de nuit vers objet", "Route vers un radeau balisé du bateau au nord apperçu sur un logiciel du bateau.");
        assertResultRow(result, row++, "16/2/2013 6:0", "-4.2168055", "53.850136", "Recherche (général)", "Nous n'avons pas retrouvé le radeau balisé appartenant au navire, nous reprenons notre route vers l'ouest");
        assertResultRow(result, row++, "16/2/2013 8:5", "-4.383472", "53.716805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "16/2/2013 8:39", "-4.4668055", "53.666805", "Route vers des systèmes observés", "\"Chicanneurs");
        assertResultRow(result, row++, "16/2/2013 8:48", "-4.5001388", "53.666805", "Thonier arrivant sur le système détecté", "\"Chicaneurs\" vus \"sur l'eau\" en surface puis ils ont plongés.");
        assertResultRow(result, row++, "16/2/2013 9:43", "-4.633472", "53.766804", "Thonier arrivant sur le système détecté", "\"chicaneurs\"");
        assertResultRow(result, row++, "16/2/2013 11:3", "-4.633472", "53.550137", "Route vers des systèmes observés", "Frégates observées.");
        assertResultRow(result, row++, "16/2/2013 11:21", "-4.5834723", "53.53347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "16/2/2013 16:40", "-4.6168056", "53.08347", "Recherche (général)", "Erreur de ma part sur l'horraire, j'ai cru que le nid de pie avait vu quelque chose hors il n'y avait rien.");
        assertResultRow(result, row++, "16/2/2013 19:8", "-4.783472", "52.68347", "Fin de veille", "Route jusqu'à 20h");
        assertResultRow(result, row++, "17/2/2013 6:12", "-4.8168054", "52.550137", "Recherche (général)", "");
        assertResultRow(result, row++, "17/2/2013 7:22", "-4.7168055", "52.350136", "Route vers des systèmes observés", "Listao, la distance est trop importante, nous allons voir \"point de volaille\" moins loin.");
        assertResultRow(result, row++, "17/2/2013 7:41", "-4.7168055", "52.28347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "17/2/2013 7:43", "-4.7168055", "52.28347", "Route vers des systèmes observés", "\"Chicaneurs\", même système observé que celui de 07h22");
        assertResultRow(result, row++, "17/2/2013 7:50", "-4.7334723", "52.266804", "Thonier arrivant sur le système détecté", "Pas de thons mais des \"Marsouins\"");
        assertResultRow(result, row++, "17/2/2013 9:9", "-4.5834723", "52.03347", "Route vers des systèmes observés", "Tâche au sonar, nous prenons le cap puis confirmation des jumelles comme quoi c'est un balbaya de \"marsouins\" et non de \"gros\" nous reprenons notre cap directement sans aller jusqu'au système observé.");
        assertResultRow(result, row++, "17/2/2013 13:45", "-4.4668055", "51.73347", "Recherche (général)", "Shiftage de la calée duy 16.02.2013 de 14h à 14h57");
        assertResultRow(result, row++, "17/2/2013 14:22", "-4.483472", "51.700138", "Début de pêche (larguage du skiff)", "Environ 10 globicéphales autour du bateau");
        assertResultRow(result, row++, "17/2/2013 19:15", "-4.7001386", "51.466805", "Fin de veille", "Pas de route cette nuit. Nous sommes stoppés.");
        assertResultRow(result, row++, "18/2/2013 7:59", "-4.7001386", "51.066803", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "18/2/2013 8:0", "-4.7001386", "51.066803", "Recherche (général)", "Shiftage de 8h30 à 10h36J'ai loupé la rencontre avec un objet flottant, voilà le détail: Aux positions 4'42 et 50'53 il y a eut récupération d'une balise 98 et  pose d'une balise 04 sur un radeau (bambou filet) appartenant à un autre armement");
        assertResultRow(result, row++, "18/2/2013 17:50", "-4.2168055", "49.40014", "Recherche (général)", "Changement d'heure (-1 heure!)");
        assertResultRow(result, row++, "18/2/2013 18:23", "-4.183472", "49.300137", "Fin de veille", "Nous faisons route toute la nuit seuf si rencontre avec d'autres navires, nous stoppons.");
        assertResultRow(result, row++, "19/2/2013 6:35", "-3.6168058", "48.116802", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "19/2/2013 7:47", "-3.550139", "47.93347", "Route vers des systèmes observés", "Présence de \"Marsousins\" et de Listao vu aux jumelle avant d'être arrivé sur le système observé, nous reprenons directement notre cap!");
        assertResultRow(result, row++, "19/2/2013 9:28", "-3.4668057", "47.766804", "Début de pêche (larguage du skiff)", "Présence du bteau TREVIGNON");
        assertResultRow(result, row++, "19/2/2013 16:40", "-3.4668057", "47.78347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "19/2/2013 16:53", "-3.4834723", "47.83347", "Route vers des systèmes observés", "Vu mattes aux jumelles, direction du système observé puis avant d'y arriver sur le système observé nous avons repris notre cap car c'était des listao!");
        assertResultRow(result, row++, "19/2/2013 17:18", "-3.550139", "47.766804", "Route vers des systèmes observés", "Gleurre");
        assertResultRow(result, row++, "19/2/2013 17:36", "-3.6168058", "47.716805", "Thonier arrivant sur le système détecté", "Gleurre en plus");
        assertResultRow(result, row++, "19/2/2013 17:40", "-3.6168058", "47.700138", "Début de pêche (larguage du skiff)", "Gleurre en plus");
        assertResultRow(result, row++, "20/2/2013 7:3", "-3.450139", "47.716805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "20/2/2013 8:45", "-3.2168057", "47.700138", "Recherche (général)", "Shiftage de la première calée du 19.02.2013 de 8h à 10h05");
        assertResultRow(result, row++, "20/2/2013 11:30", "-2.900139", "47.416805", "Route vers des systèmes observés", "Matte observée, route vers système observé, puis changement de cap avant d'arriver sur le système observé car listao aperçu. Reprenons notre cap sans se rendre sur le système observé.");
        assertResultRow(result, row++, "20/2/2013 14:37", "-2.6334722", "47.93347", "Recherche (général)", "Shiftage de la deuxième calée du 19.02.2013 de 14h à 15h30");
        assertResultRow(result, row++, "20/2/2013 18:20", "-2.2168057", "48.300137", "Fin de veille", "Stopper pour la nuit à partir de 20h.");
        assertResultRow(result, row++, "21/2/2013 8:0", "-2.150139", "48.700138", "Recherche (général)", "shiftage de la 3 ème calée du 19.02.2013");
        assertResultRow(result, row++, "21/2/2013 12:41", "-1.6168056", "49.316803", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "21/2/2013 14:2", "-1.5501388", "49.53347", "Opération sur objet flottant (visite, pose, modification, récupération ou pêche)", "");
        assertResultRow(result, row++, "21/2/2013 16:0", "-1.5334722", "49.800137", "Route vers des systèmes observés", "L'épave n'était pas à 1 milles mais plus près, nous l'avons dépassé sans la voir donc nous ne sommes pas aller sur le système observé, de plus aucun oiseaux n'avaient été vu a proximité de l'épave. Nous avons repris directement notre cap.");
        assertResultRow(result, row++, "21/2/2013 18:11", "-1.3001388", "50.066803", "Fin de veille", "Route toute la nuit.");
        assertResultRow(result, row++, "22/2/2013 7:21", "-0.25013888", "52.23347", "Opération sur objet flottant (visite, pose, modification, récupération ou pêche)", "YFT de 70 kg environ mélangé avec beaucoup de \"marsouins\" impossible de pêcher.");
        assertResultRow(result, row++, "22/2/2013 7:59", "-0.18347223", "52.316803", "Thonier arrivant sur le système détecté", "Albacore de 70 kg environ mélangés avec \"marsouins\" impossible de pêcher!");
        assertResultRow(result, row++, "22/2/2013 8:19", "-0.1501389", "52.366802", "Route vers des systèmes observés", "Vu matte au loin, prenons direction du système observé mais en cours de route les matelots aux jumelles s'apperçoivent qu'il y a des \"marsouins\" avec les YFT, nous ^reprnons notre cap sans nous être rendu sur le système observé");
        assertResultRow(result, row++, "22/2/2013 8:35", "-0.10013889", "52.350136", "Route vers des systèmes observés", "YFT + \"marsouins\", reprise du cap sans nous rendre sur le système observé.");
        assertResultRow(result, row++, "22/2/2013 10:22", "-0.16680557", "52.616802", "Thonier arrivant sur le système détecté", "YFT mélangés avec\"Marsouins\"");
        assertResultRow(result, row++, "22/2/2013 15:4", "-0.26680556", "53.23347", "Opération sur objet flottant (visite, pose, modification, récupération ou pêche)", "Changement d'heur: +1h (heure de l'île maurice, de la réunion et des seychelles!)");
        assertResultRow(result, row++, "22/2/2013 15:13", "-0.26680556", "53.250137", "Route vers des systèmes observés", "YFT + \"Marsouins\" mais vu lors du transit en allant sur le système observé, reprise du cap!");
        assertResultRow(result, row++, "22/2/2013 17:41", "-0.5501389", "53.566803", "Recherche (général)", "Marsouins observé à 2 mioles mais nous n'allons pas sur le système observé");
        assertResultRow(result, row++, "22/2/2013 18:58", "-0.7501389", "53.40014", "Fin de veille", "Route de nuit.");
        assertResultRow(result, row++, "23/2/2013 7:4", "-2.350139", "51.03347", "Thonier arrivant sur le système détecté", "Listao + présence d'un cordage pose d'un radeau activité suivante.");
        assertResultRow(result, row++, "23/2/2013 7:35", "-2.3334723", "50.98347", "Route vers des systèmes observés", "volaille apperçu aux jumelles, nous prenons la direction pour en cours de route on observe que ce sont des listao, on reprend notre cap initial sans se rendre sur le système observé.");
        assertResultRow(result, row++, "23/2/2013 8:41", "-2.2834723", "50.750137", "Route vers des systèmes observés", "vu au sonar!");
        assertResultRow(result, row++, "23/2/2013 12:40", "-2.250139", "50.65014", "Thonier arrivant sur le système détecté", "Plusieurs bancs plus ou moins gros!");
        assertResultRow(result, row++, "23/2/2013 13:1", "-2.250139", "50.633472", "Route vers des systèmes observés", "Poissons allant trop vite re tour sur système observé précédément");
        assertResultRow(result, row++, "23/2/2013 13:28", "-2.2334723", "50.65014", "Thonier arrivant sur le système détecté", "Petit banc et dispérsé");
        assertResultRow(result, row++, "23/2/2013 13:30", "-2.2334723", "50.65014", "Recherche (général)", "");
        assertResultRow(result, row++, "23/2/2013 13:47", "-2.2334723", "50.68347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "23/2/2013 16:6", "-2.2668056", "50.616802", "Recherche (général)", "");
        assertResultRow(result, row++, "23/2/2013 16:34", "-2.2668056", "50.716805", "Route vers des systèmes observés", "En train de mesurer lorsque le système a été apperçu aux jumelles!");
        assertResultRow(result, row++, "23/2/2013 19:1", "-2.300139", "50.68347", "Fin de pêche (remontée du skiff)", "Changement d'heure - 1h");
        assertResultRow(result, row++, "24/2/2013 5:27", "-2.350139", "50.616802", "Recherche (général)", "shiftage du 23.02.2013 de 5h35 à 12h33 puis après calée!");
        assertResultRow(result, row++, "24/2/2013 8:26", "-2.3168056", "50.716805", "Route vers des systèmes observés", "");
        assertResultRow(result, row++, "24/2/2013 8:42", "-2.2668056", "50.73347", "Thonier arrivant sur le système détecté", "Mélangé avec \"Marsouins\" (je pense des dauphins longirostre).");
        assertResultRow(result, row++, "24/2/2013 8:48", "-2.2668056", "50.716805", "Route vers des systèmes observés", "Le poisson se déplace trop vite, nous n'allons pas sur le système observé");
        assertResultRow(result, row++, "24/2/2013 9:59", "-2.2834723", "50.68347", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "24/2/2013 11:27", "-2.5834723", "50.716805", "Route vers des systèmes observés", "Dauphins longirostre. On fait se séparer les thons et les dauphins en suivant ces derniers.\n");
        assertResultRow(result, row++, "24/2/2013 11:30", "-2.5834723", "50.716805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "24/2/2013 11:57", "-2.600139", "50.716805", "Route vers des systèmes observés", "Arrivés sur le système à 12h03 (oublies de le noter sur le formulaire route!)");
        assertResultRow(result, row++, "24/2/2013 15:19", "-2.6168058", "50.766804", "Route vers des systèmes observés", "2 mattes");
        assertResultRow(result, row++, "24/2/2013 15:32", "-2.5834723", "50.800137", "Thonier arrivant sur le système détecté", "Mélange SKJ + YFT.");
        assertResultRow(result, row++, "24/2/2013 15:58", "-2.5168056", "50.816803", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "24/2/2013 16:58", "-2.3334723", "50.73347", "Route vers des systèmes observés", "\"Marsouins\" apperçu après avoir pris la direction du système observé, reprise du cap sans se rendre sur le système.");
        assertResultRow(result, row++, "24/2/2013 18:12", "-2.5168056", "50.616802", "Fin de veille", "Stopper pour la nuit.");
        assertResultRow(result, row++, "25/2/2013 5:52", "-2.7168057", "50.53347", "Route vers des systèmes observés", "Balbya de \"Marsouins\", reprise du cap sans s'être rendu sur le système observé.");
        assertResultRow(result, row++, "25/2/2013 6:12", "-2.7668056", "50.58347", "Recherche (général)", "Shiftage du 23 et 24.02.2013 jusqu'à 14h20");
        assertResultRow(result, row++, "25/2/2013 6:24", "-2.8168056", "50.58347", "Route vers des systèmes observés", "Banc trop petit et inintéressant, recherche général sans se rendre sur le système observé.");
        assertResultRow(result, row++, "25/2/2013 12:35", "-3.4168057", "50.600136", "Route vers des systèmes observés", "\"Marsouins\" vu en coursz de route, reprise directement du cap, sans se rendre sur le système observé");
        assertResultRow(result, row++, "25/2/2013 14:31", "-3.5834723", "50.90014", "Route vers des systèmes observés", "Fin du shiftage du 23 et 24.02.2013 à 14h 20");
        assertResultRow(result, row++, "25/2/2013 16:22", "-3.5668056", "51.050137", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "26/2/2013 6:3", "-4.300139", "52.98347", "Début de pêche (larguage du skiff)", "Sachant que nous débarquons le lendemain aux Seychelles, il n'y aura pas de shiftage, les poissons restent en saumure!");
        assertResultRow(result, row++, "26/2/2013 9:53", "-4.3501387", "53.166805", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "26/2/2013 10:35", "-4.433472", "53.23347", "Thonier arrivant sur le système détecté", "Trombe d'eau apparçu à 5 milles");
        assertResultRow(result, row++, "26/2/2013 12:4", "-4.6001387", "53.366802", "Thonier arrivant sur le système détecté", "");
        assertResultRow(result, row++, "26/2/2013 13:7", "-4.6668053", "53.58347", "Recherche (général)", "Shiftage du 25.02.2013 de 13h à 16h35");
        assertResultRow(result, row++, "26/2/2013 18:22", "-5.3168054", "54.566803", "Fin de veille", "Stopper pour la nuit.");
        assertResultRow(result, row++, "27/2/2013 15:36", "-4.5834723", "55.48347", "Autres (à préciser dans les notes)", "En attente pour rentrer au port avec le pilote");
        assertResultRow(result, row, "27/2/2013 16:22", "-4.6168056", "55.450138", "Au port", "Fin de la première partie de calée.Loch totale marée: 5953.03");
    }
}
