package fr.ird.observe.services.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.topia.ObserveServiceContextTopia;
import org.nuiton.util.DateUtil;

import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveServiceContextTopiaTaiste extends ObserveServiceContextTopia {

    public static final Date DATE = DateUtil.createDate(36, 15, 17, 21, 8, 2016);

    public ObserveServiceContextTopiaTaiste(ObserveServiceInitializer observeServiceInitializer, ObserveServiceFactory mainServiceFactory, ObserveServiceFactory serviceFactory) {
        super(observeServiceInitializer, mainServiceFactory, serviceFactory);
    }

    @Override
    public Date now() {
        return DATE;
    }

}
