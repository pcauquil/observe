package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.service.seine.RouteService;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class RouteServiceTopiaTest extends AbstractServiceTopiaTest {

    protected RouteService service;

    @Before
    public void setUp() throws Exception {

        service = topiaTestMethodResource.newService(RouteService.class);

    }

    @Test
    public void preCreateTest() {

        Form<RouteDto> form = service.preCreate(ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertNotNull(form);
        RouteDto routeDto = form.getObject();

        Assert.assertNotNull(routeDto);

        Assert.assertEquals(0, DateUtil.createDate(28, 2, 2013).compareTo(routeDto.getDate()));
        Assert.assertEquals(new Float(5953.03), routeDto.getStartLogValue());
        Assert.assertNull(routeDto.getEndLogValue());
        Assert.assertNull(routeDto.getComment());



    }

}
