package fr.ird.observe.services.topia.service;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProvider;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.UnauthorizedException;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * FIXME Ca sert à quoi ? et je comprends pas le mot rigth ?
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 *         FIXME IT tests
 */
@Ignore
@DatabaseNameConfiguration(DatabaseName.dataSourceTest)
@DatabasePasswordConfiguration(ObserveTestConfiguration.OBSTUNA_PASSWORD)
public class RigthTest extends AbstractServiceTopiaTest {

    public static final String TRIP_SEINE_ID = "fr.ird.observe.entities.seine.TripSeine#1359167789871#0.6765335978809843";

    public static final String PROGRAM_ID = "fr.ird.observe.entities.referentiel.Program#1239832686262#0.31033946454061234";


    @DatabaseLoginConfiguration(ObserveTestConfiguration.OBSTUNA_ADMIN_LOGIN)
    @Test
    public void testAdminRight() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testUser(true, true, true, true);
    }

    @DatabaseLoginConfiguration(ObserveTestConfiguration.OBSTUNA_TECHNICIEN_LOGIN)
    @Test
    public void testTechnicienAdminRight() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testUser(true, true, true, true);
    }

    @DatabaseLoginConfiguration(ObserveTestConfiguration.OBSTUNA_UTILISATEUR_LOGIN)
    @Test
    public void testUtilisateurRight() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testUser(true, false, true, false);
    }

    @DatabaseLoginConfiguration(ObserveTestConfiguration.OBSTUNA_REFERENTIEL_LOGIN)
    @Test
    public void testReferentielRight() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testUser(false, false, true, false);
    }

    private void testUser(boolean readData, boolean writeData, boolean readReferential, boolean writeReferential) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        ObserveDataSourceConnection dataSourceConnection = topiaTestMethodResource.getDataSourceConnection();

        Assert.assertEquals(readData, dataSourceConnection.canReadData());
        Assert.assertEquals(writeData, dataSourceConnection.canWriteData());
        Assert.assertEquals(readReferential, dataSourceConnection.canReadReferential());
        Assert.assertEquals(writeReferential, dataSourceConnection.canWriteReferential());

    }

    protected ObserveDataSourceConfigurationTopiaPG createDataSourceConfigurationH2(String login) {

        ObserveDataSourceConfigurationTopiaPG configurationTopiaPG = new ObserveDataSourceConfigurationTopiaPG();

        configurationTopiaPG.setJdbcUrl("jdbc:postgresql://localhost:5432/obstuna");
        configurationTopiaPG.setUsername(login);
        configurationTopiaPG.setPassword('a');
        configurationTopiaPG.setAutoMigrate(true);
        configurationTopiaPG.setModelVersion(ObserveMigrationConfigurationProvider.get().getLastVersion());

        return configurationTopiaPG;

    }


    public void testReadData(String login) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        TripSeineService service = topiaTestMethodResource.newService(TripSeineService.class);

        service.loadForm(TRIP_SEINE_ID);
    }

    protected void testWriteData(String login) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        TripSeineService service = topiaTestMethodResource.newService(TripSeineService.class);

        Form<TripSeineDto> tripSeineDtoForm = service.loadForm(TRIP_SEINE_ID);

        service.save(tripSeineDtoForm.getObject());
    }

    protected void testReadReferential(String login) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        ReferentialService service = topiaTestMethodResource.newService(ReferentialService.class);

        service.loadForm(ProgramDto.class, PROGRAM_ID);
    }

    protected void testWriteReferential(String login) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {


        ReferentialService service = topiaTestMethodResource.newService(ReferentialService.class);

        Form<ProgramDto> form = service.loadForm(ProgramDto.class, PROGRAM_ID);

        service.save(form.getObject());
    }

    @Test
    public void testAdminReadData() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testReadData("admin");
    }

    @Test
    public void testAdminWriteData() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testWriteData("admin");
    }

    @Test
    public void testAdminReadReferential() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testReadReferential("admin");
    }

    @Test
    public void testAdminWriteReferential() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testWriteReferential("admin");
    }

    @Test(expected = UnauthorizedException.class)
    public void testReferentielReadData() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testReadData("referentiel");
    }

    @Test(expected = UnauthorizedException.class)
    public void testReferentielWriteData() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testWriteData("referentiel");
    }

    @Test(expected = UnauthorizedException.class)
    public void testReferentielWriteReferential() throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        testWriteReferential("referentiel");
    }


}
