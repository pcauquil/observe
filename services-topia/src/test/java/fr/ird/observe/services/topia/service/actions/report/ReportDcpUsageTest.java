/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;

import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import org.junit.Assert;

/**
 * Pour tester le report {@code dcpUsage}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.9
 */
public class ReportDcpUsageTest extends AbstractReportServiceTopiaTest {

    @Override
    protected String getReportId() {
        return "dcpUsage";
    }

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Utilisation des DCP",
                "Tableau récapitulatif du nombre de DCP visités selon l’action effectuée (visites avec et sans pêche, mis à l’eau, récuperation), et tortues observées"
        );

        assertReportDimension(
                report,
                -1,
                7,
                new String[]{"Type de DCP (Tableau 8)",
                        "Nombre visités",
                        "Nombre pêchés",
                        "Nombre mis à l’eau seuls",
                        "Nombre renforcés par radeau balisé",
                        "Récupéré sans pêche",
                        "Nombre de tortues associées"
                }
        );

        assertReportNbRequests(report, 7);

        ReportRequest[] requests = report.getRequests();
        ReportRequest request;

        request = requests[0];
        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                0,
                0
        );

        request = requests[1];
        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                1,
                0
        );

        request = requests[2];
        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                2,
                0
        );
        request = requests[3];
        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                3,
                0
        );

        request = requests[4];
        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                4,
                0
        );
        request = requests[5];
        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                5,
                0
        );
        request = requests[6];
        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                6,
                0
        );
    }

    @Override
    protected void testReportResult(DataMatrix result) {

        // 4 DCP dans une maree
        assertResultDimension(result, 7, 8, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "10 - Caisse ou grosse planche", "1", "0", "0", "0", "0", null);
        assertResultRow(result, row++, "11 - Cordage, câble", "2", "0", "0", "0", "0", null);
        assertResultRow(result, row++, "13 - Objet de plastique (à préciser dans les notes)", "2", "0", "0", "0", "1", null);
        assertResultRow(result, row++, "14 - Un des antérieurs (du 10 à 13) balisé", "1", "0", "0", "0", "0", null);
        assertResultRow(result, row++, "16 - Radeau ou bouée en dérive", "2", "0", "0", "0", "0", null);
        assertResultRow(result, row++, "3 - Arbre (ou branche)", "3", "0", "0", "0", "0", null);
        assertResultRow(result, row++, "6 - Radeau balisé en dérive (bambou et filet)", "19", "6", "13", "0", "2", null);
        assertResultRow(result, row, "99 - Autre (à préciser dans les notes)", "2", "0", "0", "0", "0", null);

    }
}
