package fr.ird.observe.services.topia.service.actions.synchro;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.Entities;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffService;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffsEngine;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeCallbackResults;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeContext;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeEngine;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeLocalService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeResult;
import fr.ird.observe.services.topia.TopiaTestClassResource;
import fr.ird.observe.services.topia.TopiaTestMethodResource;
import fr.ird.observe.services.topia.binder.BinderEngine;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collection;
import java.util.Set;

/**
 * Created on 04/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class UnidirectionalSynchronizeReferentialTest extends AbstractServiceTopiaTest {

    @ClassRule
    public static final TopiaTestClassResource TOPIA_TEST_CLASS_RESOURCE_CENTRAL = new TopiaTestClassResource(DatabaseClassifier.CENTRAL);

    @Rule
    public final TopiaTestMethodResource topiaTestMethodResourceCentral = new TopiaTestMethodResource(TOPIA_TEST_CLASS_RESOURCE_CENTRAL);

    private UnidirectionalReferentialSynchronizeLocalService localService;
    private ReferentialSynchronizeDiffsEngine diffsEngine;

    @Before
    public void setUp() throws Exception {
        localService = topiaTestMethodResource.newService(UnidirectionalReferentialSynchronizeLocalService.class);
        diffsEngine = new ReferentialSynchronizeDiffsEngine(
                topiaTestMethodResource.newService(ReferentialSynchronizeDiffService.class),
                topiaTestMethodResourceCentral.newService(ReferentialSynchronizeDiffService.class));
    }

    @DatabaseNameConfiguration(DatabaseName.empty_h2)
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(value = DatabaseName.dataForTestSeine, classifier = DatabaseClassifier.CENTRAL)
    @DatabaseVersionConfiguration(value = ObserveTestConfiguration.MODEL_VERSION, classifier = DatabaseClassifier.CENTRAL)
    @Test
    public void testSynchronizeFromEmptyDatabase() {

        UnidirectionalReferentialSynchronizeEngine referentialSynchronizeEngine = new UnidirectionalReferentialSynchronizeEngine(diffsEngine);
        UnidirectionalReferentialSynchronizeContext unidirectionalReferentialSynchronizeContext = referentialSynchronizeEngine.prepareContext(localService);
        UnidirectionalReferentialSynchronizeResult result = referentialSynchronizeEngine.prepareResult(localService, unidirectionalReferentialSynchronizeContext, null);
        referentialSynchronizeEngine.finish(localService, unidirectionalReferentialSynchronizeContext);

        Assert.assertNotNull(result);
        Set<Class<? extends ReferentialDto>> referentialNames = result.getReferentialNames();

        Assert.assertNotNull(referentialNames);
        Assert.assertFalse(referentialNames.isEmpty());

        for (ObserveEntityEnum referenceEntity : Entities.REFERENCE_ENTITIES) {
            if (ObserveEntityEnum.LastUpdateDate.equals(referenceEntity)) {
                continue;
            }
            Class<? extends ReferentialDto> dtoType = BinderEngine.get().getReferentialDtoType(referenceEntity);
            Assert.assertTrue(referentialNames.contains(dtoType));
            Assert.assertTrue(result.getReferentialUpdated(dtoType).isEmpty());
            Assert.assertTrue(result.getReferentialRemoved(dtoType).isEmpty());
            Assert.assertTrue(result.getReferentialReplaced(dtoType).isEmpty());
            Assert.assertFalse(result.getReferentialAdded(dtoType).isEmpty());
        }

    }

    @DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(value = DatabaseName.dataForTestSeine, classifier = DatabaseClassifier.CENTRAL)
    @DatabaseVersionConfiguration(value = ObserveTestConfiguration.MODEL_VERSION, classifier = DatabaseClassifier.CENTRAL)
    @Test
    public void testSynchronizeWithNochange() {

        UnidirectionalReferentialSynchronizeEngine referentialSynchronizeEngine = new UnidirectionalReferentialSynchronizeEngine(diffsEngine);
        UnidirectionalReferentialSynchronizeContext unidirectionalReferentialSynchronizeContext = referentialSynchronizeEngine.prepareContext(localService);
        UnidirectionalReferentialSynchronizeResult result = referentialSynchronizeEngine.prepareResult(localService, unidirectionalReferentialSynchronizeContext, null);
        referentialSynchronizeEngine.finish(localService, unidirectionalReferentialSynchronizeContext);

        Assert.assertNotNull(result);
        Set<Class<? extends ReferentialDto>> referentialNames = result.getReferentialNames();
        Assert.assertNotNull(referentialNames);
        Assert.assertTrue(referentialNames.isEmpty());

        for (ObserveEntityEnum referenceEntity : Entities.REFERENCE_ENTITIES) {
            if (ObserveEntityEnum.LastUpdateDate.equals(referenceEntity)) {
                continue;
            }
            Class<? extends ReferentialDto> dtoType = BinderEngine.get().getReferentialDtoType(referenceEntity);
            Assert.assertTrue(result.getReferentialUpdated(dtoType).isEmpty());
            Assert.assertTrue(result.getReferentialRemoved(dtoType).isEmpty());
            Assert.assertTrue(result.getReferentialReplaced(dtoType).isEmpty());
            Assert.assertTrue(result.getReferentialAdded(dtoType).isEmpty());
        }

    }

    @DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
    @CopyDatabaseConfiguration
    @DatabaseNameConfiguration(value = DatabaseName.dataForTestUnidirectionalReferentialSynchro, classifier = DatabaseClassifier.CENTRAL)
    @DatabaseVersionConfiguration(value = ObserveTestConfiguration.MODEL_VERSION, classifier = DatabaseClassifier.CENTRAL)
    @Test
    public void testSynchronizeWithAllChanges() {


        UnidirectionalReferentialSynchronizeEngine referentialSynchronizeEngine = new UnidirectionalReferentialSynchronizeEngine(diffsEngine);
        UnidirectionalReferentialSynchronizeContext unidirectionalReferentialSynchronizeContext = referentialSynchronizeEngine.prepareContext(localService);

        UnidirectionalReferentialSynchronizeCallbackResults results = new UnidirectionalReferentialSynchronizeCallbackResults();
        results.addCallbackResult(PersonDto.class, "fr.ird.observe.entities.referentiel.Person#1355399844272#0.32586441962131485", "fr.ird.observe.entities.referentiel.Person#1429515754659#0.322074382333085");

        UnidirectionalReferentialSynchronizeResult result = referentialSynchronizeEngine.prepareResult(localService, unidirectionalReferentialSynchronizeContext, results);
        referentialSynchronizeEngine.finish(localService, unidirectionalReferentialSynchronizeContext);

        Assert.assertNotNull(result);
        Set<Class<? extends ReferentialDto>> referentialNames = result.getReferentialNames();
        Assert.assertNotNull(referentialNames);
        Assert.assertFalse(referentialNames.isEmpty());
        {
            Assert.assertTrue(referentialNames.contains(VesselActivitySeineDto.class));
            Assert.assertTrue(result.getReferentialAdded(VesselActivitySeineDto.class).isEmpty());
            Collection<String> referentialUpdated = result.getReferentialUpdated(VesselActivitySeineDto.class);
            Assert.assertFalse(referentialUpdated.isEmpty());
            Assert.assertEquals(1, referentialUpdated.size());
            Assert.assertTrue(referentialUpdated.contains("fr.ird.observe.entities.referentiel.seine.VesselActivitySeine#1239832675369#0.12552908048322586"));

            Assert.assertTrue(result.getReferentialRemoved(VesselActivitySeineDto.class).isEmpty());
            Assert.assertTrue(result.getReferentialReplaced(VesselActivitySeineDto.class).isEmpty());
        }

        {
            Assert.assertTrue(referentialNames.contains(SpeciesDto.class));

            Assert.assertTrue(result.getReferentialAdded(SpeciesDto.class).isEmpty());
            Collection<String> referentialUpdated = result.getReferentialUpdated(SpeciesDto.class);
            Assert.assertFalse(referentialUpdated.isEmpty());
            Assert.assertEquals(1, referentialUpdated.size());
            Assert.assertTrue(referentialUpdated.contains("fr.ird.observe.entities.referentiel.Species#1239832685474#0.8943253454598569"));

            Assert.assertTrue(result.getReferentialRemoved(SpeciesDto.class).isEmpty());
            Assert.assertTrue(result.getReferentialReplaced(SpeciesDto.class).isEmpty());
        }
        {
            Assert.assertTrue(referentialNames.contains(PersonDto.class));
            Assert.assertTrue(result.getReferentialAdded(PersonDto.class).isEmpty());
            Assert.assertTrue(result.getReferentialUpdated(PersonDto.class).isEmpty());
            Collection<String> referentialRemoved = result.getReferentialRemoved(PersonDto.class);
            Assert.assertFalse(referentialRemoved.isEmpty());
            Assert.assertEquals(1, referentialRemoved.size());
            Assert.assertTrue(referentialRemoved.contains("fr.ird.observe.entities.referentiel.Person#1355399844272#0.32586441962131485"));
            Collection<Pair<String, String>> referentialReplaced = result.getReferentialReplaced(PersonDto.class);
            Assert.assertFalse(referentialReplaced.isEmpty());
            Assert.assertEquals(1, referentialReplaced.size());
            Assert.assertTrue(referentialReplaced.contains(Pair.of("fr.ird.observe.entities.referentiel.Person#1355399844272#0.32586441962131485", "fr.ird.observe.entities.referentiel.Person#1429515754659#0.322074382333085")));
        }
        {
            Assert.assertTrue(referentialNames.contains(VesselDto.class));
            Assert.assertTrue(result.getReferentialAdded(VesselDto.class).isEmpty());
            Collection<String> referentialUpdated = result.getReferentialUpdated(VesselDto.class);
            Assert.assertFalse(referentialUpdated.isEmpty());
            Assert.assertEquals(1, referentialUpdated.size());
            Assert.assertTrue(referentialUpdated.contains("fr.ird.observe.entities.referentiel.Vessel#1306847717532#0.7435948873477364"));
            Assert.assertTrue(result.getReferentialRemoved(VesselDto.class).isEmpty());
            Assert.assertTrue(result.getReferentialReplaced(VesselDto.class).isEmpty());
        }

    }
}
