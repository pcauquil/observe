package fr.ird.observe.services.topia.service;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.ObserveEntity;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.topia.TopiaTestClassResource;
import fr.ird.observe.services.topia.TopiaTestMethodResource;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;

import java.util.Date;

/**
 * Created on 26/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.referentiel)
@DatabaseVersionConfiguration(ObserveTestConfiguration.MODEL_VERSION)
@DatabaseLoginConfiguration(ObserveTestConfiguration.H2_LOGIN)
@DatabasePasswordConfiguration(ObserveTestConfiguration.H2_PASSWORD)
public class AbstractServiceTopiaTest {

    @ClassRule
    public static final TopiaTestClassResource TOPIA_TEST_CLASS_RESOURCE = new TopiaTestClassResource();

    @Rule
    public final TopiaTestMethodResource topiaTestMethodResource = new TopiaTestMethodResource(TOPIA_TEST_CLASS_RESOURCE);

    protected <E extends ObserveEntity> Date getLastUpdateDate(Class<E> entityType) {
        try (ObserveTopiaPersistenceContext persistenceContext = topiaTestMethodResource.newPersistenceContext()) {
            return persistenceContext.getLastUpdateDate(entityType);
        }
    }

    protected void loadReferenceSets(ReferentialService referentialService, Form<?> form) {
        topiaTestMethodResource.getReferentialCache().loadReferenceSets(referentialService, form.getReferentialReferenceSetsRequestName());
    }

    protected <R extends ReferentialDto> ReferentialReference<R> getReference(Class<R> type, int index) throws DatabaseNotFoundException, BabModelVersionException, DatabaseConnexionNotAuthorizedException {

        ReferentialService referentialService = topiaTestMethodResource.newService(ReferentialService.class);
        ReferentialReferenceSet<R> referentialReferenceSet = topiaTestMethodResource.getReferentialCache().getReferentialReferenceSet(referentialService, type);
        return referentialReferenceSet.getReferenceByPosition(index);

    }

    protected void assertEntityEqualsReferenceDto(ObserveEntity entity, ReferentialReference referenceDto) {
        if (entity == null) {
            Assert.assertNull(referenceDto);
        } else {
            Assert.assertEquals(entity.getTopiaId(), referenceDto.getId());
        }
    }

    protected void assertReferenceDtoEqualsEntity(ReferentialReference referenceDto, ObserveEntity entity) {
        if (referenceDto == null) {
            Assert.assertNull(entity);
        } else {
            Assert.assertEquals(referenceDto.getId(), entity.getTopiaId());
        }
    }

}
