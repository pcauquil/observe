/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.entity;

import com.opensymphony.xwork2.interceptor.annotations.After;
import fr.ird.observe.ObserveTopiaDaoSupplier;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.constants.ReferenceStatusPersist;
import fr.ird.observe.entities.referentiel.LengthWeightParameter;
import fr.ird.observe.entities.referentiel.LengthWeightParameterTopiaDao;
import fr.ird.observe.entities.referentiel.LengthWeightParameters;
import fr.ird.observe.entities.referentiel.Ocean;
import fr.ird.observe.entities.referentiel.OceanTopiaDao;
import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.SexTopiaDao;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.referentiel.SpeciesTopiaDao;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.util.Date;
import java.util.Optional;

/**
 * Test de la classe {@link LengthWeightParameters}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.8
 */
public class LengthWeightParemetersTest extends AbstractServiceTopiaTest {

    /** Logger */
    private static final Log log = LogFactory.getLog(LengthWeightParemetersTest.class);

    private ObserveTopiaPersistenceContext persistenceContext;

    @Before
    public void setUp() {

        persistenceContext = topiaTestMethodResource.newPersistenceContext();

        SpeciesTopiaDao speciesDAO = persistenceContext.getSpeciesDao();
        OceanTopiaDao oceanDAO = persistenceContext.getOceanDao();
        SexTopiaDao sexDao = persistenceContext.getSexDao();

        Optional<Species> optionalSpecies = Optional.ofNullable(speciesDAO.forFaoCodeEquals("DOL").tryFindUnique().orNull());
        Assert.assertTrue("Could not find species with faoCode: DOL", optionalSpecies.isPresent());

        Optional<Sex> optionalUndeterminedSex = Optional.ofNullable(sexDao.forCodeEquals("0").tryFindUnique().orNull());
        Assert.assertTrue("Could not find sex with code 0 (Undetermined)", optionalUndeterminedSex.isPresent());

        Optional<Sex> optionalMaleSex = Optional.ofNullable(sexDao.forCodeEquals("1").tryFindUnique().orNull());
        Assert.assertTrue("Could not find sex with code 1 (male)", optionalMaleSex.isPresent());

        Optional<Sex> optionalFemaleSex = Optional.ofNullable(sexDao.forCodeEquals("2").tryFindUnique().orNull());
        Assert.assertTrue("Could not find sex with code 2 (female)", optionalFemaleSex.isPresent());

        Date firstStartDate = DateUtil.createDate(1, 1, 2010);
        Date firstEndDate = DateUtil.createDate(31, 12, 2010);
        Date secondStartDate = DateUtil.createDate(1, 1, 2011);

        Optional<Ocean> optionalAtlanticOcean = Optional.ofNullable(oceanDAO.forCodeEquals("1").tryFindUnique().orNull());
        Assert.assertNotNull("Could not find ocean with code 1 (Atlantic)", optionalAtlanticOcean.isPresent());

        Optional<Ocean> optionalIndianOcean = Optional.ofNullable(oceanDAO.forCodeEquals("2").tryFindUnique().orNull());
        Assert.assertNotNull("Could not find ocean with code 2 (Indian)", optionalIndianOcean.isPresent());

        LengthWeightParameterTopiaDao lengthWeightParameterDao = persistenceContext.getLengthWeightParameterDao();

        Species species = optionalSpecies.get();
        Sex maleSex = optionalMaleSex.get();
        Ocean atlanticOcean = optionalAtlanticOcean.get();
        Ocean indianOcean = optionalIndianOcean.get();

        // Ajout parametrage Male / Atlantique (2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, atlanticOcean, maleSex, firstStartDate, firstEndDate);
        // Ajout parametrage Male / Atlantique (après 2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, atlanticOcean, maleSex, secondStartDate, null);
        // Ajout parametrage Male / Indien (2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, indianOcean, maleSex, firstStartDate, firstEndDate);
        // Ajout parametrage Male / Indien (Après 2010)
        createLengthWeightParameter(lengthWeightParameterDao, species, indianOcean, maleSex, secondStartDate, null);

    }

    @After
    public final void tearDown() {
        persistenceContext.close();
    }

    protected static void createLengthWeightParameter(LengthWeightParameterTopiaDao lengthWeightParameterDao,
                                                      Species species,
                                                      Ocean ocean,
                                                      Sex sex,
                                                      Date startDate,
                                                      Date endDate) {

        LengthWeightParameter entity = lengthWeightParameterDao.newInstance();

        entity.setSpecies(species);
        entity.setSex(sex);
        entity.setOcean(ocean);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setStatus(ReferenceStatusPersist.enabled);
        entity.setCoefficients("a=3.8E-5:b=2.78");
        entity.setLengthWeightFormula("a * Math.pow(L, b)");
        entity.setWeightLengthFormula("Math.pow(P/a, 1/b)");
        entity.setLastUpdateDate(new Date());

        lengthWeightParameterDao.create(entity);

    }

    /**
     * Pour tester que l'on récupère la bonne relation.
     * See http://forge.codelutin.com/issues/7628
     */
    @Test
    @CopyDatabaseConfiguration
    public void testGetCorrectLengthWeightParameter() {

        SpeciesTopiaDao speciesDAO = persistenceContext.getSpeciesDao();
        OceanTopiaDao oceanDAO = persistenceContext.getOceanDao();
        SexTopiaDao sexDao = persistenceContext.getSexDao();

        Optional<Species> optionalSpecies = Optional.ofNullable(speciesDAO.forFaoCodeEquals("DOL").tryFindUnique().orNull());
        Assert.assertTrue("Could not find species with faoCode: DOL", optionalSpecies.isPresent());

        Optional<Sex> optionalUndeterminedSex = Optional.ofNullable(sexDao.forCodeEquals("0").tryFindUnique().orNull());
        Assert.assertTrue("Could not find sex with code 0 (Undetermined)", optionalUndeterminedSex.isPresent());

        Optional<Sex> optionalMaleSex = Optional.ofNullable(sexDao.forCodeEquals("1").tryFindUnique().orNull());
        Assert.assertTrue("Could not find sex with code 1 (male)", optionalMaleSex.isPresent());

        Optional<Sex> optionalFemaleSex = Optional.ofNullable(sexDao.forCodeEquals("2").tryFindUnique().orNull());
        Assert.assertTrue("Could not find sex with code 2 (female)", optionalFemaleSex.isPresent());

        Date date1970 = DateUtil.createDate(1, 1, 1970);
        Date date2009 = DateUtil.createDate(1, 1, 2009);
        Date date2010 = DateUtil.createDate(1, 1, 2010);
        Date date2011 = DateUtil.createDate(1, 1, 2011);

        Optional<Ocean> optionalAtlanticOcean = Optional.ofNullable(oceanDAO.forCodeEquals("1").tryFindUnique().orNull());
        Assert.assertNotNull("Could not find ocean with code 1 (Atlantic)", optionalAtlanticOcean.isPresent());

        Optional<Ocean> optionalIndianOcean = Optional.ofNullable(oceanDAO.forCodeEquals("2").tryFindUnique().orNull());
        Assert.assertNotNull("Could not find ocean with code 2 (Indian)", optionalIndianOcean.isPresent());

        Optional<Ocean> optionalPacificOcean = Optional.ofNullable(oceanDAO.forCodeEquals("3").tryFindUnique().orNull());
        Assert.assertNotNull("Could not find ocean with code 3 (Pacific)", optionalPacificOcean.isPresent());

        Species species = optionalSpecies.get();
        Sex undeterminedSex = optionalUndeterminedSex.get();
        Sex maleSex = optionalMaleSex.get();
        Sex femaleSex = optionalFemaleSex.get();
        Ocean atlanticOcean = optionalAtlanticOcean.get();
        Ocean indianOcean = optionalIndianOcean.get();
        Ocean pacificOcean = optionalPacificOcean.get();

        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, null, undeterminedSex, date2009, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, undeterminedSex, undeterminedSex, date2009, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, maleSex, undeterminedSex, date2009, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, femaleSex, undeterminedSex, date2009, date1970);

        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, null, undeterminedSex, date2010, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, undeterminedSex, undeterminedSex, date2010, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, maleSex, maleSex, date2010, date2010);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, femaleSex, undeterminedSex, date2010, date1970);

        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, null, undeterminedSex, date2011, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, undeterminedSex, undeterminedSex, date2011, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, maleSex, maleSex, date2011, date2011);
        assertFoundLengthWeightParameter(persistenceContext, species, atlanticOcean, femaleSex, undeterminedSex, date2011, date1970);

        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, null, undeterminedSex, date2009, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, undeterminedSex, undeterminedSex, date2009, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, maleSex, undeterminedSex, date2009, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, femaleSex, undeterminedSex, date2009, date1970);

        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, undeterminedSex, undeterminedSex, date2010, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, maleSex, maleSex, date2010, date2010);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, femaleSex, undeterminedSex, date2010, date1970);

        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, undeterminedSex, undeterminedSex, date2011, date1970);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, maleSex, maleSex, date2011, date2011);
        assertFoundLengthWeightParameter(persistenceContext, species, indianOcean, femaleSex, undeterminedSex, date2011, date1970);

        assertNotFoundLengthWeightParameter(persistenceContext, species, pacificOcean, undeterminedSex, date2010);
        assertNotFoundLengthWeightParameter(persistenceContext, species, pacificOcean, maleSex, date2010);
        assertNotFoundLengthWeightParameter(persistenceContext, species, pacificOcean, femaleSex, date2010);

    }

    protected void assertFoundLengthWeightParameter(ObserveTopiaDaoSupplier supplier, Species species, Ocean ocean, Sex sex, Sex expectedSex, Date date, Date expectedStartDate) {

        if (log.isInfoEnabled()) {
            log.info("Try to find length weith parameter for species " + species.getFaoCode() + " - ocean " + ocean.getLabel1() + " - sex " + (sex == null ? "null" : sex.getLabel1()) + " at " + date);
        }
        LengthWeightParameter lengthWeightParameter = LengthWeightParameters.findLengthWeightParameter(supplier, species, ocean, sex, date);

        Assert.assertNotNull("length weith parameter not found for species " + species.getFaoCode() + " - ocean " + ocean.getLabel1() + " - sex " + (sex == null ? "null" : sex.getLabel1()) + " at " + date, lengthWeightParameter);
        Assert.assertEquals("Expected sex is " + expectedSex.getLabel1() + " but the one found was " + lengthWeightParameter.getSex().getLabel1(), expectedSex, lengthWeightParameter.getSex());
        Date startDate = new Date(lengthWeightParameter.getStartDate().getTime());
        Assert.assertEquals("Expected startDate is " + expectedStartDate + " but the one found was " + startDate, expectedStartDate, startDate);
    }

    protected void assertNotFoundLengthWeightParameter(ObserveTopiaDaoSupplier supplier, Species species, Ocean ocean, Sex sex, Date date) {

        if (log.isInfoEnabled()) {
            log.info("Try to find length weith parameter for species " + species.getFaoCode() + " - ocean " + ocean.getLabel1() + " - sex " + (sex == null ? "null" : sex.getLabel1()) + " at " + date);
        }
        LengthWeightParameter lengthWeightParameter = LengthWeightParameters.findLengthWeightParameter(supplier, species, ocean, sex, date);

        Assert.assertNull("length weith parameter should not ne found for species " + species.getFaoCode() + " - ocean " + ocean.getLabel1() + " - sex " + (sex == null ? "null" : sex.getLabel1()) + " at " + date, lengthWeightParameter);
    }

}
