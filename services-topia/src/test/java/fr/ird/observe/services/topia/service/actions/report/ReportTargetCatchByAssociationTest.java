/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;

import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import org.junit.Assert;

/**
 * Test le report {@code targetCatchByAssociation}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.9
 */
public class ReportTargetCatchByAssociationTest extends AbstractReportServiceTopiaTest {

    @Override
    protected String getReportId() {
        return "targetCatchByAssociation";
    }

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Capture de thon selon le type d'association (en t)",
                "Afficher la répartitions des captures de thons selon le type d'association"
        );

        assertReportDimension(
                report,
                5,
                7,
                new String[]{"YFT",
                             "SKJ",
                             "BET",
                             "LTA",
                             "FRI",
                             "Autres",
                             "Total"
                },
                "BL sans baleine",
                "BL avec baleine",
                "BO avec requin-baleine",
                "BO sans requin-baleine",
                "Total");

        assertReportNbRequests(report, 4);

        ReportRequest[] requests = report.getRequests();

        assertReportRequestDimension(
                requests[0],
                ReportRequest.RequestLayout.row,
                0,
                0
        );

        assertReportRequestDimension(
                requests[1],
                ReportRequest.RequestLayout.row,
                0,
                1
        );

        assertReportRequestDimension(
                requests[2],
                ReportRequest.RequestLayout.row,
                0,
                2
        );

        assertReportRequestDimension(
                requests[3],
                ReportRequest.RequestLayout.row,
                0,
                3
        );
    }

    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 7, 5, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "455.2", "0.0", "40.0", "0.0", "0.0", "2.2", "497.4");
        assertResultRow(result, row++, "26.0", "0.0", "0.0", "0.0", "0.0", "0.0", "26.0");
        assertResultRow(result, row++, null, null, null, null, null, null, "0.0");
        assertResultRow(result, row++, "32.0", "71.0", "10.0", "0.0", "0.0", "0.0", "113.0");
        assertResultRow(result, row, "513.2", "71.0", "50.0", "0.0", "0.0", "2.2", "636.4");

    }
}
