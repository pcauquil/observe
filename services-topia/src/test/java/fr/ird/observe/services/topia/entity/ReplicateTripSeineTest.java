package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Test;
import org.nuiton.topia.service.sql.batch.actions.ReplicateTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.TopiaSqlTableSelectArgument;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;

/**
 * Created on 29/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class ReplicateTripSeineTest extends ReplicateTestSupport {

    @Test
    public void testReplicateAllTripSeine() throws Exception {
        testReplicate(ObserveFixtures.ALL_TRIP_SEINE_COUNT);
    }

    @Test
    public void testReplicateTripSeine1() throws Exception {
        testReplicate(ObserveFixtures.TRIP_SEINE_1_TABLES_COUNT, ObserveFixtures.TRIP_SEINE_ID_1);
    }

    @Test
    public void testReplicateAllTripSeine2() throws Exception {
        testReplicate(ObserveFixtures.ALL_TRIP_SEINE_COUNT,
                      ObserveFixtures.TRIP_SEINE_ID_1,
                      ObserveFixtures.TRIP_SEINE_ID_2,
                      ObserveFixtures.TRIP_SEINE_ID_3);
    }

    @Test
    public void testReplicateTripSeine2() throws Exception {
        testReplicate(ObserveFixtures.TRIP_SEINE_2_TABLES_COUNT, ObserveFixtures.TRIP_SEINE_ID_2);
    }

    @Test
    public void testReplicateTripSeine3() throws Exception {
        testReplicate(ObserveFixtures.TRIP_SEINE_3_TABLES_COUNT, ObserveFixtures.TRIP_SEINE_ID_3);
    }

    protected void testReplicate(ImmutableMap<String, Long> expectedTablesCount, String... tripIds) throws Exception {

        TopiaSqlTables tables = topiaTestMethodResource.getTopiaApplicationContext().getTripSeineTables();

        ReplicateTablesRequest request
                = createReplicateTablesRequest(DatabaseName.referentiel)
                .setSelectArgument(TopiaSqlTableSelectArgument.of(tripIds))
                .setTables(tables)
                .build();

        testReplicate0(request, expectedTablesCount);

    }

}
