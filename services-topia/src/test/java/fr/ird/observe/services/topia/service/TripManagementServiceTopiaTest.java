package fr.ird.observe.services.topia.service;

import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.services.service.trip.DeleteTripRequest;
import fr.ird.observe.services.service.trip.DeleteTripResult;
import fr.ird.observe.services.service.trip.ExportTripRequest;
import fr.ird.observe.services.service.trip.ExportTripResult;
import fr.ird.observe.services.service.trip.ImportTripRequest;
import fr.ird.observe.services.service.trip.TripManagementService;
import fr.ird.observe.services.topia.ObserveDataSourceConnectionTopiaTaiste;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created on 08/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class TripManagementServiceTopiaTest extends AbstractServiceTopiaTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialServiceTopiaTest.class);

    private TripManagementService tripManagementService;
    private TripSeineService tripSeineService;

    @Before
    public void setUp() throws Exception {

        tripManagementService = topiaTestMethodResource.newService(TripManagementService.class);
        tripSeineService = topiaTestMethodResource.newService(TripSeineService.class);

    }

    @Test
    public void exportTrip() {

        ExportTripRequest request = new ExportTripRequest(false, ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_SEINE_ID_1);
        ExportTripResult result = tripManagementService.exportTrip(request);
        Assert.assertNotNull(result);

    }

    @CopyDatabaseConfiguration
    @Test
    public void deleteTrip() {

        DataReferenceSet<TripSeineDto> allTripSeineBefore = tripSeineService.getAllTripSeine();

        DeleteTripRequest request = new DeleteTripRequest(ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_SEINE_ID_1);
        DeleteTripResult result = tripManagementService.deleteTrip(request);
        Assert.assertNotNull(result);

        DataReferenceSet<TripSeineDto> allTripSeineAfter = tripSeineService.getAllTripSeine();
        Assert.assertTrue(allTripSeineBefore.sizeReference() == allTripSeineAfter.sizeReference() + 1);
    }

    @Test
    public void importTripSeine() throws DatabaseConnexionNotAuthorizedException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException {

        ExportTripRequest request = new ExportTripRequest(false, ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_SEINE_ID_1);
        importTrip(request, true);

    }

    // FIXME Le test met 6 minutes, comprendre pourquoi l'import est aussi long (environ 18000 branchlines)
    @Ignore
    @DatabaseNameConfiguration(DatabaseName.dataForTestLongline)
    @Test
    public void importTripLongline() throws DatabaseConnexionNotAuthorizedException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException {

        ExportTripRequest request = new ExportTripRequest(false, ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_LONGLINE_ID_1);
        importTrip(request, false);

    }

    private void importTrip(ExportTripRequest request, boolean forSeine) throws DatabaseConnexionNotAuthorizedException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException {

        ExportTripResult result = tripManagementService.exportTrip(request);

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.createDataSourceConfigurationH2(getClass(), "importTripTarget");

        try (DataSourceService dataSourceService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class)) {

            DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
            dataSourceCreateConfiguration.setImportReferentialDataSourceConfiguration(topiaTestMethodResource.getDataSourceConfiguration());

            ObserveDataSourceConnection dataSourceConnection = dataSourceService.create(dataSourceConfiguration, dataSourceCreateConfiguration);
            dataSourceConnection = new ObserveDataSourceConnectionTopiaTaiste(dataSourceConnection.getAuthenticationToken());

            TripManagementService tripManagementService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConnection, TripManagementService.class);
            TripSeineService tripSeineService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConnection, TripSeineService.class);
            TripLonglineService tripLonglineService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConnection, TripLonglineService.class);

            int allTripSeineBefore = forSeine ? tripSeineService.getAllTripSeine().sizeReference() :
                    tripLonglineService.getAllTripLongline().sizeReference();

            tripManagementService.importTrip(new ImportTripRequest(result));

            int allTripSeineAfter = forSeine ? tripSeineService.getAllTripSeine().sizeReference() :
                    tripLonglineService.getAllTripLongline().sizeReference();
            Assert.assertTrue(allTripSeineAfter == allTripSeineBefore + 1);

        }

    }

}