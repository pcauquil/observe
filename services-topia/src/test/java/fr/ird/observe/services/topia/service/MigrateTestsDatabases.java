package fr.ird.observe.services.topia.service;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.AddSqlScriptProducerResult;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.SqlScriptProducerService;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created on 25/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@Ignore
@DatabaseVersionConfiguration(ObserveTestConfiguration.PREVIOUS_VERSION)
public class MigrateTestsDatabases extends AbstractServiceTopiaTest {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(MigrateTestsDatabases.class);

    protected SqlScriptProducerService service;

    @Before
    public void setUp() throws Exception {
        service = topiaTestMethodResource.newService(SqlScriptProducerService.class);
    }

    @DatabaseNameConfiguration(DatabaseName.referentiel)
    @Test
    public void migrateReferentielDb() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData();
        migrate(service.produceAddSqlScript(request));
    }

    @DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
    @Test
    public void migrateDataForTestSeineDb() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData();
        migrate(service.produceAddSqlScript(request));
    }

    @DatabaseNameConfiguration(DatabaseName.dataForTestLongline)
    @Test
    public void migrateDataForTestLonglineDb() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData();
        migrate(service.produceAddSqlScript(request));
    }

    @DatabaseNameConfiguration(DatabaseName.empty_h2)
    @Test
    public void migrateEmptyH2Db() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema();
        migrate(service.produceAddSqlScript(request));
    }

    @DatabaseVersionConfiguration(ObserveTestConfiguration.FIRST_VERSION_FOR_REF_SYNCHRO)
    @DatabaseNameConfiguration(DatabaseName.dataForTestUnidirectionalReferentialSynchro)
    @Test
    public void migrateDataForTestUnidirectionalReferentialSynchroDb() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData();
        migrate(service.produceAddSqlScript(request));
    }

    @DatabaseNameConfiguration(DatabaseName.empty_h2)
    @Test
    public void migrateEmptyPgDb() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {
        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forPostgres().addSchema();
        migrate(DatabaseName.empty_pg.name(), service.produceAddSqlScript(request));
    }

    private void migrate(AddSqlScriptProducerResult dump) throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseNotFoundException, BabModelVersionException, DatabaseConnexionNotAuthorizedException {
        migrate(topiaTestMethodResource.getDbName(), dump);
    }

    private void migrate(String dbName, AddSqlScriptProducerResult dump) throws IOException {

        File databasesRootDirectory = topiaTestMethodResource.getDataSourceConfiguration().getDatabaseFile();

        while (!"observe".equals(databasesRootDirectory.getName())) {
            databasesRootDirectory = databasesRootDirectory.getParentFile();
        }

        String sqlFilename = dbName + ".sql.gz";

        File scriptfile = databasesRootDirectory
                .toPath()
                .resolve("test")
                .resolve("src")
                .resolve("main")
                .resolve("resources")
                .resolve("db")
                .resolve(ObserveTestConfiguration.getModelVersion().getVersion())
                .resolve(sqlFilename)
                .toFile();

        if (log.isInfoEnabled()) {
            log.info("Will generate " + dbName + " to " + scriptfile);
        }

        Files.createDirectories(scriptfile.toPath().getParent());

        FileUtils.writeByteArrayToFile(scriptfile, dump.getSqlCode());

    }

}
