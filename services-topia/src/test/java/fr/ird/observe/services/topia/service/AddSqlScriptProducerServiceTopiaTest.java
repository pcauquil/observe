package fr.ird.observe.services.topia.service;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.SqlScriptProducerService;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.StringUtil;
import org.nuiton.util.TimeLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created on 31/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class AddSqlScriptProducerServiceTopiaTest extends AbstractServiceTopiaTest {

    private static final TimeLog timeLog = new TimeLog(AddSqlScriptProducerServiceTopiaTest.class, 10, 1000);

    protected SqlScriptProducerService service;

    @Before
    public void setUp() throws Exception {
        service = topiaTestMethodResource.newService(SqlScriptProducerService.class);
    }

    @DatabaseNameConfiguration(DatabaseName.empty_h2)
    @Test
    public void testProduceSchema() throws Exception {

        produce(AddSqlScriptProducerRequest.forH2().addSchema());
        produce(AddSqlScriptProducerRequest.forPostgres().addSchema());

    }

    @DatabaseNameConfiguration(DatabaseName.referentiel)
    @Test
    public void testProduceReferential() throws Exception {

        produce(AddSqlScriptProducerRequest.forPostgres().addSchema().addReferential());

    }

    @DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
    @Test
    public void testProduceTripSeine() throws Exception {

//        produce(AddSqlScriptProducerRequest.forPostgres().addSchema().addReferential().addAllData());
        produce(AddSqlScriptProducerRequest.forPostgres().addSchema().addReferential().dataIdsToAdd(ObserveFixtures.TRIP_SEINE_IDS));

    }

    @DatabaseNameConfiguration(DatabaseName.dataForTestLongline)
    @Test
    public void testProduceTripLongline() throws Exception {

        produce(AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData());
//        produce(AddSqlScriptProducerRequest.forH2().addSchema().addReferential().dataIdsToAdd(ObserveFixtures.TRIP_LONGLINE_IDS));

    }

    protected void produce(AddSqlScriptProducerRequest request) throws IOException {

        long time = TimeLog.getTime();

        File outputFile = TOPIA_TEST_CLASS_RESOURCE.createTemporaryFile("-out.sql.gz");

        byte[] gzContent = service.produceAddSqlScript(request).getSqlCode();

        try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {
            IOUtils.write(gzContent, fileOutputStream);
        }

        timeLog.log(time, "Output file - " + StringUtil.convertMemory(outputFile.length()) + "\n" + outputFile);
    }
}
