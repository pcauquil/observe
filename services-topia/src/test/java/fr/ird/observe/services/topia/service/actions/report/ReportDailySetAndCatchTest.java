/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;


import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import org.junit.Assert;

/**
 * Pour tester le report {@code dailySetAndCapture}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.9
 */
public class ReportDailySetAndCatchTest extends AbstractReportServiceTopiaTest {

    @Override
    protected String getReportId() {
        return "dailySetAndCatch";
    }

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Nombre de calées et captures journalières d'une marée",
                "Afficher le nombre de calées et les captures journalières d’une calée"
        );

        assertReportDimension(
                report,
                -1,
                3,
                new String[]{"Jour observation",
                             "Nombre de calées",
                             "Captures thon"
                }
        );

        assertReportNbRequests(report, 1);

        ReportRequest[] requests = report.getRequests();
        ReportRequest request = requests[0];

        assertReportRequestDimension(
                request,
                ReportRequest.RequestLayout.row,
                0,
                0
        );
    }

    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 3, 20, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "30/1/2013", "1", "20.0");
        assertResultRow(result, row++, "1/2/2013", "2", null);
        assertResultRow(result, row++, "2/2/2013", "1", null);
        assertResultRow(result, row++, "3/2/2013", "2", null);
        assertResultRow(result, row++, "4/2/2013", "1", null);
        assertResultRow(result, row++, "5/2/2013", "1", "7.8");
        assertResultRow(result, row++, "6/2/2013", "2", "36.0");
        assertResultRow(result, row++, "7/2/2013", "2", "27.0");
        assertResultRow(result, row++, "8/2/2013", "3", "14.0");
        assertResultRow(result, row++, "9/2/2013", "3", "46.0");
        assertResultRow(result, row++, "10/2/2013", "1", "7.0");
        assertResultRow(result, row++, "11/2/2013", "3", "30.41");
        assertResultRow(result, row++, "15/2/2013", "1", "0.07");
        assertResultRow(result, row++, "16/2/2013", "1", "11.005");
        assertResultRow(result, row++, "17/2/2013", "2", "40.7");
        assertResultRow(result, row++, "19/2/2013", "3", "134.701");
        assertResultRow(result, row++, "23/2/2013", "3", "197.44");
        assertResultRow(result, row++, "24/2/2013", "1", "12.0");
        assertResultRow(result, row++, "25/2/2013", "1", "48.075");
        assertResultRow(result, row, "26/2/2013", "1", "13.7");

    }
}
