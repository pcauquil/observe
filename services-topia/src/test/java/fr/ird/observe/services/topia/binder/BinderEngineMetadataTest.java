package fr.ird.observe.services.topia.binder;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import fr.ird.observe.ObserveEntityEnum;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.entities.longline.TdrRecord;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.topia.binder.data.DataBinderSupport;
import fr.ird.observe.services.topia.binder.referential.ReferentialBinderSupport;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Modifier;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 26/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BinderEngineMetadataTest {

    private BinderEngine binderEngine;

    @Before
    public void setUp() {

        binderEngine = BinderEngine.get();

    }

    @Test
    public void testGetBinder() throws Exception {

        binderEngine = BinderEngine.get();
        for (Class<? extends ReferentialDto> dtoType : binderEngine.getReferentialDtoToEntityTypes().keySet()) {

            ReferentialBinderSupport<ObserveReferentialEntity, ? extends ReferentialDto> referentialBinder = binderEngine.getReferentialBinder(dtoType);
            Assert.assertNotNull(referentialBinder);

        }

        for (Class<? extends DataDto> dtoType : binderEngine.getDataDtoToEntityTypes().keySet()) {

            DataBinderSupport<ObserveDataEntity, ? extends DataDto> referentialBinder = binderEngine.getDataBinder(dtoType);
            Assert.assertNotNull("No binder for " + dtoType, referentialBinder);

        }

    }

    @Test
    public void testGetReferentialEntityType() throws Exception {

        ImmutableMap<Class<? extends ReferentialDto>, Class<? extends ObserveReferentialEntity>> referentialDtoToEntityTypes = binderEngine.getReferentialDtoToEntityTypes();

        Assert.assertNotNull(referentialDtoToEntityTypes);
        Assert.assertEquals(59, referentialDtoToEntityTypes.size());

        Set<Class<? extends ObserveReferentialEntity>> entityTypesFromEngine = Sets.newHashSet(referentialDtoToEntityTypes.values());
        Set<Class<? extends ObserveReferentialEntity>> referentialEntityTypesFromTopia = getReferentialEntityTypes();
        Assert.assertEquals(referentialEntityTypesFromTopia.size(), entityTypesFromEngine.size());

        for (Class<? extends ReferentialDto> dtoType : referentialDtoToEntityTypes.keySet()) {

            Class<? extends ObserveReferentialEntity> entityType = binderEngine.getReferentialEntityType(dtoType);
            Assert.assertNotNull(entityType);

        }

    }

    @Test
    public void testGetDataEntityType() throws Exception {

        ImmutableMap<Class<? extends DataDto>, Class<? extends ObserveDataEntity>> dataDtoToEntityTypes = binderEngine.getDataDtoToEntityTypes();
        Assert.assertNotNull(dataDtoToEntityTypes);
        Assert.assertEquals(57, dataDtoToEntityTypes.size());

        Set<Class<? extends ObserveDataEntity>> entityTypesFromEngine = Sets.newConcurrentHashSet(dataDtoToEntityTypes.values());
        Set<Class<? extends ObserveDataEntity>> dataEntityTypesFromTopia = getDataEntityTypesFromTopia();
        Assert.assertEquals(dataEntityTypesFromTopia.size(), entityTypesFromEngine.size());

        for (Class<? extends DataDto> dtoType : dataDtoToEntityTypes.keySet()) {

            Class<ObserveDataEntity> entityType = binderEngine.getDataEntityType(dtoType);
            Assert.assertNotNull(entityType);

        }

    }

    @Test
    public void testGetReferentialDtoType() throws Exception {

        ImmutableMap<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>> referentialDtoToEntityTypes = binderEngine.getReferentialEntityToDtoTypes();

        Assert.assertNotNull(referentialDtoToEntityTypes);
        Assert.assertEquals(2 * 59, referentialDtoToEntityTypes.size());

        for (Class<? extends ObserveReferentialEntity> entityType : referentialDtoToEntityTypes.keySet()) {

            Class<? extends ReferentialDto> dtoType = binderEngine.getReferentialDtoType(entityType);
            Assert.assertNotNull(dtoType);

        }

    }

    @Test
    public void testGetDataDtoType() throws Exception {

        ImmutableMap<Class<? extends ObserveDataEntity>, Class<? extends DataDto>> referentialDtoToEntityTypes = binderEngine.getDataEntityToDtoTypes();

        Assert.assertNotNull(referentialDtoToEntityTypes);
        Assert.assertEquals(2 * 35, referentialDtoToEntityTypes.size());

        for (Class<? extends ObserveDataEntity> entityType : referentialDtoToEntityTypes.keySet()) {

            Class<? extends DataDto> dtoType = binderEngine.getDataDtoType(entityType);
            Assert.assertNotNull(dtoType);

        }

    }

    protected Set<Class<? extends ObserveReferentialEntity>> getReferentialEntityTypes() {

        Set<Class<? extends ObserveReferentialEntity>> result = new LinkedHashSet<>();

        for (ObserveEntityEnum observeEntityEnum : ObserveEntityEnum.values()) {

            Class<? extends TopiaEntity> contract = observeEntityEnum.getContract();
            if (ObserveReferentialEntity.class.isAssignableFrom(contract) && !Modifier.isAbstract(observeEntityEnum.getImplementation().getModifiers())) {

                result.add((Class<? extends ObserveReferentialEntity>) contract);
            }
        }
        return result;

    }

    protected Set<Class<? extends ObserveDataEntity>> getDataEntityTypesFromTopia() {

        Set<Class<? extends ObserveDataEntity>> result = new LinkedHashSet<>();

        for (ObserveEntityEnum observeEntityEnum : ObserveEntityEnum.values()) {

            if (ObserveDataEntity.class.isAssignableFrom(observeEntityEnum.getContract()) && !Modifier.isAbstract(observeEntityEnum.getImplementation().getModifiers())) {
                result.add((Class<? extends ObserveDataEntity>) observeEntityEnum.getContract());
            }
        }

        result.remove(TdrRecord.class);

        return result;

    }

}
