package fr.ird.observe.services.topia.service;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.entities.referentiel.LengthWeightParameter;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.entities.referentiel.Program;
import fr.ird.observe.services.topia.binder.BinderEngine;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.DataNotFoundException;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaDao;

import java.util.Date;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialServiceTopiaTest extends AbstractServiceTopiaTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialServiceTopiaTest.class);

    protected ReferentialService service;

    @Before
    public void setUp() throws Exception {

        service = topiaTestMethodResource.newService(ReferentialService.class);

    }

    @Test
    public void testLoadForm() throws Exception {

        ObserveTopiaPersistenceContext persistenceContext = topiaTestMethodResource.newPersistenceContext();

        for (Class<? extends ReferentialDto> dtoType : ReferentialServiceTopia.getReferentialDtoTypes()) {

            loadToEdit(persistenceContext, dtoType);

        }

    }

    @Test
    @CopyDatabaseConfiguration
    public void testDelete() throws Exception {

        ObserveTopiaPersistenceContext tx = topiaTestMethodResource.newPersistenceContext();

        delete(tx, ProgramDto.class, Program.class);
        delete(tx, LengthWeightParameterDto.class, LengthWeightParameter.class);

    }

    protected <D extends ReferentialDto, E extends ObserveReferentialEntity> void loadToEdit(ObserveTopiaPersistenceContext persistenceContext, Class<D> dtoType) {

        Class<E> entityType = BinderEngine.get().getReferentialEntityType(dtoType);

        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        long expectedCount = dao.count();

        if (log.isInfoEnabled()) {
            log.info("Test referential of type: " + dtoType.getSimpleName() + " expected count: " + expectedCount);
        }

        for (E o : dao) {

            if (log.isDebugEnabled()) {
                log.debug("Load to edit entity: " + o.getTopiaId());
            }

            Form formDto = service.loadForm(dtoType, o.getTopiaId());
            Assert.assertNotNull(formDto);
            Assert.assertEquals(dtoType, formDto.getType());

            IdDto form = formDto.getObject();
            Assert.assertNotNull(form);
            Assert.assertEquals(o.getTopiaId(), form.getId());

        }
    }

    protected <D extends ReferentialDto, E extends ObserveReferentialEntity> void delete(ObserveTopiaPersistenceContext persistenceContext, Class<D> dtoType, Class<E> entityType) {

        TopiaDao<E> dao = persistenceContext.getDao(entityType);
        long expectedCount = dao.count();

        E entity = dao.iterator().next();
        if (log.isInfoEnabled()) {
            log.info("Test delete: " + entity.getTopiaId());
        }

        Form form = service.loadForm(dtoType, entity.getTopiaId());
        Assert.assertNotNull(form);
        Assert.assertNotNull(form.getObject());
        Assert.assertNotNull(form.getObject().getId());

        Date lastUpdateBefore = persistenceContext.getLastUpdateDate(entityType);

        service.delete(dtoType, entity.getTopiaId());

        Assert.assertEquals(expectedCount - 1, dao.count());

        //FIXME On ne voit pas la date de mise à jour ici? alors que le dao est ok
//        Date lastUpdateAfter = persistenceContext.getLastUpdateDate(entityType);
        Date lastUpdateAfter = getLastUpdateDate(entityType);
        Assert.assertNotEquals(lastUpdateBefore, lastUpdateAfter);

        try {
            service.loadForm(dtoType, entity.getTopiaId());
            Assert.fail();
        } catch (DataNotFoundException e) {
            Assert.assertTrue(true);
        }

    }
}
