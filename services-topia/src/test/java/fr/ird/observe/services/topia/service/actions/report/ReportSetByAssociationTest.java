/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;

import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.actions.report.ReportRequest;
import org.junit.Assert;

/**
 * Pour tester le report {@code setByAssociation}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.9
 */
public class ReportSetByAssociationTest extends AbstractReportServiceTopiaTest {

    @Override
    protected String getReportId() {
        return "setByAssociation";
    }

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Nombre de calées selon le type d’association",
                "Afficher la répartitions des calées selon le type d’association l’issue du coup. Les coups sur BL baleine et BO requin-baleine sont spécifiés."
        );

        assertReportDimension(
                report,
                3,
                5,
                new String[]{"BL sans baleine",
                             "BL avec baleine",
                             "BO avec requin-baleine",
                             "BO sans requin-baleine",
                             "Total"
                },
                "Coups positifs", "Coups nuls", "Total");

        assertReportNbRequests(report, 8);

        ReportRequest[] requests = report.getRequests();

        assertReportRequestDimension(
                requests[0],
                ReportRequest.RequestLayout.column,
                0,
                0
        );
        assertReportRequestDimension(
                requests[1],
                ReportRequest.RequestLayout.column,
                0,
                1
        );
        assertReportRequestDimension(
                requests[2],
                ReportRequest.RequestLayout.column,
                1,
                0
        );
        assertReportRequestDimension(
                requests[3],
                ReportRequest.RequestLayout.column,
                1,
                1
        );
        assertReportRequestDimension(
                requests[4],
                ReportRequest.RequestLayout.column,
                2,
                0
        );
        assertReportRequestDimension(
                requests[5],
                ReportRequest.RequestLayout.column,
                2,
                1
        );
        assertReportRequestDimension(
                requests[6],
                ReportRequest.RequestLayout.column,
                3,
                0
        );
        assertReportRequestDimension(
                requests[7],
                ReportRequest.RequestLayout.column,
                3,
                1
        );
    }

    @Override
    protected void testReportResult(DataMatrix result) {
        assertResultDimension(result, 5, 3, 0, 0);

        int row = 0;
        assertResultRow(result, row++, "19" ,"1" ,"0" ,"6" ,"26");
        assertResultRow(result, row++, "9", "0", "0", "0", "9");
        assertResultRow(result, row, "28" ,"1" ,"0" ,"6" ,"35");

    }
}
