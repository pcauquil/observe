package fr.ird.observe.services.topia;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.migration.ObserveMigrationConfigurationProvider;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.test.TestHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

import java.io.File;
import java.nio.file.Path;

/**
 * Created on 26/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DataSourcesForTestManager {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataSourcesForTestManager.class);

    /**
     * Root path of shared databases.
     */
    private static Path sharedDatabasesRootPath;

    private Path getSharedDatabasesRootPath() {
        if (sharedDatabasesRootPath == null) {
            sharedDatabasesRootPath = TestHelper.getCommonsDir().toPath();
        }
        return sharedDatabasesRootPath;
    }

    private Path getDatabasePath(Path rootPath, Version dbVersion, String dbName) {
        return rootPath.resolve(dbVersion.getValidName()).resolve(dbName);
    }

    public ObserveDataSourceConfigurationTopiaH2 createSharedDataSourceConfigurationH2(Version dbVersion, String dbName, String login, char[] password) {
        Path databasePath = getDatabasePath(getSharedDatabasesRootPath(), dbVersion, dbName);
        return createDataSourceConfigurationH2(databasePath.toFile(), dbVersion, dbName, login, password);
    }

    public ObserveDataSourceConfigurationTopiaH2 createDataSourceConfigurationH2(File localDbFile, Version dbVersion, String dbName, String login, char[] password) {

        ObserveDataSourceConfigurationTopiaH2 configurationTopiaH2 = new ObserveDataSourceConfigurationTopiaH2();

        if (log.isDebugEnabled()) {
            log.debug("db directory: " + localDbFile);
        }
        configurationTopiaH2.setLabel("Commons database#" + dbName);
        configurationTopiaH2.setUsername(login);
        configurationTopiaH2.setPassword(password);
        configurationTopiaH2.setDirectory(localDbFile);
        configurationTopiaH2.setDbName("obstuna");
        configurationTopiaH2.setAutoMigrate(true);
        configurationTopiaH2.setModelVersion(dbVersion);
        return configurationTopiaH2;

    }

    public ObserveDataSourceConfigurationTopiaH2 createDataSourceConfigurationH2(File testDirectory, Class<?> testClass, String dbName, String login, char[] password) {

        ObserveDataSourceConfigurationTopiaH2 configurationTopiaH2 = new ObserveDataSourceConfigurationTopiaH2();

        File localDbFile = new File(testDirectory, dbName);

        if (log.isInfoEnabled()) {
            log.info("db directory: " + localDbFile);
        }
        configurationTopiaH2.setLabel(testClass.getSimpleName() + "#" + dbName);
        configurationTopiaH2.setUsername(login);
        configurationTopiaH2.setPassword(password);
        configurationTopiaH2.setDirectory(localDbFile);
        configurationTopiaH2.setDbName("obstuna");
        configurationTopiaH2.setAutoMigrate(true);
        configurationTopiaH2.setModelVersion(ObserveMigrationConfigurationProvider.get().getLastVersion());
        return configurationTopiaH2;

    }

}
