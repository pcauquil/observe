package fr.ird.observe.services.topia.entity;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Test;
import org.nuiton.topia.service.sql.batch.actions.ReplicateTablesRequest;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;

/**
 * Created on 29/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.referentiel)
public class ReplicateReferentialsTest extends ReplicateTestSupport {

    @Test
    public void testReplicate() throws Exception {

        TopiaSqlTables tables = topiaTestMethodResource.getTopiaApplicationContext().getReferentialTables();

        ReplicateTablesRequest request
                = createReplicateTablesRequest(DatabaseName.empty_h2)
                .setTables(tables)
                .build();

        testReplicate0(request, ObserveFixtures.REFERENTIAL_TABLES_COUNT);

    }

}
