package fr.ird.observe.services.topia.service.actions.validate;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.service.actions.validate.ValidateDataRequest;
import fr.ird.observe.services.service.actions.validate.ValidateDataResult;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsRequest;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsResult;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDto;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtoType;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDto;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.OrganismDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesGroupDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.services.dto.referential.VesselTypeDto;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.EncounterTypeDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.HookPositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.ItemHorizontalPositionDto;
import fr.ird.observe.services.dto.referential.longline.ItemVerticalPositionDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksColorDto;
import fr.ird.observe.services.dto.referential.longline.LightsticksTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.dto.referential.longline.MaturityStatusDto;
import fr.ird.observe.services.dto.referential.longline.MitigationTypeDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.referential.longline.SettingShapeDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.StomacFullnessDto;
import fr.ird.observe.services.dto.referential.longline.TripTypeDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.referential.seine.DetectionModeDto;
import fr.ird.observe.services.dto.referential.seine.ObjectFateDto;
import fr.ird.observe.services.dto.referential.seine.ObjectOperationDto;
import fr.ird.observe.services.dto.referential.seine.ObjectTypeDto;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForNullSetDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.validator.NuitonValidatorScope;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class ValidateServiceTopiaTest extends AbstractServiceTopiaTest {

    protected ValidateService service;

    @Before
    public void setUp() throws Exception {
        service = topiaTestMethodResource.newService(ValidateService.class);
    }

    @Test
    public void testValidateReferantials() throws Exception {

        ValidateReferentialsRequest request = new ValidateReferentialsRequest();

        request.setValidationContext(ValidateService.SERVICE_VALIDATION_CONTEXT);
        request.setScopes(ImmutableSet.copyOf(NuitonValidatorScope.values()));
        request.setReferentialTypes(ObserveServiceTopia.getReferentialDtoTypes());

        ValidateReferentialsResult result = service.validateReferentials(request);
        Assert.assertNotNull(result);
        ImmutableMap<Class<? extends ReferentialDto>, ValidateResultForDtoType> resultByType = result.getResultByType();
        Assert.assertNotNull(resultByType);
        Assert.assertEquals(59, resultByType.size());

//        for (Map.Entry<Class<? extends ReferentialDto>, ValidateResultForDtoType> entry : resultByType.entrySet()) {
//            System.out.println("assertValidateResultForReferentialDtoType(resultByType, " + entry.getKey().getName() + ".class, " + entry.getValue().getValidationResultForDto().size() + ");");
//        }

        assertValidateResultForReferentialDtoType(resultByType, CountryDto.class, 53);
        assertValidateResultForReferentialDtoType(resultByType, FpaZoneDto.class, 41);
        assertValidateResultForReferentialDtoType(resultByType, GearCaracteristicDto.class, 22);
        assertValidateResultForReferentialDtoType(resultByType, GearCaracteristicTypeDto.class, 6);
        assertValidateResultForReferentialDtoType(resultByType, GearDto.class, 26);
        assertValidateResultForReferentialDtoType(resultByType, HarbourDto.class, 74);
        assertValidateResultForReferentialDtoType(resultByType, LengthWeightParameterDto.class, 352);
        assertValidateResultForReferentialDtoType(resultByType, OceanDto.class, 3);
        assertValidateResultForReferentialDtoType(resultByType, OrganismDto.class, 8);
        assertValidateResultForReferentialDtoType(resultByType, PersonDto.class, 390);
        assertValidateResultForReferentialDtoType(resultByType, ProgramDto.class, 27);
        assertValidateResultForReferentialDtoType(resultByType, SexDto.class, 5);
        assertValidateResultForReferentialDtoType(resultByType, SpeciesDto.class, 275);
        assertValidateResultForReferentialDtoType(resultByType, SpeciesGroupDto.class, 8);
        assertValidateResultForReferentialDtoType(resultByType, SpeciesListDto.class, 5);
        assertValidateResultForReferentialDtoType(resultByType, VesselDto.class, 977);
        assertValidateResultForReferentialDtoType(resultByType, VesselSizeCategoryDto.class, 13);
        assertValidateResultForReferentialDtoType(resultByType, VesselTypeDto.class, 14);

        assertValidateResultForReferentialDtoType(resultByType, BaitHaulingStatusDto.class, 5);
        assertValidateResultForReferentialDtoType(resultByType, BaitSettingStatusDto.class, 5);
        assertValidateResultForReferentialDtoType(resultByType, BaitTypeDto.class, 16);
        assertValidateResultForReferentialDtoType(resultByType, CatchFateLonglineDto.class, 5);
        assertValidateResultForReferentialDtoType(resultByType, EncounterTypeDto.class, 12);
        assertValidateResultForReferentialDtoType(resultByType, HealthnessDto.class, 6);
        assertValidateResultForReferentialDtoType(resultByType, HookPositionDto.class, 13);
        assertValidateResultForReferentialDtoType(resultByType, HookSizeDto.class, 22);
        assertValidateResultForReferentialDtoType(resultByType, HookTypeDto.class, 13);
        assertValidateResultForReferentialDtoType(resultByType, ItemHorizontalPositionDto.class, 3);
        assertValidateResultForReferentialDtoType(resultByType, ItemVerticalPositionDto.class, 3);
        assertValidateResultForReferentialDtoType(resultByType, LightsticksColorDto.class, 7);
        assertValidateResultForReferentialDtoType(resultByType, LightsticksTypeDto.class, 2);
        assertValidateResultForReferentialDtoType(resultByType, LineTypeDto.class, 8);
        assertValidateResultForReferentialDtoType(resultByType, MaturityStatusDto.class, 12);
        assertValidateResultForReferentialDtoType(resultByType, MitigationTypeDto.class, 15);
        assertValidateResultForReferentialDtoType(resultByType, SensorBrandDto.class, 4);
        assertValidateResultForReferentialDtoType(resultByType, SensorDataFormatDto.class, 2);
        assertValidateResultForReferentialDtoType(resultByType, SensorTypeDto.class, 4);
        assertValidateResultForReferentialDtoType(resultByType, SettingShapeDto.class, 6);
        assertValidateResultForReferentialDtoType(resultByType, SizeMeasureTypeDto.class, 17);
        assertValidateResultForReferentialDtoType(resultByType, StomacFullnessDto.class, 7);
        assertValidateResultForReferentialDtoType(resultByType, TripTypeDto.class, 3);
        assertValidateResultForReferentialDtoType(resultByType, VesselActivityLonglineDto.class, 5);
        assertValidateResultForReferentialDtoType(resultByType, WeightMeasureTypeDto.class, 3);

        assertValidateResultForReferentialDtoType(resultByType, DetectionModeDto.class, 10);
        assertValidateResultForReferentialDtoType(resultByType, ObjectFateDto.class, 9);
        assertValidateResultForReferentialDtoType(resultByType, ObjectOperationDto.class, 4);
        assertValidateResultForReferentialDtoType(resultByType, ObjectTypeDto.class, 23);
        assertValidateResultForReferentialDtoType(resultByType, ObservedSystemDto.class, 21);
        assertValidateResultForReferentialDtoType(resultByType, ReasonForDiscardDto.class, 5);
        assertValidateResultForReferentialDtoType(resultByType, ReasonForNoFishingDto.class, 13);
        assertValidateResultForReferentialDtoType(resultByType, ReasonForNullSetDto.class, 10);
        assertValidateResultForReferentialDtoType(resultByType, SpeciesFateDto.class, 9);
        assertValidateResultForReferentialDtoType(resultByType, SpeciesStatusDto.class, 3);
        assertValidateResultForReferentialDtoType(resultByType, SurroundingActivityDto.class, 8);
        assertValidateResultForReferentialDtoType(resultByType, TransmittingBuoyOperationDto.class, 3);
        assertValidateResultForReferentialDtoType(resultByType, TransmittingBuoyTypeDto.class, 12);
        assertValidateResultForReferentialDtoType(resultByType, VesselActivitySeineDto.class, 23);
        assertValidateResultForReferentialDtoType(resultByType, WeightCategoryDto.class, 101);
        assertValidateResultForReferentialDtoType(resultByType, WindDto.class, 13);

    }

    @Test
    public void testValidateTripSeines() throws Exception {

        ValidateDataRequest request = new ValidateDataRequest();

        request.setValidationContext(ValidateService.SERVICE_VALIDATION_CONTEXT);
        request.setScopes(ImmutableSet.copyOf(NuitonValidatorScope.values()));
        request.setDataIds(ImmutableSet.of(ObserveFixtures.TRIP_SEINE_ID_1, ObserveFixtures.TRIP_SEINE_ID_2));

        ValidateDataResult result = service.validateData(request);
        Assert.assertNotNull(result);

        ImmutableMap<Class<? extends IdDto>, ValidateResultForDtoType> resultByType = result.getResultByType();
        Assert.assertNotNull(resultByType);
        Assert.assertEquals(7, resultByType.size());

//        for (Map.Entry<Class<? extends IdDto>, ValidateResultForDtoType> entry : resultByType.entrySet()) {
//            System.out.println("assertValidateResultForDtoType(resultByType, " + entry.getKey().getName() + ".class, " + entry.getValue().getValidationResultForDto().size() + ");");
//        }

        assertValidateResultForDtoType(resultByType, TripSeineDto.class, 2);
        assertValidateResultForDtoType(resultByType, RouteDto.class, 55);
        assertValidateResultForDtoType(resultByType, ActivitySeineDto.class, 1516);
        assertValidateResultForDtoType(resultByType, FloatingObjectDto.class, 99);
        assertValidateResultForDtoType(resultByType, TransmittingBuoyDto.class, 42);
        assertValidateResultForDtoType(resultByType, SetSeineDto.class, 35);
        assertValidateResultForDtoType(resultByType, NonTargetCatchDto.class, 3);

    }

    protected <D extends ReferentialDto> void assertValidateResultForReferentialDtoType(ImmutableMap<Class<? extends ReferentialDto>, ValidateResultForDtoType> resultByType, Class<D> dtoType, int expectedCount) {

        ValidateResultForDtoType<D> validateResultForDtoType = resultByType.get(dtoType);
        Assert.assertNotNull(validateResultForDtoType);
        ImmutableSet<ValidateResultForDto<D>> validateResultForDtos = validateResultForDtoType.getValidateResultForDto();
        Assert.assertNotNull(validateResultForDtos);
        Assert.assertEquals(expectedCount, validateResultForDtos.size());
        for (ValidateResultForDto<D> validateResultForDto : validateResultForDtos) {
            Assert.assertNotNull(validateResultForDto.getMessages());
            Assert.assertFalse(validateResultForDto.getMessages().isEmpty());
        }

    }

    protected <D extends IdDto> void assertValidateResultForDtoType(ImmutableMap<Class<? extends IdDto>, ValidateResultForDtoType> resultByType, Class<D> dtoType, int expectedCount) {

        ValidateResultForDtoType<D> validateResultForDtoType = resultByType.get(dtoType);
        Assert.assertNotNull(validateResultForDtoType);
        ImmutableSet<ValidateResultForDto<D>> validateResultForDtos = validateResultForDtoType.getValidateResultForDto();
        Assert.assertNotNull(validateResultForDtos);
        Assert.assertEquals(expectedCount, validateResultForDtos.size());
        for (ValidateResultForDto<D> validateResultForDto : validateResultForDtos) {
            Assert.assertNotNull(validateResultForDto.getMessages());
            Assert.assertFalse(validateResultForDto.getMessages().isEmpty());
        }

    }

}
