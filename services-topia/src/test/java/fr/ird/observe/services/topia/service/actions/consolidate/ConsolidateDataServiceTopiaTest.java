package fr.ird.observe.services.topia.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataRequest;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataResult;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created on 29/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class ConsolidateDataServiceTopiaTest extends AbstractServiceTopiaTest {

    protected ConsolidateDataService service;

    @Before
    public void setUp() throws Exception {
        service = topiaTestMethodResource.newService(ConsolidateDataService.class);
    }

    @Test
    @CopyDatabaseConfiguration
    public void testConsolidateTripSeines() throws Exception {

        ObserveTopiaPersistenceContext persistenceContext = topiaTestMethodResource.newPersistenceContext();

        List<String> tripSeineIds = persistenceContext.getTripSeineDao().findAllIds();

        ConsolidateTripSeineDataRequest request = new ConsolidateTripSeineDataRequest();
        request.setTripSeineIds(ImmutableSet.copyOf(tripSeineIds));
        request.setFailIfLenghtWeightParameterNotFound(false);

        ImmutableSet<ConsolidateTripSeineDataResult> results = service.consolidateTripSeines(request);
        Assert.assertNotNull(results);
        //FIXME faire des asserts sur le résultat
        Assert.assertEquals(3, results.size());

    }
}
