package fr.ird.observe.services.topia;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.ObserveTopiaApplicationContext;
import fr.ird.observe.ObserveTopiaPersistenceContext;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.ObserveReferentialCache;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.TestMethodResourceSupport;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.io.File;
import java.util.Objects;


/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TopiaTestMethodResource extends TestMethodResourceSupport<TopiaTestClassResource> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TopiaTestMethodResource.class);

    private ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;

    private ObserveDataSourceConnection dataSourceConnection;

    private ObserveReferentialCache referentialCache;

    public TopiaTestMethodResource(TopiaTestClassResource topiaTestClassResource) {
        super(topiaTestClassResource);
    }

    public <S extends ObserveService> S newService(Class<S> serviceType) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        ObserveDataSourceConnection dataSourceConnection = getDataSourceConnection();

        if (dataSourceConnection == null) {

            ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = getDataSourceConfiguration();

            DataSourceService dataSourceService = testClassResource.newService(dataSourceConfiguration, DataSourceService.class);

            dataSourceConnection = dataSourceService.open(dataSourceConfiguration);

            dataSourceConnection = new ObserveDataSourceConnectionTopiaTaiste(dataSourceConnection.getAuthenticationToken());

            setDataSourceConnection(dataSourceConnection);
        }

        return testClassResource.newService(dataSourceConnection, serviceType);
    }


    public ObserveDataSourceConfigurationTopiaH2 getDataSourceConfiguration() {
        return dataSourceConfiguration;
    }

    public ObserveDataSourceConnection getDataSourceConnection() {
        return dataSourceConnection;
    }

    public void setDataSourceConnection(ObserveDataSourceConnection dataSourceConnection) {
        this.dataSourceConnection = dataSourceConnection;
    }

    public ObserveReferentialCache getReferentialCache() {
        if (referentialCache == null) {
            referentialCache = new ObserveReferentialCache();
        }
        return referentialCache;
    }

    public ObserveTopiaPersistenceContext newPersistenceContext() {
        return getTopiaApplicationContext().newPersistenceContext();
    }

    public ObserveTopiaApplicationContext getTopiaApplicationContext() {
        return ObserveTopiaApplicationContextFactory.getOrCreateTopiaApplicationContext(dataSourceConfiguration);
    }

    public <E extends TopiaEntity> E findById(Class<E> entityType, String id) {

        TopiaPersistenceContext persistenceContext = newPersistenceContext();

        TopiaDao<E> dao = persistenceContext.getDao(entityType);

        return dao.forTopiaIdEquals(id).findUnique();

    }

    public <E extends TopiaEntity> boolean exists(Class<E> entityType, String id) {

        TopiaPersistenceContext persistenceContext = newPersistenceContext();

        TopiaDao<E> dao = persistenceContext.getDao(entityType);

        return dao.forTopiaIdEquals(id).exists();

    }

    public ObserveDataSourceConfigurationTopiaH2 createDataSourceConfigurationH2(Class<?> testClass, String dbName) {

        return testClassResource.getDataSourcesForTestManager().createDataSourceConfigurationH2(
                getTestDirectory(), testClass, dbName, getLogin(), getPassword());

    }

    @Override
    protected void before(Description description) throws Throwable {

        super.before(description);

        Objects.requireNonNull(getDbName(), "Pas de nom de base spécifié");
        Objects.requireNonNull(getDbVersion(), "Pas de version de base spécifié");
        Objects.requireNonNull(getLogin(), "Pas de login spécifié");
        Objects.requireNonNull(getPassword(), "Pas de password spécifié");

        CopyDatabaseConfiguration copyDatabaseConfiguration = ObserveTestConfiguration.getCopyDatabaseConfigurationAnnotation(testClassMethod, testClassResource.getClassifier());
        boolean useSharedDatabase = copyDatabaseConfiguration == null;

        File databasePath = useSharedDatabase
                ? null
                : getTestDirectory().toPath().resolve("localDb").toFile();

        dataSourceConfiguration = testClassResource.createDataSourceConfiguration(getDbVersion(), getDbName(), databasePath, getLogin(), getPassword());

    }

    @Override
    protected void after(Description description) {

        super.after(description);

        if (referentialCache != null) {
            referentialCache.close();
        }
        testClassResource.closeServiceFactory();

    }

}
