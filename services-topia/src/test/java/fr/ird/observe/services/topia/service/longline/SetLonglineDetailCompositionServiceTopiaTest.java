package fr.ird.observe.services.topia.service.longline;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.Basket;
import fr.ird.observe.entities.longline.Branchline;
import fr.ird.observe.entities.longline.Section;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.longline.BaitHaulingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import fr.ird.observe.services.dto.referential.longline.LineTypeDto;
import fr.ird.observe.services.service.longline.SetLonglineDetailCompositionService;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestLongline)
public class SetLonglineDetailCompositionServiceTopiaTest extends AbstractServiceTopiaTest {

    protected SetLonglineDetailCompositionService service;

    @Before
    public void setUp() throws Exception {

        service = topiaTestMethodResource.newService(SetLonglineDetailCompositionService.class);

    }

    @Test
    public void loadToEditTest() {

        Form<SetLonglineDetailCompositionDto> form = service.loadForm(ObserveFixtures.SET_LONGLINE_ID_1);

        Assert.assertNotNull(form);

        SetLonglineDetailCompositionDto compositionDto = form.getObject();
        Assert.assertNotNull(compositionDto);

        Assert.assertEquals(18, compositionDto.sizeSection());

        SectionDto sectionDto = compositionDto.getSection(0);
        Assert.assertEquals(new Integer(1), sectionDto.getSettingIdentifier());
        Assert.assertEquals(new Integer(18), sectionDto.getHaulingIdentifier());
        Assert.assertEquals(13, sectionDto.sizeBasket());

        BasketDto basketDto = sectionDto.getBasket(0);
        Assert.assertEquals(new Integer(1), basketDto.getSettingIdentifier());
        Assert.assertEquals(new Integer(13), basketDto.getHaulingIdentifier());
        Assert.assertEquals(6, basketDto.sizeBranchline());

        BranchlineDto branchlineDto = basketDto.getBranchline(0);
        Assert.assertEquals(new Integer(1), branchlineDto.getSettingIdentifier());
        Assert.assertEquals(new Integer(6), branchlineDto.getHaulingIdentifier());
    }

    @Test
    @CopyDatabaseConfiguration
    public void saveTest() {

        Form<SetLonglineDetailCompositionDto> form = service.loadForm(ObserveFixtures.SET_LONGLINE_ID_1);
        SetLonglineDetailCompositionDto compositionDto = form.getObject();

        SectionDto sectionDto = compositionDto.getSection(4);

        BasketDto basketDto = sectionDto.getBasket(3);
        basketDto.setFloatline1Length(4.4f);
        basketDto.setFloatline2Length(5.5f);

        BranchlineDto branchlineDto = basketDto.getBranchline(0);
        branchlineDto.setBranchlineLength(1.2f);
        branchlineDto.setTracelineLength(2.8f);

        //FIXME
//        ReferentialReference<LineTypeDto> topType = formDto.getReferentialReferenceById(LineTypeDto.class, BranchlineDto.PROPERTY_TOP_TYPE, "fr.ird.observe.entities.referentiel.longline.LineType#1239832686157#0.1");
        ReferentialReference<LineTypeDto> topType = null;
//        branchlineDto.setTopType(topType);
//        ReferentialReference<LineTypeDto> tracelineType = formDto.getReferentialReferenceById(LineTypeDto.class, BranchlineDto.PROPERTY_TRACELINE_TYPE, "fr.ird.observe.entities.referentiel.longline.LineType#1239832686157#0.3");
        ReferentialReference<LineTypeDto> tracelineType = null;
//        branchlineDto.setTracelineType(tracelineType);
        branchlineDto.setDepthRecorder(true);
        branchlineDto.setHookLost(false);
        branchlineDto.setTraceCutOff(false);
        branchlineDto.setTimer(true);
        branchlineDto.setTimeSinceContact(25478L);
        branchlineDto.setTimerTimeOnBoard(DateUtil.createDate(14, 57, 10, 2, 8, 2009));
        branchlineDto.setWeightedSnap(true);
        branchlineDto.setSnapWeight(0.8f);
        branchlineDto.setWeightedSwivel(true);
        branchlineDto.setSwivelWeight(0.45f);

        ReferentialReference<HookTypeDto> hookType = null;
//        ReferentialReference<HookTypeDto> hookType = formDto.getReferentialReferenceById(HookTypeDto.class, BranchlineDto.PROPERTY_HOOK_TYPE, "fr.ird.observe.entities.referentiel.longline.HookType#1239832686152#0.2");
//        branchlineDto.setHookType(hookType);
//        ReferentialReference<HookSizeDto> hookSize = formDto.getReferentialReferenceById(HookSizeDto.class, BranchlineDto.PROPERTY_HOOK_SIZE, "fr.ird.observe.entities.referentiel.longline.HookSize#1239832686151#0.6");
        ReferentialReference<HookSizeDto> hookSize = null;
//        branchlineDto.setHookSize(hookSize);
        branchlineDto.setHookOffset(24);
//        ReferentialReference<BaitTypeDto> baitType = formDto.getReferentialReferenceById(BaitTypeDto.class, BranchlineDto.PROPERTY_BAIT_TYPE, "fr.ird.observe.entities.referentiel.longline.BaitType#1239832686124#0.8");
        ReferentialReference<BaitTypeDto> baitType = null;
//        branchlineDto.setBaitType(baitType);
//        ReferentialReference<BaitSettingStatusDto> baitSettingStatus = formDto.getReferentialReferenceById(BaitSettingStatusDto.class, BranchlineDto.PROPERTY_BAIT_SETTING_STATUS, "fr.ird.observe.entities.referentiel.longline.BaitSettingStatus#1239832686123#0.1");
        ReferentialReference<BaitSettingStatusDto> baitSettingStatus = null;
//        branchlineDto.setBaitSettingStatus(baitSettingStatus);
//        ReferentialReference<BaitHaulingStatusDto> baitHaulingStatus = formDto.getReferentialReferenceById(BaitHaulingStatusDto.class, BranchlineDto.PROPERTY_BAIT_HAULING_STATUS, "fr.ird.observe.entities.referentiel.longline.BaitHaulingStatus#1239832686122#0.4");
        ReferentialReference<BaitHaulingStatusDto> baitHaulingStatus = null;
//        branchlineDto.setBaitHaulingStatus(baitHaulingStatus);

        service.save(compositionDto);

        SetLongline setLongline = topiaTestMethodResource.findById(SetLongline.class, ObserveFixtures.SET_LONGLINE_ID_1);

        Section section = setLongline.getSectionByTopiaId(sectionDto.getId());
        Assert.assertEquals(sectionDto.getSettingIdentifier(), section.getSettingIdentifier());
        Assert.assertEquals(sectionDto.getHaulingIdentifier(), section.getHaulingIdentifier());

        Basket basket = section.getBasketByTopiaId(basketDto.getId());
        Assert.assertEquals(basketDto.getSettingIdentifier(), basket.getSettingIdentifier());
        Assert.assertEquals(basketDto.getHaulingIdentifier(), basket.getHaulingIdentifier());
        Assert.assertEquals(basketDto.getFloatline1Length(), basket.getFloatline1Length());
        Assert.assertEquals(basketDto.getFloatline2Length(), basket.getFloatline2Length());

        Branchline branchline = basket.getBranchlineByTopiaId(branchlineDto.getId());
        Assert.assertEquals(branchlineDto.getSettingIdentifier(), branchline.getSettingIdentifier());
        Assert.assertEquals(branchlineDto.getHaulingIdentifier(), branchline.getHaulingIdentifier());
        Assert.assertEquals(branchlineDto.getBranchlineLength(), branchline.getBranchlineLength());
        Assert.assertEquals(branchlineDto.getTracelineLength(), branchline.getTracelineLength());

        assertReferenceDtoEqualsEntity(topType, branchline.getTopType());
        assertReferenceDtoEqualsEntity(tracelineType, branchline.getTracelineType());
        Assert.assertEquals(branchlineDto.getDepthRecorder(), branchline.getDepthRecorder());
        Assert.assertEquals(branchlineDto.getHookLost(), branchline.getHookLost());
        Assert.assertEquals(branchlineDto.getTraceCutOff(), branchline.getTraceCutOff());
        Assert.assertEquals(branchlineDto.getTimer(), branchline.getTimer());
        Assert.assertEquals(branchlineDto.getTimeSinceContact(), branchline.getTimeSinceContact());
        Assert.assertEquals(branchlineDto.getTimerTimeOnBoard(), branchline.getTimerTimeOnBoard());
        Assert.assertEquals(branchlineDto.getWeightedSnap(), branchline.getWeightedSnap());
        Assert.assertEquals(branchlineDto.getSnapWeight(), branchline.getSnapWeight());
        Assert.assertEquals(branchlineDto.getWeightedSwivel(), branchline.getWeightedSwivel());
        Assert.assertEquals(branchlineDto.getSwivelWeight(), branchline.getSwivelWeight());

        assertReferenceDtoEqualsEntity(hookType, branchline.getHookType());
        assertReferenceDtoEqualsEntity(hookSize, branchline.getHookSize());
        Assert.assertEquals(branchlineDto.getSwivelWeight(), branchline.getSwivelWeight());
        assertReferenceDtoEqualsEntity(baitType, branchline.getBaitType());
        assertReferenceDtoEqualsEntity(baitSettingStatus, branchline.getBaitSettingStatus());
        assertReferenceDtoEqualsEntity(baitHaulingStatus, branchline.getBaitHaulingStatus());

    }

}
