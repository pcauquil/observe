package fr.ird.observe.services.topia.service.seine;

/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.seine.TripSeine;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.services.topia.ObserveServiceContextTopiaTaiste;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.ConcurrentModificationException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class TripSeineServiceTopiaTest extends AbstractServiceTopiaTest {

    protected TripSeineService service;

    protected ReferentialService referentialService;

    @Before
    public void setUp() throws Exception {

        service = topiaTestMethodResource.newService(TripSeineService.class);

        referentialService = topiaTestMethodResource.newService(ReferentialService.class);

    }

    @Test
    public void getTripSeineByProgramTest() {

        DataReferenceSet<TripSeineDto> stubDtos = service.getTripSeineByProgram(ObserveFixtures.PROGRAM_ID);

        Assert.assertNotNull(stubDtos);

        Assert.assertEquals(3, stubDtos.sizeReference());

        DataReference<TripSeineDto> tripSeineStubDto = stubDtos.getReferenceByPosition(0);
        TripSeine tripSeine = topiaTestMethodResource.findById(TripSeine.class, ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertEquals(tripSeine.getTopiaId(), tripSeineStubDto.getId());
        Assert.assertEquals(0, tripSeine.getStartDate().compareTo((Date) tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_START_DATE)));
        Assert.assertEquals(0, tripSeine.getEndDate().compareTo((Date) tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_END_DATE)));
        Assert.assertEquals(tripSeine.getVessel().getLabel2(), tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_VESSEL));
        Assert.assertEquals(tripSeine.getObserverLabel(), tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_OBSERVER));

        tripSeineStubDto = stubDtos.getReferenceByPosition(1);
        tripSeine = topiaTestMethodResource.findById(TripSeine.class, ObserveFixtures.TRIP_SEINE_ID_2);

        Assert.assertEquals(tripSeine.getTopiaId(), tripSeineStubDto.getId());
        Assert.assertEquals(0, tripSeine.getStartDate().compareTo((Date) tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_START_DATE)));
        Assert.assertEquals(0, tripSeine.getEndDate().compareTo((Date) tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_END_DATE)));
        Assert.assertEquals(tripSeine.getVessel().getLabel2(), tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_VESSEL));
        Assert.assertEquals(tripSeine.getObserverLabel(), tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_OBSERVER));

        tripSeineStubDto = stubDtos.getReferenceByPosition(2);
        tripSeine = topiaTestMethodResource.findById(TripSeine.class, ObserveFixtures.TRIP_SEINE_ID_3);

        Assert.assertEquals(tripSeine.getTopiaId(), tripSeineStubDto.getId());
        Assert.assertEquals(0, tripSeine.getStartDate().compareTo((Date) tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_START_DATE)));
        Assert.assertEquals(0, tripSeine.getEndDate().compareTo((Date) tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_END_DATE)));
        Assert.assertEquals(tripSeine.getVessel().getLabel2(), tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_VESSEL));
        Assert.assertEquals(tripSeine.getObserverLabel(), tripSeineStubDto.getPropertyValue(TripSeineDto.PROPERTY_OBSERVER));

    }

    @Test
    public void loadFormTest() throws Exception {

        TripSeine tripSeine = topiaTestMethodResource.findById(TripSeine.class, ObserveFixtures.TRIP_SEINE_ID_1);
        Form<TripSeineDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertNotNull(form);
        TripSeineDto tripSeineDto = form.getObject();

        assertEntityEqualsReferenceDto(tripSeine.getCaptain(), tripSeineDto.getCaptain());
        assertEntityEqualsReferenceDto(tripSeine.getObserver(), tripSeineDto.getObserver());
        assertEntityEqualsReferenceDto(tripSeine.getDataEntryOperator(), tripSeineDto.getDataEntryOperator());
        assertEntityEqualsReferenceDto(tripSeine.getVessel(), tripSeineDto.getVessel());
        assertEntityEqualsReferenceDto(tripSeine.getOcean(), tripSeineDto.getOcean());
        assertEntityEqualsReferenceDto(tripSeine.getDepartureHarbour(), tripSeineDto.getDepartureHarbour());
        assertEntityEqualsReferenceDto(tripSeine.getLandingHarbour(), tripSeineDto.getLandingHarbour());
        Assert.assertEquals(tripSeine.getErsId(), tripSeineDto.getErsId());
        Assert.assertEquals(tripSeine.getStartDate(), tripSeineDto.getStartDate());
        Assert.assertEquals(tripSeine.getEndDate(), tripSeineDto.getEndDate());
        Assert.assertEquals(tripSeine.getFormsUrl(), tripSeineDto.getFormsUrl());
        Assert.assertEquals(tripSeine.getReportsUrl(), tripSeineDto.getReportsUrl());
        Assert.assertEquals(tripSeine.getComment(), tripSeineDto.getComment());
        Assert.assertEquals(tripSeine.getLastUpdateDate(), tripSeineDto.getLastUpdateDate());

        loadReferenceSets(referentialService, form);

        //FIXME Topia test
//        assertTripFormLabels(formDto);
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertTrue(referenceSetDto.sizeReference() > 0);
//
//        }

    }

    @Test
    public void preCreateTest() {
        Form<TripSeineDto> form = service.preCreate(ObserveFixtures.PROGRAM_ID);

        Assert.assertNotNull(form);
        TripSeineDto tripSeineDto = form.getObject();

        Assert.assertNull(tripSeineDto.getCaptain());
        Assert.assertNull(tripSeineDto.getObserver());
        Assert.assertNull(tripSeineDto.getDataEntryOperator());
        Assert.assertNull(tripSeineDto.getVessel());
        Assert.assertNull(tripSeineDto.getOcean());
        Assert.assertNull(tripSeineDto.getDepartureHarbour());
        Assert.assertNull(tripSeineDto.getLandingHarbour());
        Assert.assertNull(tripSeineDto.getErsId());
        Assert.assertEquals(DateUtil.getDay(ObserveServiceContextTopiaTaiste.DATE), tripSeineDto.getStartDate());
        Assert.assertEquals(DateUtil.getDay(ObserveServiceContextTopiaTaiste.DATE), tripSeineDto.getEndDate());
        Assert.assertNull(tripSeineDto.getFormsUrl());
        Assert.assertNull(tripSeineDto.getReportsUrl());
        Assert.assertNull(tripSeineDto.getComment());
        Assert.assertNull(tripSeineDto.getLastUpdateDate());

        //FIXME Topia test
//        assertTripFormLabels(formDto);
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertTrue(referenceSetDto.sizeReference() > 0);
//
//        }

    }

    @Test
    @CopyDatabaseConfiguration
    public void saveCreateTest() throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        Form<TripSeineDto> form = service.preCreate(ObserveFixtures.PROGRAM_ID);

        TripSeineDto tripSeineDto = form.getObject();

        tripSeineDto.setCaptain(getReference(PersonDto.class, 0));
        tripSeineDto.setObserver(getReference(PersonDto.class, 0));
        tripSeineDto.setDataEntryOperator(getReference(PersonDto.class, 0));

        tripSeineDto.setVessel(getReference(VesselDto.class, 0));

        tripSeineDto.setOcean(getReference(OceanDto.class, 0));

        tripSeineDto.setDepartureHarbour(getReference(HarbourDto.class, 0));
        tripSeineDto.setLandingHarbour(getReference(HarbourDto.class, 0));

        tripSeineDto.setErsId("ersid");

        tripSeineDto.setStartDate(DateUtil.createDate(24, 8, 2015));
        tripSeineDto.setEndDate(DateUtil.createDate(24, 9, 2015));

        tripSeineDto.setFormsUrl("http://une.url.com/formulaire");
        tripSeineDto.setReportsUrl("http://une.url.com/rapport");

        tripSeineDto.setComment("Un commentaire");

        Date lastUpdateBefore = getLastUpdateDate(TripSeine.class);

        SaveResultDto saveResult = service.save(form.getObject());

        TripSeine tripSeine = topiaTestMethodResource.findById(TripSeine.class, saveResult.getId());

        assertReferenceDtoEqualsEntity(tripSeineDto.getCaptain(), tripSeine.getCaptain());
        assertReferenceDtoEqualsEntity(tripSeineDto.getObserver(), tripSeine.getObserver());
        assertReferenceDtoEqualsEntity(tripSeineDto.getDataEntryOperator(), tripSeine.getDataEntryOperator());
        assertReferenceDtoEqualsEntity(tripSeineDto.getVessel(), tripSeine.getVessel());
        assertReferenceDtoEqualsEntity(tripSeineDto.getOcean(), tripSeine.getOcean());
        assertReferenceDtoEqualsEntity(tripSeineDto.getDepartureHarbour(), tripSeine.getDepartureHarbour());
        assertReferenceDtoEqualsEntity(tripSeineDto.getLandingHarbour(), tripSeine.getLandingHarbour());
        Assert.assertEquals(tripSeineDto.getErsId(), tripSeine.getErsId());
        Assert.assertEquals(tripSeineDto.getStartDate(), tripSeine.getStartDate());
        Assert.assertEquals(tripSeineDto.getEndDate(), tripSeine.getEndDate());
        Assert.assertEquals(tripSeineDto.getFormsUrl(), tripSeine.getFormsUrl());
        Assert.assertEquals(tripSeineDto.getReportsUrl(), tripSeine.getReportsUrl());
        Assert.assertEquals(tripSeineDto.getComment(), tripSeine.getComment());
        Assert.assertNotNull(tripSeine.getLastUpdateDate());

        Date lastUpdateAfter = getLastUpdateDate(TripSeine.class);
        Assert.assertNotEquals(lastUpdateBefore, lastUpdateAfter);
    }

    @Test
    @CopyDatabaseConfiguration
    public void saveUpdateTest() throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        Form<TripSeineDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        loadReferenceSets(referentialService, form);

        TripSeineDto tripSeineDto = form.getObject();

        tripSeineDto.setCaptain(getReference(PersonDto.class, 0));
        tripSeineDto.setDataEntryOperator(getReference(PersonDto.class, 0));

        tripSeineDto.setOcean(getReference(OceanDto.class, 0));

        tripSeineDto.setDepartureHarbour(getReference(HarbourDto.class, 0));
        tripSeineDto.setLandingHarbour(getReference(HarbourDto.class, 0));

        tripSeineDto.setErsId("ersid");

        tripSeineDto.setStartDate(DateUtil.createDate(24, 8, 2015));
        tripSeineDto.setEndDate(DateUtil.createDate(24, 9, 2015));

        tripSeineDto.setFormsUrl("http://une.url.com/formulaire");
        tripSeineDto.setReportsUrl("http://une.url.com/rapport");

        tripSeineDto.setComment("Un commentaire");

        Date lastUpdateBefore = getLastUpdateDate(TripSeine.class);

        service.save(form.getObject());

        TripSeine tripSeine = topiaTestMethodResource.findById(TripSeine.class, ObserveFixtures.TRIP_SEINE_ID_1);

        assertReferenceDtoEqualsEntity(tripSeineDto.getCaptain(), tripSeine.getCaptain());
        assertReferenceDtoEqualsEntity(tripSeineDto.getObserver(), tripSeine.getObserver());
        assertReferenceDtoEqualsEntity(tripSeineDto.getDataEntryOperator(), tripSeine.getDataEntryOperator());
        assertReferenceDtoEqualsEntity(tripSeineDto.getVessel(), tripSeine.getVessel());
        assertReferenceDtoEqualsEntity(tripSeineDto.getOcean(), tripSeine.getOcean());
        assertReferenceDtoEqualsEntity(tripSeineDto.getDepartureHarbour(), tripSeine.getDepartureHarbour());
        assertReferenceDtoEqualsEntity(tripSeineDto.getLandingHarbour(), tripSeine.getLandingHarbour());
        Assert.assertEquals(tripSeineDto.getErsId(), tripSeine.getErsId());
        Assert.assertEquals(tripSeineDto.getStartDate(), tripSeine.getStartDate());
        Assert.assertEquals(tripSeineDto.getEndDate(), tripSeine.getEndDate());
        Assert.assertEquals(tripSeineDto.getFormsUrl(), tripSeine.getFormsUrl());
        Assert.assertEquals(tripSeineDto.getReportsUrl(), tripSeine.getReportsUrl());
        Assert.assertEquals(tripSeineDto.getComment(), tripSeine.getComment());
        Assert.assertNotNull(tripSeine.getLastUpdateDate());
        Assert.assertNotEquals(tripSeineDto.getLastUpdateDate(), tripSeine.getLastUpdateDate());

        Date lastUpdateAfter = getLastUpdateDate(TripSeine.class);
        Assert.assertNotEquals(lastUpdateBefore, lastUpdateAfter);

    }

    @Test(expected = ConcurrentModificationException.class)
    @CopyDatabaseConfiguration
    public void saveConcurrentTest() throws BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        Form<TripSeineDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        TripSeineDto tripSeineDto = form.getObject();

        loadReferenceSets(referentialService, form);

        tripSeineDto.setCaptain(getReference(PersonDto.class, 0));
        tripSeineDto.setDataEntryOperator(getReference(PersonDto.class, 0));

        tripSeineDto.setOcean(getReference(OceanDto.class, 0));

        tripSeineDto.setDepartureHarbour(getReference(HarbourDto.class, 0));
        tripSeineDto.setLandingHarbour(getReference(HarbourDto.class, 0));

        // on fait croire que notre version est plus ancienne
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tripSeineDto.getLastUpdateDate());
        calendar.add(Calendar.HOUR, -1);
        tripSeineDto.setLastUpdateDate(calendar.getTime());

        service.save(form.getObject());

    }

    @Test
    @CopyDatabaseConfiguration
    public void deleteTest() {

        Date lastUpdateBefore = getLastUpdateDate(TripSeine.class);

        service.delete(ObserveFixtures.TRIP_SEINE_ID_1);

        DataReferenceSet<TripSeineDto> tripSeineByProgram = service.getTripSeineByProgram(ObserveFixtures.PROGRAM_ID);

        Assert.assertEquals(2, tripSeineByProgram.sizeReference());

        Assert.assertFalse(topiaTestMethodResource.exists(TripSeine.class, ObserveFixtures.TRIP_SEINE_ID_1));

        Date lastUpdateAfter = getLastUpdateDate(TripSeine.class);
        Assert.assertNotEquals(lastUpdateBefore, lastUpdateAfter);

    }

}
