package fr.ird.observe.application.web.configuration;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import fr.ird.observe.util.ObserveUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigHelper;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.version.Version;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Properties;
import java.util.Set;

/**
 * La configuration de l'application web.
 *
 * Created on 29/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebApplicationConfiguration {

    /** Logger. */
    private static Log log = LogFactory.getLog(ObserveWebApplicationConfiguration.class);

    protected static final String DEFAULT_OBSERVE_WEB_CONFIGURATION_FILENAME = "observeweb.conf";

    private final ApplicationConfig applicationConfig;

    public ObserveWebApplicationConfiguration() {
        this(DEFAULT_OBSERVE_WEB_CONFIGURATION_FILENAME);
    }

    public ObserveWebApplicationConfiguration(String confFileName) {
        applicationConfig = new ApplicationConfig();
        applicationConfig.setEncoding(Charsets.UTF_8.name());
        applicationConfig.setConfigFileName(confFileName);
        ApplicationConfigProvider applicationConfigProvider = ApplicationConfigHelper.getProvider(getClass().getClassLoader(), ObserveWebApplicationConfigurationProvider.OBSERVE_WEB_CONFIGURATION_PROVIDER_NAME);
        applicationConfig.loadDefaultOptions(applicationConfigProvider.getOptions());

    }

    public boolean isDevMode() {
        return applicationConfig.getOptionAsBoolean(ObserveWebApplicationConfigurationOption.DEV_MODE.getKey());
    }

    public Version getVersion() {
        return applicationConfig.getOption(Version.class, ObserveWebApplicationConfigurationOption.BUILD_VERSION.getKey());
    }

    public Version getModelVersion() {
        return applicationConfig.getOption(Version.class, ObserveWebApplicationConfigurationOption.MODEL_VERSION.getKey());
    }

    public File getBaseDirectory() {
        return applicationConfig.getOptionAsFile(ObserveWebApplicationConfigurationOption.BASE_DIRECTORY.getKey());
    }

    public File getTemporaryDirectory() {
        return applicationConfig.getOptionAsFile(ObserveWebApplicationConfigurationOption.TEMPORARY_DIRECTORY.getKey());
    }

    public File getDatabasesConfigurationFile() {
        return applicationConfig.getOptionAsFile(ObserveWebApplicationConfigurationOption.DATABASES_CONFIGURATION_FILE.getKey());
    }

    public File getUsersConfigurationFile() {
        return applicationConfig.getOptionAsFile(ObserveWebApplicationConfigurationOption.USERS_CONFIGURATION_FILE.getKey());
    }

    public File getLog4jConfigurationFile() {
        return applicationConfig.getOptionAsFile(ObserveWebApplicationConfigurationOption.LOG4J_CONFIGURATION_FILE.getKey());
    }

    public int getSessionMaximumSize() {
        return applicationConfig.getOptionAsInt(ObserveWebApplicationConfigurationOption.SESSION_MAXIMUM_SIZE.getKey());
    }

    public int getSessionExpirationDelay() {
        return applicationConfig.getOptionAsInt(ObserveWebApplicationConfigurationOption.SESSION_EXPIRATION_DELAY.getKey());
    }

    public String getAdminApiKey() {
        return applicationConfig.getOption(ObserveWebApplicationConfigurationOption.ADMIN_API_KEY.getKey());
    }

    public URL getApiUrl() {
        return applicationConfig.getOptionAsURL(ObserveWebApplicationConfigurationOption.API_URL.getKey());
    }


    public void init(String... args) {

        if (log.isInfoEnabled()) {
            log.info("Starts to init ObserVeWeb configuration...");
        }

        try {
            applicationConfig.parse(args);
        } catch (ArgumentsParserException e) {
            throw new ObserveWebApplicationConfigurationInitException("could not parse configuration", e);
        }

        File applicationBaseDirectory = getBaseDirectory();

        if (isDevMode() && !applicationBaseDirectory.exists()) {
            // on utilise un répertoire temporaire comme basedir

            if (log.isInfoEnabled()) {
                log.info("Using a dev mode configuration.");
            }
            try {
                // Toujours s'assurer que le répertoire temporarie du système existe
                Path tmpdir = Paths.get(System.getProperty("java.io.tmpdir"));
                if (!Files.exists(tmpdir)) {
                    Files.createDirectories(tmpdir);
                }

                Set<PosixFilePermission> posixFilePermissions = PosixFilePermissions.fromString("rwxr-x---");
                FileAttribute<Set<PosixFilePermission>> fileAttribute = PosixFilePermissions.asFileAttribute(posixFilePermissions);
                applicationBaseDirectory = Files.createTempDirectory("observeweb", fileAttribute).toFile();
            } catch (IOException e) {
                throw new ObserveWebApplicationConfigurationInitException("could not create temporary basedir", e);
            }
            if (log.isInfoEnabled()) {
                log.info("Dev mode detected, use temporary basedir: " + applicationBaseDirectory);
            }
            applicationConfig.setOption(ObserveWebApplicationConfigurationOption.BASE_DIRECTORY.getKey(), applicationBaseDirectory.getAbsolutePath());
        }

        if (log.isInfoEnabled()) {
            String message = getConfigurationDescription();
            log.info(message);
        }

        try {
            createDirectory(applicationBaseDirectory);
        } catch (IOException e) {
            throw new ObserveWebApplicationConfigurationInitException("Impossible de créer le répertoire principal de l'application (" + applicationBaseDirectory + ")", e);
        }
        File temporaryDirectory = getTemporaryDirectory();
        try {
            createDirectory(temporaryDirectory);
        } catch (IOException e) {
            throw new ObserveWebApplicationConfigurationInitException("Impossible de créer le répertoire temporaire de l'application (" + temporaryDirectory + ")", e);
        }

        File databasesConfigurationFile = getDatabasesConfigurationFile();
        File usersConfigurationFile = getUsersConfigurationFile();
        File log4jConfigurationFile = getLog4jConfigurationFile();

        if (isDevMode()) {

            if (!databasesConfigurationFile.exists()) {

                // Generate a default databasesConfigurationFile
                if (log.isInfoEnabled()) {
                    log.info("Generate a default databasesConfigurationFile for tests purpose only:\n" + DEV_DATABASES_CONFIGURATION_FILE_CONTENT);
                }

                try {
                    Files.write(databasesConfigurationFile.toPath(), DEV_DATABASES_CONFIGURATION_FILE_CONTENT.getBytes());
                } catch (IOException e) {
                    throw new ObserveWebApplicationConfigurationInitException("Impossible de créer un fichier de configuration des bases pour développement", e);
                }
            }

            if (!usersConfigurationFile.exists()) {

                // Generate a default usersConfigurationFile
                if (log.isInfoEnabled()) {
                    log.info("Generate a default usersConfigurationFile for tests purpose only:\n" + DEV_USERS_CONFIGURATION_FILE_CONTENT);
                }
                try {
                    Files.write(usersConfigurationFile.toPath(), DEV_USERS_CONFIGURATION_FILE_CONTENT.getBytes());
                } catch (IOException e) {
                    throw new ObserveWebApplicationConfigurationInitException("Impossible de créer un fichier de configuration des utilisateurs pour développement", e);
                }
            }

            if (!log4jConfigurationFile.exists()) {

                // Generate a default log4jConfigurationFile
                try {
                    CharSource charSource = Resources.asCharSource(getClass().getResource("/observeweb-log4j.conf"), Charsets.UTF_8);
                    String log4jFileContent = charSource.read();
                    if (log.isInfoEnabled()) {
                        log.info("Generate a default log4jConfigurationFile for tests purpose only:\n" + log4jFileContent);
                    }
                    Files.write(log4jConfigurationFile.toPath(), log4jFileContent.getBytes());
                } catch (IOException e) {
                    throw new ObserveWebApplicationConfigurationInitException("Impossible de créer un fichier de log4j pour développement", e);
                }
            }
        }

        if (!databasesConfigurationFile.exists()) {
            throw new ObserveWebApplicationConfigurationInitException("Le fichier de configuration des bases n'existe pas : " + databasesConfigurationFile);
        }

        if (!usersConfigurationFile.exists()) {
            throw new ObserveWebApplicationConfigurationInitException("Le fichier de configuration des utilisateurs n'existe pas : " + usersConfigurationFile);
        }

        initLog();

    }

    public String getConfigurationDescription() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n=====================================================================================================================");
        builder.append("\n=== Observe Web configuration =======================================================================================");
        builder.append(String.format("\n=== %1$-40s = %2$s", "Filename", applicationConfig.getConfigFileName()));
        for (ObserveWebApplicationConfigurationOption option : ObserveWebApplicationConfigurationOption.orderedByNameValues()) {
            builder.append(String.format("\n=== %1$-40s = %2$s", option.getKey(), applicationConfig.getOption(option)));
        }
        builder.append("\n=====================================================================================================================");
        return builder.toString();
    }

    protected void createDirectory(File directory) throws IOException {
        Set<PosixFilePermission> posixFilePermissions = PosixFilePermissions.fromString("rwxrwx---");
        FileAttribute<Set<PosixFilePermission>> fileAttribute = PosixFilePermissions.asFileAttribute(posixFilePermissions);
        Files.createDirectories(directory.toPath(), fileAttribute);
    }

    protected void initLog() {

        File logFile = getLog4jConfigurationFile();

        if (!logFile.exists()) {
            throw new ObserveWebApplicationConfigurationInitException("Le fichier de configuration des logs (" + logFile + ") n'existe pas");
        }

        if (log.isInfoEnabled()) {
            log.info("Chargement du fichier de log : " + logFile);
        }

        Properties finalLogConfigurationProperties;
        try (BufferedReader inputStream = Files.newBufferedReader(logFile.toPath(), Charsets.UTF_8)) {

            Properties logConfigurationProperties = new Properties();
            logConfigurationProperties.load(inputStream);

            finalLogConfigurationProperties = ObserveUtil.loadProperties(logConfigurationProperties, applicationConfig);

        } catch (Exception e) {
            throw new ObserveWebApplicationConfigurationInitException("Impossible de charger le fichier de configuration des logs", e);
        }

        LogManager.resetConfiguration();
        PropertyConfigurator.configure(finalLogConfigurationProperties);

        log = LogFactory.getLog(ObserveWebApplicationConfiguration.class);
        if (log.isInfoEnabled()) {
            log.info("Configuration des logs chargée depuis le fichier " + logFile);
        }

    }

    private static final String DEV_DATABASES_CONFIGURATION_FILE_CONTENT =
            "databases: \n" +
            "- name: production\n" +
            "  defaultDatabase: true\n" +
            "  roles: \n" +
            "  - login: admin\n" +
            "    password: a\n" +
            "  - login: referentiel\n" +
            "    password: a\n" +
            "  url: jdbc:postgresql://localhost:5432/obstuna";

    private static final String DEV_USERS_CONFIGURATION_FILE_CONTENT =
            "users: \n" +
            "- login: admin\n" +
            "  password: a\n" +
            "  permissions: \n" +
            "  - database: production\n" +
            "    role: admin\n" +
            "- login: referentiel\n" +
            "  password: a\n" +
            "  permissions: \n" +
            "  - database: production\n" +
            "    role: referentiel";
}
