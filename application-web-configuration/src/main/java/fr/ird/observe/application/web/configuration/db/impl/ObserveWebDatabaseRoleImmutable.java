package fr.ird.observe.application.web.configuration.db.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.configuration.db.ObserveWebDatabaseRole;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebDatabaseRoleImmutable implements ObserveWebDatabaseRole {

    private final String login;

    private final String password;

    public ObserveWebDatabaseRoleImmutable(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public ObserveWebDatabaseRoleBean toBean() {
        ObserveWebDatabaseRoleBean observeWebDatabasesBean = new ObserveWebDatabaseRoleBean();
        observeWebDatabasesBean.setLogin(login);
        observeWebDatabasesBean.setPassword(password);
        return observeWebDatabasesBean;
    }
}
