package fr.ird.observe.application.web.configuration.db.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabase;

import java.util.LinkedHashSet;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebDatabaseBean implements ObserveWebDatabase<ObserveWebDatabaseRoleBean> {

    protected String name;

    protected String url;

    protected boolean defaultDatabase;

    protected LinkedHashSet<ObserveWebDatabaseRoleBean> roles;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isDefaultDatabase() {
        return defaultDatabase;
    }

    @Override
    public String getUrl() {
        return url;
    }

    public Optional<ObserveWebDatabaseRoleBean> getDatabaseRoleByLogin(String login) {
        throw new UnsupportedOperationException("Can not call this method on bean version, use immutable one.");
    }

    @Override
    public LinkedHashSet<ObserveWebDatabaseRoleBean> getRoles() {
        return roles;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDefaultDatabase(boolean defaultDatabase) {
        this.defaultDatabase = defaultDatabase;
    }

    public void setRoles(LinkedHashSet<ObserveWebDatabaseRoleBean> roles) {
        this.roles = roles;
    }

    public ObserveWebDatabaseImmutable toImmutable() {
        return new ObserveWebDatabaseImmutable(name, defaultDatabase, url, Iterables.transform(roles, ObserveWebDatabaseRoleBean::toImmutable));
    }
}
