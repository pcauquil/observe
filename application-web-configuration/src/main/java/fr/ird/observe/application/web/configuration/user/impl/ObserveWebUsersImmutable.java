package fr.ird.observe.application.web.configuration.user.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.observe.application.web.configuration.user.ObserveWebUsers;

import java.util.Collection;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebUsersImmutable implements ObserveWebUsers<ObserveWebUserImmutable> {

    private final ImmutableMap<String, ObserveWebUserImmutable> users;

    public ObserveWebUsersImmutable(Iterable<ObserveWebUserImmutable> users) {
        this.users = Maps.uniqueIndex(users, ObserveWebUserImmutable::getLogin);
    }

    @Override
    public Collection<ObserveWebUserImmutable> getUsers() {
        return users.values();
    }

    @Override
    public Optional<ObserveWebUserImmutable> getUserByLogin(String login) {
        return Optional.ofNullable(users.get(login));
    }

    public ObserveWebUsersBean toBean() {
        ObserveWebUsersBean observeWebUsersBean = new ObserveWebUsersBean();
        observeWebUsersBean.setUsers(Sets.newLinkedHashSet(Iterables.transform(getUsers(), ObserveWebUserImmutable::toBean)));
        return observeWebUsersBean;
    }
}
