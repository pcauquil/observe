package fr.ird.observe.application.web.configuration.db.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabase;

import java.util.Collection;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebDatabaseImmutable implements ObserveWebDatabase<ObserveWebDatabaseRoleImmutable> {

    private final String name;

    private final boolean defaultDatabase;

    private final String url;

    private final ImmutableMap<String, ObserveWebDatabaseRoleImmutable> roles;

    public ObserveWebDatabaseImmutable(String name,
                                       boolean defaultDatabase,
                                       String url,
                                       Iterable<ObserveWebDatabaseRoleImmutable> roles) {
        this.name = name;
        this.defaultDatabase = defaultDatabase;
        this.url = url;
        this.roles = Maps.uniqueIndex(roles, input -> input.getLogin());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isDefaultDatabase() {
        return defaultDatabase;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public Collection<ObserveWebDatabaseRoleImmutable> getRoles() {
        return roles.values();
    }

    @Override
    public Optional<ObserveWebDatabaseRoleImmutable> getDatabaseRoleByLogin(String login) {
        ObserveWebDatabaseRoleImmutable observeWebDatabaseRole = roles.get(login);
        return Optional.ofNullable(observeWebDatabaseRole);
    }

    public ObserveWebDatabaseBean toBean() {
        ObserveWebDatabaseBean observeWebDatabaseBean = new ObserveWebDatabaseBean();
        observeWebDatabaseBean.setDefaultDatabase(defaultDatabase);
        observeWebDatabaseBean.setName(name);
        observeWebDatabaseBean.setUrl(url);
        observeWebDatabaseBean.setRoles(Sets.newLinkedHashSet(Iterables.transform(getRoles(), ObserveWebDatabaseRoleImmutable::toBean)));

        return observeWebDatabaseBean;
    }

}
