package fr.ird.observe.application.web.configuration;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.NotImplementedException;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * La configuration de l'application web.
 *
 * Created on 29/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public enum ObserveWebApplicationConfigurationOption implements ConfigOptionDef {

    BUILD_VERSION("observeweb.build.version", n("observeweb.build.version.description"), "", Version.class),
    BUILD_DATE("observeweb.build.date", n("observeweb.build.date.description"), "", String.class),
    BUILD_NUMBER("observeweb.build.number", n("observeweb.build.number.description"), "", String.class),
    ADMIN_API_KEY("observeweb.adminApiKey", n("observeweb.adminApiKey.description"), "changeme", String.class),
    API_URL("observeweb.apiUrl", n("observeweb.apiUrl.description"), "http://localhost:8080/observeweb/api/v1", URL.class),

    MODEL_VERSION("observeweb.model.version", n("observe.model.version"), null, Version.class),

    DEV_MODE("observeweb.devMode", n("observeweb.devMode.description"), "true", boolean.class),
    BASE_DIRECTORY("observeweb.baseDirectory", n("observeweb.baseDirectory.description"), "/var/local/observeweb", File.class),
    TEMPORARY_DIRECTORY("observeweb.temporaryDirectory", n("observeweb.temporaryDirectory.description"), "${observeweb.baseDirectory}/temp", File.class),
    LOG4J_CONFIGURATION_FILE("observeweb.log4jConfigurationFile", n("observeweb.log4jConfigurationFile.description"), "${observeweb.baseDirectory}/observeweb-log4j.conf", String.class),
    DATABASES_CONFIGURATION_FILE("observeweb.databasesConfigurationFile", n("observeweb.databasesConfigurationFile.description"), "${observeweb.baseDirectory}/databases.yml", File.class),
    USERS_CONFIGURATION_FILE("observeweb.usersConfigurationFile", n("observeweb.usersConfigurationFile.description"), "${observeweb.baseDirectory}/users.yml", File.class),
    SESSION_EXPIRATION_DELAY("observeweb.sessionExpirationDelay", n("observeweb.sessionExpirationDelay.description"), "60" /* en minutes */, int.class),
    SESSION_MAXIMUM_SIZE("observeweb.sessionMaximumSize", n("observeweb.sessionMaximumSize.description"), "10000", int.class);

    ObserveWebApplicationConfigurationOption(String key, String description, String defaultValue, Class<?> type) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
    }

    private final String key;

    private final String description;

    private final String defaultValue;

    private final Class<?> type;

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return false;
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        throw new NotImplementedException("Can't invoke setDefaultValue method");
    }

    @Override
    public void setTransient(boolean isTransient) {
        throw new NotImplementedException("Can't invoke setTransient method");
    }

    @Override
    public void setFinal(boolean isFinal) {
        throw new NotImplementedException("Can't invoke setFinal method");
    }

    public static ImmutableList<ObserveWebApplicationConfigurationOption> orderedByNameValues() {

        List<ObserveWebApplicationConfigurationOption> values = Lists.newArrayList(values());
        Collections.sort(values, (o1, o2) -> o1.getKey().compareTo(o2.getKey()));
        return ImmutableList.copyOf(values);

    }
}
