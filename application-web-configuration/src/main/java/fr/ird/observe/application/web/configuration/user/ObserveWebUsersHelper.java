package fr.ird.observe.application.web.configuration.user;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabase;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabaseRole;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabases;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUserBean;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUserPermissionBean;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUsersBean;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUsersImmutable;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebUsersHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveWebUsersHelper.class);

    public ObserveWebUsersImmutable load(ObserveWebDatabases databases, File file) throws InvalidObserveWebUsersException, InvalidObserveWebUserException, InvalidObserveWebUserPermissionException {

        ObserveWebUsersBean observeWebUsersBean = loadBean(file);
        validateObserveWebUsersBean(databases, observeWebUsersBean);
        return observeWebUsersBean.toImmutable();

    }

    public ObserveWebUsersBean loadBean(File file) {

        if (log.isInfoEnabled()) {
            log.info("Loading users from file: " + file);
        }
        ObserveWebUsersBean result;
        try (Reader fileReader = Files.newReader(file, Charsets.UTF_8)) {
            YamlReader reader = new YamlReader(fileReader, createConfig());
            result = reader.read(ObserveWebUsersBean.class);
            fileReader.close();

        } catch (Exception e) {
            throw new RuntimeException("Could not read users from file: " + file, e);
        }

        return result;
    }

    public void validateObserveWebUsersBean(ObserveWebDatabases databases, ObserveWebUsersBean observeWebUsersBean) throws InvalidObserveWebUserException, InvalidObserveWebUserPermissionException {

        if (CollectionUtils.isEmpty(observeWebUsersBean.getUsers())) {
            throw new InvalidObserveWebUserException("No user defined");
        }

        for (ObserveWebUserBean observeWebUserBean : observeWebUsersBean.getUsers()) {
            validateObserveWebUserBean(databases, observeWebUserBean);
        }

    }

    public void validateObserveWebUserBean(ObserveWebDatabases databases, ObserveWebUserBean observeWebUserBean) throws InvalidObserveWebUserPermissionException, InvalidObserveWebUserException {

        String login = observeWebUserBean.getLogin();
        if (StringUtils.isBlank(login)) {
            throw new InvalidObserveWebUserException("Found a user with no login defined");
        }

        if (StringUtils.isBlank(observeWebUserBean.getPassword())) {
            throw new InvalidObserveWebUserException("User " + login + " has no password defined");
        }

        if (CollectionUtils.isEmpty(observeWebUserBean.getPermissions())) {
            throw new InvalidObserveWebUserException("User " + login + " has no roles defined");
        }

        // unique database name
        Set<String> databaseNames = new LinkedHashSet<>();

        for (ObserveWebUserPermissionBean observeWebUserPermissionBean : observeWebUserBean.getPermissions()) {

            validateObserveWebUserPermissionBean(databases, observeWebUserBean, observeWebUserPermissionBean);

            boolean added = databaseNames.add(observeWebUserPermissionBean.getDatabase());
            if (!added) {
                throw new InvalidObserveWebUserException("User " + login + " contains with a doublon database permission: " + observeWebUserPermissionBean.getDatabase());
            }

        }

    }

    public void validateObserveWebUserPermissionBean(ObserveWebDatabases<?> databases, ObserveWebUserBean observeWebUserBean, ObserveWebUserPermissionBean observeWebUserRoleBean) throws InvalidObserveWebUserPermissionException {

        String login = observeWebUserBean.getLogin();

        String databaseName = observeWebUserRoleBean.getDatabase();
        if (StringUtils.isBlank(databaseName)) {
            throw new InvalidObserveWebUserPermissionException("User " + login + ", found a permission with no database name defined");
        }
        String role = observeWebUserRoleBean.getRole();
        if (StringUtils.isBlank(role)) {
            throw new InvalidObserveWebUserPermissionException("User " + login + ", found a permission " + databaseName + " with no role defined");
        }

        Optional<? extends ObserveWebDatabase> databaseByName = databases.getDatabaseByName(databaseName);
        if (!databaseByName.isPresent()) {
            throw new InvalidObserveWebUserPermissionException("User " + login + ", found a permission " + databaseName + ", but database does not exists");
        }

        ObserveWebDatabase<?> database = databaseByName.get();
        Optional<? extends ObserveWebDatabaseRole> observeWebDatabaseRole = database.getDatabaseRoleByLogin(role);
        if (!observeWebDatabaseRole.isPresent()) {
            throw new InvalidObserveWebUserPermissionException("User " + login + ", found a permission " + databaseName + " with role " + role + ", but this role is not defined on this database");
        }
    }

    public void store(ObserveWebUsers users, File file) {

        if (log.isInfoEnabled()) {
            log.info("Store users to " + file);
        }

        try (BufferedWriter writer = Files.newWriter(file, Charsets.UTF_8)) {

            store(users, writer);

        } catch (Exception e) {
            throw new RuntimeException("Could not write users to file: " + file, e);
        }

    }

    public void store(ObserveWebUsers users, Writer writer) throws YamlException {


        YamlWriter yamlWriter = new YamlWriter(writer, createConfig());
        if (users instanceof ObserveWebUsersImmutable) {

            ObserveWebUsersImmutable observeWebUsersImmutable = (ObserveWebUsersImmutable) users;
            users = observeWebUsersImmutable.toBean();

        }
        yamlWriter.write(users);
        yamlWriter.close();


    }

    protected YamlConfig createConfig() {

        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.writeConfig.setIndentSize(2);
        yamlConfig.writeConfig.setWriteRootTags(false);
        yamlConfig.setPropertyElementType(ObserveWebUsersBean.class, "users", ObserveWebUserBean.class);
        yamlConfig.setPropertyElementType(ObserveWebUserBean.class, "permissions", ObserveWebUserPermissionBean.class);
        return yamlConfig;

    }
}
