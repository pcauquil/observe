package fr.ird.observe.application.web.configuration.user.impl;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.application.web.configuration.user.ObserveWebUsers;

import java.util.LinkedHashSet;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebUsersBean implements ObserveWebUsers<ObserveWebUserBean> {

    protected LinkedHashSet<ObserveWebUserBean> users;

    @Override
    public LinkedHashSet<ObserveWebUserBean> getUsers() {
        return users;
    }

    @Override
    public Optional<ObserveWebUserBean> getUserByLogin(String databaseName) {
        throw new UnsupportedOperationException("Can not call this method on bean version, use immutable one.");
    }

    public void setUsers(LinkedHashSet<ObserveWebUserBean> users) {
        this.users = users;
    }

    public ObserveWebUsersImmutable toImmutable() {
        return new ObserveWebUsersImmutable(Iterables.transform(users, ObserveWebUserBean::toImmutable));
    }

}
