package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import jaxx.runtime.validator.swing.SwingValidator;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created on 3/18/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class LonglineDetailCompositionValidatorServiceTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LonglineDetailCompositionValidatorServiceTest.class);

    protected LonglineDetailCompositionValidatorService service;

    @Before
    public void setUp() {

        if (ObserveSwingApplicationContext.isInit()) {

            ObserveSwingApplicationContext.get().close();
        }

        ObserveSwingApplicationContext applicationContext = new ObserveSwingApplicationContext(new ObserveSwingApplicationConfig());

        SwingValidator<SectionDto> sectionValidator = new SwingValidator<>(SectionDto.class, "ui-update-table", NuitonValidatorScope.values());
        SwingValidator<BasketDto> basketValidator = new SwingValidator<>(BasketDto.class, "ui-update-table", NuitonValidatorScope.values());
        SwingValidator<BranchlineDto> branchlineValidator = new SwingValidator<>(BranchlineDto.class, "ui-update-table", NuitonValidatorScope.values());

        service = new LonglineDetailCompositionValidatorService(
                sectionValidator,
                basketValidator,
                branchlineValidator,
                Collections.emptyMap(),
                applicationContext.getDecoratorService());

        I18n.init(new ClassPathI18nInitializer(), Locale.FRANCE);

    }

    @Test
    public void testValidateSections() {

        List<SectionDto> sections = new ArrayList<>();
        {
            SectionDto section = new SectionDto();
            section.setId("0");
            sections.add(section);
        }
        {
            SectionDto section = new SectionDto();
            section.setId("1");
            sections.add(section);
        }

        {
            // 2 sections, without settingIdentifier
            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 2);
        }

        {
            // 2 sections, with settingIdentifier
            sections.get(0).setSettingIdentifier(1);
            sections.get(1).setSettingIdentifier(2);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);

        }

        { // 2 sections with baskets without settingIdentifier
            {
                BasketDto basket = new BasketDto();
                basket.setId("00");
                sections.get(0).addBasket(basket);
            }
            {
                BasketDto basket = new BasketDto();
                basket.setId("01");
                sections.get(0).addBasket(basket);
            }
            {
                BasketDto basket = new BasketDto();
                basket.setId("10");
                sections.get(1).addBasket(basket);
            }
            {
                BasketDto basket = new BasketDto();
                basket.setId("11");
                sections.get(1).addBasket(basket);
            }
            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 4);
        }

        { // 2 sections with baskets with settingIdentifier

            Iterables.get(sections.get(0).getBasket(), 0).setSettingIdentifier(1);
            Iterables.get(sections.get(0).getBasket(), 1).setSettingIdentifier(2);
            Iterables.get(sections.get(1).getBasket(), 0).setSettingIdentifier(1);
            Iterables.get(sections.get(1).getBasket(), 1).setSettingIdentifier(2);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);

        }

        { // Section 1 - Basket 1 mistmatch floatline1Length with Section 1 - Basket 2

            Iterables.get(sections.get(0).getBasket(), 0).setFloatline1Length(1f);
            Iterables.get(sections.get(0).getBasket(), 0).setFloatline2Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline1Length(1f); // should be 2
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline2Length(6f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline1Length(6f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline2Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline1Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline2Length(12f);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 1);

        }

        { // OK d'ont check if last floatline has same length of first floatline for next section

            Iterables.get(sections.get(0).getBasket(), 0).setFloatline1Length(1f);
            Iterables.get(sections.get(0).getBasket(), 0).setFloatline2Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline1Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline2Length(6f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline1Length(10f); // Should be 6
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline2Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline1Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline2Length(12f);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);

        }

        { // Ok

            Iterables.get(sections.get(0).getBasket(), 0).setFloatline1Length(1f);
            Iterables.get(sections.get(0).getBasket(), 0).setFloatline2Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline1Length(2f);
            Iterables.get(sections.get(0).getBasket(), 1).setFloatline2Length(10f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline1Length(10f);
            Iterables.get(sections.get(1).getBasket(), 0).setFloatline2Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline1Length(11f);
            Iterables.get(sections.get(1).getBasket(), 1).setFloatline2Length(12f);

            List<SwingValidatorMessage> messages = service.validateSections(sections);
            assertMessages(messages, 0);

        }

    }

    protected void assertMessages(List<SwingValidatorMessage> messages, int expectedNbMessages) {

        StringBuilder builder = new StringBuilder();
        for (SimpleBeanValidatorMessage message : messages) {
            builder.append("\n").append(message.getScope()).append(" - ").append(message.getField()).append(" - ").append(message.getI18nError(message.getMessage()));
        }
        if (log.isInfoEnabled()) {
            log.info(builder.toString());
        }
        Assert.assertEquals("Should have found " + expectedNbMessages + " messages, but found " + messages.size(), expectedNbMessages, messages.size());
    }
}
