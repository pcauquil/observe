<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

<#if configuration??>
  <h3>${label}</h3>
  <ul>
    <#if isSqlDataSource() >
      <#if configuration.h2Database??>
        <li><strong>Usuario : </strong>${configuration.username}</li>
      <#elseif configuration.postgresDatabase??>
        <li><strong>URL jdbc : </strong>${configuration.jdbcUrl}</li>
        <li><strong>Usuario : </strong>${configuration.username}</li>
        <li><strong>Modo SSL : </strong>${configuration.useSsl?then('Oui', 'Non')}</li>
      </#if>
    <#else>
      <li><strong>URL del servidor : </strong>${configuration.serverUrl}</li>
      <li><strong>Usuario : </strong>${configuration.login}</li>
      <#if configuration.optionalDatabaseName.present>
        <li><strong>Base de datos : </strong>${configuration.optionalDatabaseName.get()}</li>
      </#if>
    </#if>
  </ul>
  <h3>Droits</h3>
    <li><strong>Referencial : </strong>
      <#if canReadReferential() >
        Lectura
        <#if canWriteReferential() >
          / Escritura
        </#if>
      <#elseif canWriteReferential()>
        Escritura
      <#else>
        No tiene derecho
      </#if>
    </li>
    <li><strong>Datos observador : </strong>
      <#if canReadData() >
        Lectura
        <#if canWriteData() >
          / Escritura
        </#if>
      <#elseif canWriteData() >
        Escritura
      <#else>
        No tiene derecho
      </#if>
    </li>
  </ul>
  <h3>Version</h3>
    v ${connection.version}
<#else>
  Ninguna fuente de datos cargada
</#if>

</body>
</html>
