<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

<#if configuration??>
  <h3>${label}</h3>
  <ul>
    <#if isSqlDataSource() >
      <#if configuration.h2Database??>
        <li><strong>User : </strong>${configuration.username}</li>
      <#elseif configuration.postgresDatabase??>
        <li><strong>Jdbc url : </strong>${configuration.jdbcUrl}</li>
        <li><strong>User : </strong>${configuration.username}</li>
        <li><strong>SSL mode : </strong>${configuration.useSsl?then('Yes', 'No')}</li>
      </#if>
    <#else>
      <li><strong>Server url : </strong>${configuration.serverUrl}</li>
      <li><strong>User : </strong>${configuration.login}</li>
      <#if configuration.optionalDatabaseName.present>
        <li><strong>Database : </strong>${configuration.optionalDatabaseName.get()}</li>
      </#if>
    </#if>
  </ul>
  <h3>Rights</h3>
    <li><strong>Referential : </strong>
      <#if canReadReferential() >
        Read
        <#if canWriteReferential() >
          / Write
        </#if>
      <#elseif canWriteReferential()>
        Write
      <#else>
        No rights
      </#if>
    </li>
    <li><strong>Données observateur : </strong>
      <#if canReadData() >
        Read
        <#if canWriteData() >
          / Write
        </#if>
      <#elseif canWriteData() >
        Write
      <#else>
        No rights
      </#if>
    </li>
  </ul>
  <h3>Version</h3>
    v ${connection.version}
<#else>
  No datasource has been loaded
</#if>

</body>
</html>
