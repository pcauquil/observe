<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#import "storageModelDataSourceConfiguration_fr.ftl" as storageInfo>
<#if backupAction>

  <h2>
    <#if local>
      Copia de seguridad de la base local
    <#elseif remote>
      Copia de seguridad de la base remota
    <#elseif server>
      Copia de seguridad del servidor
    </#if>
  </h2>

  <hr/>

  <h3>
    Ubicación de la copia de seguridad :
    <ul>
      <li>${backupFile.absolutePath}</li>
    </ul>
  </h3>

  <#if useSelectData && selectDataModel??>

    <h3>Datos a esportar :</h3>

    <ul>
      <li>
        <#if selectDataModel.isDataEmpty()>

          No hay datos de observaciones a exporter

        <#elseif selectDataModel.isDataFull()>

          Todos los datos de observador deben ser exportados
          (<#if selectDataModel.selectDataSize() == 1>
             1 marea
          <#else>
            ${selectDataModel.selectDataSize()} mareas
          </#if>).

        <#else>

          <#if selectDataModel.selectDataSize() == 1>
            1 marea
          <#else>
            ${selectDataModel.selectDataSize()} mareas
          </#if>

          a exportar

          <ul>

            <#list selectDataModel.getSelectedProgram() as program>

              <li>
                <!--FIXME Bavencoff 17/03/2016 use decorator-->
                [${program.getPropertyValue("gearTypePrefix")}] Programa ${program.getPropertyValue("label")}
                <ul>

                  <#list selectDataModel.getSelectedTripsByProgram(program) as trip>

                    <li>
                      <!--FIXME Bavencoff 17/03/2016 use decorator-->
                      ${trip.getPropertyValue("startDate")?date?string.short} - ${trip.getPropertyValue("endDate")?date?string.short} - ${trip.getPropertyValue("vessel")} - ${trip.getPropertyValue("observer")}
                    </li>

                  </#list>

                </ul>

              </li>

            </#list>

          </ul>


        </#if>

      </li>

      <li>El referencial va a ser exportado</li>
    </ul>

  </#if>

<#else>

  <#if dbMode.name() == "USE_LOCAL">

    <h2>Conexión a la base local</h2>
    <hr/>

    <h3>Ubicación de la base local :</h3>

    <ul>
      <li>${h2Config.directory.absolutePath}</li>
    </ul>

  <#elseif dbMode.name() == "CREATE_LOCAL">

    <#if doBackup>

      <h2>Copia de seguridad de la base local</h2>
      <hr/>
      <h3>Ubicación de la copia de seguridad :</h3>
      <ul>
        <li>${backupFile.absolutePath}</li>
      </ul>

    </#if>

    <h2>Creación de la base local</h2>
    <hr/>

    <#if creationMode.name() == "IMPORT_INTERNAL_DUMP">
      <h3>Importación con el último referencial cargado :</h3>

      <ul>
        <li>${initialDbDump.absolutePath}</li>
      </ul>

    <#elseif creationMode.name() == "IMPORT_EXTERNAL_DUMP">

      <h3>Importación con una copia de seguridad :</h3>

      <ul>
        <li>${dumpFile.absolutePath}</li>
      </ul>

    <#elseif creationMode.name() == "IMPORT_REMOTE_STORAGE">

      <h3>Importación del referencial con una base remota :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#elseif creationMode.name() == "IMPORT_SERVER_STORAGE">

      <h3>Importación del referencial con un servidor remoto :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    </#if>


  <#elseif dbMode.name() == "USE_REMOTE">
    <#if !adminAction??>

      <h2>Conexión a una base remota</h2>
      <hr/>

      <h3>Informaciones sobre la base remota a utilizar :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#else>

      <h2>${adminActionLabel}</h2>
      <hr/>

      <h3>Informaciones sobre la conexión remota a utilisat :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

      <#if adminAction.name() == "CREATE">

        <#if importReferentiel>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Importación del reférencial con una copia de seguridad :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Importación del referencial con una base remota :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Importación del referencial con un servidor remoto :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>No Importación de referencial</h3>
        </#if>

        <#if importData>

          <#if dataImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Importación de datos con una copia de seguridad :</h3>

            <ul>
              <li>${dataSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif dataImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Importación de datos con una base remota :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=dataSourceModel />

          <#elseif dataImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Importación de datos con un servidor remoto :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=dataSourceModel />

          </#if>

        <#else>
          <h3>No Importación de datos</h3>

        </#if>

      </#if>

      <h3>Seguridad</h3>
        <ul>
          <li><strong>Proprietario : </strong>${securityModel.administrateur.name}</li>
          <li><strong>Técnicos : </strong>${securityModel.technicalUserNames?join(", ")}</li>
          <li><strong>Lectores : </strong>${securityModel.dataUserNames?join(", ")}</li>
          <li><strong>Referenciales : </strong>${securityModel.referentialUserNames?join(", ")}</li>
        </ul>


    </#if>

  <#elseif dbMode.name() == "USE_SERVER">
    <#if !adminAction??>

      <h2>Connexion a un servidor remoto</h2>
      <hr/>

      <h3>Informaciones sobre el servidor remoto a utilizar :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#else>

      <h2>${adminAction.label}</h2>
      <hr/>

      <h3>Informaciones sobre el servidor remoto a utilizar :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

      <#if adminAction.name() == "CREATE">

        <#if importReferentiel>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Importación del referencial con una copia de seguridad :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Importación del referencial con una base remota :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Importación del referencial con un servidor remoto :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>No importación de referencial</h3>
        </#if>

        <#if importData>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Importación del referencial con una copia de seguridad :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Importación del referencial con una base remota :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Importación del referencial con un servidor remoto :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>No importación de datos</h3>

        </#if>

      </#if>

      <h3>Seguridad</h3>
      <ul>
        <li><strong>Propietario : </strong>${securityModel.administrateur.name}</li>
        <li><strong>Técnicos : </strong>${securityModel.technicalUserNames?join(", ")}</li>
        <li><strong>Lectores : </strong>${securityModel.dataUserNames?join(", ")}</li>
        <li><strong>Referenciales : </strong>${securityModel.referentialUserNames?join(", ")}</li>
      </ul>

    </#if>

  </#if>

  <h3>Política de actualización</h3>

  <ul>

    <#if canMigrate>

      <li>Actualización si se necesita (versión actual : ${modelVersion})</li>

      <#if showMigrationProgression>

        <li>Mostrar la progreción durante las actualizaciones</li>

      </#if>

      <#if showMigrationSql>

        <li>Mostrar las consultas sql durante las actualizaciones</li>

      </#if>

    <#else>

      <li>No actualización posible</li>

    </#if>

  </ul>
</#if>
</body>
</html>
