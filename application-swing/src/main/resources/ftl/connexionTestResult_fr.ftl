<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
  <#if connexionStatus.name() == "UNTESTED">

    <h3>La connexion n'a jamais été validée ou a été modifiée depuis le dernier test de connexion.</h3>

  <#elseif connexionStatus.name() == "FAILED">

    <h3>La connexion a échouée pour la raison suivante :</h3>

    <ul>
      <li>${connexionStatusError}</li>
    </ul>

  <#elseif connexionStatus.name() == "SUCCESS">

    Information sur la connexion :
    <#import "storageModelDataSourceConfiguration_fr.ftl" as storageInfo>
    <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

  </#if>
</body>
</html>
