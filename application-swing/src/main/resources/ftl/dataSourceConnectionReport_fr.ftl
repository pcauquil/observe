<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#import "storageModelDataSourceConfiguration_fr.ftl" as storageInfo>
<#if backupAction>

  <h2>
    <#if local>
      Sauvegarde de la base locale
    <#elseif remote>
      Sauvegarde de la base distante
    <#elseif server>
      Sauvegarde du serveur
    </#if>
  </h2>

  <hr/>

  <h3>
    Emplacement de la sauvegarde :
    <ul>
      <li>${backupFile.absolutePath}</li>
    </ul>
  </h3>

  <#if useSelectData && selectDataModel??>

    <h3>Données à exporter :</h3>

    <ul>
      <li>
        <#if selectDataModel.isDataEmpty()>

          Pas de données observateur à exporter

        <#elseif selectDataModel.isDataFull()>

          Toutes les données observateur sont à exporter
          (<#if selectDataModel.selectDataSize() == 1>
             1 marée
          <#else>
            ${selectDataModel.selectDataSize()} marées
          </#if>).

        <#else>

          <#if selectDataModel.selectDataSize() == 1>
            1 marée
          <#else>
            ${selectDataModel.selectDataSize()} marées
          </#if>

          à exporter

          <ul>

            <#list selectDataModel.getSelectedProgram() as program>

              <li>
                <!--FIXME Bavencoff 17/03/2016 use decorator-->
                [${program.getPropertyValue("gearTypePrefix")}] Programme ${program.getPropertyValue("label")}
                <ul>

                  <#list selectDataModel.getSelectedTripsByProgram(program) as trip>

                    <li>
                      <!--FIXME Bavencoff 17/03/2016 use decorator-->
                      ${trip.getPropertyValue("startDate")?date?string.short} - ${trip.getPropertyValue("endDate")?date?string.short} - ${trip.getPropertyValue("vessel")} - ${trip.getPropertyValue("observer")}
                    </li>

                  </#list>

                </ul>

              </li>

            </#list>

          </ul>


        </#if>

      </li>

      <li>Le référentiel sera exporté</li>
    </ul>

  </#if>

<#else>

  <#if dbMode.name() == "USE_LOCAL">

    <h2>Connection à la base locale</h2>
    <hr/>

    <h3>Emplacement de la base locale :</h3>

    <ul>
      <li>${h2Config.directory.absolutePath}</li>
    </ul>

  <#elseif dbMode.name() == "CREATE_LOCAL">

    <#if doBackup>

      <h2>Sauvegarde de la base locale</h2>
      <hr/>
      <h3>Emplacement de la sauvegarde :</h3>
      <ul>
        <li>${backupFile.absolutePath}</li>
      </ul>

    </#if>

    <h2>Création de la base locale</h2>
    <hr/>

    <#if creationMode.name() == "IMPORT_INTERNAL_DUMP">
      <h3>Import depuis le dernier référentiel téléchargé :</h3>

      <ul>
        <li>${initialDbDump.absolutePath}</li>
      </ul>

    <#elseif creationMode.name() == "IMPORT_EXTERNAL_DUMP">

      <h3>Import depuis une sauvegarde :</h3>

      <ul>
        <li>${dumpFile.absolutePath}</li>
      </ul>

    <#elseif creationMode.name() == "IMPORT_REMOTE_STORAGE">

      <h3>Import du référentiel depuis une base distante :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#elseif creationMode.name() == "IMPORT_SERVER_STORAGE">

      <h3>Import du référentiel depuis un serveur distant :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    </#if>


  <#elseif dbMode.name() == "USE_REMOTE">
    <#if !adminAction??>

      <h2>Connexion à une base distante</h2>
      <hr/>

      <h3>Informations sur la base distante à utiliser :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#else>

      <h2>${adminActionLabel}</h2>
      <hr/>

      <h3>Informations sur la connexion distance à utiliser :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

      <#if adminAction.name() == "CREATE">

        <#if importReferentiel>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Import du référentiel depuis une sauvegarde :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Import du référentiel depuis une base distante :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Import du référentiel depuis un serveur distant :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>Pas d'import de référentiel</h3>
        </#if>

        <#if importData>

          <#if dataImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Import de données depuis une sauvegarde :</h3>

            <ul>
              <li>${dataSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif dataImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Import de données  depuis une base distante :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=dataSourceModel />

          <#elseif dataImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Import de données depuis un serveur distant :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=dataSourceModel />

          </#if>

        <#else>
          <h3>Pas d'import de données</h3>

        </#if>

      </#if>

      <h3>Sécurité</h3>
        <ul>
          <li><strong>Propriétaire : </strong>${securityModel.administrateur.name}</li>
          <li><strong>Techniciens : </strong>${securityModel.technicalUserNames?join(", ")}</li>
          <li><strong>Lecteurs : </strong>${securityModel.dataUserNames?join(", ")}</li>
          <li><strong>Référentiels : </strong>${securityModel.referentialUserNames?join(", ")}</li>
        </ul>


    </#if>

  <#elseif dbMode.name() == "USE_SERVER">
    <#if !adminAction??>

      <h2>Connexion à un serveur distant</h2>
      <hr/>

      <h3>Informations sur le serveur distant à utiliser :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#else>

      <h2>${adminAction}</h2>
      <hr/>

      <h3>Informations sur le serveur distant à utiliser :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

      <#if adminAction.name() == "CREATE">

        <#if importReferentiel>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Import du référentiel depuis une sauvegarde :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Import du référentiel depuis une base distante :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Import du référentiel depuis un serveur distant :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>Pas d'import de référentiel</h3>
        </#if>

        <#if importData>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Import du référentiel depuis une sauvegarde :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Import du référentiel depuis une base distante :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Import du référentiel depuis un serveur distant :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>Pas d'import de données</h3>

        </#if>

      </#if>

      <h3>Sécurité</h3>
      <ul>
        <li><strong>Propriétaire : </strong>${securityModel.administrateur.name}</li>
        <li><strong>Techniciens : </strong>${securityModel.technicalUserNames?join(", ")}</li>
        <li><strong>Lecteurs : </strong>${securityModel.dataUserNames?join(", ")}</li>
        <li><strong>Référentiels : </strong>${securityModel.referentialUserNames?join(", ")}</li>
      </ul>

    </#if>

  </#if>

  <h3>Politique de mise à jour</h3>

  <ul>

    <#if canMigrate>

      <li>Mise à jour si nécessaire (version actuelle : ${modelVersion})</li>

      <#if showMigrationProgression>

        <li>Afficher la progression lors des mises à jour</li>

      </#if>

      <#if showMigrationSql>

        <li>Afficher les requêtes sql lors des mises à jour</li>

      </#if>

    <#else>

      <li>Pas de mise à jour possible</li>

    </#if>

  </ul>
</#if>
</body>
</html>
