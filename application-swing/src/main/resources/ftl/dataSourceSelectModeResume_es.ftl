<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

  <h3>Tipo de fuentes de datos seleccionado</h3>

  <#if dbMode.name() == "USE_LOCAL">
    Utilizar una base local de tipo h2
  <#elseif dbMode.name() == "CREATE_LOCAL">
    Crear una base local de tipo h2
  <#elseif dbMode.name() == "USE_REMOTE">
    Utilizar una base remota de tipo postgres
  <#elseif dbMode.name() == "USE_SERVER">
    Utilizar un servidor remoto
  <#else>
    Ningún tipo de fuente de datos seleccionado
  </#if>

  <h3>Modo de creación seleccionado</h3>

  <#if dbMode.name() == "CREATE_LOCAL">
    <#if !creationMode??>
      Ningún modo de creación seleccionado
    <#elseif creationMode.name() == "EMPTY">
      Generar una nueva base local vacía. Esta base no va a tener un referencial así que luego se necesitara que procede a una importación de referentcial...
    <#elseif creationMode.name() == "IMPORT_INTERNAL_DUMP">
      Generar una nueva base local a partir de la última versión de la base incrustada.
    <#elseif creationMode.name() == "IMPORT_EXTERNAL_DUMP">
      Crear una nueva base local a partir de la última copia de seguridad de la aplicación.
    <#elseif creationMode.name() == "IMPORT_LOCAL_STORAGE">
      Generar una nueva base local e importar el referencial de una otra base local.
    <#elseif creationMode.name() == "IMPORT_REMOTE_STORAGE">
      Generar una nueva base local e importar el referencial de una otra base remota.
    <#elseif creationMode.name() == "IMPORT_SERVER_STORAGE">
      Generar una nueva base local e importar el referencial de un servidor remoto.
    </#if>
  <#else>
    No es necesario.
  </#if>
</body>
</html>
