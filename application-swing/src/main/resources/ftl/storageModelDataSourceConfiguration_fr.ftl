<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<#macro storageModelDataSourceInformation storageModel>
<#if storageModel??>
  <ul>
    <#if storageModel.editRemoteConfig>
      <li><strong>URL jdbc : </strong>${storageModel.pgConfig.jdbcUrl}</li>
      <li><strong>Utilisateur : </strong>${storageModel.pgConfig.username}</li>
      <li><strong>Mot de passe : </strong>*****</li>
      <li><strong>Mode SSL : </strong>${storageModel.pgConfig.useSsl?then('Oui', 'Non')}</li>
      <#elseif storageModel.editServerConfig>
        <li><strong>URL du serveur : </strong>${storageModel.restConfig.serverUrl}</li>
        <li><strong>Utilisateur : </strong>${storageModel.restConfig.login}</li>
        <li><strong>Mot de passe : </strong>*****</li>
        <#if storageModel.restConfig.optionalDatabaseName.present>
          <li><strong>Base de données : </strong>${storageModel.restConfig.optionalDatabaseName.get()}</li>
        </#if>
        <#else>
          <li><strong>Utilisateur : </strong>${storageModel.h2Config.username}</li>
          <li><strong>Mot de passe : </strong>*****</li>
    </#if>
    <li><strong>Droits : </strong>
      <ul>
        <li>
          <em>Référentiel : </em>
          <#if storageModel.dataSourceInformation.canReadReferential() >
            Lecture
            <#if storageModel.dataSourceInformation.canWriteReferential() >
              / Ecriture
            </#if>
            <#elseif storageModel.dataSourceInformation.canWriteReferential()>
              Ecriture
              <#else>
                Aucun droit
          </#if>
        </li>
        <li>
          <em>Données observateur : </em>
          <#if storageModel.dataSourceInformation.canReadData() >
            Lecture
            <#if storageModel.dataSourceInformation.canWriteData() >
              / Ecriture
            </#if>
            <#elseif storageModel.dataSourceInformation.canWriteData() >
              Ecriture
              <#else>
                Aucun droit
          </#if>
        </li>
      </ul>
    </li>
  </ul>

<#else>
  NO STORAGE MODEL
</#if>
</#macro>
