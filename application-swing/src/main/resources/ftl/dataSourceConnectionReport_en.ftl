<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
<#import "storageModelDataSourceConfiguration_fr.ftl" as storageInfo>
<#if backupAction>

  <h2>
    <#if local>
      Local database backup
    <#elseif remote>
      Remote database backup
    <#elseif server>
      Server backup
    </#if>
  </h2>

  <hr/>

  <h3>
    Backup location :
    <ul>
      <li>${backupFile.absolutePath}</li>
    </ul>
  </h3>

  <#if useSelectData && selectDataModel??>

    <h3>Data to export :</h3>

    <ul>
      <li>
        <#if selectDataModel.isDataEmpty()>

          No data to export

        <#elseif selectDataModel.isDataFull()>

          All observed data have to be exported
          (<#if selectDataModel.selectDataSize() == 1>
             1 trip
          <#else>
            ${selectDataModel.selectDataSize()} trips
          </#if>).

        <#else>

          <#if selectDataModel.selectDataSize() == 1>
            1 trip
          <#else>
            ${selectDataModel.selectDataSize()} trips
          </#if>

          à exporter

          <ul>

            <#list selectDataModel.getSelectedProgram() as program>

              <li>
                <!--FIXME Bavencoff 17/03/2016 use decorator-->
                [${program.getPropertyValue("gearTypePrefix")}] Programme ${program.getPropertyValue("label")}
                <ul>

                  <#list selectDataModel.getSelectedTripsByProgram(program) as trip>

                    <li>
                      <!--FIXME Bavencoff 17/03/2016 use decorator-->
                      ${trip.getPropertyValue("startDate")?date?string.short} - ${trip.getPropertyValue("endDate")?date?string.short} - ${trip.getPropertyValue("vessel")} - ${trip.getPropertyValue("observer")}
                    </li>

                  </#list>

                </ul>

              </li>

            </#list>

          </ul>


        </#if>

      </li>

      <li>The referential will be exported</li>
    </ul>

  </#if>

<#else>

  <#if dbMode.name() == "USE_LOCAL">

    <h2>Connection to the local database</h2>
    <hr/>

    <h3>Local database location:</h3>

    <ul>
      <li>${h2Config.directory.absolutePath}</li>
    </ul>

  <#elseif dbMode.name() == "CREATE_LOCAL">

    <#if doBackup>

      <h2>Local database backup</h2>
      <hr/>
      <h3>Backup location :</h3>
      <ul>
        <li>${backupFile.absolutePath}</li>
      </ul>

    </#if>

    <h2>Local database creation</h2>
    <hr/>

    <#if creationMode.name() == "IMPORT_INTERNAL_DUMP">
      <h3>Referential Import from the last imported referential :</h3>

      <ul>
        <li>${initialDbDump.absolutePath}</li>
      </ul>

    <#elseif creationMode.name() == "IMPORT_EXTERNAL_DUMP">

      <h3>Backup import :</h3>

      <ul>
        <li>${dumpFile.absolutePath}</li>
      </ul>

    <#elseif creationMode.name() == "IMPORT_REMOTE_STORAGE">

      <h3>Referential import from a remote database :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#elseif creationMode.name() == "IMPORT_SERVER_STORAGE">

      <h3>Referential import from a remote server :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    </#if>


  <#elseif dbMode.name() == "USE_REMOTE">
    <#if !adminAction??>

      <h2>Remote database connection</h2>
      <hr/>

      <h3>Remote database informations :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#else>

      <h2>${adminActionLabel}</h2>
      <hr/>

      <h3>Remote connection informations :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

      <#if adminAction.name() == "CREATE">

        <#if importReferentiel>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Referential import from a backup :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Referential import from a remote database :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Referential import from a remote server :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>No referential import</h3>
        </#if>

        <#if importData>

          <#if dataImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Data import from a backup :</h3>

            <ul>
              <li>${dataSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif dataImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Data import from a remote database :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=dataSourceModel />

          <#elseif dataImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Data import from a remote server :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=dataSourceModel />

          </#if>

        <#else>
          <h3>No data import</h3>

        </#if>

      </#if>

      <h3>Security</h3>
        <ul>
          <li><strong>Owner : </strong>${securityModel.administrateur.name}</li>
          <li><strong>Technicians : </strong>${securityModel.technicalUserNames?join(", ")}</li>
          <li><strong>Readers : </strong>${securityModel.dataUserNames?join(", ")}</li>
          <li><strong>Referentials : </strong>${securityModel.referentialUserNames?join(", ")}</li>
        </ul>


    </#if>

  <#elseif dbMode.name() == "USE_SERVER">
    <#if !adminAction??>

      <h2>Remote server connection</h2>
      <hr/>

      <h3>Remote server informations :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

    <#else>

      <h2>${adminAction.label}</h2>
      <hr/>

      <h3>Remote server informations :</h3>

      <@storageInfo.storageModelDataSourceInformation storageModel=.data_model />

      <#if adminAction.name() == "CREATE">

        <#if importReferentiel>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Referential import from a backup :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Referential import from a remote database :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Referential import from a remote server :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>No referential import</h3>
        </#if>

        <#if importData>

          <#if referentielImportMode.name() == "IMPORT_EXTERNAL_DUMP">

            <h3>Referential import from a backup :</h3>

            <ul>
              <li>${centralSourceModel.dumpFile.absolutePath}</li>
            </ul>

          <#elseif referentielImportMode.name() == "IMPORT_REMOTE_STORAGE">

            <h3>Referential import from a remote database :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          <#elseif referentielImportMode.name() == "IMPORT_SERVER_STORAGE">

            <h3>Referential import from a remote server :</h3>

            <@storageInfo.storageModelDataSourceInformation storageModel=centralSourceModel />

          </#if>

        <#else>
          <h3>No data import</h3>

        </#if>

      </#if>

      <h3>Sécurité</h3>
      <ul>
        <li><strong>Owner : </strong>${securityModel.administrateur.name}</li>
        <li><strong>Technicians : </strong>${securityModel.technicalUserNames?join(", ")}</li>
        <li><strong>Readers : </strong>${securityModel.dataUserNames?join(", ")}</li>
        <li><strong>Referentials : </strong>${securityModel.referentialUserNames?join(", ")}</li>
      </ul>

    </#if>

  </#if>

  <h3>Update policy</h3>

  <ul>

    <#if canMigrate>

      <li>Update if required (actual version : ${modelVersion})</li>

      <#if showMigrationProgression>

        <li>Show update progression</li>

      </#if>

      <#if showMigrationSql>

        <li>Show sql update queries</li>

      </#if>

    <#else>

      <li>No update available</li>

    </#if>

  </ul>
</#if>
</body>
</html>
