<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<#macro storageModelDataSourceInformation storageModel>
<#if storageModel??>
  <ul>
    <#if storageModel.editRemoteConfig>
      <li><strong>Jdbc url : </strong>${storageModel.pgConfig.jdbcUrl}</li>
      <li><strong>User : </strong>${storageModel.pgConfig.username}</li>
      <li><strong>Password : </strong>*****</li>
      <li><strong>SSL mode : </strong>${storageModel.pgConfig.useSsl?then('Yes', 'No')}</li>
      <#elseif storageModel.editServerConfig>
        <li><strong>Server url : </strong>${storageModel.restConfig.serverUrl}</li>
        <li><strong>User : </strong>${storageModel.restConfig.login}</li>
        <li><strong>Password : </strong>*****</li>
        <#if storageModel.restConfig.optionalDatabaseName.present>
          <li><strong>Database : </strong>${storageModel.restConfig.optionalDatabaseName.get()}</li>
        </#if>
        <#else>
          <li><strong>User : </strong>${storageModel.h2Config.username}</li>
          <li><strong>Password : </strong>*****</li>
    </#if>
    <li><strong>Rights : </strong>
      <ul>
        <li>
          <em>Referential : </em>
          <#if storageModel.dataSourceInformation.canReadReferential() >
            Read
            <#if storageModel.dataSourceInformation.canWriteReferential() >
              / Write
            </#if>
            <#elseif storageModel.dataSourceInformation.canWriteReferential()>
              Write
              <#else>
                No rights
          </#if>
        </li>
        <li>
          <em>Données observateur : </em>
          <#if storageModel.dataSourceInformation.canReadData() >
            Read
            <#if storageModel.dataSourceInformation.canWriteData() >
              / Write
            </#if>
            <#elseif storageModel.dataSourceInformation.canWriteData() >
              Write
              <#else>
                No rights
          </#if>
        </li>
      </ul>
    </li>
  </ul>

<#else>
  NO STORAGE MODEL
</#if>
</#macro>
