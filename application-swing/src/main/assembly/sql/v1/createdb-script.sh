###
# #%L
# ObServe :: Admin Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
#!/bin/sh

# script pour creation de la base obstuna :
#  - creation du role proprietaire de la base
#  - creation de la base
#  - creation des roles
#  - creation du schema + remplissage referentiel
#  - ajout de la securite sur la base et sur les roles
#
# Note : Ce script doit etre lance par l'utilisateur postgres sur la machine
# hebergeant la base.


if [ ! $# -eq 5 ] ; then
    echo "le nombre de paramètres n'est pas correct."
    echo ""
    echo "usage $0 dbname owner \"technicien1 technicien2 ...\" \"reader1 reader2 ...\" \"-h localhost | \"\"\""
    exit 1
fi

# parameter 1 : db name
OBSTUNA_DB=$1

# parameter 2 : owner
OBSTUNA_OWNER=$2

# parameter 3 : techniciens
OBSTUNA_TECHNICIENS="$3"

# parameter 4 : readers
OBSTUNA_READERS="$4"

# parameter 5 : host ("-h localhost" ou vide)
OBSTUNA_HOST="$5"

OBSTUNA_TIMESTAMP=`date | tr ' '  '_'`

OBSTUNA_DB_SCRIPT=sql/obstuna.sql
OBSTUNA_SECURITY_SCRIPT=tmp/obstuna-security_$OBSTUNA_TIMESTAMP.sql
OBSTUNA_LOGFILE=tmp/$0-$OBSTUNA_TIMESTAMP.log


mkdir -p tmp

(cd security ; ./create-security-script.sh ../"$OBSTUNA_SECURITY_SCRIPT" "$OBSTUNA_OWNER" "$OBSTUNA_TECHNICIENS" "$OBSTUNA_READERS")

echo "Création du role proprietaire '$OBSTUNA_OWNER'..."
createuser -SDRP "$OBSTUNA_OWNER"

for role in $OBSTUNA_TECHNICIENS ;
do
    echo "Création du role technicien '$role'..."
    createuser -SDRP "$role"
done

for role in $OBSTUNA_READERS ;
do
    echo "Création du role lecteur '$role'..."
    createuser -SDRP "$role"
done

echo -n "Création de la base '$OBSTUNA_DB' (proprietaire $OBSTUNA_OWNER)..."
createdb -E UTF-8 -O "$OBSTUNA_OWNER" "$OBSTUNA_DB" > $OBSTUNA_LOGFILE
if [ $? -eq 1 ] ; then
    echo "KO"
    exit 1
fi
echo "OK"

echo -n "Création du schema et référentiel (script '$OBSTUNA_DB_SCRIPT') sur $OBSTUNA_DB ..."
psql -e -f $OBSTUNA_DB_SCRIPT $OBSTUNA_HOST $OBSTUNA_DB >> $OBSTUNA_LOGFILE
if [ $? -eq 1 ] ; then
    echo "KO"
    exit 1
fi
echo "OK"

echo -n "ajout securite (script $OBSTUNA_SECURITY_SCRIPT) sur $OBSTUNA_DB ..."
psql -e -f $OBSTUNA_SECURITY_SCRIPT $OBSTUNA_HOST $OBSTUNA_DB >> $OBSTUNA_LOGFILE
if [ $? -eq 1 ] ; then
    echo "KO"
    exit 1
fi
echo "OK"
