---
-- #%L
-- ObServe :: Application Swing
-- %%
-- Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the 
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public 
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activite; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE activite (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    heureobservation timestamp without time zone,
    quadrant integer,
    latitude real,
    longitude real,
    vitessebateau real,
    temperaturesurface real,
    distancesystemeobserve real,
    commentaire character varying(255),
    open boolean,
    calee character varying(255),
    activitebateau character varying(255),
    activiteenvironnante character varying(255),
    ventbeaufort character varying(255),
    modedetection character varying(255),
    causenoncoupsenne character varying(255),
    route character varying(255)
);


-- ALTER TABLE public.activite OWNER TO admin;

--
-- Name: activite_systemeobserve; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE activite_systemeobserve (
    activite character varying(255) NOT NULL,
    systemeobserve character varying(255) NOT NULL
);


-- ALTER TABLE public.activite_systemeobserve OWNER TO admin;

--
-- Name: activitebateau; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE activitebateau (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);


-- ALTER TABLE public.activitebateau OWNER TO admin;

--
-- Name: activiteenvironnante; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE activiteenvironnante (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);


-- ALTER TABLE public.activiteenvironnante OWNER TO admin;

--
-- Name: baliselue; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE baliselue (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code character varying(255),
    typebalise character varying(255),
    operationbalise character varying(255),
    objetflottant character varying(255)
);


-- ALTER TABLE public.baliselue OWNER TO admin;

--
-- Name: bateau; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE bateau (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    codequille integer,
    codeflotte integer,
    datechargement timestamp without time zone,
    anneeservice integer,
    longueurhorstoute real,
    capacitetransport real,
    puissancegroupeprincipal integer,
    vitessemaximaleprocespection real,
    libelle character varying(255),
    commentaire character varying(255),
    pavillon character varying(255),
    pecherie character varying(255),
    jauge character varying(255)
);


-- ALTER TABLE public.bateau OWNER TO admin;

--
-- Name: calee; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE calee (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    heuredebut timestamp without time zone,
    heurefincoulissage timestamp without time zone,
    heurefin timestamp without time zone,
    profondeurmaximumengin integer,
    vitessecourant real,
    directioncourant integer,
    profondeursommetbanc integer,
    profondeurmoyennebanc integer,
    epaisseurbanc integer,
    utilisationsonar boolean,
    nomsupply character varying(255),
    rejetthon boolean,
    rejetfaune boolean,
    commentaire character varying(255),
    causecoupnul character varying(255)
);


-- ALTER TABLE public.calee OWNER TO admin;

--
-- Name: capturefaune; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE capturefaune (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    poidsestime real,
    nombreestime integer,
    poidsmoyen integer,
    taillemoyenne integer,
    commentaire character varying(255),
    raisonrejet character varying(255),
    espece character varying(255),
    devenirfaune character varying(255),
    calee character varying(255)
);


-- ALTER TABLE public.capturefaune OWNER TO admin;

--
-- Name: capturefaunecalcule; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE capturefaunecalcule (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    poidsestime real,
    nombreestime integer,
    poidsmoyen integer,
    taillemoyenne integer,
    commentaire character varying(255),
    raisonrejet character varying(255),
    especefaune character varying(255),
    devenirfaune character varying(255),
    taillepoidsfaune character varying(255),
    calee character varying(255)
);


-- ALTER TABLE public.capturefaunecalcule OWNER TO admin;

--
-- Name: capturethon; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE capturethon (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    cuve character varying(255),
    rejete boolean NOT NULL,
    calee character varying(255) NOT NULL,
    categoriepoids character varying(255) NOT NULL,
    raisonrejet character varying(255),
    topiacreatedate date,
    poids integer,
    surlepont boolean,
    commentaire character varying(255)
);



--
-- Name: categoriebateau; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE categoriebateau (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libellejauge character varying(255),
    libellecapacite character varying(255)
);



--
-- Name: categoriepoids; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE categoriepoids (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    espece character varying(255)
);



--
-- Name: causecoupnul; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE causecoupnul (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: causenoncoupsenne; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE causenoncoupsenne (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);



--
-- Name: devenirfaune; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE devenirfaune (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: devenirobjet; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE devenirobjet (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: echantillonfaune; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE echantillonfaune (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    commentaire character varying(255),
    calee character varying(255)
);



--
-- Name: echantillonthon; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE echantillonthon (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    nature character varying(255),
    commentaire character varying(255),
    calee character varying(255)
);


--
-- Name: especefaune; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE especefaune (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    code3l character varying(255),
    libellescientifique character varying(255),
    libellefr character varying(255),
    libellees character varying(255),
    libelleuk character varying(255),
    needcomment boolean,
    groupe character varying(255)
);



--
-- Name: especefauneobservee; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE especefauneobservee (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    nombre integer,
    statut integer,
    espece character varying(255),
    statutespece character varying(255),
    objetflottant character varying(255)
);



--
-- Name: especethon; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE especethon (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    code3l character varying(255),
    libellescientifique character varying(255),
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: estimationbanc; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE estimationbanc (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    calee character varying(255) NOT NULL,
    espece character varying(255) NOT NULL,
    topiacreatedate date,
    poids integer,
    poidsindividuel integer
);



--
-- Name: estimationbancobjet; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE estimationbancobjet (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    poids integer,
    espece character varying(255),
    objetflottant character varying(255)
);



--
-- Name: groupeespecefaune; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE groupeespecefaune (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: maree; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE maree (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    datedebut timestamp without time zone,
    datefin timestamp without time zone,
    organisme character varying(255),
    datearriveeport character varying(255),
    commentaire character varying(255),
    niveauverification integer,
    open boolean,
    ocean character varying(255),
    senne character varying(255),
    observateur character varying(255),
    bateau character varying(255),
    programme character varying(255)
);



--
-- Name: modedetection; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE modedetection (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);


ALTER TABLE public.modedetection OWNER TO admin;

--
-- Name: objetflottant; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE objetflottant (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    nbjoureau integer,
    commentaire character varying(255),
    nomsupply character varying(255),
    appartenance integer,
    type character varying(255),
    devenir character varying(255),
    activite character varying(255)
);


ALTER TABLE public.objetflottant OWNER TO admin;

--
-- Name: objetflottant_operation; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE objetflottant_operation (
    objetflottant character varying(255) NOT NULL,
    operation character varying(255) NOT NULL
);


--
-- Name: observateur; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE observateur (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    identifiant integer,
    nom character varying(255),
    prenom character varying(255)
);


--
-- Name: ocean; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE ocean (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);


--
-- Name: operationbalise; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE operationbalise (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);


--
-- Name: operationobjet; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE operationobjet (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);


--
-- Name: pays; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE pays (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);



--
-- Name: programme; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE programme (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);



--
-- Name: raisonrejet; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE raisonrejet (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: route; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE route (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    jourobservation timestamp without time zone,
    lochmatin integer,
    lochsoir integer,
    commentaire character varying(255),
    niveauverification integer,
    open boolean,
    maree character varying(255)
);



--
-- Name: senne; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE senne (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    identifiant integer,
    longueur integer,
    chute integer,
    poidslest integer
);



--
-- Name: statutespece; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE statutespece (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);



--
-- Name: systemeobserve; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE systemeobserve (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);


--
-- Name: taillefaune; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE taillefaune (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    longueur integer,
    sexe integer,
    referencephoto character varying(255),
    espece character varying(255),
    echantillonfaune character varying(255)
);



--
-- Name: taillepoidsfaune; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE taillepoidsfaune (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    annee integer,
    poidslongueura real,
    poidslongueurb real,
    ocean character varying(255),
    espece character varying(255)
);



--
-- Name: taillepoidsthon; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE taillepoidsthon (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    annee integer,
    poidslongueura real,
    poidslongueurb real,
    espece character varying(255),
    ocean character varying(255)
);



--
-- Name: taillethon; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE taillethon (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    longueur real NOT NULL,
    echantillonthon character varying(255) NOT NULL,
    espece character varying(255) NOT NULL,
    topiacreatedate date,
    codemesure integer,
    effectif integer
);



--
-- Name: typebalise; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE typebalise (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: typebateau; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE typebateau (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255)
);



--
-- Name: typeobjet; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE typeobjet (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelle character varying(255),
    needcomment boolean
);



--
-- Name: ventbeaufort; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE ventbeaufort (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    code integer,
    libelledescriptifvent character varying(255),
    libellevitessevent character varying(255),
    libelledescriptifmer character varying(255),
    libellehauteurmoyennevagues character varying(255)
);

--
-- Name: tmsversion; Type: TABLE; Schema: public; Owner: admin; Tablespace:
--

CREATE TABLE tmsversion (
    topiaid character varying(255) NOT NULL,
    topiaversion bigint NOT NULL,
    topiacreatedate date,
    version character varying(255)
);


--
-- Data for Name: activite; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY activite (topiaid, topiaversion, topiacreatedate, heureobservation, quadrant, latitude, longitude, vitessebateau, temperaturesurface, distancesystemeobserve, commentaire, open, calee, activitebateau, activiteenvironnante, ventbeaufort, modedetection, causenoncoupsenne, route) FROM stdin;
\.


--
-- Data for Name: activite_systemeobserve; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY activite_systemeobserve (activite, systemeobserve) FROM stdin;
\.


--
-- Data for Name: activitebateau; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY activitebateau (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675370#0.9125190289998782	1	2009-04-15	13	Pose ou modification d'une épave.	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675371#0.15012142294280495	1	2009-04-15	14	Récupération d'une épave propre	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675371#0.23195165331640677	1	2009-04-15	15	Récupération d'une épave étrangère	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675372#0.21399033380125898	1	2009-04-15	16	Fin de veille	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675372#0.6114689927107798	1	2009-04-15	17	Recherche sur hauts-fonds	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675372#0.43920247699937853	1	2009-04-15	99	Autres ( à préciser dans les notes )	t
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675349#0.363119635949572	1	2009-04-15	0	Au port	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675368#0.976590514005213	1	2009-04-15	1	Transit ( route sans recherche )	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675368#0.23635552874754517	1	2009-04-15	2	Recherche ( général )	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675368#0.9186535488462055	1	2009-04-15	3	Recherche exclusive d'objets flottants	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675368#0.4510981840764775	1	2009-04-15	4	Route vers des systèmes observés	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675369#0.08910085510139154	1	2009-04-15	5	Thonier arrivant sur le système détecté	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675369#0.12552908048322586	1	2009-04-15	6	Début de pêche ( larguage du skiff )	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675369#0.027011527426829218	1	2009-04-15	7	Fin de pêche ( remontée du skiff)	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675369#0.10731168577264139	1	2009-04-15	8	En dérive près d'un banc ou d'un objet flottant	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675370#0.6475754136223717	1	2009-04-15	9	En dérive de nuit ( moteur stoppé )	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675370#0.45119681303299985	1	2009-04-15	10	A la cape	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675370#0.495613158646268	1	2009-04-15	11	Avaries en mer	f
fr.ird.observe.entities.referentiel.ActiviteBateau#1239832675370#0.7823480383025052	1	2009-04-15	12	Transbordement en pleine mer	f
\.


--
-- Data for Name: activiteenvironnante; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY activiteenvironnante (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.ActiviteEnvironnante#1239832675515#0.7689130226736774	1	2009-04-15	0	Indéterminé
fr.ird.observe.entities.referentiel.ActiviteEnvironnante#1239832675515#0.16947202179029996	1	2009-04-15	1	Seul dans la zone
fr.ird.observe.entities.referentiel.ActiviteEnvironnante#1239832675515#0.4863937578955101	1	2009-04-15	2	Dans un groupe, avec autre(s) thonier(s) visible(s) au radar
fr.ird.observe.entities.referentiel.ActiviteEnvironnante#1239832675516#0.5705551463868741	1	2009-04-15	3	De même engin et de même pavillon
fr.ird.observe.entities.referentiel.ActiviteEnvironnante#1239832675516#0.9870201380782583	1	2009-04-15	4	D'engins différents et de même pavillon
fr.ird.observe.entities.referentiel.ActiviteEnvironnante#1239832675516#0.16306326741128818	1	2009-04-15	5	De même engin et de pavillons différents
fr.ird.observe.entities.referentiel.ActiviteEnvironnante#1239832675516#0.09902335653153349	1	2009-04-15	6	D'engins et de pavillons différents
\.


--
-- Data for Name: baliselue; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY baliselue (topiaid, topiaversion, topiacreatedate, code, typebalise, operationbalise, objetflottant) FROM stdin;
\.


--
-- Data for Name: bateau; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY bateau (topiaid, topiaversion, topiacreatedate, code, codequille, codeflotte, datechargement, anneeservice, longueurhorstoute, capacitetransport, puissancegroupeprincipal, vitessemaximaleprocespection, libelle, commentaire, pavillon, pecherie, jauge) FROM stdin;
fr.ird.observe.entities.referentiel.Bateau#1239832675881#0.2673983572610924	1	2009-04-15	4	4	1	\N	1963	30.799999	142	600	0	JACQUES CHRISTIAN	Coule	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832675909#0.3141672787961748	1	2009-04-15	5	5	1	\N	1963	0	0	0	0	KERSIDAN	Sardinier Pointe Noire	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832675967#0.05192473809216902	1	2009-04-15	6	6	1	\N	1965	41.799999	380	950	0	NAVARRA	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676024#0.02979239433292613	1	2009-04-15	8	8	1	\N	1964	31.799999	140	620	0	TROPICAL	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676633#0.6751681311403186	1	2009-04-15	24	24	1	\N	0	0	0	0	0	ANDRE CHANTAL	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676660#0.38397681020258445	1	2009-04-15	25	25	1	\N	1958	24.75	110	330	0	BENIGUET	Arrete fin 73	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676671#0.47871457917337956	1	2009-04-15	26	26	1	\N	1958	28.799999	0	0	0	CHEVALIER BAYARD	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676702#0.9608465922314458	1	2009-04-15	27	27	1	\N	1958	27.75	82	300	0	BELLE GUEUSE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676713#0.3422425994370628	1	2009-04-15	28	28	1	\N	1958	27.75	82	350	0	CALLIOPE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676740#0.8716868682651489	1	2009-04-15	29	29	1	\N	1958	27.75	82	350	0	INAKI	Ex CAVALIER DES VAGUES, Arrete fin 73	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676747#0.5777187514643711	1	2009-04-15	30	30	1	\N	1965	0	0	0	0	DOUCE FRANCE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676753#0.749992433462453	1	2009-04-15	31	31	1	\N	1964	31	0	550	0	FOULQUE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676777#0.15946479296724336	1	2009-04-15	32	32	1	\N	1956	25.5	65	350	0	GOMERA	Arrete fin 73	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676783#0.9781033285383098	1	2009-04-15	33	33	1	\N	0	0	0	0	0	HENRI MICHEL	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676789#0.49682351559225024	1	2009-04-15	34	34	1	\N	1965	31	0	550	0	MACAREUX	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676838#0.526998348663061	1	2009-04-15	35	35	1	\N	1963	31	0	550	0	NATHALIE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676843#0.46649759144773306	1	2009-04-15	36	36	1	\N	1968	38.25	325	865	0	PATUDO	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676848#0.293898275929791	1	2009-04-15	37	37	1	\N	1959	29.01	82	440	0	PERCEVAL	Sardinier coule a Pointe Noire	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676852#0.3262328266853113	1	2009-04-15	38	38	1	\N	1956	25.200001	85	350	0	GUERNIKA 2	ex  PETIT LOIC	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676862#0.5535323684636367	1	2009-04-15	40	40	1	\N	1964	30.799999	142	600	0	RAVENNE	Senneur depuis fevrier 70	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676977#0.9751722256958688	1	2009-04-15	56	56	99	\N	0	0	0	0	0	CAP SAINT ANNE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676982#0.2850621986559837	1	2009-04-15	57	57	99	\N	0	0	0	0	0	CAP SAINT VINCENT	Cap de 650 T systeme americain.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676987#0.6351814687352666	1	2009-04-15	58	58	99	\N	0	0	0	0	0	DAY ISLANDS	Cap de 1100 T systeme americain.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676991#0.7799944541289812	1	2009-04-15	59	59	99	\N	0	0	0	0	0	CAP COD	Cap de 900 T systeme americain.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676996#0.40872303666868337	1	2009-04-15	60	60	99	\N	1969	23.469999	0	350	0	BOLD VENTURE	Cap de 930 T systeme americain.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677000#0.8928546974352654	1	2009-04-15	61	61	99	\N	0	0	0	0	0	NAUTILUS	Cap systeme americain 920 T.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677005#0.46102741340883946	1	2009-04-15	62	62	99	\N	0	0	0	0	0	NEPTUNE	AMERICAIN - Cap de 900 T systeme americain.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677071#0.12538456201508552	1	2009-04-15	63	63	99	\N	0	0	0	0	0	BLUE PACIFIC	Cap systeme americain 930 T.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677076#0.23593096939385505	1	2009-04-15	64	64	99	\N	0	0	0	0	0	CITY OF PANAMA	Cap transport systeme americain 930 T	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677081#0.2550595484840944	1	2009-04-15	65	65	99	\N	0	0	0	0	0	NORMANDIE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677087#0.7393824802795664	1	2009-04-15	66	66	99	\N	1973	0	0	0	0	MARINER	Cap systeme americain 1100 T.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677093#0.06522651155134018	1	2009-04-15	67	67	99	\N	1973	65.220001	0	3600	0	CAROL VIRGINIA	Cap systeme americain 1100 T.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677098#0.8382874805372169	1	2009-04-15	68	68	1	\N	1957	26	80	300	0	AIGLE DES MERS	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677104#0.4198106146968025	1	2009-04-15	69	69	1	\N	1958	20	0	300	0	AMIRAL BARJOT	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677109#0.0555535918318637	1	2009-04-15	70	70	1	\N	1956	25.5	65	350	0	ASTRIA	Saisi en Mauritanie en 1980, puis arrete dans la foulee,	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677121#0.05816104759661944	1	2009-04-15	72	72	1	\N	1958	24	0	300	0	BARBARA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677207#0.06768960223760245	1	2009-04-15	87	87	1	\N	1956	22	50	350	0	GISELE MARIE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677214#0.5060988549885651	1	2009-04-15	88	88	1	\N	1956	22	35	330	0	GURE BIZIA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677228#0.5320400301320354	1	2009-04-15	89	89	1	\N	1956	20	35	300	0	GURE IZARRA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677234#0.7072142886711905	1	2009-04-15	90	90	1	\N	1958	24	0	300	0	HIPPOMENE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677239#0.5490519387714696	1	2009-04-15	91	91	1	\N	0	0	0	0	0	INTRON MARIA G M	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677245#0.17272701695451897	1	2009-04-15	92	92	1	\N	0	0	0	0	0	INTRON VARIA A M	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677251#0.7790259333635017	1	2009-04-15	93	93	1	\N	1955	0	50	350	0	IZURDIA	Perdu le 6 février 1970 en face d'Abidjan.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677257#0.5616965862257495	1	2009-04-15	94	94	1	\N	1958	26.299999	78	420	0	JASMIN	Arrive a DAKAR 1979	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677263#0.19809368782828507	1	2009-04-15	95	95	1	\N	1958	26	78	400	0	KERAVEN	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677268#0.6591983555258683	1	2009-04-15	96	96	1	\N	1960	25	72	400	0	KERILLIS	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677274#0.624304650587529	1	2009-04-15	97	97	1	\N	1959	25	60	400	0	KER TREGUIER	=TUTTI /112209/PUIS N°402	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677279#0.6612101958753589	1	2009-04-15	98	98	1	\N	1956	22	30	300	0	KILUDY	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677284#0.6260101791122434	1	2009-04-15	99	99	1	\N	0	0	0	0	0	LA BALLERINE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677289#0.0010097279761637212	1	2009-04-15	100	100	1	\N	0	0	0	0	0	LA HOULE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677299#0.13526200434222957	1	2009-04-15	102	102	1	\N	1957	20	20	200	0	LE BASQUE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677368#0.19216263752802398	1	2009-04-15	118	118	1	\N	1956	22	50	300	0	ROBERT MICHEL 3	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677371#0.4399101639186177	1	2009-04-15	119	119	1	\N	0	0	0	0	0	ROLAND ISABELLE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677375#0.4795167849538735	1	2009-04-15	120	120	1	\N	1956	24	50	350	0	SARDARA	Premier thonier à rester toute l'année à Dakar - bateau coulé le 25/06/1998 dans le cadre du plan Mellick, au large de Gorée	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677378#0.6744795379908071	1	2009-04-15	121	121	1	\N	1956	22	50	300	0	SI TOUS LES GARS	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677382#0.7654929059545601	1	2009-04-15	122	122	1	\N	1956	24.5	50	380	0	SOCORRI	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677386#0.8485914907703843	1	2009-04-15	123	123	1	\N	1957	22	35	300	0	TCHIKITTIN	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677389#0.8285604235159074	1	2009-04-15	124	124	1	\N	1955	24	50	350	0	TUTINA	En 1964, effectua une campagne de prospection à Djibouti. Devenu MAWDO 2	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677392#0.07404630696545034	1	2009-04-15	125	125	1	\N	1957	22.799999	55	300	0	VENUS	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677405#0.6973415021689955	1	2009-04-15	126	126	1	\N	1956	22	50	300	0	RONCEVEAU 3	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677409#0.1314469119893883	1	2009-04-15	127	127	99	\N	0	0	0	0	0	JACQUELINE A	Cap systeme americain 780 T	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677417#0.6646136848141067	1	2009-04-15	128	128	1	\N	1969	53.950001	1015	2400	0	BISCAYA	Ex OLMECA 1 (Mexicain) Cuves mixtes GO et Poisson de 244 M3 ( 216+244)	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677424#0.0036758590335427277	1	2009-04-15	129	129	3	\N	1970	47	510	1800	0	ILE BOULAY	Sister-ship du Richelieu, Jacques Cœur et Henri Polo	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677430#0.7672411607133688	1	2009-04-15	130	130	1	\N	1957	22	30	300	0	AIDE TOI	Ex MAWDO /211246/	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677438#0.3542972328985696	1	2009-04-15	132	6	1	\N	1965	41.799999	380	950	0	RAIS	Ex NAVARRA	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9860530760211061	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677499#0.14390321352800028	1	2009-04-15	149	148	4	\N	1969	49.700001	700	1600	0	ALBACORA 1	IXTAS ZIERBANA PUIS MONTECLARO	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677504#0.9170061261851666	1	2009-04-15	150	149	4	\N	1970	0	700	1600	0	JUAN DE AKURIO	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677507#0.885086199679135	1	2009-04-15	151	150	4	\N	1972	49.509998	600	2000	0	PLAYA DE LEKEITIO	sardinier depuis avr 86	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677511#0.1833546256400137	1	2009-04-15	152	151	4	\N	1971	58.799999	1100	2300	0	BETI ALAI	coule 86	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677515#0.9568673875388758	1	2009-04-15	153	152	4	\N	1971	47.5	700	1240	0	ALCARAVAN	TXIRRINE en 87	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677519#0.1723998707327845	1	2009-04-15	154	153	4	\N	1970	47.07	750	1840	0	BERMEOTARAK	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677523#0.22989839039434334	1	2009-04-15	157	154	4	\N	1972	58.400002	1250	2670	0	ALBACORA 3	Devenu ITXAS GANE	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677527#0.8803899194087401	1	2009-04-15	158	155	2	\N	1971	36.810001	0	950	0	SIRIMANA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677531#0.0801917005254994	1	2009-04-15	159	156	4	\N	1971	54.619999	850	2000	0	ALBACORA 2	Devenu MONTENEME EN 87 466406	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677535#0.5726656261645778	1	2009-04-15	166	157	2	\N	1970	36.75	0	950	0	GANDIOLE	Devenu XURIBELTZ 124446 en 89	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677540#0.3449592640739847	1	2009-04-15	168	158	2	\N	1971	36.810001	0	950	0	LEONA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677543#0.34144533395126964	1	2009-04-15	170	159	1	\N	1972	75.5	2250	4000	0	ALABA	Devient BONSA (353), arret 81	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832677547#0.8640076438544488	1	2009-04-15	171	160	1	\N	0	0	0	0	0	AITA RAMUNCHO	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677551#0.6406498194197155	1	2009-04-15	172	161	1	1981-02-01 00:00:00	1972	56.82	865	2800	0	MORGAT	Devenu OLMECA 2 81	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677555#0.051503641023914026	1	2009-04-15	173	162	4	\N	0	0	170	1275	0	BENITO PURTUONDO	Arrete 81	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677564#0.37311933616721116	1	2009-04-15	175	164	4	\N	1972	62.740002	0	3600	0	COSTA DE MARFIL	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677639#0.7173948656394551	1	2009-04-15	190	9	1	\N	1958	36.25	210	750	0	BALBAYA	Voir VENDOME	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677643#0.12420411674196663	1	2009-04-15	191	179	1	\N	1959	27.5	0	450	0	LOUIS-BERNARD	Arrete fin 60	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677647#0.4997701487731613	1	2009-04-15	192	180	1	\N	0	0	0	0	0	ANTOROS	Arrete fin 61	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677650#0.4015145447867484	1	2009-04-15	193	181	1	\N	1972	39.299999	220	1050	0	CAP SAINT PIERRE	Devenu canneur ( ETOILE ESPERANCE)	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677654#0.6000934232661312	1	2009-04-15	194	182	1	\N	1972	41.080002	0	1300	0	TRENOVA	Vendu aux Phillipines	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677658#0.9246599784789458	1	2009-04-15	195	183	1	\N	1973	62.740002	980	3650	0	CHRISTOPHE COLOMB	12/91 devenu 504 st vincent	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677662#0.7917780179019475	1	2009-04-15	196	184	3	\N	1970	40	230	1050	0	TRIDENT	Vendu en Afrique du Sud	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677669#0.8978333060784457	1	2009-04-15	197	185	2	\N	0	0	0	0	0	YENE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677673#0.941061272622935	1	2009-04-15	198	186	2	\N	0	0	0	0	0	DJIRNDA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677677#0.3726972807565633	1	2009-04-15	199	187	2	\N	0	0	0	0	0	CAP DE NAZE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677685#0.12229560167812337	1	2009-04-15	200	188	2	\N	0	0	0	0	0	GOUDOMP	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677689#0.32549915961841813	1	2009-04-15	201	189	2	\N	1973	34.599998	0	810	0	ILE AUX SERPENTS	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677693#0.8766470712661654	1	2009-04-15	202	190	2	\N	0	0	0	0	0	LES MAMELLES	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677697#0.6333901365900212	1	2009-04-15	203	191	2	\N	1973	34.599998	0	810	0	SALLY	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677705#0.337940022913236	1	2009-04-15	205	193	2	\N	1970	34.599998	0	810	0	NIODOR (2)	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677785#0.37130373852142096	1	2009-04-15	221	103	1	\N	1955	24	50	350	0	CURLINKA	Premier thonier français à faire un coup de senne en Afrique en avril 1961 ( Sierra Leone )	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677789#0.6852663022196283	1	2009-04-15	222	206	4	\N	1974	0	800	2200	0	ITXAS EDER	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677793#0.18667674886331165	1	2009-04-15	223	207	4	\N	1974	0	600	1800	0	PLAYA DE LAIDA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677800#0.7301842666371928	1	2009-04-15	224	208	4	\N	0	0	0	0	0	ATALDE	arrete	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677818#0.8965634485687902	1	2009-04-15	225	209	1	\N	1974	64.669998	1575	4000	0	PERE BRIANT	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677823#0.292478536793217	1	2009-04-15	226	210	99	\N	0	0	0	0	0	EQUATEUR	?	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.18032080790817928	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677828#0.6196669948869438	1	2009-04-15	227	211	1	\N	0	0	0	0	0	BELLE GUEUSE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677834#0.25005659070252295	1	2009-04-15	228	212	1	\N	1974	56.82	850	3000	0	ILE AUX MOINES	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677840#0.22254206614702376	1	2009-04-15	229	213	3	\N	1974	51	675	2200	0	LAURENT	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677846#0.14568950609362608	1	2009-04-15	230	214	99	\N	1973	59.970001	0	3600	0	SANDRA C	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677851#0.37370686794596875	1	2009-04-15	231	215	1	\N	1974	51	675	2200	0	MERVENT	Sister-ship du Bélier, Gevred, etc..	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677897#0.674767631824804	1	2009-04-15	232	216	1	\N	1975	51	675	2200	0	CAP BOJADOR	Devenu EBONY LADY	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677902#0.7384329048025571	1	2009-04-15	233	217	3	\N	1974	51	675	2200	0	BELIER	Devenu BELUGA 165433	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677908#0.048049910515759575	1	2009-04-15	234	218	3	\N	1975	51	675	2200	0	N'ZIDA	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677925#0.8235567137559866	1	2009-04-15	236	49	1	\N	1969	47	510	1800	0	TARFAYA	Ex RICHELIEU Desarme 79	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9860530760211061	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678007#0.13396112380502956	1	2009-04-15	251	230	3	\N	1976	54.009998	675	2400	0	NEPTUNE	1976 devenu 165	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678013#0.43358446195117795	1	2009-04-15	252	231	1	\N	1976	63.330002	1100	3000	0	MARLIN	ex TXORI GORRI	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9860530760211061	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678018#0.5876065843296562	1	2009-04-15	253	232	1	\N	1977	78.800003	2272	6000	0	LOANGO	Congolais 77	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.43427027828177345	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678023#0.4504434541071176	1	2009-04-15	254	233	2	\N	1975	0	0	0	0	DIOFONDOR	Senegalais 76	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678029#0.10876730848924476	1	2009-04-15	255	234	2	\N	1975	55.169998	1000	2850	0	JILOR	Senegalais 76	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678034#0.22118888460619457	1	2009-04-15	256	235	2	\N	1975	55.169998	1000	2850	0	NIOMBRE	Senegalais 76	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678040#0.32511146030815075	1	2009-04-15	257	236	1	1991-11-01 00:00:00	1978	62.740002	980	3652	0	JACQUES CARTIER	nov 91 devenu 512 st vincent	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678045#0.23052630053566903	1	2009-04-15	258	237	1	\N	1978	62.77	996	3650	0	PRINCE DE JOINVILLE	Entrée en service en  78	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678051#0.10411950439324624	1	2009-04-15	259	238	1	\N	1978	62.77	980	3650	0	L. A. 'BOUGAINVILLE	Français 78 " Louis Albert de Bougainville"	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678056#0.11165157653870728	1	2009-04-15	260	165	3	\N	1973	64.599998	1500	3000	0	HARMATTAN	Devenu VIA HARMATTAN 166401	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678062#0.8414312018841227	1	2009-04-15	261	239	4	\N	1975	56.110001	750	2200	0	KURTXIO	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678067#0.40146146898627966	1	2009-04-15	262	240	4	\N	1977	0	1450	4400	0	GALDRUMEIRO	Devenu EUSKADI ALAI	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678073#0.7048486632778159	1	2009-04-15	263	241	4	\N	1976	63.330002	1050	3000	0	TXORI EDER	Esp 76	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678078#0.5880906953659392	1	2009-04-15	264	242	4	\N	1976	63.330002	1050	3000	0	TXORI URDIN	Esp 76	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678089#0.30897417058281595	1	2009-04-15	266	244	4	\N	1975	0	925	300	0	ALMADRABA 1	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678170#0.6500880195694608	1	2009-04-15	281	258	4	\N	0	0	0	0	0	ALBACORA 8	Canneur	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678175#0.8415549805915425	1	2009-04-15	282	259	4	\N	1976	76.760002	1400	4000	0	ALBACORA 9	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678180#0.938777253231857	1	2009-04-15	283	260	4	\N	1977	76.760002	1400	4000	0	ALBACORA 10	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678186#0.49679780190228473	1	2009-04-15	284	9	1	\N	1958	36.25	210	750	0	PIERRETTE	Ex VENDOME puis coule	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832678192#0.200243620823658	1	2009-04-15	285	261	1	\N	1964	24.9	0	0	0	HENDAYAKO IZARRA	janv.-79	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832678197#0.4207148979488987	1	2009-04-15	286	20	1	1979-01-01 00:00:00	1952	32	380	630	0	IRRINTZINA	Ex 154020 depuis janvier 79	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832678203#0.6158170797470678	1	2009-04-15	287	262	4	\N	1975	0	1200	4000	0	NARANCO	Devenu KAI ALAI	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678208#0.7260327377325902	1	2009-04-15	288	263	4	\N	1979	61.27	1100	3000	0	PLAYA DE NOJA	Arrêté fin 2001. Dernière sortie d'Abidjan le 28/11/01. 	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678213#0.5638701081487253	1	2009-04-15	289	264	4	\N	1979	69	1262	4400	0	ITXAS CANTABRICO	Coule aout 83	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678218#0.06930017233408858	1	2009-04-15	290	265	4	\N	1978	0	1090	4000	0	ITXAS ESTE	Abidjan aout 90 apres 1 an Espagne	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678223#0.09090233058087671	1	2009-04-15	291	266	4	\N	1975	53.040001	650	2500	0	BERMEOTARAK DOS	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678227#0.9334602164889675	1	2009-04-15	292	240	4	\N	1977	0	1450	4400	0	EUSKADI ALAI	Ex GALDUMEIRO 467262	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678232#0.3543897398427308	1	2009-04-15	293	267	4	\N	1978	76.75	1450	4400	0	HALADEIRO	Retour O Indien 11/88	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678238#0.6312996251961113	1	2009-04-15	294	268	3	\N	1980	69	1250	3900	0	BAYOTA	Lancé en 1979, devenu 166430	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678248#0.178209009653285	1	2009-04-15	296	270	1	\N	1980	69	1250	4400	0	TREVIGNON 2	Lance 80	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678340#0.5972978786082259	1	2009-04-15	312	161	1	\N	1972	56.82	865	2800	0	OLMECA 2	Ex MORGAT devenu MEXICAIN	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678345#0.8624970174764721	1	2009-04-15	313	284	14	\N	1973	66.5	1250	3600	0	GOLD COAST	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678351#0.46281825717510405	1	2009-04-15	314	285	14	\N	1980	55	950	3600	0	CAPT STENDHAL	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678356#0.7273009550399987	1	2009-04-15	315	286	14	\N	1980	55	950	3600	0	DONNA H	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678361#0.6795532085053385	1	2009-04-15	316	287	7	\N	1982	56.009998	1000	2500	0	WAKASHIO MARU 1	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678367#0.6686915061847586	1	2009-04-15	317	288	4	\N	1980	73.889999	1700	4399	0	ENTREMARES 1	Devenu ALACRAN 467348	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678372#0.3201419551131226	1	2009-04-15	318	289	4	\N	1982	77.32	1850	4400	0	ALBACORA 12	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678377#0.30000771000054527	1	2009-04-15	319	290	14	\N	1981	55	950	3600	0	MARIA ROSINA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678383#0.12753082790557912	1	2009-04-15	320	51	2	\N	1969	47	510	1800	0	TOUBAB DIALAW	Ex H POLO devenu VIA FOEHN	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678388#0.7834543167330018	1	2009-04-15	321	291	4	\N	0	0	0	0	0	TUNAMAR	Devenu VENEZUELA	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.411796820475434	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678393#0.7162935660932679	1	2009-04-15	322	292	14	\N	1981	55	950	3600	0	WANSIMA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678398#0.018749509971470846	1	2009-04-15	323	293	14	\N	1981	55	950	3600	0	PIONEER 2	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678404#0.6603687821975046	1	2009-04-15	324	294	1	\N	1982	55.43	800	2800	0	SANTA MARIA	Même caractéristiques que le Cap St Paul 2, l'Armen et l'Avel Viz.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678410#0.35210937407886955	1	2009-04-15	325	295	1	\N	1982	70.18	1380	3600	0	DRENNEC	Vendu en Equateur 05/97	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678425#0.5207600352288683	1	2009-04-15	328	298	1	\N	1983	55.450001	800	2800	11	ARMEN	Sorti de flotte durant le dernier semestre 2004 ( Plan EU: Récupération de KW )	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678504#0.5081537670341737	1	2009-04-15	342	309	99	\N	0	0	0	0	0	TUNAORO 2	?	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678510#0.5973047169264356	1	2009-04-15	343	310	99	\N	0	0	0	0	0	TUNAORO 3	?	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678515#0.19461873091068116	1	2009-04-15	344	311	99	\N	0	0	0	0	0	TUNAORO 4	?	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678521#0.742846449515561	1	2009-04-15	345	312	1	\N	1984	55.400002	813	2400	0	AVEL VIZ	Même caractéristiques que Santa Maria, l'Armen et le Cap Saint Paul 2.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678526#0.7174677777453458	1	2009-04-15	346	313	4	\N	1983	85.849998	1850	4350	0	ALBACORA 15	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678532#0.2527030973600062	1	2009-04-15	347	314	4	\N	1983	77.32	1850	4400	0	ALBACORA 16	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678538#0.9011088407695924	1	2009-04-15	348	288	4	\N	1980	73.889999	1700	4400	0	ALACRAN	Ex ENTREMARES 1 467317	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678544#0.2847823951579975	1	2009-04-15	349	282	4	\N	1980	73.889999	1700	4400	0	TXORI ZURI	Ex ENTREMARES 2 467309	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678549#0.10019150140969924	1	2009-04-15	350	148	4	\N	1969	49.700001	700	1600	0	ITXAS ZIERBANA	Ex ALBACORA 1 dev MONTECLARO	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678555#0.7893065061827271	1	2009-04-15	351	315	4	\N	1984	78.010002	1450	4350	0	TXORI AUNDI	Senneur panameen arme par les espagnols	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678561#0.6773220959688093	1	2009-04-15	352	133	2	\N	1969	47.5	550	1800	0	AIDA	Ex PT LACOUR	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678566#0.07045269445448721	1	2009-04-15	353	159	14	\N	1972	75.5	2250	4000	0	BONSA	ex ALABA (170), devient DRAGO (642)	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678578#0.37743949941679356	1	2009-04-15	355	318	4	\N	1984	77.300003	1450	4350	0	JUAN RAMON EGANA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678672#0.9622185101507931	1	2009-04-15	371	331	14	\N	1972	46.110001	0	1000	0	FERNANDA MARISA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678677#0.35245741159157695	1	2009-04-15	372	332	14	\N	1975	55.450001	0	1600	0	GBESE 6	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678683#0.5859621056201679	1	2009-04-15	373	333	14	\N	1975	55.450001	0	1600	0	GBESE 8	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678688#0.9796057405659001	1	2009-04-15	374	334	14	\N	1975	55.450001	0	1600	0	GBESE 9	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678693#0.7069240711912386	1	2009-04-15	375	335	14	\N	1975	55.450001	0	1600	0	GHAKO 101	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678703#0.4903951028079754	1	2009-04-15	376	336	14	\N	0	0	0	0	0	KAAS 102	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678709#0.9748919024253353	1	2009-04-15	377	337	14	\N	1974	57.43	0	1800	0	KAAS 105	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678715#0.032904894153117925	1	2009-04-15	378	338	14	\N	1966	34.509998	0	750	0	KAAS 101	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678721#0.5084080759439501	1	2009-04-15	379	339	14	\N	1972	46.110001	0	1000	0	KWAMINA NORTEY	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678727#0.3561631176214608	1	2009-04-15	380	340	14	\N	0	0	0	0	0	KAAS 103	GOSHEN 601 en 87	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678733#0.3885242817182418	1	2009-04-15	381	341	14	\N	1974	57.43	0	1800	0	KAAS 107	AFKO 313 en 87	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678739#0.10086876563501113	1	2009-04-15	382	342	14	\N	1971	40.619999	0	1000	0	MARY RADINE	Ex KURUSHIO MARU 61	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678744#0.16641174975522188	1	2009-04-15	383	343	14	\N	1972	0	0	1000	0	NICK T	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678749#0.4851273210807606	1	2009-04-15	384	344	14	\N	0	0	0	0	0	NII ANUMLA	Ex KATSUSHIO MARU 205	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678755#0.29949774992961997	1	2009-04-15	385	345	14	\N	0	0	0	0	0	LK TUNO 201	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678766#0.2829905660294799	1	2009-04-15	387	347	14	\N	1975	55.450001	0	1600	0	GBESE 7	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678857#0.5406165300551693	1	2009-04-15	403	271	1	\N	1980	69.019997	1250	3455	0	VIA SIMOUN	Ex BEOUMI 366297 DEPUIS 86	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678862#0.8634723724649198	1	2009-04-15	404	360	4	\N	1983	73	1350	3600	0	GAZTELUGATXE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678868#0.38657421589769625	1	2009-04-15	405	148	4	\N	1969	49.700001	700	1600	0	MONTECLARO	Ex ITXAS ZIERBANA 465350	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678874#0.006016109912323153	1	2009-04-15	406	156	4	\N	1971	54.619999	850	2000	0	MONTENEME	Ex ALBACORA 2	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678880#0.17215929706708544	1	2009-04-15	407	340	14	\N	0	0	0	0	0	GOSHEN 601	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678886#0.7953127751924652	1	2009-04-15	408	355	14	\N	1966	47.5	0	850	0	RABBI	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678891#0.004798184428181584	1	2009-04-15	409	362	14	\N	0	0	0	0	0	KATSUSHIO MARU 301	Opere en 84	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678898#0.6570762164821973	1	2009-04-15	410	363	14	\N	0	0	0	0	0	SHEISHO MARU 30	Opere en 84	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678903#0.24078902807619407	1	2009-04-15	411	364	14	\N	1973	49.099998	0	1300	0	KATSUSHIO MARU 206	Opere en 84	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678909#0.8478624975899507	1	2009-04-15	412	365	14	\N	1972	0	0	2000	0	CROTO	Canneur PFCI	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678915#0.8339212560546295	1	2009-04-15	413	366	14	\N	1961	52.580002	0	1100	0	AFKO 105	Devenu transporteur  en 2000	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678920#0.46185017654540084	1	2009-04-15	414	367	14	\N	1972	57.459999	0	1800	0	TULIPAN	Canneur PFCI	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678926#0.6376980749974057	1	2009-04-15	415	368	14	\N	0	0	0	0	0	GHAKO 301	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678932#0.2580069194922876	1	2009-04-15	416	152	4	\N	1971	47.5	700	1650	0	TXIRRINE	Ex ALCARAVAN 465153 EN 87	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678938#0.25533635061354965	1	2009-04-15	417	23	1	\N	1970	38.380001	220	1000	0	MASCAROI	Ex RIO AGUEDA 1954273 devient ERNAI	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678950#0.3028560318274538	1	2009-04-15	419	369	4	\N	1978	0	0	800	0	ITXAS ALDE	Canneur espagnol - JB 142.25 Tx  Immatriculation BI-2-2739	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832679066#0.5047642020583095	1	2009-04-15	435	382	99	\N	0	0	0	0	0	INDUS 1	Ex senneur US PATRICIAN	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.4407649460417081	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679073#0.8815438926158871	1	2009-04-15	436	383	7	\N	1986	78.57	1750	2000	0	NIPPON MARU	Navire de recherches japonais	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679083#0.02698677911401348	1	2009-04-15	437	384	4	\N	1988	54.400002	800	2000	0	ALBONIGA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679089#0.3641987071518997	1	2009-04-15	438	385	4	\N	1964	0	0	316	0	BRISAS DE TORRONTERO	Canneur espagnol 1988  JB 114.33 Tx   Immatriculation BI-2-2430	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832679096#0.17059069107951574	1	2009-04-15	439	386	99	\N	1985	0	0	299	0	MUNCRECA	Canneur cap verdien 1988	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832679102#0.07965288253498892	1	2009-04-15	440	387	99	\N	0	0	0	0	0	SUL DE MAIO	Canneur cap verdien 1988	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832679109#0.1007096078260794	1	2009-04-15	441	51	1	\N	1969	47	510	1800	0	VIA FOEHN	Ex Toubab Dialo puis Duc de Praslin	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679116#0.4133563030949192	1	2009-04-15	442	388	14	\N	0	0	0	0	0	PAULA H	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679123#0.7118631694187496	1	2009-04-15	443	389	4	1988-11-01 00:00:00	1988	62	850	3300	0	BERMEOTARRAK TRES	Mise en service 11/88	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679131#0.3708092473162218	1	2009-04-15	444	154	4	\N	1972	58.400002	1250	3000	0	ITXAS GANE	Ex ALBACORA 3	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679137#0.7047938844895926	1	2009-04-15	445	181	1	\N	1972	39.299999	220	1050	0	ETOILE D'ESPERANCE	Ancien CAP ST PIERRE 154193	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679144#0.7836161241489806	1	2009-04-15	446	157	1	\N	1970	36.75	0	950	0	XURI BELTZ	Ancien GANDIOLE 234166 DK	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679151#0.4521616722069872	1	2009-04-15	447	390	14	\N	1975	59.869999	0	3550	0	FRANCOISE L	Ex JEANNE LOU construit en 75	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.10207212372204011	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679157#0.28961874704945434	1	2009-04-15	448	391	2	\N	1963	30	0	575	0	HORTENSIA	Ancien chalutier	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832679171#0.14481723146906633	1	2009-04-15	450	393	4	\N	0	0	0	0	0	GUADIANA	Septembre89 premier debarquement a Abidjan	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679280#0.5825448717300783	1	2009-04-15	465	262	4	\N	1975	0	1200	4000	0	KAI ALAI	Ancien NARANCO  maltais 90 ?	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.3241600448153684	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679288#0.8180139890743064	1	2009-04-15	466	405	20	\N	0	0	0	0	0	UVAROVSK	Ocean indien 	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679295#0.880883704932655	1	2009-04-15	467	406	20	\N	0	0	0	0	0	UZHGORSK	Ocean indien	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679308#0.0075431578722992	1	2009-04-15	468	407	7	\N	1977	55.400002	700	2500	0	FUKUICHI MARU 63	Atlantique	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679316#0.29759286827455445	1	2009-04-15	469	408	7	\N	0	0	950	0	0	HAYABUSA MARU	Ocean indien	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679323#0.6704334185862101	1	2009-04-15	470	409	7	\N	1990	63.240002	750	2700	0	TOMI MARU 61	Ocean indien	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679330#0.05694168384354503	1	2009-04-15	471	410	14	\N	0	0	0	0	0	NOVA TUNA I	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679337#0.4000499491496665	1	2009-04-15	472	378	14	\N	1972	54.400002	0	1800	0	ABETO	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679344#0.6623203349263505	1	2009-04-15	473	412	4	\N	1990	77.300003	1700	7944	0	INTERTUNA DOS	Abidjan juin 90	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679351#0.7054305630193587	1	2009-04-15	474	6	1	\N	1965	41.799999	380	950	0	NAVARRA	Ancien senneur	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679358#0.7119870600761139	1	2009-04-15	475	413	14	\N	0	0	0	0	0	MARINE 701	Tema decembre 89	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679365#0.9440692882942426	1	2009-04-15	476	283	4	\N	1980	77.32	1750	4399	0	INTERTUNA UNO	Ancien Maratun	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679372#0.2805993994080752	1	2009-04-15	477	23	1	\N	1970	38.380001	220	1000	0	ERNAI	Ancien senneur MASCAROI ( bateau du PTR ) juillet 90	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679380#0.03858376942621711	1	2009-04-15	478	414	20	\N	0	0	0	0	0	TORKHUS	Signale Ocean Indien en septembre 90	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679403#0.6994759054216589	1	2009-04-15	480	416	20	\N	0	0	0	0	0	GORIAHEGORSK	Signale Ocean Indien en septembre 90	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679527#0.08582329704456626	1	2009-04-15	496	430	7	\N	1981	0	950	2600	0	TOKIWA MARU 1	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679533#0.7445895366259427	1	2009-04-15	497	431	7	\N	1978	57.099998	950	2500	0	TAIKEI MARU 51	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679540#0.4016791382199575	1	2009-04-15	498	432	7	\N	1981	0	1000	2000	0	SHOYU MARU 38	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679547#0.17837058652964433	1	2009-04-15	499	433	7	\N	1981	0	950	2600	0	KOYO MARU 75	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679577#0.499373287450259	1	2009-04-15	500	434	7	\N	1981	0	1000	2500	0	KOYO MARU 88	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679584#0.9791000349769647	1	2009-04-15	501	435	7	\N	1981	56.27	700	2600	0	GENKUPU MARU 86	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679590#0.7290991239143804	1	2009-04-15	502	436	1	\N	1991	77.879997	1650	4200	16	VIA MISTRAL	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679597#0.46952407958581077	1	2009-04-15	503	437	14	\N	1967	0	0	0	0	YOUNG BOK	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679603#0.5762495864572836	1	2009-04-15	504	183	1	1991-12-01 00:00:00	1973	62.740002	980	3650	0	CHRISTOPHE COLOMB	Arret 93 revient octobre 95	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679610#0.7804446680880538	1	2009-04-15	505	438	14	\N	0	0	0	0	0	PINO	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679617#0.7721780279513504	1	2009-04-15	506	439	20	\N	1992	79.800003	1951	4957	0	KAOURI	Sister ship Pinna	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679623#0.8225520084958498	1	2009-04-15	507	440	4	\N	1991	75.599998	1700	3944	0	PLAYA DE BAKIO	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679630#0.6941800175344158	1	2009-04-15	508	441	4	\N	1991	82.400002	1450	4690	0	AGUR ZUBEROA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679637#0.252252176034105	1	2009-04-15	509	281	14	\N	1980	77	1850	4400	0	PONTESA PRIMERO	Ex Albacora 11	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679643#0.7322399173804504	1	2009-04-15	510	442	7	\N	1979	55.66	500	0	0	FUKUICHI MARU 83	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679660#0.9222045967585316	1	2009-04-15	512	236	1	\N	1978	62.740002	980	3652	0	JACQUES CARTIER	Ex 544	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679776#0.4762440741924444	1	2009-04-15	527	443	1	\N	1991	77.800003	1650	4200	0	RIO MARE	Premiere maree en pavillon italien	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.1799231640486968	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679785#0.5506564829972519	1	2009-04-15	528	462	20	\N	1993	79.800003	1851	4950	0	RODIOS	Sister ship PINNA 2	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679793#0.2607537025943826	1	2009-04-15	529	287	14	1993-05-01 00:00:00	1982	56.009998	1000	2500	0	CHOPES	Ex wakashiu maru 316 mai 93	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.6435649696754744	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679801#0.5437987634908192	1	2009-04-15	530	456	4	\N	1973	80	1750	3949	0	ELAI ALAI	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679809#0.5899890810672939	1	2009-04-15	531	457	14	\N	1974	62.869999	0	2100	0	MARINE 703	Canneur tema juillet 93	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679816#0.7924773436762587	1	2009-04-15	532	437	14	\N	1967	0	0	0	0	YOUNG BOK	Ancien senneur young bok ( Afko 106 ? )	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.4516205269922994	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679824#0.9958506399700275	1	2009-04-15	533	287	14	\N	1982	56.009998	1000	2500	0	PONTESA SEGUNDO	Ex chopes ex wakashiu	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.10207212372204011	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679832#0.13130629640608715	1	2009-04-15	534	196	14	\N	1972	75.5	2250	4000	0	MARINE KIM	Numero non changé au chang nom ( Ghana ) ex Cap Sizun N° 210	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.10207212372204011	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679840#0.6866834186012969	1	2009-04-15	535	288	4	\N	1980	73.889999	1700	4399	0	ALACRAN	changement pavillon ex 348	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.6435649696754744	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679848#0.11786635788673772	1	2009-04-15	536	157	1	\N	1970	36.75	0	950	0	ERIKA	Ex XURI BELTZ n°446	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679856#0.046587403098315106	1	2009-04-15	539	458	14	\N	1974	54.5	0	2100	0	AFKO FOODS 801	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679864#0.19255420248760413	1	2009-04-15	540	459	14	\N	1974	54.5	0	2200	0	AFKO FOODS 802	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679876#0.928679300280521	1	2009-04-15	541	460	1	\N	1967	33.25	0	1000	0	VIA ZEPHYR	Supply Saupiquet	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832679884#0.18815811705133112	1	2009-04-15	542	461	1	\N	1965	33.25	0	770	0	AMARILLYS	Supply Saupiquet	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832679900#0.31427384911021994	1	2009-04-15	544	236	1	\N	1978	62.740002	980	3652	0	JACQUES CARTIER	Devient liberien en septembre 94	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680030#0.9917846938199594	1	2009-04-15	560	467	20	\N	1992	79.800003	1200	4957	0	GOMER	Signale en Ocean indien aout 95	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.07349229119463152	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680037#0.18691170809448288	1	2009-04-15	561	468	14	\N	0	0	0	0	0	MARINE 705	Signale a Tema octobre 95	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680044#0.920685824668797	1	2009-04-15	562	469	14	\N	1977	0	0	2100	0	JOE B	Signale a Tema octobre 95	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680051#0.03754363635726932	1	2009-04-15	563	470	14	\N	1974	0	0	2000	0	GBESE 2	Signale a Tema octobre 95	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680058#0.3022931885709538	1	2009-04-15	564	471	14	\N	1974	57.48	0	2000	0	AFKO FOODS 803	Signale a Tema fevrier 95	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680065#0.4420674450442289	1	2009-04-15	565	472	4	\N	1968	0	0	425	0	WAKSMAN	Dakar en mai 96  JB 133.13 Tx  Immatriculation BI-2-2499	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680072#0.27034591330090185	1	2009-04-15	566	473	2	\N	0	0	0	0	0	LEONA	Dakar en mai 96	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680079#0.8191421427343974	1	2009-04-15	567	474	4	\N	0	0	0	0	0	ALMIKE	Dakar en aout 96	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680086#0.1035300538346885	1	2009-04-15	568	441	4	\N	1991	82.400002	1450	4690	0	ZUBEROA	Ancien Agur Zuberoa Ocean Indien aout en 96	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680093#0.23324429873186558	1	2009-04-15	569	475	4	\N	1996	105	2650	7999	0	ALBACORA 1	Ocean Indien en septembre 96	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680101#0.40114151339132487	1	2009-04-15	570	476	4	\N	1996	109.3	2600	7944	0	DONIENE	Ocean Indien en septembre 96	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680112#0.6125140372623353	1	2009-04-15	571	246	4	\N	1977	78.82	2272	6000	0	XIXILI	italien de mediterannee ( Ocean Indien  septembre 96 )	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680119#0.6404738137759839	1	2009-04-15	572	478	1	\N	1996	67.300003	1206	4079	13	AVEL VAD	neuf Ocean Indien en octobre 96	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680126#0.9819155648353692	1	2009-04-15	573	259	4	1996-05-09 00:00:00	1976	76.760002	1400	4000	0	ALBACORA 9	Ex panama changement le 9/5/96	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680134#0.016507367076809	1	2009-04-15	574	260	4	1996-04-01 00:00:00	1977	76.760002	1400	4000	0	ALBACORA 10	Ex panama (283) changement le 1/4/96	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680161#0.8397099489906245	1	2009-04-15	576	320	14	\N	1981	80.470001	0	6000	0	MARINE JO	Ex FIRAW (357), est en train de pourrir à Takoradi (Ghana)	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680270#0.3956292344528629	1	2009-04-15	591	451	1	1997-08-01 00:00:00	1992	79.800003	1951	4957	15	MEN CREN	Devient seychellois vers aout 97	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680294#0.5152440311650618	1	2009-04-15	592	466	1	1997-08-01 00:00:00	1992	79.800003	1951	4957	16	MEN GOE	Devient seychellois vers aout 97	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680302#0.3384500932768175	1	2009-04-15	593	447	1	1997-08-01 00:00:00	1992	79.800003	1951	4957	14	TALENDUIC	Devient seychellois vers aout 97	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680311#0.6369480433141761	1	2009-04-15	594	449	1	1997-08-01 00:00:00	1992	79.800003	1951	4957	0	CASTEL BRAZ	Devient seychellois vers aout 97Quitte La COBRECAF le 1/04/00 pour passer ss gérance GS Fisheries ( Américain). Dps dans l'OP ( apparemment St Vincent ). 	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680319#0.9067166062278091	1	2009-04-15	595	484	1	\N	1997	81.900002	1790	5017	15	TORRE GIULIA	Nouveau en aout 97 géré par COBRECAF	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.1799231640486968	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680327#0.5290165811886844	1	2009-04-15	596	455	4	\N	1991	79.800003	1851	4957	0	SANT YAGO 1	Repris par les espagnols  ex Dourveil	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680335#0.894693207472039	1	2009-04-15	597	462	4	\N	1993	79.800003	1851	4950	0	SANT YAGO 2	Repris par les espagnols  ex Brunec	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680342#0.28502512467159247	1	2009-04-15	598	485	4	\N	1997	0	0	500	0	GURE BALENZIAGA	Nouveau canneur espagnol a Dakar en octobre 97  JB 149 Tx  Immatriculation BI-2-2-97	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680349#0.12054739478862397	1	2009-04-15	599	245	4	\N	1977	78.82	2272	6121	0	ERROXAPE	?	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680357#0.6062428108042933	1	2009-04-15	600	487	1	\N	1998	67.300003	1206	4079	13	CAP SAINTE MARIE	nouveau fevrier 98 sister ship du 572	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680364#0.7369556260642733	1	2009-04-15	601	412	4	\N	1990	77.300003	1700	7944	0	INTERTUNA DOS	Ex 473	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680372#0.09570090650425	1	2009-04-15	602	395	20	\N	0	85	1050	5200	0	OCEAN STAR MARINE	Ex 452	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680387#0.8896936990015263	1	2009-04-15	604	428	20	\N	1993	79.800003	1951	4957	0	EXCELLENCE	Ex Pinna ( 494 )	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680505#0.014407905615871908	1	2009-04-15	619	446	20	\N	1992	79.800003	1951	4957	0	TS EMERALD	Ex TIVELA  517	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680512#0.4901217508214093	1	2009-04-15	620	374	20	\N	1979	85.099998	1250	5200	0	MARINE OCEAN	Ex BST RODINA  424	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680520#0.45361267129327965	1	2009-04-15	621	496	4	\N	0	0	0	0	0	PANAMA TUNA	LHT=116 m lancé le 16 avril 99	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680527#0.946293095553979	1	2009-04-15	622	289	4	\N	1982	77.32	1850	4400	0	ALBACORA 12	Changement pavillon ( Guatemala ) ex 318	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.3299006221926467	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680535#0.5929639202887148	1	2009-04-15	623	300	4	\N	1983	77.32	1850	4300	0	ALBACORA 14	Changement pavillon ( Guatemala ) ex 330	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.3299006221926467	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680543#0.39392072286602553	1	2009-04-15	624	497	4	\N	1964	0	0	600	0	NUEVO SAN LUIS	Novembre 99  JB 137.83 Tx  Immatriculation SS-2-1720	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680551#0.32479680131449085	1	2009-04-15	625	498	2	\N	1964	0	0	300	0	VIRGEN DE DORLETA	Décembre 99  JB 113.66 Tx  Immatriculation BI-1-2956	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680558#0.26174533601808003	1	2009-04-15	626	499	2	\N	1965	0	0	700	0	NUEVO MARIA ZULAICA	Décembre 99  JB 116.63 TX  Immatriculation BI-1-3121	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680566#0.872351171225615	1	2009-04-15	627	500	4	\N	2000	39.549999	250	800	0	INTREPIDO 2	mars-00	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680574#0.3121397614401962	1	2009-04-15	628	501	4	\N	2000	39.549999	250	800	0	INTREPIDO 3	mars-00	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680582#0.8919085132398891	1	2009-04-15	629	502	4	\N	2000	106.5	2900	7956	18.5	TXORI TOKI	Mars 2000 consommation 17 tonnes / jours en vitesse recherche. 21 tonnes au maxi. ( MP + Auxilliaires )	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680589#0.4276242961014819	1	2009-04-15	630	455	4	\N	1991	79.800003	1851	4957	0	SANT YAGO 1	changement pavillon ( Guatemala ) ex 596	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.3299006221926467	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680597#0.6752350261274329	1	2009-04-15	631	462	4	\N	1993	79.800003	1851	4950	0	SANT YAGO 2	changement pavillon ( Guatemala ) ex 597	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.3299006221926467	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680613#0.03493595876044442	1	2009-04-15	633	503	4	\N	1999	112.56	2966	6000	19	ARTZA	 2966 m3 cuves à poisson	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680730#0.7244042821641429	1	2009-04-15	648	513	1	\N	2000	67.300003	1200	4080	13	CAP SAINT VINCENT 2	Sorti de Concarneau en decembre 2000, immatriule en CC et au 15 avril 2001 immatricule a Mayotte.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680746#0.5200393422441332	1	2009-04-15	650	29	1	\N	1958	27.75	82	300	0	CAVALIER DES VAGUES	Canneur	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680754#0.7460948385767486	1	2009-04-15	651	131	1	\N	1961	20.700001	38	300	0	FRANCISCO MANUEL	Devient en 1969 EMIGRANT	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680762#0.02973344548965695	1	2009-04-15	652	511	1	\N	1994	40.450001	215	0	0	JEAN LOUIS RAPHAEL 2	A titre expérimental pour COBRECAF ( Construit à CC Ind Radio FGTX ) JB 278 Tx JN 83 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680770#0.3390859883926072	1	2009-04-15	653	514	99	\N	1956	24.83	65	200	0	VALDISOLE	Senneur avec plateau arrière tournant. Début en Adriatique. Après 2 marées racheté et transformé à Bordeaux	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.1799231640486968	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680777#0.8032287043348416	1	2009-04-15	654	514	1	\N	1956	24.83	65	360	0	ANGELITA	Devient en 61 le MAITECHU annexe du SOPITE	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680793#0.7917583793984888	1	2009-04-15	656	515	1	\N	1956	24.83	65	360	0	EDERRA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680785#0.32802057464559775	1	2009-04-15	655	515	99	\N	1956	24.83	65	200	0	VALCURVA	Senneur avec plateau arrière tournant. Début en Adriatique. Après 2 marées racheté et transformé à Bordeaux	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.1799231640486968	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680801#0.024405504707850634	1	2009-04-15	657	516	99	\N	1956	24.83	65	200	0	VALDANGUSTO	Senneur avec plateau arrière tournant. Début en Adriatique. Après 2 marées racheté et transformé à Bordeaux	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.1799231640486968	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680809#0.5929031428074923	1	2009-04-15	658	516	1	\N	1956	24.83	65	360	0	SACAILLA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680825#0.6685330585419094	1	2009-04-15	660	518	1	\N	1956	20	30	300	0	BROCUA	Construit en Espagne, faute de place dans les chantiers français. 	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680972#0.25363600398727837	1	2009-04-15	675	201	1	\N	1973	75.5	2250	4000	0	NAVARRA 2	Luz Armement	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680982#0.2518083985015963	1	2009-04-15	676	253	1	2000-12-19 00:00:00	1979	54.009998	675	2400	0	CAP SAINT PIERRE 2	Ex 276 Changement de pavillon ( Saint Vincent ) le 19/12/00	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680992#0.19976607591241602	1	2009-04-15	677	296	1	2000-12-19 00:00:00	1982	53.52	813	2400	0	CAP SAINT PAUL	Ex 326 Changement de pavillon ( Saint Vincent ) le 19/12/00	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832681002#0.7174296574268894	1	2009-04-15	678	533	20	\N	0	0	0	0	0	OCEAN LION	Origine à vérifier il s'agit surement d'un ancien russe de la série des BST.	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681012#0.09857022355026612	1	2009-04-15	679	466	1	2001-01-16 00:00:00	1992	79.800003	1742	4957	16	MEN GOE	Changement de pavillon au 16/01/2001 ex 559 DEMOSFEN.| 2003-2004 : Modification de la taille des cuves (condamnation de 2 cuves à l'avant du bateau), perte de 176 m3	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681021#0.6175443843889693	1	2009-04-15	680	246	4	2001-01-29 00:00:00	1977	78.82	2272	6000	0	XIXILI	Passé seychellois le 29 janvier 2001	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681029#0.35355696542657067	1	2009-04-15	681	288	4	2001-01-29 00:00:00	1980	73.889999	1700	4399	0	ALACRAN	Passé seychellois le 29 janvier 2001	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681038#0.7132293575813359	1	2009-04-15	682	245	4	2001-01-29 00:00:00	1977	78.82	2272	6121	0	ERROXAPE	Passé seychellois le 29 janvier 2001	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681047#0.9070169380810994	1	2009-04-15	683	275	4	2001-01-29 00:00:00	1977	75.5	2265	4000	0	IZARO	Passé seychellois le 29 janvier 2001	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681057#0.8500618360201643	1	2009-04-15	684	534	1	\N	1973	35.400002	0	0	0	EREBUS 2	Supply Saupiquet Coulé en 12/2001 ( incendie à bord )	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681075#0.4427316445870958	1	2009-04-15	686	257	4	\N	0	0	0	0	0	ALBACORA 7	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681202#0.7327068087369893	1	2009-04-15	701	513	1	2001-04-15 00:00:00	2000	67.300003	1200	4080	13	CAP SAINT VINCENT 2	Passé sous pavillon Mayotte le 15/04/2001	fr.ird.observe.entities.referentiel.Pays#1239832675597#0.708958027082267	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681210#0.9541767540797277	1	2009-04-15	702	536	1	2001-06-21 00:00:00	2001	67.300003	1200	4080	13	STERENN 2	Passé sous pavillon Mayotte le 21/06/2001	fr.ird.observe.entities.referentiel.Pays#1239832675597#0.708958027082267	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681218#0.8164733595244482	1	2009-04-15	703	536	1	2002-03-07 00:00:00	2001	67.300003	1200	4080	13	STERENN 2	Passé sous pavillon français le 07/03/2002	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681227#0.6861363522730599	1	2009-04-15	704	296	1	2001-06-01 00:00:00	1982	53.52	813	2400	0	CAP SAINT PAUL	Repasse sous pavillon français le 01/06/01	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832681235#0.07161770956447144	1	2009-04-15	705	253	1	2001-06-01 00:00:00	1979	54.009998	675	2400	0	CAP SAINT PIERRE 2	Repasse sous pavillon français le 01/06/01	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832681243#0.1651596245159752	1	2009-04-15	706	513	1	2002-03-07 00:00:00	2000	67.300003	1200	4080	13	CAP SAINT VINCENT 2	Passé sous pavillon français le 7/03/2002	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681252#0.4078865318637416	1	2009-04-15	707	451	1	2002-07-19 00:00:00	1992	79.800003	1742	4957	15	MEN CREN	Passe sous pavillon français le 19/07/2002 | 2003-2004 : Modification de la taille des cuves (condamnation de 2 cuves à l'avant du bateau), perte de 176 m3	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681260#0.7439688010145203	1	2009-04-15	708	538	4	\N	1971	27	0	500	0	ONDARTZAPE	Arrivée signalée à Dakar en octobre 2002. JB 147.9 Tx  Immatriculation BI-2-2567	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832681268#0.492330893745478	1	2009-04-15	709	539	4	\N	2002	36	0	660	0	ORTUBE BERRIA	Arrivée signalée à Dakar en octobre 2002. JB 204.84 Tx  Immatriculation BI-2-6-01	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681276#0.10284362660603363	1	2009-04-15	710	540	4	\N	2002	44.5	0	0	0	PATUDO	Signalé en 11/02. Capacité de transport 200 à 220 tonnes. Janvier 2003, sortie de flotte ( refus de licence sénégalais en tant que supply du groupe Albacora).	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681301#0.6676141665235822	1	2009-04-15	712	541	20	\N	0	0	1250	0	0	TS PROSPERITY	Caractéristiques à vérifier.	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681422#0.8006481662975031	1	2009-04-15	726	217	14	2003-09-04 00:00:00	1974	50.849998	675	2200	0	BELOUGA	?	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.9573433532024453	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832681430#0.13434091886053334	1	2009-04-15	727	548	2	\N	1987	30.700001	85	650	0	PDT MAGATTE DIACK 2	JB 160 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681439#0.3447767446008019	1	2009-04-15	728	549	2	\N	1987	30.799999	85	650	0	CDT BIRAME THIAW	JB 160 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681447#0.441573423202428	1	2009-04-15	729	550	0	\N	0	26	60	0	0	PRAIA GRANDE	JB 120 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832681455#0.5461943147783409	1	2009-04-15	730	551	0	\N	0	26	60	0	0	PONTA CALHAU	JB 120 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832681464#0.47481346128837787	1	2009-04-15	731	552	0	\N	0	26	60	0	0	PONTA BICUDA	JB 120 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832681472#0.14262222362273913	1	2009-04-15	732	215	14	2003-10-25 00:00:00	1974	51	675	2200	0	MERVENT	?	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.9573433532024453	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832681481#0.008245554149998169	1	2009-04-15	733	146	4	2003-06-30 00:00:00	1968	41.41	480	960	0	GURE CAMPOLIBRE	Ex 147 Détruit le 09/04/2004.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832681491#0.4445332638479409	1	2009-04-15	734	275	31	2004-01-28 00:00:00	1977	75.5	2265	4000	0	IZARO	?	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681501#0.28931750760918407	1	2009-04-15	735	270	31	2001-04-27 00:00:00	1980	69	1250	4400	13	HAVOUR 1	Chgt le 27/04/01	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681511#0.719980507833151	1	2009-04-15	736	251	31	2001-04-27 00:00:00	1979	69	1250	4000	13	HAVOUR 2	Chgt le 27/04/01	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681521#0.8400021783857162	1	2009-04-15	737	417	4	2003-03-15 00:00:00	1990	77.300003	1800	4399	0	ALBACORA CARIBE	?	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681531#0.9611601602620186	1	2009-04-15	738	315	4	2004-02-01 00:00:00	1984	78.010002	1450	4350	0	TXORI AUNDI	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681549#0.9720695533246346	1	2009-04-15	740	256	4	2004-03-29 00:00:00	1976	76.760002	1300	4000	0	KOOSHA 2	?	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681686#0.6482609231834334	1	2009-04-15	756	259	4	2004-09-01 00:00:00	1976	76.760002	1400	4000	0	ALBACORA 9	Chgt de Pavillon :ex Antilles Hollandaises (ex 573)	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681695#0.8165685733380568	1	2009-04-15	757	260	4	2004-09-01 00:00:00	1977	76.760002	1400	4000	0	ALBACORA 10	Chgt de Pavillon :ex Antilles Hollandaises (ex 574)	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681704#0.8568577340413567	1	2009-04-15	758	417	4	2004-09-01 00:00:00	1990	77.300003	1800	4399	0	ALBACORA CARIBE	Chgt de Pavillon :ex Antilles Hollandaises (ex 737)	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681712#0.6150594082125697	1	2009-04-15	759	279	31	2005-08-09 00:00:00	1982	70.5	1250	4400	13	BALUCH	ex TRESCAO code bateau 306, vendu à l'Iran	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681721#0.14196981136380782	1	2009-04-15	760	560	1	2005-12-15 00:00:00	2005	84.099998	1527	5440	17.700001	GLENAN	JB 2319 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681731#0.001224711966661518	1	2009-04-15	761	453	4	2001-01-01 00:00:00	1991	0	0	710	0	SANTA GEMA QUINTO	Vendu en 2001 au Vénézuela JB 139.91 Tx  - Ex code 526	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.1719246898492357	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681741#0.20079479378589127	1	2009-04-15	762	561	4	\N	2006	104.3	2680	6159	0	ALAKRANA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681750#0.9830685633268439	1	2009-04-15	763	280	4	2006-03-28 00:00:00	1979	76.760002	1700	4400	0	GALERNA	Ex bateau 307 	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681760#0.906982617554075	1	2009-04-15	764	562	4	\N	2006	95.699997	2593.5	8158	18	DRACO	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681769#0.34096506024034345	1	2009-04-15	765	485	4	\N	1997	0	0	500	0	ALAKRANTXU	Nouveau canneur espagnol a Dakar en octobre 97  JB 149 Tx  Immatriculation BI-2-2-97, changement de nom début 2005 (ex 598 GURE BALENZIAGA)	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681787#0.4539499174353051	1	2009-04-15	767	563	1	\N	2006	84.099998	1527	5440	17.700001	TREVIGNON	JB 2319 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675597#0.708958027082267	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832675781#0.24723042133098205	1	2009-04-15	1	1	1	\N	1958	31.799999	140	620	0	AUSTRAL	Arrete   ( Sister-ship du Boréal et du Tropical )	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832675808#0.8268722529245126	1	2009-04-15	2	2	1	\N	1963	31.799999	140	620	0	BOREAL	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832675847#0.5409131119127172	1	2009-04-15	3	3	1	\N	1963	28.76	140	460	0	CABELLOU	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832675983#0.07573052107518208	1	2009-04-15	7	7	1	\N	1963	30.799999	142	600	0	ROCROI	Arrete fin 73	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676056#0.9519581223764562	1	2009-04-15	9	9	1	\N	1958	36.25	210	750	0	VENDOME	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676098#0.037541091155538275	1	2009-04-15	10	10	1	\N	1963	30	135	600	0	AR BREIZAD	Arrete fin mars 74	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676140#0.9226729455905118	1	2009-04-15	11	11	1	\N	1963	30	135	600	0	MEN MEUR	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676175#0.7961142042164074	1	2009-04-15	12	12	1	\N	0	0	0	0	0	MONTMARTRE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676210#0.4012307419678379	1	2009-04-15	13	13	1	\N	1963	30	135	600	0	POPEYE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676265#0.3613523073980671	1	2009-04-15	14	14	1	\N	1964	31	160	630	0	ROUZ	Arrete fin 73	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676316#0.29791489852601083	1	2009-04-15	15	15	2	\N	1969	35	174	850	0	SOMONE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676373#0.36569167379945144	1	2009-04-15	16	16	2	\N	1969	32.919998	174	850	0	SOUMBEDIOUME	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676416#0.11341633295248599	1	2009-04-15	17	17	2	\N	1969	32.919998	174	850	0	MARSASSOUM	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676449#0.6558855278179592	1	2009-04-15	18	18	2	\N	1969	35	174	850	0	TASSINERE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676509#0.401379703715674	1	2009-04-15	19	19	1	\N	0	32	0	0	0	ILE DES FAISANS	= IXILIK /153208/	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676535#0.21088147059513307	1	2009-04-15	20	20	1	1979-01-01 00:00:00	1952	32	380	630	0	IRRINTZINA	Janvier 79, EX /154020/	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676571#0.31599922331187624	1	2009-04-15	21	21	3	\N	1970	38.380001	219	1050	0	ENTENTE	Sardinier a Abidjan en mai 76	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676612#0.9362269056365975	1	2009-04-15	22	22	4	\N	0	0	0	0	0	ALGESIRAS	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676622#0.07574909764102045	1	2009-04-15	23	23	1	\N	1970	38.380001	220	1000	0	CAP SAINT PAUL	RIO AGUEDA Puis MASCAROI	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832676857#0.1907907307998412	1	2009-04-15	39	39	1	\N	1962	28.75	29	460	0	PORSGUIR	Arrete fin 73	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676868#0.9100121040477112	1	2009-04-15	41	41	1	\N	1958	28	80	450	0	SIMONE VALENTINE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832676878#0.25998121985823053	1	2009-04-15	42	42	1	\N	1964	31	0	550	0	SOUCHET	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676887#0.6592210583530472	1	2009-04-15	43	43	2	\N	1965	37	0	0	0	ABDUL AZIZ WANNE	Arrete fin 72	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676892#0.7534091164439819	1	2009-04-15	44	44	2	\N	0	0	0	0	0	DIOGUE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676897#0.3551699393697453	1	2009-04-15	45	45	2	\N	1965	30	134	600	0	DIONOUAR	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676902#0.014537077495371808	1	2009-04-15	46	46	2	\N	0	0	0	0	0	GUET N'DAR	Arrete fin 73	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676914#0.8844425225657034	1	2009-04-15	47	47	2	\N	1965	31.5	0	600	0	MAMMA N'GUEYE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832676941#0.3580667848836081	1	2009-04-15	48	48	1	\N	1970	47.5	500	1800	0	ILE DE SEIN	Sister-ship du Trévignon	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676945#0.37372717495320373	1	2009-04-15	49	49	1	\N	1969	47	510	1800	0	RICHELIEU	Ex TARFAYA /1065236/	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676949#0.7246446198870192	1	2009-04-15	50	50	1	\N	1969	47.5	500	1500	0	TREVIGNON	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676954#0.709302596073471	1	2009-04-15	51	51	1	\N	1969	47	510	1800	0	HENRI POLO	Ex TOUBAB DIALAW /265320/	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676958#0.8421427033481478	1	2009-04-15	52	52	1	\N	1969	47	510	1800	0	JACQUES COEUR	Offshore	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676963#0.25542251704593555	1	2009-04-15	53	53	99	\N	1977	0	1000	0	0	CARRIBDEAN	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676968#0.6727583782679324	1	2009-04-15	54	54	99	\N	0	0	0	0	0	JEANNETTE C	Coule	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832676973#0.24752119770009395	1	2009-04-15	55	55	99	\N	0	0	0	0	0	SAN JUAN	Cap de 1000 T systeme americain.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677115#0.6821025488586526	1	2009-04-15	71	71	1	\N	1956	19.5	23	200	0	ATTALAYA	2 cales de 8 tonnes	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677126#0.5144183569457691	1	2009-04-15	73	73	1	\N	1955	25.85	80	350	0	BIXINTXO 2	Premier thonier en acier. Participera à la première campagne expérimentale en 1955.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677131#0.38904459263003877	1	2009-04-15	74	74	1	\N	0	0	0	0	0	BLEUN BRUG	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677136#0.4040058407754271	1	2009-04-15	75	75	1	\N	1957	19	20	200	0	BOGA BOGA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677142#0.3654183574287315	1	2009-04-15	76	76	1	\N	1955	24	50	350	0	CARMENCHU	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677148#0.22815128685768749	1	2009-04-15	77	77	1	\N	0	0	0	0	0	CAYOLA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677153#0.6639116040689583	1	2009-04-15	78	78	1	\N	0	0	0	0	0	CDT LEVASSEUR	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677159#0.13883490800748255	1	2009-04-15	79	79	1	\N	1956	22	50	300	0	DOLORES	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677165#0.22264329613329248	1	2009-04-15	80	80	1	\N	0	0	0	0	0	HEGOKO IZARRA	Ex EDERRA	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677171#0.5584425450673562	1	2009-04-15	81	81	1	\N	1956	22	35	300	0	EGUN ON	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677178#0.3264058982719251	1	2009-04-15	82	82	1	\N	0	0	0	0	0	ERREGUINA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677183#0.1300308307259188	1	2009-04-15	83	83	1	\N	1958	24	50	350	0	ESPERANTZA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677189#0.2958350057773489	1	2009-04-15	84	84	1	\N	1957	22.799999	75	300	0	ETOILE D'ESPERANCE	Changement de puissance en 1983 440 CV	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677195#0.7586619077222576	1	2009-04-15	85	85	1	\N	1957	25.85	80	300	0	GABY BERNARD	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677201#0.095320460894608	1	2009-04-15	86	86	1	\N	1956	22	35	300	0	GALERNA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677294#0.02489741442072757	1	2009-04-15	101	101	1	\N	0	0	0	0	0	L'ALSACIENNE	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677304#0.9427074158987019	1	2009-04-15	103	116	1	\N	1956	24	50	350	0	MARITXU	Ex PRODIGE	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677309#0.9678841414383429	1	2009-04-15	104	104	1	\N	1956	22	35	450	0	MARTHA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677314#0.9922115161512793	1	2009-04-15	105	105	1	\N	1956	22	50	430	0	MAURICE RENE	coule le 2/7/86	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677318#0.9408055757031256	1	2009-04-15	106	106	1	\N	1955	25.85	80	350	0	MICHEL JOSEPH	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677323#0.7139848761244563	1	2009-04-15	107	32	1	\N	1956	25.5	65	350	0	MIRENTXU	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677327#0.8927834523619652	1	2009-04-15	108	108	1	\N	1959	21.700001	0	330	0	NOTRE DAME DU PONT	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677332#0.34860262682126597	1	2009-04-15	109	109	1	\N	0	0	0	0	0	PERE D'ALZON	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677336#0.03848906752941128	1	2009-04-15	110	110	1	\N	0	0	0	0	0	PERSISTANT	Arrete	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677340#0.09991312966710975	1	2009-04-15	111	111	1	\N	1957	22	50	300	0	PHARAON	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677348#0.3862867394925341	1	2009-04-15	112	112	1	\N	0	0	0	0	0	PIERRE LAURENCE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677352#0.9829578210701971	1	2009-04-15	113	113	1	\N	1958	26	78	400	0	PIERRE NICOLE	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677355#0.45873959701036016	1	2009-04-15	114	114	1	\N	1956	22.9	45	300	0	PIERROT	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677358#0.4069815294574606	1	2009-04-15	115	115	1	\N	1959	23	50	330	0	PIERROT MICHEL	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677361#0.7472775329639256	1	2009-04-15	116	116	1	\N	1956	24	50	350	0	PRODIGE	Devient MARITXU en 1965.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677365#0.7700023999960282	1	2009-04-15	117	117	1	\N	1958	26.299999	78	400	0	RESSAC	Coule	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677434#0.07855415734282489	1	2009-04-15	131	131	1	\N	1961	20.700001	38	430	0	EMIGRANT	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677442#0.8495500036069341	1	2009-04-15	133	133	1	\N	1969	47.5	550	1800	0	PRESIDENT LACOUR	Devenu AIDA	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677446#0.17699967980804054	1	2009-04-15	134	134	2	\N	0	0	0	0	0	FADIOUTH	Senneur depuis 1972	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677450#0.2162212442015764	1	2009-04-15	135	135	2	\N	0	0	0	0	0	LOMPOUL	Coule en 73	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677453#0.4879323205672784	1	2009-04-15	136	136	2	\N	0	0	0	0	0	NIODOR	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677457#0.0018723305182387628	1	2009-04-15	137	137	99	\N	0	0	0	0	0	PURITAN	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677461#0.23512959382050747	1	2009-04-15	138	138	99	\N	0	0	0	0	0	ANNA MARIA	Cap systeme americain 650 T	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677465#0.249253643216931	1	2009-04-15	140	139	2	\N	1971	36.810001	0	950	0	FASS BOYE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677469#0.45883593142082824	1	2009-04-15	141	140	2	\N	1972	36.810001	0	950	0	POINTE SANGOMAR	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677473#0.11372702304134519	1	2009-04-15	142	141	4	\N	1969	44.68	550	960	0	ALCOTAN	voir ABDU N'DIAYE /244311/	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677476#0.15312931098135651	1	2009-04-15	143	142	4	\N	0	30	170	620	0	PLAYA DE BAQUIYO	sardinier arret 73	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677480#0.5879809273445992	1	2009-04-15	144	143	4	\N	1963	36.799999	260	750	0	ALACRAN	arrete 75	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677484#0.9272489022910796	1	2009-04-15	145	144	4	\N	1964	36.799999	260	750	0	ALBONIGA	Coule 1987	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677488#0.9070540040421888	1	2009-04-15	146	145	4	\N	1966	38.279999	260	800	0	PLAYA DE PEDRENA	sardinier depuis mars 78	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677492#0.9066495881473792	1	2009-04-15	147	146	4	\N	1968	41.41	480	960	0	GURE CAMPOLIBRE	Type M-36-A.C.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677496#0.7941804617420198	1	2009-04-15	148	147	4	\N	1969	42.75	540	1100	0	PLAYA DE BERMEO	Sardinier depuis OCT 80	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677559#0.7269883881432812	1	2009-04-15	174	163	4	\N	1972	42.900002	425	980	0	JAI ALAI	BAJA en 1983	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677568#0.40581572058282844	1	2009-04-15	176	165	4	\N	1973	64.599998	1500	3000	0	AUGUSTIN 1	devenu HARMATTAN	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677572#0.25574513046432623	1	2009-04-15	177	166	1	\N	1958	31	120	500	0	AFRICAIN	Arrete fin 65	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677576#0.539217337765675	1	2009-04-15	178	167	1	\N	0	0	0	0	0	CAP LOPEZ	Ex YVON LOIC 2 ARRET 64	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677580#0.0547564927432872	1	2009-04-15	179	168	1	\N	0	0	0	0	0	POULDOHAN	Arrete fin 64	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677584#0.40383537809816494	1	2009-04-15	180	169	1	\N	0	0	0	0	0	CAP LOPEZ	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677588#0.7989934347179484	1	2009-04-15	181	20	1	\N	1952	32	380	630	0	COLUMBIA	Construit aux USA vendu en 1969 à St Jean de Luz et devient IRRINTZINA	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677592#0.6216784928469644	1	2009-04-15	182	171	1	\N	1963	31.5	0	0	0	BEG MEIL	Arrete fin 64	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677596#0.6560425469387059	1	2009-04-15	183	172	1	\N	0	0	0	0	0	KERFANT	Arrete fin 65	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677601#0.16626049520633923	1	2009-04-15	184	173	1	\N	0	0	0	0	0	PALMA	Arrete fin 66	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677613#0.36810924202856476	1	2009-04-15	185	174	1	\N	1958	28	80	450	0	COTE D ARGENT	Arrete fin 65	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677622#0.9597075895849472	1	2009-04-15	186	175	1	\N	1958	42.5	340	600	0	GAMBIE	Arrete fin 64	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677626#0.3378631491885846	1	2009-04-15	187	176	1	\N	0	0	0	0	0	VICTORINE BERTIN	Arrete fin 61	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677630#0.11790536766927318	1	2009-04-15	188	177	1	\N	0	31.879999	0	0	0	YOLANDE BERTIN	Arrete fin 62	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677635#0.9583936126428195	1	2009-04-15	189	178	1	\N	1950	33.950001	0	640	0	DANGUY	Arrete fin 66	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677701#0.32035045853545063	1	2009-04-15	204	192	2	\N	1970	34.599998	0	810	0	FADIOUTH (2)	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677709#0.5965006528667627	1	2009-04-15	206	194	2	\N	0	0	0	0	0	LOMPOUL 2	Coule	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677712#0.15172714464614345	1	2009-04-15	207	195	1	\N	1971	75.5	2250	4000	0	GUIPUZKOA	Vendu au Nicaragua vers 1975.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832677716#0.5916558221714627	1	2009-04-15	208	19	1	\N	0	32	0	0	0	IXILIK	Ex ILE DES FAISANTS	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677720#0.9068841995909076	1	2009-04-15	209	97	1	\N	1959	25	45	400	0	TUTTI	Ex KER TREGUIER	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832677724#0.9336089882882613	1	2009-04-15	210	196	1	\N	1972	75.5	2250	4000	0	CAP SIZUN	arret fin 82	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832677732#0.0362231723492642	1	2009-04-15	211	197	4	\N	1973	54.490002	800	2250	0	ALCAUDON	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677748#0.2809969641034946	1	2009-04-15	212	198	1	\N	1973	39.27	250	1050	0	KERNEVAD	Arret fin 76 devient MARSOUIN	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677752#0.019805527528823474	1	2009-04-15	213	199	4	\N	1973	54.490002	800	2250	0	TXORI	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677756#0.4935293150142165	1	2009-04-15	214	200	4	\N	1974	76.209999	1850	4000	0	ALBACORA 4	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832677760#0.4024507029171731	1	2009-04-15	215	201	1	\N	1973	75.5	2250	4000	0	PENDRUC	Ex NAVARRA 2	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832677764#0.8212298465607553	1	2009-04-15	216	153	99	\N	1970	47.5	750	1240	0	JAGUA	Senneur cubain	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9558433552679892	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677768#0.6986931041938954	1	2009-04-15	217	203	99	\N	0	0	0	0	0	SUN TUNA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.35040962267355735	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677772#0.11301067725137792	1	2009-04-15	218	204	1	\N	1975	63.299999	1053	3800	0	GLENAN	Vendu aux espagnols en octobre 91.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677776#0.537239732942937	1	2009-04-15	219	205	1	\N	1974	39.299999	250	1050	0	KELERENN	Arret fin 76 devient DAUPHIN	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677780#0.7630873794893344	1	2009-04-15	220	82	2	\N	0	0	0	0	0	JARWIN	Ex ERREGUINA	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677913#0.7506455820410047	1	2009-04-15	235	219	1	1991-04-01 00:00:00	1975	51	675	2200	0	GEVRED	avril 91 devenu 511 st vincent	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677929#0.8712271233646515	1	2009-04-15	237	220	99	\N	1973	0	0	0	0	ATLANTIS	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832677934#0.9694694733095696	1	2009-04-15	238	221	1	\N	1975	0	0	0	0	CALAO	philipin depuis 81	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677939#0.19759887409965238	1	2009-04-15	239	222	1	\N	1975	53.98	665	3200	0	ILE TRISTAN	Sorti de flotte durant le dernier semestre 2004 ( Plan EU: Récupération de KW )	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677944#0.5552031576718547	1	2009-04-15	240	223	1	\N	1975	62.700001	980	3650	0	MAGELLAN	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677949#0.3732802314181801	1	2009-04-15	241	224	1	\N	0	0	0	0	0	COPERNICUS	philipin depuis 81	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832677955#0.8490325368088042	1	2009-04-15	242	225	4	\N	0	0	0	0	0	ISABEL 4	?	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.18032080790817928	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677960#0.6818096449418161	1	2009-04-15	243	226	4	\N	1975	72.120003	1150	3600	0	ISABEL 5	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677965#0.20922294809046615	1	2009-04-15	244	227	4	\N	1975	56.110001	850	2000	0	MATXIKORTA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677971#0.04872632922239972	1	2009-04-15	245	228	4	\N	1975	61.27	800	3000	0	JUAN MARIA SOROA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832677976#0.9261124184936721	1	2009-04-15	246	130	2	\N	1957	22	30	300	0	MAWDO	Ex AIDE TOI	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832677986#0.048133694548140316	1	2009-04-15	247	39	1	\N	1962	28.75	29	460	0	OBE	(PORSGUIR) Senneur depuis 74	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832677991#0.7427105846859947	1	2009-04-15	248	205	1	\N	1974	39.299999	250	1050	0	DAUPHIN	Ex KELERENN desarme 1985	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9860530760211061	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832677997#0.3747027371633682	1	2009-04-15	249	198	1	\N	1973	39.27	250	1050	0	MARSOUIN	Ex KERNEVAD Désarmé 1985	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9860530760211061	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678002#0.741938929637814	1	2009-04-15	250	229	1	\N	1976	0	0	0	0	CUSCO	Debut en 1976 arrete (philipin?)	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678084#0.14632509977103647	1	2009-04-15	265	243	4	\N	1975	0	1160	3000	0	ATERPE ALAI	Coulé le 19/09/90 à 4h à l'entrée du port de Mahé. Début de l'incendie le 18/09 à 10h;	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678095#0.28322829026553664	1	2009-04-15	267	245	1	\N	1977	78.800003	2272	6000	0	MANICONGO	Congolais 77	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.43427027828177345	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678100#0.6347104300744734	1	2009-04-15	268	246	1	\N	1977	78.800003	2272	6000	0	ANZIKA	Congolais 77	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.43427027828177345	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678106#0.15759640065686775	1	2009-04-15	269	247	4	\N	1975	61.240002	925	3000	0	PLAYA DE ARITZATXU	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678112#0.17159689721734905	1	2009-04-15	270	248	4	\N	1977	65.110001	1090	4000	0	ITXAS NORTE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678117#0.4753850617060442	1	2009-04-15	271	249	4	\N	1977	0	1090	4000	0	ITXAS SUR	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678123#0.8090200849831751	1	2009-04-15	272	250	4	\N	0	0	0	0	0	ITXAS TURN	Bateau fantome	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678128#0.10234448317676537	1	2009-04-15	273	23	4	\N	1970	38.380001	220	1000	0	RIO AGUEDA	ex CAP ST PAUL devenu MASCAROI	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.411796820475434	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678134#0.8552300218450951	1	2009-04-15	274	251	1	\N	1979	69	1250	4000	0	ROSPICO	août-79	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678140#0.3227994633527759	1	2009-04-15	275	252	1	\N	1980	69	1250	3900	0	YVES de 'KERGUELEN	Mis en service 2 semestre 79 Départ pour le Pacifique 09/97 (  même Nom et pavillon )	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678145#0.7778564258556802	1	2009-04-15	276	253	1	\N	1979	54.009998	675	2400	0	CAP SAINT PIERRE 2	janv.-79	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678151#0.953988565755796	1	2009-04-15	277	254	4	\N	1975	52.299999	650	1850	0	IZURDIA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678156#0.9784033545260628	1	2009-04-15	278	255	4	\N	1976	65.360001	1050	1850	0	ALBACORA 5	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678160#0.7316360110659332	1	2009-04-15	279	256	4	\N	1976	76.760002	1300	4000	0	ALBACORA 6	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678165#0.8705845479832213	1	2009-04-15	280	257	4	\N	0	0	0	0	0	ALBACORA 7	Canneur Golfe de Gascogne,	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678243#0.9231948191097726	1	2009-04-15	295	269	4	\N	1979	0	1350	4800	0	ALMADRABA 2	nov.-79	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678261#0.00940344994138853	1	2009-04-15	297	271	3	\N	1980	69.019997	1250	3455	0	BEOUMI	Lance 80 devenu166403 VIA SIMOUN	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678266#0.38494556964810045	1	2009-04-15	298	272	1	\N	1980	54.009998	675	2400	0	STERENN	Lance 80	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678271#0.7258576930342913	1	2009-04-15	299	273	4	\N	1980	76.760002	1900	4400	0	MONTECELO	1980 retour de O Indien 	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678276#0.8999241118477258	1	2009-04-15	300	274	4	1980-02-01 00:00:00	1980	73.82	1450	4400	0	USISA	Fevrier 80 devenu 467332	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678281#0.05565439202500555	1	2009-04-15	301	275	1	\N	1977	75.5	2265	4000	0	ACHERNAR	 A la sortie de chantier: Esperansa puis devenu IBERIC	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.4429520672460452	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678287#0.6080412279119387	1	2009-04-15	302	276	1	\N	1977	75.519997	2265	4000	0	DELPHIN	Même catactéristiques que l'IBERIC	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.4429520672460452	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678292#0.700854050517628	1	2009-04-15	303	277	1	\N	1977	75.519997	2265	4000	0	GALLIC	Ex DELPHIN	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.4429520672460452	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678297#0.5943562265414539	1	2009-04-15	304	275	1	\N	1977	75.5	2265	4000	0	IBERIC	Transfere 1767341 MEXIQUE	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.4429520672460452	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678303#0.4931064216631643	1	2009-04-15	305	278	1	\N	1981	69	1250	3900	13	HUON DE KERMADEC	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678308#0.5844320249697519	1	2009-04-15	306	279	1	\N	1982	70.5	1250	4400	13	TRESCAO	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678313#0.8224776494927836	1	2009-04-15	307	280	4	\N	1979	76.760002	1700	4400	0	GERMON	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9860530760211061	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678319#0.36022572798233565	1	2009-04-15	308	281	4	\N	1980	77	1850	4400	0	ALBACORA 11	Devenu pontesa primero en 91 Capacite de transport 1250 tonnes	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678324#0.1242175514570385	1	2009-04-15	309	282	4	\N	1980	73.900002	1700	4400	0	ENTREMARES 2	Devenu TXORI ZURI 467349	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678329#0.5556796548266134	1	2009-04-15	310	283	99	\N	1980	77.32	1750	4399	0	MARATUN	Devenu INTERTUNA 1	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.1719246898492357	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678335#0.9864187317316306	1	2009-04-15	311	141	2	\N	1969	44.68	550	960	0	ABDOU NDIAYE	Ex ALCOTAN	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678415#0.24749482302179926	1	2009-04-15	326	296	1	\N	1982	55.52	813	2400	0	CAP SAINT PAUL	Même caractéristiques que le ^Santa Maria, l'Armen et l'Avel Viz.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678420#0.7527066282038004	1	2009-04-15	327	297	1	\N	1983	70.5	1380	3600	13	KERSAINT de COETNEMPREN	Nom complet " Kersaint de Coetnempren" - devenu TAKAMAKA en janvier 2007 (code 771)	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678431#0.37012900575901886	1	2009-04-15	329	299	4	\N	1979	0	1150	4600	0	ARATZ	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678436#0.653577946900982	1	2009-04-15	330	300	4	\N	1983	77.32	1850	4300	0	ALBACORA 14	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678441#0.38821375704428573	1	2009-04-15	331	301	4	\N	1981	76	1358	4400	0	MONTEFRISA 7	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678446#0.6349887676742348	1	2009-04-15	332	274	4	\N	1980	73.82	1360	4400	0	ALMADRABA 3	Ex USISA 467300	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678451#0.11720300603529987	1	2009-04-15	333	302	20	\N	0	0	0	0	0	TRIDAKNA	URSS depuis 1981	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678456#0.04955002586440027	1	2009-04-15	334	303	1	\N	1983	70.5	1380	3600	13	PENDRUC 2	1983. Départ pour le Pacifique le 05/12/2004	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678461#0.8848371349192811	1	2009-04-15	335	304	4	\N	1983	52.330002	700	2000	0	EGALUZE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678466#0.49540633328970407	1	2009-04-15	336	305	4	\N	1983	52.330002	700	2000	0	EGALABUR	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678471#0.8932839788272164	1	2009-04-15	337	306	99	\N	1982	71.019997	0	3600	0	CABO SAN LUCAS	?	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678483#0.923740535013173	1	2009-04-15	338	512	99	\N	0	0	0	0	0	TUNAORO 1	A opéré en Atlantique vers 83	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678488#0.2763260041882859	1	2009-04-15	339	307	99	\N	1982	71.019997	0	3600	0	MARIA FERNANDA	?	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678493#0.13415206448388817	1	2009-04-15	340	308	99	\N	1983	71.019997	0	3600	0	MARIA VERONICA	?	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678498#0.921751571738052	1	2009-04-15	341	275	99	\N	1977	75.5	2265	4000	0	IBERIC	Transfere MEXIQUE ex 1667304	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678572#0.1488127029466283	1	2009-04-15	354	317	4	\N	1983	76.760002	1850	4399	0	MONTEFRISA 9	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678584#0.27076043099681335	1	2009-04-15	356	319	4	\N	1984	83.019997	1850	5700	0	MAR DE SERGIO	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678590#0.6944050146400026	1	2009-04-15	357	320	14	\N	1981	80.470001	0	6000	0	FIRAW	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678596#0.8753392175761001	1	2009-04-15	358	124	2	\N	1955	24	50	350	0	MAWDO 2	Ex TUTINA	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832678601#0.5656798805949445	1	2009-04-15	359	230	1	\N	1976	54.009998	675	2400	0	CAP SAINT VINCENT 02	Ex NEPTUNE 365251	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678607#0.5028321116210787	1	2009-04-15	360	321	7	\N	1979	56.009998	750	2600	0	LADY SUSHILL	Senneur mauricien	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.24921769452411147	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678613#0.7877666743983743	1	2009-04-15	361	216	99	\N	1975	50.880001	675	2200	0	EBONY LADY	Ex CAP BOJADOR	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.35040962267355735	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678619#0.7744561518880975	1	2009-04-15	362	322	14	\N	1969	49.200001	0	850	0	AFKO 103	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678625#0.032632222044040704	1	2009-04-15	363	323	14	\N	1969	72.900002	0	2400	0	AFKO 203	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678631#0.1833817291951474	1	2009-04-15	364	324	14	\N	1968	49.610001	0	950	0	AFKO 303	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678636#0.17805974417928594	1	2009-04-15	365	325	14	\N	1974	57.869999	0	1800	0	AFKO 305	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678642#0.16859269676083155	1	2009-04-15	366	326	14	\N	1974	57.869999	0	1800	0	AFKO 306	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678648#0.1271739333297217	1	2009-04-15	367	327	14	\N	1974	57.869999	0	1800	0	AFKO 307	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678654#0.30356806296636907	1	2009-04-15	368	328	14	\N	1974	57.099998	0	1800	0	AFKO 308	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678660#0.8463539451953007	1	2009-04-15	369	329	14	\N	0	0	0	0	0	AFKO 311	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678666#0.4575116371778487	1	2009-04-15	370	330	14	\N	1972	46.110001	0	1000	0	BIG JOHN	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678761#0.481359376738983	1	2009-04-15	386	346	14	\N	0	0	0	0	0	LOIS 2	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678772#0.672022960447404	1	2009-04-15	388	348	14	\N	1968	47.990002	0	1100	0	AFKO 301	Devenu transporteur oct 88	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678777#0.0385348629415766	1	2009-04-15	389	349	14	\N	0	0	0	0	0	AFKO 302	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678782#0.3267590032066594	1	2009-04-15	390	350	14	\N	0	0	0	0	0	AFKO 310	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678788#0.11419023908657389	1	2009-04-15	391	351	14	\N	0	0	0	0	0	KAAS 106	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678794#0.12472239488927728	1	2009-04-15	392	352	14	\N	0	0	0	0	0	BRENYA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678799#0.5229553328580481	1	2009-04-15	393	353	14	\N	0	0	0	0	0	NANKO STAR	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678805#0.5690188034768544	1	2009-04-15	394	356	14	\N	1963	46.84	0	650	0	JOY	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678810#0.7617999022886767	1	2009-04-15	395	129	1	\N	1970	47	510	1800	0	MARSOUIN	Ex ILE BOULAY 365129 en fin 86	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678816#0.9242744587193744	1	2009-04-15	396	355	14	\N	1966	47.5	0	850	0	OBAATAN	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678822#0.19887811990837045	1	2009-04-15	397	356	14	\N	1963	46.84	0	650	0	AFKO 101	Transporteur	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678827#0.3254934707236591	1	2009-04-15	398	357	14	\N	1973	51.490002	0	1600	0	COAST LINE 101	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678833#0.6606577646280735	1	2009-04-15	399	275	4	\N	1977	75.5	2265	4000	0	IZARO	Seychellois devenu maltais	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.3241600448153684	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678840#0.131699747217899	1	2009-04-15	400	359	7	\N	1977	55.400002	700	2500	0	FUKUICHI MARU	Ocean indien aout 89 meme que 468	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832678846#0.5886222180072141	1	2009-04-15	401	165	1	\N	1973	64.599998	1500	3000	0	VIA HARMATTAN	Ex HARMATTAN ivoirien 366260	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832678851#0.15973853370350832	1	2009-04-15	402	97	2	\N	1959	25	45	400	0	SANTA CATARINA	Ex KER TREGUIER	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832678944#0.2830137381357497	1	2009-04-15	418	341	14	\N	1974	57.43	0	1800	0	AFKO 313	Ex KAAS 107	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832678956#0.8460566411086201	1	2009-04-15	420	370	4	\N	1968	0	0	515	0	ILUSION LAREDANA	Canneur espagnol - JB 137.71 Tx  Immatriculation AL-1817	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832678963#0.6175208186943999	1	2009-04-15	421	371	4	\N	0	0	0	0	0	AMIGOS DEL MAR	Canneur espagnol	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832678969#0.8239472688514017	1	2009-04-15	422	372	4	\N	1963	0	0	330	0	GRAN TIMOTEA	Canneur espagnol JB 107.86 Tx  Immatriculation BI-2-2377	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832678977#0.5115384396569796	1	2009-04-15	423	373	20	\N	1980	85	1000	0	0	BST PREOBRAJENSKY	Senneur russe Ocean Indien	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678983#0.05169462007975956	1	2009-04-15	424	374	20	\N	1979	85	1250	5200	0	BST RODINA	Senneur russe Ocean Indien	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678990#0.9722782577055608	1	2009-04-15	425	375	20	\N	0	0	0	0	0	BST BORZON	Senneur russe Ocean Indien	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832678997#0.15044112705003232	1	2009-04-15	426	376	1	\N	1987	61.75	1050	2600	0	LADY SUSHILL 02	Senneur mauricien lance en 87	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.24921769452411147	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679005#0.15645006622610713	1	2009-04-15	427	377	1	\N	1982	33.529999	0	0	0	CORONA DEL MAR	Arrivé à Dakar en 88	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679011#0.23054030096934452	1	2009-04-15	428	17	2	\N	1969	32.919998	174	850	0	MARSASSOUM	Ex senneur  234017 puis canneur 	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679019#0.35596790257975897	1	2009-04-15	429	378	14	\N	1972	54.400002	0	1800	0	GOSHEN 603	Devenu ABETO en 91	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679033#0.6045641468396421	1	2009-04-15	430	268	1	\N	1980	69.019997	1250	3900	0	BAYOTA	Ex:  366294	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679039#0.14498086439294522	1	2009-04-15	431	379	14	\N	1975	62.110001	0	2200	0	LAUREL	Canneur PFCI 88	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679046#0.7783783114482294	1	2009-04-15	432	337	14	\N	1974	57.43	0	1800	0	AFKO 312	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679052#0.7962941573864698	1	2009-04-15	433	217	1	\N	1974	50.849998	675	2200	0	BELOUGA	Ex ivoirien BELIER 365233	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679059#0.3364341212227221	1	2009-04-15	434	381	14	\N	0	0	0	0	0	GOSHEN 602	Nouveau venu de Coree	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679165#0.0048999318214689724	1	2009-04-15	449	392	99	\N	0	0	0	0	0	TUNA WEST	Premier debarquement a Abidjan en aout 89	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.35040962267355735	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679178#0.5311196562140889	1	2009-04-15	451	394	99	\N	1965	0	750	1800	0	EASTERN PACIFIC	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.24921769452411147	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679185#0.24478211263939165	1	2009-04-15	452	395	20	\N	0	85	1050	5200	0	TIORA	?	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679192#0.2684688607549879	1	2009-04-15	453	218	1	\N	1975	50.880001	675	2200	0	ILE DE KERBIHAN	Ancien N'ZIDA (n°234)	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679199#0.4729967826134056	1	2009-04-15	454	396	4	\N	1989	77.300003	1650	4690	0	FELIPE RUANO	Octobre 89 premier debarquement a ABIDJAN	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679206#0.4636956893063564	1	2009-04-15	455	342	14	\N	1971	40.619999	0	1000	0	BANI	Ancien MARY RADINE	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679212#0.41573691211049313	1	2009-04-15	456	397	14	\N	1974	50.700001	0	1350	0	CEDRO	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679219#0.3528334244369815	1	2009-04-15	457	398	14	\N	1972	59.52	0	2100	0	JAZMIN	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679226#0.30351776562335	1	2009-04-15	458	399	4	\N	1989	79.349998	1750	3835	0	CAMPOLIBRE ALAI	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679234#0.6630576857980705	1	2009-04-15	459	400	4	\N	1983	69.220001	1282	4080	0	MONTELAPE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679242#0.6767624173659832	1	2009-04-15	460	401	4	\N	1982	69.220001	1282	4080	0	MONTEALEGRE	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679253#0.3821154545346408	1	2009-04-15	461	402	14	\N	0	0	0	0	0	GOSHEN 605	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679261#0.10023694572129516	1	2009-04-15	462	213	1	\N	1974	51	675	2200	0	LAURENT	Debut 90 à Abidjan	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679267#0.21067973897725356	1	2009-04-15	463	403	1	\N	1990	58.169998	825	3899	0	GOMBESSA	Nouveau en Ocean Indien en avril 90	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679274#0.3421570344807776	1	2009-04-15	464	404	4	\N	1963	0	0	270	0	CUMBRE DE GORBEA	canneur espagnol a Dakar  JB 109.51 Tx  Immatriculation BI-2-2395	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832679396#0.07036568834095758	1	2009-04-15	479	415	20	\N	1979	59.009998	0	2200	0	ZEMLYANSK	Signale Ocean Indien en septembre 90	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679410#0.5193748435856844	1	2009-04-15	481	417	4	\N	1990	77.300003	1800	4399	0	ALBACORA CARIBE	Neuf premier debarquement en octobre 90,	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679417#0.49715523728597366	1	2009-04-15	482	418	1	\N	1990	78.339996	1650	4200	16.799999	VIA AVENIR	Construit USA lance octobre 90	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679425#0.9136908731720471	1	2009-04-15	483	419	1	\N	1990	61	1050	3340	13	CAP BOJADOR	Nouveau abidjan en 12/90 pour marée d'essai puis Seychelles.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679435#0.4356480154033766	1	2009-04-15	484	420	4	\N	0	0	0	0	0	AITA	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679442#0.7108892528872649	1	2009-04-15	485	51	1	\N	1969	47	510	1800	0	DUC DE PRASLIN	Ex VIA FOEHN - Vendu à l'Equateur en 1992	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679449#0.9875327800610685	1	2009-04-15	486	421	4	\N	1990	105	3500	8157	0	ALBACORA	Seul classe 8 en 1991	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832679457#0.7507489037163033	1	2009-04-15	487	284	14	\N	1973	66.5	1250	3600	0	GOLD COAST	Ex Gold Coast 313	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679472#0.867319585479984	1	2009-04-15	488	422	4	\N	1991	81	1850	4500	0	TXORI BERRI	Neuf vers juin 91	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679479#0.6368037658583545	1	2009-04-15	489	423	7	\N	1986	64.699997	1050	3000	0	CIRNE	Neuf en service 1991	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.24921769452411147	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679486#0.11454560958948556	1	2009-04-15	490	424	1	\N	1991	81.900002	1800	4957	15.5	GUEOTEC	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679493#0.2315348086757415	1	2009-04-15	491	425	1	\N	1991	61	1050	3340	13.5	AVEL VOR	Sister ship Cap Bojador	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679500#0.49724148562040205	1	2009-04-15	492	426	1	\N	1991	81.900002	1800	4957	15.5	GUERIDEN	Sister-ship du Guéotec	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679507#0.15740408680282503	1	2009-04-15	493	427	99	\N	0	0	300	0	0	SPIRIT OF KOXE	Coque plastique construit a Sete	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679513#0.4531972018808279	1	2009-04-15	494	428	20	\N	1992	79.800003	1951	4957	0	PINNA	?	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679520#0.25308046370368964	1	2009-04-15	495	429	7	\N	0	0	0	0	0	GIKYU MARU 1	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679653#0.5833854972713811	1	2009-04-15	511	219	1	1991-04-01 00:00:00	1975	51	675	2200	0	GEVRED	Pavillon St VINCENT depuis avril 91	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679666#0.5462726714356573	1	2009-04-15	513	443	1	\N	1991	77.800003	1650	4200	0	RIO MARE	Armement Italien sister Via Avenir	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679673#0.6771400613485944	1	2009-04-15	514	204	4	\N	1975	63.299999	1053	3800	0	ITXAS BIDE	Ancien Glenan	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679680#0.8235001811940197	1	2009-04-15	515	444	99	\N	1988	55.810001	800	2875	0	AZEDEGAN 1	Iranien	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679686#0.9971389685261008	1	2009-04-15	516	445	99	\N	1984	55.810001	850	2875	0	AZEDEGAN 2	Iranien	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679693#0.07501691716232939	1	2009-04-15	517	446	20	\N	1992	79.800003	1951	4957	0	TIVELA	Sister ship Kaouri et Pinna	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679700#0.6623458903640128	1	2009-04-15	518	447	20	\N	1992	79.800003	1951	4957	0	PLATON	Sister ship Pinna et Kaouri	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679707#0.9583461572508706	1	2009-04-15	519	448	4	\N	1984	26.01	0	500	0	GAROUPA	Canneur Cap Vert	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832679714#0.8771530073060781	1	2009-04-15	520	449	20	\N	1992	79.800003	1951	4957	0	ARISTOTEL	Sister ship pinna et kaouri	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679721#0.7360275235993841	1	2009-04-15	521	450	4	\N	1991	77.300003	1900	4000	0	ALBACAN	Debarque en Espagne	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679727#0.8512065556815798	1	2009-04-15	522	451	20	\N	1992	79.800003	1951	4957	0	TELLINA	Sister ship du PINNA	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679734#0.20292580011018402	1	2009-04-15	523	452	7	\N	1982	57.310001	0	1500	0	HAKKO MARU 27	?	fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679752#0.29400356696365215	1	2009-04-15	524	207	4	\N	1974	0	600	1800	0	ALIZE	Ex PLAYA DE LAIDA	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832679760#0.5500342695210556	1	2009-04-15	525	281	14	\N	1980	77	1850	4400	0	PONTESA PRIMERO	Ex ALBOCORA 11	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.10207212372204011	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679767#0.6365163876549216	1	2009-04-15	526	453	4	\N	1991	0	0	710	0	SANTA GEMA QUINTO	Nouveau canneur espagnol. Vendu en 2001  JB 139.91 Tx  Immatriculation SS-1-2451	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679892#0.18794394471175213	1	2009-04-15	543	275	4	\N	1977	75.5	2265	4000	0	IZARO	Ex 537 et 399 10/6/94	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679908#0.3817540184699588	1	2009-04-15	545	271	1	\N	1980	69.019997	1250	3455	0	VIA SIMOUN	devient liberien en septembre 94	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679916#0.4468018330605117	1	2009-04-15	546	256	4	\N	1976	76.760002	1300	4000	0	ALBACORA 6	change de pavillon debut 93	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679925#0.73150053110101	1	2009-04-15	547	455	20	\N	1991	79.800003	1851	4957	0	PURPURA	Sister ship kaouri	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679940#0.24599359953405753	1	2009-04-15	548	262	4	\N	1975	0	1200	4000	0	KAI ALAI	Ex 465	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832679948#0.8856943879271462	1	2009-04-15	549	240	4	\N	1977	0	1450	4400	0	ALAI	Ex EUZKADI ALAI coule en octobre 94	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679956#0.6642833281599697	1	2009-04-15	550	288	4	\N	1980	73.889999	1700	4399	0	ALACRAN 	348	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679966#0.6801316918742202	1	2009-04-15	551	443	1	\N	1991	77.800003	1650	4200	16	VIA EUROS	Ex Rio Mare	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832679973#0.5970247979333803	1	2009-04-15	552	420	2	\N	0	0	0	0	0	ROKHAYA	Ex AITA	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832679980#0.45914179395618926	1	2009-04-15	553	132	1	\N	0	0	0	0	0	ESPERANZA	Congelateur jusqu au 19/1/95	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832679987#0.2879453448807643	1	2009-04-15	554	132	2	\N	0	0	0	0	0	MOHAMED FADEL	Ex esperanza	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832679994#0.9365685437477651	1	2009-04-15	555	219	1	1995-03-15 00:00:00	1975	50.880001	668	2200	0	AVEL HUEL	Ex Gevred ( changement mi mars 95)	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680001#0.09245034416783682	1	2009-04-15	556	274	14	1995-03-30 00:00:00	1980	73.82	1450	4400	0	PONTESA CUATRO	Ex almadraba 3 changement fin mars 95	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.9573433532024453	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680008#0.44710270771738125	1	2009-04-15	557	464	14	\N	1981	0	1000	2200	0	PONTESA 3	arrive a Abidjan mars 95	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.9573433532024453	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680016#0.9948078876948003	1	2009-04-15	558	465	14	\N	1973	57.709999	0	1650	0	TULIPAN 2	Signale a Tema juillet 95	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680023#0.5071440248905501	1	2009-04-15	559	466	20	\N	1992	79.800003	1951	4957	0	DEMOSFEN	Signale en Ocean indien aout 95	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.07349229119463152	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680141#0.2489640963183476	1	2009-04-15	575	417	4	1996-04-01 00:00:00	1990	77.300003	1800	4399	0	ALBACORA CARIBE	Ex panama (481) changement en avril 96	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680169#0.07440706922020535	1	2009-04-15	577	479	14	\N	1975	62.82	0	2100	0	MARINE 707	Signale au Ghana en octobre 96	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680176#0.8058934174956165	1	2009-04-15	578	480	14	\N	1975	68.129997	0	3600	0	MARINE 712	signale au Ghana en octobre 96	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680190#0.55643022997713	1	2009-04-15	580	449	1	\N	1992	79.800003	1951	4957	0	CASTEL BRAZ	Changement debut 97 ex Aristotel ( 520 )	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680183#0.9914313967421479	1	2009-04-15	579	481	1	\N	1997	107.5	1972	8429	17.5	VIA LIBECCIO	Neuf janvier 97 bateau usine ( Cap reduite suite transformation usine )	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680198#0.05283197775201365	1	2009-04-15	581	466	1	\N	1992	79.800003	1951	4957	16	MEN GOE	Changement debut 97 ex Demosfen ( 559 )	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680205#0.7241609643757315	1	2009-04-15	582	455	1	\N	1991	79.800003	1851	4957	0	DOURVEIL	Changement debut 97 ex Purpura  ( 547 )	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680212#0.06287874219258438	1	2009-04-15	583	447	1	\N	1992	79.800003	1951	4957	14	TALENDUIC	Changement debut 97 ex Platon ( 518 )	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680219#0.16958913130483577	1	2009-04-15	584	462	1	\N	1993	79.800003	1851	4950	0	BRUNEC	Changement debut 97 ex Rodios ( 528 )	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680227#0.6442419148144217	1	2009-04-15	585	451	1	\N	1992	79.800003	1951	4957	15	MEN CREN	Changement debut 97 ex Tellina ( 522 )	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680234#0.11616045106071649	1	2009-04-15	586	154	4	\N	1972	58.400002	1250	3000	0	ITXAS GANE	Ex ITXAS GANE ( 444 )	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.5623209070898836	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680241#0.5832866805169316	1	2009-04-15	587	482	1	\N	1997	107.5	1972	8429	17.5	VIA GWALARN	Neuf mai 97 bateau usine  ( Cap reduite suite transformation usine )	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680249#0.10837744447489761	1	2009-04-15	588	16	2	\N	1969	32.919998	174	850	0	Pdt MAGATTE DIACK	Ex  Soumbedioume ( 16 ). Coulé en janvier 2003 au large de la Mauritanie faisant 8 disparus. TJB 302.38 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680256#0.7504605554827521	1	2009-04-15	589	483	4	\N	0	0	0	0	0	SAN FRANCISCO	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680263#0.06115471386126292	1	2009-04-15	590	482	1	1997-08-01 00:00:00	1997	107.5	1972	8429	17.5	VIA GWALARN	Devient seychellois vers aout 97  ( Cap reduite suite transformation usine )	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680379#0.4998212345384294	1	2009-04-15	603	373	20	\N	1980	85	1000	0	0	OCEAN PRIDE MARINE	Ex 423	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680395#0.012928086136635653	1	2009-04-15	605	439	20	\N	1992	79.800003	1951	4957	0	ELEGANCE	Ex Kaouri ( 506 )	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680402#0.6262264182947519	1	2009-04-15	606	488	99	\N	1966	35.970001	0	700	0	SKARVOY	Chalutier transformé en canneur	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680410#0.15552006590879597	1	2009-04-15	607	489	1	\N	0	0	0	0	0	SAINT RIOC	En septembre 98 canneur supply ACF	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680417#0.880040767713646	1	2009-04-15	608	490	4	\N	1996	0	0	430	0	PILAR TORRE	Canneur en septembre 98 JB 136.99 Tx  Immatriculation ST-2-4-96	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680425#0.6576285566421428	1	2009-04-15	609	17	2	1998-09-01 00:00:00	1969	32.919998	174	850	0	PDT MATAR NDIAYE	Ex Marsassoum ( 428 ) changement de nom en 09/98. Sister ship du 588. TJB 302.38 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680433#0.8149915569124698	1	2009-04-15	610	283	4	\N	1980	77.32	1750	4399	0	INTERTUNA UNO	Ex 476	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680440#0.13497438820268481	1	2009-04-15	611	492	99	\N	1995	76	1080	3499	13	JIHAD-E-AZADEGAN 3	Jauge Brute 1200 Tx Capacite de transport 750 tonnes. 02/08/01 gérance Cobrecaf ( nouvelle senne)	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680450#0.5700396046300109	1	2009-04-15	612	146	4	\N	1968	41.41	480	960	0	GURE CAMPOLIBRE	Ex 147	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680458#0.930078378778818	1	2009-04-15	613	204	4	\N	1975	63.299999	1053	3800	0	ITXAS BIDE	Ex 514	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680465#0.6614111311861987	1	2009-04-15	614	181	1	\N	1972	39.299999	220	1050	0	ETOILE D'ESPERANCE	Ex 445 ( changement de pavillon ). Vendu en 2001	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680473#0.2555887361220569	1	2009-04-15	615	493	4	\N	1998	0	2000	0	0	PLAYA DE ANZORAS	85 métres Capacite de transport 1400 tonnes	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832680480#0.7601223110433766	1	2009-04-15	616	448	4	\N	1985	26.01	0	500	0	BATALLAU	Ex GAROUPA 519. Coulé au large de la Mauritanie le 27/08/01	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680489#0.40464960050209975	1	2009-04-15	617	494	2	\N	1965	0	0	380	0	JOHN F. KENNEDY	JB 116.19 Tx  Immatriculation BI-1-3128	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680497#0.6037755894703554	1	2009-04-15	618	495	4	\N	0	0	0	0	0	FARO VILLANO	?	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680606#0.6825471285172159	1	2009-04-15	632	256	4	1999-10-27 00:00:00	1976	76.760002	1300	4000	0	ALBACORA 6	Devient Antilles Hollandaises le 27/10/99	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680621#0.8413127363054944	1	2009-04-15	634	232	4	\N	1977	78.800003	2272	6000	0	DEMIKU	Refait à neuf en début 2000 ( armement esp ) ex Loango	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680629#0.15950699637513155	1	2009-04-15	635	256	4	2000-08-01 00:00:00	1976	76.760002	1300	4000	0	ALBACORA 6	Devient seychellois le 01/08/00	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680636#0.06869554758613872	1	2009-04-15	636	504	14	\N	0	0	0	0	0	GBESSE 11	Canneur	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680644#0.39420005762052857	1	2009-04-15	637	505	14	\N	0	0	0	0	0	SINFIN TRES	Canneur	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680652#0.40381667210941297	1	2009-04-15	638	506	14	\N	0	0	0	0	0	SINFIN UNO	Canneur	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832680660#0.4025575263700838	1	2009-04-15	639	507	14	\N	0	0	0	0	0	AFKO 805	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680667#0.6843596718212518	1	2009-04-15	640	403	14	\N	1990	58.169998	825	3899	0	AGYENKWA	Changement de pavillon en 2000	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832680675#0.9339959863666697	1	2009-04-15	641	509	14	\N	0	0	0	0	0	ALAMO	?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680683#0.06175285060765112	1	2009-04-15	642	159	14	\N	1972	75.5	2250	4000	0	DRAGO	ex BONSA (353)	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832680691#0.7356451215918068	1	2009-04-15	643	218	14	\N	1975	51	675	2200	0	ILE DE KERBIHAN	Vers début 1999.	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680699#0.924669444547416	1	2009-04-15	644	213	14	\N	1974	51	675	2200	0	LAURENT	Vers début 1999.	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680707#0.35040132297578275	1	2009-04-15	645	219	14	2000-06-01 00:00:00	1975	51	668	2200	0	AVEL HUEL	Devenu ghaneen vers 06/2000.	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832680714#0.0537402359871304	1	2009-04-15	646	500	4	\N	2000	39.549999	250	800	0	LIO 1	Ex bateau 627	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680722#0.31459829240783155	1	2009-04-15	647	501	4	\N	2000	39.549999	250	800	0	LIO 2	Ex bateau 628	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832680738#0.8627102868853659	1	2009-04-15	649	38	1	\N	1958	25.200001	85	350	0	PETIT LOIC	Devenu GUERNIKA 2, arrêté en 1985.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680817#0.6079175782251972	1	2009-04-15	659	517	1	\N	1955	24	50	350	0	CURLINKA	En avril 1961, premier thonier français a tourner un filet à thons sur les côtes africaines au large de la Sierra Léone ( Patron Jacky URNAU )  	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680833#0.36190548510558473	1	2009-04-15	661	519	1	\N	1956	22	50	300	0	EDERRENA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680843#0.04882387108577069	1	2009-04-15	662	520	1	\N	1956	20	30	240	0	HENRI	Navigue toujours en 2000.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680852#0.5152897012748471	1	2009-04-15	663	521	1	\N	1956	20	35	350	0	LE VAGABOND	Pratique toujours le germon dans le golfe de Gascogne en 2000.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680866#0.05680858316788262	1	2009-04-15	664	522	1	\N	1956	20	35	240	0	MARGARITA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680876#0.5301188695141958	1	2009-04-15	665	523	1	\N	1956	22	50	450	0	MASSILIA	Seul bateau construit en bois en Méditerranée.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832680886#0.1746453676197789	1	2009-04-15	666	524	1	\N	1956	21	27	300	0	NERE NAHIA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680896#0.28154105979100863	1	2009-04-15	667	525	1	\N	1956	19	30	330	0	NOIZ BAIT	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680905#0.16556876632287598	1	2009-04-15	668	526	1	\N	1956	18	15	150	0	POURQUOI PAS	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680915#0.3527259600067715	1	2009-04-15	669	527	1	\N	1957	20	20	200	0	ARROKA	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680924#0.008290030528504966	1	2009-04-15	670	528	1	\N	1957	22	35	300	0	ANGE DES MERS	?	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680934#0.7255808689231416	1	2009-04-15	671	529	1	\N	1948	20	0	200	0	LAURENCE	Quitta St Jean de Luz le 24/12/1949 pour rejoindre Douala.	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680943#0.5904936906788806	1	2009-04-15	672	530	1	\N	1950	14	0	72	0	ALEGERA	Repose aujourd'hui sous le sol de la Conserverie du Sénégal	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680953#0.18132558750502958	1	2009-04-15	673	531	1	\N	1950	14	0	72	0	DANTON	Repose aujourd'hui sous le sol de la Conserverie du Sénégal	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848
fr.ird.observe.entities.referentiel.Bateau#1239832680962#0.6452359996962729	1	2009-04-15	674	532	4	\N	2000	115	3250	0	0	INTERTUNA TRES	?	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681066#0.7690163505614248	1	2009-04-15	685	535	4	\N	2001	91.900002	2554	6600	0	MONTELUCIA	Sorti Vigo le 9/02/2001	fr.ird.observe.entities.referentiel.Pays#1239832675596#0.9401263656512455	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681084#0.7718904783895076	1	2009-04-15	687	258	4	\N	0	0	0	0	0	ALBACORA 8	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681093#0.885750687769418	1	2009-04-15	688	270	1	2001-04-27 00:00:00	1980	69	1250	4400	13	HAVOUR 1	Chgt le 27/04/01	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681101#0.09419132723208068	1	2009-04-15	689	251	1	2001-04-27 00:00:00	1979	69	1250	4000	13	HAVOUR 2	Chgt le 27/04/01	fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681109#0.028027887687233632	1	2009-04-15	690	232	4	2001-01-29 00:00:00	1977	78.800003	2272	6000	0	DEMIKU	Passé seychellois le 29 janvier 2001	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681117#0.20334796911464947	1	2009-04-15	691	536	1	\N	2001	67.300003	1200	4080	13	STERENN 2	Sorti Concarneau en mars 2001,	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681126#0.48614914150582234	1	2009-04-15	692	417	4	2000-12-01 00:00:00	1990	77.300003	1800	4399	0	ALBACORA CARIBE	Ex panama (481) changement en décembre 2000	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.1719246898492357	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681134#0.6672214254991515	1	2009-04-15	693	447	1	2001-09-26 00:00:00	1992	79.800003	1742	4957	14	TALENDUIC	Devient français le 26/09/01 | 2003-2004 : Modification de la taille des cuves (condamnation de 2 cuves à l'avant du bateau), perte de 176 m3	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681142#0.029383148627042832	1	2009-04-15	694	480	14	\N	1975	68.129997	0	3600	0	AGNES 1	Changement de nom en 2001.	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681151#0.33303243866724264	1	2009-04-15	695	468	14	\N	0	0	0	0	0	JITO	Date du changement de nom ?	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681159#0.9832884741492554	1	2009-04-15	696	183	99	\N	1973	62.740002	980	3650	0	CHRISTOPHE COLOMB	vendu en 01/2002 Armé au thon rouge.	fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681167#0.7976982223262854	1	2009-04-15	697	237	99	2002-02-01 00:00:00	1978	62.77	996	3650	0	PRINCE DE JOINVILLE	Vendu à l'Equateur en 02/2002	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.18032080790817928	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681176#0.7060251698409777	1	2009-04-15	698	238	99	2002-02-01 00:00:00	1978	62.77	980	3650	0	L. A. 'BOUGAINVILLE	Vendu à l'Equateur en 02/2002	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.18032080790817928	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681185#0.8544351799704745	1	2009-04-15	699	156	4	2001-12-18 00:00:00	1971	54.619999	850	2000	0	MONTENEME	Changement depuis le 18/12/01	fr.ird.observe.entities.referentiel.Pays#1239832675592#0.1719246898492357	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681193#0.7828932439273804	1	2009-04-15	700	537	4	\N	2002	85.5	2000	6000	17	PLAYA DE ARITZATXU	Sorti Bilbao le 02/03/02. Armateur PEVASA.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681293#0.2968138123470996	1	2009-04-15	711	450	4	\N	1991	85.849998	1900	4020	0	ALBACAN	Arrivé sur zone après modification le 21/09/2002	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681309#0.6565556108335776	1	2009-04-15	713	288	4	2003-05-01 00:00:00	1980	73.889999	1700	4399	0	ALACRAN 	Arrivé à Abidjan au début mai 2003 sous pavillon de BELIZE	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681318#0.9124101457296595	1	2009-04-15	714	288	4	2003-06-01 00:00:00	1980	73.889999	1700	4399	0	BERMEOTARAK CUATRO	Passé sous pavillon ghanéen au mois de juin 2003	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681326#0.9229208311217805	1	2009-04-15	715	542	4	\N	0	38.299999	0	0	0	KERMANTXO	Capacité de transport avoisinant 150 tonnes. Jauge brute 220 Tx. Signalé le 4/06/2003	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681335#0.7846381812304221	1	2009-04-15	716	543	4	\N	0	36	0	0	0	IRIBAR ZULAIKA	Capacité de transport de 100 à 130 tonnes. Jauge brute 148 Tx. Signalé le 4/06/2003	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681343#0.07284380612198582	1	2009-04-15	717	544	4	\N	0	40	0	0	0	AITA FRAXKU	Capacité de transport de 140 tonnes. Jauge brute de 213 Tx. Signalé le 12/06/2003	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681352#0.541344268591028	1	2009-04-15	718	545	14	\N	0	65	0	0	0	PANOFI FRONTIER	Capacité de transport de 800 tonnes. Signalé le 6/06/2003.	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681361#0.37870399337964633	1	2009-04-15	719	546	14	2003-06-04 00:00:00	1988	56.639999	1163	3000	14.5	PANOFI MASTER	Aperçu à Abidjan le 03/08/03 pour la première fois.	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681371#0.15207421937504761	1	2009-04-15	720	547	14	\N	1985	64	1100	2800	0	PANOFI VOLUNTEER	aperçu à Abidjan le 20/08/03 pour la première fois.	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681379#0.5134761831108614	1	2009-04-15	721	283	4	2003-05-17 00:00:00	1980	77.32	1750	4399	0	INTERTUNA UNO	Ex 476	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681387#0.17876487241240768	1	2009-04-15	722	412	4	2003-05-17 00:00:00	1990	77.300003	1700	7944	0	INTERTUNA DOS	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681396#0.16210675559239762	1	2009-04-15	723	532	4	2003-05-17 00:00:00	2000	115	3250	0	0	INTERTUNA TRES	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681404#0.7164057641331766	1	2009-04-15	724	400	4	2003-08-13 00:00:00	1983	69.220001	1282	4080	0	MONTELAPE	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681413#0.30493426047028616	1	2009-04-15	725	401	4	2003-08-13 00:00:00	1982	69.220001	1282	4080	0	MONTEALEGRE	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681540#0.7981536081474646	1	2009-04-15	739	503	4	2004-05-03 00:00:00	1999	112.56	2966	6000	19	ARTZA	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681558#0.695642156104691	1	2009-04-15	741	553	4	\N	2004	35.5	160	0	0	BERRIZ SAN FRANCISCO	JB = 160 Tx et l= 8.40 m	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681566#0.9399368884847806	1	2009-04-15	742	554	4	\N	2004	106.5	2900	7948	0	TXORI ARGI	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681575#0.7076052113702495	1	2009-04-15	743	555	4	\N	2004	116	3250	5850	0	ALBATUN DOS	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681584#0.49928011515311976	1	2009-04-15	744	556	4	\N	2004	30.5	0	0	0	SIEMPRE ARTURO	JB 149.57 Tx, l = 7.20 m et CT = 120 à 125 Tonnes.	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681592#0.4767504634227464	1	2009-04-15	745	421	4	2004-11-15 00:00:00	1990	105	3500	8157	0	INTERTUNA CUATRO	Seul classe 8 en 1991	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681601#0.7791909830022049	1	2009-04-15	746	273	4	2005-01-01 00:00:00	1980	76.760002	1900	4400	0	MONTECELO	1980 retour de O Indien 	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681610#0.4512006309890385	1	2009-04-15	747	317	4	2005-01-01 00:00:00	1983	76.760002	1850	4399	0	MONTEFRISA 9	?	fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681619#0.007558357046135389	1	2009-04-15	748	557	4	\N	2004	116	3095	7995	19.5	ALBATUN TRES	Premier débarquement à Victoria début février 2005	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681628#0.18500799507487897	1	2009-04-15	749	558	14	\N	1975	47.25	0	1600	0	TRUST 77	premier débarquement à Abidjan en 02/05	fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681638#0.09938899532494527	1	2009-04-15	750	559	4	\N	2004	106.5	2900	7956	18.5	IZURDIA	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681647#0.4764508883707348	1	2009-04-15	751	157	2	2005-01-01 00:00:00	1970	36.75	0	950	0	RAMATOULAYE	TJB 288.49 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681655#0.90222125648527	1	2009-04-15	752	500	2	2005-04-01 00:00:00	2000	39.549999	250	800	0	LIO 1	 Capacité de transport 180 tonnes (soit 250 m3 approximativement) Ex bateau 627, 646. TJB 293 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681669#0.37793409011299905	1	2009-04-15	753	501	2	2005-04-01 00:00:00	2000	39.549999	250	800	0	LIO 2	 Capacité de transport 180 tonnes (soit 250 m3 approximativement) Ex bateau 628, 647. TJB 293 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681678#0.7350560547768495	1	2009-04-15	755	556	4	\N	2004	30.5	0	0	0	GAZTELUGAITZ	JB 149.57 Tx, l = 7.20 m et CT = 120 à 125 Tonnes. Changement de nom en juillet 2005	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
fr.ird.observe.entities.referentiel.Bateau#1239832681778#0.5460423065684507	1	2009-04-15	766	453	2	2006-08-01 00:00:00	1991	0	0	710	0	ETOILE DES MERS	Chgt de pavillon au 01/08/2006 : ex Venezuela (code 761)	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381
fr.ird.observe.entities.referentiel.Bateau#1239832681795#0.8962154534608345	1	2009-04-15	768	68	1	1978-06-01 00:00:00	1957	26	105	300	0	AIGLE DES MERS	Ex bat 68 - Modif de structure : Passage de la glace à la congélation, augmentation de la capacité de transport	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832681804#0.26285104096909484	1	2009-04-15	769	278	1	\N	1981	69	1250	3900	13	LE TITAN	ex code bateau 305 - Changement de nom suite à un changement de propriétaire à partir du 20/10/2006	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681813#0.7036834769917802	1	2009-04-15	770	564	1	\N	2006	84.099998	1527	5440	17.700001	DRENNEC	JB 2319 Tx	fr.ird.observe.entities.referentiel.Pays#1239832675597#0.708958027082267	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613
fr.ird.observe.entities.referentiel.Bateau#1239832681822#0.6020495477314077	1	2009-04-15	771	297	1	\N	1983	70.5	1380	3600	13	TAKAMAKA	Ex " Kersaint de Coetnempren" code 327 ; changement d'armateur (SAPMER) 	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597
fr.ird.observe.entities.referentiel.Bateau#1239832681831#0.7770818629963822	1	2009-04-15	772	253	14	2006-11-04 00:00:00	1979	54.009998	675	2400	0	AVRA	Changement de nom et changement de pavillon (devient pavillon Guinée Conakry) à partir du 04/11/2006	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.9573433532024453	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586
fr.ird.observe.entities.referentiel.Bateau#1239832681840#0.9528188292649812	1	2009-04-15	773	565	4	\N	2007	27	464	460	0	AGURTZA BERRIA	Largeur = 7.20 m ; il assistera le MATXIKORTA et le KURTZIO	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832681848#0.3058298007269634	1	2009-04-15	774	566	34	\N	2007	0	0	0	0	BRAGO	?	fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851
fr.ird.observe.entities.referentiel.Bateau#1239832681857#0.13376266503898582	1	2009-04-15	775	567	4	\N	2007	95.800003	2250	6500	0	TXORI GORRI	?	fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267
fr.ird.observe.entities.referentiel.Bateau#1239832681866#0.7665077120574486	1	2009-04-15	776	568	1	\N	2005	41.919998	160	1088	0	PROVENCE COTE D AZUR II	Jauge Brute : 313 UMS - Jauge nette : 93 UMS - Capacité de réfrigération et congélation : 80 tonnes	fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813
\.


--
-- Data for Name: calee; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY calee (topiaid, topiaversion, topiacreatedate, heuredebut, heurefincoulissage, heurefin, profondeurmaximumengin, vitessecourant, directioncourant, profondeursommetbanc, profondeurmoyennebanc, epaisseurbanc, utilisationsonar, nomsupply, rejetthon, rejetfaune, commentaire, causecoupnul) FROM stdin;
\.


--
-- Data for Name: capturefaune; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY capturefaune (topiaid, topiaversion, topiacreatedate, poidsestime, nombreestime, poidsmoyen, taillemoyenne, commentaire, raisonrejet, espece, devenirfaune, calee) FROM stdin;
\.


--
-- Data for Name: capturefaunecalcule; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY capturefaunecalcule (topiaid, topiaversion, topiacreatedate, poidsestime, nombreestime, poidsmoyen, taillemoyenne, commentaire, raisonrejet, especefaune, devenirfaune, taillepoidsfaune, calee) FROM stdin;
\.


--
-- Data for Name: capturethon; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY capturethon (topiaid, topiaversion, cuve, rejete, calee, categoriepoids, raisonrejet, topiacreatedate, poids, surlepont, commentaire) FROM stdin;
\.


--
-- Data for Name: categoriebateau; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY categoriebateau (topiaid, topiaversion, topiacreatedate, code, libellejauge, libellecapacite) FROM stdin;
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.9745941347934848	1	2009-04-15	1	- 95 TX	20 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8706856200939851	1	2009-04-15	2	95 - 189 TX	40 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675540#0.8075885994998813	1	2009-04-15	3	190 - 299 TX	90 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675541#0.06272117478600381	1	2009-04-15	4	300- 449 TX	200 - 400 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.13575801400098586	1	2009-04-15	5	450 - 749 TX	401 - 600 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675542#0.750774668545597	1	2009-04-15	6	750 - 1249 TX	601 - 800 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.18210832858904613	1	2009-04-15	7	1250 - 2300 TX	801 - 1200 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.35095194502894267	1	2009-04-15	8	 + 2300 TX	1201 -  1400 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.5972664139548473	1	2009-04-15	9	 + 2300 TX	1401 - 1600 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675543#0.4203494866534334	1	2009-04-15	10	 + 2300 TX	1601 - 1800 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675544#0.22787925329212033	1	2009-04-15	11	 + 2300 TX	1801 - 2000 tonnes
fr.ird.observe.entities.referentiel.CategorieBateau#1239832675544#0.9700962746308415	1	2009-04-15	12	 + 2300 TX	> 2000 tonnes
\.


--
-- Data for Name: categoriepoids; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY categoriepoids (topiaid, topiaversion, topiacreatedate, code, libelle, espece) FROM stdin;
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685667#0.6336269826738086	8	2009-04-15	2	Ravil > 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685563#0.737149325111727	7	2009-04-15	3	Ravil de 1.8 à 4 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685569#0.04233285910464413	7	2009-04-15	4	Ravil de 1.8 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685575#0.38737292024847103	7	2009-04-15	5	Ravil de 4 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685581#0.9988011739485506	7	2009-04-15	6	Ravil de 4 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685597#0.8275351743804099	6	2009-04-15	7	Ravil de 6 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685603#0.24359172513166727	7	2009-04-15	8	Ravil > 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685610#0.04119062512809801	7	2009-04-15	9	Ravil de taille inconnue	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685673#0.3163596616269717	1	2009-04-15	3	Listao de 1.8 à 4 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685679#0.019733673419385367	1	2009-04-15	4	Listao de 1.8 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685685#0.7315998797178913	1	2009-04-15	5	Listao de 4 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685692#0.5940916998227946	1	2009-04-15	6	Listao de 4 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685722#0.9714869893887224	1	2009-04-15	7	Listao de 6 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685729#0.0015801959709714763	1	2009-04-15	8	Listao > 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685741#0.9084571608340902	1	2009-04-15	9	Listao de taille inconnue	fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685747#0.8903018444376307	1	2009-04-15	1	Patudo < 3 kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685754#0.10338259022397256	1	2009-04-15	2	Patudo de 3 à 10 kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685760#0.5190060347627616	1	2009-04-15	3	Patudo de 11 à 30 kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685767#0.17985459818605176	1	2009-04-15	4	Patudo de 3 à 30 kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685773#0.36018870624305044	1	2009-04-15	5	Patudo de 31 à 50 kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685779#0.6459697632929102	1	2009-04-15	6	Patudo de 11 à 50 kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685785#0.017557737970281928	1	2009-04-15	7	Patudo > 50 kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685791#0.96882088900269	1	2009-04-15	8	Patudo > 10kg	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685798#0.8205778575049493	1	2009-04-15	9	Patudo de taille inconnue	fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685804#0.034641865339321565	1	2009-04-15	1	Germon < 3 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685810#0.9054833697032716	1	2009-04-15	2	Germon de 3 à 10 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685818#0.5146901232402814	1	2009-04-15	3	Germon de 11 à 30 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685824#0.30572839692068965	1	2009-04-15	4	Germon de 3 à 30 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685830#0.3505424853460628	1	2009-04-15	5	Germon de 31 à 50 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685837#0.9977145363359975	1	2009-04-15	6	Germon de 11 à 50 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685843#0.5227248075951917	1	2009-04-15	8	Germon > 10kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685849#0.25030425638120324	1	2009-04-15	9	Germon de taille inconnue	fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685856#0.37681463866680454	1	2009-04-15	1	Auxide Bonitou < 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685862#0.3577261083569382	1	2009-04-15	2	Auxide Bonitou > 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685869#0.1577912646543379	1	2009-04-15	3	Auxide Bonitou de 1.8 à 4 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685876#0.571764466269097	1	2009-04-15	4	Auxide Bonitou de 1.8 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685882#0.6060344598115799	1	2009-04-15	5	Auxide Bonitou de 4 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685888#0.04548287749163349	1	2009-04-15	6	Auxide Bonitou de 4 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685894#0.7596812507048669	1	2009-04-15	7	Auxide Bonitou de 6 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685901#0.9401665182005969	1	2009-04-15	8	Auxide Bonitou > 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685907#0.703864325385401	1	2009-04-15	9	Auxide Bonitou de taille inconnu	fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685913#0.6908151762213386	1	2009-04-15	1	Auxide < 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685919#0.32661937164161003	1	2009-04-15	2	Auxide > 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685926#0.26263537384431435	1	2009-04-15	3	Auxide de 1.8 à 4 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685932#0.05603149764956994	1	2009-04-15	4	Auxide de 1.8 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685938#0.1289482411270122	1	2009-04-15	5	Auxide de 4 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685944#0.6386905101076812	1	2009-04-15	6	Auxide de 4 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685950#0.5552574879416816	1	2009-04-15	7	Auxide de 6 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685956#0.8040582347597208	1	2009-04-15	8	Auxide > 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685962#0.5585432399656218	1	2009-04-15	9	Auxide de taille inconnue	fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685968#0.8743676757879854	1	2009-04-15	1	Ravil < 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685975#0.9269185705341096	1	2009-04-15	2	Ravil > 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685981#0.20973946035734992	1	2009-04-15	3	Ravil de 1.8 à 4 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685987#0.8124900788304172	1	2009-04-15	4	Ravil de 1.8 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.WeightCategory#1239832685993#0.7110149379733451	1	2009-04-15	5	Ravil de 4 à 6 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685999#0.35760983661297885	1	2009-04-15	6	Ravil de 4 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832686006#0.27918502949209356	1	2009-04-15	7	Ravil de 6 à 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832686012#0.3678254910247588	1	2009-04-15	8	Ravil > 8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832686018#0.19418668665886385	1	2009-04-15	9	Ravil de taille inconnue	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685499#0.45137386216494235	1	2009-04-15	1	Albacore < 3 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.8943253454598569
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685505#0.6893474228813191	1	2009-04-15	2	Albacore de 3 à 10 kg	fr.ird.observe.entities.referentiel.Species#1239832685474#0.8943253454598569
fr.ird.observe.entities.referentiel.seine.WeightCategory#1239832685636#0.8720899036293043	8	2009-04-15	1	Ravil < 1.8 kg	fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615
\.


--
-- Data for Name: causecoupnul; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY causecoupnul (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683529#0.10743661058036036	1	2009-04-15	0	Inconnue	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683530#0.1763093301083687	1	2009-04-15	1	Poisson ayant coulé	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683530#0.20948361090350653	1	2009-04-15	2	Poisson allant trop vite	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683530#0.5654682887841957	1	2009-04-15	3	Courant trop fort	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683530#0.9133658836856285	1	2009-04-15	4	Trop de poisson	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683531#0.5326433760483198	1	2009-04-15	5	Filet déchiré	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683532#0.14897611809028388	1	2009-04-15	6	Panne de treuil	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683532#0.10780065373945669	1	2009-04-15	7	Mauvais temps	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683532#0.06913259265099414	1	2009-04-15	8	Echappement de la baleine et le poisson suit	f
fr.ird.observe.entities.referentiel.CauseCoupNul#1239832683532#0.9435364267696991	1	2009-04-15	9	Autres ( à préciser dans les notes )	t
\.


--
-- Data for Name: causenoncoupsenne; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY causenoncoupsenne (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683550#0.5793978455280235	1	2009-04-15	0	Rien à signaler ( pas d'observat
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683551#0.9835503687831603	1	2009-04-15	1	Banc trop petit
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683551#0.44213405530673244	1	2009-04-15	2	Poissons trop petits ( poids, ta
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683551#0.18973682980311035	1	2009-04-15	3	Par décision de l'armateur ( ex:
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683552#0.06665978495065894	1	2009-04-15	4	Se déplace trop rapidement
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683553#0.9488710902970946	1	2009-04-15	5	Le poisson plonge avant la calée
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683553#0.38837795129071284	1	2009-04-15	6	Trop profond ( détecté par le so
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683553#0.30600894871538153	1	2009-04-15	7	Observation sans poisson
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683553#0.40849514526979613	1	2009-04-15	8	Forts courants
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683553#0.8925023551900819	1	2009-04-15	9	Avarie mécanique
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683553#0.8532700555518433	1	2009-04-15	10	Un autre bateau encercle le banc
fr.ird.observe.entities.referentiel.CauseNonCoupSenne#1239832683553#0.11808971412238256	1	2009-04-15	99	Autres ( à préciser dans les not
\.


--
-- Data for Name: devenirfaune; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY devenirfaune (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683618#0.06155887805368032	1	2009-04-15	1	echappe du filet ( pour requin-b	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683619#0.9931091059863436	1	2009-04-15	2	Sortie vivant du filet ( pour re	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683619#0.11883784875534997	1	2009-04-15	3	Sortie mort du filet ( pour requ	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683619#0.5308862132841506	1	2009-04-15	4	Rejeté vivant à la mer	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683619#0.6250731662108877	1	2009-04-15	5	Rejeté mort à la mer	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683619#0.5722739932065866	1	2009-04-15	6	Mis en cuve	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683620#0.46609703818634485	1	2009-04-15	7	Partiellement conservé ( ex: ail	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683621#0.6728026426066158	1	2009-04-15	8	Utilisé en cuisine du bord	f
fr.ird.observe.entities.referentiel.DevenirFaune#1239832683621#0.9099804284263154	1	2009-04-15	9	Autres ( à préciser dans les notes )	t
\.


--
-- Data for Name: devenirobjet; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY devenirobjet (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.DevenirObjet#1239832683674#0.32469201752917276	1	2009-04-15	1	Abandonné	f
fr.ird.observe.entities.referentiel.DevenirObjet#1239832683675#0.08083860336717841	1	2009-04-15	2	Pose d'une balise	f
fr.ird.observe.entities.referentiel.DevenirObjet#1239832683675#0.7559688295127481	1	2009-04-15	3	Remonté à bord	f
fr.ird.observe.entities.referentiel.DevenirObjet#1239832683675#0.88526017739943	1	2009-04-15	4	Détruit	f
fr.ird.observe.entities.referentiel.DevenirObjet#1239832683675#0.190123844350496	1	2009-04-15	5	Coulé	f
fr.ird.observe.entities.referentiel.DevenirObjet#1239832683675#0.24693458404051094	1	2009-04-15	9	Autre ( à préciser dans les notes )	t
\.


--
-- Data for Name: echantillonfaune; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY echantillonfaune (topiaid, topiaversion, topiacreatedate, commentaire, calee) FROM stdin;
\.


--
-- Data for Name: echantillonthon; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY echantillonthon (topiaid, topiaversion, topiacreatedate, nature, commentaire, calee) FROM stdin;
\.


--
-- Data for Name: especefaune; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY especefaune (topiaid, topiaversion, topiacreatedate, code, code3l, libellescientifique, libellefr, libellees, libelleuk, needcomment, groupe) FROM stdin;
fr.ird.observe.entities.referentiel.Species#1239832683934#0.6772566681424695	1	2009-04-15	211	FCA	Carcharhinidae sp.	Famille Carcharhinidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683940#0.07573448831638663	1	2009-04-15	212	FDA	Dasyatidae sp.	Famille Dasyatidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683946#0.08647986431898924	1	2009-04-15	213	FLA	Lamnidae sp.	Famille Lamnidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683951#0.07631086898797512	1	2009-04-15	214	FRH	Rhiniodontidae sp.	Famille Rhiniodontidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683979#0.7849257703362092	1	2009-04-15	215	FSP	Sphyrnidae sp.	Famille Sphyrnidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683985#0.48335145439788996	1	2009-04-15	216	GCU	Galeocerdo cuvieri	Requin tigre commun	Tintorera		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683991#0.18251172983518738	1	2009-04-15	217	IBR	Isistius brasiliensis	Squalelet féroce	Tollo cigarro	Cookie cutter shark	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683996#0.008204938561974351	1	2009-04-15	218	IOX	Isurus oxyrinchus	Taupe bleu	Marrajo dientuso	Shortfin mako	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684030#0.5103072200573924	1	2009-04-15	219	MAQ	Myliobatis aquila	Aigle commun	Aguila marina	Common eagle ray	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684036#0.959050780883495	1	2009-04-15	220	MBA	Manta birostris	Mante atlantique	Manta gigante	Giant manta	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684041#0.017218753201439285	1	2009-04-15	221	MCO	Mobula coilloti				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684047#0.060538310241706084	1	2009-04-15	222	MOM	Mobula mobula		Manta		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684079#0.00928072353834386	1	2009-04-15	223	MPE	Megachasma pelagios	Requin grande gueule	Tiburón bocudo	Megamouth shark	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684085#0.7869093021514663	1	2009-04-15	224	MRA	Mobula rancurelli				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684091#0.041169428365426364	1	2009-04-15	225	OCA	xx	Ordre Carcharhiniformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684109#0.8386577306728308	1	2009-04-15	226	OHT	xx	Ordre Heterodontiformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684115#0.8532070129016956	1	2009-04-15	227	OHX	xx	Ordre Hexanchiformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684121#0.31855037007349263	1	2009-04-15	228	OLA	xx	Ordre Lamniformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684126#0.8236547673863684	1	2009-04-15	229	OOE	xx	Ordre Orectolobiformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684152#0.6225981327485742	1	2009-04-15	230	OPR	xx	Ordre Pristiophoriformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684163#0.3961514127062187	1	2009-04-15	232	OST	xx	Ordre Squatiniformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684483#0.7342362968091883	1	2009-04-15	309	BEA	Ablennesse hians				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684489#0.9534225034717015	1	2009-04-15	310	BON	Sarda sarda	Bonite à dos rayé	Bonito del Atlántico	Atlantic bonito	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684519#0.10519861144934894	1	2009-04-15	311	BRA	xx	Famille Bramidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684525#0.5397109556051357	1	2009-04-15	312	CLM	Decapterus macarellus		Macarela caballa	Mackerel scad	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684531#0.01962657179799221	1	2009-04-15	313	COE	Coryphaena equiselis	Coryphène dauphin	Dorado	Pompano dolphinfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684537#0.2397229787936519	1	2009-04-15	314	COH	Coryphaena hippurus	Coryphène commun	Lampuga /Dorado común	Common dolphinfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684543#0.8674149166335942	1	2009-04-15	315	COI	Carangoides orthogrammus			Island trevally	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684549#0.4997270804575126	1	2009-04-15	316	CRS	Caranx sexfasciatus			Bigeye trevally	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684579#0.8633684859727454	1	2009-04-15	317	CRY	Caranx crysos		Cojinúa negra	Blue runner	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684585#0.5554464036993899	1	2009-04-15	318	CUH	Uraspis helvola				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684612#0.24453311934325705	1	2009-04-15	319	CUS	Uraspis secunda		Jurel volantín	Cottonmouth jack	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684617#0.3170644296156072	1	2009-04-15	320	CUX	Uraspis sp.				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684632#0.6056515174598422	1	2009-04-15	321	DIH	Diodon hystrix		Pez erizo	Spot-fin porcupinefish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684638#0.8261519152704289	1	2009-04-15	322	DIO	Diodon sp.	Famille Diodontidae (Indien)			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684644#0.6230088204735412	1	2009-04-15	323	ELP	Elagatis bipinnulata	Commère saumon	Macarela salmón	Rainbow runner	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684650#0.7391574204799725	1	2009-04-15	324	EPL	Phtheirichthys lineatus			Slender suckerfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684676#0.2890210388179896	1	2009-04-15	325	FBA	xx	Famille Balistidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684682#0.41031752407171773	1	2009-04-15	326	FBL	xx	Famille Belonidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684688#0.8193074495746601	1	2009-04-15	327	FCO	xx	Famille Coryphaenidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684694#0.11252990999137946	1	2009-04-15	328	FCR	xx	Famille Carangidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684710#0.8543374844909662	1	2009-04-15	330	FEP	xx	Famille Ephippidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684843#0.7390723511555113	1	2009-04-15	351	NAD	Naucrates ductor	Poisson pilote	Pez piloto	Pilotfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684849#0.0923785524882147	1	2009-04-15	352	PLS	Platax sp.				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684855#0.08481411600261735	1	2009-04-15	353	PLT	Platax teira			Longfin batfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684861#0.36396067375732244	1	2009-04-15	354	RAL	Ranzania laevis			Slender sunfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684867#0.6198135091590454	1	2009-04-15	355	RAU	Remora australis				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684873#0.9434547986085544	1	2009-04-15	356	REA	Remorina albescens			White suckerfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684879#0.08604217023705107	1	2009-04-15	357	REM	Remora remora	Rémora	Rémora	Shark sucker	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684885#0.5770224986830752	1	2009-04-15	358	RUP	Ruvettus pretiosus	Rouvet	Escolar clavo	Oilfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684892#0.9272035028442585	1	2009-04-15	359	SER	Seriola rivoliana		Medregal limón	Longfin yellowtail	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684898#0.9265687425163222	1	2009-04-15	360	SJA	Scomber japonicus	Maquereau espagnol	Estornino	Chub mackerel	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684904#0.7999183854146987	1	2009-04-15	361	SPB	Sphyraena barracuda	Barracuda	Picuda barracuda	Great barracuda	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684910#0.7216156517919907	1	2009-04-15	362	SPH	xx	Famille Sphyraenidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684916#0.7058605960700699	1	2009-04-15	363	SSC	Scomber scombrus	Maquereau commun	Caballa del Atlántico	Atlantic mackerel	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684922#0.7105602395040097	1	2009-04-15	364	TCC	Tylosurus crocodilus			Hound needlefish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684928#0.20119509838828453	1	2009-04-15	365	WAH	Acanthocybium solandri	Thazard bâtard	Peto	Wahoo	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684934#0.09319660964127574	1	2009-04-15	366	ZAC	Zanclus cornutus			Moorish idol	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684940#0.3941475292396889	1	2009-04-15	401	CCC	Caretta caretta	Tortue caouane	Tortuga boba  / caguama	Loggerhead turtle	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832684946#0.29129778706560716	1	2009-04-15	402	CMM	Chelonia mydas	Tortue verte	Tortuga verde	Green turtle	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832684953#0.5908437547687754	1	2009-04-15	403	DCC	Dermochelys coriacea	Tortue luth	Tortuga laud	Leatherback turtle	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832684959#0.8280432216962142	1	2009-04-15	404	EIM	Eretmochelys imbricata	Tortue imbriquée	Tortuga carey	Hawksbill turtle	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832684971#0.8622121191938249	1	2009-04-15	406	LOL	Lepidochelis olivacea	Tortue Ridley	Tortuga bastarda / golfina		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832685107#0.34538015098950847	1	2009-04-15	519	ODO	xx	Odontocète non identifié	Odontoceto no identificado		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685113#0.7014499115802707	1	2009-04-15	520	OOR	Orcinus orca	Orque	Orca	Killer whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685119#0.05669776907577062	1	2009-04-15	521	PCR	Pseudorca crassidens	Faux orque	Orca falsa	False killer whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685161#0.28093299934924054	1	2009-04-15	522	PEP	Peponocephala electra	Péponocéphale	Calderón pequeño	Melon-headed whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685167#0.7434905579765678	1	2009-04-15	523	PMA	Physeter macrocephalus	Cachalot	Cachalote	Sperm whale/cachalot	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685173#0.8611327537175576	1	2009-04-15	524	SAT	Stenella attenuata	Dauphin tacheté pantropical	Estenela moteada	Pantropical spotted dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685190#0.134249199404346	1	2009-04-15	525	SBR	Steno bredanensis	Sténo	Esteno	Rough-toothed dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685196#0.7901391722300486	1	2009-04-15	526	SCL	Stenella clymene	Dauphin de clymene	Delfín clymene	Clymene dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685202#0.7616513963317285	1	2009-04-15	527	SCO	Stenella coeruleoalba	Dauphin bleu et blanc	Estenela listada	Striped dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685208#0.49030599026931276	1	2009-04-15	528	SFR	Stenella frontalis	Dauphin tacheté Atlantique	Delfín pintado	Atlantic spotted dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685226#0.9274141847765787	1	2009-04-15	529	SLO	Stenella longirostris	Dauphin à long bec	Estenela giradora	Spinner dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685233#0.9753172599708958	1	2009-04-15	530	STE	xx	Dauphin non identifié	Delfín no identificado	Dolphin not identified	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685240#0.16727135240325364	1	2009-04-15	531	TTR	Tursiops truncatus	Grand dauphin	Tursion	Bottlenose dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685246#0.6691444658155139	1	2009-04-15	532	ZCA	Ziphius cavirostris	Zyphius	Zifio de Cuvier	Cuvier's beaked whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685252#0.4730616548156924	1	2009-04-15	999	XXX	A préciser	xxx	A préciser	xxx	t	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.15109899841418006
fr.ird.observe.entities.referentiel.Species#1239832683725#0.39445809291491807	1	2009-04-15	101	BLM	Makaira indica	Makaire noir	Aguja negra	Black marlin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683731#0.3892121873590658	1	2009-04-15	102	BUM	Makaira nigricans	Makaire bleu	Aguja azul del Atlántico	Atlantic blue marlin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683737#0.9054429876194163	1	2009-04-15	103	FIS	xx	Famille des Istiophoridés			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683743#0.8616022446692306	1	2009-04-15	104	SAI	Istiophorus albicans	Voilier de l'Atlantique	Pez vela del Atlántico	Atlantic sailfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683748#0.9215143239720961	1	2009-04-15	105	SAP	Istiophorus platypterus	Voilier de l'Océan Indien	Pez vela del Indo-Pacífico	Indo-Pacific sailfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683774#0.3664090332253188	1	2009-04-15	106	SHS	Tetrapturus angustirostris	Makaire à rostre court	Marlín trompa corta	Shortbill spearfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683780#0.0859039531908029	1	2009-04-15	107	SPF	Tetrapturus pfluegeri	Makaire bécune	Aguja picuda	Longbill spearfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683785#0.49501050869628815	1	2009-04-15	108	STM	Tetrapturus audax	Marlin rayé	Marlín rayado	Striped marlin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683791#0.20975568563021063	1	2009-04-15	109	SWO	Xiphias gladius	Espadon	Pez espada	Swordfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683796#0.28890729732639275	1	2009-04-15	110	WHM	Tetrapterus albidus	Makaire blanc	Aguja blanca		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002
fr.ird.observe.entities.referentiel.Species#1239832683830#0.6643682548910844	1	2009-04-15	201	ANA	Aetobatus narinari	Aigle de mer léopard	Chucho pintado	Spotted eagle ray	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683835#0.03404189793534684	1	2009-04-15	202	APE	Alopias pelagicus	Renard pélagique	Zorro pelágico	Pelagic thresher	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683841#0.25560065674558863	1	2009-04-15	203	ASU	Alopias superciliosus	Renard à gros yeux	Zorro ojón	Bigeye thresher	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683846#0.9864285434180587	1	2009-04-15	204	AVU	Alopias vulpinus	Renard	Zorro	Thresher	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683861#0.7598003083421893	1	2009-04-15	205	CCA	Carcharodon carcharias	Grand requin blanc	Jaquetón blanco	Great white shark	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683867#0.5251675316716491	1	2009-04-15	206	CFA	Carcharhinus falciformis	Requin soyeux	Tiburón jaquetón	Silky shark	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683883#0.6583610159371419	1	2009-04-15	207	CLO	Carcharhinus longimanus	Requin océanique	Tiburón oceánico	Oceanic whitetip shark	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683888#0.6223394488789843	1	2009-04-15	208	DVI	Dasyatis violacea	Pastenague	Chucho	Pelagic stingray	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683895#0.07544360746392853	1	2009-04-15	209	ETM	Etmopterus sp.	Genre Etmopterus			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832683921#0.2103636331742268	1	2009-04-15	210	FAL	Alopiidae sp.	Famille Alopiidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684157#0.9835191073325458	1	2009-04-15	231	OSR	xx	Ordre Squaliformes			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684201#0.30554150296793947	1	2009-04-15	233	PGL	Prionace glauca	Peau bleue	Tiburón azul	Blue shark	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684207#0.6586381203701787	1	2009-04-15	234	POR	Lamna nasus	Requin taupe commun	Marrajo sardinero	Porbeagle	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684213#0.644772725177114	1	2009-04-15	235	RAX	xx	Raie non identifiée			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684219#0.36964121735214706	1	2009-04-15	236	REX	xx	Requin non identifié			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684225#0.567526382162922	1	2009-04-15	237	RHI	Rhinopteridae sp.	Famille Rhinopteridae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684254#0.9942181924044194	1	2009-04-15	238	RHS	Rhinoptera sp.	Mourine			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684279#0.3790726155671563	1	2009-04-15	239	RMV	Mobula sp.	Mante sp.			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684284#0.6799191748475211	1	2009-04-15	240	RPE	Cetorhinus maximus	Requin pèlerin	Peregrino	Basking shark	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684290#0.04680507324710936	1	2009-04-15	241	RTY	Rhinodion typus	Requin baleine	Tiburón ballena		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684296#0.3383076607094946	1	2009-04-15	242	SLE	Sphyrna lewini	Requin marteau halicorne	Cornuda común	Scalloped hammerhead	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684327#0.8415482323514992	1	2009-04-15	243	SMO	Sphyrna mokarran	Grand requin marteau	Cornuda gigante	Great hammerhead	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684332#0.5394989374753841	1	2009-04-15	244	SZY	Sphyrna zygaena	Requin marteau commun	Cornuda cruz(=Pez martillo)	Smooth hammerhead	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075
fr.ird.observe.entities.referentiel.Species#1239832684338#0.39005276952353063	1	2009-04-15	301	AVA	Abudefduf vaigiensis				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684402#0.6920019339255605	1	2009-04-15	302	BAC	Balistes capriscus	Baliste (Atlantique)			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684408#0.2795612752795422	1	2009-04-15	303	BAE	Abalistes stellatus	Baliste étoilé	Pejepuerco estrellado		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684414#0.45779952398671275	1	2009-04-15	304	BAL	Balistes carolinensis		Pejepuerco blanco	Grey triggerfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684432#0.291745731612848	1	2009-04-15	305	BAP	Balistes punctatus	Baliste à taches bleues	Pejepuerco moteado		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684439#0.9130769003657221	1	2009-04-15	306	BAS	Aluterus scriptus			Scribbled leatherjacket filefi	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684445#0.007584405154870022	1	2009-04-15	307	BAT	Aluterus monoceros			Unicorn leatherjacket filefish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684477#0.23037713658559167	1	2009-04-15	308	BCM	Canthidermis maculatus			Ocean triggerfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684704#0.949304260063034	1	2009-04-15	329	FEC	xx	Famille Echeneidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684716#0.08548163540381626	1	2009-04-15	331	FEV	Euleptorhamphosus velox				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684722#0.6842656474581286	1	2009-04-15	332	FEX	xx	Famille Exocoetidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684735#0.97180274753603	1	2009-04-15	333	FFI	xx	Famille Fistularidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684741#0.5830333217484074	1	2009-04-15	334	FKY	Kyphosus sp.				f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684747#0.2582166402211624	1	2009-04-15	335	FMO	xx	Famille Molidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684753#0.24544050963314223	1	2009-04-15	336	FPO	xx	Famille Pomacentridae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684759#0.7758435974255872	1	2009-04-15	337	FSC	xx	Famille Scombridae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684765#0.6659673830194481	1	2009-04-15	338	FSE	xx	Famille Serranidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684771#0.9223458397529432	1	2009-04-15	339	FTT	xx	Famille Tetraodontidae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684778#0.3373723808020953	1	2009-04-15	340	KPC	Kyphosus cinerascens		Chopa azul	Blue sea chub	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684784#0.40816402255662565	1	2009-04-15	341	KPS	Kyphosus sectator		Chopón		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684789#0.6250440529371997	1	2009-04-15	342	KPV	Kyphosus vaigiensis			Brassy chub	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684795#0.6053463019344153	1	2009-04-15	343	LAG	Lampris guttatus	Opah	Opa	Opah	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684801#0.019864694251475634	1	2009-04-15	344	LAM	xx	Famille Lampridae			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684807#0.8492371601281514	1	2009-04-15	345	LLA	Lagocephalus lagocephalus			Oceanic puffer	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684813#0.23032822909041595	1	2009-04-15	346	LOB	Lobotes surinamensis		Dormilona	Tripletail	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684819#0.5710822650999473	1	2009-04-15	347	MAL	Masturus lanceolatus		Pez luna	Sharptail mola	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684825#0.6351897087012173	1	2009-04-15	348	MAW	Scomberomorus tritor	Thazard blanc	Carite lusitánico	West African Spanish mackerel	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684831#0.9665297005737801	1	2009-04-15	349	MMO	Mola mola	Poisson lune	Pez luna	Ocean sunfish	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684837#0.36870634574294836	1	2009-04-15	350	MZZ	xx	Autre poisson non identifié	Otros peces sin ident.		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995
fr.ird.observe.entities.referentiel.Species#1239832684965#0.23296430970714022	1	2009-04-15	405	LKE	Lepidochelis kempii	Tortue de Kemp	Tortuga de Kemp / lora		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832684977#0.5056399209950906	1	2009-04-15	407	TOE	xx	Tortue à écaille non identifiée			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832684983#0.38453892687871216	1	2009-04-15	408	TOX	xx	Tortue non identifiée			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461
fr.ird.observe.entities.referentiel.Species#1239832684989#0.8258827419292817	1	2009-04-15	501	BAC	Balaenoptera acutorostrata	Petit rorqual	Rorcual enano	Minke whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832684996#0.009762826302784466	1	2009-04-15	502	BBO	Balaenoptera borealis	Rorqual de Rudolphi	Rorcual del Norte	Sei whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685002#0.5743318174525757	1	2009-04-15	503	BED	Balaenoptera edeni	Rorqual de Bryde	Rorcual tropical	Bryde's whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685008#0.6701451796911269	1	2009-04-15	504	BMU	Balaenoptera musculus	Rorqual bleu	Ballena azul	Blue whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685014#0.4579522419131945	1	2009-04-15	505	BPH	Balaenoptera physalus	Rorqual commun	Rorcual común	Fin whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685020#0.7691470969492022	1	2009-04-15	506	CEX	xx	Cétacé non identifié	Cetáceo no identificado		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685027#0.9184457914427075	1	2009-04-15	507	DDE	Delphinus delphis	Dauphin commun	Delfín común	Common dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685033#0.6252495045228381	1	2009-04-15	508	FAT	Feresa attenuata	Orque pygmée	Orca pigmea	Pygmy killer whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685039#0.9885160092180597	1	2009-04-15	509	GGR	Grampus griseus	Dauphin de Risso	Delfín de Risso	Risso's dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685052#0.41637903112663477	1	2009-04-15	510	GMA	Globicephala macrorhynchus	Globicéphale tropical	Calderón de aletas cortas	Short-finned pilot whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685058#0.9819141520017292	1	2009-04-15	511	GME	Globicephala melas	Globicéphale commun	Calderón común	Long-finned pilot whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685064#0.6190226213740994	1	2009-04-15	512	KBR	Kogia breviceps	Cachalot pygmée	Cachalote pigmeo	Pygmy sperm whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685070#0.7476327950083782	1	2009-04-15	513	KSI	Kogia simus	Cachalot nain	Cachalote enano	Dwarf sperm whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685076#0.8699400186341582	1	2009-04-15	514	LHO	Lagenodelphis hosei	Dauphin de Fraser	Delfín de Fraser	Fraser's dolphin	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685082#0.866094293518799	1	2009-04-15	515	MDE	Mesoplodon densirostris	Mesoplodon de Blainville	Zifio de Blanville	Blainville's beaked whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685088#0.7126125616036756	1	2009-04-15	516	MEU	Mesoplodon europaeus	Mesoplodon de Gervais	Zifio de Gervais	Gervais' beaked whale	f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685094#0.8862082966884622	1	2009-04-15	517	MNO	Magaptera novaeangliae	Baleine à bosse			f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
fr.ird.observe.entities.referentiel.Species#1239832685100#0.2285189576701152	1	2009-04-15	518	MYS	xx	Mysticète non identifié	Misticeto no identificado		f	fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977
\.


--
-- Data for Name: especefauneobservee; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY especefauneobservee (topiaid, topiaversion, topiacreatedate, nombre, statut, espece, statutespece, objetflottant) FROM stdin;
\.


--
-- Data for Name: especethon; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY especethon (topiaid, topiaversion, topiacreatedate, code, code3l, libellescientifique, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.Species#1239832685474#0.8943253454598569	1	2009-04-15	1	YFT	Thunnus albacares	Albacore, thon à nageoires jaune	f
fr.ird.observe.entities.referentiel.Species#1239832685474#0.975344121171992	1	2009-04-15	2	SKJ	Katsuwonus pelamis	Listao, bonite à ventre rayé	f
fr.ird.observe.entities.referentiel.Species#1239832685475#0.13349466123905152	1	2009-04-15	3	BET	Thunnus obesus	Patudo, thon obèse, thon gros ye	f
fr.ird.observe.entities.referentiel.Species#1239832685476#0.5618871286604711	1	2009-04-15	4	ALB	Thunnus alalunga	Germon	f
fr.ird.observe.entities.referentiel.Species#1239832685476#0.36339915670317835	1	2009-04-15	5	BLT	Auxis rochei	Auxide, bonitou	f
fr.ird.observe.entities.referentiel.Species#1239832685477#0.3846921632590058	1	2009-04-15	6	FRI	Auxis thazard	Auxide	f
fr.ird.observe.entities.referentiel.Species#1239832685477#0.8024257002747615	1	2009-04-15	7	LTA	Euthynnus alleteratus	Thonine, ravil	f
fr.ird.observe.entities.referentiel.Species#1239832685477#0.5989181185528589	1	2009-04-15	8	FRZ	Auxis sp.	Auxis sp.	f
fr.ird.observe.entities.referentiel.Species#1239832685477#0.6417869140779674	1	2009-04-15	9	?	préciser dans les notes	Autre espèce	t
fr.ird.observe.entities.referentiel.Species#1239832685477#0.2673009297087321	1	2009-04-15	10	KAW	Euthynnus affinis	Thonine orientale, kawakawa	f
fr.ird.observe.entities.referentiel.Species#1239832685477#0.2908846499255108	1	2009-04-15	11	BLF	Thunnus atlanticus	Thon à nageoires noires	f
fr.ird.observe.entities.referentiel.Species#1239832685478#0.7676744877900202	1	2009-04-15	12	LOT	Thunnus tonggol	Thon mignon	f
fr.ird.observe.entities.referentiel.Species#1239832685478#0.35608154846055595	1	2009-04-15	13	?	Tout le banc	Tout le banc	f
\.


--
-- Data for Name: estimationbanc; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY estimationbanc (topiaid, topiaversion, calee, espece, topiacreatedate, poids, poidsindividuel) FROM stdin;
\.


--
-- Data for Name: estimationbancobjet; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY estimationbancobjet (topiaid, topiaversion, topiacreatedate, poids, espece, objetflottant) FROM stdin;
\.


--
-- Data for Name: groupeespecefaune; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY groupeespecefaune (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.17327457618202002	1	2009-04-15	1	Poissons Porte_Epée	f
fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.7120116158620075	1	2009-04-15	2	Sélaciens	f
fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683689#0.12092280503502995	1	2009-04-15	3	Autres poissons	f
fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.24333033683679461	1	2009-04-15	4	Tortues	f
fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.9204972827240977	1	2009-04-15	5	Cétacés	f
fr.ird.observe.entities.referentiel.GroupeEspeceFaune#1239832683690#0.15109899841418006	1	2009-04-15	9	A préciser	t
\.


--
-- Data for Name: maree; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY maree (topiaid, topiaversion, topiacreatedate, datedebut, datefin, organisme, datearriveeport, commentaire, niveauverification, open, ocean, senne, observateur, bateau, programme) FROM stdin;
\.


--
-- Data for Name: modedetection; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY modedetection (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.ModeDetection#1239832686121#0.31798673746139705	1	2009-04-15	0	Mode de détection inconnu	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686122#0.3243132538654122	1	2009-04-15	1	Oiel nu	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686122#0.6635325462393349	1	2009-04-15	2	Jumelles	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686122#0.11073840364502163	1	2009-04-15	3	Radar oiseaux	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686122#0.047471332037872016	1	2009-04-15	4	Radar	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686122#0.543003086001909	1	2009-04-15	5	Sonar	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686124#0.14361047153250828	1	2009-04-15	6	Sondeur	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686124#0.20721072808839902	1	2009-04-15	7	Bouée émettrice	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686124#0.4849472210706479	1	2009-04-15	8	Apparence signalée par un autre navire	f
fr.ird.observe.entities.referentiel.ModeDetection#1239832686124#0.7586824955387137	1	2009-04-15	9	Autres ( à préciser dans les notes )	t
\.


--
-- Data for Name: objetflottant; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY objetflottant (topiaid, topiaversion, topiacreatedate, nbjoureau, commentaire, nomsupply, appartenance, type, devenir, activite) FROM stdin;
\.


--
-- Data for Name: objetflottant_operation; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY objetflottant_operation (objetflottant, operation) FROM stdin;
\.


--
-- Data for Name: observateur; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY observateur (topiaid, topiaversion, topiacreatedate, identifiant, nom, prenom) FROM stdin;
fr.ird.observe.entities.referentiel.Observateur#1239832686142#0.19768626135103717	1	2009-04-15	0	Chemit	Tony
fr.ird.observe.entities.referentiel.Observateur#1239832686142#0.3576428053060211	1	2009-04-15	1	Poussin	Benjamin
fr.ird.observe.entities.referentiel.Observateur#1239832686142#0.4291040181630852	1	2009-04-15	2	Pot	Jack
\.


--
-- Data for Name: ocean; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY ocean (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.Ocean#1239832686151#0.17595105505051245	1	2009-04-15	1	Atlantique
fr.ird.observe.entities.referentiel.Ocean#1239832686152#0.8325731048817705	1	2009-04-15	2	Indien
fr.ird.observe.entities.referentiel.Ocean#1239832686152#0.7039171539191688	1	2009-04-15	3	Pacifique
\.


--
-- Data for Name: operationbalise; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY operationbalise (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.OperationBalise#1239832686237#0.4947461794167761	1	2009-04-15	1	Visite
fr.ird.observe.entities.referentiel.OperationBalise#1239832686238#0.38090479793636556	1	2009-04-15	2	Récuperation
fr.ird.observe.entities.referentiel.OperationBalise#1239832686238#0.4755624782839416	1	2009-04-15	3	Mise à l'eau
\.


--
-- Data for Name: operationobjet; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY operationobjet (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.OperationObjet#1239832686248#0.8669327599318251	1	2009-04-15	1	Mise à l'eau
fr.ird.observe.entities.referentiel.OperationObjet#1239832686249#0.8268884472438458	1	2009-04-15	2	Visite/Rencontre
fr.ird.observe.entities.referentiel.OperationObjet#1239832686249#0.8431519556575698	1	2009-04-15	3	Pêche
fr.ird.observe.entities.referentiel.OperationObjet#1239832686249#0.7838704130950722	1	2009-04-15	4	Récupération sans pêche
\.


--
-- Data for Name: pays; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY pays (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.Pays#1239832675590#0.35040962267355735	1	2009-04-15	12	Norvège
fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9558433552679892	1	2009-04-15	13	Cuba
fr.ird.observe.entities.referentiel.Pays#1239832675590#0.8642150406061876	1	2009-04-15	14	Ghana
fr.ird.observe.entities.referentiel.Pays#1239832675591#0.43427027828177345	1	2009-04-15	15	Congo
fr.ird.observe.entities.referentiel.Pays#1239832675591#0.4429520672460452	1	2009-04-15	16	Grand Caïman
fr.ird.observe.entities.referentiel.Pays#1239832675591#0.13220958420172946	1	2009-04-15	17	Mexique
fr.ird.observe.entities.referentiel.Pays#1239832675592#0.1719246898492357	1	2009-04-15	18	Vénézuela
fr.ird.observe.entities.referentiel.Pays#1239832675592#0.411796820475434	1	2009-04-15	19	Portugal
fr.ird.observe.entities.referentiel.Pays#1239832675592#0.17985901517657754	1	2009-04-15	20	U.R.S.S.
fr.ird.observe.entities.referentiel.Pays#1239832675592#0.18032080790817928	1	2009-04-15	21	Equateur
fr.ird.observe.entities.referentiel.Pays#1239832675593#0.24921769452411147	1	2009-04-15	22	Maurice
fr.ird.observe.entities.referentiel.Pays#1239832675593#0.4407649460417081	1	2009-04-15	24	Inde
fr.ird.observe.entities.referentiel.Pays#1239832675583#0.9493110781716075	1	2009-04-15	1	France
fr.ird.observe.entities.referentiel.Pays#1239832675584#0.10128636927717882	1	2009-04-15	2	Sénégal
fr.ird.observe.entities.referentiel.Pays#1239832675584#0.7208429105932204	1	2009-04-15	3	Côte d'Ivoire
fr.ird.observe.entities.referentiel.Pays#1239832675584#0.0783072255559325	1	2009-04-15	4	Espagne
fr.ird.observe.entities.referentiel.Pays#1239832675584#0.5193392010996171	1	2009-04-15	5	Yougoslavie
fr.ird.observe.entities.referentiel.Pays#1239832675584#0.4865280661128637	1	2009-04-15	6	U.S.A.
fr.ird.observe.entities.referentiel.Pays#1239832675585#0.8112726872159317	1	2009-04-15	7	Japon
fr.ird.observe.entities.referentiel.Pays#1239832675585#0.5822998021269842	1	2009-04-15	8	Chine Taiwan
fr.ird.observe.entities.referentiel.Pays#1239832675585#0.7535948739996284	1	2009-04-15	9	Corée
fr.ird.observe.entities.referentiel.Pays#1239832675590#0.9860530760211061	1	2009-04-15	10	Maroc
fr.ird.observe.entities.referentiel.Pays#1239832675590#0.46984340766571253	1	2009-04-15	11	Panama
fr.ird.observe.entities.referentiel.Pays#1239832675593#0.3601938043845213	1	2009-04-15	23	Seychelles
fr.ird.observe.entities.referentiel.Pays#1239832675593#0.5564506922817382	1	2009-04-15	25	Cap Vert
fr.ird.observe.entities.referentiel.Pays#1239832675593#0.10207212372204011	1	2009-04-15	26	Vanuatu
fr.ird.observe.entities.referentiel.Pays#1239832675594#0.6435649696754744	1	2009-04-15	27	Honduras
fr.ird.observe.entities.referentiel.Pays#1239832675594#0.3241600448153684	1	2009-04-15	28	Malte
fr.ird.observe.entities.referentiel.Pays#1239832675594#0.8561205682417258	1	2009-04-15	29	Saint Vincent
fr.ird.observe.entities.referentiel.Pays#1239832675594#0.141909095388714	1	2009-04-15	30	Libéria
fr.ird.observe.entities.referentiel.Pays#1239832675594#0.7391367529849715	1	2009-04-15	31	Iran
fr.ird.observe.entities.referentiel.Pays#1239832675595#0.1799231640486968	1	2009-04-15	32	Italie
fr.ird.observe.entities.referentiel.Pays#1239832675595#0.4516205269922994	1	2009-04-15	33	Sierra Leone
fr.ird.observe.entities.referentiel.Pays#1239832675595#0.6170308801319087	1	2009-04-15	34	Belize
fr.ird.observe.entities.referentiel.Pays#1239832675595#0.9573433532024453	1	2009-04-15	35	Guinée Conakry
fr.ird.observe.entities.referentiel.Pays#1239832675595#0.07349229119463152	1	2009-04-15	36	Russie
fr.ird.observe.entities.referentiel.Pays#1239832675596#0.4441716945441837	1	2009-04-15	37	Antilles hollandaises
fr.ird.observe.entities.referentiel.Pays#1239832675596#0.5623209070898836	1	2009-04-15	38	Malaisie
fr.ird.observe.entities.referentiel.Pays#1239832675596#0.3299006221926467	1	2009-04-15	39	Guatemala
fr.ird.observe.entities.referentiel.Pays#1239832675596#0.9401263656512455	1	2009-04-15	40	El Salvador
fr.ird.observe.entities.referentiel.Pays#1239832675597#0.708958027082267	1	2009-04-15	41	Mayotte
\.


--
-- Data for Name: programme; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY programme (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.Programme#1239832686259#0.5583492343428125	1	2009-04-15	1	DCR
fr.ird.observe.entities.referentiel.Programme#1239832686262#0.3362184368606246	1	2009-04-15	2	Patudo
fr.ird.observe.entities.referentiel.Programme#1239832686262#0.8610783468649943	1	2009-04-15	3	Moratoire
fr.ird.observe.entities.referentiel.Programme#1239832686262#0.714540816186228	1	2009-04-15	4	SFA
fr.ird.observe.entities.referentiel.Programme#1239832686262#0.31033946454061234	1	2009-04-15	5	Embarquement Ird
fr.ird.observe.entities.referentiel.Programme#1239832686262#0.42751447061198444	1	2009-04-15	9	Indéterminé
\.


--
-- Data for Name: raisonrejet; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY raisonrejet (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.RaisonRejet#1239832686292#0.2327417962534054	1	2009-04-15	1	Espèce	f
fr.ird.observe.entities.referentiel.RaisonRejet#1239832686293#0.3952415543026304	1	2009-04-15	2	Taille	f
fr.ird.observe.entities.referentiel.RaisonRejet#1239832686293#0.5847340206418373	1	2009-04-15	3	Cuve pleine	f
fr.ird.observe.entities.referentiel.RaisonRejet#1239832686293#0.8068644206295019	1	2009-04-15	4	Poisson abîmé	f
fr.ird.observe.entities.referentiel.RaisonRejet#1239832686293#0.9318446503236287	1	2009-04-15	99	Autre ( à préciser dans les notes )	t
\.


--
-- Data for Name: route; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY route (topiaid, topiaversion, topiacreatedate, jourobservation, lochmatin, lochsoir, commentaire, niveauverification, open, maree) FROM stdin;
\.


--
-- Data for Name: senne; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY senne (topiaid, topiaversion, topiacreatedate, identifiant, longueur, chute, poidslest) FROM stdin;
fr.ird.observe.entities.referentiel.Senne#1239832686356#0.5287719847367115	1	2009-04-15	1	100	10	100
fr.ird.observe.entities.referentiel.Senne#1239832686356#0.22313550034114016	1	2009-04-15	2	200	10	100
fr.ird.observe.entities.referentiel.Senne#1239832686357#0.9740396504236823	1	2009-04-15	3	200	20	100
fr.ird.observe.entities.referentiel.Senne#1239832686357#0.7215544657951589	1	2009-04-15	4	200	20	200
fr.ird.observe.entities.referentiel.Senne#1239832686357#0.16273338521705816	1	2009-04-15	5	200	30	300
\.


--
-- Data for Name: statutespece; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY statutespece (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.StatutEspece#1239832686390#0.056026648869705986	1	2009-04-15	1	maillée vivante
fr.ird.observe.entities.referentiel.StatutEspece#1239832686391#0.43313533290909123	1	2009-04-15	2	maillée morte
fr.ird.observe.entities.referentiel.StatutEspece#1239832686391#0.9485129645120297	1	2009-04-15	3	libre
\.


--
-- Data for Name: systemeobserve; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY systemeobserve (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.9395222812356602	1	2009-04-15	11	Mysticèdes ( Baleines )	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.9217864901728908	1	2009-04-15	12	Requin baleine	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.8697950628458667	1	2009-04-15	13	Requin	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.3156304184082318	1	2009-04-15	14	Autre thonier	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.7184788635514053	1	2009-04-15	15	Navire auxiliaire ( Supply )	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686429#0.47481094287896464	1	2009-04-15	16	Même banc échappé d'un encerclement	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686429#0.16963563399176218	1	2009-04-15	17	Banc sous le thonier	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686430#0.0707482498533355	1	2009-04-15	18	Pêche sur haut-fond ( Guyot )	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686430#0.9306046432586412	1	2009-04-15	19	Pêche sur rupture du plateau con	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686430#0.010891536437443339	1	2009-04-15	99	Autres ( A préciser dans les notes )	t
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686426#0.6606964000652922	1	2009-04-15	0	Aucun système	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686427#0.5015912524161882	1	2009-04-15	1	Matte ( Pas de précision sur le 	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686427#0.4875966267297659	1	2009-04-15	2	Balbaya, sardara, brisant ou rou	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686427#0.7251934343058472	1	2009-04-15	3	Thons en profondeur	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686427#0.6223326827132584	1	2009-04-15	4	Oiseaux	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686427#0.15391195068515717	1	2009-04-15	5	Epave non balisée	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686427#0.725007998980404	1	2009-04-15	6	Epave balisée	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686427#0.33083751936947536	1	2009-04-15	7	Charogne	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.31736040820946176	1	2009-04-15	8	Charogne balisée	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.6854047537933218	1	2009-04-15	9	Petits odontocètes ( Dauphins, g	f
fr.ird.observe.entities.referentiel.SystemeObserve#1239832686428#0.9425305842216437	1	2009-04-15	10	Grands odontocèdes ( Cachalots )	f
\.


--
-- Data for Name: taillefaune; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY taillefaune (topiaid, topiaversion, topiacreatedate, longueur, sexe, referencephoto, espece, echantillonfaune) FROM stdin;
\.


--
-- Data for Name: taillepoidsfaune; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY taillepoidsfaune (topiaid, topiaversion, topiacreatedate, annee, poidslongueura, poidslongueurb, ocean, espece) FROM stdin;
\.


--
-- Data for Name: taillepoidsthon; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY taillepoidsthon (topiaid, topiaversion, topiacreatedate, annee, poidslongueura, poidslongueurb, espece, ocean) FROM stdin;
\.


--
-- Data for Name: taillethon; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY taillethon (topiaid, topiaversion, longueur, echantillonthon, espece, topiacreatedate, codemesure, effectif) FROM stdin;
\.


--
-- Data for Name: typebalise; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY typebalise (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.TypeBalise#1239832686500#0.12631179181869223	1	2009-04-15	1	Radiogoniomètre	f
fr.ird.observe.entities.referentiel.TypeBalise#1239832686500#0.7868193331348977	1	2009-04-15	2	Radiogonio + GPS	f
fr.ird.observe.entities.referentiel.TypeBalise#1239832686500#0.9221804289591944	1	2009-04-15	3	GPS SHERPE	f
fr.ird.observe.entities.referentiel.TypeBalise#1239832686500#0.7638501036770929	1	2009-04-15	4	Satellite + échosondeur	f
fr.ird.observe.entities.referentiel.TypeBalise#1239832686500#0.7406749287430566	1	2009-04-15	5	Satellite sans échosondeur	f
fr.ird.observe.entities.referentiel.TypeBalise#1239832686501#0.7210594539339781	1	2009-04-15	6	Satellite + Sonar	f
fr.ird.observe.entities.referentiel.TypeBalise#1239832686501#0.7801253242325985	1	2009-04-15	99	Type à préciser	t
\.


--
-- Data for Name: typebateau; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY typebateau (topiaid, topiaversion, topiacreatedate, code, libelle) FROM stdin;
fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.24685054061673772	1	2009-04-15	1	Canne / glacier
fr.ird.observe.entities.referentiel.TypeBateau#1239832675734#0.4191950326431938	1	2009-04-15	2	Canne / congélateur
fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.044156847891821505	1	2009-04-15	3	Mixte
fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.7380146830307519	1	2009-04-15	4	Senneur avec appât
fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.9086075071905084	1	2009-04-15	5	Senneur sans appât
fr.ird.observe.entities.referentiel.TypeBateau#1239832675735#0.307197212385357	1	2009-04-15	6	Grand senneur
fr.ird.observe.entities.referentiel.TypeBateau#1239832675736#0.8708229847859869	1	2009-04-15	7	Palangrier
fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.3189669579867931	1	2009-04-15	8	Bateau Mère
fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.03138158577140615	1	2009-04-15	9	Bateau Usine
fr.ird.observe.entities.referentiel.TypeBateau#1239832675737#0.43324169605639407	1	2009-04-15	10	Supply
\.


--
-- Data for Name: typeobjet; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY typeobjet (topiaid, topiaversion, topiacreatedate, code, libelle, needcomment) FROM stdin;
fr.ird.observe.entities.referentiel.TypeObjet#1239832686563#0.688534215381033	1	2009-04-15	1	tas de paille	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.13889047716567404	1	2009-04-15	2	Palme de cocotier / palmier	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.23119123436653222	1	2009-04-15	3	Arbre ( ou branche )	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.002691776291490311	1	2009-04-15	4	Charogne ( préciser la nature dans les notes )	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.8004309360361301	1	2009-04-15	5	Charogne balisée	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.08078169174465666	1	2009-04-15	6	Radeau balisé en dérive ( ligne et filet )	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.44929275294648374	1	2009-04-15	7	DCP ( Dispositif de Concentration de Poisson )	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.7433235240138468	1	2009-04-15	8	Thonier ( ou skiff )	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686564#0.9947624092230969	1	2009-04-15	9	Bateau d'appui ( supply )	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686565#0.28689628152638935	1	2009-04-15	10	Caisse ou grosse planche	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686565#0.8517078464341938	1	2009-04-15	11	Cordage, câble	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686565#0.05946979309573974	1	2009-04-15	12	Filet ou morceau de filet	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686565#0.07397538492536193	1	2009-04-15	13	Objet de plastique ( à préciser dans les notes )	t
fr.ird.observe.entities.referentiel.TypeObjet#1239832686565#0.13571777026332488	1	2009-04-15	14	Un des antérieurs ( du 10 à 13 ) balisé	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686565#0.2649661094338329	1	2009-04-15	15	Objet expérimental	f
fr.ird.observe.entities.referentiel.TypeObjet#1239832686565#0.1264694743692112	1	2009-04-15	99	Autre ( à préciser dans les notes )	t
\.


--
-- Data for Name: ventbeaufort; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY ventbeaufort (topiaid, topiaversion, topiacreatedate, code, libelledescriptifvent, libellevitessevent, libelledescriptifmer, libellehauteurmoyennevagues) FROM stdin;
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686604#0.6861700847985526	1	2009-04-15	0	Calme	<1	Calme	0
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686605#0.013889226172736802	1	2009-04-15	1	Très légère brise	1 - 3	Calme, ridée	0 - 0,1
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686605#0.14941733815370462	1	2009-04-15	2	Légère brise	4 - 6	Belle, vaguelettes	0,1 - 0,5
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686605#0.561188597983181	1	2009-04-15	3	Petite brise	7 - 10	Peu agitée, vaguelettes surmontées de moutons	0,5 - 1,25
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686605#0.756896053766457	1	2009-04-15	4	Jolie brise	11 - 16	Agitée, patites vagues, nombreux moutons	1,25 - 2,5
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686606#0.4900119760201238	1	2009-04-15	5	Bonne brise	17 - 21	Forte, vagues moyennes écumeuses	2,5 - 4
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686607#0.7102940452522897	1	2009-04-15	6	Vent frais	22 - 27	Très forte, grosses vagues très écumeuses	4 - 6
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686608#0.8409158782611442	1	2009-04-15	7	Grand frais	28 - 33	Grosse, formation de trainées d'écumes	6 - 9
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686608#0.7828762960828749	1	2009-04-15	8	Coup de vent	34 - 40	Très grosse, trainées d'écumes bien nettes	9 - 14
fr.ird.observe.entities.referentiel.VentBeaufort#1239832686608#0.5265273246398433	1	2009-04-15	9	Fort coup de vent	41 - 47	Enorme, visibilité se réduisant avec l'augmentation de l'écume d	> 14
\.


--
-- Name: activite_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT activite_pkey PRIMARY KEY (topiaid);


--
-- Name: activitebateau_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY activitebateau
    ADD CONSTRAINT activitebateau_pkey PRIMARY KEY (topiaid);


--
-- Name: activiteenvironnante_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY activiteenvironnante
    ADD CONSTRAINT activiteenvironnante_pkey PRIMARY KEY (topiaid);


--
-- Name: baliselue_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY baliselue
    ADD CONSTRAINT baliselue_pkey PRIMARY KEY (topiaid);


--
-- Name: bateau_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY bateau
    ADD CONSTRAINT bateau_pkey PRIMARY KEY (topiaid);


--
-- Name: calee_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY calee
    ADD CONSTRAINT calee_pkey PRIMARY KEY (topiaid);


--
-- Name: capturefaune_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY capturefaune
    ADD CONSTRAINT capturefaune_pkey PRIMARY KEY (topiaid);


--
-- Name: capturefaunecalcule_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY capturefaunecalcule
    ADD CONSTRAINT capturefaunecalcule_pkey PRIMARY KEY (topiaid);


--
-- Name: capturethon_cuve_key; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY capturethon
    ADD CONSTRAINT capturethon_cuve_key UNIQUE (cuve, rejete, calee, categoriepoids, raisonrejet);


--
-- Name: capturethon_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY capturethon
    ADD CONSTRAINT capturethon_pkey PRIMARY KEY (topiaid);


--
-- Name: categoriebateau_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY categoriebateau
    ADD CONSTRAINT categoriebateau_pkey PRIMARY KEY (topiaid);


--
-- Name: categoriepoids_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY categoriepoids
    ADD CONSTRAINT categoriepoids_pkey PRIMARY KEY (topiaid);


--
-- Name: causecoupnul_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY causecoupnul
    ADD CONSTRAINT causecoupnul_pkey PRIMARY KEY (topiaid);


--
-- Name: causenoncoupsenne_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY causenoncoupsenne
    ADD CONSTRAINT causenoncoupsenne_pkey PRIMARY KEY (topiaid);


--
-- Name: devenirfaune_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY devenirfaune
    ADD CONSTRAINT devenirfaune_pkey PRIMARY KEY (topiaid);


--
-- Name: devenirobjet_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY devenirobjet
    ADD CONSTRAINT devenirobjet_pkey PRIMARY KEY (topiaid);


--
-- Name: echantillonfaune_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY echantillonfaune
    ADD CONSTRAINT echantillonfaune_pkey PRIMARY KEY (topiaid);


--
-- Name: echantillonthon_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY echantillonthon
    ADD CONSTRAINT echantillonthon_pkey PRIMARY KEY (topiaid);


--
-- Name: especefaune_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY especefaune
    ADD CONSTRAINT especefaune_pkey PRIMARY KEY (topiaid);


--
-- Name: especefauneobservee_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY especefauneobservee
    ADD CONSTRAINT especefauneobservee_pkey PRIMARY KEY (topiaid);


--
-- Name: especethon_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY especethon
    ADD CONSTRAINT especethon_pkey PRIMARY KEY (topiaid);


--
-- Name: estimationbanc_calee_key; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY estimationbanc
    ADD CONSTRAINT estimationbanc_calee_key UNIQUE (calee, espece);


--
-- Name: estimationbanc_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY estimationbanc
    ADD CONSTRAINT estimationbanc_pkey PRIMARY KEY (topiaid);


--
-- Name: estimationbancobjet_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY estimationbancobjet
    ADD CONSTRAINT estimationbancobjet_pkey PRIMARY KEY (topiaid);


--
-- Name: groupeespecefaune_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY groupeespecefaune
    ADD CONSTRAINT groupeespecefaune_pkey PRIMARY KEY (topiaid);


--
-- Name: maree_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY maree
    ADD CONSTRAINT maree_pkey PRIMARY KEY (topiaid);


--
-- Name: modedetection_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY modedetection
    ADD CONSTRAINT modedetection_pkey PRIMARY KEY (topiaid);


--
-- Name: objetflottant_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY objetflottant
    ADD CONSTRAINT objetflottant_pkey PRIMARY KEY (topiaid);


--
-- Name: observateur_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY observateur
    ADD CONSTRAINT observateur_pkey PRIMARY KEY (topiaid);


--
-- Name: ocean_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY ocean
    ADD CONSTRAINT ocean_pkey PRIMARY KEY (topiaid);


--
-- Name: operationbalise_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY operationbalise
    ADD CONSTRAINT operationbalise_pkey PRIMARY KEY (topiaid);


--
-- Name: operationobjet_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY operationobjet
    ADD CONSTRAINT operationobjet_pkey PRIMARY KEY (topiaid);


--
-- Name: pays_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY pays
    ADD CONSTRAINT pays_pkey PRIMARY KEY (topiaid);


--
-- Name: programme_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY programme
    ADD CONSTRAINT programme_pkey PRIMARY KEY (topiaid);


--
-- Name: raisonrejet_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY raisonrejet
    ADD CONSTRAINT raisonrejet_pkey PRIMARY KEY (topiaid);


--
-- Name: route_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_pkey PRIMARY KEY (topiaid);


--
-- Name: senne_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY senne
    ADD CONSTRAINT senne_pkey PRIMARY KEY (topiaid);


--
-- Name: statutespece_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY statutespece
    ADD CONSTRAINT statutespece_pkey PRIMARY KEY (topiaid);


--
-- Name: systemeobserve_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY systemeobserve
    ADD CONSTRAINT systemeobserve_pkey PRIMARY KEY (topiaid);


--
-- Name: taillefaune_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY taillefaune
    ADD CONSTRAINT taillefaune_pkey PRIMARY KEY (topiaid);


--
-- Name: taillepoidsfaune_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY taillepoidsfaune
    ADD CONSTRAINT taillepoidsfaune_pkey PRIMARY KEY (topiaid);


--
-- Name: taillepoidsthon_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY taillepoidsthon
    ADD CONSTRAINT taillepoidsthon_pkey PRIMARY KEY (topiaid);


--
-- Name: taillethon_longueur_key; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY taillethon
    ADD CONSTRAINT taillethon_longueur_key UNIQUE (longueur, echantillonthon, espece);


--
-- Name: taillethon_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY taillethon
    ADD CONSTRAINT taillethon_pkey PRIMARY KEY (topiaid);


--
-- Name: typebalise_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY typebalise
    ADD CONSTRAINT typebalise_pkey PRIMARY KEY (topiaid);


--
-- Name: typebateau_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY typebateau
    ADD CONSTRAINT typebateau_pkey PRIMARY KEY (topiaid);


--
-- Name: typeobjet_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY typeobjet
    ADD CONSTRAINT typeobjet_pkey PRIMARY KEY (topiaid);


--
-- Name: ventbeaufort_pkey; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY ventbeaufort
    ADD CONSTRAINT ventbeaufort_pkey PRIMARY KEY (topiaid);


--
-- Name: fk10351ea46f9a5377; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY objetflottant
    ADD CONSTRAINT fk10351ea46f9a5377 FOREIGN KEY (type) REFERENCES typeobjet(topiaid);


--
-- Name: fk10351ea47062c615; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY objetflottant
    ADD CONSTRAINT fk10351ea47062c615 FOREIGN KEY (devenir) REFERENCES devenirobjet(topiaid);


--
-- Name: fk10351ea4e182719c; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY objetflottant
    ADD CONSTRAINT fk10351ea4e182719c FOREIGN KEY (activite) REFERENCES activite(topiaid);


--
-- Name: fk1b528647356ff242; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY estimationbancobjet
    ADD CONSTRAINT fk1b528647356ff242 FOREIGN KEY (objetflottant) REFERENCES objetflottant(topiaid);


--
-- Name: fk1b528647da39ffac; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY estimationbancobjet
    ADD CONSTRAINT fk1b528647da39ffac FOREIGN KEY (espece) REFERENCES especethon(topiaid);


--
-- Name: fk2955fbce356ff242; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY baliselue
    ADD CONSTRAINT fk2955fbce356ff242 FOREIGN KEY (objetflottant) REFERENCES objetflottant(topiaid);


--
-- Name: fk2955fbceb3e8d13b; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY baliselue
    ADD CONSTRAINT fk2955fbceb3e8d13b FOREIGN KEY (operationbalise) REFERENCES operationbalise(topiaid);


--
-- Name: fk2955fbcee7f36f5f; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY baliselue
    ADD CONSTRAINT fk2955fbcee7f36f5f FOREIGN KEY (typebalise) REFERENCES typebalise(topiaid);


--
-- Name: fk2c05ad62da39ffac; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY categoriepoids
    ADD CONSTRAINT fk2c05ad62da39ffac FOREIGN KEY (espece) REFERENCES especethon(topiaid);


--
-- Name: fk2c501eab7dbe4cad; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaune
    ADD CONSTRAINT fk2c501eab7dbe4cad FOREIGN KEY (raisonrejet) REFERENCES raisonrejet(topiaid);


--
-- Name: fk2c501eabbf8bcb63; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaune
    ADD CONSTRAINT fk2c501eabbf8bcb63 FOREIGN KEY (devenirfaune) REFERENCES devenirfaune(topiaid);


--
-- Name: fk2c501eabc2754c82; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaune
    ADD CONSTRAINT fk2c501eabc2754c82 FOREIGN KEY (espece) REFERENCES especefaune(topiaid);


--
-- Name: fk2c501eabfd52fff6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaune
    ADD CONSTRAINT fk2c501eabfd52fff6 FOREIGN KEY (calee) REFERENCES calee(topiaid);


--
-- Name: fk30d62821b2c5c0ed; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillepoidsfaune
    ADD CONSTRAINT fk30d62821b2c5c0ed FOREIGN KEY (ocean) REFERENCES ocean(topiaid);


--
-- Name: fk30d62821c2754c82; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillepoidsfaune
    ADD CONSTRAINT fk30d62821c2754c82 FOREIGN KEY (espece) REFERENCES especefaune(topiaid);


--
-- Name: fk542e6783b2c5c0ed; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillepoidsthon
    ADD CONSTRAINT fk542e6783b2c5c0ed FOREIGN KEY (ocean) REFERENCES ocean(topiaid);


--
-- Name: fk542e6783da39ffac; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillepoidsthon
    ADD CONSTRAINT fk542e6783da39ffac FOREIGN KEY (espece) REFERENCES especethon(topiaid);


--
-- Name: fk54d2b175356ff242; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY especefauneobservee
    ADD CONSTRAINT fk54d2b175356ff242 FOREIGN KEY (objetflottant) REFERENCES objetflottant(topiaid);


--
-- Name: fk54d2b175c2754c82; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY especefauneobservee
    ADD CONSTRAINT fk54d2b175c2754c82 FOREIGN KEY (espece) REFERENCES especefaune(topiaid);


--
-- Name: fk54d2b175f7ac3e7f; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY especefauneobservee
    ADD CONSTRAINT fk54d2b175f7ac3e7f FOREIGN KEY (statutespece) REFERENCES statutespece(topiaid);


--
-- Name: fk5a0d0ee936a7dd9; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY calee
    ADD CONSTRAINT fk5a0d0ee936a7dd9 FOREIGN KEY (causecoupnul) REFERENCES causecoupnul(topiaid);


--
-- Name: fk62dd27e226c05a9; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY maree
    ADD CONSTRAINT fk62dd27e226c05a9 FOREIGN KEY (programme) REFERENCES programme(topiaid);


--
-- Name: fk62dd27e6eb8e5fd; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY maree
    ADD CONSTRAINT fk62dd27e6eb8e5fd FOREIGN KEY (observateur) REFERENCES observateur(topiaid);


--
-- Name: fk62dd27eb2c5c0ed; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY maree
    ADD CONSTRAINT fk62dd27eb2c5c0ed FOREIGN KEY (ocean) REFERENCES ocean(topiaid);


--
-- Name: fk62dd27ec59b8297; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY maree
    ADD CONSTRAINT fk62dd27ec59b8297 FOREIGN KEY (senne) REFERENCES senne(topiaid);


--
-- Name: fk62dd27ed561a0f7; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY maree
    ADD CONSTRAINT fk62dd27ed561a0f7 FOREIGN KEY (bateau) REFERENCES bateau(topiaid);


--
-- Name: fk64071e0a11188b1b; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite_systemeobserve
    ADD CONSTRAINT fk64071e0a11188b1b FOREIGN KEY (systemeobserve) REFERENCES systemeobserve(topiaid);


--
-- Name: fk64071e0ae182719c; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite_systemeobserve
    ADD CONSTRAINT fk64071e0ae182719c FOREIGN KEY (activite) REFERENCES activite(topiaid);


--
-- Name: fk67ab2495f1bb16; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY route
    ADD CONSTRAINT fk67ab2495f1bb16 FOREIGN KEY (maree) REFERENCES maree(topiaid);


--
-- Name: fk7a6a468ccc49501d; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY especefaune
    ADD CONSTRAINT fk7a6a468ccc49501d FOREIGN KEY (groupe) REFERENCES groupeespecefaune(topiaid);


--
-- Name: fk88ea14dfda39ffac; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY estimationbanc
    ADD CONSTRAINT fk88ea14dfda39ffac FOREIGN KEY (espece) REFERENCES especethon(topiaid);


--
-- Name: fk88ea14dffd52fff6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY estimationbanc
    ADD CONSTRAINT fk88ea14dffd52fff6 FOREIGN KEY (calee) REFERENCES calee(topiaid);


--
-- Name: fk89b424bcb868fbf6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillethon
    ADD CONSTRAINT fk89b424bcb868fbf6 FOREIGN KEY (echantillonthon) REFERENCES echantillonthon(topiaid);


--
-- Name: fk89b424bcda39ffac; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillethon
    ADD CONSTRAINT fk89b424bcda39ffac FOREIGN KEY (espece) REFERENCES especethon(topiaid);


--
-- Name: fk9b677b97dbe4cad; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturethon
    ADD CONSTRAINT fk9b677b97dbe4cad FOREIGN KEY (raisonrejet) REFERENCES raisonrejet(topiaid);


--
-- Name: fk9b677b9994c8f93; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturethon
    ADD CONSTRAINT fk9b677b9994c8f93 FOREIGN KEY (categoriepoids) REFERENCES categoriepoids(topiaid);


--
-- Name: fk9b677b9fd52fff6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturethon
    ADD CONSTRAINT fk9b677b9fd52fff6 FOREIGN KEY (calee) REFERENCES calee(topiaid);


--
-- Name: fk9d4bf2fb33d7e3f3; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT fk9d4bf2fb33d7e3f3 FOREIGN KEY (activiteenvironnante) REFERENCES activiteenvironnante(topiaid);


--
-- Name: fk9d4bf2fb52309d2c; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT fk9d4bf2fb52309d2c FOREIGN KEY (route) REFERENCES route(topiaid);


--
-- Name: fk9d4bf2fb53f6c55; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT fk9d4bf2fb53f6c55 FOREIGN KEY (modedetection) REFERENCES modedetection(topiaid);


--
-- Name: fk9d4bf2fb54bcde0d; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT fk9d4bf2fb54bcde0d FOREIGN KEY (activitebateau) REFERENCES activitebateau(topiaid);


--
-- Name: fk9d4bf2fb7ba91f7d; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT fk9d4bf2fb7ba91f7d FOREIGN KEY (ventbeaufort) REFERENCES ventbeaufort(topiaid);


--
-- Name: fk9d4bf2fbe47090a1; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT fk9d4bf2fbe47090a1 FOREIGN KEY (causenoncoupsenne) REFERENCES causenoncoupsenne(topiaid);


--
-- Name: fk9d4bf2fbfd52fff6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY activite
    ADD CONSTRAINT fk9d4bf2fbfd52fff6 FOREIGN KEY (calee) REFERENCES calee(topiaid);


--
-- Name: fka443610e7dbe4cad; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaunecalcule
    ADD CONSTRAINT fka443610e7dbe4cad FOREIGN KEY (raisonrejet) REFERENCES raisonrejet(topiaid);


--
-- Name: fka443610e8bc00c69; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaunecalcule
    ADD CONSTRAINT fka443610e8bc00c69 FOREIGN KEY (especefaune) REFERENCES especefaune(topiaid);


--
-- Name: fka443610eb45411b1; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaunecalcule
    ADD CONSTRAINT fka443610eb45411b1 FOREIGN KEY (taillepoidsfaune) REFERENCES taillepoidsfaune(topiaid);


--
-- Name: fka443610ebf8bcb63; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaunecalcule
    ADD CONSTRAINT fka443610ebf8bcb63 FOREIGN KEY (devenirfaune) REFERENCES devenirfaune(topiaid);


--
-- Name: fka443610efd52fff6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY capturefaunecalcule
    ADD CONSTRAINT fka443610efd52fff6 FOREIGN KEY (calee) REFERENCES calee(topiaid);


--
-- Name: fkac0812089fe763d2; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillefaune
    ADD CONSTRAINT fkac0812089fe763d2 FOREIGN KEY (echantillonfaune) REFERENCES echantillonfaune(topiaid);


--
-- Name: fkac081208c2754c82; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY taillefaune
    ADD CONSTRAINT fkac081208c2754c82 FOREIGN KEY (espece) REFERENCES especefaune(topiaid);


--
-- Name: fkacc81724227335e8; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY bateau
    ADD CONSTRAINT fkacc81724227335e8 FOREIGN KEY (jauge) REFERENCES categoriebateau(topiaid);


--
-- Name: fkacc817247202dbd0; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY bateau
    ADD CONSTRAINT fkacc817247202dbd0 FOREIGN KEY (pecherie) REFERENCES typebateau(topiaid);


--
-- Name: fkacc81724844dc97d; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY bateau
    ADD CONSTRAINT fkacc81724844dc97d FOREIGN KEY (pavillon) REFERENCES pays(topiaid);


--
-- Name: fkde02356c356ff242; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY objetflottant_operation
    ADD CONSTRAINT fkde02356c356ff242 FOREIGN KEY (objetflottant) REFERENCES objetflottant(topiaid);


--
-- Name: fkde02356ca005fe55; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY objetflottant_operation
    ADD CONSTRAINT fkde02356ca005fe55 FOREIGN KEY (operation) REFERENCES operationobjet(topiaid);


--
-- Name: fke7aa45c6fd52fff6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY echantillonfaune
    ADD CONSTRAINT fke7aa45c6fd52fff6 FOREIGN KEY (calee) REFERENCES calee(topiaid);


--
-- Name: fkff3d813efd52fff6; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY echantillonthon
    ADD CONSTRAINT fkff3d813efd52fff6 FOREIGN KEY (calee) REFERENCES calee(topiaid);



--
-- PostgreSQL database dump complete
--

