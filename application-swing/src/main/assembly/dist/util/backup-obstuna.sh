###
# #%L
# ObServe :: Admin Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
#!/bin/sh

#
# script pour faire une backup d'obstuna.
#
# Utilisation : ./backup-obstuna.sh host filename_prefix [dbName]
#
# - host : l'adresse du serveur obstuna
# - filename_prefix : le prefix des scripts sql generes.
# - dbName : le nom optionel de la base (par defaut on prend 'obstuna').
#
#
# Le script genere 8 fichiers sql :
#
# filename_prefix-schema.sql (script de creation du schema complet)
# filename_prefix-drop.sql (script des destruction du shcema de base)
# filename_prefix-schema-without-constraints.sql (script de creation du schema sans les constraintes)
# filename_prefix-constraints.sql (script de creation des constraintes)
# filename_prefix-referentiel.sql (script des donnees du referentiel)
# filename_prefix-data.sql (script des donnees observateurs)
# filename_prefix-createdb_referentiel.sql (script des creation base + referentiel)
# filename_prefix-createdb_full.sql (script des creation base + referentiel + donnees)
#

if [ $# -lt 2 ] ; then
    echo "le nombre de paramètres n'est pas correct."
    echo ""
    echo "usage $0 host filename_prefix [dbname] [dbUser]"
    exit 1
fi

HOST=$1
FILENAME_PREFIX=$2

DB_NAME="obstuna"

if [ $# -ge 3 ] ; then
  DB_NAME=$3
fi

DB_USER="utilisateur"

if [ $# -ge 4 ] ; then
  DB_USER=$4
fi

DATA_TABLES=`cat tables/data-tables.txt`
REFERENTIEL_TABLES=`cat tables/referentiel-tables.txt`

FILE=$FILENAME_PREFIX-schema.sql
COMMAND="pg_dump -h $HOST -U $DB_USER -s -x -c -O -f $FILE $DB_NAME"
echo "creation du script $FILE"
echo $COMMAND
eval $COMMAND

FILE=$FILENAME_PREFIX-referentiel.sql
COMMAND="pg_dump -h $HOST -U $DB_USER -a --inserts -f $FILE"

for table in $REFERENTIEL_TABLES ;
do
    COMMAND="$COMMAND -t $table"
done

COMMAND="$COMMAND $DB_NAME"
echo "creation du script $FILE"
echo $COMMAND
eval $COMMAND

FILE=$FILENAME_PREFIX-data.sql
COMMAND="pg_dump -h $HOST -U $DB_USER -a --inserts -f $FILE"

for table in $DATA_TABLES ;
do
    COMMAND="$COMMAND -t $table"
done
COMMAND="$COMMAND $DB_NAME"

echo "creation du script $FILE"
echo $COMMAND
eval $COMMAND

SCHEMA_FILE=$FILENAME_PREFIX-schema.sql
NB=$(( $(grep -Hne "Name: activite_pkey;" $SCHEMA_FILE | cut -d : -f2) - 3 ))
NB1=$(( $(grep -Hne "Name: public; Type: SCHEMA; Schema: -; Owner: -" $SCHEMA_FILE | cut -d : -f2) - 2 ))

echo "NB = $NB"
echo "NB1 = $NB1"

FILE=$FILENAME_PREFIX-constraints.sql
FILE2=$FILENAME_PREFIX-schema-without-constraints.sql
FILE3=$FILENAME_PREFIX-drop.sql
echo "creation du script $FILE"
echo "creation du script $FILE2"
echo "creation du script $FILE3"
i=0
echo "" > $FILE
echo "" > $FILE2
echo "" > $FILE3

while read line
do
   if [ $i -lt $NB1 ]; then
       echo $line >> $FILE3
   else
       if [ $i -lt $NB ]; then
           echo $line >> $FILE2
       else
           echo $line >> $FILE
       fi
   fi
   i=$(( $i + 1))
done < $SCHEMA_FILE

FILE=$FILENAME_PREFIX-create_referentiel.sql
echo "creation du script $FILE"

#cat $FILENAME_PREFIX-drop.sql > $FILE
cat $FILENAME_PREFIX-schema-without-constraints.sql > $FILE
cat $FILENAME_PREFIX-referentiel.sql >> $FILE
cat $FILENAME_PREFIX-constraints.sql >> $FILE

FILE=$FILENAME_PREFIX-create_full.sql
echo "creation du script $FILE"

#cat $FILENAME_PREFIX-drop.sql > $FILE
cat $FILENAME_PREFIX-schema-without-constraints.sql > $FILE
cat $FILENAME_PREFIX-referentiel.sql >> $FILE
cat $FILENAME_PREFIX-data.sql >> $FILE
cat $FILENAME_PREFIX-constraints.sql >> $FILE
