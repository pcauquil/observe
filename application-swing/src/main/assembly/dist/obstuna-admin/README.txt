
Pour créer une nouvelle base obstuna
------------------------------------

Lancer le script (sh)

./createdb/create-ird_obstuna.sh

Puis lancer ObServe en mode création de base obstuna

./create-obstuna.sh

ou

./create-obstuna.bat


Pour mettre à jour une base obstuna v1 vers vXXX
------------------------------------------------

./update-obstuna.sh

ou

./update-obstuna.bat

Pour mettre à jour la sécurité sur une base obstuna
---------------------------------------------------

Il faut ensuite lancer l'action de mise à jour de base via ObServe :

./update-security-obstuna.sh

ou

./update-security-obstuna.bat

Pour vider une base obstuna
---------------------------

./drop-obstuna.sh

ou

./drop-obstuna.bat
