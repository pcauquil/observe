#! /bin/bash
###
# #%L
# ObServe :: Admin Client
#
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###

# Lance les script sql du répertoire extra

echo -n "Nom de la base à utiliser ? [obstuna] "
read dbName
if [ "$dbName" == "" ] ; then
  dbName=obstuna
fi

echo -n "Machine de la base à utiliser ? [localhost] "
read dbHost
if [ "$dbHost" == "" ] ; then
  dbHost=localhost
fi

echo -n "Port de la base à utiliser ? [5432] "
read dbPort
if [ "$dbPort" == "" ] ; then
  dbPort=5432
fi

echo -n "Nom de l'utilisateur administrateur de la base ? [admin] "
read dbUser
if [ "$dbUser" == "" ] ; then
  dbUser=admin
fi

for script in $(ls extra/*.sql); do
    echo -n "Voulez-vous executer le script suivant : $script ? [O/n] "
    read i
    if [ "$i" == "O" -o "$i" == "o" -o "$i" == "" ] ; then
      echo "Execution du script $script"
      cat $script | psql -d $dbName -U $dbUser -h $dbHost -p $dbPort
    fi
done