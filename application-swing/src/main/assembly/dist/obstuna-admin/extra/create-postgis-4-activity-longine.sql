---
-- #%L
-- ObServe :: Application Swing
-- %%
-- Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the 
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public 
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

SELECT AddGeometryColumn('observe_longline','activity', 'the_geom', 4326, 'POINT',2 );
UPDATE observe_longline.activity SET the_geom=ST_SetSRID(ST_MakePoint(longitude,latitude), 4326);
DROP TRIGGER IF EXISTS tr_sync_longline_activity_the_geom ON observe_longline.activity;
CREATE TRIGGER tr_sync_longline_activity_the_geom BEFORE insert or update ON observe_longline.activity FOR EACH ROW EXECUTE PROCEDURE sync_activity_the_geom();
