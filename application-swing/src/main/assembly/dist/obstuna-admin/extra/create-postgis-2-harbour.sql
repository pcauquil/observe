---
-- #%L
-- ObServe :: Application Swing
-- %%
-- Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the 
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public 
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

SELECT AddGeometryColumn('observe_common', 'harbour', 'the_geom', 4326, 'POINT',2 );
UPDATE observe_common.harbour SET the_geom=ST_SetSRID(ST_MakePoint(longitude,latitude), 4326);

CREATE OR REPLACE function sync_harbour_the_geom () returns trigger as '
BEGIN

  -- TODO : differencier si on est en creation ou update
  --  IF (TG_OP = ''UPDATE'') THEN ...

  IF (NEW.latitude IS NULL OR NEW.longitude IS NULL) THEN
    -- on ne calcule pas le point postgis si au moins une des
    -- coordonnees n est pas renseignee

    RAISE NOTICE ''No latitude or longitude, can not compute postgis field for harbour % '', NEW.topiaId;
    return NEW;
  END IF;

  RAISE NOTICE ''Will compute the_geom for harbour % - latitude % and longitude %'', NEW.topiaId, NEW.latitude, NEW.longitude;

  -- affectation du point
  NEW.the_geom := ST_SetSRID(ST_MakePoint(NEW.longitude,NEW.latitude), 4326);

  RAISE NOTICE ''Computed for harbour % latitude % and longitude %, the_geom %'', NEW.topiaId, NEW.latitude, NEW.longitude, NEW.the_geom;

  RETURN NEW;
END
'
LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS tr_sync_harbour_the_geom ON observe_common.harbour;
CREATE TRIGGER tr_sync_harbour_the_geom BEFORE insert or update ON observe_common.harbour FOR EACH ROW EXECUTE PROCEDURE sync_harbour_the_geom();
