package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUICallback;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.util.Locale;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ChangeApplicationLanguageAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeApplicationLanguageAction.class);

    private final ObserveMainUI ui;

    private final Locale newLocale;

    public ChangeApplicationLanguageAction(ObserveMainUI ui, Locale newLocale, String name, String description) {

        super(name, SwingUtil.getUIManagerActionIcon("i18n-" + newLocale.getLanguage()));
        this.ui = ui;
        this.newLocale = newLocale;
        putValue(SHORT_DESCRIPTION, description);
        putValue(MNEMONIC_KEY, (int) newLocale.getLanguage().toUpperCase().charAt(0));

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        if (log.isInfoEnabled()) {
            log.info("ObServe changing application language...");
        }
        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {
            ObserveSwingApplicationConfig config = ui.getConfig();

            // sauvegarde de la nouvelle locale
            config.setOption(ObserveSwingApplicationConfigOption.LOCALE, newLocale);
            ObserveUICallback.ui.run();
        }

    }

}
