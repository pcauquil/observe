/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import com.google.common.collect.Sets;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTree;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Modèle de sélection dans un arbre de navigation prévu pour sélectionner des
 * données.
 *
 * La sélection se fait en cliquant (reclic pour supprimer).
 *
 * Si on sélectionne un program, alors ses marée sont aussi sélectionnées.
 *
 * Un program est sélectionné uniquement si toutes ses marées le sont.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class DataSelectionTreeSelectionModel implements TreeSelectionModel, PropertyChangeListener, Serializable {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(DataSelectionTreeSelectionModel.class);

    private static final long serialVersionUID = -1L;

    protected DataSelectionModel dataModel;

    /**
     * Paths that are currently selected.  Will be null if nothing is currently
     * selected.
     */
    protected TreePath[] selection;

    /** Event listener list. */
    protected final EventListenerList listenerList = new EventListenerList();

    /** Last path that was added. */
    protected TreePath leadPath;

    /** Index of the lead path in selection. */
    protected int leadIndex;

    /** Lead row. */
    protected int leadRow;

    protected int minSelectionRow;

    protected int maxSelectionRow;

    protected int[] selectionRows;

    /** Provides a row for a given path. */
    transient protected RowMapper rowMapper;

    protected Set<TreePath> universe;

    private PropertyChangeSupport changeSupport;

    private static final PropertyChangeListener[]
            EMPTY_PROPERTY_CHANGE_LISTENERS = new PropertyChangeListener[]{};

    public void initUI(JTree tree) {
        clearSelection();
        if (universe != null) {
            universe.clear();
        }
        // expand tree
        int i = 0;
        while (i < tree.getRowCount()) {
            tree.expandRow(i++);
        }
        // build universe of path : position of path in universe is
        // exactly the row of the path
        int count = tree.getRowCount();
        universe = new HashSet<>(count);
        for (i = 0; i < count; i++) {
            TreePath path = tree.getPathForRow(i);
            universe.add(path);
            if (log.isDebugEnabled()) {
                log.debug("init path : " + path.getLastPathComponent());
            }
        }
    }

    public void setDataModel(DataSelectionModel dataModel) {
        DataSelectionModel oldModel = this.dataModel;
        if (oldModel != null) {
            oldModel.removePropertyChangeListener(this);
            oldModel.destroy();
            //TODO Should destroy all other listeners ?
            //TODO this means model is dead...
        }
        this.dataModel = dataModel;
        if (dataModel != null) {
            this.dataModel.addPropertyChangeListener(this);
        }
        updateModel();
    }

    @Override
    public void setSelectionMode(int mode) {
        // pas utilise
    }

    @Override
    public int getSelectionMode() {
        return DISCONTIGUOUS_TREE_SELECTION;
    }

    @Override
    public void setSelectionPath(TreePath path) {
        if (dataModel == null) {
            return;
        }
        Object o = path.getLastPathComponent();
        ObserveNode node = AbstractObserveTreeCellRenderer.getNode(o);
        if (node == null) {
            return;
        }
        Class<?> internalClass = node.getInternalClass();

        boolean referentiel = node.isReferentielNode();

        if (!universe.contains(path)) {
            // new path
            universe.add(path);
            if (log.isDebugEnabled()) {
                log.debug("add new path to universe " +
                          path.getLastPathComponent());
            }
            if (!referentiel && ProgramDto.class.equals(internalClass)) {
                if (log.isDebugEnabled()) {
                    log.debug("Adding program childs path (" +
                              node.getChildCount() + ")");
                }
                // on ajoute toutes les marees du program
                Enumeration<?> childs = node.children();
                while (childs.hasMoreElements()) {
                    Object o1 = childs.nextElement();
                    TreePath path1 = path.pathByAddingChild(o1);
                    if (!universe.contains(path1)) {
                        if (log.isDebugEnabled()) {
                            log.debug("adding path for node " + o1);
                        }
                        universe.add(path1);
                    }
                }
            }
            if (TripSeineDto.class.equals(internalClass)) {
                TreePath parentPath = path.getParentPath();

                if (!universe.contains(parentPath)) {
                    // on ajoute son parent (et tous ses freres)
                    universe.add(parentPath);
                    ObserveNode parentNode = node.getParent();
                    if (log.isDebugEnabled()) {
                        log.debug("Adding program with his childs (" +
                                  parentNode.getChildCount() + ")");
                    }
                    Enumeration<?> childs = parentNode.children();
                    while (childs.hasMoreElements()) {
                        Object o1 = childs.nextElement();
                        TreePath path1 = parentPath.pathByAddingChild(o1);
                        if (!universe.contains(path1)) {
                            if (log.isDebugEnabled()) {
                                log.debug("adding path for node " + o1);
                            }
                            universe.add(path1);
                        }
                    }
                }
            }
        }

        boolean pathSelected = isPathSelected(path);

        if (log.isDebugEnabled()) {
            log.debug("node " + node + ", path selected ? " + pathSelected);
        }

        if (referentiel && node.isStringNode()) {

            Set<Class<? extends ReferentialDto>> subClasses = Sets.newHashSet();

            for (Enumeration<ObserveNode> children = node.children(); children.hasMoreElements(); ) {
                subClasses.add((Class<? extends ReferentialDto>) children.nextElement().getInternalClass());
            }

            if (pathSelected) {
                if (log.isDebugEnabled()) {
                    log.debug("Will remove all referentiels on " + node);
                }
                dataModel.removeAllReferentiel(subClasses);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Will add all referentiels on " + node);
                }
                dataModel.addAllReferentiel(subClasses);
            }
            return;
        }

        if (referentiel) {

            if (pathSelected) {
                if (log.isDebugEnabled()) {
                    log.debug("Will remove referentiel " + internalClass + " to model");
                }
                dataModel.removeSelectedReferentiel(internalClass);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Will add referentiel " + internalClass + " to model");
                }
                dataModel.addSelectedReferentiel((Class<? extends ReferentialDto>) internalClass);
            }
            return;
        }

        if (node instanceof AbstrctReferenceNodeSupport) {
            AbstractReference dto = ((AbstrctReferenceNodeSupport) node).getEntity();
            if (log.isDebugEnabled()) {
                log.debug("bean " + dto.getId());
            }
            if (pathSelected) {
                if (log.isDebugEnabled()) {
                    log.debug("Already add ? remove it.");
                }
                removeFromDataModel(dto);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Will add selection to model");
                }
                addToDataModel(dto);
            }
        }
    }

    @Override
    public void setSelectionPaths(TreePath[] paths) {
        // do nothing
    }

    @Override
    public void addSelectionPath(TreePath path) {
        // do nothing
    }

    @Override
    public void addSelectionPaths(TreePath[] paths) {
        // do nothing
    }

    @Override
    public void removeSelectionPath(TreePath path) {
        // do nothing
    }

    @Override
    public void removeSelectionPaths(TreePath[] paths) {
        // do nothing
    }

    @Override
    public TreePath getSelectionPath() {
        return selection == null ? null : selection[0];
    }

    @Override
    public TreePath[] getSelectionPaths() {
        return selection;
    }

    @Override
    public int getSelectionCount() {
        return selection == null ? 0 : selection.length;
    }

    @Override
    public boolean isPathSelected(TreePath path) {
        if (dataModel == null) {
            return false;
        }
        Object o = path.getLastPathComponent();
        ObserveNode node = AbstractObserveTreeCellRenderer.getNode(o);
        if (node != null) {

            Class<?> internalClass = node.getInternalClass();
            if (node.isReferentielNode() && node.isStringNode()) {
                Set<Class<? extends ReferentialDto>> subClasses = Sets.newHashSet();

                for (Enumeration<ObserveNode> children = node.children(); children.hasMoreElements(); ) {
                    subClasses.add((Class<? extends ReferentialDto>) children.nextElement().getInternalClass());
                }

                boolean result = dataModel.isReferentielSelectAll(subClasses);
                if (log.isTraceEnabled()) {
                    log.trace("selectModel use full referentiel " +
                              internalClass + " ? " + result);
                }
                return result;
            }
            if (node.isReferentielNode()) {
                boolean result = dataModel.isSelectedReferentiel(internalClass);
                if (log.isTraceEnabled()) {
                    log.trace("selectModel use referentiel " +
                              internalClass + " ? " + result);
                }
                return result;
            }

            if (log.isTraceEnabled()) {
                log.trace("begin data node " + node);
            }

            if (node instanceof AbstrctReferenceNodeSupport) {
                boolean result = dataModel.isSelectedData(((AbstrctReferenceNodeSupport) node).getEntity());
                if (log.isTraceEnabled()) {
                    log.trace("selectModel contains the program ? " + result);
                }
                return result;
            }


        }
        return false;
    }

    @Override
    public boolean isSelectionEmpty() {
        return selection == null || selection.length == 0;
    }

    @Override
    public void clearSelection() {
        if (dataModel == null) {
            return;
        }
        selection = null;
        dataModel.removeAll();
    }

    @Override
    public RowMapper getRowMapper() {
        return rowMapper;
    }

    @Override
    public void setRowMapper(RowMapper newMapper) {
        rowMapper = newMapper;
    }

    @Override
    public int[] getSelectionRows() {
        // on doit recalcule cette valeur a chaque fois
        //This is currently rather expensive.Needs
        // to be better support from ListSelectionModel to speed this up.
        return selectionRows;
    }

    @Override
    public int getMinSelectionRow() {
        return minSelectionRow;
    }

    @Override
    public int getMaxSelectionRow() {
        return maxSelectionRow;
    }

    @Override
    public int getLeadSelectionRow() {
        return leadRow;
    }

    @Override
    public TreePath getLeadSelectionPath() {
        return leadPath;
    }

    @Override
    public boolean isRowSelected(int row) {
        if (!isSelectionEmpty()) {
            for (int i : selectionRows) {
                if (row == i) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void resetRowSelection() {
        if (log.isDebugEnabled()) {
            log.debug("start rebuild row values...");
        }
        leadRow = minSelectionRow = maxSelectionRow = -1;
        selectionRows = null;
        if (rowMapper == null || selection == null || selection.length == 0) {
            return;
        }

        int[] rows = rowMapper.getRowsForPaths(selection);

        if (rows != null) {
            int invisCount = 0;

            for (int counter = rows.length - 1; counter >= 0; counter--) {
                if (rows[counter] == -1) {
                    invisCount++;
                }
            }
            if (invisCount > 0) {
                if (invisCount == rows.length) {
                    rows = null;
                } else {
                    int[] tempRows = new int[rows.length - invisCount];

                    for (int counter = rows.length - 1, visCounter = 0;
                         counter >= 0; counter--) {
                        if (rows[counter] != -1) {
                            tempRows[visCounter++] = rows[counter];
                        }
                    }
                    rows = tempRows;
                }
            }
        }

        selectionRows = rows;

        if (isSelectionEmpty()) {
            leadPath = null;
            leadIndex = -1;
            leadRow = -1;
        } else {
            leadPath = selection[0];
            leadIndex = 0;
            leadRow = selectionRows[0];
        }

        int selectionLength = selectionRows.length;

        minSelectionRow = -1;
        maxSelectionRow = 0;
        for (int i = 0; i < selectionLength; i++) {
            int row = selectionRows[i];
            if (row > maxSelectionRow) {
                maxSelectionRow = row;
            }
            if (row < minSelectionRow) {
                minSelectionRow = row;
            }
        }
    }

    /**
     * Adds x to the list of listeners that are notified each time the set of
     * selected TreePaths changes.
     *
     * @param x the new listener to be added
     */
    @Override
    public void addTreeSelectionListener(TreeSelectionListener x) {
        listenerList.add(TreeSelectionListener.class, x);
    }

    /**
     * Removes x from the list of listeners that are notified each time the set
     * of selected TreePaths changes.
     *
     * @param x the listener to remove
     */
    @Override
    public void removeTreeSelectionListener(TreeSelectionListener x) {
        listenerList.remove(TreeSelectionListener.class, x);
    }

    /**
     * Returns an array of all the tree selection listeners registered on this
     * model.
     *
     * @return all of this model's {@code TreeSelectionListener}s or an
     * empty array if no tree selection listeners are currently
     * registered
     * @see #addTreeSelectionListener
     * @see #removeTreeSelectionListener
     * @since 1.4
     */
    public TreeSelectionListener[] getTreeSelectionListeners() {
        return listenerList.getListeners(TreeSelectionListener.class);
    }

    /**
     * Adds a PropertyChangeListener to the listener list. The listener is
     * registered for all properties.
     *
     * A PropertyChangeEvent will get fired when the selection mode changes.
     *
     * @param listener the PropertyChangeListener to be added
     */
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (changeSupport == null) {
            changeSupport = new PropertyChangeSupport(this);
        }
        changeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Removes a PropertyChangeListener from the listener list. This removes a
     * PropertyChangeListener that was registered for all properties.
     *
     * @param listener the PropertyChangeListener to be removed
     */

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        if (changeSupport == null) {
            return;
        }
        changeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Returns an array of all the property change listeners registered on this
     * {@code DefaultTreeSelectionModel}.
     *
     * @return all of this model's {@code PropertyChangeListener}s or an
     * empty array if no property change listeners are currently
     * registered
     * @see #addPropertyChangeListener
     * @see #removePropertyChangeListener
     * @since 1.4
     */
    public PropertyChangeListener[] getPropertyChangeListeners() {
        if (changeSupport == null) {
            return EMPTY_PROPERTY_CHANGE_LISTENERS;
        }
        return changeSupport.getPropertyChangeListeners();
    }

    protected void fireValueChanged(TreeSelectionEvent e) {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        // TreeSelectionEvent e = null;
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (TreeSelectionListener.class.equals(listeners[i])) {
                // Lazily create the event:
                // if (e == null)
                // e = new ListSelectionEvent(this, firstIndex, lastIndex);
                ((TreeSelectionListener) listeners[i + 1]).valueChanged(e);
            }
        }
    }

    protected void addToDataModel(AbstractReference dto) {

        if (log.isTraceEnabled()) {
            log.trace("selectModel add " + dto.getId());
        }
        dataModel.addSelectedData(dto);

    }

    protected void removeFromDataModel(AbstractReference dto) {
        if (log.isTraceEnabled()) {
            log.trace("selectModel remove " + dto.getId());
        }
        dataModel.removeSelectedData(dto);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        if (DataSelectionModel.PROPERTY_SELECTED_DATA.equals(propertyName) ||
            DataSelectionModel.PROPERTY_SELECTED_REFERENTIEL.equals(propertyName)) {
            // la selection a changee
            Object value = evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug("selection data changed " + value);
            }
        }
        updateModel();
    }

    protected void updateModel() {
        if (rowMapper == null || universe == null) {
            return;
        }

        // recalcule de la selection

        List<TreePath> oldSelection = isSelectionEmpty() ?
                                      Collections.emptyList() :
                                      Arrays.asList(selection);

        List<TreePath> newSelection = new ArrayList<>();

        for (TreePath p : universe) {
            if (isPathSelected(p)) {
                newSelection.add(p);
            }
        }

        int selectionLength = newSelection.size();

        selection = newSelection.toArray(new TreePath[selectionLength]);

        // calcul du leadPath

        TreePath oldLeadPath = leadPath;

        if (isSelectionEmpty()) {
            leadPath = null;
            leadIndex = -1;
        } else {
            leadPath = selectionLength > 0 ? selection[0] : null;
            leadIndex = 0;
        }

        if (log.isDebugEnabled()) {
            log.debug("new selection length = " + selectionLength);
        }

        // recalcule des donnees de rows
        resetRowSelection();

        if (log.isDebugEnabled()) {
            log.debug("new selection length = " +
                      (selectionRows == null ? 0 : selectionRows.length));
            if (log.isDebugEnabled()) {
                log.debug("selected rows = " + Arrays.toString(selectionRows));
            }
        }

        // calcule des paths qui ont changés
        List<TreePath> obsoleteSelection = new ArrayList<>(oldSelection);
        obsoleteSelection.removeAll(newSelection);

        List<TreePath> changedPaths = new ArrayList<>(oldSelection);
        changedPaths.addAll(newSelection);

        TreePath[] treePaths = changedPaths.toArray(new TreePath[changedPaths.size()]);

        // notification des modifications sur la selection
        TreeSelectionEvent event = new TreeSelectionEvent(
                this,
                treePaths,
                new boolean[treePaths.length],
                oldLeadPath,
                leadPath
        );
        fireValueChanged(event);
    }

}
