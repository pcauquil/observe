<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI superGenericType='HarbourDto'>

  <style source="ReferenceEntity.jcss"/>

  <import>
    fr.ird.observe.services.dto.constants.ReferenceStatus
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.HarbourDto
    fr.ird.observe.services.dto.referential.CountryDto
    fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel

    jaxx.runtime.swing.editor.bean.BeanComboBox

    org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true' context='ui-create'
                 beanClass='fr.ird.observe.services.dto.referential.HarbourDto'
                 errorTableModel='{getErrorTableModel()}'>

    <field name='longitude' component='coordinates'/>
    <field name='latitude' component='coordinates'/>
    <field name='quadrant' component='coordinates'/>
    <field name='name' component='harbourName'/>

  </BeanValidator>

  <!-- model -->
  <HarbourUIModel id='model'/>

  <!-- edit bean -->
  <HarbourDto id='bean'/>

  <Table id='editTable'>

    <!-- uri -->
    <row>
      <cell anchor="west">
        <JLabel id='uriLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='uri' onKeyReleased='getBean().setUri(uri.getText())'/>
      </cell>
    </row>

    <!-- code / status -->
    <row>
      <cell anchor="west">
        <JLabel id='codeStatusLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JPanel id='codeStatusPanel' layout='{new BorderLayout()}'>
          <JTextField id='code' constraints='BorderLayout.WEST'
                      onKeyReleased='getBean().setCode(code.getText())'/>
          <EnumEditor id='status' constraints='BorderLayout.CENTER'
                      constructorParams='ReferenceStatus.class'
                      genericType='ReferenceStatus'
                      onItemStateChanged='getBean().setStatus((ReferenceStatus)status.getSelectedItem())'/>
        </JPanel>
      </cell>
    </row>

    <!-- locode -->
    <row>
      <cell anchor='west'>
        <JLabel id='locodeLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='locode' onKeyReleased='getBean().setLocode(locode.getText())'/>
      </cell>
    </row>

    <!-- name -->
    <row>
      <cell anchor='west'>
        <JLabel id='harbourNameLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='harbourName' onKeyReleased='getBean().setName(harbourName.getText())'/>
      </cell>
    </row>

    <!-- country -->
    <row>
      <cell anchor='west'>
        <JLabel id='countryLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <BeanComboBox id='country' constructorParams='this' genericType='ReferentialReference&lt;CountryDto&gt;' _entityClass='CountryDto.class'/>
      </cell>
    </row>

    <!-- coordinates -->
    <row>
      <cell anchor='west'>
        <JLabel id='coordinatesLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <CoordinatesEditor id='coordinates' constructorParams='this'/>
      </cell>

    </row>

    <!-- needComment -->
    <row>
      <cell anchor='east' weightx="1" fill="both" columns="2">
        <JCheckBox id='needComment'
                   onItemStateChanged='getBean().setNeedComment(needComment.isSelected())'/>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI>
