/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.validate;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUI;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.config.SelectDataUI;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.util.ObserveValidationMessageTableRenderer;
import fr.ird.observe.application.swing.validation.ValidationModelMode;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.actions.validate.ValidateDataRequest;
import fr.ird.observe.services.service.actions.validate.ValidateDataResult;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsRequest;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsResult;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDto;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtoType;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtos;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import fr.ird.observe.services.service.actions.validate.ValidationMessage;
import fr.ird.observe.services.service.actions.validate.ValidatorDto;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.context.JAXXInitialContext;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

//import jaxx.runtime.validator.swing.SwingValidator;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ValidateUIHandler extends AdminTabUIHandler {

    public static final String[] EMPTY_STRING_ARRAY = new String[]{};

    public static final String LINE =
            "--------------------------------------------------------------------------------";

    /** Logger */
    private static final Log log = LogFactory.getLog(ValidateUIHandler.class);

    protected ObserveSwingDataSource source;

    protected Decorator<TripSeineDto> dTrip;

    protected Decorator<ProgramDto> dProgram;

    public ValidateUIHandler(AdminTabUI ui) {
        super(ui);
    }

    public void initTabUI(AdminUI ui, ValidateUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        JTable messageTable = tabUI.getMessageTable();

        messageTable.setDefaultRenderer(Object.class, new ObserveValidationMessageTableRenderer());
        messageTable.getRowSorter().setSortKeys(
                Collections.singletonList(new RowSorter.SortKey(0, SortOrder.ASCENDING)));
        SwingUtil.setI18nTableHeaderRenderer(
                messageTable,
                n("observe.actions.validate.validator.scope.header"),
                n("observe.actions.validate.validator.scope.header.tip"),
                n("observe.actions.validate.validator.field.header"),
                n("observe.actions.validate.validator.field.header.tip"),
                n("observe.actions.validate.validator.message.header"),
                n("observe.actions.validate.validator.message.header.tip"));
        SwingUtil.fixTableColumnWidth(messageTable, 0, 25);

        ValidateEntityListCellRenderer listRenderer =
                new ValidateEntityListCellRenderer(tabUI.getStepModel());
        tabUI.getTypeList().setCellRenderer(listRenderer);
        tabUI.getRefList().setCellRenderer(listRenderer);

        tabUI.getStartButton().setText(t("observe.actions.synchro.launch.operation", t(tabUI.getStep().getOperationLabel())));

        // initialisation de l'ui de configuration
        if (log.isInfoEnabled()) {
            log.info("Init extra configuration for " + tabUI.getName());
        }

        ConfigUI configUI = (ConfigUI) ui.getStepUI(AdminStep.CONFIG);
        JAXXInitialContext tx = new JAXXInitialContext().add(configUI).add(this);
        ValidateConfigUI extraConfig = new ValidateConfigUI(tx);
        configUI.getExtraConfig().add(extraConfig);

        final SelectDataUI selectTabUI = (SelectDataUI) ui.getStepUI(AdminStep.SELECT_DATA);

        getModel().getValidateModel().addPropertyChangeListener(ValidateModel.PROPERTY_MODEL_MODE, evt -> {
            ValidationModelMode value = (ValidationModelMode) evt.getNewValue();
            if (value == null) {
                // rien a faire pour le moment...
                return;
            }
            DataSelectionModel selectDataModel;
            selectDataModel = selectTabUI.getSelectDataModel();
            if (log.isDebugEnabled()) {
                log.debug("validation model changed to " + value);
            }
            switch (value) {
                case REFERENTIEL:

                    selectDataModel.setUseData(false);
                    selectDataModel.setUseReferentiel(true);
                    break;
                case DATA:
                    selectDataModel.setUseData(true);
                    selectDataModel.setUseReferentiel(false);
                    break;
                case ALL:
                    selectDataModel.setUseData(true);
                    selectDataModel.setUseReferentiel(true);
                    break;
            }
            //updateModel();
        });

        selectTabUI.getModel().addPropertyChangeListener(AdminUIModel.SELECTION_MODEL_CHANGED_PROPERTY_NAME, evt -> {
            AdminUIModel model1 = (AdminUIModel) evt.getSource();
            if (!model1.containsStep(selectTabUI.getStep())) {
                // avoid multi-cast
                return;
            }
            DataSelectionModel value = (DataSelectionModel) evt.getNewValue();
            if (log.isInfoEnabled()) {
                log.info("selection model changed to " + value);
            }
            updateSelectionModel(selectTabUI);
        });

        ImmutableSet<ValidatorDto> validators = ObserveSwingApplicationContext.get().getValidators();

        ValidateModel stepModel = model.getValidateModel();

        stepModel.setAllValidators(validators);
    }

    public void updateState(ValidateUI tabUI, WizardState newState) {

        super.updateState(tabUI, newState);

        if (newState == WizardState.NEED_FIX) {
            updateTypes();
            tabUI.resumeLabel.setText(t("observe.actions.validate.save.reportFile", tabUI.getStepModel().getReportFile()));
            String actionText;
            if (tabUI.getStepModel().isGenerateReport()) {
                actionText = t("observe.actions.validate.save");
            } else {
                actionText = t("observe.actions.validate.continue.with.no.save.report");
            }
            tabUI.saveReport.setText(actionText);
            return;
        }
        if (newState == WizardState.RUNNING) {
            tabUI.typeList.clearSelection();
            tabUI.refList.clearSelection();
            tabUI.messageTable.clearSelection();
            tabUI.typeModel.clear();
            tabUI.refModel.clear();
            tabUI.messagesModel.clear();
        }
    }

    public void updateTypes() {

        ValidateUI tabUI = (ValidateUI) ui;

        DefaultListModel<Class<?>> typeModel = tabUI.typeModel;
        tabUI.typeSelectionModel.clearSelection();
        typeModel.clear();

        Set<Class> messageTypes = (Set) tabUI.getStepModel().getMessageTypes();

        List<Class> classes = ObserveI18nDecoratorHelper.sortTypes(messageTypes);

        for (Class<?> e : classes) {

            typeModel.addElement(e);

        }

        tabUI.typeList.setSelectedIndex(0);
    }

    public void updateSelectedType() {


        ValidateUI tabUI = (ValidateUI) ui;

        getModel().setBusy(true);
        // on nettoye le modele des refs
        try {
            tabUI.refSelectionModel.clearSelection();
            tabUI.refModel.clear();

            Object o = tabUI.typeList.getSelectedValue();
            if (log.isInfoEnabled()) {
                log.info("new selected type = " + o);
            }
            if (o == null) {
                return;
            }

            Class<?> type = (Class<?>) o;

            List<AbstractReference> refs = tabUI.getStepModel().getMessagesDto(type);
            for (AbstractReference ref : Iterables.limit(refs, 100)) {
                if (log.isDebugEnabled()) {
                    log.debug("add ref = " + ref);
                }
                tabUI.refModel.addElement(ref);
            }
            tabUI.refList.setSelectedIndex(0);
        } finally {
            getModel().setBusy(false);
        }

    }

    public void updateSelectedRef() {


        ValidateUI tabUI = (ValidateUI) ui;

        // on nettoye le modele des messages
        tabUI.messageTable.clearSelection();
        tabUI.messagesModel.clear();

        Object o = tabUI.refList.getSelectedValue();

        if (log.isDebugEnabled()) {
            log.debug("new selected ref = " + o);
        }

        if (o == null) {
            return;
        }

        AbstractReference ref = (AbstractReference) o;

        if (log.isDebugEnabled()) {
            log.debug(ref);
        }

        ValidateResultForDto resultForDto = tabUI.getStepModel().getMessages(ref);

        ImmutableSet<ValidationMessage> messages = resultForDto.getMessages();

        tabUI.messagesModel.setMessages(messages);


    }

    public void startAction() {

        addAdminWorker(((ValidateUI) ui).getStartButton().getToolTipText(), this::doAction);
    }

    public WizardState doAction() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug(this);
        }

        WizardState init = initDB();

        if (init != null) {
            // une erreur ou une annulation
            return init;
        }

        launchValidation();

        Map<Class<? extends IdDto>, ValidateResultForDtoType> messages = model.getValidateModel().getMessages();
        if (messages == null || messages.isEmpty()) {

            // pas de validation ou bien pas d'erreurs rencontrées

            sendMessage(t("observe.actions.validate.message.nothing.to.do"));
            sendMessage(t("observe.actions.validate.message.operation.done", new Date()));

            return WizardState.SUCCESSED;
        }

        sendMessage(
                t("observe.actions.validate.message.operation.needFix", new Date()));

        return WizardState.NEED_FIX;
    }

    protected WizardState initDB() throws Exception {


        // on recupere la source de données
        source = model.getSafeLocalSource(false);

        DecoratorService decoratorService = getDecoratorService();

        dTrip = decoratorService.getDecoratorByType(TripSeineDto.class);

        dProgram = decoratorService.getDecoratorByType(ProgramDto.class);

        openSource(source);

        // recuperation des validateurs du modele

        ValidateModel validationModel = model.getValidateModel();
        Set<ValidatorDto> validators = validationModel.getValidators();

        sendMessage(t("observe.actions.validate.message.use.storage",
                      source.getLabel()));

        sendMessage(t("observe.actions.validate.message.prepare.validators"));

        if (!validators.isEmpty()) {

            // des validateurs ont été trouvés

            for (ValidatorDto v : validators) {
                String label = t(ObserveI18nDecoratorHelper.getTypeI18nKey(v.getType()));
                sendMessage(t("observe.actions.validate.message.detected", label));
            }
        } else {
            sendMessage(t("observe.actions.validate.message.no.validation.detected"));
        }

        return null;
    }

    public void launchValidation() throws Exception {

        // on vide les anciens messages
        ValidateModel stepModel = model.getValidateModel();
        stepModel.setMessages(null);

        DataSelectionModel dataModel = model.getSelectionDataModel();

        ObserveSwingDataSource dataSourceToValidate = model.getLocalSource();

        if (!dataSourceToValidate.isOpen()) {
            dataSourceToValidate.open();
        }

        ValidateService validateService = dataSourceToValidate.newValidateService();

        Map<Class<? extends IdDto>, ValidateResultForDtoType> messages = Maps.newHashMap();

        if (dataModel.isUseReferentiel()) {

            // validation des referentiels selectionnes
            ValidateReferentialsRequest request = new ValidateReferentialsRequest();

            request.setReferentialTypes(ImmutableSet.copyOf(dataModel.getSelectedReferentiel()));
            request.setScopes(ImmutableSet.copyOf(stepModel.getScopes()));
            request.setValidationContext(stepModel.getContextName());

            ValidateReferentialsResult result = validateService.validateReferentials(request);

            messages.putAll(result.getResultByType());
        }

        if (dataModel.isUseData()) {

            // validation des donnees observateur selectionnee
            ValidateDataRequest request = new ValidateDataRequest();

            request.setDataIds(ImmutableSet.copyOf(dataModel.getSelectedData().stream()
                                                            .map(DataReference.ID_FUNCTION)
                                                            .collect(Collectors.toSet())));
            request.setScopes(ImmutableSet.copyOf(stepModel.getScopes()));
            request.setValidationContext(stepModel.getContextName());

            ValidateDataResult result = validateService.validateData(request);

            messages.putAll(result.getResultByType());
        }

        dataSourceToValidate.close();

        stepModel.setMessages(messages);

    }

    public void saveReport() {
        WizardState finalState = null;
        ValidateModel validationModel = model.getValidateModel();
        try {
            if (validationModel.isGenerateReport()) {

                sendMessage(
                        t("observe.actions.validate.message.save.report", validationModel.getReportFile()));

                generateReportFile(validationModel);
            } else {
                sendMessage(
                        t("observe.actions.validate.message.not.save.report"));
            }
            finalState = WizardState.SUCCESSED;
        } catch (Exception e) {
            validationModel.setError(e);
            finalState = WizardState.FAILED;
        } finally {
            model.setStepState(AdminStep.VALIDATE, finalState);
        }
    }

    //FIXME A remplacer par une template
    public void generateReportFile(ValidateModel validationModel) throws IOException {
        File reportFile = validationModel.getReportFile();
        if (log.isInfoEnabled()) {
            log.info("save report in " + reportFile);
        }

        DecoratorService service = getDecoratorService();
        StringBuilder builder = new StringBuilder();
        builder.append(LINE).append('\n');

        builder.append(t("observe.actions.validate.report.title", new Date())).append('\n');
        builder.append(t("observe.actions.validate.report.scopes", validationModel.getScopes())).append('\n');
        builder.append(t("observe.actions.validate.report.contextName", validationModel.getContextName())).append('\n');

        Map<Class<? extends IdDto>, ValidateResultForDtoType> messages = validationModel.getMessages();

        builder.append(t("observe.actions.validate.report.entities.with.messages", messages.size())).append('\n');
        builder.append(LINE).append('\n').append('\n');

        for (ValidateResultForDtoType validateResultForDtoType : messages.values()) {

            ImmutableSet<ValidateResultForDto> validateResultForDtos = validateResultForDtoType.getValidateResultForDto();
            for (ValidateResultForDto validateResultForDto : validateResultForDtos) {
                AbstractReference referenceDto = validateResultForDto.getDto();

                String refStr = service.getReferenceDecorator(referenceDto.getType()).toString(referenceDto);

                Set<ValidationMessage> refMessages = validateResultForDto.getMessages();

                EnumSet<NuitonValidatorScope> scopes = ValidateResultForDtos.getScopes(validateResultForDto);

                builder.append(t("observe.actions.validate.report.entity", referenceDto.getId(), refStr, refMessages.size(), scopes)).append('\n');
                for (NuitonValidatorScope scope : scopes) {

                    List<ValidationMessage> messagesByScope = ValidateResultForDtos.scopeMessageFilter(scope, validateResultForDto);

                    for (ValidationMessage message : messagesByScope) {

                        builder.append(message.getScope() + " - " + message.getMessage()).append('\n');

                    }

                    builder.append('\n');
                }

            }
            builder.append(LINE).append('\n').append('\n');
        }

        String content = builder.toString();
        if (log.isInfoEnabled()) {
            log.info(content);
        }
        FileUtils.write(reportFile, content, Charsets.UTF_8.name());
    }


    // ------------------------------------------------------------------------
    // -- ValidateConfgUI methods
    // ------------------------------------------------------------------------

    public void updateValidationScopes(JCheckBox checkBox) {
        NuitonValidatorScope scope = getValidatorScope(checkBox);
        ValidateModel validateModel = getModel().getValidateModel();
        if (checkBox.isSelected()) {
            // ajout du scope
            validateModel.addScope(scope);
        } else {
            // supprime le scope
            validateModel.removeScope(scope);
        }
    }

    public ComboBoxModel updateComboModel(Object... datas) {
        if (datas == null) {
            return new DefaultComboBoxModel();
        }
        return new DefaultComboBoxModel(datas);
    }

    public String updateValidatorResumeLabel(boolean valid) {

        ValidateModel validateModel = getModel().getValidateModel();

        if (validateModel == null) {
            return null;
        }
        return t("observe.actions.validate.selected.validators", validateModel.getValidators().size());
    }

    public AdminStep getObjectOperation(JCheckBox checkBox) {
        return AdminStep.valueOf(checkBox.getName());
    }

    public NuitonValidatorScope getValidatorScope(JCheckBox checkBox) {
        return (NuitonValidatorScope) checkBox.getClientProperty("value");
    }

    public boolean isScopeSelected(Set<NuitonValidatorScope> scopes, JCheckBox checkBox) {
        NuitonValidatorScope scope = getValidatorScope(checkBox);
        return scopes.contains(scope);
    }

    public boolean isObjectOperationSelected(Set<AdminStep> objectOperations, JCheckBox checkBox) {
//        AdminStep scope = getObjectOperation(checkBox);
        return objectOperations.contains(getObjectOperation(checkBox));
    }

    public void chooseValidationReportFile(ValidateConfigUI configUI) {
        File f = UIHelper.chooseDirectory(
                configUI,
                t("observe.actions.validate.title.choose.report.directory"),
                t("observe.actions.validate.choose.report.directory"),
                new File(configUI.validationReportDirectoryText.getText())
        );
        changeValidationReportDirectory(configUI, f);
    }

    public void changeValidationReportDirectory(ValidateConfigUI configUI, File f) {
        getModel().getValidateModel().setReportFile(new File(f, configUI.validationReportFilenameText.getText()));
    }

    public void changeValidationReportFilename(ValidateConfigUI configUI, String filename) {
        getModel().getValidateModel().setReportFile(new File(configUI.validationReportDirectoryText.getText(), filename));
    }

}
