package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

import java.util.Set;

/**
 * Created on 9/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class VesselUIModel extends ContentReferenceUIModel<VesselDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_OTHER_TAB_VALID = "otherTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(VesselDto.PROPERTY_URI,
                                               VesselDto.PROPERTY_CODE,
                                               VesselDto.PROPERTY_STATUS,
                                               VesselDto.PROPERTY_KEEL_CODE,
                                               VesselDto.PROPERTY_VESSEL_TYPE,
                                               VesselDto.PROPERTY_VESSEL_SIZE_CATEGORY,
                                               VesselDto.PROPERTY_NEED_COMMENT,
                                               VesselDto.PROPERTY_FLAG_COUNTRY,
                                               VesselDto.PROPERTY_FLEET_COUNTRY,
                                               VesselDto.PROPERTY_LABEL1,
                                               VesselDto.PROPERTY_LABEL2,
                                               VesselDto.PROPERTY_LABEL3,
                                               VesselDto.PROPERTY_LABEL4,
                                               VesselDto.PROPERTY_LABEL5,
                                               VesselDto.PROPERTY_LABEL6,
                                               VesselDto.PROPERTY_LABEL7,
                                               VesselDto.PROPERTY_LABEL8).build();

    public static final Set<String> OTHER_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(VesselDto.PROPERTY_CAPACITY,
                                               VesselDto.PROPERTY_CHANGE_DATE,
                                               VesselDto.PROPERTY_COMMENT,
                                               VesselDto.PROPERTY_LENGTH,
                                               VesselDto.PROPERTY_LENGTH,
                                               VesselDto.PROPERTY_POWER,
                                               VesselDto.PROPERTY_SEARCH_MAXIMUM,
                                               VesselDto.PROPERTY_YEAR_SERVICE).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean otherTabValid;

    public VesselUIModel() {
        super(VesselDto.class,
              new String[]{
                      VesselDto.PROPERTY_VESSEL_TYPE,
                      VesselDto.PROPERTY_VESSEL_SIZE_CATEGORY,
                      VesselDto.PROPERTY_FLAG_COUNTRY,
                      VesselDto.PROPERTY_KEEL_CODE,
                      VesselDto.PROPERTY_FLEET_COUNTRY,
                      VesselDto.PROPERTY_YEAR_SERVICE,
                      VesselDto.PROPERTY_LENGTH,
                      VesselDto.PROPERTY_CAPACITY,
                      VesselDto.PROPERTY_POWER,
                      VesselDto.PROPERTY_SEARCH_MAXIMUM,
                      VesselDto.PROPERTY_CHANGE_DATE},
              new String[]{VesselUI.BINDING_VESSEL_TYPE_SELECTED_ITEM,
                           VesselUI.BINDING_VESSEL_SIZE_CATEGORY_SELECTED_ITEM,
                           VesselUI.BINDING_FLAG_COUNTRY_SELECTED_ITEM,
                           VesselUI.BINDING_KEEL_CODE_MODEL,
                           VesselUI.BINDING_FLEET_COUNTRY_MODEL,
                           VesselUI.BINDING_YEAR_SERVICE_MODEL,
                           VesselUI.BINDING_LENGTH_MODEL,
                           VesselUI.BINDING_CAPACITY_MODEL,
                           VesselUI.BINDING_POWER_MODEL,
                           VesselUI.BINDING_SEARCH_MAXIMUM_MODEL,
                           VesselUI.BINDING_CHANGE_DATE_DATE}
        );
    }

    public boolean isOtherTabValid() {
        return otherTabValid;
    }

    public void setOtherTabValid(boolean otherTabValid) {
        Object oldValue = isOtherTabValid();
        this.otherTabValid = otherTabValid;
        firePropertyChange(PROPERTY_OTHER_TAB_VALID, oldValue, otherTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
