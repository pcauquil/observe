/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ObserveContentUI;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import jaxx.runtime.SwingUtil;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import static org.nuiton.i18n.I18n.t;

/**
 * Pour implanter les actions communes.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class AbstractUIAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private final ObserveMainUI mainUI;

    private final String actionId;

    public AbstractUIAction(ObserveMainUI mainUI,
                            String actionId,
                            String label,
                            String shortDescription,
                            String actionIcon) {
        super(t(label), SwingUtil.getUIManagerActionIcon(actionIcon));
        this.actionId = actionId;
        this.mainUI = mainUI;
        putValue(SHORT_DESCRIPTION, t(shortDescription));
    }

    public ObserveMainUI getMainUI() {
        return mainUI;
    }

    public String getActionId() {
        return actionId;
    }

    public void initAction(ObserveContentUI<?> ui, AbstractButton editor) {
        editor.setAction(this);
        editor.putClientProperty("mainUI", mainUI);
    }

    public void updateAction(ObserveContentUI<?> ui, AbstractButton editor) {
        editor.putClientProperty("ui", ui);
        String tip = (String) editor.getClientProperty("toolTipText");
        if (tip != null) {
            editor.setToolTipText(tip);
        }
        String text = (String) editor.getClientProperty("text");
        if (text != null) {
            editor.setText(text);
        }
        String actionIcon = (String) editor.getClientProperty("actionIcon");
        if (actionIcon != null) {
            Icon icon = SwingUtil.getUIManagerActionIcon(actionIcon);
            editor.setIcon(icon);
        }
    }

    public static boolean isOpenActivityNodeCollapsed(JTree tree,
                                                      ObserveTreeHelper treeHelper,
                                                      DataContext dataContext) {

        // on regarde si le noeud de l'activity ouverte est collapsé

        ObserveNode node = treeHelper.getSelectedNode();

        TreePath path = tree.getSelectionPath();

        String id = dataContext.getOpenActivityId();

        ObserveNode node1 = treeHelper.findNode(node, id);

        TreePath activityPath = path.pathByAddingChild(node1);

        return tree.isCollapsed(activityPath);
    }
}
