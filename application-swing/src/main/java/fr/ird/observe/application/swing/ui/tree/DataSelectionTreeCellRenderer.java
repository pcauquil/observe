/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JTree;
import java.awt.Color;
import java.awt.Component;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Le renderer pour décorer l'arbre de sélection des données.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class DataSelectionTreeCellRenderer extends AbstractObserveTreeCellRenderer {

    /** Logger */
    private static final Log log = LogFactory.getLog(DataSelectionTreeCellRenderer.class);

    private static final long serialVersionUID = 1L;

    protected List<DataReference<?>> existingTrips;

    public List<DataReference<?>> getExistingTrips() {
        return existingTrips;
    }

    public void setExistingTrips(List<DataReference<?>> existingTrips) {
        this.existingTrips = existingTrips;
    }

    public DataSelectionTreeCellRenderer() {
    }

    @Override
    protected void init() {
        super.init();
        setBackgroundNonSelectionColor(null);
        setBackgroundSelectionColor(null);
        setBackground(null);

        setTextNonSelectionColor(Color.BLACK);
        setTextSelectionColor(Color.BLUE);
    }

    @Override
    public Component getTreeCellRendererComponent(
            JTree tree,
            Object value,
            boolean sel,
            boolean expanded,
            boolean leaf,
            int row,
            boolean hasFocus) {

        // get the icon to set for the node
        ObserveNode node = getNode(value);

        if (node == null) {
            return super.getTreeCellRendererComponent(
                    tree,
                    value,
                    sel,
                    expanded,
                    leaf,
                    row,
                    hasFocus
            );
        }
        setIcon(node);

        String text = getNodeText(node);
        if (log.isDebugEnabled()) {
            text += " (" + row + ')';
            log.debug("repaint node " + text + " (selected:" + sel +
                              ") for node  " + node.getId());
        }

        boolean exist = false;

        if (TripSeineDto.class.equals(node.getInternalClass())) {

            if (existingTrips != null) {

                if (existingTrips.contains(((TripSeineNode) node).getEntity())) {
                    text = t("observe.common.exist.on.remote", text);
                    exist = true;
                }
            }
        }
        Component comp = super.getTreeCellRendererComponent(
                tree,
                text,
                sel,
                expanded,
                leaf,
                row,
                hasFocus
        );

        //FIXME-TC20100316 can not display tooltiptext...
//        delegate.setToolTipText(text);
        if (exist) {
            text = t("observe.message.warning.will.be.delete", text);
        }
        ((JComponent) comp).setToolTipText(text);
        return comp;
    }

}
