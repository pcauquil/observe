package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ReferentialSynchronizeMode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModelsBuilder;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffService;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffsEngine;
import jaxx.runtime.context.JAXXInitialContext;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionListener;
import java.awt.Color;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchroUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(ReferentialSynchroUIHandler.class);

    private final TreeSelectionListener treeSelectionListener;

    public ReferentialSynchroUIHandler(ReferentialSynchroUI ui) {
        super(ui);
        this.treeSelectionListener = evt -> updateEnabledActions();
    }

    public ReferentialSynchroModel getStepModel() {
        return model.getReferentialSynchroModel();
    }

    @Override
    public ReferentialSynchroUI getUi() {
        return (ReferentialSynchroUI) super.getUi();
    }

    public void initTabUI(AdminUI ui, ReferentialSynchroUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        ConfigUI configUI = (ConfigUI) ui.getStepUI(AdminStep.CONFIG);
        JAXXInitialContext tx = new JAXXInitialContext().add(configUI).add(this);
        ReferentialSynchroConfigUI extraConfig = new ReferentialSynchroConfigUI(tx);
        configUI.getExtraConfig().add(extraConfig);

    }

    public void doStartAction() {

        addAdminWorker(getUi().getStartAction().getToolTipText(), this::doStartAction0);

    }

    private WizardState doStartAction0() {

        ReferentialSynchroModel stepModel = getStepModel();

        ObserveSwingDataSource leftSource = getModel().getSafeLocalSource(true);
        stepModel.setLeftSource(leftSource);

        ObserveSwingDataSource rightSource = getModel().getSafeCentralSource(true);
        stepModel.setRightSource(rightSource);

        ReferentialSynchronizeDiffService leftDiffSynchronizeService = leftSource.newReferentialSynchronizeDiffService();
        ReferentialSynchronizeDiffService rightDiffSynchronizeService = rightSource.newReferentialSynchronizeDiffService();

        ReferentialSynchronizeDiffsEngine engine = new ReferentialSynchronizeDiffsEngine(leftDiffSynchronizeService, rightDiffSynchronizeService);
        stepModel.setEngine(engine);

        ReferentialSynchronizeMode synchronizeMode = stepModel.getSynchronizeMode();
        ReferentialSynchronizeTreeModelsBuilder treeModelsBuilder = new ReferentialSynchronizeTreeModelsBuilder(synchronizeMode, engine);
        Pair<ReferentialSynchronizeTreeModel, ReferentialSynchronizeTreeModel> treePair = treeModelsBuilder.build();
        stepModel.setLeftTreeModel(treePair.getLeft());
        stepModel.setRightTreeModel(treePair.getRight());

        stepModel.getTasks().removeAllElements();

        ReferentialSynchroUI ui = getUi();
        initTree(ui.getLeftTree(),
                 ui.getLeftTreePane(),
                 stepModel.getLeftTreeModel(),
                 getModel().getLocalSourceModel().getLabel(),
                 t("observe.actions.synchro.referential.message.referential.leftData.loaded"));

        initTree(ui.getRightTree(),
                 ui.getRightTreePane(),
                 stepModel.getRightTreeModel(),
                 getModel().getCentralSourceModel().getLabel(),
                 t("observe.actions.synchro.referential.message.referential.rightData.loaded"));

        return WizardState.NEED_FIX;
        
    }

    private void updateEnabledActions() {

        ReferentialSynchroModel stepModel = getStepModel();

        updateLeftTreeEnableActions(stepModel.getLeftTreeModel());
        updateRightTreeEnableActions(stepModel.getRightTreeModel());

    }

    private void updateLeftTreeEnableActions(ReferentialSynchronizeTreeModel treeModel) {

        ReferentialSynchroModel stepModel = getStepModel();

        treeModel.updateSelectedActions();
        stepModel.setCopyLeft(treeModel.isCanAdd() || treeModel.isCanUpdate());
        stepModel.setDeleteLeft(treeModel.isCanDelete());
        stepModel.setDesactivateLeft(treeModel.isCanDelete());
        stepModel.setDesactivateWithReplaceLeft(treeModel.isCanDelete());
        stepModel.setRevertLeft(treeModel.isCanRevert());
        stepModel.setSkipLeft(treeModel.isCanSkip());
    }

    private void updateRightTreeEnableActions(ReferentialSynchronizeTreeModel treeModel) {

        ReferentialSynchroModel stepModel = getStepModel();

        treeModel.updateSelectedActions();

        stepModel.setCopyRight(treeModel.isCanAdd() || treeModel.isCanUpdate());
        stepModel.setDeleteRight(treeModel.isCanDelete());
        stepModel.setDesactivateRight(treeModel.isCanDelete());
        stepModel.setDesactivateWithReplaceRight(treeModel.isCanDelete());
        stepModel.setRevertRight(treeModel.isCanRevert());
        stepModel.setSkipRight(treeModel.isCanSkip());
    }

    private void initTree(JTree tree, JScrollPane treePane, ReferentialSynchronizeTreeModel treeModel, String title, String message) {

        ReferentialSynchronizeMode newValue = getStepModel().getSynchronizeMode();

        Color color =
                (treeModel.isLeft() && newValue.isLeftWrite() || !treeModel.isLeft() && newValue.isRightWrite()) ?
                        Color.BLACK : Color.RED;

        TitledBorder border = new TitledBorder(title);
        border.setTitleColor(color);

        treePane.setBorder(border);
        tree.setModel(treeModel);
        tree.setSelectionModel(treeModel);

        treeModel.removeTreeSelectionListener(treeSelectionListener);
        treeModel.addTreeSelectionListener(treeSelectionListener);

        UIHelper.initUI(treePane, tree);
        //UIHelper.expandTree(tree);

        sendMessage(message);

        if (treeModel.isLeft()) {

            updateLeftTreeEnableActions(treeModel);

        } else {
            updateRightTreeEnableActions(treeModel);
        }

        ConfigUI configUI = (ConfigUI) parentUI.getStepUI(AdminStep.CONFIG);
        configUI.getLocalSourceConfig().setBorder(new TitledBorder(getModel().getLocalSourceLabel()));
        configUI.getCentralSourceConfig().setBorder(new TitledBorder(getModel().getCentralSourceLabel()));

    }


}
