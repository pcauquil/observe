/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.validate;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDto;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtoType;
import fr.ird.observe.services.service.actions.validate.ValidatorDto;
import fr.ird.observe.services.service.actions.validate.ValidatorDtos;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.validation.ValidationModelMode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import java.io.File;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Modele pour preparer une validation de donnees d'une base.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ValidateModel extends AdminActionModel {

    public static final String PROPERTY_ALL_CONTEXT_NAMES = "allContextNames";

    public static final String PROPERTY_CONTEXT_NAME = "contextName";

    public static final String PROPERTY_SCOPES = "scopes";

    public static final String PROPERTY_MODEL_MODE = "modelMode";

    public static final String PROPERTY_GENERATE_REPORT = "generateReport";

    public static final String PROPERTY_REPORT_FILE = "reportFile";

    public static final String PROPERTY_MESSAGES = "messages";

    /** le pattern du fichier de rapport après validation */
    private static final String REPORT_PATTERN = "report-%1$tF--%1$tk-%1$tM-%1$tS.txt";

    /** Logger */
    private static final Log log = LogFactory.getLog(ValidateModel.class);

    /** les scopes a utiliser */
    protected final EnumSet<NuitonValidatorScope> scopes;

    /** le lastName du context de validation */
    protected String contextName;

    /** le type de modele a utiliser */
    protected ValidationModelMode modelMode;

    /** pour generer un rapport et le sauvegarder */
    protected boolean generateReport;

    /** le fichier où sauvegarder les résultats de la validation */
    protected File reportFile = new File("");

    /** tout les validateur de la base */
    protected ImmutableSet<ValidatorDto> allValidators;

    /** les validateurs selectionnées */
    protected Set<ValidatorDto> validators;

    /** le dictionnaire des paths d'entites detectees */
    protected Map<Class<? extends IdDto>, ValidateResultForDtoType> messages;

    public ValidateModel() {
        super(AdminStep.VALIDATE);
        scopes = EnumSet.noneOf(NuitonValidatorScope.class);
        messages = new TreeMap<>();
    }

    /**
     * @return le nom par defaut du rapport de validation à enregistrer.
     */
    public String getDefaultReportFilename() {
        return String.format(REPORT_PATTERN, new Date());
    }

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        Object oldvalue = this.contextName;
        this.contextName = contextName;
        validators = null;
        firePropertyChange(PROPERTY_CONTEXT_NAME, oldvalue, contextName);
    }

    public ValidationModelMode getModelMode() {
        return modelMode;
    }

    public void setModelMode(ValidationModelMode modelMode) {
        Object oldvalue = this.modelMode;
        this.modelMode = modelMode;
        validators = null;
        firePropertyChange(PROPERTY_MODEL_MODE, oldvalue, modelMode);
    }

    public EnumSet<NuitonValidatorScope> getScopes() {
        return scopes;
    }

    public boolean isGenerateReport() {
        return generateReport;
    }

    public void setGenerateReport(boolean generateReport) {
        Object oldValue = this.generateReport;
        this.generateReport = generateReport;
        firePropertyChange(PROPERTY_GENERATE_REPORT, oldValue, generateReport);
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(File reportFile) {
        Object oldValue = this.reportFile;
        this.reportFile = reportFile;
        firePropertyChange(PROPERTY_REPORT_FILE, oldValue, reportFile);
    }

    public void setAllValidators(ImmutableSet<ValidatorDto> allValidators) {
        this.allValidators = allValidators;
    }

    public Set<ValidatorDto> getValidators() {
        if (validators == null) {
            if (allValidators != null && getContextName() != null) {
                validators = ValidatorDtos.filter(
                        allValidators,
                        getModelMode().isData(),
                        getModelMode().isReferential(),
                        getScopes(),
                        getContextName());
            } else {
                validators = Sets.newHashSet();
            }

        }
        return validators;
    }

    public Map<Class<? extends IdDto>, ValidateResultForDtoType> getMessages() {
        return messages;
    }

    public void setMessages(Map<Class<? extends IdDto>, ValidateResultForDtoType> messages) {
        Object oldValue = this.messages;
        this.messages = messages;
        firePropertyChange(PROPERTY_MESSAGES, oldValue, messages);
    }

    public Set<Class<? extends IdDto>> getMessageTypes() {
        Set<Class<? extends IdDto>> result;

        if (messages != null && !messages.isEmpty()) {
            result = messages.keySet();
        } else {
            result = Sets.newHashSet();
        }

        return result;
    }

    public List<AbstractReference> getMessagesDto(Class dtoType) {

        List<AbstractReference> result = Lists.newArrayList();

        if (messages != null) {

            ValidateResultForDtoType resultForDtoType = messages.get(dtoType);

            ImmutableSet<ValidateResultForDto> validateResultForDto = resultForDtoType.getValidateResultForDto();

            for (ValidateResultForDto resultForDto : validateResultForDto) {

                result.add(resultForDto.getDto());
            }
        }
        return result;
    }

    public ValidateResultForDto getMessages(AbstractReference referenceDto) {

        ValidateResultForDto result = null;

        if (messages != null) {

            ValidateResultForDtoType resultForDtoType = messages.get(referenceDto.getType());

            result = resultForDtoType.getValidateResult(referenceDto);

        }

        return result;
    }

    public void addScope(NuitonValidatorScope scope) {
        scopes.add(scope);
        validators = null;
        firePropertyChange(PROPERTY_SCOPES, null, scopes);
    }

    public void removeScope(NuitonValidatorScope scope) {
        scopes.remove(scope);
        validators = null;
        firePropertyChange(PROPERTY_SCOPES, null, scopes);
    }

    @Override
    public void destroy() {
        super.destroy();
        if (messages != null) {
            setMessages(null);
        }
        if (validators != null) {
            validators = null;
        }
    }

    public ImmutableSet<ValidatorDto> getAllValidators() {
        return allValidators;
    }
}
