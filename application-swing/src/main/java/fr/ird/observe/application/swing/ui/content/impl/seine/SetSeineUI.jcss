/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
  enabled:{!model.isReadingMode()};
}

#generalTab {
  title:{t("observe.content.setSeine.tab.general")};
  icon:{getHandler().getErrorIconIfFalse(model.isGeneralTabValid())};
}

#measurementsTab {
  title:{t("observe.content.setSeine.tab.measurements")};
  icon:{getHandler().getErrorIconIfFalse(model.isMeasurementsTabValid())};
}

#startTime {
  propertyDate:{SetSeineDto.PROPERTY_START_TIME};
  label:{t("observe.content.setSeine.startTime")};
  date:{bean.getStartTime()};
  dateEditable:false;
  _validatorLabel:{t("observe.content.setSeine.startTime")};
}

#endPursingTimeStamp {
  propertyDate: {SetSeineDto.PROPERTY_END_PURSING_TIME_STAMP};
  propertyDayDate: {SetSeineDto.PROPERTY_END_PURSING_DATE};
  propertyTimeDate: {SetSeineDto.PROPERTY_END_PURSING_TIME};
  label:{t("observe.content.setSeine.endPursingTimeStamp")};
  date:{bean.getEndPursingTimeStamp()};
  _validatorLabel:{t("observe.content.setSeine.endPursingTimeStamp")};
}

#endSetTimeStamp {
  propertyDate: {SetSeineDto.PROPERTY_END_SET_TIME_STAMP};
  propertyDayDate: {SetSeineDto.PROPERTY_END_SET_DATE};
  propertyTimeDate: {SetSeineDto.PROPERTY_END_SET_TIME};
  label:{t("observe.content.setSeine.endSetTimeStamp")};
  date:{bean.getEndSetTimeStamp()};
  _validatorLabel:{t("observe.content.setSeine.endSetTimeStamp")};
}

#reasonForNullSetLabel {
  text:"observe.content.setSeine.reasonForNullSet";
  labelFor:{reasonForNullSet};
}

#reasonForNullSet {
  property:{SetSeineDto.PROPERTY_REASON_FOR_NULL_SET};
  selectedItem:{bean.getReasonForNullSet()};
  _validatorLabel:{t("observe.content.setSeine.reasonForNullSet")};
}

#supportVesselNameLabel {
  text:"observe.content.setSeine.supportVesselName";
  labelFor:{supportVesselName};
}

#supportVesselName {
  _propertyName:{SetSeineDto.PROPERTY_SUPPORT_VESSEL_NAME};
  text:{getStringValue(bean.getSupportVesselName())};
  _validatorLabel:{t("observe.content.setSeine.supportVesselName")};
}

#resetSupportVesselName{
  toolTipText:"observe.content.action.reset.supportVesselName.tip";
  _resetPropertyName:{SetSeineDto.PROPERTY_SUPPORT_VESSEL_NAME};
}

#targetDiscardedLabel {
  text:"observe.content.setSeine.targetDiscarded";
  labelFor:{targetDiscarded};
}

#targetDiscarded {
  _propertyName:{SetSeineDto.PROPERTY_TARGET_DISCARDED};
  booleanValue:{bean.getTargetDiscarded()};
  _validatorLabel:{t("observe.content.setSeine.targetDiscarded")};
}

#nonTargetDiscardedLabel {
  text:"observe.content.setSeine.nonTargetDiscarded";
  labelFor:{nonTargetDiscarded};
}

#nonTargetDiscarded {
  _propertyName:{SetSeineDto.PROPERTY_NON_TARGET_DISCARDED};
  booleanValue:{bean.getNonTargetDiscarded()};
  _validatorLabel:{t("observe.content.setSeine.nonTargetDiscarded")};
}

#schoolTypeLabel {
  actionIcon:information;
  text:"observe.content.setSeine.schoolType";
  toolTipText:"observe.content.setSeine.message.information.schoolType";
  labelFor:{schoolType};
}

#schoolType {
  font-weight:bold;
  text:{getHandler().updateTypeValue(bean.getSchoolType())};
  _validatorLabel:{t("observe.content.setSeine.schoolType")};
}

#sonarUsedAvantSetPane {
  border:{new TitledBorder(null, t("observe.content.setSeine.sonarUsedAvantSet", 1, 0, getFont()))};
}

#schoolThicknessLabel {
  text:"observe.content.setSeine.schoolThickness";
  labelFor:{schoolThickness};
}

#schoolThickness {
  property:{SetSeineDto.PROPERTY_SCHOOL_THICKNESS};
  model:{bean.getSchoolThickness()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
  _validatorLabel:{t("observe.content.setSeine.schoolThickness")};
}

#schoolMeanDepthLabel {
  text:"observe.content.setSeine.schoolMeanDepth";
  labelFor:{schoolMeanDepth};
}

#schoolMeanDepth {
  property:{SetSeineDto.PROPERTY_SCHOOL_MEAN_DEPTH};
  model:{bean.getSchoolMeanDepth()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
  _validatorLabel:{t("observe.content.setSeine.schoolMeanDepth")};
}

#schoolTopDepthLabel {
  text:"observe.content.setSeine.schoolTopDepth";
  labelFor:{schoolTopDepth};
}

#schoolTopDepth {
  property:{SetSeineDto.PROPERTY_SCHOOL_TOP_DEPTH};
  model:{bean.getSchoolTopDepth()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
  _validatorLabel:{t("observe.content.setSeine.schoolTopDepth")};
}

#currentMeasureDepthLabel {
  text:"observe.content.setSeine.currentMeasureDepth";
  labelFor:{currentMeasureDepth};
}

#currentMeasureDepth {
  property:{SetSeineDto.PROPERTY_CURRENT_MEASURE_DEPTH};
  model:{bean.getCurrentMeasureDepth()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
  _validatorLabel:{t("observe.content.setSeine.currentMeasureDepth")};
}

#currentSpeedLabel {
  text:"observe.content.setSeine.currentSpeed";
  labelFor:{currentSpeed};
}

#currentSpeed {
  property:{SetSeineDto.PROPERTY_CURRENT_SPEED};
  model:{bean.getCurrentSpeed()};
  useFloat:true;
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.DECIMAL1_PATTERN};
  _validatorLabel:{t("observe.content.setSeine.currentSpeed")};
}

#currentDirectionLabel {
  text:"observe.content.setSeine.currentDirection";
  labelFor:{currentDirection};
}

#currentDirection {
  property:{SetSeineDto.PROPERTY_CURRENT_DIRECTION};
  model:{bean.getCurrentDirection()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_3_DIGITS_PATTERN};
  _validatorLabel:{t("observe.content.setSeine.currentDirection")};
}

#maxGearDepthLabel {
  text:"observe.content.setSeine.maxGearDepth";
  labelFor:{maxGearDepth};
}

#maxGearDepth {
  property:{SetSeineDto.PROPERTY_MAX_GEAR_DEPTH};
  model:{bean.getMaxGearDepth()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
  _validatorLabel:{t("observe.content.setSeine.maxGearDepth")};
}

#sonarUsed {
  _propertyName:{SetSeineDto.PROPERTY_SONAR_USED};
  text:"observe.content.setSeine.sonarUsed";
  selected:{bean.isSonarUsed()};
  _validatorLabel:{t("observe.content.setSeine.sonarUsed")};
}

#delete {
  _toolTipText:{t("observe.action.delete.set.tip")};
}
