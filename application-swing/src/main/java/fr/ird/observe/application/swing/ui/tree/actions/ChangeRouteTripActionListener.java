package fr.ird.observe.application.swing.ui.tree.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.service.seine.RouteService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 1/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class ChangeRouteTripActionListener extends NodeChangeActionListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeRouteTripActionListener.class);

    public ChangeRouteTripActionListener(ObserveTreeHelper treeHelper,
                                         ObserveSwingDataSource dataSource,
                                         String routeId,
                                         String tripId) {
        super(treeHelper, routeId, tripId);
    }

    @Override
    protected void closeNode(String routeId) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();

        if (openDataManager.isOpenRoute(routeId)) {
            openDataManager.closeRoute(routeId);
        }
    }

    @Override
    protected ObserveNode getParentNode(ObserveNode node) {
        return node.getParent().getParent();
    }

    @Override
    protected ObserveNode getNewParentNode(ObserveNode grandParentNode, String parentNodeId) {
        ObserveNode tripNode = getTreeHelper().getChild(grandParentNode, parentNodeId);
        String routesNodeId = ObserveI18nDecoratorHelper.getTypePluralI18nKey(RouteDto.class);
        return getTreeHelper().getChild(tripNode, routesNodeId);
    }

    @Override
    protected int moveNodeToParent(String nodeId, String parentNodeId, String oldParentNodeId) {
        int position;

        RouteService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newRouteService();
        position = service.moveRouteToTripSeine(nodeId, parentNodeId);

        return position;
    }
}
