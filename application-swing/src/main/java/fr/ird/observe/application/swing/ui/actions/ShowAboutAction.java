package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.AboutPanel;
import org.nuiton.util.Resource;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.net.MalformedURLException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ShowAboutAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private final ObserveMainUI ui;

    public ShowAboutAction(ObserveMainUI ui) {

        super(t("observe.action.about"), SwingUtil.getUIManagerActionIcon("about"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.about.tip"));
        putValue(MNEMONIC_KEY, (int) 'A');

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        AboutPanel about = new AboutPanel() {

            private static final long serialVersionUID = 1L;

            @Override
            public void buildTopPanel() {
                topPanel.setLayout(new BorderLayout());
                JLabel labelIcon;
                Icon logoIcon;
                logoIcon = Resource.getIcon("/icons/logo-OT_web.png");
                labelIcon = new JLabel(logoIcon);
                topPanel.add(labelIcon, BorderLayout.WEST);

                logoIcon = Resource.getIcon("/icons/logo_ird.png");
                labelIcon = new JLabel(logoIcon);
                topPanel.add(labelIcon, BorderLayout.EAST);
            }
        };

        String name = ObserveRunner.getRunner().getRunnerName();
        about.setTitle(t("observe.title.about"));
        about.setAboutText(t("observe.about.message"));
        about.setBottomText(ui.getConfig().getCopyrightText());
        about.setIconPath("/icons/logo-OT_web.png");
        about.setLicenseFile("META-INF/" + name + "-LICENSE.txt");
        about.setThirdpartyFile("META-INF/" + name + "-THIRD-PARTY.txt");
        about.buildTopPanel();

        //
        // translate tab
        //

        JScrollPane translatePane = new JScrollPane();
        JEditorPane translateArea = new JEditorPane();
        translateArea.setContentType("text/html");
        translateArea.setEditable(false);
        if (translateArea.getFont() != null) {
            translateArea.setFont(translateArea.getFont().deriveFont((float) 11));
        }

        translateArea.setBorder(null);
        File csvFile = new File(ui.getConfig().getI18nDirectory(), "observe-i18n.csv");
        String translateText;
        try {
            translateText = t("observe.about.translate.content", csvFile.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RuntimeException("Could not obtain url from " + csvFile, e);
        }
        translateArea.setText(translateText);
        translatePane.getViewport().add(translateArea);
        translateArea.addHyperlinkListener(UIHelper::openLink);

        about.getTabs().add(t("observe.about.translate.title"), translatePane);

        about.init();
        about.showInDialog(ui, true);

    }
}
