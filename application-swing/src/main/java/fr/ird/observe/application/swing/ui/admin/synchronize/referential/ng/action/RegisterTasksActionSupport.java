package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractObserveAction;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialReplaceUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialReplaceUIHandler;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.ReferentialSynchronizeTaskSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferenceReferentialSynchroNodeSupport;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import jaxx.runtime.context.JAXXInitialContext;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

import javax.swing.Action;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public abstract class RegisterTasksActionSupport extends AbstractObserveAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RegisterTasksActionSupport.class);

    private static final long serialVersionUID = 1L;
    protected final Predicate<ReferenceReferentialSynchroNodeSupport> predicate;
    private final ReferentialSynchroUI ui;
    private final boolean left;
    private final boolean needReplace;
    protected String typeStr;
    protected String referenceStr;

    public RegisterTasksActionSupport(ReferentialSynchroUI ui,
                                      ReferentialSynchronizeResources resource,
                                      boolean left,
                                      boolean needReplace) {
        super(null, null);
        String tip = resource.getActionTip(left);
        if (tip != null) {
            putValue(SHORT_DESCRIPTION, t(tip));
        }
        String actionName = resource.getActionName(left);
        if (actionName != null) {
            putValue(Action.SMALL_ICON, UIHelper.getUIManagerActionIcon(actionName));
        }
        this.ui = ui;
        this.left = left;
        this.needReplace = needReplace;
        this.predicate = resource.getPredicate();
        String propertyName = resource.getPropertyName(left);
        if (propertyName != null) {
            ui.getModel().getReferentialSynchroModel().addPropertyChangeListener(propertyName, evt -> setEnabled((Boolean) evt.getNewValue()));
        }
    }

    protected abstract <R extends ReferentialDto> ReferentialSynchronizeTaskSupport<R> createTask(boolean left, ReferentialReference<R> reference, ReferentialReference<R> replaceReference);

    public boolean isLeft() {
        return left;
    }

    protected String getReplaceTitle(ReferentialReference reference) {
        throw new NotImplementedException("");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ReferentialSynchronizeTreeModel treeModel = getTreeModel();

        createTasks();

        treeModel.clearSelection();

    }

    protected void createTasks() {
        createTasks(getTreeModel(), predicate);
    }

    protected <R extends ReferentialDto> void createTasks(ReferentialSynchronizeTreeModel treeModel, Predicate<ReferenceReferentialSynchroNodeSupport> predicate) {

        Collection<ReferenceReferentialSynchroNodeSupport> removedNodes = new LinkedList<>();
        Collection<ReferentialSynchronizeTaskSupport> addedTasks = new LinkedList<>();

        for (ReferenceReferentialSynchroNodeSupport node : getReferenceReferentialSynchroNodes(predicate)) {

            ReferentialReference<R> reference = node.getUserObject();
            ReferentialReference<R> replaceReference = null;

            if (needReplace) {

                Class<R> type = reference.getType();

                List<ReferentialReference<R>> references = ui.getStepModel().getPossibleReplaceUniverse(left, type, reference);

                ReferentialReferenceDecorator<R> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(type);

                ReferentialReplaceUI<R> replaceUI = new ReferentialReplaceUI<>(new JAXXInitialContext()
                                                                                       .add(ReferentialReplaceUIHandler.CONTEXT_NAME, reference)
                                                                                       .add(ReferentialReplaceUIHandler.CONTEXT_NAME, references)
                                                                                       .add(ReferentialReplaceUIHandler.CONTEXT_NAME, decorator)
                                                                                       .add(this));

                typeStr = t(ObserveI18nDecoratorHelper.getTypeI18nKey(type));
                referenceStr = ui.getHandler().getDecoratorService().getReferentialReferenceDecorator(type).toString(reference);
                replaceUI.getMessage().setText(t("observe.actions.synchro.referential.replaceBefore.message", typeStr, referenceStr));

                int response = UIHelper.askUser(
                        null,
                        t(getReplaceTitle(reference), typeStr, referenceStr),
                        replaceUI,
                        JOptionPane.WARNING_MESSAGE,
                        new Object[]{I18n.t("observe.choice.replace"), I18n.t("observe.choice.cancel")},
                        1);

                switch (response) {
                    case JOptionPane.CLOSED_OPTION:
                    case 1:

                        break;
                    case 0:
                        replaceReference = replaceUI.getReplaceReference();
                        break;
                }

                if (log.isInfoEnabled()) {
                    log.info("replaceReference: " + replaceReference);
                }

                if (replaceReference == null) {
                    if (log.isWarnEnabled()) {
                        log.warn("Skip this task, no replace referential reference selected");
                    }

                    continue;
                }

                if (log.isInfoEnabled()) {
                    log.info("Selected replace referential reference: " + replaceReference);
                }

            }

            ReferentialSynchronizeTaskSupport task = createTask(left, reference, replaceReference);

            if (log.isInfoEnabled()) {
                log.info("Add " + task.getStripLabel());
            }

            ui.getHandler().sendMessage(task.getStripLabel());

            removedNodes.add(node);
            addedTasks.add(task);

        }

        treeModel.removeReferenceNodes(removedNodes);
        ui.getStepModel().getTasks().addTasks(addedTasks);
    }

    protected Collection<ReferenceReferentialSynchroNodeSupport> getReferenceReferentialSynchroNodes(Predicate<ReferenceReferentialSynchroNodeSupport> predicate) {
        return getTreeModel().filterSelectedReferenceNodes(predicate);
    }

    protected ReferentialSynchronizeTreeModel getTreeModel() {
        return left ? ui.getStepModel().getLeftTreeModel() : ui.getStepModel().getRightTreeModel();
    }

}
