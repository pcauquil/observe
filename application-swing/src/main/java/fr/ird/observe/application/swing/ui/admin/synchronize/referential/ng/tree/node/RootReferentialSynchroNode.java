package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.Enumeration;

/**
 * Created on 10/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class RootReferentialSynchroNode extends ReferentialSynchroNodeSupport {

    private static final long serialVersionUID = 1L;

    private final boolean left;
    private final boolean canWrite;

    public RootReferentialSynchroNode(boolean left, boolean canWrite) {
        super(null, null);
        this.left = left;
        this.canWrite = canWrite;
    }

    @Override
    public boolean isLeft() {
        return left;
    }

    public boolean isCanWrite() {
        return canWrite;
    }

    public <R extends ReferentialDto> TypeReferentialSynchroNode getOrAddTypeNode(Class<R> referentialName) {
        TypeReferentialSynchroNode node = getChild(referentialName);
        if (node == null) {
            node = new TypeReferentialSynchroNode(referentialName);
            add(node);
        }
        return node;
    }

    public <R extends ReferentialDto> TypeReferentialSynchroNode getChild(Class<R> referentialName) {
        Enumeration children = children();
        while (children.hasMoreElements()) {
            TypeReferentialSynchroNode o = (TypeReferentialSynchroNode) children.nextElement();
            if (referentialName.equals(o.getUserObject())) {
                return o;
            }
        }
        return null;
    }

}
