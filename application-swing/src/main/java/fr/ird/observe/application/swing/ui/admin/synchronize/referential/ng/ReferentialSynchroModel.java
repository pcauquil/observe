package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ReferentialSynchronizeMode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.AddReferentialSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.DeleteReferentialSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.DesactivateReferentialSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.ReferentialSynchronizeTaskSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.ReferentialSynchronizeTreeModel;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffsEngine;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeService;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeServiceEngine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchroModel extends AdminActionModel {

    public static final String LEFT_SOURCE_PROPERTY_NAME = "leftSource";
    public static final String RIGHT_SOURCE_PROPERTY_NAME = "rightSource";
    public static final String TASKS_PROPERTY_NAME = "tasks";
    public static final String SYNCHRONIZE_MODE_PROPERTY_NAME = "synchronizeMode";
    public static final String RIGHT_TREE_MODEL_PROPERTY_NAME = "rightTreeModel";
    public static final String LEFT_TREE_MODEL_PROPERTY_NAME = "lefttTreeModel";
    public static final String COPY_RIGHT_PROPERTY_NAME = "copyRight";
    public static final String COPY_LEFT_PROPERTY_NAME = "copyLeft";
    public static final String SKIP_RIGHT_PROPERTY_NAME = "skipRight";
    public static final String SKIP_LEFT_PROPERTY_NAME = "skipLeft";
    public static final String DELETE_RIGHT_PROPERTY_NAME = "deleteRight";
    public static final String DELETE_LEFT_PROPERTY_NAME = "deleteLeft";
    public static final String DESACTIVATE_RIGHT_PROPERTY_NAME = "desactivateRight";
    public static final String DESACTIVATE_LEFT_PROPERTY_NAME = "desactivateLeft";
    public static final String DESACTIVATE_WITH_REPLACE_RIGHT_PROPERTY_NAME = "desactivateWithReplaceRight";
    public static final String DESACTIVATE_WITH_REPLACE_LEFT_PROPERTY_NAME = "desactivateWithReplaceLeft";
    public static final String REVERT_RIGHT_PROPERTY_NAME = "revertRight";
    public static final String REVERT_LEFT_PROPERTY_NAME = "revertLeft";
    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialSynchroModel.class);
    private final ReferentialSynchronizeTaskListModel tasks;
    private ObserveSwingDataSource leftSource;
    private ObserveSwingDataSource rightSource;
    private ReferentialSynchronizeMode synchronizeMode;
    private ReferentialSynchronizeTreeModel leftTreeModel;
    private ReferentialSynchronizeTreeModel rightTreeModel;
    private ReferentialSynchronizeDiffsEngine engine;
    private boolean copyLeft;
    private boolean revertLeft;
    private boolean skipLeft;
    private boolean deleteLeft;
    private boolean desactivateLeft;
    private boolean desactivateWithReplaceLeft;
    private boolean copyRight;
    private boolean revertRight;
    private boolean skipRight;
    private boolean deleteRight;
    private boolean desactivateRight;
    private boolean desactivateWithReplaceRight;

    public ReferentialSynchroModel() {
        super(AdminStep.REFERENTIAL_SYNCHRONIZE);
        this.tasks = new ReferentialSynchronizeTaskListModel();
    }

    public ReferentialSynchronizeMode getSynchronizeMode() {
        return synchronizeMode;
    }

    public void setSynchronizeMode(ReferentialSynchronizeMode synchronizeMode) {
        Object oldValue = getSynchronizeMode();
        this.synchronizeMode = synchronizeMode;
        firePropertyChange(SYNCHRONIZE_MODE_PROPERTY_NAME, oldValue, synchronizeMode);
    }

    public ObserveSwingDataSource getLeftSource() {
        return leftSource;
    }

    public void setLeftSource(ObserveSwingDataSource leftSource) {
        this.leftSource = leftSource;
        firePropertyChange(LEFT_SOURCE_PROPERTY_NAME, leftSource);
    }

    public ObserveSwingDataSource getRightSource() {
        return rightSource;
    }

    public void setRightSource(ObserveSwingDataSource rightSource) {
        this.rightSource = rightSource;
        firePropertyChange(RIGHT_SOURCE_PROPERTY_NAME, rightSource);
    }

    public ReferentialSynchronizeTreeModel getLeftTreeModel() {
        return leftTreeModel;
    }

    public void setLeftTreeModel(ReferentialSynchronizeTreeModel leftTreeModel) {
        Object oldValue = getLeftTreeModel();
        this.leftTreeModel = leftTreeModel;
        firePropertyChange(LEFT_TREE_MODEL_PROPERTY_NAME, oldValue, leftTreeModel);
    }

    public ReferentialSynchronizeTreeModel getRightTreeModel() {
        return rightTreeModel;
    }

    public void setRightTreeModel(ReferentialSynchronizeTreeModel rightTreeModel) {
        Object oldValue = getRightTreeModel();
        this.rightTreeModel = rightTreeModel;
        firePropertyChange(RIGHT_TREE_MODEL_PROPERTY_NAME, oldValue, rightTreeModel);
    }

    public ReferentialSynchronizeTaskListModel getTasks() {
        return tasks;
    }

    public void setEngine(ReferentialSynchronizeDiffsEngine engine) {
        this.engine = engine;
    }

    public <D extends ReferentialDto> List<ReferentialReference<D>> getPossibleReplaceUniverse(boolean left, Class<D> type, ReferentialReference<D> referenceToReplace) {

        ReferentialReferenceSet<D> referencesSet;
        if (left) {
            referencesSet = engine.getLeftEnabledReferentialReferenceSet(type);
        } else {
            referencesSet = engine.getRightEnabledReferentialReferenceSet(type);
        }

        List<ReferentialReference<D>> references = new ArrayList<>(referencesSet.getReferences());
        references.remove(referenceToReplace);

        for (ReferentialSynchronizeTaskSupport task : tasks) {

            if (!type.equals(task.getType())) {
                continue;
            }
            if (left == task.isLeft()) {

                if (task instanceof DeleteReferentialSynchronizeTask) {

                    // on enlève ce référentiel car il a été supprimé de ce côté
                    ReferentialReference<D> referential = task.getReferential();
                    references.remove(referential);
                }

                if (task instanceof DesactivateReferentialSynchronizeTask) {

                    // on enlève ce référentiel car il a été désactivé de ce côté
                    ReferentialReference<D> referential = task.getReferential();
                    references.remove(referential);
                }

                continue;
            }

            if (type.equals(task.getType())) {

                if (task instanceof AddReferentialSynchronizeTask) {

                    // on ajoute ce référentiel car il a été ajouté depuis l'autre côté
                    ReferentialReference<D> referential = task.getReferential();
                    references.add(referential);
                }

            }

        }

        return references;
    }

    public boolean isCopyRight() {
        return copyRight;
    }

    public void setCopyRight(boolean copyRight) {
        this.copyRight = copyRight;
        firePropertyChange(COPY_RIGHT_PROPERTY_NAME, copyRight);
    }

    public boolean isCopyLeft() {
        return copyLeft;
    }

    public void setCopyLeft(boolean copyLeft) {
        this.copyLeft = copyLeft;
        firePropertyChange(COPY_LEFT_PROPERTY_NAME, copyLeft);
    }

    public boolean isRevertRight() {
        return revertRight;
    }

    public void setRevertRight(boolean revertRight) {
        this.revertRight = revertRight;
        firePropertyChange(REVERT_RIGHT_PROPERTY_NAME, revertRight);
    }

    public boolean isRevertLeft() {
        return revertLeft;
    }

    public void setRevertLeft(boolean revertLeft) {
        this.revertLeft = revertLeft;
        firePropertyChange(REVERT_LEFT_PROPERTY_NAME, revertLeft);
    }

    public boolean isSkipLeft() {
        return skipLeft;
    }

    public void setSkipLeft(boolean skipLeft) {
        this.skipLeft = skipLeft;
        firePropertyChange(SKIP_LEFT_PROPERTY_NAME, skipLeft);
    }

    public boolean isSkipRight() {
        return skipRight;
    }

    public void setSkipRight(boolean skipRight) {
        this.skipRight = skipRight;
        firePropertyChange(SKIP_RIGHT_PROPERTY_NAME, skipRight);
    }

    public boolean isDeleteLeft() {
        return deleteLeft;
    }

    public void setDeleteLeft(boolean deleteLeft) {
        this.deleteLeft = deleteLeft;
        firePropertyChange(DELETE_LEFT_PROPERTY_NAME, deleteLeft);
    }

    public boolean isDeleteRight() {
        return deleteRight;
    }

    public void setDeleteRight(boolean deleteRight) {
        this.deleteRight = deleteRight;
        firePropertyChange(DELETE_RIGHT_PROPERTY_NAME, deleteRight);
    }

    public boolean isDesactivateRight() {
        return desactivateRight;
    }

    public void setDesactivateRight(boolean desactivateRight) {
        this.desactivateRight = desactivateRight;
        firePropertyChange(DESACTIVATE_RIGHT_PROPERTY_NAME, desactivateRight);
    }

    public boolean isDesactivateLeft() {
        return desactivateLeft;
    }

    public void setDesactivateLeft(boolean desactivateLeft) {
        this.desactivateLeft = desactivateLeft;
        firePropertyChange(DESACTIVATE_LEFT_PROPERTY_NAME, desactivateLeft);
    }

    public boolean isDesactivateWithReplaceRight() {
        return desactivateWithReplaceRight;
    }

    public void setDesactivateWithReplaceRight(boolean desactivateWithReplaceRight) {
        this.desactivateWithReplaceRight = desactivateWithReplaceRight;
        firePropertyChange(DESACTIVATE_WITH_REPLACE_RIGHT_PROPERTY_NAME, desactivateWithReplaceRight);
    }

    public boolean isDesactivateWithReplaceLeft() {
        return desactivateWithReplaceLeft;
    }

    public void setDesactivateWithReplaceLeft(boolean desactivateWithReplaceLeft) {
        this.desactivateWithReplaceLeft = desactivateWithReplaceLeft;
        firePropertyChange(DESACTIVATE_WITH_REPLACE_LEFT_PROPERTY_NAME, desactivateWithReplaceLeft);
    }


    public ReferentialSynchronizeServiceEngine newReferentialSynchronizeServiceEngine() {
        ReferentialSynchronizeService leftService = leftSource.newReferentialSynchronizeService();
        ReferentialSynchronizeService rightService = rightSource.newReferentialSynchronizeService();
        return new ReferentialSynchronizeServiceEngine(leftService, rightService);
    }
}
