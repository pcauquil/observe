/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.ActivityLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.ActivitySeinesUI;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.ActivityLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.ActivitySeineUI;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour fermer l'objet sous-jacent à l'écran et en créer un nouveau.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class CloseAndCreateUIAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "closeAndCreate";

    public CloseAndCreateUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.content.action.closeAndCreate"),
              n("observe.content.action.closeAndCreate.tip"),
              "add"
        );
    }

    @Override
    public void actionPerformed(final ActionEvent e) {

        SwingUtilities.invokeLater(() -> {
            JComponent c = (JComponent) e.getSource();
            ContentUI<?> ui = (ContentUI<?>)
                    c.getClientProperty("ui");
            if (ui == null) {
                throw new IllegalStateException(
                        "could not find client property " +
                        "ui on component" + c);
            }
            if (ui instanceof ContentOpenableUI) {
                ((ContentOpenableUI<?>) ui).closeAndCreateData();
                return;
            }

            if (ui instanceof ActivitySeinesUI) {

                // cas particulier pour l'écran des activitys

                JTree tree = getMainUI().getNavigation();
                ObserveTreeHelper treeHelper = getMainUI().getTreeHelper();

                boolean wasCollapsed = isOpenActivityNodeCollapsed(
                        tree,
                        treeHelper,
                        getMainUI().getDataContext()
                );

                // selection du noeud de l'activity ouverte
                treeHelper.selectOpenNode(ActivitySeineDto.class);

                // on conserve le path de l'activity
                TreePath path = tree.getSelectionPath();

                // recuperation de l'écran associé
                ActivitySeineUI selectedUI = (ActivitySeineUI)
                        ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

                // fermeture de l'activity et création d'une nouvelle
                selectedUI.closeAndCreateData();

                if (wasCollapsed) {

                    // on ferme le noeud de l'activity (qui a ete ouvert
                    // lors de la selection de celle-ci)
                    tree.collapsePath(path);
                }

                return;
            }

            if (ui instanceof ActivityLonglinesUI) {

                // cas particulier pour l'écran des activitys

                JTree tree = getMainUI().getNavigation();
                ObserveTreeHelper treeHelper = getMainUI().getTreeHelper();

                boolean wasCollapsed = isOpenActivityNodeCollapsed(
                        tree,
                        treeHelper,
                        getMainUI().getDataContext()
                );

                // selection du noeud de l'activity ouverte
                treeHelper.selectOpenNode(ActivityLonglineDto.class);

                // on conserve le path de l'activity
                TreePath path = tree.getSelectionPath();

                // recuperation de l'écran associé
                ActivityLonglineUI selectedUI = (ActivityLonglineUI)
                        ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

                // fermeture de l'activity et création d'une nouvelle
                selectedUI.closeAndCreateData();

                if (wasCollapsed) {

                    // on ferme le noeud de l'activity (qui a ete ouvert
                    // lors de la selection de celle-ci)
                    tree.collapsePath(path);
                }

                return;
            }

            throw new IllegalStateException("Can not come here!");
        });
    }
}
