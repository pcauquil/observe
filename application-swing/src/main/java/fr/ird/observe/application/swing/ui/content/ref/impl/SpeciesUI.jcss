/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#generalTab {
  title:{t("observe.content.species.tab.general")};
  icon:{getHandler().getErrorIconIfFalse(model.isGeneralTabValid())};
}

#otherTab {
  title:{t("observe.content.species.tab.other")};
  icon:{getHandler().getErrorIconIfFalse(model.isOtherTabValid())};
}

#editI18nTable2 {
  border:{new TitledBorder(t("observe.common.libelles"))};
}

#homeIdFAOWormsIdLabel {
  font-style:"italic";
  text:"observe.common.homeIdFAOWormsId";
}

#homeId {
  text:{getStringValue(bean.getHomeId())};
  _validatorLabel: {t("observe.common.homeId")};
}

#faoCode {
  text:{getStringValue(bean.getFaoCode())};
  _validatorLabel: {t("observe.common.faoCode")};
}

#wormsId {
  text:{getStringValue(bean.getWormsId())};
  _validatorLabel: {t("observe.common.wormsId")};
}

#scientificLabelLabel {
  text:"observe.common.scientificLabel";
  labelFor:{scientificLabel};
}

#scientificLabel {
  font-style:"italic";
  text:{getStringValue(bean.getScientificLabel())};
  _validatorLabel: {t("observe.common.scientificLabel")};
}

#oceanPane {
  border:{new TitledBorder(t("observe.common.oceanForSpecies"))};
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_NEVER};
  horizontalScrollBarPolicy:{JScrollPane.HORIZONTAL_SCROLLBAR_NEVER};
}

#ocean {
  property: {SpeciesDto.PROPERTY_OCEAN};
  selected:{(Collection)bean.getOcean()};
  _validatorLabel: {t("observe.common.ocean")};
  universeLabel:{t("observe.content.species.availableOcean")};
  selectedLabel:{t("observe.content.species.selectedOcean")};
}

#speciesGroupLabel {
  text:"observe.common.speciesGroup";
  labelFor:{speciesGroup};
}

#speciesGroup {
  property:{SpeciesDto.PROPERTY_SPECIES_GROUP};
  selectedItem:{bean.getSpeciesGroup()};
  _validatorLabel: {t("observe.common.speciesGroup")};
}

#editExtraTable {
  visible:true;
}

#editTaillePoids {
  border:{new TitledBorder(t("observe.common.taillePoids.caracteristics"))};
}

#lengthMeasureTypeLabel {
  text:"observe.common.lengthMeasureType";
  labelFor:{lengthMeasureType};
}

#lengthMeasureType {
  text:{getStringValue(bean.getLengthMeasureType())};
}

#minLengthMaxLabel {
  text:"observe.common.minLengthMax";
}

#minWeightMaxLabel {
  text:"observe.common.minWeightMax";
}

#minLength {
  property: {SpeciesDto.PROPERTY_MIN_LENGTH};
  model:{bean.getMinLength()};
  _validatorLabel: {t("observe.common.minLength")};
}

#maxLength {
  property:{SpeciesDto.PROPERTY_MAX_LENGTH};
  model:{bean.getMaxLength()};
  _validatorLabel: {t("observe.common.maxLength")};
}

#minWeight {
  property:{SpeciesDto.PROPERTY_MIN_WEIGHT};
  model:{bean.getMinWeight()};
  _validatorLabel: {t("observe.common.minWeight")};
}

#maxWeight {
  property:{SpeciesDto.PROPERTY_MAX_WEIGHT};
  model:{bean.getMaxWeight()};
  _validatorLabel: {t("observe.common.maxWeight")};
}


