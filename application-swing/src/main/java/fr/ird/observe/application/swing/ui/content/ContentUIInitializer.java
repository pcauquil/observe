package fr.ird.observe.application.swing.ui.content;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.actions.shared.AbstractUIAction;
import fr.ird.observe.application.swing.ui.util.BooleanEditor;
import fr.ird.observe.application.swing.ui.util.tripMap.ObserveMapPane;
import fr.ird.observe.application.swing.validation.ObserveSwingValidator;
import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import jaxx.runtime.JAXXValidator;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.OneClicListSelectionModel;
import jaxx.runtime.swing.editor.EnumEditor;
import jaxx.runtime.swing.editor.NumberEditor;
import jaxx.runtime.swing.editor.bean.BeanComboBox;
import jaxx.runtime.swing.editor.bean.BeanListHeader;
import jaxx.runtime.swing.editor.bean.BeanUIUtil;
import jaxx.runtime.swing.editor.cell.NumberCellEditor;
import jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import jaxx.runtime.swing.renderer.EnumEditorRenderer;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.bean.JavaBeanObjectUtil;
import org.nuiton.jaxx.widgets.datetime.DateTimeEditor;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.select.FilterableDoubleList;
import org.nuiton.util.DateUtil;

import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * To initialize ui.
 *
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class ContentUIInitializer<E extends IdDto, UI extends ObserveContentUI<E>> {

    public static final String OBSERVE_ACTION = "observeAction";

    public static final String CLIENT_PROPERTY_PROPERTY_NAME = "propertyName";

    public static final String CLIENT_PROPERTY_RESET_PROPERTY_NAME = "resetPropertyName";

    public static final String CLIENT_PROPERTY_NOT_BLOCKING = "notBlocking";

    /** Logger. */
    private static final Log log = LogFactory.getLog(ContentUIInitializer.class);

    protected final UI ui;

    protected final DecoratorService decoratorService;

    public ContentUIInitializer(UI ui) {
        this.ui = ui;
        this.decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
    }

    public static TableCellEditor newFloatColumnEditor(JTable table) {

        NumberCellEditor<Float> editor = JAXXWidgetUtil.newNumberTableCellEditor(Float.class, false);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern("\\d{0,6}(\\.\\d{0,4})?");
        return editor;

    }

    public static TableCellEditor newIntegerColumnEditor(JTable table) {

        NumberCellEditor<Integer> editor = JAXXWidgetUtil.newNumberTableCellEditor(Integer.class, false);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern("\\d{0,4}?");
        return editor;

    }

    public static <B> ComboBoxCellEditor newDataColumnEditor(List<B> data, Decorator<B> decorator) {

        JComboBox<B> comboBox = new JComboBox<>();

        return newDataColumnEditor(comboBox, data, decorator);

    }

    public static <B> ComboBoxCellEditor newDataColumnEditor(JComboBox<B> comboBox, List<B> data, Decorator<B> decorator) {

        ListCellRenderer<B> renderer = new DecoratorListCellRenderer(decorator);
        comboBox.setRenderer(renderer);

        prepareComboBoxData(comboBox, data);

        ObjectToStringConverter converter = BeanUIUtil.newDecoratedObjectToStringConverter(decorator);
        BeanUIUtil.decorate(comboBox, converter);
        return new ComboBoxCellEditor(comboBox);

    }

    public static <B> void prepareComboBoxData(JComboBox comboBox, List<B> data) {
        List<B> dataToList = Lists.newArrayList(data);

        // add a null value at first position
        if (!dataToList.isEmpty() && dataToList.get(0) != null) {
            dataToList.add(0, null);
        }
        SwingUtil.fillComboBox(comboBox, dataToList, null);
    }

    protected E getBean() {

        return ui.getModel().getBean();

    }

    protected Form<E> getFormDto() {
        return ui.getModel().getForm();
    }

    public void initUI() {

        if (log.isDebugEnabled()) {
            log.debug("ui " + getClass());
        }

        ActionMap actionMap = ObserveSwingApplicationContext.get().getActionMap();

        // initialisation des éditeurs

        Set<String> doNotBlockComponentIds = new HashSet<>();

        for (String name : ui.get$objectMap().keySet()) {
            Object o = ui.getObjectById(name);

            if (o == null) {
                continue;
            }

            if (o instanceof JComponent) {

                init((JComponent) o, doNotBlockComponentIds);
            }

            if (o instanceof JCheckBox) {
                init((JCheckBox) o);
            }

            if (o instanceof AbstractButton) {
                init(actionMap, (AbstractButton) o);
                continue;
            }

            if (o instanceof NumberEditor) {
                init((NumberEditor) o);
                continue;
            }

            if (o instanceof BeanComboBox<?>) {
                init((BeanComboBox<?>) o);
                continue;
            }

            if (o instanceof BeanListHeader<?>) {
                init((BeanListHeader) o);
                continue;
            }

            if (o instanceof FilterableDoubleList<?>) {
                init((FilterableDoubleList<?>) o);
                continue;
            }

            if (o instanceof JXDatePicker) {
                init((JXDatePicker) o);
                continue;
            }

            if (o instanceof TimeEditor) {
                init((TimeEditor) o);
                continue;
            }

            if (o instanceof DateTimeEditor) {
                init((DateTimeEditor) o);
                continue;
            }

            if (o instanceof CoordinatesEditor) {
                init((CoordinatesEditor) o);
                continue;
            }

            if (o instanceof BooleanEditor) {
                init((BooleanEditor) o);
                continue;
            }

            if (o instanceof EnumEditor) {
                init((EnumEditor) o);
                continue;
            }

            if (o instanceof ObserveSwingValidator<?>) {
                init(ui, (ObserveSwingValidator<?>) o);
                continue;
            }

            if (o instanceof JTextField) {
                init((JTextField) o);
                continue;
            }

            if (o instanceof JTextArea) {
                init((JTextArea) o);
            }

        }

        if (!doNotBlockComponentIds.isEmpty()) {

            String[] acceptedComponentNames = doNotBlockComponentIds.toArray(new String[doNotBlockComponentIds.size()]);

            initBlockLayerUI(acceptedComponentNames);

        }
    }

    protected void initBlockLayerUI(String... doNotBlockComponentIds) {
        ui.getBlockLayerUI().setAcceptedComponentTypes(ObserveMapPane.class, JScrollBar.class);
        ui.getBlockLayerUI().setAcceptedComponentNames(doNotBlockComponentIds);

    }

    protected void init(UI ui, ObserveSwingValidator<?> validator) {
        SwingValidatorUtil.listenValidatorContextNameAndRefreshFields(
                validator,
                (JAXXValidator) ui
        );
    }

    protected void init(ActionMap actionMap, AbstractButton editor) {
        String actionId = (String) editor.getClientProperty(OBSERVE_ACTION);
        if (actionId == null) {
            // le boutton n'est pas commun

            final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_RESET_PROPERTY_NAME);
            if (propertyName != null) {
                editor.addActionListener(e -> JavaBeanObjectUtil.setProperty(getBean(), propertyName, null));
            }

            return;
        }

        // on a trouve une action commune
        AbstractUIAction action = (AbstractUIAction) actionMap.get(actionId);

        if (action == null) {

            // l'action n'est pas enregistrée
            throw new IllegalStateException(
                    "action [" + actionId + "] not found for ui " +
                    ui.getClass().getName());
        }

        if (log.isDebugEnabled()) {
            log.debug("init common action " + actionId);
        }

        action.initAction(null, editor);
    }

    protected void init(NumberEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init number editor " + editor.getName());
        }
        editor.init();
    }

    protected void init(BeanComboBox beanComboBox) {
        if (log.isDebugEnabled()) {
            log.debug("init combobox for " + beanComboBox.getBeanType());
        }
        beanComboBox.setI18nPrefix("observe.common.");
        beanComboBox.setMinimumSize(new Dimension(0, 24));

        Class dtoClass = getDtoClass(beanComboBox);

        if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
            prepareReferentialBeanComboBox(dtoClass, beanComboBox);
        } else {
            prepareDataBeanComboBox(dtoClass, beanComboBox);
        }

    }

    protected void init(BeanListHeader beanList) {

        beanList.setI18nPrefix("observe.common.");

        if (log.isInfoEnabled()) {
            log.info("init list for " + beanList.getBeanType());
        }

        Class dtoClass = getDtoClass(beanList);

        if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
            prepareReferentialEntityList(dtoClass, beanList);
        } else {
            prepareDataEntityList(dtoClass, beanList);
        }

    }

    protected void init(FilterableDoubleList beanList) {

        beanList.setI18nPrefix("observe.common.");

        if (log.isInfoEnabled()) {
            log.info("init list for " + beanList.getBeanType());
        }

        Class dtoClass = getDtoClass(beanList);

        if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
            prepareReferentialFilterableDoubleList(dtoClass, beanList);
        } else {
            prepareDataFilterableDoubleList(dtoClass, beanList);
        }

    }

    protected void init(TimeEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init time editor " + editor.getName());
        }
        editor.init();
        if (isAutoSelectOnFocus(editor)) {

            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getHourEditor().getEditor());
            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getMinuteEditor().getEditor());

        }
    }

    protected void init(DateTimeEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init date time editor " + editor.getName());
        }
        editor.init();
        if (isAutoSelectOnFocus(editor)) {

            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getHourEditor().getEditor());
            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getMinuteEditor().getEditor());

        }
    }

    private void addAutoSelectOnFocus(JSpinner.DateEditor hourEditor) {
        addAutoSelectOnFocus(hourEditor.getTextField());
    }

    protected void init(CoordinatesEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init coordinates editor " + editor.getName());
        }
        editor.init();
    }

    protected void init(JTextField editor) {

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addKeyListener(new KeyAdapter() {

                @Override
                public void keyReleased(KeyEvent e) {
                    JTextField source = (JTextField) e.getSource();
                    String text = source.getText();
                    JavaBeanObjectUtil.setProperty(getBean(), propertyName, text);
                }
            });
        }
    }

    protected void init(JTextArea editor) {

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addKeyListener(new KeyAdapter() {

                @Override
                public void keyReleased(KeyEvent e) {
                    JTextArea source = (JTextArea) e.getSource();
                    String text = source.getText();
                    JavaBeanObjectUtil.setProperty(getBean(), propertyName, text);
                }
            });
        }
    }

    protected void init(BooleanEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init boolean editor " + editor.getName());
        }
        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addItemListener(event -> {
                if (event.getStateChange() == ItemEvent.SELECTED) {

                    Boolean newValue = ((BooleanEditor) event.getSource()).getBooleanValue();
                    JavaBeanObjectUtil.setProperty(getBean(), propertyName, newValue);
                }
            });
        }
    }

    protected void init(JComponent editor, Set<String> notBlockingComponents) {
        final Boolean propertyName = (Boolean) editor.getClientProperty(CLIENT_PROPERTY_NOT_BLOCKING);
        if (propertyName != null) {

            if (editor instanceof FilterableDoubleList) {

                notBlockingComponents.add("universeListPane");
                notBlockingComponents.add("selectedListPane");

            } else {

                notBlockingComponents.add(editor.getName());

            }
        }
    }

    protected void init(JCheckBox editor) {
        if (log.isDebugEnabled()) {
            log.debug("init simple boolean editor " + editor.getName());
        }
        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addItemListener(event -> {
                Boolean newValue = ((JCheckBox) event.getSource()).isSelected();
                JavaBeanObjectUtil.setProperty(getBean(), propertyName, newValue);
            });
        }
    }

    protected void init(JXDatePicker picker) {

        if (log.isDebugEnabled()) {
            log.debug("disable JXDatePicker editor" + picker.getName());
        }
        picker.getEditor().setEditable(false);
        JXMonthView monthView = new JXMonthView();
        monthView.setLowerBound(DateUtil.createDate(1, 1, 1970));
        monthView.setTraversable(true);
        monthView.setZoomable(true);
        picker.setMonthView(monthView);

        final String propertyName = (String) picker.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            picker.addActionListener(e -> {
                JXDatePicker source = (JXDatePicker) e.getSource();
                Date date = source.getDate();
                JavaBeanObjectUtil.setProperty(getBean(), propertyName, date);
            });
        }

    }

    protected <B extends Enum<B>> void init(EnumEditor<B> editor) {
        if (log.isDebugEnabled()) {
            log.debug("init enumEditor editor " + editor.getName());
        }
        ImmutableMap.Builder<B, String> labelsBuilder = ImmutableMap.builder();
        for (B e : EnumSet.allOf(editor.getType())) {
            String label = I18nEnumHelper.getLabel(e);
            labelsBuilder.put(e, label);
        }
        editor.setRenderer(new EnumEditorRenderer<>(labelsBuilder.build()));
    }

    protected boolean isAutoSelectOnFocus(JComponent comp) {
        Boolean selectOnFocus = (Boolean) comp.getClientProperty("selectOnFocus");
        return BooleanUtils.isTrue(selectOnFocus);
    }

    protected void addAutoSelectOnFocus(JTextField jTextField) {
        jTextField.addFocusListener(new FocusAdapter() {

            @Override
            public void focusGained(final FocusEvent e) {
                SwingUtilities.invokeLater(() -> {
                    JTextField source = (JTextField) e.getSource();
                    source.selectAll();
                });

            }
        });
    }

    /**
     * Ajoute à une liste graphique donnée le comportement de
     * sélection-déselection en un seul click.
     *
     * TODO: il faudrait que cela ne perturbe pas le comportement des raccourcis clavier.
     *
     * @param list la liste graphique à traiter
     */
    protected void prepareToogleListSelectionModel(JList list) {

        OneClicListSelectionModel model = new OneClicListSelectionModel(list.getSelectionModel(), list.getModel());
        list.setSelectionModel(model);

    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends DataDto> void prepareDataFilterableDoubleList(Class<D> dtoClass, FilterableDoubleList<DataReference<D>> list) {

        DataReferenceDecorator<D> decorator = decoratorService.getDataReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.getPopupSortLabel().setText(t("observe.content.type.data", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, new ArrayList<>(), new ArrayList<>());

        JList<AbstractReference<D>> selectedList = list.getSelectedList();
        ListCellRenderer<? super AbstractReference<D>> renderer = selectedList.getCellRenderer();

        selectedList.setCellRenderer(new ReferentielListCellRenderer<>(renderer));
        list.getUniverseList().setCellRenderer(new ReferentielListCellRenderer<>((ListCellRenderer<? super AbstractReference<IdDto>>) renderer));

    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends ReferentialDto> void prepareReferentialFilterableDoubleList(Class<D> dtoClass, FilterableDoubleList<ReferentialReference<D>> list) {

        ReferentialReferenceDecorator<D> decorator = decoratorService.getReferentialReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.getPopupSortLabel().setText(t("observe.content.type.referential", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, new ArrayList<>(), new ArrayList<>());

        JList<AbstractReference<D>> selectedList = list.getSelectedList();
        ListCellRenderer<? super AbstractReference<D>> renderer = selectedList.getCellRenderer();

        selectedList.setCellRenderer(new ReferentielListCellRenderer<>(renderer));
        list.getUniverseList().setCellRenderer(new ReferentielListCellRenderer<>((ListCellRenderer<? super AbstractReference<IdDto>>) renderer));

    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends DataDto> void prepareDataEntityList(Class<D> dtoClass, BeanListHeader<DataReference<D>> list) {

        DataReferenceDecorator<D> decorator = decoratorService.getDataReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.setPopupTitleText(t("observe.content.type.data", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, Collections.emptyList());

        JList<AbstractReference<D>> list1 = list.getList();
        ListCellRenderer<? super AbstractReference<D>> renderer = list1.getCellRenderer();
        list1.setCellRenderer(new ReferentielListCellRenderer<>(renderer));

    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends ReferentialDto> void prepareReferentialEntityList(Class<D> dtoClass, BeanListHeader<ReferentialReference<D>> list) {

        ReferentialReferenceDecorator<D> decorator = decoratorService.getReferentialReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.setPopupTitleText(t("observe.content.type.referential", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, Collections.emptyList());

        JList<AbstractReference<D>> list1 = list.getList();
        ListCellRenderer<? super AbstractReference<D>> renderer = list1.getCellRenderer();
        list1.setCellRenderer(new ReferentielListCellRenderer<>(renderer));

    }

    /**
     * Prépare un component de choix d'entités pour un type d'entité donné et pour un service de persistance donné.
     *
     * @param <D>      le type de l'entité
     * @param comboBox le component graphique à initialiser
     */
    protected <D extends DataDto> void prepareDataBeanComboBox(Class<D> dtoClass, BeanComboBox<DataReference<D>> comboBox) {

        DataReferenceDecorator<D> decorator = decoratorService.getDataReferenceDecorator(dtoClass);

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        comboBox.setPopupTitleText(t("observe.content.type.data", entityLabel));

        comboBox.init(decorator, Collections.emptyList());

        JComboBox<DataReference<D>> combobox = comboBox.getCombobox();

        ListCellRenderer<DataReference<D>> toolTipRenderer = new ComboBoxListCellRenderer<>(combobox.getRenderer());
        combobox.setRenderer(toolTipRenderer);
        if (log.isDebugEnabled()) {
            log.debug("combo  list [" + dtoClass.getName() + "] : " + comboBox.getData().size());
        }

    }

    /**
     * Prépare un component de choix d'entités pour un type d'entité donné et pour un service de persistance donné.
     *
     * @param <D>      le type de l'entité
     * @param comboBox le component graphique à initialiser
     */
    protected <D extends ReferentialDto> void prepareReferentialBeanComboBox(Class<D> dtoClass, BeanComboBox<ReferentialReference<D>> comboBox) {

        ReferentialReferenceDecorator<D> decorator = decoratorService.getReferentialReferenceDecorator(dtoClass);

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        comboBox.setPopupTitleText(t("observe.content.type.referential", entityLabel));

        comboBox.init(decorator, Collections.emptyList());

        JComboBox<ReferentialReference<D>> combobox = comboBox.getCombobox();

        ListCellRenderer<ReferentialReference<D>> toolTipRenderer = new ComboBoxListCellRenderer<>(combobox.getRenderer());
        combobox.setRenderer(toolTipRenderer);
        if (log.isDebugEnabled()) {
            log.debug("combo  list [" + dtoClass.getName() + "] : " + comboBox.getData().size());
        }

    }

    protected <D extends IdDto> Class<D> getDtoClass(JComponent list) {
        Object clientProperty = list.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_ENTITY_CLASS);
        return (Class<D>) clientProperty;
    }

    /**
     * Un renderer de liste d'entites d'un referentiel dans le quel on veut
     * differencier les entites qui sont desactivees.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 1.2
     */
    public static class ReferentielListCellRenderer<D extends IdDto> implements ListCellRenderer<AbstractReference<D>> {

        /** la couleur normal pour les entites non desactivees */
        protected Color normalColor;

        /** la couleur a utiliser pour les entites desactivees */
        protected Color disableColor = Color.LIGHT_GRAY;

        protected ListCellRenderer<? super AbstractReference<D>> delegate;

        public ReferentielListCellRenderer(ListCellRenderer<? super AbstractReference<D>> delegate) {
            this.delegate = delegate;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends AbstractReference<D>> list, AbstractReference<D> value, int index, boolean isSelected, boolean cellHasFocus) {
            JComponent comp;
            comp = (JComponent) delegate.getListCellRendererComponent(
                    list,
                    value,
                    index,
                    isSelected,
                    cellHasFocus);
            if (normalColor == null) {
                // premiere fois, on intialise la couleur dite normale
                normalColor = comp.getForeground();
            }

            String tip = null;

            // par defaut, on utilise la couleur normale
            Color col = normalColor;
            if (value != null) {

                boolean enabled = true;

                if (value instanceof ReferentialReference<?>) {

                    ReferentialReference e = (ReferentialReference) value;
                    enabled = e.isEnabled();

                }

                if (!enabled) {
                    // l'entite est desactivee
                    // on la grise pour bien la differencier
                    col = disableColor;
                    tip = t("observe.common.obsolete.entity", ((JLabel) comp).getText());
                }
            }
            comp.setForeground(col);
            comp.setToolTipText(tip);
            return comp;
        }

    }

    private static class ComboBoxListCellRenderer<E> implements ListCellRenderer<E> {

        private final ListCellRenderer<? super E> renderer;

        public ComboBoxListCellRenderer(ListCellRenderer<? super E> renderer) {
            this.renderer = renderer;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends E> list,
                                                      E value,
                                                      int index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus) {
            Component comp = renderer.getListCellRendererComponent(
                    list,
                    value,
                    index,
                    isSelected,
                    cellHasFocus
            );
            if (comp instanceof JLabel) {
                JLabel jcomp = (JLabel) comp;
                jcomp.setToolTipText(jcomp.getText());
            }
            return comp;
        }
    }
}
