package fr.ird.observe.application.swing.ui.content.impl.longline;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import jaxx.runtime.validator.swing.SwingValidator;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class LonglineDetailCompositionValidatorService {

    private final SwingValidator<SectionDto> sectionValidator;

    private final SwingValidator<BasketDto> basketValidator;

    private final SwingValidator<BranchlineDto> branchlineValidator;

    private final DecoratorService decoratorService;

    private final Map<SwingValidator, JComponent> validatorEditors;

    public LonglineDetailCompositionValidatorService(SwingValidator<SectionDto> sectionValidator,
                                                     SwingValidator<BasketDto> basketValidator,
                                                     SwingValidator<BranchlineDto> branchlineValidator,
                                                     Map<SwingValidator, JComponent> validatorEditors,
                                                     DecoratorService decoratorService) {
        this.sectionValidator = sectionValidator;
        this.basketValidator = basketValidator;
        this.branchlineValidator = branchlineValidator;
        this.validatorEditors = validatorEditors;
        this.decoratorService = decoratorService;

    }

    public List<SwingValidatorMessage> validateSections(List<? extends SectionDto> sections) {

        LonglineDetailCompositionValidationContext validationContext = new LonglineDetailCompositionValidationContext(decoratorService, validatorEditors);

        addListener(validationContext);

        try {

            for (SectionDto section : sections) {

                validateSection(validationContext, section);

            }

        } finally {

            removeListener(validationContext);

        }

        return validationContext.getMessages();

    }

    protected void validateSection(LonglineDetailCompositionValidationContext validationContext, SectionDto section) {

        validationContext.setSection(section);
        validationContext.setBasket(null);
        validationContext.setBranchline(null);

        sectionValidator.setBean(null);
        sectionValidator.setBean(section);

        if (!section.isBasketEmpty()) {

            BasketDto previousBasket = null;

            for (BasketDto basket : section.getBasket()) {

                if (previousBasket != null) {

                    // validate previousBasket.floatline2Length = basket.floatline1Length
                    Float previousBasketFloatline2Length = previousBasket.getFloatline2Length();
                    Float basketFloatline1Length = basket.getFloatline1Length();
                    if (previousBasketFloatline2Length != null
                            && basketFloatline1Length != null
                            && Math.abs(previousBasketFloatline2Length - basketFloatline1Length) > 0.001f) {

                        validationContext.addMessage(basketValidator, NuitonValidatorScope.ERROR, "floatline2Length", t("observe.content.basket.invalid.nextFloatline1Length", previousBasketFloatline2Length, basketFloatline1Length));

                    }
                }

                validateBasket(validationContext, basket);

                previousBasket = basket;

            }
        }

    }

    protected void validateBasket(LonglineDetailCompositionValidationContext validationContext, BasketDto basket) {

        validationContext.setBranchline(null);
        validationContext.setBasket(basket);

        basketValidator.setBean(null);
        basketValidator.setBean(basket);

        if (!basket.isBranchlineEmpty()) {

            for (BranchlineDto branchline : basket.getBranchline()) {

                validateBranchline(validationContext, branchline);

            }
        }

        validationContext.setBranchline(null);

    }

    protected void validateBranchline(LonglineDetailCompositionValidationContext validationContext, BranchlineDto branchline) {

        validationContext.setBranchline(branchline);
        branchlineValidator.setBean(null);
        branchlineValidator.setBean(branchline);


    }

    protected void addListener(LonglineDetailCompositionValidationContext validationContext) {

        sectionValidator.addSimpleBeanValidatorListener(validationContext);
        basketValidator.addSimpleBeanValidatorListener(validationContext);
        branchlineValidator.addSimpleBeanValidatorListener(validationContext);

    }

    protected void removeListener(LonglineDetailCompositionValidationContext validationContext) {

        sectionValidator.removeSimpleBeanValidatorListener(validationContext);
        basketValidator.removeSimpleBeanValidatorListener(validationContext);
        branchlineValidator.removeSimpleBeanValidatorListener(validationContext);

    }

}
