/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.util.table.ObserveBooleanTableCellRenderer;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataFileDto;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.security.InvalidAuthenticationTokenException;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.swing.renderer.DecoratorTableCellRenderer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.painter.Painter;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.runtime.JaxxFileChooser;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.event.HyperlinkEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Une classe d'aide pour les ui
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class UIHelper extends SwingUtil {

    /** Logger */
    static private final Log log = LogFactory.getLog(UIHelper.class);

    /**
     * Pattern to use for short numeric values in editors with max 3 digits.
     *
     * @since 2.3
     */
    public static final String INT_3_DIGITS_PATTERN = "\\d{0,3}";

    /**
     * Pattern to use for integer numeric values in editors with max 6 digits.
     *
     * @since 2.3
     */
    public static final String INT_6_DIGITS_PATTERN = "\\d{0,6}";

    /**
     * Pattern to use for integer numeric values in editors with max 7 digits.
     *
     * @since 2.3
     */
    public static final String INT_7_DIGITS_PATTERN = "\\d{0,7}";

    /**
     * Pattern to use for long numeric values in editors with max 10 digits.
     *
     * @since 3.7
     */
    public static final String LONG_10_DIGITS_PATTERN = "\\d{0,10}";

    /**
     * Pattern to use for decimal numeric values with 1 decimal digits in
     * editors.
     *
     * @since 2.3
     */
    public static final String DECIMAL1_PATTERN = "\\d{0,6}|\\d{1,6}.\\d{0,1}";

    /**
     * Pattern to use for decimal numeric values with 2 decimal digits in
     * editors.
     *
     * @since 2.3
     */
    public static final String DECIMAL2_PATTERN = "\\d{0,6}|\\d{1,6}.\\d{0,2}";

    /**
     * Pattern to use for decimal numeric values with 3 decimal digits in
     * editors.
     *
     * @since 2.3
     */
    public static final String DECIMAL3_PATTERN = "\\d{0,6}|\\d{1,6}.\\d{0,3}";

    /**
     * Components which must NOT be blocked by any layer in ui.
     *
     * @since 2.0
     */
    public static final String[] ACCEPTABLE_COMPONENTS = {
            // show progress ui in acess import
            "showProgressButton",
            // copy global progression of resume admin tab to clipboard
            "globalProgressionCopyToClipBoard",
            // copy progression of any admin tab to clipboard
            "progressionTopCopyCliptBoard"
    };

    public static void displayInfo(String text) {

        ObserveMainUI ui = ObserveSwingApplicationContext.get().getMainUI();
        if (ui == null) {
            JOptionPane.showMessageDialog(null, text);
        } else {
            ui.getStatus().setStatus(text);
        }
    }

    public static void displayWarning(String title, String text) {

        ObserveMainUI ui = ObserveSwingApplicationContext.get().getMainUI();
        JOptionPane.showMessageDialog(ui, text, title, JOptionPane.WARNING_MESSAGE);

    }

    public static int askUser(String title,
                              String message,
                              int typeMessage,
                              Object[] options,
                              int defaultOption) {
        return askUser(
                null,
                title,
                message,
                typeMessage,
                options,
                defaultOption
        );
    }

    public static int askUser(Component parent,
                              String title,
                              Object message,
                              int typeMessage,
                              Object[] options,
                              int defaultOption) {
        if (parent == null) {
            ObserveSwingApplicationContext tx = ObserveSwingApplicationContext.get();
            if (tx != null) {
                parent = ObserveSwingApplicationContext.get().getMainUI();
            }
        }
        return JOptionPane.showOptionDialog(
                parent,
                message,
                title,
                JOptionPane.DEFAULT_OPTION,
                typeMessage,
                null,
                options,
                options[defaultOption]
        );
    }

    /**
     * Choisir un fichier via un sélecteur graphique de fichiers.
     *
     * @param parent      le component swing appelant le controle
     * @param title       le titre du dialogue de sélection
     * @param buttonLabel le label du boutton d'acceptation
     * @param incoming    le fichier de base à utilier
     * @param filters     les filtres + descriptions sur le sélecteur de
     *                    fichiers
     * @return le fichier choisi ou le fichier incoming si l'opération a été
     * annulée
     */
    public static File chooseFile(Component parent,
                                  String title,
                                  String buttonLabel,
                                  File incoming,
                                  String... filters) {

        JaxxFileChooser.ToLoadFile toLoadFile = JaxxFileChooser.forLoadingFile()
                                                               .setParent(parent)
                                                               .setTitle(title)
                                                               .setApprovalText(buttonLabel)
                                                               .setPatternOrDescriptionFilters(Arrays.asList(filters));

        File parentDirectoryIfExist = getParentDirectoryIfExist(incoming);
        if (parentDirectoryIfExist != null) {
            toLoadFile.setStartDirectory(parentDirectoryIfExist);
        }
        File file = toLoadFile.choose();
        ;
        if (log.isDebugEnabled()) {
            log.debug(title + " : " + file);
        }
        return file == null ? incoming : file;
    }

    /**
     * Choisir un répertoire via un sélecteur graphique de fichiers.
     *
     * @param parent      le component swing appelant le controle
     * @param title       le titre de la boite de dialogue de sléection
     * @param buttonLabel le label de l'action d'acceptation
     * @param incoming    le fichier de base à utiliser
     * @return le répertoire choisi ou le répertoire incoming si l'opération a
     * été annulée
     */
    public static File chooseDirectory(Component parent,
                                       String title,
                                       String buttonLabel,
                                       File incoming) {

        JaxxFileChooser.ToLoadDirectory toLoadDirectory = JaxxFileChooser.forLoadingDirectory()
                                                                         .setParent(parent)
                                                                         .setTitle(title)
                                                                         .setApprovalText(buttonLabel);
        File parentDirectoryIfExist = getParentDirectoryIfExist(incoming);
        if (parentDirectoryIfExist != null) {
            toLoadDirectory.setStartDirectory(parentDirectoryIfExist);
        }
        File file = toLoadDirectory.choose();
        if (log.isDebugEnabled()) {
            log.debug(title + " : " + file);
        }
        return file;
    }

    private static File getParentDirectoryIfExist(File incoming) {
        if (incoming != null) {
            File basedir;
            if (incoming.isFile()) {
                basedir = incoming.getParentFile();
            } else {
                basedir = incoming;
            }
            if (basedir.exists()) {
                return basedir;
            }
        }
        return null;
    }

    /**
     * Ouvre une demande de confirmation, avant suppression d'une entité.
     *
     * @param <E>le     type de l'entité à supprimer
     * @param parent    le component graphique demande l'action
     * @param beanClass le tyê de l'entité à supprimer
     * @param bean      l'entité à supprimer
     * @return {@code true} si l'utilisateur a confitmé la suppression,
     * {@code false} sinon.
     */
    public static <E extends IdDto> boolean confirmForEntityDelete(
            JAXXObject parent,
            Class<E> beanClass,
            E bean) {

        return confirmForEntityDelete(parent, beanClass, bean, null);
    }

    /**
     * Ouvre une demande de confirmation, avant suppression d'une entité.
     *
     * @param <E>          le type de l'entité à supprimer
     * @param parent       le component graphique demande l'action
     * @param beanClass    le tyê de l'entité à supprimer
     * @param bean         l'entité à supprimer
     * @param extraMessage un message supplémentaire a ajouter
     * @return {@code true} si l'utilisateur a confitmé la suppression,
     * {@code false} sinon.
     */
    public static <E extends IdDto> boolean confirmForEntityDelete(
            JAXXObject parent,
            Class<E> beanClass,
            E bean,
            String extraMessage) {
        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();

        if (parent == null) {
            // on cherche l'ui principale
            parent = mainUI;
        }

        if (mainUI != null) {
            mainUI.setBusy(true);
        }
        DecoratorService decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        Decorator<E> decorator = decoratorService.getDecoratorByType(beanClass);
        String beanStr;
        String messageDelete;
        String type = ObserveI18nDecoratorHelper.getTypeI18nKey(beanClass);
        type = t(type);

        if (bean == null || bean.getId() == null || decorator == null) {
            // delete new entity
            messageDelete = t("observe.message.delete.new", type);
        } else {
            try {
                // delete existing entity
                beanStr = decorator.toString(bean);
                messageDelete = t("observe.message.delete", type, beanStr);
            } catch (Exception e) {
                messageDelete = t("observe.message.delete.new", type);
            }
        }

        if (extraMessage != null) {
            messageDelete += '\n' + extraMessage;
        }

        int response = askUser((Component) parent,
                               t("observe.title.delete"),
                               messageDelete,
                               JOptionPane.WARNING_MESSAGE,
                               new Object[]{t("observe.choice.confirm.delete"),
                                       t("observe.choice.cancel")},
                               1);
        if (mainUI != null) {
            mainUI.setBusy(true);
        }
        return response == 0;
    }

    public static boolean confirmOverwriteFileIfExist(Component parent, File file) {
        boolean write = true;
        if (file.exists()) {
            write = UIHelper.askUser(
                    parent,
                    t("observe.common.saveFile.overwrite.title"),
                    t("observe.common.saveFile.overwrite"),
                    JOptionPane.OK_CANCEL_OPTION,
                    new Object[]{
                            t("observe.common.saveFile.overwrite.ok"),
                            t("observe.common.saveFile.overwrite.cancel")},
                    1) == 0;
        }

        return write;
    }


    public static DecoratorTableCellRenderer newDecorateTableCellRenderer(
            TableCellRenderer renderer, Class<?> entityClass) {
        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getDecoratorByType(entityClass);
        return new DecoratorTableCellRenderer(renderer, decorator);
    }

    public static DecoratorTableCellRenderer newDecorateTableCellRenderer(
            TableCellRenderer renderer, Class<?> entityClass, String context) {
        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getDecoratorByType(entityClass, context);
        return new DecoratorTableCellRenderer(renderer, decorator);
    }

    public static <T extends DataDto> DecoratorTableCellRenderer newDataReferenceDecorateTableCellRenderer(TableCellRenderer renderer,
                                                                                                           Class<T> entityClass,
                                                                                                           String context) {
        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getDataReferenceDecorator(entityClass, context);
        return new DecoratorTableCellRenderer(renderer, decorator);
    }

    public static <T extends ReferentialDto> DecoratorTableCellRenderer newReferentialReferenceDecorateTableCellRenderer(TableCellRenderer renderer,
                                                                                                                         Class<T> entityClass) {
        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(entityClass);
        return new DecoratorTableCellRenderer(renderer, decorator);
    }

    /**
     * Copy to clipBoard the content of the given text.
     *
     * @param text text to copy to clipboard
     * @since 2.0
     */
    public static void copyToClipBoard(String text) {

        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        if (log.isInfoEnabled()) {
            log.info("Put in clipboard :\n" + text);
        }
        StringSelection selection = new StringSelection(text);
        clipboard.setContents(selection, selection);
    }

    public static void setMainUIVisible(final ObserveMainUI ui) {

        // force le redimensionnement du splitpane
        ui.getSplitpane().revalidate();

        // force le redimensionnement du splitpane2
        ui.getSplitpane2().revalidate();

        // affichage de l'interface graphique
        SwingUtilities.invokeLater(() -> ui.setVisible(true));
    }

    public static <E extends IdDto> E getEntity(List<E> list, E entity) {
        for (E e : list) {
            if (e.getId().equals(entity.getId())) {
                return e;
            }
        }
        return null;
    }

    /**
     * Open a link coming from a {@link HyperlinkEvent}.
     *
     * And try to open the link if an url in a browser.
     *
     * @param he the event to treate
     * @since 1.6.0
     */
    public static void openLink(HyperlinkEvent he) {
        if (he.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {

            if (Desktop.isDesktopSupported()) {
                try {
                    URL u = he.getURL();
                    if (u.getProtocol().equalsIgnoreCase("mailto") ||
                            u.getProtocol().equalsIgnoreCase("http") ||
                            u.getProtocol().equalsIgnoreCase("ftp") ||
                            u.getProtocol().equalsIgnoreCase("file")) {
                        Desktop.getDesktop().browse(u.toURI());
                    }
                } catch (IOException | URISyntaxException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Error while opening link", e);
                    }
                }
            }
        }
    }

    public static void stopEditing(JTable table) {
        TableCellEditor cellEditor = table.getCellEditor();
        if (cellEditor != null) {
            cellEditor.cancelCellEditing();
        }
    }

    public static TableCellRenderer newBooleanTableCellRenderer(
            TableCellRenderer renderer) {
        return new ObserveBooleanTableCellRenderer(renderer);
    }

    public static DataFileDto fileToDataFileDto(File file) {

        DataFileDto result = new DataFileDto();

        try {

            byte[] fileContent = FileUtils.readFileToByteArray(file);

            result.setContent(fileContent);
            result.setName(file.getName());

        } catch (IOException e) {
            throw new ObserveSwingTechnicalException("Could not read file: " + file, e);
        }

        return result;
    }

    public static void handlingError(Exception e) {
        handlingError(e.getMessage(), e);

    }

    public static void handlingError(String message, Exception e) {

        if (log.isErrorEnabled()) {
            log.error(message, e);
        }

        if (containsExceptionInStack(e, InvalidAuthenticationTokenException.class)) {

            ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

            // on indique que la source de donnée a expiré
            context.getDataSourcesManager().getMainDataSource().expired();

            ObserveMainUI observeMainUI = context.getMainUI();

            int result = askUser(observeMainUI,
                                 t("observe.storage.server.sessionExpire.title"),
                                 t("observe.storage.server.sessionExpire"),
                                 JOptionPane.OK_CANCEL_OPTION,
                                 new Object[]{
                                         t("observe.storage.server.sessionExpire.reload"),
                                         t("observe.storage.server.sessionExpire.change"),
                                         t("observe.storage.server.sessionExpire.close")},
                                 0);


            // FIXME SBavencoff 23/03/2016 est on sùr que l'erreur proviens du main storage ?
            switch (result) {
                case 0:
                    observeMainUI.getReloadStorageAction().run();
                    break;
                case 1:
                    observeMainUI.getChangeStorageAction().run();
                    break;
                default:
                    observeMainUI.getCloseStorageAction().run();
                    break;
            }

        } else {
            ErrorDialogUI.showError(e);
        }

    }

    static protected <E extends Throwable> boolean containsExceptionInStack(Throwable e, Class<E> type) {

        if (type.isInstance(e)) {
            return true;
        }

        while (e.getCause() != null) {
            e = e.getCause();

            if (type.isInstance(e)) {
                return true;
            }

        }

        return false;

    }


    public static void initUI(JScrollPane selectedTreePane, JTree tree) {

        // customize tree selection colors
        UIDefaults defaults = new UIDefaults();

        Painter<JComponent> painter = (g, c, w, h) -> g.fillRect(0, 0, w, h);
        defaults.put("Tree:TreeCell[Enabled+Selected].backgroundPainter", painter);
        defaults.put("Tree:TreeCell[Enabled+Focused].backgroundPainter", painter);
        defaults.put("Tree:TreeCell[Focused+Selected].backgroundPainter", painter);

        tree.putClientProperty("Nimbus.Overrides", defaults);
        // Fix http://forge.codelutin.com/issues/2781
        selectedTreePane.getViewport().setBackground(Color.WHITE);
    }
}
