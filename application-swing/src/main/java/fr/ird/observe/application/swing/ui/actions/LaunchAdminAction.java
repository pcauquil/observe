package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUILauncher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class LaunchAdminAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(LaunchAdminAction.class);

    private final ObserveMainUI ui;

    private final AdminStep action;

    public LaunchAdminAction(ObserveMainUI ui, AdminStep action) {

        super(t(action.getOperationLabel()), action.getIcon());
        this.ui = ui;
        this.action = action;
        putValue(SHORT_DESCRIPTION, t(action.getOperationDescription()));
        putValue(MNEMONIC_KEY, (int) t(action.getOperationLabel()).charAt(0));

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {

            if (log.isInfoEnabled()) {
                log.info("Will start admin action: " + I18nEnumHelper.getLabel(action));
            }
            AdminUILauncher launcher = AdminUILauncher.newLauncher(ui, action);
            launcher.start();
        }

    }

}
