package fr.ird.observe.application.swing.ui.tree.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created on 1/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public abstract class NodeChangeActionListener implements ActionListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(NodeChangeActionListener.class);

    private final String nodeId;

    private final String parentNodeId;

    private final ObserveTreeHelper treeHelper;

    public NodeChangeActionListener(ObserveTreeHelper treeHelper,
                                    String nodeId,
                                    String parentNodeId) {
        this.nodeId = nodeId;
        this.parentNodeId = parentNodeId;
        this.treeHelper = treeHelper;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        ObserveNode node = treeHelper.getSelectedNode();
        ObserveNode oldParentNode = getParentNode(node);
        ObserveNode grandParentNode = oldParentNode.getParent();
        ObserveNode newParentNode = getNewParentNode(grandParentNode, parentNodeId);

        closeNode(node.getId());

        int position = moveNodeToParent(nodeId, parentNodeId, oldParentNode.getId());

        treeHelper.selectNode(newParentNode);

        treeHelper.removeNode(node);

        ObserveNode newNode = treeHelper.getChild(newParentNode, nodeId);

        if (newNode == null) {

            // create it
            if (log.isInfoEnabled()) {
                log.info("Insert node: ");
            }
            treeHelper.insertNode(newParentNode, node, position);
            newNode = node;
        }

        treeHelper.selectNode(newNode);

    }

    protected ObserveTreeHelper getTreeHelper() {
        return treeHelper;
    }

    protected abstract void closeNode(String nodeId);

    protected abstract ObserveNode getParentNode(ObserveNode node);

    protected abstract ObserveNode getNewParentNode(ObserveNode grandParentNode, String parentNodeId);

    protected abstract int moveNodeToParent(String nodeId, String parentNodeId, String oldParentNodeId);
}
