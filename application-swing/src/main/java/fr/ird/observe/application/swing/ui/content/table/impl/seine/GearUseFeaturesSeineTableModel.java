package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.GearUseFeaturesMeasurementSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDtos;
import fr.ird.observe.services.dto.seine.TripSeineGearUseDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.16
 */
public class GearUseFeaturesSeineTableModel extends ContentTableModel<TripSeineGearUseDto, GearUseFeaturesSeineDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GearUseFeaturesSeineTableModel.class);

    private static final long serialVersionUID = 1L;

    private transient GearUseFeaturesSeineUIHandler gearUseFeaturesSeineUIHandler;

    public GearUseFeaturesSeineTableModel(ObserveContentTableUI<TripSeineGearUseDto, GearUseFeaturesSeineDto> context,
                                          List<ContentTableMeta<GearUseFeaturesSeineDto>> contentTableMetas) {
        super(context, contentTableMetas);
    }

    public void setGearUseFeaturesSeineUIHandler(GearUseFeaturesSeineUIHandler gearUseFeaturesSeineUIHandler) {
        this.gearUseFeaturesSeineUIHandler = gearUseFeaturesSeineUIHandler;
    }

    @Override
    public void addNewEntry() {

        int editingRow = getSelectedRow();

        if (editingRow > -1) {

            // store sizes and weights for the selected row
            // before creating a new one ?
            GearUseFeaturesSeineUIModel model = getModel();
            model.getMeasurementsTableModel().storeInCacheForRow(editingRow);

        }

        super.addNewEntry();
    }

    @Override
    public void updateRowFromEditBean() {

        GearUseFeaturesSeineUIModel model = getModel();

        int editingRow = getSelectedRow();
        GearUseFeaturesSeineDto rowBean = getRowBean();
        GearUseFeaturesMeasurementSeinesTableModel measurementsTableModel = model.getMeasurementsTableModel();

        List<GearUseFeaturesMeasurementSeineDto> measurements;

        if (rowBean.getId() == null && CollectionUtils.isEmpty(measurementsTableModel.getCacheForRow(editingRow))) {

            // new gear usage, add default measurements

            ReferentialReference<GearDto> gear = rowBean.getGear();
            measurements = gearUseFeaturesSeineUIHandler.getDefaultGearUseFeaturesMeasurementSeine(gear.getId());
            if (log.isInfoEnabled()) {
                log.info("Create mode, use default measurements: " + measurements.size());
            }
            measurementsTableModel.removeCacheForRow(editingRow);
            measurementsTableModel.initCacheForRow(editingRow, measurements);

            measurementsTableModel.setData(measurements);
            measurementsTableModel.setModified(false);

        } else {

            // store current measurements for the selected row
            measurementsTableModel.storeInCacheForRow(editingRow);

            measurements = measurementsTableModel.getData();
        }

        rowBean.setGearUseFeaturesMeasurement(Sets.newLinkedHashSet(measurements));

        super.updateRowFromEditBean();


    }

    @Override
    protected void removeRow(int row) {
        super.removeRow(row);

        // remove sizes and weights for the deleted row
        // also update rows to row - 1 (when after the deleted row)
        GearUseFeaturesSeineUIModel model = getModel();
        model.getMeasurementsTableModel().removeCacheForRow(row);

    }

    @Override
    protected void setChilds(TripSeineGearUseDto parent, List<GearUseFeaturesSeineDto> childs) {
        parent.getGearUseFeaturesSeine().clear();
        parent.addAllGearUseFeaturesSeine(childs);
    }

    @Override
    public void resetEditBean() {

        int row = getSelectedRow();
        if (log.isInfoEnabled()) {
            log.info("Reset edit bean at row: " + row);
        }
        GearUseFeaturesSeineUIModel model = getModel();
        model.getMeasurementsTableModel().resetCacheForRow(row);

        super.resetEditBean();

    }

    @Override
    protected Collection<GearUseFeaturesSeineDto> getChilds(TripSeineGearUseDto bean) {
        return bean.getGearUseFeaturesSeine();
    }

    @Override
    protected void load(GearUseFeaturesSeineDto source, GearUseFeaturesSeineDto target) {
        GearUseFeaturesSeineDtos.copyGearUseFeaturesSeineDto(source, target);
    }

    @Override
    protected void resetRow(int row) {
        super.resetRow(row);

        GearUseFeaturesSeineUIModel model = getModel();
        model.getMeasurementsTableModel().resetCacheForRow(row);
    }

    @Override
    protected GearUseFeaturesSeineUIModel getModel() {
        return (GearUseFeaturesSeineUIModel) super.getModel();
    }
}
