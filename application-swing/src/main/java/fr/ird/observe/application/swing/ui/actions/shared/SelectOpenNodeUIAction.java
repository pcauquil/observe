/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.ui.ObserveMainUI;

import javax.swing.JComponent;
import java.awt.event.ActionEvent;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class SelectOpenNodeUIAction extends AbstractUIAction {

    public static final String ACTION_NAME = "selectOpen";

    private static final long serialVersionUID = 1L;

    public SelectOpenNodeUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, "<NONE>", "<NONE>", "go-down");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JComponent c = (JComponent) e.getSource();
        Class<?> type = (Class<?>) c.getClientProperty("type");
        if (type == null) {
            throw new IllegalStateException(
                    "could not find client property " +
                    "type on component" + c);
        }
        getMainUI().getTreeHelper().selectOpenNode(type);
    }
}
