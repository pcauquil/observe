/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.trip.ExportTripRequest;
import fr.ird.observe.services.service.trip.ExportTripResult;
import fr.ird.observe.services.service.trip.ImportTripRequest;
import fr.ird.observe.services.service.trip.TripManagementService;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Window;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Un wizard pour effectuer des backup de storages.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class StorageBackupUILauncher extends StorageUILauncher {


    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(StorageBackupUILauncher.class);

    public StorageBackupUILauncher(JAXXContext context,
                                   Window frame,
                                   String title) {
        super(context, frame, title);
    }

    @Override
    protected void init(StorageUI ui) {
        super.init(ui);
        ui.getBACKUP().getDoBackup().setSelected(true);
        ui.getBACKUP().getDoBackup().setVisible(false);

        ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();

        StorageUIModel model = ui.getModel();
        model.setCanCreateLocalService(false);

        if (source.isLocal()) {
            model.setCanUseLocalService(true);
            model.setDbMode(DbMode.USE_LOCAL);
        } else if (source.isRemote()) {
            model.setCanUseRemoteService(true);
            model.setDbMode(DbMode.USE_REMOTE);
        } else if (source.isServer()) {
            model.setCanUseServerService(true);
            model.setDbMode(DbMode.USE_SERVER);
        }

        model.setSteps(StorageStep.BACKUP, StorageStep.SELECT_DATA, StorageStep.CONFIRM);
        ui.setTitle(title);
        try {

            // Selection des données uniquement pour une base locale
            // Voir https://forge.codelutin.com/issues/7207
            boolean selectAllData = model.isLocal();
            getStorageUIHandler().initSelectData(ui, source, selectAllData);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(e, e);
            }
        }
    }

    @Override
    protected void doAction(StorageUI ui) {
        super.doAction(ui);

        StorageUIModel storageModel = ui.getModel();
        File backupFile = storageModel.getBackupFile();

        Objects.requireNonNull(backupFile, "file where to backup can not be null");

        ObserveSwingDataSource localSource = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
        ObserveSwingDataSource sourceToBackup = localSource;

        try {
            DataSelectionModel dataModel = storageModel.getSelectDataModel();
            Map<ReferentialReference<ProgramDto>, List<DataReference>> tripsByProgram;
            if (dataModel != null && !dataModel.isDataFull()) {
//            // on renseigne les marees a exporter uniquement si
//            // on en a selectionner, de plus si on a selectionne
//            // toutes les marees, on le les passe pas : car c un dump
//            // complet de la base.

                tripsByProgram = dataModel.getSelectedDataByProgram();

                // on doit creer une nouvelle base avec tout ce qui va bien

                TripManagementService managementService = localSource.newTripManagementService();

                sourceToBackup = ObserveSwingApplicationContext.get().getDataSourcesManager().newTemporaryH2Datasource("Backup-" + backupFile.getName());

                DataSourceCreateConfigurationDto createDto = new DataSourceCreateConfigurationDto();
                createDto.setLeaveOpenSource(true);
                createDto.setImportReferentialDataSourceConfiguration(localSource.getConfiguration());

                sourceToBackup.create(createDto);

                TripManagementService tmpManagementService = sourceToBackup.newTripManagementService();

                for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference>> entry : tripsByProgram.entrySet()) {

                    for (DataReference trip : entry.getValue()) {

                        ExportTripRequest exportRequest = new ExportTripRequest(false, entry.getKey().getId(), trip.getId());
                        ExportTripResult exportTripResult = managementService.exportTrip(exportRequest);

                        ImportTripRequest importTripRequest = new ImportTripRequest(exportTripResult);
                        tmpManagementService.importTrip(importTripRequest);

                    }

                }

            }

            if (log.isInfoEnabled()) {
                log.info("Exporting to " + backupFile);
            }

            sourceToBackup.newDataSourceService().backup(backupFile);

        } catch (DatabaseNotFoundException | DatabaseConnexionNotAuthorizedException | BabModelVersionException | IncompatibleDataSourceCreateConfigurationException | DataSourceCreateWithNoReferentialImportException e) {
            throw new ObserveSwingTechnicalException(e);
        } finally {

            if (sourceToBackup != localSource) {
                sourceToBackup.close();
            }

        }
    }
}
