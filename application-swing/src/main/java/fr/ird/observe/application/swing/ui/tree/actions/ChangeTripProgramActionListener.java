package fr.ird.observe.application.swing.ui.tree.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.dto.IdDtos;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 1/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class ChangeTripProgramActionListener extends NodeChangeActionListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeTripProgramActionListener.class);

    public ChangeTripProgramActionListener(ObserveTreeHelper treeHelper,
                                           ObserveSwingDataSource dataSource,
                                           String tripId,
                                           String programId) {
        super(treeHelper, tripId, programId);
    }

    @Override
    protected void closeNode(String tripId) {
        // Don't do anything : trip should stay open when being transferred
    }

    @Override
    protected ObserveNode getParentNode(ObserveNode node) {
        return node.getParent();
    }

    @Override
    protected ObserveNode getNewParentNode(ObserveNode grandParentNode, String parentNodeId) {
        return getTreeHelper().getChild(grandParentNode, parentNodeId);
    }

    @Override
    protected int moveNodeToParent(String nodeId, String parentNodeId, String oldParentNodeId) {
        int position;

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveServicesProvider servicesProvider = applicationContext.getMainDataSourceServicesProvider();
        if (IdDtos.isTripLonglineId(nodeId)) {
            TripLonglineService service = servicesProvider.newTripLonglineService();
            position = service.moveTripLonglineToProgram(nodeId, parentNodeId);

        } else {
            TripSeineService service = servicesProvider.newTripSeineService();
            position = service.moveTripSeineToProgram(nodeId, parentNodeId);
        }

        // Close old program and open new program
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();
        if (openDataManager.isOpen(nodeId)) {
            openDataManager.closeProgram(oldParentNodeId);
            openDataManager.openProgram(parentNodeId);
        }

        return position;
    }
}
