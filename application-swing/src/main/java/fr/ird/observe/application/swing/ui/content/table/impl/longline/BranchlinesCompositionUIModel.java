package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDtos;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 12/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class BranchlinesCompositionUIModel extends ContentTableUIModel<SetLonglineGlobalCompositionDto, BranchlinesCompositionDto> {

    private static final long serialVersionUID = 1L;

    public BranchlinesCompositionUIModel(BranchlinesCompositionUI ui) {
        super(SetLonglineGlobalCompositionDto.class,
              BranchlinesCompositionDto.class,
              new String[]{
                      SetLonglineGlobalCompositionDto.PROPERTY_BRANCHLINES_COMPOSITION
              },
              new String[]{BranchlinesCompositionDto.PROPERTY_TOP_TYPE,
                           BranchlinesCompositionDto.PROPERTY_TRACELINE_TYPE,
                           BranchlinesCompositionDto.PROPERTY_LENGTH,
                           BranchlinesCompositionDto.PROPERTY_PROPORTION});

        List<ContentTableMeta<BranchlinesCompositionDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(BranchlinesCompositionDto.class, BranchlinesCompositionDto.PROPERTY_TOP_TYPE, false),
                ContentTableModel.newTableMeta(BranchlinesCompositionDto.class, BranchlinesCompositionDto.PROPERTY_TRACELINE_TYPE, false),
                ContentTableModel.newTableMeta(BranchlinesCompositionDto.class, BranchlinesCompositionDto.PROPERTY_LENGTH, false),
                ContentTableModel.newTableMeta(BranchlinesCompositionDto.class, BranchlinesCompositionDto.PROPERTY_PROPORTION, false));

        initModel(ui, metas);

    }


    @Override
    protected ContentTableModel<SetLonglineGlobalCompositionDto, BranchlinesCompositionDto> createTableModel(
            ObserveContentTableUI<SetLonglineGlobalCompositionDto, BranchlinesCompositionDto> ui,
            List<ContentTableMeta<BranchlinesCompositionDto>> contentTableMetas) {

        return new ContentTableModel<SetLonglineGlobalCompositionDto, BranchlinesCompositionDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Collection<BranchlinesCompositionDto> getChilds(SetLonglineGlobalCompositionDto bean) {
                return bean.getBranchlinesComposition();
            }

            @Override
            protected void load(BranchlinesCompositionDto source, BranchlinesCompositionDto target) {
                BranchlinesCompositionDtos.copyBranchlinesCompositionDto(source, target);
            }

            @Override
            protected void setChilds(SetLonglineGlobalCompositionDto parent, List<BranchlinesCompositionDto> childs) {
                parent.setBranchlinesComposition(childs);
            }
        };
    }
}
