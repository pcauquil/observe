package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.DataFileDto;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.SetLonglineTdrDto;
import fr.ird.observe.services.dto.longline.SetLonglineTdrDtos;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.longline.TdrService;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import jaxx.runtime.context.JAXXContextEntryDef;
import jaxx.runtime.swing.HidorButton;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JaxxFileChooser;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/30/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class TdrUIHandler extends ContentTableUIHandler<SetLonglineTdrDto, TdrDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(TdrUIHandler.class);

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    // Change toogle target visible on all TimeEditors
    protected final PropertyChangeListener onToogleTimeEditorSliderChangedListener;

    protected boolean toogleTimeEditorSliderIsChanging;

    private static final JAXXContextEntryDef<LonglinePositionHelper<TdrDto>> POSITION_HELPER_ENTRY =
            UIHelper.newContextEntryDef("TdrUI-positionHelper", LonglinePositionHelper.class);

    public TdrUIHandler(TdrUI ui) {
        super(ui, DataContextType.SetLongline);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
        onToogleTimeEditorSliderChangedListener = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onToogleTimeEditorSliderChanged(newValue);
        };
    }

    @Override
    public TdrUI getUi() {
        return (TdrUI) super.getUi();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, TdrDto bean, boolean create) {

        ContentTableModel<SetLonglineTdrDto, TdrDto> model = getTableModel();

        TdrUI ui = getUi();

        boolean withTimestamp = bean.getFishingStart() != null;
        ui.getEnableTimestamp().setSelected(withTimestamp);

        if (!model.isEditable()) {
            return;
        }

        String homeId = bean.getHomeId();

        if (log.isDebugEnabled()) {
            log.debug("selected tdr " + homeId);
        }
        JComponent requestFocus = ui.getHomeId();

        if (create) {

            // set date - time

            SetLonglineTdrDto setLongline = getBean();

            Date settingStartTimeStamp = setLongline.getSettingStartTimeStamp();
            setTimestamp(bean, settingStartTimeStamp);

            // go back to first tab
            ui.getEditTabPane().setSelectedIndex(0);

            // enable timestamps
            ui.getEnableTimestamp().setSelected(true);

        } else if (!withTimestamp) {

            // clean timestamps
            setTimestamp(bean, null);

        }

        LonglinePositionHelper<TdrDto> positionHelper = POSITION_HELPER_ENTRY.getContextValue(ui);
        positionHelper.resetPosition(bean);

        requestFocus.requestFocus();

    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(table,
                                            n("observe.content.tdr.table.homeId"),
                                            n("observe.content.tdr.table.homeId.tip"),
                                            n("observe.content.tdr.table.serialNo"),
                                            n("observe.content.tdr.table.serialNo.tip"),
                                            n("observe.content.tdr.table.sensorBrand"),
                                            n("observe.content.tdr.table.sensorBrand.tip"),
                                            n("observe.content.tdr.table.data"),
                                            n("observe.content.sensorUsed.table.data.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newStringTableCellRenderer(renderer, 10, true));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newStringTableCellRenderer(renderer, 10, true));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SensorBrandDto.class));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newBooleanTableCellRenderer(renderer));

    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @Override
    public void initUI() {

        super.initUI();

        LonglinePositionHelper<TdrDto> positionHelper = new LonglinePositionHelper<>(
                getUi().getSection(),
                getUi().getBasket(),
                getUi().getBranchline(),
                getTableEditBean());

        POSITION_HELPER_ENTRY.setContextValue(getUi(), positionHelper);

        getUi().getDeployementStart().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        getUi().getDeployementEnd().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        getUi().getFishingStart().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        getUi().getFishingEnd().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);

    }

    @Override
    public void openUI() {

        if (log.isInfoEnabled()) {
            log.info("OpenUI: " + getModel());
        }
        super.openUI();

        // Reset all sections
        LonglinePositionHelper<TdrDto> positionHelper = POSITION_HELPER_ENTRY.getContextValue(getUi());
        getUi().getSection().setData(positionHelper.getSections());

        // To be sure always remove listener (could prevent some leaks)
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        getUi().getErrorTableModel().addTableModelListener(computeTabValidStateListener);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

    }

    @Override
    protected void closeSafeUI() {
        if (log.isInfoEnabled()) {
            log.info("CloseUI: " + getModel());
        }
        super.closeSafeUI();

        // remove listener
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);

    }

    @Override
    protected void resetEditBean() {

        super.resetEditBean();
        TdrDto tableEditBean = getTableEditBean();

        boolean withTimestamp = tableEditBean.getFishingStart() != null;
        getUi().getEnableTimestamp().setSelected(withTimestamp);

    }

    public void changeEnableTimestamp(boolean enableTimestamp) {

        Date timeStamp;

        if (enableTimestamp) {

            SetLonglineTdrDto setLongline = getBean();
            timeStamp = setLongline.getSettingStartTimeStamp();

        } else {

            timeStamp = null;

        }

        TdrDto bean = getTableEditBean();
        setTimestamp(bean, timeStamp);

    }

    public void importData() {

        File file = UIHelper.chooseFile((Component) ui,
                                        t("observe.content.choose.tdr.title.importData"),
                                        t("observe.action.choose.tdr.importData"),
                                        null);

        if (file != null) {

            if (log.isInfoEnabled()) {
                log.info("Set data from file: " + file);
            }

            DataFileDto dataFileDto = UIHelper.fileToDataFileDto(file);
            getTableEditBean().setData(dataFileDto);
            getTableEditBean().setHasData(true);

        }

    }

    public void deleteData() {

        int response = UIHelper.askUser((Component) ui,
                                        t("observe.title.delete"),
                                        t("observe.content.tdr.delete.data.message"),
                                        JOptionPane.WARNING_MESSAGE,
                                        new Object[]{t("observe.choice.confirm.delete"),
                                                     t("observe.choice.cancel")},
                                        1);

        boolean doDelete = response == 0;

        if (doDelete) {

            if (log.isInfoEnabled()) {
                log.info("Delete tdr data " + getTableEditBean().getData());
            }
            getTableEditBean().setData(null);
            getTableEditBean().setHasData(false);

        }

    }

    public void exportData() {

        DataFileDto dataFile = getTableEditBean().getData();

        if (dataFile == null) {
            dataFile = getTdrService().getDataFile(getTableEditBean().getId());
        }

        File file = JaxxFileChooser
                .forSaving()
                .setParent(getUi())
                .setTitle(t("observe.content.choose.tdr.title.exportData"))
                .setApprovalText(t("observe.action.choose.tdr.exportData"))
                .setFilename(dataFile.getName())
                .setUseAcceptAllFileFilter(true)
                .choose();

        if (file != null && UIHelper.confirmOverwriteFileIfExist(getUi(), file)) {

            if (log.isInfoEnabled()) {
                log.info("save tdr data to " + file);
            }

            try {

                Files.write(file.toPath(), dataFile.getContent());

                ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
                mainUI.getStatus().setStatus(t("observe.content.sensorUsed.message.data.exported", file));
            } catch (IOException e) {
                throw new ObserveSwingTechnicalException("Could not save binary data to " + file, e);
            }

        }

    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean caracteristicsTabValid = !errorProperties.removeAll(TdrUIModel.CARACTERISTIC_TAB_PROPERTIES);
        boolean localisationTabValid = !errorProperties.removeAll(TdrUIModel.LOCALISATION_TAB_PROPERTIES);
        boolean timestampTabValid = !errorProperties.removeAll(TdrUIModel.TIMESTAMP_TAB_PROPERTIES);
        boolean keyDataTabValid = !errorProperties.removeAll(TdrUIModel.KEY_DATA_TAB_PROPERTIES);
        boolean speciesTabValid = !errorProperties.removeAll(TdrUIModel.SPECIES_TAB_PROPERTIES);

        TdrUIModel model = (TdrUIModel) getModel();
        model.setCaracteristicsTabValid(caracteristicsTabValid);
        model.setLocalisationTabValid(localisationTabValid);
        model.setTimestampTabValid(timestampTabValid);
        model.setKeyDataTabValid(keyDataTabValid);
        model.setSpeciesTabValid(speciesTabValid);

    }

    protected void onToogleTimeEditorSliderChanged(boolean newValue) {

        if (!toogleTimeEditorSliderIsChanging) {

            toogleTimeEditorSliderIsChanging = true;

            try {

                boolean selected = !newValue;

                getUi().getDeployementStart().getSliderHidor().setSelected(selected);
                getUi().getDeployementStart().getSliderHidor().setTargetVisible(newValue);

                getUi().getFishingStart().getSliderHidor().setSelected(selected);
                getUi().getFishingStart().getSliderHidor().setTargetVisible(newValue);

                getUi().getFishingEnd().getSliderHidor().setSelected(selected);
                getUi().getFishingEnd().getSliderHidor().setTargetVisible(newValue);

                getUi().getDeployementEnd().getSliderHidor().setSelected(selected);
                getUi().getDeployementEnd().getSliderHidor().setTargetVisible(newValue);

            } finally {

                toogleTimeEditorSliderIsChanging = false;

            }

        }

    }

    protected void setTimestamp(TdrDto bean, Date timestamp) {

        bean.setDeployementStart(timestamp);
        bean.setDeployementEnd(timestamp);
        bean.setFishingStart(timestamp);
        bean.setFishingEnd(timestamp);

    }

    @Override
    protected void doPersist(SetLonglineTdrDto bean) {

        SaveResultDto saveResult = getTdrService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case TdrDto.PROPERTY_SPECIES: {
                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListLonglineCatchId();

                String tripLonglineId = getDataContext().getSelectedTripLonglineId();

                TripLonglineService tripLonglineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripLonglineService();
                result = (List) tripLonglineService.getSpeciesByListAndTrip(tripLonglineId, speciesListId);

                result = ReferentialReferences.filterEnabled(result);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }

                break;
            }

        }

        return result;

    }

    @Override
    protected void loadEditBean(String beanId) {

        Form<SetLonglineTdrDto> form = getTdrService().loadForm(beanId);

        ContentTableUIModel<SetLonglineTdrDto, TdrDto> model = getModel();
        loadReferentialReferenceSetsInModel(form);
        model.setForm(form);

        LonglinePositionHelper<TdrDto> positionHelper = POSITION_HELPER_ENTRY.getContextValue(getUi());
        positionHelper.initSections(form.getObject(),
                                    form.getObject().getTdr());

        SetLonglineTdrDtos.copySetLonglineTdrDto(form.getObject(), getBean());

    }

    protected TdrService getTdrService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTdrService();
    }
}
