/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.content.impl.seine.FloatingObjectUI;
import fr.ird.observe.application.swing.ui.content.impl.seine.SetSeineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.ActivityLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.TripLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.ActivitySeineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.RouteUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.TripSeineUI;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.OpenableDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import jaxx.runtime.swing.nav.tree.AbstractNavTreeCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import javax.swing.Icon;
import javax.swing.UIManager;
import javax.swing.tree.DefaultTreeModel;
import java.awt.Color;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Le renderer abstrait (qui a toutes les methodes qui aident) pour implanter de
 * vrai renderer pour les différents cas d'utilisation de l'abre de navigation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class AbstractObserveTreeCellRenderer extends AbstractNavTreeCellRenderer<DefaultTreeModel, ObserveNode> {

    /** Logger */
    protected static final Log log =
            LogFactory.getLog(AbstractObserveTreeCellRenderer.class);

    /** la liste des ui qui peuvent être en mode création */
    private static final List<Class<?>> CREATION_UI =
            Collections.unmodifiableList(
                    Arrays.<Class<?>>asList(
                            TripSeineUI.class,
                            RouteUI.class,
                            ActivitySeineUI.class,
                            FloatingObjectUI.class,
                            SetSeineUI.class,
                            TripLonglineUI.class,
                            ActivityLonglineUI.class)
            );

    private static final long serialVersionUID = 1L;

    /** la couleur des noeuds fermés */
    private Color closeColor;

    /** la couleur des noeuds de données ouverts */
    private Color openColor;

    /** la couleur pour indiquer que la données est non valide ou non sauvée */
    private Color redColor;

    /** service de décoration */
    private transient DecoratorService decoratorService;

    public DecoratorService getDecoratorService() {
        if (decoratorService == null) {

            // on récupère le service commun
            decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        }
        return decoratorService;
    }

    @Override
    public ObserveDataProvider getDataProvider() {
        ObserveDataProvider provider = (ObserveDataProvider) super.getDataProvider();
//        Objects.requireNonNull(provider);
//        //FIXME ? why
        if (provider == null) {
            ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
            if (source != null) {
                provider = new ObserveDataProvider(source);
                setDataProvider(provider);
            }
        }
        return provider;
    }

    AbstractObserveTreeCellRenderer() {
        init();
    }

    protected void init() {
        closeColor = Color.LIGHT_GRAY;
        openColor = getForeground();
        redColor = Color.RED;
    }

    /**
     * Positionne dans le renderer delege l'icone a utiliser.
     *
     * @param node le noeud passé au renderer
     */
    public void setIcon(ObserveNode node) {
        Icon icon = getNavigationIcon(node);
        setDefaultIcons(icon);
    }

    /**
     * Récupère l'icon adequate pour le noeud donnes.
     *
     * @param node le noeud passé au renderer
     * @return l'icone du noeud (ou null si non trouve)
     */
    public Icon getNavigationIcon(ObserveNode node) {
        return getNavigationIcon(node, null);

    }

    @Override
    public Color getBackgroundSelectionColor() {
        return null;
    }

    @Override
    public Color getBackgroundNonSelectionColor() {
        // Fixes  http://forge.codelutin.com/issues/830 for jdk 7
        return Color.WHITE;
    }

    /**
     * Récupère l'icon adequate pour le noeud donnes.
     *
     * @param node   le noeud passé au renderer
     * @param suffix un suffix a ajouter a la clef qui identifie l'icone
     * @return l'icone du noeud (ou null si non trouve)
     */
    public Icon getNavigationIcon(ObserveNode node, String suffix) {
        if (node == null || node.isRoot()) {
            return null;
        }
        if (suffix == null) {
            suffix = "";
        }

        Icon icon;

        if (node.isReferentielNode()) {

            // referentiel root node

            if (node.isStringNode()) {
                icon = UIManager.getIcon("navigation.referentiel" + suffix);
            } else {
                icon = UIManager.getIcon("navigation.sub.referentiel" + suffix);
            }
            return icon;
        }

        if (!node.isStringNode()) {

            // on est sur un noeud de donnees, rien a calculer
            String path = "navigation." + node.getInternalClass().getName() + suffix;
            icon = UIManager.getIcon(path);
            return icon;
        }

        // dernier cas, on est sur un noeud intermediaire sans donnee
        // on doit se baser sur le container node

        ObserveNode containerNode = node.getContainerNode();

        Class<?> containerClass = containerNode.getInternalClass();
        String path;
        if (TripSeineDto.class.equals(containerClass)) {
            // remonte sur une maree : donc routes
            path = RouteDto.class.getName();
        } else if (TripLonglineDto.class.equals(containerClass)) {
            // remonte sur une maree : donc activités
            path = ActivityLonglineDto.class.getName();
        } else if (RouteDto.class.equals(containerClass)) {
            // remonte sur une route : donc activitys
            path = ActivitySeineDto.class.getName();
        }
//        else if (ActivitySeine.class.equals(containerClass)) {
//            // remonte sur une activity : donc observedSystem
//            path = ObservedSystemDto.class.getName();
//        }
        else {
            // dans le cas d'un sub, il n'y a pas de suffixe possible
            suffix = "";
            path = containerClass.getName() + ".sub";
        }
        icon = UIManager.getIcon("navigation." + path + suffix);
        return icon;
    }

    private void setDefaultIcons(Icon icon) {
        if (icon == null) {
            // the icon is not customized for this node
            setOpenIcon(getDefaultOpenIcon());
            setClosedIcon(getDefaultClosedIcon());
            setLeafIcon(getDefaultLeafIcon());
        } else {
            // replace all possible icons for this node
            setOpenIcon(icon);
            setClosedIcon(icon);
            setLeafIcon(icon);
        }
    }

    @Override
    protected String computeNodeText(ObserveNode node) {

        if (node.isStringNode()) {
            return t(node.getId());
        }

        Class<?> beanType = node.getInternalClass();

        if (node.isReferentielNode()) {

            return t(ObserveI18nDecoratorHelper.getTypeI18nKey(beanType));
        }

        // noeud de donnée

        String id = node.getId();

        if (id == null) {

            // noeud d'un objet en cours de création

            return t(node.getContext());
        }

        // noeud de donnée connue en base

        if (getDataProvider() == null) {
            // data provider non utilisable
            return "No data provider opened to render " + id;
        }

        if (node instanceof SetLonglineNode) {
            return t(ObserveI18nDecoratorHelper.getTypeI18nKey(((SetLonglineNode) node).getEntity().getType()));
//            return t("observe.tree.setLongline");
        }

        if (node instanceof SetSeineNode) {
            return t(ObserveI18nDecoratorHelper.getTypeI18nKey(((SetSeineNode) node).getEntity().getType()));
//            return t("observe.tree.setSeine");
        }

        if (node instanceof DataReferenceNodeSupport) {
            DataReferenceNodeSupport<?> entityNodeSupport = (DataReferenceNodeSupport) node;

            DataReference entity = entityNodeSupport.getEntity();

            Objects.requireNonNull(entity, "L'entité doit être chargée dans le noeud: " + node);

            Decorator<?> decorator = getDecoratorService().getDataReferenceDecorator((Class) beanType);

            return decorator.toString(entity);
        }

        if (node instanceof ReferentialReferenceNodeSupport) {
            ReferentialReferenceNodeSupport<?> entityNodeSupport = (ReferentialReferenceNodeSupport) node;

            ReferentialReference entity = entityNodeSupport.getEntity();

            Objects.requireNonNull(entity, "L'entité doit être chargée dans le noeud: " + node);

            Decorator<?> decorator = getDecoratorService().getReferentialReferenceDecorator((Class) beanType);

            return decorator.toString(entity);
        }

        throw new IllegalStateException("Don't know how to render node: " + node);

    }

    public Color getNavigationTextColor(ObserveNode node) {

        if (node == null) {
            return openColor;
        }

        if (node.isRoot() || node.isReferentielNode()) {
            return openColor;
        }

        // le noeud pointe sur un objet ouvert

        Class<?> contentClass = node.getInternalClass();
        ObserveNode parentNode = node.getParent();
        Class<?> parentContentClass = parentNode == null ? null : parentNode.getInternalClass();

        if (log.isTraceEnabled()) {
            log.trace("[" + node + "] Content class to use : " + contentClass);
            log.trace("[" + node + "] Parent Content class to use : " + parentContentClass);
        }
        DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
        String nodeId = node.getId();

        if (ProgramDto.class.equals(contentClass)) {

            // on grise le program si aucune marée ouverte sur ce programme
            String p = dataContext.getOpenProgramId();
            if (p != null && p.equals(nodeId)) {
                return openColor;
            }
            return closeColor;
        }

        boolean nodeIsOpen = isOpenNode(node);

        if (!nodeIsOpen) {
            if (log.isDebugEnabled()) {
                log.debug("[" + node + "] is not open, use closeColor");
            }
            // l'objet pointé n'est pas ouvert
            return closeColor;
        }

        if (CREATION_UI.contains(contentClass)) {

            // un noeud de creation
            if (nodeId == null) {
                // mode creation
                return redColor;
            }
            return openColor;
        }

        return openColor;
    }

    private boolean isOpenNode(ObserveNode node) {

        if (node.isRoot() || node.isReferentielNode()) {
            return false;
        }

        boolean nodeIsOpen = false;

        ObserveNode openableNode;
        openableNode = getFirstOpenableNode(node);
        if (log.isDebugEnabled()) {
            log.debug("openable node " +
                      Arrays.toString(openableNode.getPath()) + " for " +
                      Arrays.toString(node.getPath()));
        }
        if (openableNode != null) {
            Boolean open = openableNode.isOpen();
            nodeIsOpen = open != null && open;
        }

        return nodeIsOpen;
    }

    /**
     * @param value the value which should be a node
     * @return the cast {@link ObserveNode}, or {@code null} if
     * value is null.
     */
    static ObserveNode getNode(Object value) {
        ObserveNode node = null;
        if (value instanceof ObserveNode) {
            node = (ObserveNode) value;
        }
        return node;
    }

    private ObserveNode getFirstOpenableNode(ObserveNode node) {

        if (OpenableDto.class.isAssignableFrom(node.getInternalClass())) {
            return node;
        }

        if (ProgramDto.class.isAssignableFrom(node.getInternalClass())) {
            return node;
        }

        // this is not a openable node, go to parent node
        ObserveNode parentNode = node.getParent();
        if (parentNode != null) {
            return getFirstOpenableNode(parentNode);
        }
        return null;
    }

}
