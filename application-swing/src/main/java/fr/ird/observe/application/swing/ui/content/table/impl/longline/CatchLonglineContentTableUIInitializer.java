package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIInitializer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.bean.JavaBeanObjectUtil;

import javax.swing.JCheckBox;

/**
 * Created on 3/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class CatchLonglineContentTableUIInitializer extends ContentTableUIInitializer<SetLonglineCatchDto, CatchLonglineDto, CatchLonglineUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CatchLonglineContentTableUIInitializer.class);

    public CatchLonglineContentTableUIInitializer(CatchLonglineUI ui) {
        super(ui);
    }

    @Override
    protected void init(JCheckBox editor) {
        if (log.isDebugEnabled()) {
            log.debug("init simple boolean editor " + editor.getName());
        }
        super.init(editor);
        final String propertyName = (String) editor.getClientProperty("branchlineBeanPropertyName");
        if (propertyName != null) {
            editor.addItemListener(event -> {
                Boolean newValue = ((JCheckBox) event.getSource()).isSelected();
                JavaBeanObjectUtil.setProperty(ui.getBranchlineBean(), propertyName, newValue);
            });
        }
    }

}
