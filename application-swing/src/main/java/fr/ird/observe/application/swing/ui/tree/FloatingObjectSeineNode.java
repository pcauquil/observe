package fr.ird.observe.application.swing.ui.tree;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.service.seine.FloatingObjectService;
import fr.ird.observe.application.swing.ui.tree.loadors.FloatingObjectNodeChildLoador;

/**
 * Created on 4/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class FloatingObjectSeineNode extends DataReferenceNodeSupport<FloatingObjectDto> {

    private static final long serialVersionUID = 1L;

    public FloatingObjectSeineNode(DataReference<FloatingObjectDto> entity) {
        super(FloatingObjectDto.class, entity, ObserveTreeHelper.getChildLoador(FloatingObjectNodeChildLoador.class));
    }

    @Override
    protected DataReference<FloatingObjectDto> fetchEntity() {
        FloatingObjectService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newFloatingObjectService();
        return service.loadReferenceToRead(getId());
    }

}
