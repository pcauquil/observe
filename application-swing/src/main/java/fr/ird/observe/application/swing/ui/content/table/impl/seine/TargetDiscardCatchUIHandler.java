/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDtos;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDtos;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDtos;
import fr.ird.observe.services.service.seine.TargetCatchService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import jaxx.runtime.swing.editor.bean.BeanComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TargetDiscardCatchUIHandler extends ContentTableUIHandler<SetSeineTargetCatchDto, TargetCatchDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(TargetDiscardCatchUIHandler.class);

    public TargetDiscardCatchUIHandler(TargetDiscardCatchUI ui) {
        super(ui, DataContextType.SetSeine);
    }

    protected static Collection<ReferentialReference<WeightCategoryDto>> getWeightCategoryUsed(
            ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model) {

        List<ReferentialReference<WeightCategoryDto>> list = model.getColumnValues(1);
        Collection<ReferentialReference<WeightCategoryDto>> set = Sets.newHashSet(list);
        list.clear();
        return set;
    }

    protected static Collection<ReferentialReference<ReasonForDiscardDto>> getReasonForDiscardUsed(
            ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model,
            ReferentialReference<WeightCategoryDto> weightCategory) {

        Collection<ReferentialReference<ReasonForDiscardDto>> set = Sets.newHashSet();
        if (weightCategory != null) {
            for (int i = 0; i < model.getRowCount(); i++) {
                TargetCatchDto c = model.getValueAt(i);
                if (c != null && weightCategory.equals(c.getWeightCategory()) && c.getReasonForDiscard() != null) {
                    set.add(c.getReasonForDiscard());
                }
            }
        }
        return set;
    }

    @Override
    public TargetDiscardCatchUI getUi() {
        return (TargetDiscardCatchUI) super.getUi();
    }

    @Override
    public void initUI() {

        super.initUI();

        TargetDiscardCatchUI ui = getUi();

        // lors de la modification d'une species (sur une entree non sauvee)
        //   - on recalcule la liste des categories pour cette species.
        //   - on reinitialiser la categorie selectionnee

        ui.getSpecies().addPropertyChangeListener(
                BeanComboBox.PROPERTY_SELECTED_ITEM,
                evt -> {

                    ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model = getTableModel();

                    if (model.isNewRow()) {

                        // en mode creation , on doit recalculer la liste des categories
                        ReferentialReference<SpeciesDto> species = (ReferentialReference<SpeciesDto>) evt.getNewValue();
                        onSpeciesChanged(model.getRowBean(), species);

                    }

                }
        );

        // lors de la modification d'une catégorie (sur une entree non sauvee)
        //   - on recalcule la liste des raison rejets.
        //   - on reinitialiser la raison rejet selectionnee

        ui.getWeightCategory().addPropertyChangeListener(
                BeanComboBox.PROPERTY_SELECTED_ITEM,
                evt -> {

                    ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model = getTableModel();
                    if (model.isNewRow()) {

                        // en mode creation , on doit recalculer la liste des raison rejet
                        ReferentialReference<WeightCategoryDto> weightCategory =
                                (ReferentialReference<WeightCategoryDto>) evt.getNewValue();
                        onWeightCategoryChanged(model.getRowBean(), weightCategory);

                    }

                }
        );

    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @SuppressWarnings("Duplicates")
    @Override
    protected boolean prepareSave(SetSeineTargetCatchDto bean, List<TargetCatchDto> objets) {

        //on cherche si parmis les captures supprimer certaines ont des échantillons
        SetSeineTargetCatchDto originalSetSeineTargetCatchDto = getModel().getForm().getObject();
        SetSeineTargetCatchDto currentSetSeineTargetCatch = getModel().getBean();

        Set<ReferentialReference<SpeciesDto>> speciesToDelete = Sets.newHashSet();

        for (TargetCatchDto targetCatchDto : originalSetSeineTargetCatchDto.getTargetCatch()) {

            if (targetCatchDto.isHasSample()) {

                ReferentialReference<SpeciesDto> species = targetCatchDto.getSpecies();

                Iterable<TargetCatchDto> currentTargetCatchSpecies = TargetCatchDtos.filterBySpecies(currentSetSeineTargetCatch.getTargetCatch(), species);

                if (Iterables.isEmpty(currentTargetCatchSpecies)) {

                    speciesToDelete.add(species);

                }
            }

        }

        if (!speciesToDelete.isEmpty()) {
            // il existe des echantillon thon a supprimer on demande une confirmation
            StringBuilder sb = new StringBuilder(512);
            Decorator<ReferentialReference<SpeciesDto>> decorator =
                    getReferentialReferenceDecorator(SpeciesDto.class);

            for (ReferentialReference<SpeciesDto> species : speciesToDelete) {
                sb.append("  - ").append(decorator.toString(species)).append('\n');
            }
            int reponse = UIHelper.askUser(
                    t("observe.title.need.confirm"),
                    t("observe.content.targetDiscarded.message.table.will.delete.targetLength", sb.toString()),
                    JOptionPane.ERROR_MESSAGE,
                    new Object[]{
                            t("observe.choice.continue"),
                            t("observe.choice.cancel")},
                    0);
            if (log.isDebugEnabled()) {
                log.debug("response : " + reponse);
            }
            boolean canContinue = false;
            switch (reponse) {
                case 0:
                    // wil reset ui
                    canContinue = true;
                    break;
            }

            if (!canContinue) {

                // l'utilisateur a choisi de ne pas continuer
                return false;
            }
        }

        return true;
    }

    @SuppressWarnings("Duplicates")
    @Override
    protected void onSelectedRowChanged(int editingRow, TargetCatchDto bean, boolean create) {
        TargetDiscardCatchUI ui = getUi();

        ReferentialReference<WeightCategoryDto> weightCategory = bean.getWeightCategory();

        ReferentialReference<SpeciesDto> species = null;
        if (weightCategory != null) {
            species = getWeightCategorySpecies(weightCategory).orElse(null);
        }

        ReferentialReference<ReasonForDiscardDto> reasonForDiscard = bean.getReasonForDiscard();
        if (log.isDebugEnabled()) {
            log.debug("selected categoriePoid " + weightCategory);
            log.debug("selected species " + species);
            log.debug("selected reasonForDiscard " + reasonForDiscard);
        }

        JComponent requestFocus;
        if (create) {
            ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model = getTableModel();

            if (model.isCreate()) {
                // par défaut, on considère que l'espèce a été montée sur le pont
                // c'est le cas le plus fréquent.
                bean.setBroughtOnDeck(true);
            }

            // on recalcule la liste des speciess disponibles
            List<ReferentialReference<SpeciesDto>> availableSpecies = buildSpeciesList(species, weightCategory);
            ui.getSpecies().setData(availableSpecies);

            // on reinitilise toujours l'species (pour reinitialiser la liste des categories)
            ui.getSpecies().setSelectedItem(null);

            if (!model.isCreate()) {
                // on repositionne l'espèce (cela reconstruira la liste des categories)
                ui.getSpecies().setSelectedItem(species);
                // on repositionne la categorie
                ui.getWeightCategory().setSelectedItem(weightCategory);
                ui.getReasonForDiscard().setSelectedItem(reasonForDiscard);
            }

            requestFocus = ui.getSpecies();
        } else {
            // en mode mise a jour, on restreint la liste des categories
            // au singleton de sa valeur correspondante dans le bean
            // puisque dans ce mode, pas possibilite de modifier de cette
            // valeur (clef metier)
            ui.getSpecies().setSelectedItem(species);
            ui.getWeightCategory().setData(Collections.singletonList(weightCategory));
            ui.getWeightCategory().setSelectedItem(weightCategory);
            ui.getReasonForDiscard().setData(Collections.singletonList(reasonForDiscard));
            ui.getReasonForDiscard().setSelectedItem(reasonForDiscard);

            requestFocus = ui.getCatchWeight();
        }

        requestFocus.requestFocus();
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {
        JTable table = getUi().getTable();
        UIHelper.fixTableColumnWidth(table, 3, 75);
        UIHelper.fixTableColumnWidth(table, 4, 20);
        UIHelper.fixTableColumnWidth(table, 5, 50);

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.targetDiscarded.table.speciesThon"),
                n("observe.content.targetDiscarded.table.speciesThon.tip"),
                n("observe.content.targetDiscarded.table.weightCategory"),
                n("observe.content.targetDiscarded.table.weightCategory.tip"),
                n("observe.content.targetDiscarded.table.reasonForDiscard"),
                n("observe.content.targetDiscarded.table.reasonForDiscard.tip"),
                n("observe.content.targetDiscarded.table.weight"),
                n("observe.content.targetDiscarded.table.weight.tip"),
                n("observe.content.targetDiscarded.table.broughtOnDeck"),
                n("observe.content.targetDiscarded.table.broughtOnDeck.tip"),
                n("observe.content.targetDiscarded.table.comment"),
                n("observe.content.targetDiscarded.table.comment.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, WeightCategoryDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, ReasonForDiscardDto.class));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newStringTableCellRenderer(renderer, 20, false));
        UIHelper.setTableColumnRenderer(table, 5, UIHelper.newStringTableCellRenderer(renderer, 10, true));
    }

    protected void onSpeciesChanged(TargetCatchDto bean, ReferentialReference<SpeciesDto> species) {

        ReferentialReference<WeightCategoryDto> weightCategory = null;

        List<ReferentialReference<WeightCategoryDto>> data;

        if (species == null) {

            // aucune espèce selectionnee, on vide simplement la liste des categories
            // car il faut d'abord choisir une espèce puis une categorie
            data = Collections.emptyList();

        } else {

            // un espèce est selectionnee, on met a jour la liste des categories disponibles pour cette espèce
            List<ReferentialReference<WeightCategoryDto>> availableCategories = buildWeightCategoryList(species);

            data = availableCategories;

            weightCategory = bean.getWeightCategory();

            if (weightCategory != null && !availableCategories.contains(weightCategory)) {
                weightCategory = null;
            }
        }

        // on met a jour le model de la liste des categories
        BeanComboBox<ReferentialReference<WeightCategoryDto>> combo = getUi().getWeightCategory();
        combo.setData(data);

        // on selectionne la categorie retenue
        combo.setSelectedItem(weightCategory);
    }

    protected void onWeightCategoryChanged(TargetCatchDto bean, ReferentialReference<WeightCategoryDto> weightCategory) {
        TargetDiscardCatchUI ui = getUi();

        ReferentialReference<ReasonForDiscardDto> reasonForDiscard = null;
        List<ReferentialReference<ReasonForDiscardDto>> data;

        if (weightCategory == null) {
            // aucune categorie selectionnee, on vide simplement
            // la liste des raison rejets car il faut d'abord choisir une
            // espèce puis une categorie, puis une raison rejet
            data = Collections.emptyList();

        } else {

            // un categorie est selectionne, on met a jour la liste des raisons
            // rejet disponible pour cette categorie

            reasonForDiscard = bean.getReasonForDiscard();

            List<ReferentialReference<ReasonForDiscardDto>> reasonForDiscardList = buildReasonFordiscardList(weightCategory, reasonForDiscard);

            if (log.isDebugEnabled()) {
                log.debug("new reasonForDiscard list " + reasonForDiscardList);
            }

            data = reasonForDiscardList;

            if (reasonForDiscard != null && !reasonForDiscardList.contains(reasonForDiscard)) {
                // on sélectionne la première raison rejet disponible
                reasonForDiscard = null;
            }
        }

        BeanComboBox<ReferentialReference<ReasonForDiscardDto>> combo = ui.getReasonForDiscard();

        // on met a jour le model de la liste des raison rejets
        combo.setData(data);

        // on selectionne la raison rejet retenue
        combo.setSelectedItem(reasonForDiscard);
    }

    protected List<ReferentialReference<SpeciesDto>> buildSpeciesList(ReferentialReference<SpeciesDto> species,
                                                                      ReferentialReference<WeightCategoryDto> weightCategory) {

        List<ReferentialReference<WeightCategoryDto>> weightCategoryList = getAllWeightCategories();

        removeFullyUsedWeightCategories(weightCategoryList);

        if (weightCategory != null) {
            weightCategoryList.add(weightCategory);
        }

        Collection<ReferentialReference<SpeciesDto>> usedSpecies = Sets.newHashSet();
        for (ReferentialReference<WeightCategoryDto> cat : weightCategoryList) {

            Optional<ReferentialReference<SpeciesDto>> optionalSpecies = getWeightCategorySpecies(cat);

            if (optionalSpecies.isPresent()) {
                usedSpecies.add(optionalSpecies.get());
            }

        }

        List<ReferentialReference<SpeciesDto>> result = Lists.newArrayList(usedSpecies);
        weightCategoryList.clear();
        usedSpecies.clear();
        return result;

    }

    protected List<ReferentialReference<WeightCategoryDto>> buildWeightCategoryList(ReferentialReference<SpeciesDto> species) {

        Set<ReferentialReference<WeightCategoryDto>> allCategories = getModel().getReferentialReferences(TargetCatchDto.PROPERTY_WEIGHT_CATEGORY);
        List<ReferentialReference<WeightCategoryDto>> weightCategoryList = WeightCategoryDtos.filterSpeciesWeightCategories(allCategories, species.getId());

        // on filtre toutes les categories qui ont deja references sur toutes les raisons de rejet
        removeFullyUsedWeightCategories(weightCategoryList);

        return weightCategoryList;
    }

    @SuppressWarnings("unchecked")
    protected List<ReferentialReference<ReasonForDiscardDto>> buildReasonFordiscardList(ReferentialReference<WeightCategoryDto> weightCategory,
                                                                                        ReferentialReference<ReasonForDiscardDto> reasonForDiscard) {

        ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model = getTableModel();
        Set<ReferentialReference<ReasonForDiscardDto>> reasonForDiscardSet = getModel().getReferentialReferences(TargetCatchDto.PROPERTY_REASON_FOR_DISCARD);

        List<ReferentialReference<ReasonForDiscardDto>> reasonForDiscardList = Lists.newArrayList(reasonForDiscardSet);

        // on filtre les raison rejet sur la categorie
        for (int i = 0; i < model.getRowCount(); i++) {
            TargetCatchDto c = model.getValueAt(i);

            if (model.getSelectedRow() != i
                    && c != null
                    && c.getReasonForDiscard() != null
                    && (reasonForDiscard == null || !reasonForDiscard.equals(c.getReasonForDiscard()))
                    && weightCategory.equals(c.getWeightCategory())) {
                reasonForDiscardList.remove(c.getReasonForDiscard());
            }
        }

        return reasonForDiscardList;
    }

    protected List<ReferentialReference<WeightCategoryDto>> getAllWeightCategories() {

        Set<ReferentialReference<WeightCategoryDto>> weightCategorySet = getModel().getReferentialReferences(TargetCatchDto.PROPERTY_WEIGHT_CATEGORY);
        return Lists.newArrayList(weightCategorySet);

    }

    protected void removeFullyUsedWeightCategories(List<ReferentialReference<WeightCategoryDto>> weightCategoryAvaillables) {

        ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model = getTableModel();

        Set<ReferentialReference<ReasonForDiscardDto>> reasonForDiscardSet = getModel().getReferentialReferences(TargetCatchDto.PROPERTY_REASON_FOR_DISCARD);

        int nbReasonForDiscard = reasonForDiscardSet.size();

        // on filtre toutes les weightCategory qui ont deja references toutes les raisons rejets
        for (ReferentialReference<WeightCategoryDto> c : getWeightCategoryUsed(model)) {
            Collection<ReferentialReference<ReasonForDiscardDto>> raisonUsed = getReasonForDiscardUsed(model, c);
            if (raisonUsed.size() == nbReasonForDiscard) {
                // toutes les raison de rejet epuisé pour cette categorie
                weightCategoryAvaillables.remove(c);
            }
        }
    }

    @Override
    protected void doPersist(SetSeineTargetCatchDto bean) {

        SaveResultDto saveResult = getTargetCatchService().save(bean);
        saveResult.toDto(bean);

    }


    @Override
    protected void loadEditBean(String beanId) {
        Form<SetSeineTargetCatchDto> form = getTargetCatchService().loadForm(beanId, true);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        SetSeineTargetCatchDtos.copySetSeineTargetCatchDto(form.getObject(), getBean());
    }


    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case ObjectSchoolEstimateDto.PROPERTY_SPECIES: {

                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListSeineTargetCatchId();
                String tripSeineId = getDataContext().getSelectedTripSeineId();

                TripSeineService tripSeineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
                result = (List) tripSeineService.getSpeciesByListAndTrip(tripSeineId, speciesListId);

                result = ReferentialReferences.filterEnabled(result);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected TargetCatchService getTargetCatchService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTargetCatchService();
    }
}
