/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.resume;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ShowResumeUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(ShowResumeUIHandler.class);

    public ShowResumeUIHandler(ShowResumeUI ui) {
        super(ui);
    }

    @Override
    public ShowResumeUI getUi() {
        return (ShowResumeUI) super.getUi();
    }

    public void initTabUI(AdminUI ui, ShowResumeUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        UIHelper.setLayerUI(tabUI.getContent(), null);
    }


    public void updateText() {
        StringBuilder buffer = new StringBuilder();
        if (!model.isWasStarted()) {
            buffer.append(t("observe.admin.resume.no.operation.done"));
        } else {
            for (AdminStep s : model.getOperations()) {
                WizardState state = model.getStepState(s);
                String stateStr = "";
                switch (state) {
                    case CANCELED:
                        stateStr = t("observe.admin.resume.operation.canceled");
                        break;
                    case FAILED:
                        stateStr = t("observe.admin.resume.operation.failed");
                        break;
                    case NEED_FIX:
                        stateStr = t("observe.admin.resume.operation.need.fix");
                        break;
                    case PENDING:
                        stateStr = t("observe.admin.resume.operation.not.started");
                        break;
                    case RUNNING:
                        stateStr = t("observe.admin.resume.operation.running");
                        break;
                    case SUCCESSED:
                        stateStr = t("observe.admin.resume.operation.done");
                        break;
                }
                buffer.append("\n\n");
                buffer.append(I18nEnumHelper.getLabel(s));
                buffer.append(" : ");
                buffer.append(stateStr);
                buffer.append(".");
            }
            buffer.append("\n\n");
        }
        getUi().getResume().setText(buffer.toString());
    }

    public void addMessage(AdminStep step, String text) {
        getUi().getGlobalProgression().append(I18nEnumHelper.getLabel(step) + " - " + text + "\n");
    }
}
