/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import java.awt.Color;
import java.awt.Component;

/**
 * Un renderer pour modifier l'apparence des noeuds de l'arbre de navigation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class NavigationTreeCellRenderer extends AbstractObserveTreeCellRenderer {

    private static final long serialVersionUID = 1L;

    public NavigationTreeCellRenderer() {
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean sel,
                                                  boolean expanded,
                                                  boolean leaf,
                                                  int row,
                                                  boolean hasFocus) {

        if (!(tree.getModel() instanceof DefaultTreeModel)) {
            Component rendererComponent;
            rendererComponent = super.getTreeCellRendererComponent(
                    tree,
                    value,
                    sel,
                    expanded,
                    leaf,
                    row,
                    hasFocus
            );
            return rendererComponent;
        }

        // get the icon to set for the node
        ObserveNode node = getNode(value);

        setIcon(node);

        if (!sel) {

            Color color = getNavigationTextColor(node);
            if (log.isTraceEnabled()) {
                log.trace("===" + color + " for node " +
                          node.getInternalClass() + " - " + node.getId());
            }
            setTextNonSelectionColor(color);
        }

        String text = getNodeText(node);
        if (log.isTraceEnabled()) {
            log.trace("===" + text + " for node " +
                      node.getInternalClass() + " - " + node.getId());
        }
        Component comp = super.getTreeCellRendererComponent(
                tree,
                text,
                sel,
                expanded,
                leaf,
                row,
                hasFocus
        );
        ((JComponent) comp).setToolTipText(text);
        return comp;
    }

}
