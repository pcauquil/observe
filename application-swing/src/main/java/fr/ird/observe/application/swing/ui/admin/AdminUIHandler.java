/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveActionExecutor;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.resume.ShowResumeUI;
import fr.ird.observe.application.swing.ui.admin.save.SaveLocalUI;
import jaxx.runtime.swing.wizard.WizardUILancher;
import jaxx.runtime.swing.wizard.ext.WizardExtUtil;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.border.TitledBorder;
import java.awt.Component;
import java.util.Arrays;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur de l'ui des actions d'administration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class AdminUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(AdminUIHandler.class);

    public AdminStep getSelectedStep(AdminUI ui) {
        int index = ui.getTabs().getSelectedIndex();
        AdminTabUI c = null;
        if (index > -1) {
            c = (AdminTabUI) ui.getTabs().getComponentAt(index);
        }
        return c == null ? null : c.getStep();
    }

    public AdminTabUI getStepUI(AdminUI ui, AdminStep step) {
        if (step != null) {
            return (AdminTabUI) ui.getObjectById(step.name());
        }
        return null;
    }

    public AdminTabUI getStepUI(AdminUI ui, int stepIndex) {
        if (stepIndex > ui.getTabs().getTabCount()) {
            return null;
        }
        return (AdminTabUI) ui.getTabs().getComponentAt(stepIndex);
    }

    public AdminTabUI getSelectedStepUI(AdminUI ui) {
        AdminStep step = getSelectedStep(ui);
        return getStepUI(ui, step);
    }

    public void blockOperations(AdminUI ui) {

        ui.getModel().setValueAdjusting(true);
        for (AdminStep op : AdminStep.values()) {
            ConfigUI configUI = (ConfigUI)
                    ui.getStepUI(AdminStep.CONFIG);
            JCheckBox comp = (JCheckBox) configUI.getObjectById(op.name());
            if (comp == null) {
                continue;
            }
            if (!ui.getModel().getOperations().contains(op)) {
                comp.setVisible(false);
            }
        }
        ui.operationBlockLayerUI.setBlock(true);
        ui.getModel().setValueAdjusting(false);
    }

    public void onWasInit(AdminUI ui) {
        if (log.isDebugEnabled()) {
            log.debug(ui.getName() + " model was init at " + new Date());
        }
    }

    public void onWasStarted(AdminUI ui) {
        if (log.isDebugEnabled()) {
            log.debug(ui.getName() + " model was started at " + new Date());
        }
    }

    public void onStepsChanged(AdminUI ui, AdminStep[] steps) {

        if (log.isDebugEnabled()) {
            log.debug("Will use these steps : " + Arrays.toString(steps));
        }

        ui.getModel().setValueAdjusting(true);


        // on supprime tous les onglets
        while (ui.tabs.getTabCount() > 1) {

            int index = ui.tabs.getTabCount() - 1;
            if (log.isDebugEnabled()) {
                log.debug("remove step : " + index);
            }

            ui.tabs.remove(index);
        }

        // on recree les onglets
        for (int i = 0, max = steps.length; i < max; i++) {
            AdminStep step = steps[i];
            AdminTabUI c = ui.getStepUI(step);
            if (log.isDebugEnabled()) {
                log.debug("StepUI [" + step + "] : " + c);
            }
            if (c == null) {

                if (log.isDebugEnabled()) {
                    log.debug("Will init step [" + step + "]");
                }

                c = step.newUI(ui);

                if (log.isDebugEnabled()) {
                    log.debug("Created tabUI [" + c.getName() + "]");
                }

                ui.get$objectMap().put(step.name(), c);

                if (log.isDebugEnabled()) {
                    log.debug("Will init tabUI [" + c.getName() + "]");
                }
                c.initUI(ui);

                JComponent content = c.getContent();
                UIHelper.setLayerUI(content, ui.getBusyBlockLayerUI());
            }

            String title = I18nEnumHelper.getLabel(step);
            String tip = I18nEnumHelper.getDescription(step);
            ui.tabs.addTab(title, null, c, tip);
            c.setEnabled(i == 0);
        }

        ui.getModel().setValueAdjusting(false);
    }

    public void onStepChanged(AdminUI ui, AdminStep oldStep, AdminStep newStep) {
        if (log.isTraceEnabled()) {
            log.trace(newStep);
        }
        AdminTabUI c = ui.getStepUI(newStep);

        AdminUIModel model = ui.getModel();

        if (oldStep == null
                && newStep == AdminStep.CONFIG
                && (model.containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE) || model.containsOperation(AdminStep.DATA_SYNCHRONIZE))) {

            ConfigUI configUI = (ConfigUI) ui.getStepUI(AdminStep.CONFIG);

            model.setLocalSourceLabel(t("observe.storage.config.left.storage"));
            model.setCentralSourceLabel(t("observe.storage.config.right.storage"));

            configUI.getLocalSourceConfig().setBorder(new TitledBorder(model.getLocalSourceLabel()));
            configUI.getCentralSourceConfig().setBorder(new TitledBorder(model.getCentralSourceLabel()));

        }

        if (c != null && c.getStep() == AdminStep.SAVE_LOCAL) {

            // on met a jour le descriptif
            SaveLocalUI saveUI = (SaveLocalUI) ui.getStepUI(AdminStep.SAVE_LOCAL);
            saveUI.getHandler().updateText();
        }
        if (c != null && c.getStep() == AdminStep.SHOW_RESUME) {

            // on met a jour le resumé des opérations
            ShowResumeUI showResumeUI = (ShowResumeUI) ui.getStepUI(AdminStep.SHOW_RESUME);
            showResumeUI.getHandler().updateText();
        }

        // selection du nouvel onglet
        int index = ui.tabs.indexOfComponent(c);
        if (index > -1 && ui.tabs.getSelectedIndex() != index) {
            ui.tabs.setSelectedIndex(index);
        }
    }

    /**
     * Call back lorsque l'état du modèle a changé.
     *
     * Ici, on va rendre accessible (ou pas) les onglets selon l'état du modèle.
     *
     * @param ui       l'ui concernée
     * @param newState le nouvel état.
     */
    public void onModelStateChanged(AdminUI ui, WizardState newState) {
        if (log.isDebugEnabled()) {
            log.debug(newState);
        }

        AdminTabUI selected = ui.getSelectedStepUI();
        boolean busy = WizardExtUtil.acceptStates(newState, WizardState.RUNNING);
        UIHelper.TabbedPaneIterator<Component> itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        if (busy) {
            if (log.isDebugEnabled()) {
                log.debug("Busy state, can only access selected step : " + selected);
            }
            // seul l'onglet courant est accessible
            for (; itr.hasNext(); ) {
                Component tab = itr.next();
                ui.tabs.setEnabledAt(itr.getIndex() - 1, tab.equals(selected));
            }
            return;
        }

        if (selected != null && selected.getStep().isConfig()) {

            if (log.isDebugEnabled()) {
                log.debug("Selected step is config : " + selected.getStep());
            }
            for (; itr.hasNext(); ) {
                int index = itr.getIndex();
                Component tab = itr.next();
                AdminTabUI tabUI = (AdminTabUI) tab;
                AdminStep synchroStep = tabUI.getStep();
                if (synchroStep.isConfig()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Test config panel : " + synchroStep);
                    }
                    boolean valid = ui.getModel().validate(synchroStep);
                    if (valid) {
                        if (log.isDebugEnabled()) {
                            log.debug("step " + synchroStep + " is valid");
                        }
                        ui.tabs.setIconAt(index, getTabIcon(WizardState.SUCCESSED));
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("step " + synchroStep + " is not valid!");
                        }
                        ui.tabs.setIconAt(index, getTabIcon(WizardState.FAILED));
                    }
                }
            }
            itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        }

        if (selected != null && selected.getStep() == AdminStep.SHOW_RESUME) {

            // rien a faire sur le dernier onglet
            if (log.isDebugEnabled()) {
                log.debug("We are on last step (resume), nothing to do.");
            }
            return;
        }
        boolean[] accessibleSteps = ui.getModel().getAccessibleSteps();
        if (log.isDebugEnabled()) {
            log.debug("Accessibles step : " + Arrays.toString(accessibleSteps));
        }
        for (; itr.hasNext(); itr.next()) {
            int index = itr.getIndex();
            ui.tabs.setEnabledAt(index, accessibleSteps[index]);
        }
        if (!ui.getModel().containsOperation(AdminStep.REPORT)) {

            // le dernier onglet est accessible uniquement si c'est un resume
            // actuellement seul l'action de report n'a pas de resume
            //TODO chemit 20190 09 29 Il faudrait avoir la propriete needResume pour faire ça de manière générique            
            ui.tabs.setEnabledAt(accessibleSteps.length - 1, true);
        }
    }

    public void onOperationStateChanged(AdminUI ui, AdminStep step, WizardState newState) {
        if (log.isDebugEnabled()) {
            log.debug(step + " - " + newState);
        }

        // mettre a jour l'onglet
        AdminTabUI selected = ui.getStepUI(step);
        int index = ui.tabs.indexOfComponent(selected);
        if (index > -1 && !selected.getStep().isConfig()) {
            ui.tabs.setIconAt(index, getTabIcon(newState));
        }
        if (selected != null && step == selected.getStep()) {
            selected.updateState(newState);
        }
    }

    public void start(AdminUI ui) {

        try {
            // on demarre le modele
            ui.getModel().start(ui);

            // affichage ui
            ui.setVisible(true);

        } finally {

            ui.getModel().setBusy(false);
        }
    }

    public void close(AdminUI ui) {

        Runnable action = WizardUILancher.APPLY_DEF.getContextValue(ui);

        ObserveActionExecutor executor = ObserveRunner.getActionExecutor();

        executor.addAction(t("observe.action.admin.close"), action);
    }

    public void cancel(AdminUI ui) {

        Runnable action = WizardUILancher.CANCEL_DEF.getContextValue(ui);

        ObserveActionExecutor executor = ObserveRunner.getActionExecutor();

        executor.addAction(t("observe.action.admin.cancel"), action);
    }

    public void destroy(AdminUI ui) {
        ui.getModel().destroy();
        if (log.isDebugEnabled()) {
            log.debug("destroy ui " + ui.getName());
        }
        UIHelper.TabbedPaneIterator<Component> itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        for (; itr.hasNext(); ) {
            AdminTabUI tab = (AdminTabUI) itr.next();
            if (log.isDebugEnabled()) {
                log.debug("destroy ui " + tab.getName());
            }
            tab.destroy();
        }
        UIHelper.destroy(ui);
    }

    public void dispose(AdminUI ui) {
        destroy(ui);
    }

    public Icon getTabIcon(WizardState state) {
        Icon i = null;
        if (state != null) {
            String key = "wizard-state-" + state.name().toLowerCase();
            i = UIHelper.getUIManagerActionIcon(key);
        }
        return i;
    }


}
