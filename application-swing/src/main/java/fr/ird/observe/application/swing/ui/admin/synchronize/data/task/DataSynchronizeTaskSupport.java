package fr.ird.observe.application.swing.ui.admin.synchronize.data.task;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import javax.swing.Icon;

/**
 * Created on 03/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public abstract class DataSynchronizeTaskSupport {

    private final ReferentialReference<ProgramDto> program;
    private final DataReference trip;
    private final Icon icon;

    public abstract String getLabel();

    public ReferentialReference<ProgramDto> getProgram() {
        return program;
    }

    public DataReference getTrip() {
        return trip;
    }

    public Icon getIcon() {
        return icon;
    }

    protected DataSynchronizeTaskSupport(ReferentialReference<ProgramDto> program, DataReference trip, String iconName) {
        this.program = program;
        this.trip = trip;
        this.icon = UIHelper.getUIManagerActionIcon(iconName);
    }

    protected String decorateProgram() {
        DecoratorService decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        return decoratorService.getReferentialReferenceDecorator(ProgramDto.class).toString(getProgram());
    }

    protected String decorateTrip() {
        DecoratorService decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        return decoratorService.getTripReferenceDecorator(getTrip()).toString(getTrip());
    }

}
