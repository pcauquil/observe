package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

import java.util.Set;

/**
 * Created on 9/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class SpeciesUIModel extends ContentReferenceUIModel<SpeciesDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_OTHER_TAB_VALID = "otherTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(SpeciesDto.PROPERTY_URI,
                                               SpeciesDto.PROPERTY_CODE,
                                               SpeciesDto.PROPERTY_STATUS,
                                               SpeciesDto.PROPERTY_FAO_CODE,
                                               SpeciesDto.PROPERTY_HOME_ID,
                                               SpeciesDto.PROPERTY_WORMS_ID,
                                               SpeciesDto.PROPERTY_NEED_COMMENT,
                                               SpeciesDto.PROPERTY_SCIENTIFIC_LABEL,
                                               SpeciesDto.PROPERTY_LABEL1,
                                               SpeciesDto.PROPERTY_LABEL2,
                                               SpeciesDto.PROPERTY_LABEL3,
                                               SpeciesDto.PROPERTY_LABEL4,
                                               SpeciesDto.PROPERTY_LABEL5,
                                               SpeciesDto.PROPERTY_LABEL6,
                                               SpeciesDto.PROPERTY_LABEL7,
                                               SpeciesDto.PROPERTY_LABEL8).build();

    public static final Set<String> OTHER_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(SpeciesDto.PROPERTY_SPECIES_GROUP,
                                               SpeciesDto.PROPERTY_OCEAN,
                                               SpeciesDto.PROPERTY_MAX_LENGTH,
                                               SpeciesDto.PROPERTY_MIN_LENGTH,
                                               SpeciesDto.PROPERTY_MAX_WEIGHT,
                                               SpeciesDto.PROPERTY_MIN_WEIGHT).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean otherTabValid;


    public SpeciesUIModel() {
        super(SpeciesDto.class,
              new String[]{SpeciesDto.PROPERTY_SPECIES_GROUP,
                           SpeciesDto.PROPERTY_OCEAN,
                           SpeciesDto.PROPERTY_LENGTH_MEASURE_TYPE,
                           SpeciesDto.PROPERTY_MIN_LENGTH,
                           SpeciesDto.PROPERTY_MAX_LENGTH,
                           SpeciesDto.PROPERTY_MIN_WEIGHT,
                           SpeciesDto.PROPERTY_MAX_WEIGHT,
                           SpeciesDto.PROPERTY_HOME_ID,
                           SpeciesDto.PROPERTY_FAO_CODE,
                           SpeciesDto.PROPERTY_WORMS_ID,
                           SpeciesDto.PROPERTY_SCIENTIFIC_LABEL},
              new String[]{
                      SpeciesDto.PROPERTY_SPECIES_GROUP + SUFFIX_SELECTED_ITEM,
                      SpeciesDto.PROPERTY_LENGTH_MEASURE_TYPE + SUFFIX_TEXT,
                      SpeciesDto.PROPERTY_MIN_LENGTH + SUFFIX_MODEL,
                      SpeciesDto.PROPERTY_MAX_LENGTH + SUFFIX_MODEL,
                      SpeciesDto.PROPERTY_MIN_WEIGHT + SUFFIX_MODEL,
                      SpeciesDto.PROPERTY_MAX_WEIGHT + SUFFIX_MODEL,
                      SpeciesDto.PROPERTY_HOME_ID + SUFFIX_TEXT,
                      SpeciesDto.PROPERTY_FAO_CODE + SUFFIX_TEXT,
                      SpeciesDto.PROPERTY_WORMS_ID + SUFFIX_TEXT,
                      SpeciesDto.PROPERTY_SCIENTIFIC_LABEL + SUFFIX_TEXT,
                      SpeciesDto.PROPERTY_OCEAN + SUFFIX_SELECTED}
        );
    }

    public boolean isOtherTabValid() {
        return otherTabValid;
    }

    public void setOtherTabValid(boolean otherTabValid) {
        Object oldValue = isOtherTabValid();
        this.otherTabValid = otherTabValid;
        firePropertyChange(PROPERTY_OTHER_TAB_VALID, oldValue, otherTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
