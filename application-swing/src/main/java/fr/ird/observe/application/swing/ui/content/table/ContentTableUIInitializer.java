package fr.ird.observe.application.swing.ui.content.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.application.swing.ui.content.ContentUIInitializer;
import fr.ird.observe.application.swing.ui.util.BooleanEditor;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.bean.JavaBeanObjectUtil;

import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * To initialize ui.
 *
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class ContentTableUIInitializer<E extends IdDto, D extends IdDto, UI extends ObserveContentTableUI<E, D>> extends ContentUIInitializer<E, UI> {

    public static final String CLIENT_PROPERTY_TABLE_PROPERTY_NAME = "tablePropertyName";

    public static final String CLIENT_PROPERTY_RESET_TABLE_PROPERTY_NAME = "resetTablePropertyName";

    /** Logger. */
    private static final Log log = LogFactory.getLog(ContentTableUIInitializer.class);

    public ContentTableUIInitializer(UI ui) {
        super(ui);
    }

    protected D getTableEditBean() {
        return getModel().getTableEditBean();
    }

    protected ContentTableUIModel<E, D> getModel() {
        return ui.getModel();
    }

    protected ContentTableUIHandler<E, D> getHandler() {
        return ui.getHandler();
    }

    @Override
    public void initUI() {

        super.initUI();

        ContentTableModel<?, ?> tableModel = ui.getTableModel();
        ui.setContextValue(tableModel);

        if (ui.getBody() != null) {

            // on supprime le layer de blocage en mode disable
            SwingUtil.setLayerUI(ui.getBody(), null);

        }

        if (ui.getExtraZone() != null) {

            // et on le remet sur les zones non clicables de l'ui
            SwingUtil.setLayerUI(ui.getExtraZone(), ui.getBlockLayerUI());

        }

        // on ajoute un layer pour bloquer l'édition des entrées si nécessaire
        SwingUtil.setLayerUI(ui.getEditor(), ui.getEditorBlockLayerUI());

        // préparation du tableau dans l'ui
        getHandler().initTableUI(new DefaultTableCellRenderer());

        // ajout d'un listener pour preparer l'éditeur d'une entrée selectionnée
        tableModel.addPropertyChangeListener(
                ContentTableModel.SELECTED_ROW_PROPERTY,
                evt -> {
                    ContentTableModel<E, D> model;
                    model = (ContentTableModel<E, D>) evt.getSource();
                    D bean = model.getRowBean();
                    boolean create = bean.getId() == null;
                    Integer selectedRow = (Integer) evt.getNewValue();
                    if (log.isDebugEnabled()) {
                        log.debug("callback new selectedRow : " + selectedRow + " : " + bean.getId());
                    }
                    getHandler().onSelectedRowChanged(selectedRow, bean, create);
                    getModel().setRowSaved(!create);
                    if (selectedRow == -1) {
                        if (log.isDebugEnabled()) {
                            log.debug(">>>>>>>>>> will clear selection...");
                        }
                        // on supprime la selection
                        ui.getSelectionModel().clearSelection();
                        if (log.isDebugEnabled()) {
                            log.debug("<<<<<<<<<< has clear selection...");
                        }
                    } else {
                        // on met a jour le modele de selection
                        ui.getSelectionModel().setSelectionInterval(
                                selectedRow, selectedRow);
                    }
                });

        getModel().addPropertyChangeListener(
                ContentTableUIModel.PROPERTY_SHOW_DATA,
                evt -> {

                    Boolean newValue = (Boolean) evt.getNewValue();
                    onShowDataChanged(newValue);

                });

    }

    @Override
    protected void initBlockLayerUI(String... doNotBlockComponentIds) {

        super.initBlockLayerUI(doNotBlockComponentIds);

        //tchemit-2014-12-29 We should use a spearate list for this, but there is no collision possible...
        ui.getEditorBlockLayerUI().setAcceptedComponentNames(doNotBlockComponentIds);

    }

    protected void onShowDataChanged(Boolean showData) {

        JComponent body = ui.getBody();
        if (showData != null && showData) {
            body.remove(ui.getHideForm());
            body.add(ui.getShowForm(), BorderLayout.CENTER);
        } else {
            body.remove(ui.getShowForm());
            body.add(ui.getHideForm(), BorderLayout.CENTER);
        }
    }

    @Override
    protected void init(ActionMap actionMap,
                        AbstractButton editor) {

        super.init(actionMap, editor);

        String actionId = (String) editor.getClientProperty(ContentUIInitializer.OBSERVE_ACTION);
        if (actionId == null) {

            // le boutton n'est pas commun

            final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_RESET_TABLE_PROPERTY_NAME);
            if (propertyName != null) {
                editor.addActionListener(e -> JavaBeanObjectUtil.setProperty(getTableEditBean(), propertyName, null));
            }

        }

    }

    @Override
    protected void init(JTextField editor) {

        super.init(editor);

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_TABLE_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    JTextField source = (JTextField) e.getSource();
                    String text = source.getText();
                    JavaBeanObjectUtil.setProperty(getTableEditBean(), propertyName, text);
                }
            });
        }

    }

    @Override
    protected void init(JTextArea editor) {

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_TABLE_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    JTextArea source = (JTextArea) e.getSource();
                    String text = source.getText();
                    JavaBeanObjectUtil.setProperty(getTableEditBean(), propertyName, text);
                }
            });
        }

    }

    @Override
    protected void init(final JCheckBox editor) {

        super.init(editor);

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_TABLE_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addItemListener(event -> {
                Boolean newValue = ((JCheckBox) event.getSource()).isSelected();
                JavaBeanObjectUtil.setProperty(getTableEditBean(), propertyName, newValue);
            });
        }

    }

    @Override
    protected void init(BooleanEditor editor) {

        super.init(editor);

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_TABLE_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addItemListener(event -> {
                if (event.getStateChange() == ItemEvent.SELECTED) {

                    Boolean newValue = ((BooleanEditor) event.getSource()).getBooleanValue();
                    JavaBeanObjectUtil.setProperty(getTableEditBean(), propertyName, newValue);
                }
            });
        }
    }

}
