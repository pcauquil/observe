package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.SectionTemplate;
import fr.ird.observe.services.dto.longline.SectionWithTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class SectionsTableModel extends LonglineCompositionTableModelSupport<SectionWithTemplate> {

    public static final String TEMPLATE_PROPERTY = "template";

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(SectionsTableModel.class);

    public SectionsTableModel(LonglineDetailCompositionUIModel model) {
        super(model);
    }

    @Override
    protected SectionWithTemplate createNewRow() {
        return new SectionWithTemplate();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {

        boolean result;

        switch (columnIndex) {

            case 0:

                // can never edit setting id
                result = false;
                break;

            case 1:

                // can edit hauling id if and only if set has hauling breaks
                result = !isGenerateHaulingIds();
                break;

            case 2:

                // can change template if in generate mode
                result = isCanGenerate();
                break;

            default:
                throw new IllegalStateException("Can't come here");

        }

        return result;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SectionWithTemplate row = data.get(rowIndex);
        Object result;

        switch (columnIndex) {
            case 0:

                result = row.getSettingIdentifier();
                break;

            case 1:

                result = row.getHaulingIdentifier();
                break;

            case 2:

                result = row.getSectionTemplate();
                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

        return result;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        SectionWithTemplate row = data.get(rowIndex);

        switch (columnIndex) {
            case 0:

                row.setSettingIdentifier((Integer) aValue);
                setModified(true);
                break;

            case 1:

                row.setHaulingIdentifier((Integer) aValue);
                setModified(true);
                break;

            case 2:

                SectionTemplate sectionTemplate = (SectionTemplate) aValue;

                boolean changeTemplate = true;

                if (sectionTemplate != null) {

                    // check if can use this template
                    int basketsCount = row.sizeBasket();
                    boolean compiliantWithBasketCount = sectionTemplate.isCompiliantWithBasketCount(basketsCount);

                    if (!compiliantWithBasketCount) {

                        // We can't use this value
                        if (log.isWarnEnabled()) {
                            log.warn("sectionTemplate " + sectionTemplate + " is not compliant with basketCount: " + basketsCount);
                        }
                        //TODO Send user a message

                        changeTemplate = false;

                    }

                }

                if (changeTemplate) {

                    SectionTemplate previousSectionTemplate = row.getSectionTemplate();
                    row.setSectionTemplate(sectionTemplate);
                    firePropertyChange(TEMPLATE_PROPERTY, previousSectionTemplate, sectionTemplate);

                }

                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

    }

}
