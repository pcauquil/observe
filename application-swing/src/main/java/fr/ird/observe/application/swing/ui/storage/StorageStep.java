/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import fr.ird.observe.application.swing.I18nEnumHelper;
import jaxx.runtime.swing.wizard.WizardStep;

/**
 * Pour caractériser les étapes (correspond aux onglets de l'ui).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public enum StorageStep implements WizardStep {

    /** pour choisir le mode de connexion (local ou remote) */
    CHOOSE_DB_MODE,
    /**
     * pour configurer la connexion à la source de données (uniquement si
     * besoin)
     */
    CONFIG,
    /**
     * pour configurer le referentiel a utiliser lors de la creation d'une base
     * distante
     */
    CONFIG_REFERENTIEL,
    /**
     * pour configurer la sources des données a utiliser lors de la creation d'une base
     * distante
     */
    CONFIG_DATA,
    /**
     * pour effectuer une sauvegarde de la base locale. (uniquement disponible
     * si on veut générer une nouvelle base locale)
     */
    BACKUP,
    /**
     * pour sélectionner les données à sauvegarder (unqiuement sur une ui de
     * backup)
     */
    SELECT_DATA,
    /**
     * Pour sélectionner les rôles à appliquer pour la création ou mise à jour
     * d'une base distante
     */
    ROLES,
    /** pour confirmer et réaliser les actions demandées */
    CONFIRM;

    @Override
    public String getLabel() {
        return I18nEnumHelper.getLabel(this);
    }

    @Override
    public String getDescription() {
        return I18nEnumHelper.getLabel(this);
    }
}
