package fr.ird.observe.application.swing.ui.util;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.tree.AbstrctReferenceNodeSupport;
import fr.ird.observe.services.dto.IdDto;
import org.nuiton.decorator.Decorator;

import java.util.Objects;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class DecoratedNodeEntity {

    private final String id;

    private final String label;

    public static <E extends IdDto> DecoratedNodeEntity newDecoratedNodeEntity(AbstrctReferenceNodeSupport<E, ?> node, Decorator<?> decorator) {
        Objects.requireNonNull(node);
        Objects.requireNonNull(decorator);
        return new DecoratedNodeEntity(node.getId(), decorator.toString(node.getEntity()));
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return label;
    }

    protected DecoratedNodeEntity(String id, String label) {
        this.id = id;
        this.label = label;
    }

}
