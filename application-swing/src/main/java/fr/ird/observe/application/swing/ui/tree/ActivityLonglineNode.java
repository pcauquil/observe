package fr.ird.observe.application.swing.ui.tree;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import fr.ird.observe.application.swing.ui.tree.loadors.ActivityLonglineNodeChildLoador;

/**
 * Created on 4/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public class ActivityLonglineNode extends DataReferenceNodeSupport<ActivityLonglineDto> {

    private static final long serialVersionUID = 1L;

    public ActivityLonglineNode(DataReference<ActivityLonglineDto> entity) {
        super(ActivityLonglineDto.class, entity, ObserveTreeHelper.getChildLoador(ActivityLonglineNodeChildLoador.class));
    }

    @Override
    protected DataReference<ActivityLonglineDto> fetchEntity() {
        ActivityLonglineService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivityLonglineService();
        return service.loadReferenceToRead(getId());
    }

}
