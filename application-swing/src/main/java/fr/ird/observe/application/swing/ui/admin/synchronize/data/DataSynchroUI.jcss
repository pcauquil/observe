/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#PENDING_content {
  layout:{new BorderLayout()};
}

#NEED_FIX_content {
  layout:{new BorderLayout()};
}

#startAction {
  actionIcon:"wizard-start";
  text:"observe.actions.synchro.data.launch.operation";
}

#contentSplitPane {
  orientation: {JSplitPane.VERTICAL_SPLIT};
  resizeWeight: 0.8;
}

#applyAction {
  actionIcon:accept;
  text:"observe.action.apply";
}

#leftTree {
  rootVisible:false;
  largeModel:true;
  minimumSize:{UIHelper.newMinDimension()};
  font-size:11;
  showsRootHandles:false;
  toggleClickCount:100;
  selectionModel:{leftSelectionModel};
  cellRenderer:{new DataSelectionTreeCellRenderer()};
}

#leftSelectionModel {
  selectionMode: {TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION};
  dataModel:{leftSelectDataModel};
}

#leftTreePane {
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED};
}

#rightTree {
  rootVisible:false;
  largeModel:true;
  minimumSize:{UIHelper.newMinDimension()};
  font-size:11;
  showsRootHandles:false;
  toggleClickCount:100;
  selectionModel:{rightSelectionModel};
  cellRenderer:{new DataSelectionTreeCellRenderer()};
}

#rightSelectionModel {
  selectionMode: {TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION};
  dataModel:{rightSelectDataModel};
}

#rightTreePane {
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED};
}

#copyToLeft {
  actionIcon:copyToLeft;
  toolTipText:"observe.actions.synchro.data.copyToLeft.tip";
  enabled:false;
}

#copyToRight {
  actionIcon:copyToRight;
  toolTipText:"observe.actions.synchro.data.copyToRight.tip";
  enabled:false;
}

#deleteFromLeft {
  actionIcon:deleteFromLeft;
  toolTipText:"observe.actions.synchro.data.deleteFromLeft.tip";
  enabled:false;
}

#deleteFromRight {
  actionIcon:deleteFromRight;
  toolTipText:"observe.actions.synchro.data.deleteFromRight.tip";
  enabled:false;
}

#actionsToConsumePane {
  border:{new TitledBorder(t("observe.actions.synchro.data.actionsToPerform"))};
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED};
}

#actionsToConsume {
  selectionMode:{ListSelectionModel.SINGLE_SELECTION};
  cellRenderer:{new DataSynchronizeTaskListCellRenderer()};
  model:{getStepModel().getTasks()};
}