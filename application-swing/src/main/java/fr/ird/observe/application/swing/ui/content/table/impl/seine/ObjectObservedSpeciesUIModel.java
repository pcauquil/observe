package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDtos;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class ObjectObservedSpeciesUIModel extends ContentTableUIModel<FloatingObjectObservedSpeciesDto, ObjectObservedSpeciesDto> {

    private static final long serialVersionUID = 1L;

    public ObjectObservedSpeciesUIModel(ObjectObservedSpeciesUI ui) {

        super(FloatingObjectObservedSpeciesDto.class,
              ObjectObservedSpeciesDto.class,
              new String[]{
                      FloatingObjectObservedSpeciesDto.PROPERTY_OBJECT_OBSERVED_SPECIES,
                      FloatingObjectObservedSpeciesDto.PROPERTY_COMMENT},
              new String[]{
                      ObjectObservedSpeciesDto.PROPERTY_SPECIES,
                      ObjectObservedSpeciesDto.PROPERTY_SPECIES_STATUS,
                      ObjectObservedSpeciesDto.PROPERTY_COUNT}
        );

        List<ContentTableMeta<ObjectObservedSpeciesDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(ObjectObservedSpeciesDto.class, ObjectObservedSpeciesDto.PROPERTY_SPECIES, false),
                ContentTableModel.newTableMeta(ObjectObservedSpeciesDto.class, ObjectObservedSpeciesDto.PROPERTY_SPECIES_STATUS, false),
                ContentTableModel.newTableMeta(ObjectObservedSpeciesDto.class, ObjectObservedSpeciesDto.PROPERTY_COUNT, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<FloatingObjectObservedSpeciesDto, ObjectObservedSpeciesDto> createTableModel(
            ObserveContentTableUI<FloatingObjectObservedSpeciesDto, ObjectObservedSpeciesDto> ui,
            List<ContentTableMeta<ObjectObservedSpeciesDto>> contentTableMetas) {

        return new ContentTableModel<FloatingObjectObservedSpeciesDto, ObjectObservedSpeciesDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<ObjectObservedSpeciesDto> getChilds(FloatingObjectObservedSpeciesDto bean) {
                return bean.getObjectObservedSpecies();
            }

            @Override
            protected void load(ObjectObservedSpeciesDto source, ObjectObservedSpeciesDto target) {
                ObjectObservedSpeciesDtos.copyObjectObservedSpeciesDto(source, target);
            }

            @Override
            protected void setChilds(FloatingObjectObservedSpeciesDto parent, List<ObjectObservedSpeciesDto> childs) {
                bean.setObjectObservedSpecies(childs);
            }
        };
    }
}
