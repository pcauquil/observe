<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<JDialog id="storageMain"
         implements='jaxx.runtime.swing.wizard.WizardUI&lt;StorageStep, StorageUIModel&gt;'
         width='550' height='600' defaultCloseOperation='dispose_on_close'
  >

  <import>
    fr.ird.observe.application.swing.ui.UIHelper
    fr.ird.observe.application.swing.ui.storage.tabs.StorageTabUI
    fr.ird.observe.application.swing.ui.storage.tabs.BackupUI
    fr.ird.observe.application.swing.ui.storage.tabs.ChooseDbModeUI
    fr.ird.observe.application.swing.ui.storage.tabs.ConfigReferentielUI
    fr.ird.observe.application.swing.ui.storage.tabs.ConfigDataUI
    fr.ird.observe.application.swing.ui.storage.tabs.ConfigUI
    fr.ird.observe.application.swing.ui.storage.tabs.ConfirmUI
    fr.ird.observe.application.swing.ui.storage.tabs.RolesUI
    fr.ird.observe.application.swing.ui.storage.tabs.SelectDataUI

    java.awt.Window

    jaxx.runtime.JAXXContext
    javax.swing.UIManager
  </import>

  <StorageUIHandler id='handler' initializer='getContextValue(StorageUIHandler.class)'/>

  <StorageUIModel id='model' javaBean='getContextValue(StorageUIModel.class)'/>

  <!-- le bloqueur d'ui lorsqu'une action est en cours ou annulée -->
  <BlockingLayerUI id='busyBlockLayerUI'/>

  <CardLayout>
    <!-- les differents contenu d'onglets -->
    <ChooseDbModeUI id='CHOOSE_DB_MODE' constructorParams='this'/>
    <ConfigUI id='CONFIG' constructorParams='this'/>
    <ConfigReferentielUI id='CONFIG_REFERENTIEL' constructorParams='this'/>
    <ConfigDataUI id='CONFIG_DATA' constructorParams='this'/>
    <BackupUI id='BACKUP' constructorParams='this'/>
    <SelectDataUI id='SELECT_DATA' constructorParams='this'/>
    <RolesUI id='ROLES' constructorParams='this'/>
    <ConfirmUI id='CONFIRM' constructorParams='this'/>
  </CardLayout>

  <script><![CDATA[

private boolean contextInitialized;

public StorageUI(Window owner, JAXXContext parentContext) {
    super(owner);
    // verification du context parent
    UIHelper.checkJAXXContextEntry(parentContext, UIHelper.newContextEntryDef(StorageUIHandler.class));
    UIHelper.checkJAXXContextEntry(parentContext, UIHelper.newContextEntryDef(StorageUIModel.class));
    UIHelper.checkJAXXContextEntry(parentContext, UIHelper.newContextEntryDef("apply", Runnable.class));
    UIHelper.checkJAXXContextEntry(parentContext, UIHelper.newContextEntryDef("cancel", Runnable.class));

    if (owner != null) {
        setContextValue(owner, "parent");
    }
    UIHelper.initContext(this, parentContext);
    contextInitialized = true;
}

public void destroy() {
    getHandler().destroy(this);    
}

@Override
public void dispose() {
    if (log.isDebugEnabled()) {
        log.debug("dispose storage ui " + this);
    }
    destroy();
    super.dispose();
}

@Override
protected void finalize() throws Throwable {
    super.finalize();
    destroy();
}

@Override
public void start() {
    getHandler().start(this);
}

@Override
public StorageStep getSelectedStep() {
    int index = tabs.getSelectedIndex();
    StorageTabUI c = null;
    if (index > -1) {
        c = (StorageTabUI)  tabs.getComponentAt(index);
    }
    StorageStep result = c == null ? null : c.getStep();
    return result;
}

@Override
public StorageTabUI getStepUI(StorageStep step) {
    if (step != null) {
        return (StorageTabUI) getObjectById(step.name());
    }
    return null;
}

@Override
public StorageTabUI getStepUI(int stepIndex) {
    if (stepIndex > tabs.getTabCount()) {
        return null;
    }
    return (StorageTabUI)  tabs.getComponentAt(stepIndex);
}

@Override
public StorageTabUI getSelectedStepUI() {
    StorageStep step = getSelectedStep();
    StorageTabUI ui = getStepUI(step);
    return ui;
}

@Override
public void onStepChanged(StorageStep oldStep, StorageStep newStep) {
    getHandler().onStepChanged(this, oldStep, newStep);
}

@Override
public void onStepsChanged(StorageStep[] steps) {
    
}

void $afterCompleteSetup() {
    getHandler().initUI(this);
}
]]>
  </script>

  <!-- les onglets -->
  <JTabbedPane id='tabs' decorator='boxed'
               constraints='BorderLayout.CENTER'
               onStateChanged='if (getSelectedStep() != null) { getModel().gotoStep(getSelectedStep()); }'/>

  <!-- les actions -->
  <Table id='actions' weightx='1' fill='both' constraints='BorderLayout.SOUTH'>
    <row>
      <cell weightx='0.5' fill="both">
        <!-- pour annuler  -->
        <JButton id="cancelAction"
                 onActionPerformed='getHandler().launchCancel(this)'/>
      </cell>
      <cell weightx='0.5' fill="both">
        <!-- pour aller sur l'onglet précédent -->
        <JButton id="previousAction"
                 onActionPerformed='getModel().gotoPreviousStep()'/>
      </cell>
      <cell weightx='0.5' fill="both">
        <!-- pour aller sur l'onglet suivant -->
        <JButton id="nextAction"
                 onActionPerformed='getModel().gotoNextStep()'/>
      </cell>
      <cell weightx='0.5' fill="both">
        <!-- pour apliquer la configuration -->
        <JButton id="applyAction"
                 onActionPerformed='getHandler().launchApply(this)'/>
      </cell>
    </row>
  </Table>

</JDialog>
