package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;

import javax.swing.UIManager;
import java.util.Enumeration;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TypeReferentialSynchroNode extends ReferentialSynchroNodeSupport {

    private static final long serialVersionUID = 1L;

    public TypeReferentialSynchroNode(Class<? extends ReferentialDto> type) {
        super(UIManager.getIcon("navigation.sub.referentiel-16"), type);
    }

    @Override
    public RootReferentialSynchroNode getParent() {
        return (RootReferentialSynchroNode) super.getParent();
    }

    public <R extends ReferentialDto> ReferenceReferentialSynchroNodeSupport getChild(String id) {
        Enumeration children = children();
        while (children.hasMoreElements()) {
            ReferenceReferentialSynchroNodeSupport o = (ReferenceReferentialSynchroNodeSupport) children.nextElement();
            if (id.equals(o.getUserObject().getId())) {
                return o;
            }
        }
        return null;
    }

    @Override
    public Class<? extends ReferentialDto> getUserObject() {
        return (Class) super.getUserObject();
    }

}
