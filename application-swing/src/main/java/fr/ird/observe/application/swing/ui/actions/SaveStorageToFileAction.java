package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.storage.StorageBackupUILauncher;
import jaxx.runtime.SwingUtil;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class SaveStorageToFileAction extends AbstractObserveAction {

    private static final long serialVersionUID = 1L;

    private final ObserveMainUI ui;

    public SaveStorageToFileAction(ObserveMainUI ui) {

        super(t("observe.action.save.to.file"), SwingUtil.getUIManagerActionIcon("local-export"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.save.to.file.tip"));
        putValue(MNEMONIC_KEY, (int) 'L');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {

            // on teste que l'utilisateur peut lire-ecrire sur la source de données

            ObserveSwingDataSource service = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();

            if (!service.isLocal() && !(service.canReadData() && service.canWriteData())) {

                UIHelper.askUser(
                        null,
                        t("observe.title.can.not.export.obstuna"),
                        t("observe.storage.required.rw.on.data", service),
                        JOptionPane.ERROR_MESSAGE,
                        new Object[]{t("observe.choice.cancel")},
                        0
                );
                return;
            }

            String title = service.isLocal() ?
                           t("observe.title.save.localDB") :
                           t("observe.title.save.remoteDB");

            StorageBackupUILauncher launcher =
                    new StorageBackupUILauncher(ui, ui, title);
            launcher.start();
        }

    }

}
