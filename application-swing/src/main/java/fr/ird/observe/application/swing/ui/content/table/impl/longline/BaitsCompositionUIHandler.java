package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.longline.BaitsCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.referential.longline.BaitSettingStatusDto;
import fr.ird.observe.services.dto.referential.longline.BaitTypeDto;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 12/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class BaitsCompositionUIHandler extends ContentTableUIHandler<SetLonglineGlobalCompositionDto, BaitsCompositionDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(BaitsCompositionUIHandler.class);

    public BaitsCompositionUIHandler(BaitsCompositionUI ui) {
        super(ui, DataContextType.SetLongline);
    }

    @Override
    public BaitsCompositionUI getUi() {
        return (BaitsCompositionUI) super.getUi();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, BaitsCompositionDto bean, boolean create) {

        if (getTableModel().isEditable()) {
            if (log.isDebugEnabled()) {
                log.debug("Row has changed to " + editingRow);
            }
            getUi().getBaitType().requestFocus();
        }

    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.baitsComposition.table.baitType"),
                n("observe.content.baitsComposition.table.baitType.tip"),
                n("observe.content.baitsComposition.table.baitSettingStatus"),
                n("observe.content.baitsComposition.table.baitSettingStatus.tip"),
                n("observe.content.baitsComposition.table.individualSize"),
                n("observe.content.baitsComposition.table.individualSize.tip"),
                n("observe.content.baitsComposition.table.individualWeight"),
                n("observe.content.baitsComposition.table.individualWeight.tip"),
                n("observe.content.baitsComposition.table.proportion"),
                n("observe.content.baitsComposition.table.proportion.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, BaitTypeDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, BaitSettingStatusDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newEmptyNumberTableCellRenderer(renderer));

        // when model change in table, let's recompute the proportion sum
        table.getModel().addTableModelListener(e -> {
            int proportionSum = getBean().getBaitsCompositionProportionSum();
            getBean().setBaitsCompositionProportionSum(proportionSum);
        });

    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @Override
    protected void doPersist(SetLonglineGlobalCompositionDto bean) {
        // fait par le doSave de LonglineGlobalCompositionUIHandler
    }

    @Override
    protected void loadEditBean(String beanId) {
        // fait par le loadEditBean de LonglineGlobalCompositionUIHandler
    }

    @Override
    public void afterSave(boolean refresh) {
        super.afterSave(refresh);
    }

}
