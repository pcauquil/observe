package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDtos;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.application.swing.ui.UIHelper;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.16
 */
public class GearUseFeatureMeasurementCellRenderer implements TableCellRenderer {

    protected final int caracteristicColumn;

    protected final Map<String, TableCellRenderer> renderersByCaracteristicTypeId;


    public GearUseFeatureMeasurementCellRenderer(int caracteristicColumn, DefaultTableCellRenderer renderer) {

        this.caracteristicColumn = caracteristicColumn;
        renderersByCaracteristicTypeId = new TreeMap<>();

        {
            // texte
            TableCellRenderer editor = UIHelper.newStringTableCellRenderer(renderer, 10, true);
            renderersByCaracteristicTypeId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.1", editor);
        }
        {
            // boolean
            TableCellRenderer editor = UIHelper.newBooleanTableCellRenderer(renderer);
            renderersByCaracteristicTypeId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.2", editor);
        }
        {
            // entier signé
            TableCellRenderer editor = UIHelper.newEmptyNumberTableCellRenderer(renderer);
            renderersByCaracteristicTypeId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.3", editor);
        }
        {
            // décimal signé
            TableCellRenderer editor = UIHelper.newEmptyNumberTableCellRenderer(renderer);
            renderersByCaracteristicTypeId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.4", editor);
        }
        {
            // entier non signé
            TableCellRenderer editor = UIHelper.newEmptyNumberTableCellRenderer(renderer);
            renderersByCaracteristicTypeId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.5", editor);
        }
        {
            // décimal non signé
            TableCellRenderer editor = UIHelper.newEmptyNumberTableCellRenderer(renderer);
            renderersByCaracteristicTypeId.put("fr.ird.observe.entities.referentiel.GearCaracteristicType#1239832686123#0.6", editor);
        }
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        TableCellRenderer tableCellRenderer;

        ReferentialReference<GearCaracteristicDto> caracteristicRef = (ReferentialReference<GearCaracteristicDto>) table.getModel().getValueAt(row, caracteristicColumn);
        if (caracteristicRef == null) {

            tableCellRenderer = table.getDefaultRenderer(Object.class);

        } else {
            String gearCaracteristicTypeId = (String) caracteristicRef.getPropertyValue(GearCaracteristicDto.PROPERTY_GEAR_CARACTERISTIC_TYPE);

            tableCellRenderer = renderersByCaracteristicTypeId.get(gearCaracteristicTypeId);

            value = GearCaracteristicTypeDtos.getTypeValue(gearCaracteristicTypeId, value);

        }

        return tableCellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    }

}
