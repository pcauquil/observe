/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.usage;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.ReferenceMap;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class UsagesUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(UsagesUIHandler.class);

    /**
     * Afficher les usages d'une entite donnee.
     *
     * @param ui       l'ui
     * @param message  le message a afficher en haut
     * @param message2 message supplementaire a afficher en haut
     * @param message3 message supplementaire  a afficher en haut
     * @param usages   les utilisations de l'entite donnee
     */
    public void initUI(UsagesUI ui,
                       String message,
                       String message2,
                       String message3,
                       ReferenceMap usages) {
        // toujours nettoyer l'ui avant tout
        cleanUI(ui);

        ui.getMessage().setText(t(message));
        if (message2 != null) {
            ui.getMessage2().setVisible(true);
            ui.getMessage2().setText(message2);
        }
        if (message3 != null) {
            ui.getMessage3().setVisible(true);
            ui.getMessage3().setText(message3);
        }
        if (usages.isEmpty()) {
            ui.getUsages().add(new JLabel(t("observe.message.no.usage.for.entity")));
        } else {

            for (Map.Entry<Class<? extends IdDto>, Set<? extends AbstractReference>> entry : usages.entrySet()) {
                Class dtoType = entry.getKey();
                Set references = entry.getValue();
                String typeTitle = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoType));
                if (DataDto.class.isAssignableFrom(dtoType)) {
                    addDataReferenceUsages(ui, dtoType, references, typeTitle);
                } else {
                    addReferentialReferenceUsages(ui, dtoType, references, typeTitle);
                }

            }
        }
    }

    public void cleanUI(UsagesUI ui) {
        ui.getUsages().removeAll();
        ui.getMessage().setText(null);
        ui.getMessage2().setText(null);
        ui.getMessage2().setVisible(false);
        ui.getMessage3().setText(null);
        ui.getMessage3().setVisible(false);
    }

    protected <D extends DataDto> void addDataReferenceUsages(UsagesUI ui,
                                                              Class<D> dtoType,
                                                              Set<DataReference<D>> references,
                                                              String typeTitle) {

        int size = references.size();
        String typetitle = n("observe.content.label.usage.data.title");
        typetitle = t(typetitle, typeTitle, size);

        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getDataReferenceDecorator(dtoType);
        Objects.requireNonNull(decorator, "could not find decorator for type " + dtoType);

        List<String> data = new ArrayList<>(size);
        data.addAll(references.stream().map(decorator::toString).collect(Collectors.toList()));

        JList<? super String> l = new JList<>(data.toArray());

        JScrollPane pane = new JScrollPane();
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        pane.setMinimumSize(new Dimension(300, 30));
//                pane.setMaximumSize(new Dimension(300, 100));
        pane.setColumnHeaderView(new JLabel(typetitle));
        pane.setViewportView(l);
        ui.getUsages().add(pane);
    }


    protected <D extends ReferentialDto> void addReferentialReferenceUsages(UsagesUI ui,
                                                                            Class<D> dtoType,
                                                                            Set<ReferentialReference<D>> references,
                                                                            String typeTitle) {

        int size = references.size();
        String typetitle = n("observe.content.label.usage.referentiel.title");
        typetitle = t(typetitle, typeTitle, size);

        Decorator<?> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(dtoType);
        Objects.requireNonNull(decorator, "could not find decorator for type " + dtoType);

        List<String> data = new ArrayList<>(size);
        data.addAll(references.stream().map(decorator::toString).collect(Collectors.toList()));

        JList<? super String> l = new JList<>(data.toArray());

        JScrollPane pane = new JScrollPane();
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        pane.setMinimumSize(new Dimension(300, 30));
//                pane.setMaximumSize(new Dimension(300, 100));
        pane.setColumnHeaderView(new JLabel(typetitle));
        pane.setViewportView(l);
        ui.getUsages().add(pane);
    }

}
