/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.db.constants;

import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;

/**
 * Les types de données connues par le {@link DataContext}.
 *
 * Chaque constant permet de récupérer des données du context de données.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public enum DataContextType {

    Program(ProgramDto.class, DataContext.PROPERTY_OPEN_PROGRAM) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenProgram();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenProgramId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenProgramId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedProgramId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedProgramId(id);
        }
    },

    TripSeine(TripSeineDto.class, DataContext.PROPERTY_OPEN_TRIP) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenTripSeine();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenTripSeineId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenTripSeineId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedTripSeineId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedTripId(id);
        }
    },

    TripLongline(TripLonglineDto.class, DataContext.PROPERTY_OPEN_TRIP) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenTripLongline();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenTripLonglineId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenTripLonglineId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedTripLonglineId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedTripId(id);
        }
    },

    Route(RouteDto.class, DataContext.PROPERTY_OPEN_ROUTE) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenRoute();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenRouteId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenRouteId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedRouteId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedRouteId(id);
        }
    },

    ActivitySeine(ActivitySeineDto.class, DataContext.PROPERTY_OPEN_ACTIVITY) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenActivityLongline();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenActivitySeineId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenActivitySeineId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedActivitySeineId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedActivityId(id);
        }
    },

    ActivityLongline(ActivityLonglineDto.class, DataContext.PROPERTY_OPEN_ACTIVITY) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenActivityLongline();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenActivityLonglineId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenActivityLonglineId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedActivityLonglineId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedActivityId(id);
        }
    },

    SetSeine(SetSeineDto.class, DataContext.PROPERTY_OPEN_SET) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenSetSeine();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenSetSeineId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenSetSeineId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedSetSeineId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedSetId(id);
        }
    },

    SetLongline(SetLonglineDto.class, DataContext.PROPERTY_OPEN_SET) {
        @Override
        public boolean isOpen(DataContext context) {
            return context.isOpenSetLongline();
        }

        @Override
        public String getOpenId(DataContext context) {
            return context.getOpenSetLonglineId();
        }

        @Override
        public void setOpenId(DataContext dataContext, String id) {
            dataContext.setOpenSetLonglineId(id);
        }

        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedSetLonglineId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedSetId(id);
        }
    },

    FloatingObject(FloatingObjectDto.class, null) {
        @Override
        public String getSelectedId(DataContext context) {
            return context.getSelectedFloatingObjectId();
        }

        @Override
        public void setSelectedId(DataContext context, String id) {
            context.setSelectedFloatingObjectId(id);
        }
    };

    private final Class<?> type;

    private final String openProperty;

    DataContextType(Class<?> type, String openProperty) {
        this.type = type;
        this.openProperty = openProperty;
    }

    public boolean acceptType(Class<?> type) {
        return type.equals(getType());
    }

    public boolean acceptId0(String id) {
        return id.startsWith(getType().getName());
    }

    public Class<?> getType() {
        return type;
    }

    public String getOpenProperty() {
        return openProperty;
    }

    public boolean canOpen() {
        return openProperty != null;
    }

    public boolean isOpen(DataContext context) {
        return false;
    }

    public String getOpenId(DataContext context) {
        return null;
    }

    public abstract String getSelectedId(DataContext context);

    public abstract void setSelectedId(DataContext context, String id);

    public static DataContextType acceptId(String s) {
        DataContextType result = null;
        for (DataContextType type : DataContextType.values()) {

            String[] entityFullClass = s.split("#")[0].split("\\.");
            String typeName = type.getType().getSimpleName();
            if (typeName.startsWith(entityFullClass[entityFullClass.length - 1])) {
                result = type;
                break;
            }
        }
        return result;
    }

    public void setOpenId(DataContext dataContext, String id) {
        // rien a faire par defaut
    }
}
