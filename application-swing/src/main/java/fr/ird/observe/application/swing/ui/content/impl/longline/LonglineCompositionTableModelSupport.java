package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.AbstractObserveDto;
import fr.ird.observe.services.dto.longline.LonglineCompositionDto;
import fr.ird.observe.application.swing.ui.util.table.EditableTableModelSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public abstract class LonglineCompositionTableModelSupport<E extends AbstractObserveDto & LonglineCompositionDto> extends EditableTableModelSupport<E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LonglineCompositionTableModelSupport.class);

    private static final long serialVersionUID = 1L;

    private final LonglineDetailCompositionUIModel model;

    public LonglineCompositionTableModelSupport(LonglineDetailCompositionUIModel model) {
        super(false);
        this.model = model;
    }

    @Override
    protected final boolean isRowValid(E valid) {
        // Not used here
        return true;
    }

    @Override
    public final boolean isRowNotEmpty(E valid) {
        // Not used here
        return true;
    }

    public void rearrangeIds(boolean fire) {

        rearrangeIds(data);

        if (fire && !isEmpty()) {

            fireTableRowsUpdated(0, getRowCount() - 1);

        }

    }

    public void rearrangeIds(List<E> data) {

        boolean generateHaulingIds = isGenerateHaulingIds();
        boolean haulingdirectionSameAsSettings = model.isHaulingdirectionSameAsSettings();

        int dataSize = data.size();

        if (log.isInfoEnabled()) {
            log.info("Will rearrange ids for " + dataSize + " data on " + this);
            log.info("generateHaulingIds ? " + generateHaulingIds);
            log.info("haulingdirectionSameAsSettings ? " + haulingdirectionSameAsSettings);
        }

        int index = 1;

        for (E e : data) {

            if (log.isInfoEnabled()) {
                log.info("SettingIdentifier : " + index);
            }
            e.setSettingIdentifier(index);

            if (generateHaulingIds) {

                int haulingId;

                if (haulingdirectionSameAsSettings) {
                    haulingId = index;
                } else {
                    haulingId = dataSize - index + 1;
                }

                if (log.isInfoEnabled()) {
                    log.info("HaulingIdentifier : " + haulingId);
                }
                e.setHaulingIdentifier(haulingId);

            }

            index++;

        }

    }

    @Override
    public void addNewRow() {

        super.addNewRow();

        validate();
        setModified(true);

    }

    @Override
    public void insertAfterSelectedRow() {

        super.insertAfterSelectedRow();

        validate();
        setModified(true);

    }

    @Override
    public void insertBeforeSelectedRow() {

        super.insertBeforeSelectedRow();

        validate();
        setModified(true);

    }

    @Override
    public void removeData(int selectedRow) {

        super.removeData(selectedRow);

        validate();
        setModified(true);

    }

    protected boolean isGenerateHaulingIds() {
        return model.isGenerateHaulingIds();
    }

    protected boolean isCanGenerate() {
        return model.isCanGenerate();
    }

    @Override
    public void fireTableDataChanged() {

        // rearrange ids when data has changed
        rearrangeIds(false);

        super.fireTableDataChanged();

    }

    @Override
    public void fireTableRowsDeleted(int firstRow, int lastRow) {
        super.fireTableRowsDeleted(firstRow, lastRow);

        // must rearrange ids
        rearrangeIds(true);

    }

    @Override
    public void fireTableRowsInserted(int firstRow, int lastRow) {
        super.fireTableRowsInserted(firstRow, lastRow);

        // must rearrange ids
        rearrangeIds(true);

    }

    @Override
    public void removeSelectedRow() {

        super.removeSelectedRow();
        validate();
        setModified(true);

    }


    @Override
    public void setData(List<E> data) {

        super.setData(data);
        validate();

    }
}
