package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.storage.StorageUILauncher;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ChangeStorageAction extends AbstractAction {


    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeStorageAction.class);

    private final ObserveMainUI ui;

    private final Set<DbMode> dbModes;

    private final String title;

    public ChangeStorageAction(ObserveMainUI ui) {
        this(ui, null, null);
    }

    public ChangeStorageAction(ObserveMainUI ui, Set<DbMode> dbModes, String title) {

        super(t("observe.action.change.storage"), SwingUtil.getUIManagerActionIcon("db-change"));
        this.ui = ui;
        this.dbModes = dbModes == null ? EnumSet.noneOf(DbMode.class) : dbModes;
        this.title = title;
        putValue(SHORT_DESCRIPTION, t("observe.action.change.storage.tip"));
        putValue(MNEMONIC_KEY, (int) 'C');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        run();

    }

    public void run() {

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {

            if (log.isInfoEnabled()) {
                log.info("Start change storage with dbMode: " + dbModes.stream().map(DbMode::name).collect(Collectors.joining(", ")));
            }

            StorageUILauncher.changeStorage(ui, ui, dbModes, t(title));

        }

    }
}
