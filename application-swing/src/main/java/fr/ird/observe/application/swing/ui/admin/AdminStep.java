/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.config.SelectDataUI;
import fr.ird.observe.application.swing.ui.admin.consolidate.ConsolidateModel;
import fr.ird.observe.application.swing.ui.admin.consolidate.ConsolidateUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.DataSynchroModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.DataSynchroUI;
import fr.ird.observe.application.swing.ui.admin.export.ExportModel;
import fr.ird.observe.application.swing.ui.admin.export.ExportUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.application.swing.ui.admin.report.ReportModel;
import fr.ird.observe.application.swing.ui.admin.report.ReportUI;
import fr.ird.observe.application.swing.ui.admin.resume.ShowResumeUI;
import fr.ird.observe.application.swing.ui.admin.save.SaveLocalModel;
import fr.ird.observe.application.swing.ui.admin.save.SaveLocalUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy.SynchronizeUI;
import fr.ird.observe.application.swing.ui.admin.validate.ValidateModel;
import fr.ird.observe.application.swing.ui.admin.validate.ValidateUI;
import jaxx.runtime.swing.wizard.ext.WizardExtStep;
import jaxx.runtime.swing.wizard.ext.WizardExtUI;

import javax.swing.ImageIcon;
import java.lang.reflect.Constructor;
import java.util.EnumSet;

import static org.nuiton.i18n.I18n.n;

/**
 * Pour caractériser les étapes (correspond aux onglets de l'ui).
 *
 * Chaque constante représente un onglet de l'ui et est responsable de
 * l'instanciation des différents objets associés à un onglet, à savoir :
 * <ul>
 * <li>le model {@link #newModel()}</li>
 * <li>l'ui {@link #newUI(WizardExtUI)} </li>
 * </ul>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public enum AdminStep implements WizardExtStep {

    /** pour configurer les actions à réaliser et voir la progression */
    CONFIG(
            null,
            ConfigUI.class,
            true
    ),

    /** pour selectionner les donnes */
    SELECT_DATA(
            null,
            SelectDataUI.class,
            true
    ),

    /** pour résoudre les entités obosolètes */
    SYNCHRONIZE(
            n("observe.actions.synchro.referential.legacy"),
            n("observe.actions.synchro.referential.legacy.tip"),
            "synchronizeReferentiel",
            n("observe.actions.synchro.referential.legacy"),
            n("observe.actions.synchro.referential.legacy.description"),
            SynchronizeModel.class,
            SynchronizeUI.class,
            true,
            false,
            true,
            false,
            DbMode.USE_LOCAL,
            DbMode.USE_REMOTE,
            DbMode.USE_SERVER
    ),

    DATA_SYNCHRONIZE(
            n("observe.actions.synchro.data.title"),
            n("observe.actions.synchro.data.title.tip"),
            "dataSynchronize",
            n("observe.actions.synchro.data"),
            n("observe.actions.synchro.data.description"),
            DataSynchroModel.class,
            DataSynchroUI.class,
            true,
            false,
            false,
            false,
            DbMode.USE_LOCAL,
            DbMode.USE_REMOTE,
            DbMode.USE_SERVER
    ),

    REFERENTIAL_SYNCHRONIZE(
            n("observe.actions.synchro.referential.title"),
            n("observe.actions.synchro.referential.title.tip"),
            "referentialSynchronize",
            n("observe.actions.synchro.referential"),
            n("observe.actions.synchro.referential.description"),
            ReferentialSynchroModel.class,
            ReferentialSynchroUI.class,
            true,
            false,
            false,
            false,
            DbMode.USE_LOCAL,
            DbMode.USE_REMOTE,
            DbMode.USE_SERVER
    ),

    /** pour lancer la validation des donnees */
    VALIDATE(
            n("observe.actions.validate.title"),
            n("observe.actions.validate.title.tip"),
            "validate",
            n("observe.actions.validate"),
            n("observe.actions.validate.description"),
            ValidateModel.class,
            ValidateUI.class,
            false,
            true,
            false,
            false,
            DbMode.USE_LOCAL,
            DbMode.USE_REMOTE,
            DbMode.USE_SERVER
    ),

    /** pour lancer la consolidation des données (mesure taille-poids...) */
    CONSOLIDATE(
            n("observe.actions.consolidate.title"),
            n("observe.actions.consolidate.title.tip"),
            "consolidate",
            n("observe.actions.consolidate"),
            n("observe.actions.consolidate.description"),
            ConsolidateModel.class,
            ConsolidateUI.class,
            false,
            true,
            false,
            false,
            DbMode.USE_LOCAL,
            DbMode.USE_REMOTE,
            DbMode.USE_SERVER
    ),

    /** pour lancer la generation des rapports */
    REPORT(
            n("observe.actions.report.title"),
            n("observe.actions.report.title.tip"),
            "report",
            n("observe.actions.report"),
            n("observe.actions.report.description"),
            ReportModel.class,
            ReportUI.class,
            false,
            true,
            false,
            false,
            DbMode.USE_LOCAL,
            DbMode.USE_REMOTE,
            DbMode.USE_SERVER
    ),

    /** pour effectuer les modifications vers la base locale */
    SAVE_LOCAL(
            null,
            null,
            "save",
            n("observe.actions.saveLocal"),
            n("observe.actions.saveLocal.description"),
            SaveLocalModel.class,
            SaveLocalUI.class,
            false,
            false,
            false,
            false
    ),

    /** pour effectuer les modifications vers la base distante */
    EXPORT_DATA(
            n("observe.actions.exportData.title"),
            n("observe.actions.exportData.title.tip"),
            "remote-export",
            n("observe.actions.exportData"),
            n("observe.actions.exportData.description"),
            ExportModel.class,
            ExportUI.class,
            true,
            true,
            false,
            false,
            DbMode.USE_LOCAL,
            DbMode.USE_REMOTE,
            DbMode.USE_SERVER
    ),

    /** pour afficher après les opération un ecran de résumé */
    SHOW_RESUME(
            null,
            ShowResumeUI.class,
            false
    );

    private final String iconName;

    private final String title;

    private final String titleTip;

    private final String operationLabel;

    private final String operationDescription;

    private final Class<? extends AdminActionModel> modelClass;

    private final Class<? extends AdminTabUI> uiClass;

    private final DbMode[] incomingModes;

    private final boolean needReferentiel;

    private final boolean needSave;

    private final boolean needSelect;

    private final boolean config;

    private transient ImageIcon icon;

    AdminStep(Class<? extends AdminActionModel> modelClass,
              Class<? extends AdminTabUI> uiClass,
              boolean config) {
        this(null,
             null,
             null,
             null,
             null,
             modelClass,
             uiClass,
             false,
             false,
             false,
             config
        );
    }

    AdminStep(String title,
              String titleTip,
              String iconName,
              String operationLabel,
              String operationDescription,
              Class<? extends AdminActionModel> modelClass,
              Class<? extends AdminTabUI> uiClass,
              boolean needReferentiel,
              boolean needSelect,
              boolean needSave,
              boolean config,
              DbMode... incomingModes) {
        this.title = title;
        this.titleTip = titleTip;
        this.iconName = iconName;
        this.operationDescription = operationDescription;
        this.operationLabel = operationLabel;
        this.needReferentiel = needReferentiel;
        this.needSave = needSave;
        this.needSelect = needSelect;
        this.modelClass = modelClass;
        this.uiClass = uiClass;
        this.config = config;
        this.incomingModes = incomingModes;
    }

    public static EnumSet<AdminStep> getOperations() {
        EnumSet<AdminStep> operations = EnumSet.noneOf(AdminStep.class);
        for (AdminStep step : AdminStep.values()) {
            if (step.isOperation()) {
                operations.add(step);
            }
        }
        return operations;
    }

    public static AdminStep valueOfIgnoreCase(String value) {
        for (AdminStep step : AdminStep.values()) {
            if (step.name().equalsIgnoreCase(value)) {
                return step;
            }
        }
        return null;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleTip() {
        return titleTip;
    }

    public String getIconName() {
        return iconName;
    }

    public ImageIcon getIcon() {
        if (iconName != null && icon == null) {
            icon = (ImageIcon) UIHelper.getUIManagerActionIcon(iconName);
        }
        return icon;
    }

    @Override
    public String getLabel() {
        return I18nEnumHelper.getLabel(this);
    }

    @Override
    public String getDescription() {
        return I18nEnumHelper.getDescription(this);
    }

    @Override
    public String getOperationDescription() {
        return operationDescription;
    }

    @Override
    public String getOperationLabel() {
        return operationLabel;
    }

    @Override
    public boolean isOperation() {
        return operationLabel != null;
    }

    @Override
    public boolean isConfig() {
        return config;
    }

    public boolean isNeedSelect() {
        return needSelect;
    }

    @Override
    public Class<? extends AdminActionModel> getModelClass() {
        return modelClass;
    }

    @Override
    public Class<? extends AdminTabUI> getUiClass() {
        return uiClass;
    }

    @Override
    public AdminActionModel newModel() {
        if (!isOperation()) {
            throw new IllegalStateException("The current step [" + this + "] is not an operation.");
        }
        try {
            return getModelClass().newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("Could not init model : " + getModelClass(), e);
        }
    }

    @Override
    public AdminTabUI newUI(WizardExtUI<?, ?> ui) {
        try {
            Constructor<?> constructor = uiClass.getConstructor(AdminUI.class);
            Object tabPanelUI = constructor.newInstance(ui);
            return (AdminTabUI) tabPanelUI;
        } catch (Exception e) {
            throw new IllegalStateException("Could not init ui " + this, e);
        }
    }

    public boolean isNeedReferentiel() {
        return needReferentiel;
    }

    public boolean isNeedSave() {
        return needSave;
    }

    public DbMode[] getIncomingModes() {
        return incomingModes;
    }

    public boolean hasIncomingModes() {
        return incomingModes.length > 0;
    }
}
