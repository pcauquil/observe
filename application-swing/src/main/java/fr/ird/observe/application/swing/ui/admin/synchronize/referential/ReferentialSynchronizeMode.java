package fr.ird.observe.application.swing.ui.admin.synchronize.referential;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * Type de synchronisation de référentiel. Définit quelles bases sont en écriture.
 *
 * Created on 10/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public enum ReferentialSynchronizeMode {

    /**
     * La base de gauche est en lecture, la base de droite est en écriture.
     */
    FROM_LEFT_TO_RIGHT(false, true),
    /**
     * La base de gauche est en écriture, la base de droite est en lecture.
     */
    FROM_RIGHT_TO_LEFT(true, false),
    /**
     * La base de gauche est en écriture, la base de droite est en écriture.
     */
    BOTH(true, true);

    /**
     * Est ce que la source de gauche est en écriture ?
     */
    private final boolean leftWrite;
    /**
     * Est ce que la source de droite est en écriture ?
     */
    private final boolean rightWrite;

    ReferentialSynchronizeMode(boolean leftWrite, boolean rightWrite) {
        this.leftWrite = leftWrite;
        this.rightWrite = rightWrite;
    }

    public boolean isLeftWrite() {
        return leftWrite;
    }

    public boolean isRightWrite() {
        return rightWrite;
    }

}
