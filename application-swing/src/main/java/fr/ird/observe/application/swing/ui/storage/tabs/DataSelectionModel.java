/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage.tabs;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ProgramDtos;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialDtos;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.services.service.seine.TripSeineService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Un modèle pour représenter la sélection de données.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class DataSelectionModel implements Serializable {

    public static void populate(DataSelectionModel model, ObserveSwingDataSource source) {

        Preconditions.checkState(source.isOpen());

        Map<ReferentialReference<ProgramDto>, List<DataReference>> datas = Maps.newHashMap();

        TripSeineService tripSeineService = source.newTripSeineService();
        TripLonglineService tripLonglineService = source.newTripLonglineService();

        Set<ReferentialReference<ProgramDto>> programSet = source.getReferentialReferences(ProgramDto.class);

        for (ReferentialReference<ProgramDto> program : programSet) {

            List<DataReference> referenceDtos = Lists.newArrayList();

            if (ProgramDtos.isProgramSeine(program)) {

                DataReferenceSet<TripSeineDto> tripSeineRef = tripSeineService.getTripSeineByProgram(program.getId());
                referenceDtos.addAll(tripSeineRef.getReferences());

            } else if (ProgramDtos.isProgramLongline(program)) {

                DataReferenceSet<TripLonglineDto> tripLonglineRef = tripLonglineService.getTripLonglineByProgram(program.getId());
                referenceDtos.addAll(tripLonglineRef.getReferences());

            }

            populate(program, referenceDtos, datas);

        }
        model.setDatas(datas);

    }

    protected static void populate(ReferentialReference<ProgramDto> program,
                                   List<DataReference> trips,
                                   Map<ReferentialReference<ProgramDto>, List<DataReference>> datas) {

        if (!trips.isEmpty()) {

            datas.put(program, trips);
            if (log.isDebugEnabled()) {
                log.debug("Add program " + program.getPropertyValue(ProgramDto.PROPERTY_LABEL1) + " with " + trips.size() + " trip(s).");
            }


        }

    }

    public static final String PROPERTY_USE_REFERENTIEL = "useReferentiel";

    public static final String PROPERTY_USE_DATA = "useData";

    public static final String PROPERTY_USE_OPEN_DATA = "useOpenData";

    public static final String PROPERTY_SELECTED_DATA = "selectedData";

    public static final String PROPERTY_SELECTED_REFERENTIEL = "selectedReferentiel";

    public static final String PROPERTY_DATAS = "datas";

    /** Logger */
    private static final Log log = LogFactory.getLog(DataSelectionModel.class);

    private static final long serialVersionUID = 2L;

    /** un drapeau pour selectionner ou non les donnees observers */
    protected boolean useData;

    /** un drapeau pour selectionner ou non des donnees observers ouvertes */
    protected boolean useOpenData;

    /** un drapeau pour selectionner ou non des donnees observer */
    protected boolean useReferentiel;

    /** la liste des programs utilisables (qui ont des marees) */
    protected transient Map<ReferentialReference<ProgramDto>, List<DataReference>> datas;

    /** la liste des marees selectionnee */
    protected transient Set<DataReference> selectedData;

    /** la liste des referentiels possibles */
    protected final Set<Class<? extends ReferentialDto>> referentiel;

    /** la liste des referentiels selectionnes */
    protected final Set<Class<? extends ReferentialDto>> selectedReferentiel;

    /** nb marees */
    protected int nbTrips;

    /** nb referentiels */
    protected final int nbReferentiels;

    protected final PropertyChangeSupport pcs;

    public void populate() {
        throw new UnsupportedOperationException();
    }

    public DataSelectionModel() {
        pcs = new PropertyChangeSupport(this);
        referentiel = Sets.newHashSet(ReferentialDtos.REFERENCE_DTOS);
        selectedReferentiel = Sets.newHashSet();
        nbReferentiels = referentiel.size();
    }

    public boolean isUseReferentiel() {
        return useReferentiel;
    }

    public boolean isUseData() {
        return useData;
    }

    public boolean isUseOpenData() {
        return useOpenData;
    }

    public boolean isEmpty() {
        if (isUseReferentiel()) {
            if (!isReferentielEmpty()) {
                return false;
            }
        }
        if (isUseData()) {
            if (!isDataEmpty()) {
                return false;
            }
        }
        return true;
    }


    public boolean isReferentielEmpty() {
        return selectedReferentiel.isEmpty();
    }

    public boolean isReferentielFull() {
        return selectedReferentiel.size() == nbReferentiels;
    }

    public boolean isDataEmpty() {
        return selectedData == null || selectedData.isEmpty();
    }

    public boolean isDataFull() {
        return selectedData != null && selectedData.size() == nbTrips;
    }

    public int selectDataSize() {
        return selectedData == null ? 0 : selectedData.size();
    }

    public Set<Class<? extends ReferentialDto>> getSelectedReferentiel() {
        return selectedReferentiel;
    }

    public Map<ReferentialReference<ProgramDto>, List<DataReference>> getDatas() {
        if (datas == null) {
            datas = Maps.newHashMap();
        }
        return datas;
    }

    public boolean containsData(ReferentialReference<ProgramDto> program, DataReference trip) {
        List<DataReference> datas = getDatas(program.getId());
        return datas!=null && datas.contains(trip);
    }

    public Set<DataReference> getSelectedData() {
        if (selectedData == null) {
            selectedData = Sets.newHashSet();
        }
        return selectedData;
    }

    public Map<ReferentialReference<ProgramDto>, List<DataReference>> getSelectedDataByProgram() {
        if (datas == null || selectedData == null || selectedData.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<ReferentialReference<ProgramDto>, List<DataReference>> result = Maps.newHashMap();

        for (DataReference referenceDto : selectedData) {

            // on recherche son program
            for (ReferentialReference<ProgramDto> p : datas.keySet()) {
                if (datas.get(p).contains(referenceDto)) {
                    List<DataReference> referenceDtos = result.get(p);
                    if (referenceDtos == null) {
                        referenceDtos = Lists.newArrayList();
                        result.put(p, referenceDtos);
                    }
                    referenceDtos.add(referenceDto);
                    break;
                }
            }
        }
        return result;
    }

    public List<ReferentialReference<ProgramDto>> getSelectedProgram() {
        if (datas == null || selectedData == null || selectedData.isEmpty()) {
            return Collections.emptyList();
        }

        Set<ReferentialReference<ProgramDto>> result = Sets.newHashSet();

        for (DataReference referenceDto : selectedData) {

            // on recherche son program
            for (ReferentialReference<ProgramDto> p : datas.keySet()) {
                if (datas.get(p).contains(referenceDto)) {
                    result.add(p);
                    break;
                }
            }
        }

        return result.stream().collect(Collectors.toList());
    }

    public List<DataReference> getSelectedTripsByProgram(ReferentialReference<ProgramDto> program) {
        if (datas == null || selectedData == null || selectedData.isEmpty()) {
            return Collections.emptyList();
        }

        return datas.get(program).stream()
                    .filter(trip -> selectedData.contains(trip))
                    .collect(Collectors.toList());
    }


    public boolean isSelectedData(AbstractReference<?> referenceDto) {

        boolean result;

        if (ProgramDto.class.isAssignableFrom(referenceDto.getType())) {

            List<DataReference> trips = datas.get(referenceDto);
            result = CollectionUtils.isNotEmpty(trips);

            if (result) {
                for (DataReference trip : trips) {
                    if (!getSelectedData().contains(trip)) {

                        // au moins une marée non sélectionnée

                        result = false;
                    }
                }
            }
        } else {

            // recherche directe sur les ids de marees
            result = getSelectedData().contains(referenceDto);

        }

        // ne devrait pas arrivée
        return result;

    }

    public boolean isSelectedReferentiel(Class<?> type) {
        checkReferentielType(type);
        return getSelectedReferentiel().contains(type);
    }

    public void setUseReferentiel(boolean useReferentiel) {
        this.useReferentiel = useReferentiel;
        firePropertyChange(PROPERTY_USE_REFERENTIEL, useReferentiel);
    }

    public void setUseData(boolean useData) {
        this.useData = useData;
        firePropertyChange(PROPERTY_USE_DATA, useData);
    }

    public void setUseOpenData(boolean useOpenData) {
        this.useOpenData = useOpenData;
        firePropertyChange(PROPERTY_USE_OPEN_DATA, useOpenData);
    }

    public void addSelectedReferentiel(Class<? extends ReferentialDto> type) {
        checkReferentielType(type);
        if (log.isDebugEnabled()) {
            log.debug("Add referentiel type " + type);
        }
        getSelectedReferentiel().add(type);
        firePropertyChange(PROPERTY_SELECTED_REFERENTIEL, selectedReferentiel);
    }

    public void addAllSelectedReferentiel() {
        getSelectedReferentiel().addAll(referentiel);
        firePropertyChange(PROPERTY_SELECTED_REFERENTIEL, selectedReferentiel);
    }

    public void removeSelectedReferentiel(Class<?> type) {
        checkReferentielType(type);
        if (log.isDebugEnabled()) {
            log.debug("remove referentiel type " + type);
        }
        getSelectedReferentiel().remove(type);
        firePropertyChange(PROPERTY_SELECTED_REFERENTIEL, selectedReferentiel);
    }

    public void removeAll() {
        getSelectedReferentiel().clear();
        getSelectedData().clear();
        firePropertyChange(PROPERTY_SELECTED_REFERENTIEL, selectedReferentiel);
        firePropertyChange(PROPERTY_SELECTED_DATA, selectedData);
    }

    public void removeAllSelectedReferentiel() {
        getSelectedReferentiel().clear();
        firePropertyChange(PROPERTY_SELECTED_REFERENTIEL, selectedReferentiel);
    }

    public boolean isReferentielSelectAll(Collection<Class<? extends ReferentialDto>> classes) {
        return selectedReferentiel.containsAll(classes);
    }

    public void removeAllReferentiel(Collection<Class<? extends ReferentialDto>> classes) {
        selectedReferentiel.removeAll(classes);
        firePropertyChange(PROPERTY_SELECTED_REFERENTIEL, selectedReferentiel);
    }

    public void addAllReferentiel(Collection<Class<? extends ReferentialDto>> classes) {
        selectedReferentiel.addAll(classes);
        firePropertyChange(PROPERTY_SELECTED_REFERENTIEL, selectedReferentiel);
    }

    public void setDatas(Map<ReferentialReference<ProgramDto>, List<DataReference>> datas) {
        this.datas = datas;

        // on compte le count de marées totale
        int nbTrips = 0;
        if (datas != null) {
            for (List<DataReference> marees : datas.values()) {
                nbTrips += marees.size();
            }
        }
        this.nbTrips = nbTrips;

        if (log.isDebugEnabled()) {
            log.debug("Nb program registred : " + (datas == null ? 0 : datas.size()));
            log.debug("Nb trip registred    : " + this.nbTrips);
        }
        firePropertyChange(PROPERTY_DATAS, datas);
    }

    public void addSelectedData(AbstractReference referenceDto) {

        if (ProgramDto.class.isAssignableFrom(referenceDto.getType())) {

            // ajout de toutes les marées du program

            if (log.isDebugEnabled()) {
                log.debug("Add all trips of program " + referenceDto.getId());
            }
            getSelectedData().addAll(datas.get(referenceDto));

        } else {
            // ajout d'une marée
            if (log.isDebugEnabled()) {
                log.debug("Add Trip " + referenceDto.getId());
            }
            getSelectedData().add((DataReference) referenceDto);
        }
        firePropertyChange(PROPERTY_SELECTED_DATA, selectedData);
    }

    public void addAllSelectedData() {
        if (log.isDebugEnabled()) {
            log.debug("All all data.");
        }
        for (List<DataReference> p : datas.values()) {
            getSelectedData().addAll(p);
        }
        firePropertyChange(PROPERTY_SELECTED_DATA, selectedData);
    }

    public void removeSelectedData(AbstractReference referenceDto) {

        if (ProgramDto.class.isAssignableFrom(referenceDto.getType())) {

            // ajout de toutes les marées du program

            if (log.isDebugEnabled()) {
                log.debug("Remove all marees of program " + referenceDto);
            }
            getSelectedData().removeAll(datas.get(referenceDto));
        } else {

            // ajout d'une marée
            if (log.isDebugEnabled()) {
                log.debug("Remove maree " + referenceDto);
            }
            getSelectedData().remove(referenceDto);
        }
        firePropertyChange(PROPERTY_SELECTED_DATA, selectedData);
    }

    public void removeAllSelectedData() {
        if (selectedData != null) {
            selectedData.clear();
        }
        firePropertyChange(PROPERTY_SELECTED_DATA, selectedData);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        pcs.firePropertyChange(propertyName, null, newValue);
    }

    protected void firePropertyChange(String propertyName,
                                      Object oldValue,
                                      Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected void checkReferentielType(Class<?> type) {
        if (!referentiel.contains(type)) {
            throw new IllegalArgumentException(
                    "given <" + type + "> is not in referentiel universe : " +
                            referentiel);
        }
    }

    public void destroy() {
        removeAll();
        // suppression de tous les listeners
        PropertyChangeListener[] listeners = pcs.getPropertyChangeListeners();
        for (PropertyChangeListener l : listeners) {
            removePropertyChangeListener(l);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        destroy();
    }

    public List<DataReference> getDatas(String programId) {
        Optional<ReferentialReference<ProgramDto>> programRefOptional = getDatas().keySet().stream().filter(ReferentialReference.newIdPredicate(programId)).findFirst();

        List<DataReference> result;

        if (programRefOptional.isPresent()) {

            result = getDatas().get(programRefOptional.get());

        } else {

            result = Collections.emptyList();

        }

        return result;
    }

    public void addData(ReferentialReference<ProgramDto> program, DataReference trip) {
        if (!getDatas().containsKey(program)) {
            getDatas().put(program, new ArrayList<>());
        }
        getDatas(program.getId()).add(trip);
        firePropertyChange(PROPERTY_DATAS, getDatas());
    }

    public Set<ReferentialReference<ProgramDto>> getPrograms() {
        return getDatas().keySet();
    }

    public void removeTrip(ReferentialReference<ProgramDto> program, DataReference trip) {
        List<DataReference> trips = getDatas(program.getId());
        trips.remove(trip);
        if (trips.isEmpty()) {
            getDatas().remove(program);
        }
    }
}
