<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI superGenericType='PersonDto'>

  <style source="ReferenceEntity.jcss"/>

  <import>
    fr.ird.observe.services.dto.constants.ReferenceStatus
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.PersonDto
    fr.ird.observe.services.dto.referential.CountryDto
    fr.ird.observe.application.swing.ui.UIHelper
    fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel

    javax.swing.JTable
    javax.swing.JScrollPane
    javax.swing.table.TableCellRenderer

    jaxx.runtime.swing.editor.bean.BeanComboBox

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true'
                 beanClass='fr.ird.observe.services.dto.referential.PersonDto'
                 context='ui-create'
                 errorTableModel='{getErrorTableModel()}'/>

  <!-- model -->
  <PersonUIModel id='model'/>

  <!-- edit bean -->
  <PersonDto id='bean'/>

  <script><![CDATA[
@Override
public void decorateUniqueKeyTable(JTable table,
                                   TableCellRenderer renderer,
                                   JScrollPane pane) {
    UIHelper.fixTableColumnWidth(table, 1, 120);
}
]]></script>

  <Table id='editTable'>

    <!-- uri -->
    <row>
      <cell anchor="west">
        <JLabel id='uriLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='uri' onKeyReleased='getBean().setUri(uri.getText())'/>
      </cell>
    </row>

    <!-- code / status -->
    <row>
      <cell anchor="west">
        <JLabel id='codeStatusLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JPanel id='codeStatusPanel' layout='{new BorderLayout()}'>
          <JTextField id='code' constraints='BorderLayout.WEST'
                      onKeyReleased='getBean().setCode(code.getText())'/>
          <EnumEditor id='status' constraints='BorderLayout.CENTER'
                      constructorParams='ReferenceStatus.class'
                      genericType='ReferenceStatus'
                      onItemStateChanged='getBean().setStatus((ReferenceStatus)status.getSelectedItem())'/>
        </JPanel>
      </cell>
    </row>

    <!-- lastName -->
    <row>
      <cell anchor="west">
        <JLabel id='lastNameLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='lastName'
                    onKeyReleased='getBean().setLastName(lastName.getText())'/>
      </cell>
    </row>

    <!-- firstName -->
    <row>
      <cell anchor="west">
        <JLabel id='firstNameLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='firstName'
                    onKeyReleased='getBean().setFirstName(firstName.getText())'/>
      </cell>
    </row>

    <!-- country -->
    <row>
      <cell anchor='west'>
        <JLabel id='countryLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <BeanComboBox id='country' constructorParams='this' genericType='ReferentialReference&lt;CountryDto&gt;' _entityClass='CountryDto.class'/>
      </cell>
    </row>

    <!-- needComment -->
    <row>
      <cell anchor='east' weightx="1" fill="both" columns="2">
        <JCheckBox id='needComment'
                   onItemStateChanged='getBean().setNeedComment(needComment.isSelected())'/>
      </cell>
    </row>

    <!-- is observer -->
    <row>
      <cell columns="2">
        <JCheckBox id='observer'
                   onStateChanged='getBean().setObserver(observer.isSelected())'/>
      </cell>
    </row>

    <!-- is captain -->
    <row>
      <cell columns="2">
        <JCheckBox id='captain'
                   onStateChanged='getBean().setCaptain(captain.isSelected())'/>
      </cell>
    </row>

    <!-- is dataEntryOperator -->
    <row>
      <cell columns="2">
        <JCheckBox id='dataEntryOperator'
                   onStateChanged='getBean().setDataEntryOperator(dataEntryOperator.isSelected())'/>
      </cell>
    </row>
  </Table>
</fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI>
