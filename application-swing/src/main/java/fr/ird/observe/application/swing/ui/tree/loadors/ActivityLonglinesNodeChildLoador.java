package fr.ird.observe.application.swing.ui.tree.loadors;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.tree.ActivityLonglineNode;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import jaxx.runtime.swing.nav.NavDataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created on 8/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class ActivityLonglinesNodeChildLoador extends AbstractDataReferenceChildLoador<ActivityLonglineDto> {

    private static final long serialVersionUID = 1L;

    public ActivityLonglinesNodeChildLoador() {
        super(ActivityLonglineDto.class);
    }

    @Override
    public List<DataReference<ActivityLonglineDto>> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) throws Exception {
        ActivityLonglineService activityLonglineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivityLonglineService();
        DataReferenceSet<ActivityLonglineDto> activityLonglineByTripLongline = activityLonglineService.getActivityLonglineByTripLongline(parentId);
        return new ArrayList<>(activityLonglineByTripLongline.getReferences());
    }

    @Override
    public ObserveNode createNode(DataReference<ActivityLonglineDto> data, NavDataProvider dataProvider) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        return new ActivityLonglineNode(data);
    }
}
