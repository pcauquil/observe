/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

import java.awt.Window;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Les différents types d'actions d'administration possible sur une base
 * distante.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public enum ObstunaAdminAction {

    /**
     * pour créer une nouvelle base obstuna.
     *
     * Il faut au préalable avoir exécuté le script {@code create-obstuna_v2.sh}.
     */
    CREATE() {
        @Override
        public RemoteUILauncher newLauncher(JAXXContext context, Window frame) {
            return new RemoteUILauncher(this, context, frame, I18nEnumHelper.getLabel(this)) {

                protected DataSourceCreateConfigurationDto createConfigurationDto;
                protected ObserveSwingDataSource dataSource;
                protected Set<ObserveDbUserDto> users;

                @Override
                protected void init(StorageUI ui) {
                    StorageUIModel model = ui.getModel();

                    // on autorise le mode de creation import referentiel
                    // depuis une source distante
                    model.getPgConfig().setCanMigrate(true);
                    super.init(ui);
                }

                protected void checkImportDbVersion(StorageUIModel model, ObserveDataSourceConfiguration dataSourceConfig) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

                    ObserveSwingDataSource importDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(dataSourceConfig);

                    Preconditions.checkState(importDataSource != null, "Can't select data on a null dataSource");

                    try {

                        importDataSource.open();

                        model.checkImportDbVersion(importDataSource);

                    } finally {

                        importDataSource.close();
                    }

                }

                protected void initTask(StorageUIModel model) throws Exception {

                    if (DbMode.USE_REMOTE.equals(model.getDbMode())) {
                        ObserveDataSourceConfigurationTopiaPG pgConfig = model.getPgConfig();

                        // pas autorise a migrer automatiquement
                        pgConfig.setCanMigrate(false);

                        dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(pgConfig);
                    } else {
                        ObserveDataSourceConfigurationRest restConfig = model.getRestConfig();

                        dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(restConfig);
                    }

                    createConfigurationDto = model.toImportReferentielSourceConfig();

                    ObserveDataSourceConfiguration importReferentialConfig = createConfigurationDto.getImportReferentialDataSourceConfiguration();


                    if (importReferentialConfig != null) {

                        if (log.isInfoEnabled()) {
                            log.info("Use referentiel import data source " + importReferentialConfig.getLabel());
                        }

                        checkImportDbVersion(model, importReferentialConfig);

                    }

                    // data import

                    ObserveSwingDataSource importDataConfig = model.toImportDataSourceConfig();

                    if (importDataConfig != null) {

                        if (log.isInfoEnabled()) {
                            log.info("Use data import data source " + importDataConfig.getLabel());
                        }

                        checkImportDbVersion(model, importDataConfig.getConfiguration());

                        DataSelectionModel dataModel = model.getSelectDataModel();

                        ImmutableSet<String> importDataIds = ImmutableSet.copyOf(
                                dataModel.getSelectedData()
                                         .stream()
                                         .map(DataReference::getId)
                                         .collect(Collectors.toSet()));

                        createConfigurationDto.setImportDataConfiguration(importDataConfig.getConfiguration(), importDataIds);
                    }

                    users = model.getSecurityModel().getUsers();

                    super.initTask(model);
                }

                @Override
                protected String getPgLabel() {
                    return t("observe.storage.label.db.to.create");
                }

                @Override
                protected void execute() throws Exception {
                    if (log.isInfoEnabled()) {
                        log.info("Create db...");
                    }

                    try {
                        dataSource.create(createConfigurationDto);

                        if (log.isInfoEnabled()) {
                            log.info("Open [" + dataSource.getLabel() + "] and create it.");
                        }

                    } finally {
                        if (dataSource.isOpen()) {
                            dataSource.close();
                        }
                    }
                }

                @Override
                protected void applySecurity() {
                    if (log.isInfoEnabled()) {
                        log.info("Apply security...");
                    }
                    dataSource.applySecurity(users);
                }
            };
        }
    },
    /**
     * pour mettre à jour une base distante.
     *
     * Il faut au préalable avoir une base en version {@code 1.0}.
     */
    UPDATE() {
        @Override
        public RemoteUILauncher newLauncher(JAXXContext context, Window frame) {
            return new RemoteUILauncher(this, context, frame, I18nEnumHelper.getLabel(this)) {

                protected ObserveSwingDataSource dataSource;
                protected ObserveDataSourceInformation dataSourceInformation;
                protected Version targetVersion;
                protected Set<ObserveDbUserDto> users;

                @Override
                protected void initTask(StorageUIModel model) throws Exception {

                    if (DbMode.USE_REMOTE.equals(model.getDbMode())) {
                        ObserveDataSourceConfigurationTopiaPG pgConfig = model.getPgConfig();

                        dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(pgConfig);

                    } else {

                        ObserveDataSourceConfigurationRest restConfig = model.getRestConfig();

                        dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(restConfig);
                    }

                    targetVersion = ObserveSwingApplicationContext.get().getConfig().getModelVersion();

                    users = model.getSecurityModel().getUsers();

                    dataSourceInformation = model.getDataSourceInformation();

                }

                @Override
                protected void execute() throws Exception {

                    dataSource.migrateData(dataSourceInformation, targetVersion);
                }

                @Override
                protected String getPgLabel() {
                    return t("observe.storage.label.db.to.update");
                }

                @Override
                protected void applySecurity() {
                    if (log.isInfoEnabled()) {
                        log.info("Apply security...");
                    }
                    dataSource.applySecurity(users);
                }
            };
        }
    },
    /**
     * pour mettre à jour la sécurité d'une base obstuna.
     *
     * Il faut au préalable avoir une base en version {@code 1.4}.
     */
    SECURITY() {
        @Override
        public RemoteUILauncher newLauncher(JAXXContext context, Window frame) {
            return new RemoteUILauncher(this, context, frame, I18nEnumHelper.getLabel(this)) {

                protected ObserveSwingDataSource dataSource;
                protected Set<ObserveDbUserDto> users;

                @Override
                protected void initTask(StorageUIModel model) throws Exception {

                    if (DbMode.USE_REMOTE.equals(model.getDbMode())) {
                        ObserveDataSourceConfigurationTopiaPG pgConfig = model.getPgConfig();

                        // pas autorise a migrer automatiquement
                        pgConfig.setCanMigrate(false);

                        dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(pgConfig);
                    } else {
                        ObserveDataSourceConfigurationRest restConfig = model.getRestConfig();

                        dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(restConfig);
                    }

                    users = model.getSecurityModel().getUsers();

                }

                @Override
                protected String getPgLabel() {
                    return t("observe.storage.label.db.to.update.security");
                }

                @Override
                protected void applySecurity() {
                    if (log.isInfoEnabled()) {
                        log.info("Apply security...");
                    }
                    dataSource.applySecurity(users);
                }
            };
        }
    };

    /** Logger */
    private static final Log log = LogFactory.getLog(ObstunaAdminAction.class);

    public static ObstunaAdminAction valueOfIgnoreCase(String value) {
        for (ObstunaAdminAction step : ObstunaAdminAction.values()) {
            if (step.name().equalsIgnoreCase(value)) {
                return step;
            }
        }
        return null;
    }

    public abstract RemoteUILauncher newLauncher(JAXXContext context, Window frame);

    @Override
    public String toString() {
        return I18nEnumHelper.getLabel(this);
    }
}
