/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.validation;

import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import fr.ird.observe.services.service.longline.SetLonglineService;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.services.service.seine.ActivitySeineService;
import fr.ird.observe.services.service.seine.FloatingObjectService;
import fr.ird.observe.services.service.seine.RouteService;
import fr.ird.observe.services.service.seine.SetSeineService;
import fr.ird.observe.services.service.seine.TripSeineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Contient les objets en cours de validation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ValidationContext {

    /** Logger */
    static private final Log log = LogFactory.getLog(ValidationContext.class);

    public static final String VALIDATION_TRANSACTION_NAME = "validation";

    private static final DtoSupplier<TripSeineDto> TRIP_SEINE_DTO_SUPPLIER = new DtoSupplier<TripSeineDto>() {

        @Override
        public TripSeineDto get(ObserveSwingDataSource dataSource, String id) {
            TripSeineService service = dataSource.newTripSeineService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<RouteDto> ROUTE_DTO_SUPPLIER = new DtoSupplier<RouteDto>() {

        @Override
        public RouteDto get(ObserveSwingDataSource dataSource, String id) {
            RouteService service = dataSource.newRouteService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<ActivitySeineDto> ACTIVITY_SEINE_DTO_SUPPLIER = new DtoSupplier<ActivitySeineDto>() {

        @Override
        public ActivitySeineDto get(ObserveSwingDataSource dataSource, String id) {
            ActivitySeineService service = dataSource.newActivitySeineService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<TripLonglineDto> TRIP_LONGLINE_DTO_SUPPLIER = new DtoSupplier<TripLonglineDto>() {

        @Override
        public TripLonglineDto get(ObserveSwingDataSource dataSource, String id) {
            TripLonglineService service = dataSource.newTripLonglineService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<ActivityLonglineDto> ACTIVITY_LONGLINE_DTO_SUPPLIER = new DtoSupplier<ActivityLonglineDto>() {

        @Override
        public ActivityLonglineDto get(ObserveSwingDataSource dataSource, String id) {
            ActivityLonglineService service = dataSource.newActivityLonglineService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<SetSeineDto> SET_SEINE_DTO_SUPPLIER = new DtoSupplier<SetSeineDto>() {

        @Override
        public SetSeineDto get(ObserveSwingDataSource dataSource, String id) {
            SetSeineService service = dataSource.newSetSeineService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<SetLonglineDto> SET_LONGLINE_DTO_SUPPLIER = new DtoSupplier<SetLonglineDto>() {

        @Override
        public SetLonglineDto get(ObserveSwingDataSource dataSource, String id) {
            SetLonglineService service = dataSource.newSetLonglineService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<FloatingObjectDto> FLOATING_OBJECT_DTO_SUPPLIER = new DtoSupplier<FloatingObjectDto>() {

        @Override
        public FloatingObjectDto get(ObserveSwingDataSource dataSource, String id) {
            FloatingObjectService service = dataSource.newFloatingObjectService();
            return service.loadDto(id);
        }
    };

    private static final DtoSupplier<SpeciesDto> SPECIES_DTO_SUPPLIER = new DtoSupplier<SpeciesDto>() {

        @Override
        public SpeciesDto get(ObserveSwingDataSource dataSource, String id) {
            ReferentialService service = dataSource.newReferentialService();
            return service.loadSpecies(id);
        }
    };

    protected final DataContext dataContext;

    protected Map<String, Object> cache;

    private List<?> referentielList;

    public Map<String, Object> getCache() {
        if (cache == null) {
            cache = new TreeMap<>();
        }
        return cache;
    }

    public void cleanCache() {
        getCache().clear();
        referentielList = null;
    }

    public ValidationContext(DataContext dataContext) {
        this.dataContext = dataContext;
    }

    public DecoratorService getDecoratorService() {
        return ObserveSwingApplicationContext.get().getDecoratorService();
    }

    public ObserveOpenDataManager getOpenDataManager() {
        return ObserveSwingApplicationContext.get().getOpenDataManager();
    }

    public DataContext getDataContext() {
        return dataContext;
    }

    public TripSeineDto getCurrentTripSeine() {
        String tripSeineId = dataContext.getSelectedTripSeineId();
        TripSeineDto result = null;
        if (tripSeineId != null) {
            result = getDto(TRIP_SEINE_DTO_SUPPLIER, tripSeineId);
        }
        return result;
    }

    public TripLonglineDto getCurrentTripLongline() {
        String tripLonglineId = dataContext.getSelectedTripLonglineId();
        TripLonglineDto result = null;
        if (tripLonglineId != null) {
            result = getDto(TRIP_LONGLINE_DTO_SUPPLIER, tripLonglineId);
        }
        return result;
    }

    public RouteDto getCurrentRoute() {
        String routeId = dataContext.getSelectedRouteId();
        RouteDto result = null;
        if (routeId != null) {
            result = getDto(ROUTE_DTO_SUPPLIER, routeId);
        }
        return result;
    }

    public ActivitySeineDto getCurrentActivitySeine() {
        String activitySeineId = dataContext.getSelectedActivitySeineId();
        ActivitySeineDto result = null;
        if (activitySeineId != null) {
            result = getDto(ACTIVITY_SEINE_DTO_SUPPLIER, activitySeineId);
        }
        return result;
    }

    public ActivityLonglineDto getCurrentActivityLongline() {
        String activityLonglineId = dataContext.getSelectedActivityLonglineId();
        ActivityLonglineDto result = null;
        if (activityLonglineId != null) {
            result = getDto(ACTIVITY_LONGLINE_DTO_SUPPLIER, activityLonglineId);
        }
        return result;
    }

    public SetSeineDto getCurrentSetSeine() {
        String setSeineId = dataContext.getSelectedSetSeineId();
        SetSeineDto result = null;
        if (setSeineId != null) {
            result = getDto(SET_SEINE_DTO_SUPPLIER, setSeineId);
        }
        return result;
    }

    public SetLonglineDto getCurrentSetLongline() {
        String setLonglineId = dataContext.getSelectedSetLonglineId();
        SetLonglineDto result = null;
        if (setLonglineId != null) {
            result = getDto(SET_LONGLINE_DTO_SUPPLIER, setLonglineId);
        }
        return result;
    }

    public FloatingObjectDto getCurrentFloatingObject() {
        String floatingObjectId = dataContext.getSelectedFloatingObjectId();
        FloatingObjectDto result = null;
        if (floatingObjectId != null) {
            result = getDto(FLOATING_OBJECT_DTO_SUPPLIER, floatingObjectId);
        }
        return result;
    }

    public SpeciesDto getSpecies(String speciesId) {
        SpeciesDto result = null;
        if (speciesId != null) {
            result = getDto(SPECIES_DTO_SUPPLIER, speciesId);
        }
        return result;
    }

    public List<?> getEditingReferentielList() {
        return referentielList;
    }

    public void setEditingReferentielList(List<?> referentielList) {
        if (log.isDebugEnabled()) {
            log.debug("Add referentielList (size : " + (referentielList == null ? 0 : referentielList.size()) + ")");
        }
        this.referentielList = referentielList;
    }

    protected <D extends IdDto> D getDto(DtoSupplier<D> dtoSupplier, String id) {
        Object o = getCache().get(id);
        if (o != null) {
            if (log.isDebugEnabled()) {
                log.debug("Use cached entity : " + id);
            }
            // found in cache
            return (D) o;
        }

        D result = dtoSupplier.get(id);

        if (result != null) {
            if (log.isInfoEnabled()) {
                log.info("Put entity into cache : " + id);
            }
            getCache().put(id, result);

        }

        return result;
    }

    protected static abstract class DtoSupplier<D extends IdDto> {

        public final D get(String id) {
            ObserveSwingDataSource dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
            return get(dataSource, id);
        }

        public abstract D get(ObserveSwingDataSource dataSource, String id);

    }
}
