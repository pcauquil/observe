/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree.loadors;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.TripSeineNode;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.seine.GearUseFeaturesSeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.seine.TripSeineService;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.tree.NavTreeBridge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Le chargeur des noeuds fils d'un program (les marees du programme).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ProgramSeineNodeChildLoador extends AbstractNodeChildLoador<DataReference<TripSeineDto>, TripSeineDto> {

    private static final long serialVersionUID = 1L;

    private boolean addChilds;

    public ProgramSeineNodeChildLoador() {
        super(TripSeineDto.class);
    }

    @Override
    public void loadChilds(NavTreeBridge<ObserveNode> model, ObserveNode parentNode, NavDataProvider dataProvider) throws Exception {
        DataSelectionModel selectionModel = getSelectionModel(dataProvider);
        addChilds = selectionModel == null;

        try {
            super.loadChilds(model, parentNode, dataProvider);
        } finally {
            addChilds = true;
        }
    }

    @Override
    public List<DataReference<TripSeineDto>> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) throws Exception {

        DataSelectionModel selectionModel = getSelectionModel(dataProvider);

        List<DataReference<TripSeineDto>> result;

        if (selectionModel != null) {

            result = new ArrayList<>((Collection) selectionModel.getDatas(parentId));

        } else {

            TripSeineService tripSeineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
            DataReferenceSet<TripSeineDto> tripSeineByProgram = tripSeineService.getTripSeineByProgram(parentId);

            result = new ArrayList<>(tripSeineByProgram.getReferences());

        }
        return result;

    }

    @Override
    public ObserveNode createNode(DataReference<TripSeineDto> data, NavDataProvider dataProvider) {

        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");

        ObserveNode result = new TripSeineNode(data);

        if (addChilds) {
            result.add(createPluralizeStringNode(GearUseFeaturesSeineDto.class, null));
            result.add(createPluralizeStringNode(RouteDto.class, RoutesNodeChildLoador.class));
        }

        return result;

    }

    public boolean isAddChilds() {
        return addChilds;
    }

    public void setAddChilds(boolean addChilds) {
        this.addChilds = addChilds;
    }
}
