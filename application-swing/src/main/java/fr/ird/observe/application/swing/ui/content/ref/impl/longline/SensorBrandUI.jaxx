<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI superGenericType='SensorBrandDto'>

  <style source="../ReferenceEntity.jcss"/>
  <style source="../I18nReferenceEntity.jcss"/>

  <import>
    fr.ird.observe.services.dto.constants.ReferenceStatus
    fr.ird.observe.services.dto.referential.longline.SensorBrandDto

    fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true'
                 beanClass='fr.ird.observe.services.dto.referential.longline.SensorBrandDto'
                 context='ui-create'
                 errorTableModel='{getErrorTableModel()}'/>

  <!-- model -->
  <SensorBrandUIModel id='model'/>

  <!-- edit bean -->
  <SensorBrandDto id='bean'/>

  <Table id='editTable'>

    <!-- uri -->
    <row>
      <cell anchor="west">
        <JLabel id='uriLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='uri' onKeyReleased='getBean().setUri(uri.getText())'/>
      </cell>
    </row>

    <!-- code / status -->
    <row>
      <cell anchor="west">
        <JLabel id='codeStatusLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JPanel id='codeStatusPanel' layout='{new BorderLayout()}'>
          <JTextField id='code' constraints='BorderLayout.WEST'
                      onKeyReleased='getBean().setCode(code.getText())'/>
          <EnumEditor id='status' constraints='BorderLayout.CENTER'
                      constructorParams='ReferenceStatus.class'
                      genericType='ReferenceStatus'
                      onItemStateChanged='getBean().setStatus((ReferenceStatus)status.getSelectedItem())'/>
        </JPanel>
      </cell>
    </row>

    <!-- brandName -->
    <row>
      <cell anchor="west">
        <JLabel id='brandNameLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='brandName'
                    onKeyReleased='getBean().setBrandName(brandName.getText())'/>
      </cell>
    </row>

    <!-- needComment -->
    <row>
      <cell anchor='east' weightx="1" fill="both" columns="2">
        <JCheckBox id='needComment'
                   onItemStateChanged='getBean().setNeedComment(needComment.isSelected())'/>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI>
