package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.ActivityLonglineEncounterDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.EncounterDtos;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class EncounterUIModel extends ContentTableUIModel<ActivityLonglineEncounterDto, EncounterDto> {

    private static final long serialVersionUID = 1L;

    public EncounterUIModel(EncounterUI ui) {
        super(ActivityLonglineEncounterDto.class,
              EncounterDto.class,
              new String[]{
                      ActivityLonglineEncounterDto.PROPERTY_ENCOUNTER,
                      ActivityLonglineEncounterDto.PROPERTY_COMMENT},
              new String[]{
                      EncounterDto.PROPERTY_SPECIES,
                      EncounterDto.PROPERTY_DISTANCE,
                      EncounterDto.PROPERTY_COUNT,
                      EncounterDto.PROPERTY_ENCOUNTER_TYPE});

        List<ContentTableMeta<EncounterDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(EncounterDto.class, EncounterDto.PROPERTY_ENCOUNTER_TYPE, false),
                ContentTableModel.newTableMeta(EncounterDto.class, EncounterDto.PROPERTY_SPECIES, false),
                ContentTableModel.newTableMeta(EncounterDto.class, EncounterDto.PROPERTY_DISTANCE, false),
                ContentTableModel.newTableMeta(EncounterDto.class, EncounterDto.PROPERTY_COUNT, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<ActivityLonglineEncounterDto, EncounterDto> createTableModel(
            ObserveContentTableUI<ActivityLonglineEncounterDto, EncounterDto> ui,
            List<ContentTableMeta<EncounterDto>> contentTableMetas) {
        return new ContentTableModel<ActivityLonglineEncounterDto, EncounterDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Collection<EncounterDto> getChilds(ActivityLonglineEncounterDto bean) {
                return bean.getEncounter();
            }

            @Override
            protected void load(EncounterDto source, EncounterDto target) {
                EncounterDtos.copyEncounterDto(source, target);
            }

            @Override
            protected void setChilds(ActivityLonglineEncounterDto parent, List<EncounterDto> childs) {
                parent.setEncounter(childs);
            }
        };
    }
}
