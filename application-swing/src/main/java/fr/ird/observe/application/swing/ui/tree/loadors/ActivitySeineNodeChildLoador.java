/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree.loadors;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.tree.ActivitySeineNode;
import fr.ird.observe.application.swing.ui.tree.FloatingObjectSeineNode;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.SetSeineNode;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.referential.seine.ObservedSystemDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.service.seine.FloatingObjectService;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.tree.NavTreeBridge;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Le chargeur des noeuds de marees.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ActivitySeineNodeChildLoador extends AbstractDataReferenceChildLoador<FloatingObjectDto> {

    private static final long serialVersionUID = 1L;

    public ActivitySeineNodeChildLoador() {
        super(FloatingObjectDto.class);
    }

    @Override
    public void loadChilds(NavTreeBridge<ObserveNode> model, ObserveNode parentNode, NavDataProvider dataProvider) throws Exception {

        ObserveNode containerNode = parentNode.getContainerNode();

        if (containerNode == null) {
            throw new IllegalStateException(
                    "Could not find containerNode of " + parentNode);
        }

        // Creation d'un node systeme observe
        ObserveNode child = createPluralizeStringNode(ObservedSystemDto.class, null);

        parentNode.add(child);

        DataReference<ActivitySeineDto> activitySeineRef = ((ActivitySeineNode) parentNode).getEntity();
        DataReference<SetSeineDto> setSeineRef = (DataReference) activitySeineRef.getPropertyValue(ActivitySeineDto.PROPERTY_SET_SEINE);
        if (setSeineRef != null) {
            parentNode.add(createSetNode(setSeineRef));
        }

        // ajout des objets flottants
        super.loadChilds(model, parentNode, dataProvider);
    }

    public ObserveNode createSetNode(DataReference<SetSeineDto> data) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        return new SetSeineNode(data);
    }

    @Override
    public ObserveNode createNode(DataReference<FloatingObjectDto> data, NavDataProvider dataProvider) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        return new FloatingObjectSeineNode(data);
    }

    @Override
    public List<DataReference<FloatingObjectDto>> getData(Class<?> parentClass,
                                                          String parentId,
                                                          NavDataProvider dataProvider) throws Exception {

        FloatingObjectService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newFloatingObjectService();
        DataReferenceSet<FloatingObjectDto> floatingObjectByActivitySeine = service.getFloatingObjectByActivitySeine(parentId);
        return new ArrayList<>(floatingObjectByActivitySeine.getReferences());
    }

}
