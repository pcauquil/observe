/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDtos;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDtos;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDtos;
import fr.ird.observe.services.service.seine.TargetCatchService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import jaxx.runtime.swing.editor.bean.BeanComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TargetCatchUIHandler extends ContentTableUIHandler<SetSeineTargetCatchDto, TargetCatchDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(TargetCatchUIHandler.class);

    public TargetCatchUIHandler(TargetCatchUI ui) {
        super(ui, DataContextType.SetSeine);
    }

    @Override
    public TargetCatchUI getUi() {
        return (TargetCatchUI) super.getUi();
    }

    @Override
    public void initUI() {
        super.initUI();

        // lors de la modification d'une species (sur une entree non sauvee)
        //   - on recalcule la liste des categories pour cette species.
        //   - on reinitialiser la categorie selectionnee

        getUi().getSpecies().addPropertyChangeListener(
                BeanComboBox.PROPERTY_SELECTED_ITEM,
                evt -> {

                    ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> model = getTableModel();
                    if (model.isNewRow()) {

                        // en mode creation , on doit recalculer la liste des categories
                        ReferentialReference<SpeciesDto> species = (ReferentialReference<SpeciesDto>) evt.getNewValue();
                        onSpeciesChanged(species);

                    }
                }
        );
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @SuppressWarnings("Duplicates")
    @Override
    protected boolean prepareSave(SetSeineTargetCatchDto bean, List<TargetCatchDto> objets) {

        // on cherche si parmis les captures supprimer certaines ont des échantillons
        SetSeineTargetCatchDto originalSetSeineTargetCatchDto = getModel().getForm().getObject();
        SetSeineTargetCatchDto currentSetSeineTargetCatch = getModel().getBean();

        Set<ReferentialReference<SpeciesDto>> speciesToDelete = Sets.newHashSet();

        for (TargetCatchDto targetCatchDto : originalSetSeineTargetCatchDto.getTargetCatch()) {

            if (targetCatchDto.isHasSample()) {

                ReferentialReference<SpeciesDto> species = targetCatchDto.getSpecies();

                Iterable<TargetCatchDto> currentTargetCatchSpecies = TargetCatchDtos.filterBySpecies(currentSetSeineTargetCatch.getTargetCatch(), species);

                if (Iterables.isEmpty(currentTargetCatchSpecies)) {

                    speciesToDelete.add(species);

                }
            }

        }

        if (!speciesToDelete.isEmpty()) {

            // il existe des echantillons thon a supprimer on demande une confirmation
            StringBuilder sb = new StringBuilder(512);
            Decorator<ReferentialReference<SpeciesDto>> decorator = getReferentialReferenceDecorator(SpeciesDto.class);

            for (ReferentialReference<SpeciesDto> species : speciesToDelete) {
                sb.append("  - ").append(decorator.toString(species)).append('\n');
            }
            int reponse = UIHelper.askUser(
                    t("observe.title.need.confirm"),
                    t("observe.content.targetDiscarded.message.table.will.delete.targetLength", sb.toString()),
                    JOptionPane.ERROR_MESSAGE,
                    new Object[]{
                            t("observe.choice.continue"),
                            t("observe.choice.cancel")},
                    0);
            if (log.isDebugEnabled()) {
                log.debug("response : " + reponse);
            }
            boolean canContinue = false;
            switch (reponse) {
                case 0:
                    // wil reset ui
                    canContinue = true;
                    break;
            }

            if (!canContinue) {

                // l'utilisateur a choisi de ne pas continuer
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, TargetCatchDto bean, boolean create) {

        TargetCatchUI ui = getUi();

        ReferentialReference<WeightCategoryDto> weightCategory = bean.getWeightCategory();

        ReferentialReference<SpeciesDto> species = null;
        if (weightCategory != null) {
            species = getWeightCategorySpecies(weightCategory).orElse(null);
        }

        JComponent requestFocus;
        if (create) {

            // on reinitilise toujours l'espèce (pour reinitialiser la liste des categories)
            ui.getSpecies().setSelectedItem(null);

            if (!getTableModel().isCreate()) {
                // on repositionne l'espèce (cela reconstruira la liste des categories)
                ui.getSpecies().setSelectedItem(species);
                // on repositionne la categorie
                ui.getWeightCategory().setSelectedItem(weightCategory);
            }

            requestFocus = ui.getSpecies();

        } else {

            // en mode mise a jour, on restreint la liste des categories
            // au singleton de sa valeur correspondante dans le bean
            // puisque dans ce mode, pas possibilite de modifier de cette
            // valeur (clef metier)
            ui.getSpecies().setSelectedItem(species);
            ui.getWeightCategory().setData(Collections.singletonList(weightCategory));
            ui.getWeightCategory().setSelectedItem(weightCategory);

            requestFocus = ui.getCatchWeight();
        }

        if (log.isDebugEnabled()) {
            log.debug("selected weightcategory " + weightCategory);
            log.debug("selected species " + species);
        }
        requestFocus.requestFocus();
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.fixTableColumnWidth(table, 2, 100);
        UIHelper.fixTableColumnWidth(table, 3, 50);
        UIHelper.fixTableColumnWidth(table, 4, 50);

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.targetCatch.table.speciesThon"),
                n("observe.content.targetCatch.table.speciesThon.tip"),
                n("observe.content.targetCatch.table.weightCategory"),
                n("observe.content.targetCatch.table.weightCategory.tip"),
                n("observe.content.targetCatch.table.well"),
                n("observe.content.targetCatch.table.well.tip"),
                n("observe.content.targetCatch.table.weight"),
                n("observe.content.targetCatch.table.weight.tip"),
                n("observe.content.targetCatch.table.comment"),
                n("observe.content.targetCatch.table.comment.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, WeightCategoryDto.class));
        UIHelper.setTableColumnRenderer(table, 2, renderer);
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newStringTableCellRenderer(renderer, 10, true));
    }

    protected void onSpeciesChanged(ReferentialReference<SpeciesDto> species) {

        List<ReferentialReference<WeightCategoryDto>> availableCategories;

        if (species == null) {

            // aucune espèce selectionnee, on vide simplement la liste des categories
            // car il faut d'abord choisir une espèce puis une categorie
            availableCategories = Collections.emptyList();

        } else {

            // un espèce est selectionnee, on calcule les categories pour cette espèce
            // on ne conserve que les categories de l'espèce
            Set<ReferentialReference<WeightCategoryDto>> allCategories = getModel().getReferentialReferences(TargetCatchDto.PROPERTY_WEIGHT_CATEGORY);
            availableCategories = WeightCategoryDtos.filterSpeciesWeightCategories(allCategories, species.getId());

        }

        // on met a jour la liste des categories disponibles
        BeanComboBox<ReferentialReference<WeightCategoryDto>> combo = getUi().getWeightCategory();
        combo.setData(availableCategories);

        // on reinitialise toujours la categorie selectionnee
        combo.setSelectedItem(null);

    }

    @Override
    protected void doPersist(SetSeineTargetCatchDto bean) {

        SaveResultDto saveResult = getTargetCatchService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<SetSeineTargetCatchDto> form = getTargetCatchService().loadForm(beanId, false);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        SetSeineTargetCatchDtos.copySetSeineTargetCatchDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case ObjectSchoolEstimateDto.PROPERTY_SPECIES: {

                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListSeineTargetCatchId();
                String tripSeineId = getDataContext().getSelectedTripSeineId();

                TripSeineService tripSeineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
                result = (List) tripSeineService.getSpeciesByListAndTrip(tripSeineId, speciesListId);

                result = ReferentialReferences.filterEnabled(result);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected TargetCatchService getTargetCatchService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTargetCatchService();
    }

}
