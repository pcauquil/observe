/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing;

import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.admin.AdminActionWorker;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import jaxx.runtime.swing.application.ActionExecutor;
import jaxx.runtime.swing.application.ActionWorker;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Action executor.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ObserveActionExecutor extends ActionExecutor {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveActionExecutor.class);

    @Override
    public void onActionStart(ActionWorker<?, ?> source) {
        if (log.isDebugEnabled()) {
            log.debug("Action [" + source.getActionLabel() + "] was started at " + new Date(source.getStartTime()));
        }
        if (source instanceof ObserveCLAction.CommandLineActionWorker) {
            if (log.isInfoEnabled()) {
                log.info("Action [" + source.getActionLabel() + "] démarrée à " + new Date(source.getStartTime()));
            }

            return;
        }
        if (ObserveSwingApplicationContext.get().isClosed()) {

            // l'application est déjà fermée, on ne fait rien
            return;
        }
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            AdminUIModel model = admin.getHandler().getModel();
            model.setBusy(true);
            model.setStepState(WizardState.RUNNING);
        }
        ObserveMainUI ui = ObserveSwingApplicationContext.get().getMainUI();
        if (ui != null) {
            ui.setBusy(true);
        }
    }

    @Override
    public void onActionFail(ActionWorker<?, ?> source) {
        Exception error = source.getError();
        if (log.isInfoEnabled()) {
            if (log.isErrorEnabled()) {
                log.error("Action [" + source.getActionLabel() + "] failed with error " + error.getCause(), error);
            }
        }
        if (ObserveSwingApplicationContext.get().isClosed()) {

            // l'application est déjà fermée, on ne fait rien
            return;
        }
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            AdminUIModel model = admin.getHandler().getModel();
            model.getStepModel(model.getOperation()).setError(error);
            model.setStepState(WizardState.FAILED);
            return;
        }
        ObserveMainUI ui = ObserveSwingApplicationContext.get().getMainUI();
        if (ui != null) {
            ui.getStatus().setStatus("Action [" + source.getActionLabel() + "] arrêté à cause d'un erreur " + error.getMessage());
        }
    }

    @Override
    public void onActionCancel(ActionWorker<?, ?> source) {
        if (log.isDebugEnabled()) {
            log.debug("Action [" + source.getActionLabel() + "] was canceled");
        }
        if (ObserveSwingApplicationContext.get().isClosed()) {

            // l'application est déjà fermée, on ne fait rien
            return;
        }
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            admin.getHandler().getModel().setStepState(WizardState.CANCELED);
            return;
        }
        ObserveMainUI ui = ObserveSwingApplicationContext.get().getMainUI();
        if (ui != null) {
            ui.getStatus().setStatus("Action [" + source.getActionLabel() + "] annulée");
        }
    }

    @Override
    public void onActionEnd(ActionWorker<?, ?> source) {
        if (log.isDebugEnabled()) {
            log.debug("Action [" + source.getActionLabel() + " ] was done in " + source.getTime());
        }
        if (ObserveSwingApplicationContext.get().isClosed()) {

            // l'application est déjà fermée, on ne fait rien
            return;
        }
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            WizardState state;
            try {
                state = admin.get();
                admin.getHandler().getModel().setStepState(state);
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not retrive data from worker " + admin, e);
                }
            }
            return;
        }
        ObserveMainUI ui = ObserveSwingApplicationContext.get().getMainUI();
        if (ui != null) {
            ui.getStatus().setStatus("Action [" + source.getActionLabel() + "] terminée.");
        }
    }

    @Override
    public void onAfterAction(ActionWorker<?, ?> source) {
        long count = getNbActions();
        if (log.isDebugEnabled()) {
            log.debug("Action [" + source.getActionLabel() + " ] is consumed (still " + count + " tasks to treat).");
        }
        if (ObserveSwingApplicationContext.get().isClosed()) {

            // l'application est déjà fermée, on ne fait rien
            return;
        }
        if (source instanceof ObserveCLAction.CommandLineActionWorker) {

            if (log.isInfoEnabled()) {
                log.info("Action [" + source.getActionLabel() + "] terminée à " + new Date(source.getStartTime()));
            }

            ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
            if (log.isDebugEnabled()) {
                log.debug("Unlock main context " + context);
            }
            context.releaseLock();
            return;
        }
        if (source instanceof AdminActionWorker) {
            AdminActionWorker admin = (AdminActionWorker) source;
            admin.getHandler().getModel().setBusy(false);
        }
        ObserveMainUI ui = ObserveSwingApplicationContext.get().getMainUI();
        if (count < 1 && ui != null) {
            ui.setBusy(false);
        }
    }

    /**
     * Add an new worker to perform.
     *
     * @param worker the worker to run
     * @return the worker that will launch the action
     */
    public ActionWorker<?, ?> addAction(ActionWorker<?, ?> worker) {
        addAction(worker.getActionLabel(), worker);
        return worker;
    }

    /**
     * Add an new action to perform.
     *
     * @param actionLabel the name of the action to perform
     * @param action      the action to perform
     * @return the worker that will launch the action
     */
    public ActionWorker<?, ?> addAction(String actionLabel, Runnable action) {

        ActionWorker<?, ?> worker;
        if (action instanceof ActionWorker) {

            worker = (ActionWorker<?, ?>) action;
        } else {

            worker = new ActionWorker(actionLabel, action);
        }
        worker.addPropertyChangeListener(workerListener);
        tasks.add(worker);
        if (log.isDebugEnabled()) {
            log.debug("Launch worker [" + actionLabel + "] now...");
        }
        getWorkersExecutorService().execute(worker);
        if (log.isDebugEnabled()) {
            log.debug("Launch worker [" + actionLabel + "] is on...");
        }
        return worker;
    }

    private static ExecutorService executorService;

    /**
     * On surcharge celui offer par SwingWorker car le corepool est à 1 et cela
     * ne permet pas d'exécuter plusieurs actions en même temps...
     * returns workersExecutorService.
     *
     * returns the service stored in the appContext or creates it if
     * necessary.
     *
     * @return ExecutorService for the {@code SwingWorkers}
     */
    private static ExecutorService getWorkersExecutorService() {
        if (executorService == null) {
            //this creates daemon threads.
            ThreadFactory threadFactory =
                    new ThreadFactory() {
                        ThreadFactory defaultFactory = Executors.defaultThreadFactory();

                        public Thread newThread(Runnable r) {
                            Thread thread = defaultFactory.newThread(r);
                            thread.setName("ActionWorker-" + thread.getName());
                            thread.setDaemon(true);
                            return thread;
                        }
                    };

            executorService = new ThreadPoolExecutor(5, 10, 10L, TimeUnit.MINUTES, new LinkedBlockingQueue<>(), threadFactory);

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    if (executorService != null) {
                        executorService.shutdownNow();
                    }
                }
            });
        }
        return executorService;
    }
}
