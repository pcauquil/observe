package fr.ird.observe.application.swing.ui.tree.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.service.seine.ActivitySeineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 1/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class ChangeActivityRouteActionListener extends NodeChangeActionListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeActivityRouteActionListener.class);

    public ChangeActivityRouteActionListener(ObserveTreeHelper treeHelper,
                                             ObserveSwingDataSource dataSource,
                                             String activityId,
                                             String routeId) {
        super(treeHelper, activityId, routeId);
    }

    @Override
    protected void closeNode(String activityId) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();

        if (openDataManager.isOpenActivitySeine(activityId)) {
            openDataManager.closeActivitySeine(activityId);
        }
    }

    @Override
    protected ObserveNode getParentNode(ObserveNode node) {
        return node.getParent().getParent();
    }

    @Override
    protected ObserveNode getNewParentNode(ObserveNode grandParentNode, String parentNodeId) {
        ObserveNode routeNode = getTreeHelper().getChild(grandParentNode, parentNodeId);
        String activitiesNodeId = ObserveI18nDecoratorHelper.getTypePluralI18nKey(ActivitySeineDto.class);
        return getTreeHelper().getChild(routeNode, activitiesNodeId);
    }

    @Override
    protected int moveNodeToParent(String nodeId, String parentNodeId, String oldParentNodeId) {
        int position;

        ActivitySeineService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivitySeineService();
        position = service.moveActivitySeineToRoute(nodeId, parentNodeId);

        return position;
    }

}
