package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.SectionTemplate;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentUIInitializer;
import fr.ird.observe.application.swing.ui.util.table.AutotSelectRowAndShowPopupActionSupport;
import fr.ird.observe.application.swing.ui.util.table.EditableTableModelSupport;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.nuiton.decorator.Decorator;

import javax.swing.JComboBox;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import java.awt.Container;
import java.io.Serializable;
import java.util.Collections;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class LonglineDetailCompositionUIInitializer extends ContentUIInitializer<SetLonglineDetailCompositionDto, LonglineDetailCompositionUI> {

    public LonglineDetailCompositionUIInitializer(LonglineDetailCompositionUI ui) {
        super(ui);
    }

    @Override
    public void initUI() {
        super.initUI();

        LonglineDetailCompositionUIModel model = ui.getModel();

        {
            // init section templates table
            JTable table = ui.getSectionTemplatesTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.longlineDetailComposition.sectionTemplatesTable.identifier"),
                                                n("observe.content.longlineDetailComposition.sectionTemplatesTable.identifier.tip"),
                                                n("observe.content.longlineDetailComposition.sectionTemplatesTable.value"),
                                                n("observe.content.longlineDetailComposition.sectionTemplatesTable.value.tip"));

            SectionTemplatesTableModel tableModel = model.getSectionTemplatesTableModel();
            tableModel.installTableKeyListener(table);
            tableModel.installTableFocusListener(table);

            ListSelectionModel selectionModel = table.getSelectionModel();
            selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tableModel.installSelectionListener(table);

            new SectionTemplatesAutotSelectRowAndShowPopupAction(ui, ui.getSectionTemplatesPane(), table, ui.getSectionTemplatesPopup());

        }

        {
            // init sections table
            JTable table = ui.getSectionsTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.longlineDetailComposition.sectionsTable.settingIdentifier"),
                                                n("observe.content.longlineDetailComposition.sectionsTable.settingIdentifier.tip"),
                                                n("observe.content.longlineDetailComposition.sectionsTable.haulingIdentifier"),
                                                n("observe.content.longlineDetailComposition.sectionsTable.haulingIdentifier.tip"),
                                                n("observe.content.longlineDetailComposition.sectionsTable.template"),
                                                n("observe.content.longlineDetailComposition.sectionsTable.template.tip"));

            TableCellRenderer renderer = table.getDefaultRenderer(Object.class);
            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 2, UIHelper.newDecorateTableCellRenderer(renderer, SectionTemplate.class));

            UIHelper.setTableColumnEditor(table, 1, newIntegerColumnEditor(table));

            DecoratorService decoratorService = ui.getHandler().getDecoratorService();
            Decorator<SectionTemplate> sectionTemplateDecorator = decoratorService.getDecoratorByType(SectionTemplate.class);
            JComboBox comboBox = new JComboBox();
            table.putClientProperty(LonglineDetailCompositionUIHandler.SECTION_TEMPLATES_EDITOR, comboBox);
            ComboBoxCellEditor editor = newDataColumnEditor(comboBox, Collections.emptyList(), sectionTemplateDecorator);
            UIHelper.setTableColumnEditor(table, 2, editor);

            SectionsTableModel tableModel = model.getSectionsTableModel();
            tableModel.installTableKeyListener(table);
            tableModel.installTableFocusListener(table);

            ListSelectionModel selectionModel = table.getSelectionModel();
            selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tableModel.installSelectionListener(table);

            new SectionsAutotSelectRowAndShowPopupAction(ui, ui.getSectionsPane(), table, ui.getSectionsPopup());

        }
        {
            // init baskets table
            JTable table = ui.getBasketsTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.longlineDetailComposition.basketsTable.settingIdentifier"),
                                                n("observe.content.longlineDetailComposition.basketsTable.settingIdentifier.tip"),
                                                n("observe.content.longlineDetailComposition.basketsTable.haulingIdentifier"),
                                                n("observe.content.longlineDetailComposition.basketsTable.haulingIdentifier.tip"),
                                                n("observe.content.longlineDetailComposition.basketsTable.floatline1Length"),
                                                n("observe.content.longlineDetailComposition.basketsTable.floatline1Length.tip"),
                                                n("observe.content.longlineDetailComposition.basketsTable.floatline2Length"),
                                                n("observe.content.longlineDetailComposition.basketsTable.floatline2Length.tip"));

            TableCellRenderer renderer = table.getDefaultRenderer(Object.class);
            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));

            UIHelper.setTableColumnEditor(table, 1, newIntegerColumnEditor(table));
            UIHelper.setTableColumnEditor(table, 2, newFloatColumnEditor(table));
            UIHelper.setTableColumnEditor(table, 3, newFloatColumnEditor(table));

            BasketsTableModel tableModel = model.getBasketsTableModel();
            tableModel.installTableKeyListener(table);
            tableModel.installTableFocusListener(table);

            ListSelectionModel selectionModel = table.getSelectionModel();
            selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tableModel.installSelectionListener(table);

            new BasketsAutotSelectRowAndShowPopupAction(ui, ui.getBasketsPane(), table, ui.getBasketsPopup());

        }

        {
            // init branchlines table
            JTable table = ui.getBranchlinesTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.longlineDetailComposition.branchlinesTable.settingIdentifier"),
                                                n("observe.content.longlineDetailComposition.branchlinesTable.settingIdentifier.tip"),
                                                n("observe.content.longlineDetailComposition.branchlinesTable.haulingIdentifier"),
                                                n("observe.content.longlineDetailComposition.branchlinesTable.haulingIdentifier.tip"),
                                                n("observe.content.longlineDetailComposition.branchlinesTable.branchlineLength"),
                                                n("observe.content.longlineDetailComposition.branchlinesTable.branchlineLength.tip"),
                                                n("observe.content.longlineDetailComposition.branchlinesTable.tracelineLength"),
                                                n("observe.content.longlineDetailComposition.branchlinesTable.tracelineLength.tip"));

            TableCellRenderer renderer = table.getDefaultRenderer(Object.class);
            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));

            UIHelper.setTableColumnEditor(table, 1, newIntegerColumnEditor(table));
            UIHelper.setTableColumnEditor(table, 2, newFloatColumnEditor(table));
            UIHelper.setTableColumnEditor(table, 3, newFloatColumnEditor(table));

            BranchlinesTableModel tableModel = model.getBranchlinesTableModel();
            tableModel.installTableKeyListener(table);
            tableModel.installTableFocusListener(table);

            ListSelectionModel selectionModel = table.getSelectionModel();
            selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tableModel.installSelectionListener(table);

            new BranchlinesAutotSelectRowAndShowPopupAction(ui, ui.getBranchlinesPane(), table, ui.getBranchlinesPopup());

        }

        {

            // init branchlineDetail tab
            BranchlineUI compositionUI = ui.getBranchlineDetailUI();
            compositionUI.init();
            Container parent = ui.getBranchlineDetailPanel();
            parent .removeAll();
            parent.add(compositionUI.getBody());

        }

    }


    static class SectionTemplatesAutotSelectRowAndShowPopupAction extends AutotSelectRowAndShowPopupActionSupport {

        private final LonglineDetailCompositionUI ui;

        public SectionTemplatesAutotSelectRowAndShowPopupAction(LonglineDetailCompositionUI ui,
                                                                JScrollPane pane,
                                                                JTable table,
                                                                JPopupMenu popup) {
            super(pane, table, popup);
            this.ui = ui;
        }

        @Override
        protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {

            EditableTableModelSupport model = (EditableTableModelSupport) getTable().getModel();

            boolean canDelete = !model.isSelectionEmpty();

            if (canDelete) {

                // check also that the row is not empty
                Serializable selectedData = model.getSelectedRow();
                canDelete = model.isRowNotEmpty(selectedData);

            }

            ui.getDeleteSelectedSectionTemplate().setEnabled(canDelete);

        }

    }

    static class SectionsAutotSelectRowAndShowPopupAction extends AutotSelectRowAndShowPopupActionSupport {

        private final LonglineDetailCompositionUI ui;

        public SectionsAutotSelectRowAndShowPopupAction(LonglineDetailCompositionUI ui,
                                                        JScrollPane pane,
                                                        JTable table,
                                                        JPopupMenu popup) {
            super(pane, table, popup);
            this.ui = ui;
        }

        @Override
        protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {

            EditableTableModelSupport model = (EditableTableModelSupport) getTable().getModel();

            boolean selectionNotEmpty = !model.isSelectionEmpty();
            boolean selectedRowIsNotEmpty = false;

            if (selectionNotEmpty) {

                Serializable selectedData = model.getSelectedRow();
                selectedRowIsNotEmpty = model.isRowNotEmpty(selectedData);

            }

            boolean canDelete = false;
            boolean canInsertBefore = true;
            boolean canInsertAfter = true;

            if (selectionNotEmpty) {

                // can delete only a non empty selected row
                canDelete = selectedRowIsNotEmpty;

                // won't add before an empty row
                canInsertBefore = selectedRowIsNotEmpty;

                // won't after before an empty row
                canInsertAfter = selectedRowIsNotEmpty;

            }

            ui.getDeleteSelectedSection().setEnabled(canDelete);
            ui.getInsertBeforeSelectedSection().setEnabled(canInsertBefore);
            ui.getInsertAfterSelectedSection().setEnabled(canInsertAfter);

        }

    }

    static class BasketsAutotSelectRowAndShowPopupAction extends AutotSelectRowAndShowPopupActionSupport {

        private final LonglineDetailCompositionUI ui;

        public BasketsAutotSelectRowAndShowPopupAction(LonglineDetailCompositionUI ui,
                                                       JScrollPane pane,
                                                       JTable table,
                                                       JPopupMenu popup) {
            super(pane, table, popup);
            this.ui = ui;
        }

        @Override
        protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {

            EditableTableModelSupport model = (EditableTableModelSupport) getTable().getModel();

            boolean selectionNotEmpty = !model.isSelectionEmpty();
            boolean selectedRowIsNotEmpty = false;

            if (selectionNotEmpty) {

                Serializable selectedData = model.getSelectedRow();
                selectedRowIsNotEmpty = model.isRowNotEmpty(selectedData);

            }

            boolean canDelete = false;
            boolean canInsertBefore = true;
            boolean canInsertAfter = true;

            if (selectionNotEmpty) {

                // can delete only a non empty selected row
                canDelete = selectedRowIsNotEmpty;

                // won't add before an empty row
                canInsertBefore = selectedRowIsNotEmpty;

                // won't after before an empty row
                canInsertAfter = selectedRowIsNotEmpty;

            }

            ui.getDeleteSelectedBasket().setEnabled(canDelete);
            ui.getInsertBeforeSelectedBasket().setEnabled(canInsertBefore);
            ui.getInsertAfterSelectedBasket().setEnabled(canInsertAfter);

        }

    }

    static class BranchlinesAutotSelectRowAndShowPopupAction extends AutotSelectRowAndShowPopupActionSupport {

        private final LonglineDetailCompositionUI ui;

        public BranchlinesAutotSelectRowAndShowPopupAction(LonglineDetailCompositionUI ui,
                                                           JScrollPane pane,
                                                           JTable table,
                                                           JPopupMenu popup) {
            super(pane, table, popup);
            this.ui = ui;
        }

        @Override
        protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {

            EditableTableModelSupport model = (EditableTableModelSupport) getTable().getModel();

            boolean selectionNotEmpty = !model.isSelectionEmpty();
            boolean selectedRowIsNotEmpty = false;

            if (selectionNotEmpty) {

                Serializable selectedData = model.getSelectedRow();
                selectedRowIsNotEmpty = model.isRowNotEmpty(selectedData);

            }

            boolean canDelete = false;
            boolean canInsertBefore = true;
            boolean canInsertAfter = true;

            if (selectionNotEmpty) {

                // can delete only a non empty selected row
                canDelete = selectedRowIsNotEmpty;

                // won't add before an empty row
                canInsertBefore = selectedRowIsNotEmpty;

                // won't after before an empty row
                canInsertAfter = selectedRowIsNotEmpty;

            }

            ui.getDeleteSelectedBranchline().setEnabled(canDelete);
            ui.getInsertBeforeSelectedBranchline().setEnabled(canInsertBefore);
            ui.getInsertAfterSelectedBranchline().setEnabled(canInsertAfter);

        }

    }
}
