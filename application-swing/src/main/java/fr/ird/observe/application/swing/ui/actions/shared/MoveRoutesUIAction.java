/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.RoutesUI;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.tree.TripSeineNode;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.seine.RouteService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Action pour changer le programme d'une ou plusieurs marée dans la liste.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveRoutesUIAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(MoveRoutesUIAction.class);

    public static final String ACTION_NAME = "moveRoutes";

    public MoveRoutesUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.content.action.move.routes"),
              n("observe.content.action.move.routes.tip"),
              "move-routes"
        );
    }

    @Override
    public void actionPerformed(final ActionEvent e) {

        SwingUtilities.invokeLater(() -> {
            JComponent c = (JComponent) e.getSource();
            ContentUI<?> ui = (ContentUI<?>)
                    c.getClientProperty("ui");
            if (ui == null) {
                throw new IllegalStateException(
                        "could not find client property " +
                                "ui on component" + c);
            }

            if (!(ui instanceof RoutesUI)) {
                throw new IllegalStateException("Can not come here!");
            }

            RoutesUI theUi = (RoutesUI) ui;

            // get current tripseine id
            ObserveTreeHelper treeHelper = getMainUI().getTreeHelper();
            ObserveNode oldRoutesNode = treeHelper.getSelectedNode();
            ObserveNode oldTripSeineNode = oldRoutesNode.getParent();

            // choose the new tripseine
            String tripSeineId = chooseNewTripSeine(theUi, oldTripSeineNode);

            if (tripSeineId != null) {
                // change the tripseine of the selected routes
                List<DataReference<RouteDto>> selectedDatas = theUi.getModel().getSelectedDatas();
                List<String> routeIds = selectedDatas.stream().map(DataReference.ID_FUNCTION).collect(Collectors.toList());
                RouteService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newRouteService();
                List<Integer> positions = service.moveRoutesToTripSeine(routeIds, tripSeineId);

                // update the tree
                updateTree(oldRoutesNode, tripSeineId, routeIds);
            }

        });

    }

    protected String chooseNewTripSeine(ContentUI<?> ui, ObserveNode oldTripSeineNode) {
        ObserveNode programNode = oldTripSeineNode.getParent();
        String oldTripSeineId = oldTripSeineNode.getId();
        int tripSeineNb = programNode.getChildCount();

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        DataReferenceDecorator<TripSeineDto> decorator = applicationContext.getDecoratorService().getDataReferenceDecorator(TripSeineDto.class);

        //on crée un tableau avec une marée en moins car on ne propose pas la marée actuel
        DecoratedNodeEntity[] decoratedTripSeines = new DecoratedNodeEntity[tripSeineNb - 1];

        int j = 0;
        for (int i = 0; i < tripSeineNb; i++) {

            TripSeineNode tripSeineNode = (TripSeineNode) programNode.getChildAt(i);

            String tripSeineId = tripSeineNode.getId();

            if (!oldTripSeineId.equals(tripSeineId)) {
                decoratedTripSeines[j++] = DecoratedNodeEntity.newDecoratedNodeEntity(tripSeineNode, decorator);
            }
        }

        Object decoratedTripSeine = JOptionPane.showInputDialog(ui,
                                                                t("observe.action.choose.tripSeine.message"),
                                                                t("observe.action.choose.tripSeine.title"),
                                                                JOptionPane.QUESTION_MESSAGE,
                                                                null,
                                                                decoratedTripSeines,
                                                                null);
        return decoratedTripSeine != null ? ((DecoratedNodeEntity) decoratedTripSeine).getId() : null;
    }

    protected void updateTree(ObserveNode oldRoutesNode,
                              String tripSeineId,
                              List<String> routeIds) {

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();
        ObserveTreeHelper treeHelper = getMainUI().getTreeHelper();

        ObserveNode oldTripSeineNode = oldRoutesNode.getParent();
        ObserveNode programNode = oldTripSeineNode.getParent();
        ObserveNode newTripSeineNode = treeHelper.getChild(programNode, tripSeineId);
        String routesNodeId = ObserveI18nDecoratorHelper.getTypePluralI18nKey(RouteDto.class);
        ObserveNode newRoutesNode = treeHelper.getChild(newTripSeineNode, routesNodeId);

        // Let's check if we're moving an open route
        Optional<String> openRoute = routeIds
                .stream()
                .filter(openDataManager::isOpenRoute)
                .findFirst();

        // If so, we close it to avoid ending up with an open route into a closed trip.
        if (openRoute.isPresent()) {
            openDataManager.closeRoute(openRoute.get());
        }

        // Let's reload the sub tree of each routes node.
        // As the change have already be done in database, we just call the child loaders to regenerate the routes nodes sub trees
        treeHelper.reloadNodeSubTree(oldRoutesNode, true);
        treeHelper.reloadNodeSubTree(newRoutesNode, true);

        // 1. Select the newRoutesNode :
        // only a selected node can be reloaded (@see fr.ird.observe.application.swing.ui.tree.ObserveTreeBridge.canLoadChild).
        // (And also it's better for user experience because it puts the focus on the routes node which receive the routes)
        //
        // However, the node selection has to be done after that the trip has been opened :
        // selecting the newRoutesNode opens a list layout which compute a rendering mode associated to the trip state.
        // After the mode has been computed, it is injected in the list component which triggers listeners on associated components, such as buttons.
        //
        // That's why, if we do this before the trip is opened,
        // we will end up with an incoherent list view (some buttons will be deactivated while they should be activated for instance)
        treeHelper.selectNode(newRoutesNode);

    }

}
