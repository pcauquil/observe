/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2014 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{model.isCompositionTabValid() && model.isBranchlineDetailTabValid()};
  enabled:{!model.isReadingMode()};
  generateTabValid: {!model.isCanGenerate() || sectionTemplatesTableModel.isValid()};
  /*compositionTabValid: {sectionsTableModel.isValid() && basketsTableModel.isValid() && branchlinesTableModel.isValid()};*/
  branchlineDetailTabValid: {branchlineDetailUIModel.isValid()};
}

/* ***************************************************************************** */
/*  GENERATE TAB *************************************************************** */
/* ***************************************************************************** */

#generateTab {
  title:{t("observe.content.longlineDetailComposition.tab.generate")};
  icon:{getHandler().getErrorIconIfFalse(model.isGenerateTabValid())};
}

#canGenerate {
  horizontalTextPosition:{JCheckBox.LEFT};
  text:"observe.content.longlineDetailComposition.canGenerate";
  selected:{model.isCanGenerate()};
}

#deleteSelectedSectionTemplate {
  text: "observe.content.longlineDetailComposition.action.deleteSelectedSectionTemplate";
  toolTipText: "observe.content.longlineDetailComposition.action.deleteSelectedSectionTemplate.tip";
  actionIcon: delete;
}

#sectionTemplatesTableModel {
   editable: {model.isCanGenerate()};
}

#sectionTemplatesTable {
  model: {sectionTemplatesTableModel};
  enabled: {model.isCanGenerate()};
}

/* ***************************************************************************** */
/*  COMPOSITION TAB ************************************************************ */
/* ***************************************************************************** */

#compositionTab {
  title: {t("observe.content.longlineDetailComposition.tab.composition")};
  icon: {getHandler().getErrorIconIfFalse(model.isCompositionTabValid())};
  enabled: {model.isGenerateTabValid()};
}

#generateAll {
  text: "observe.content.longlineDetailComposition.action.generateAllSections";
  toolTipText: "observe.content.longlineDetailComposition.action.generateAllSections.tip";
  actionIcon: generate;
  visible: {model.isUpdatingMode()};
  enabled: {model.isCanGenerate() && sectionsTableModel.isEmpty()};
}

#deleteAll {
  text: "observe.content.longlineDetailComposition.action.deleteAllSections";
  toolTipText: "observe.content.longlineDetailComposition.action.deleteAllSections.tip";
  actionIcon: delete;
  visible: {model.isUpdatingMode()};
  enabled: {!sectionsTableModel.isEmpty()};
}

#sectionsPane {
  border: {new TitledBorder(t("observe.content.longlineDetailComposition.sections"))};
  _doNotTranslateFieldName: true;
}

#deleteSelectedSection {
  text: "observe.content.longlineDetailComposition.action.deleteSelectedSection";
  toolTipText: "observe.content.longlineDetailComposition.action.deleteSelectedSection.tip";
  actionIcon: delete;
}

#insertBeforeSelectedSection {
  text: "observe.content.longlineDetailComposition.action.insertBeforeSelectedSection";
  toolTipText: "observe.content.longlineDetailComposition.action.insertBeforeSelectedSection.tip";
  actionIcon: insert-before;
}

#insertAfterSelectedSection {
  text: "observe.content.longlineDetailComposition.action.insertAfterSelectedSection";
  toolTipText: "observe.content.longlineDetailComposition.action.insertAfterSelectedSection.tip";
  actionIcon: insert-after;
}

#sectionsTableModel {
  editable: {model.isEnabled()};
}

#sectionsTable {
  model: {sectionsTableModel};
  editable: {model.isEnabled()};
  _notBlocking: true;
}

#basketsPane {
  border: {new TitledBorder(t("observe.content.longlineDetailComposition.baskets"))};
  _doNotTranslateFieldName: true;
}

#deleteSelectedBasket {
  text: "observe.content.longlineDetailComposition.action.deleteSelectedBasket";
  toolTipText: "observe.content.longlineDetailComposition.action.deleteSelectedBasket.tip";
  actionIcon: delete;
}

#insertBeforeSelectedBasket {
  text: "observe.content.longlineDetailComposition.action.insertBeforeSelectedBasket";
  toolTipText: "observe.content.longlineDetailComposition.action.insertBeforeSelectedBasket.tip";
  actionIcon: insert-before;
}

#insertAfterSelectedBasket {
  text: "observe.content.longlineDetailComposition.action.insertAfterSelectedBasket";
  toolTipText: "observe.content.longlineDetailComposition.action.insertAfterSelectedBasket.tip";
  actionIcon: insert-after;
}

#basketsTableModel {
  editable: {model.isEnabled()};
}

#basketsTable {
  model: {basketsTableModel};
  editable: {model.isEnabled()};
  _notBlocking: true;
}

#branchlinesPane {
  border: {new TitledBorder(t("observe.content.longlineDetailComposition.branchlines"))};
  _doNotTranslateFieldName: true;
}

#deleteSelectedBranchline {
  text: "observe.content.longlineDetailComposition.action.deleteSelectedBranchline";
  toolTipText: "observe.content.longlineDetailComposition.action.deleteSelectedBranchline.tip";
  actionIcon: delete;
}

#insertBeforeSelectedBranchline {
  text: "observe.content.longlineDetailComposition.action.insertBeforeSelectedBranchline";
  toolTipText: "observe.content.longlineDetailComposition.action.insertBeforeSelectedBranchline.tip";
  actionIcon: insert-before;
}

#insertAfterSelectedBranchline {
  text: "observe.content.longlineDetailComposition.action.insertAfterSelectedBranchline";
  toolTipText: "observe.content.longlineDetailComposition.action.insertAfterSelectedBranchline.tip";
  actionIcon: insert-after;
}

#branchlinesTableModel {
  editable: {model.isEnabled()};
}

#branchlinesTable {
  model: {branchlinesTableModel};
  editable: {model.isEnabled()};
  _notBlocking: true;
}

/* ***************************************************************************** */
/*  BRANCHLINE TAB ************************************************************* */
/* ***************************************************************************** */

#branchlineDetailTab {
  title: {t("observe.content.longlineDetailComposition.tab.branchlineDetail")};
  icon: {getHandler().getErrorIconIfFalse(model.isBranchlineDetailTabValid())};
  enabled: {compositionTab.isEnabled() && model.isCompositionTabValid() && !branchlinesTableModel.isSelectionEmpty()};
}
