/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class CancelCreateUIAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "cancelCreate";

    public CancelCreateUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.action.cancel"),
              n("observe.action.cancel.create.tip"),
              "cancel"
        );
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        SwingUtilities.invokeLater(() -> {
            JComponent c = (JComponent) e.getSource();
            ContentUI<?> ui = (ContentUI<?>)
                    c.getClientProperty("ui");
            if (ui == null) {
                throw new IllegalStateException(
                        "could not find client property " +
                        "ui on component" + c);
            }
            ui.stopEdit();
            ObserveNode parentNode = ui.getTreeHelper().removeNode(ui.getTreeHelper().getSelectedNode());
            ui.getTreeHelper().selectNode(parentNode);
        });
    }
}
