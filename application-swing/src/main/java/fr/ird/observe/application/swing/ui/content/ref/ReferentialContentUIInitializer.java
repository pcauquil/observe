package fr.ird.observe.application.swing.ui.content.ref;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.application.swing.ui.content.ContentUIInitializer;

/**
 * Created on 11/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class ReferentialContentUIInitializer<E extends ReferentialDto, UI extends ContentReferenceUI<E>> extends ContentUIInitializer<E, UI> {

//    /** Logger. */
//    private static final Log log = LogFactory.getLog(ReferentialContentUIInitializer.class);

    public ReferentialContentUIInitializer(UI ui) {
        super(ui);
    }

    @Override
    protected void initBlockLayerUI(String... doNotBlockComponentIds) {

        super.initBlockLayerUI(doNotBlockComponentIds);

        //tchemit-2014-12-29 We should use a spearate list for this, but there is no collision possible...
        ui.getEditKeyTableLayerUI().setAcceptedComponentNames(doNotBlockComponentIds);

    }

//    protected Decorator<ReferenceDto> getDecorator(Class<? extends ReferentialDto> dtoClass) {
//        return ObserveSwingApplicationContext.get().getDecorator(ReferenceDto.class, dtoClass.getSimpleName());
//    }

}
