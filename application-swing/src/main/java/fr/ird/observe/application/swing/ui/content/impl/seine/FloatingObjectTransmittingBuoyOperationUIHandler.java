/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.impl.seine;

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.seine.Ownership;
import fr.ird.observe.services.dto.constants.seine.TypeTransmittingBuoyOperation;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDtos;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDto;
import fr.ird.observe.services.dto.seine.TransmittingBuoyDtos;
import fr.ird.observe.services.service.seine.TransmittingBuoyOperationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class FloatingObjectTransmittingBuoyOperationUIHandler extends ContentUIHandler<FloatingObjectTransmittingBuoyDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(FloatingObjectTransmittingBuoyOperationUIHandler.class);

    public FloatingObjectTransmittingBuoyOperationUIHandler(FloatingObjectTransmittingBuoyOperationUI ui) {
        super(ui, DataContextType.FloatingObject, null);
    }

    @Override
    public FloatingObjectTransmittingBuoyOperationUI getUi() {
        return (FloatingObjectTransmittingBuoyOperationUI) super.getUi();
    }

    @Override
    public FloatingObjectTransmittingBuoyOperationUIModel getModel() {
        return (FloatingObjectTransmittingBuoyOperationUIModel) super.getModel();
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        if (getOpenDataManager().isOpenActivitySeine(dataContext.getSelectedActivitySeineId())) {

            // l'activity courante est ouverte, on peut modifier
            return ContentMode.UPDATE;
        }

        // activity courante non ouverte
        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(ActivitySeineDto.class),
                   t("observe.storage.activitySeine.message.not.open"));

        return ContentMode.READ;
    }

    @Override
    public void openUI() {
        super.openUI();

        ContentMode mode = computeContentMode();

        String dcpId = getSelectedParentId();

        if (log.isInfoEnabled()) {
            log.info("dcpId = " + dcpId);
        }

        FloatingObjectTransmittingBuoyDto bean = getBean();

        bean.getTransmittingBuoy().clear();

        // pour etre sur que rien ne va changer pendant le chargement de l'objet.

        getModel().setEditing(false);

        Form<FloatingObjectTransmittingBuoyDto> form = getTransmittingBuoyOperationService().loadForm(dcpId);
        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        FloatingObjectTransmittingBuoyDtos.copyFloatingObjectTransmittingBuoyDto(form.getObject(), bean);

        if (bean.sizeTransmittingBuoy() >= 1) {
            TransmittingBuoyDtos.copyTransmittingBuoyDto(
                    bean.getTransmittingBuoy(0),
                    getUi().getTransmittingBuoy1());

            if (bean.sizeTransmittingBuoy() >= 2) {
                TransmittingBuoyDtos.copyTransmittingBuoyDto(
                        bean.getTransmittingBuoy(1),
                        getUi().getTransmittingBuoy2());
            }
        }

        getUi().processDataBinding(
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TYPE_OPERATION_SELECTED_ITEM);

        TypeTransmittingBuoyOperation typeOperation =
                getUi().getTypeOperation().getSelectedItem();
        changeTypeOperation(typeOperation, false);

        getModel().setMode(mode);

        if (mode == ContentMode.UPDATE) {
            getUi().startEdit(null);
        }
    }


    @Override
    public void startEditUI(String... binding) {
        super.startEditUI(
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_OPERATION1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_OPERATION2_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_CODE1_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_CODE2_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_OWNERSHIP1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_OWNERSHIP2_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_TYPE1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_TYPE2_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_BRAND1_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_BRAND2_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_COUNTRY1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_COUNTRY2_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TYPE_OPERATION_SELECTED_ITEM
 );

        FloatingObjectTransmittingBuoyOperationUI ui = getUi();

        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(FloatingObjectDto.class),
                   t("observe.message.updating.floatingObject"));

        Collection<TransmittingBuoyDto> balises = getBean().getTransmittingBuoy();
        if (balises != null && !balises.isEmpty()) {
            TransmittingBuoyDto transmittingBuoy1 = ui.getTransmittingBuoy1();
            ui.getValidatorBalise1().setBean(transmittingBuoy1);

            if (balises.size() == 2) {
                TransmittingBuoyDto transmittingBuoy2 = ui.getTransmittingBuoy2();
                ui.getValidatorBalise2().setBean(transmittingBuoy2);
            }
        }

        UIHelper.processDataBinding(
                ui,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_OPERATION1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_OPERATION2_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_CODE1_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_CODE2_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_OWNERSHIP1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_OWNERSHIP2_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_BRAND1_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_BRAND2_TEXT,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_COUNTRY1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_COUNTRY2_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_TYPE1_SELECTED_ITEM,
                FloatingObjectTransmittingBuoyOperationUI.BINDING_TRANSMITTING_BUOY_TYPE2_SELECTED_ITEM);
        getModel().setModified(false);
    }

    public boolean isBalisePose(TransmittingBuoyOperationDto operation) {
        return operation != null && "3".equals(operation.getCode());
    }

    public boolean isBalisePose(ReferentialReference<TransmittingBuoyOperationDto> operation) {
        return operation != null && "3".equals(operation.getPropertyValue(TransmittingBuoyOperationDto.PROPERTY_CODE));
    }

    @Override
    protected boolean doSave(FloatingObjectTransmittingBuoyDto bean) throws Exception {

        SaveResultDto saveResult = getTransmittingBuoyOperationService().save(bean);
        saveResult.toDto(bean);
        //FIXME, il faut declancher un evenement dans le cache du storage pour pouvoir redessiner le noeud
        return true;
    }

    @Override
    protected void afterSave(boolean refresh) {
        // rien a rafraichir ?
        resetEditUI();
    }

    protected void changeTypeOperation(TypeTransmittingBuoyOperation typeOperation, boolean reset) {

        FloatingObjectTransmittingBuoyOperationUI ui = getUi();

        if (log.isInfoEnabled()) {
            log.info(typeOperation + " doReset ? " + reset);
        }
        int nbBalises = typeOperation.getNbBalises();

        FloatingObjectTransmittingBuoyDto bean = getBean();

        boolean hasBalise = !bean.isTransmittingBuoyEmpty();

        List<TransmittingBuoyDto> objets = new ArrayList<>();
        if (!reset) {
            if (hasBalise) {
                objets.addAll(bean.getTransmittingBuoy());
            }
        }

        if (hasBalise) {
            // always reset balise lues of editBean to avoid side-effects
            bean.getTransmittingBuoy().clear();
        }

        // clean container
        JPanel editorPanel = ui.getTransmittingBuoys();
        editorPanel.removeAll();

        String[] codeOperations = typeOperation.getCodeOperation();
        List<ReferentialReference<TransmittingBuoyOperationDto>> operations =
                ui.getTransmittingBuoyOperation1().getData();

        TransmittingBuoyDto transmittingBuoy;
        ReferentialReference<TransmittingBuoyOperationDto> objectOperation;
        switch (nbBalises) {
            case 0:
                // no balise
                editorPanel.setLayout(new GridLayout());
                editorPanel.add(ui.getNoBaliseEditor());
                if (getModel().isEditing()) {
                    ui.getValidatorBalise1().setBean(null);
                    ui.getValidatorBalise2().setBean(null);
                }
                break;

            case 1:

                transmittingBuoy = reset ? null : objets.get(0);

                objectOperation = reset ? getObjectOperation(operations, codeOperations[0]) : null;

                bindEditBalise(ui.getTransmittingBuoy1(), objectOperation, transmittingBuoy);

                editorPanel.setLayout(new GridLayout(1, 0));
                editorPanel.add(ui.getTransmittingBuoy1Editor());
                if (getModel().isEditing()) {
                    ui.getValidatorBalise1().setBean(ui.getTransmittingBuoy1());
                    ui.getValidatorBalise2().setBean(null);
                }
                break;

            case 2:

                transmittingBuoy = reset ? null : objets.get(0);
                objectOperation = reset ? getObjectOperation(operations, codeOperations[0]) : null;
                bindEditBalise(ui.getTransmittingBuoy1(), objectOperation, transmittingBuoy);

                transmittingBuoy = reset ? null : objets.get(1);
                objectOperation = reset ? getObjectOperation(operations, codeOperations[1]) : null;
                bindEditBalise(ui.getTransmittingBuoy2(), objectOperation, transmittingBuoy);

                editorPanel.setLayout(new GridLayout(2, 0));
                editorPanel.add(ui.getTransmittingBuoy1Editor());
                editorPanel.add(ui.getTransmittingBuoy2Editor());
                if (getModel().isEditing()) {
                    ui.getValidatorBalise1().setBean(ui.getTransmittingBuoy1());
                    ui.getValidatorBalise2().setBean(ui.getTransmittingBuoy2());
                }
                break;
        }

        SwingUtilities.invokeLater(() -> {

            // recalcul de la disposition
            getUi().revalidate();
        });

    }

    protected void bindEditBalise(TransmittingBuoyDto editBean,
                                  ReferentialReference<TransmittingBuoyOperationDto> transmittingBuoyOperation,
                                  TransmittingBuoyDto transmittingBuoy) {

        if (transmittingBuoy == null) {

            TransmittingBuoyDtos.copyTransmittingBuoyDto(new TransmittingBuoyDto(), editBean);

            editBean.setTransmittingBuoyOperation(transmittingBuoyOperation);

            if (log.isDebugEnabled()) {
                log.debug("Reuse an empty balise lue for objectOperation " +
                          transmittingBuoyOperation.getPropertyValue(TransmittingBuoyOperationDto.PROPERTY_LABEL1));
            }

            if (isBalisePose(transmittingBuoyOperation)) {

                // objectOperation de pose
                // toujours appartient au navire
                editBean.setOwnership(Ownership.ceVessel);
            }

        } else {

            TransmittingBuoyDtos.copyTransmittingBuoyDto(editBean, transmittingBuoy);

        }

        FloatingObjectTransmittingBuoyDto bean = getBean();

        if (bean.getTransmittingBuoy() == null) {
            bean.setTransmittingBuoy(Lists.newArrayList());
        }
        bean.getTransmittingBuoy().add(editBean);
    }

    protected ReferentialReference<TransmittingBuoyOperationDto> getObjectOperation(List<ReferentialReference<TransmittingBuoyOperationDto>> objectOperations,
                                                                String codeOperation) {
        ReferentialReference<TransmittingBuoyOperationDto> objectOperation = null;
        for (ReferentialReference<TransmittingBuoyOperationDto> op : objectOperations) {
            if (codeOperation.equals(op.getPropertyValue(TransmittingBuoyOperationDto.PROPERTY_CODE))) {
                objectOperation = op;
                break;
            }
        }
        if (objectOperation == null) {
            throw new IllegalArgumentException(
                    "could not find a " +
                    TransmittingBuoyOperationDto.class + " with code " + codeOperation);
        }
        return objectOperation;
    }

    protected TransmittingBuoyOperationService getTransmittingBuoyOperationService() {
        return  ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTransmittingBuoyOperationService();
    }

}
