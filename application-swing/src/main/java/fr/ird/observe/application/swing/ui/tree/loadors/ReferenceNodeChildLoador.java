/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree.loadors;


import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.services.dto.referential.ReferentialDtos;
import jaxx.runtime.swing.nav.NavDataProvider;

import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Le chargeur des noeuds du referentiel.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
class ReferenceNodeChildLoador extends AbstractNodeChildLoador<Class, Class> {

    public static class CommonReferenceNodeChildLoador extends ReferenceNodeChildLoador {
        public CommonReferenceNodeChildLoador() {
            super(ReferentialDtos.REFERENCE_COMMON_DTOS);
        }
    }

    public static class SeineReferenceNodeChildLoador extends ReferenceNodeChildLoador {
        public SeineReferenceNodeChildLoador() {
            super(ReferentialDtos.REFERENCE_SEINE_DTOS);
        }
    }

    public static class LonglineReferenceNodeChildLoador extends ReferenceNodeChildLoador {
        public LonglineReferenceNodeChildLoador() {
            super(ReferentialDtos.REFERENCE_LONGLINE_DTOS);
        }
    }


    private static final long serialVersionUID = 1L;

    private final Set<Class> classes;

    private ReferenceNodeChildLoador(Set classes) {
        super(Class.class);
        this.classes = classes;
    }

    @Override
    public List<Class> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) {
        return ObserveI18nDecoratorHelper.sortTypes(classes);
    }

    @Override
    public ObserveNode createNode(Class data, NavDataProvider dataProvider) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        return new ObserveNode(data, data.getSimpleName(), null, true);
    }
}
