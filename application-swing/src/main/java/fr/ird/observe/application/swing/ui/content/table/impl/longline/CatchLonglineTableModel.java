package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDtos;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 12/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class CatchLonglineTableModel extends ContentTableModel<SetLonglineCatchDto, CatchLonglineDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CatchLonglineTableModel.class);

    private static final long serialVersionUID = 1L;

    public CatchLonglineTableModel(ObserveContentTableUI<SetLonglineCatchDto, CatchLonglineDto> context,
                                   List<ContentTableMeta<CatchLonglineDto>> contentTableMetas) {
        super(context, contentTableMetas);
    }

    @Override
    public void addNewEntry() {

        int editingRow = getSelectedRow();

        if (editingRow > -1) {

            // store sizes and weights for the selected row
            // before creating a new one ?
            CatchLonglineUIModel model = getModel();
            model.getSizeMeasuresTableModel().storeInCacheForRow(editingRow);
            model.getWeightMeasuresTableModel().storeInCacheForRow(editingRow);

        }

        super.addNewEntry();
    }

    @Override
    public void updateRowFromEditBean() {

        // store sizes and weights for the selected row

        int editingRow = getSelectedRow();

        CatchLonglineUIModel model = getModel();

        model.getSizeMeasuresTableModel().storeInCacheForRow(editingRow);

        List<SizeMeasureDto> sizeMeasures = model.getSizeMeasuresTableModel().getData().stream()
                .filter(sizeMeasureDto -> sizeMeasureDto.getSizeMeasureType() != null)
                .collect(Collectors.toList());

        getRowBean().setSizeMeasure(sizeMeasures);

        model.getWeightMeasuresTableModel().storeInCacheForRow(editingRow);

        List<WeightMeasureDto> weightMeasures = model.getWeightMeasuresTableModel().getData().stream()
                .filter(weightMeasureDto -> weightMeasureDto.getWeightMeasureType() != null)
                .collect(Collectors.toList());

        getRowBean().setWeightMeasure(weightMeasures);

        super.updateRowFromEditBean();


    }

    @Override
    protected void removeRow(int row) {
        super.removeRow(row);

        // remove sizes and weights for the deleted row
        // also update rows to row - 1 (when after the deleted row)
        CatchLonglineUIModel model = getModel();
        model.getSizeMeasuresTableModel().removeCacheForRow(row);
        model.getWeightMeasuresTableModel().removeCacheForRow(row);

    }

    @Override
    protected void setChilds(SetLonglineCatchDto parent, List<CatchLonglineDto> childs) {
        parent.setCatchLongline(childs);
    }

    @Override
    public void resetEditBean() {

        int row = getSelectedRow();
        if (log.isInfoEnabled()) {
            log.info("Reset edit bean at row: " + row);
        }
        CatchLonglineUIModel model = getModel();
        model.getSizeMeasuresTableModel().resetCacheForRow(row);
        model.getWeightMeasuresTableModel().resetCacheForRow(row);

        super.resetEditBean();

    }

    @Override
    protected Collection<CatchLonglineDto> getChilds(SetLonglineCatchDto bean) {
        return bean.getCatchLongline();
    }

    @Override
    protected void load(CatchLonglineDto source, CatchLonglineDto target) {
        CatchLonglineDtos.copyCatchLonglineDto(source, target);
    }

    @Override
    protected void resetRow(int row) {
        super.resetRow(row);

        CatchLonglineUIModel model = getModel();
        model.getSizeMeasuresTableModel().resetCacheForRow(row);
        model.getWeightMeasuresTableModel().resetCacheForRow(row);
    }

    @Override
    protected CatchLonglineUIModel getModel() {
        return (CatchLonglineUIModel) super.getModel();
    }
}
