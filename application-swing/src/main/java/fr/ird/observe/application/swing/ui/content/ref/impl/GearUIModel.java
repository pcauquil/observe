package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

import java.util.Set;

/**
 * Created on 3/23/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.16
 */
public class GearUIModel extends ContentReferenceUIModel<GearDto> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_GEAR_CARACTERISTIC_TAB_VALID = "gearCaracteristicTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(GearDto.PROPERTY_URI,
                                               GearDto.PROPERTY_CODE,
                                               GearDto.PROPERTY_STATUS,
                                               GearDto.PROPERTY_NEED_COMMENT,
                                               GearDto.PROPERTY_LABEL1,
                                               GearDto.PROPERTY_LABEL2,
                                               GearDto.PROPERTY_LABEL3,
                                               GearDto.PROPERTY_LABEL4,
                                               GearDto.PROPERTY_LABEL5,
                                               GearDto.PROPERTY_LABEL6,
                                               GearDto.PROPERTY_LABEL7,
                                               GearDto.PROPERTY_LABEL8).build();

    public static final Set<String> GEAR_CARACTERISTIC_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(GearDto.PROPERTY_GEAR_CARACTERISTIC).build();

    protected boolean generalTabValid;

    protected boolean gearCaracteristicTabValid;

    public GearUIModel() {
        super(GearDto.class,
              new String[]{GearDto.PROPERTY_GEAR_CARACTERISTIC},
              new String[]{GearDto.PROPERTY_GEAR_CARACTERISTIC + SUFFIX_SELECTED});
    }

    public boolean isGearCaracteristicTabValid() {
        return gearCaracteristicTabValid;
    }

    public void setGearCaracteristicTabValid(boolean gearCaracteristicTabValid) {
        Object oldValue = isGearCaracteristicTabValid();
        this.gearCaracteristicTabValid = gearCaracteristicTabValid;
        firePropertyChange(PROPERTY_GEAR_CARACTERISTIC_TAB_VALID, oldValue, gearCaracteristicTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
