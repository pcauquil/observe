package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

import java.util.Set;

/**
 * Created on 9/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class SpeciesListUIModel extends ContentReferenceUIModel<SpeciesListDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_SPECIES_TAB_VALID = "speciesTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(SpeciesListDto.PROPERTY_URI,
                                               SpeciesListDto.PROPERTY_CODE,
                                               SpeciesListDto.PROPERTY_STATUS,
                                               SpeciesListDto.PROPERTY_NEED_COMMENT,
                                               SpeciesListDto.PROPERTY_LABEL1,
                                               SpeciesListDto.PROPERTY_LABEL2,
                                               SpeciesListDto.PROPERTY_LABEL3,
                                               SpeciesListDto.PROPERTY_LABEL4,
                                               SpeciesListDto.PROPERTY_LABEL5,
                                               SpeciesListDto.PROPERTY_LABEL6,
                                               SpeciesListDto.PROPERTY_LABEL7,
                                               SpeciesListDto.PROPERTY_LABEL8).build();

    public static final Set<String> SPECIES_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(SpeciesListDto.PROPERTY_SPECIES).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean speciesTabValid;

    public SpeciesListUIModel() {
        super(SpeciesListDto.class,
              new String[]{SpeciesListDto.PROPERTY_SPECIES},
              new String[]{SpeciesListDto.PROPERTY_SPECIES + SUFFIX_SELECTED},
              null
        );
    }

    public boolean isSpeciesTabValid() {
        return speciesTabValid;
    }

    public void setSpeciesTabValid(boolean speciesTabValid) {
        Object oldValue = isSpeciesTabValid();
        this.speciesTabValid = speciesTabValid;
        firePropertyChange(PROPERTY_SPECIES_TAB_VALID, oldValue, speciesTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
