package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDtos;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class NonTargetCatchUIModel extends ContentTableUIModel<SetSeineNonTargetCatchDto, NonTargetCatchDto> {

    private static final long serialVersionUID = 1L;

    public NonTargetCatchUIModel(NonTargetCatchUI ui) {
        super(SetSeineNonTargetCatchDto.class,
              NonTargetCatchDto.class,
              new String[]{
                      SetSeineNonTargetCatchDto.PROPERTY_NON_TARGET_CATCH},
              new String[]{
                      NonTargetCatchDto.PROPERTY_SPECIES,
                      NonTargetCatchDto.PROPERTY_CATCH_WEIGHT,
                      NonTargetCatchDto.PROPERTY_TOTAL_COUNT,
                      NonTargetCatchDto.PROPERTY_MEAN_WEIGHT,
                      NonTargetCatchDto.PROPERTY_MEAN_LENGTH,
                      NonTargetCatchDto.PROPERTY_REASON_FOR_DISCARD,
                      NonTargetCatchDto.PROPERTY_SPECIES_FATE,
                      NonTargetCatchDto.PROPERTY_TOTAL_COUNT_COMPUTED_SOURCE,
                      NonTargetCatchDto.PROPERTY_MEAN_WEIGHT_COMPUTED_SOURCE,
                      NonTargetCatchDto.PROPERTY_CATCH_WEIGHT_COMPUTED_SOURCE,
                      NonTargetCatchDto.PROPERTY_MEAN_LENGTH_COMPUTED_SOURCE,
                      NonTargetCatchDto.PROPERTY_COMMENT
              });

        List<ContentTableMeta<NonTargetCatchDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_SPECIES, false),
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_SPECIES_FATE, false),
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_REASON_FOR_DISCARD, false),
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_CATCH_WEIGHT, false),
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_TOTAL_COUNT, false),
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_MEAN_WEIGHT, false),
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_MEAN_LENGTH, false),
                ContentTableModel.newTableMeta(NonTargetCatchDto.class, NonTargetCatchDto.PROPERTY_COMMENT, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<SetSeineNonTargetCatchDto, NonTargetCatchDto> createTableModel(
            ObserveContentTableUI<SetSeineNonTargetCatchDto, NonTargetCatchDto> ui,
            List<ContentTableMeta<NonTargetCatchDto>> contentTableMetas) {

        return new ContentTableModel<SetSeineNonTargetCatchDto, NonTargetCatchDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Collection<NonTargetCatchDto> getChilds(SetSeineNonTargetCatchDto bean) {
                return bean.getNonTargetCatch();
            }

            @Override
            protected void load(NonTargetCatchDto source, NonTargetCatchDto target) {
                NonTargetCatchDtos.copyNonTargetCatchDto(source, target);
            }

            @Override
            protected void setChilds(SetSeineNonTargetCatchDto parent, List<NonTargetCatchDto> childs) {
                bean.setNonTargetCatch(childs);
            }
        };
    }
}
