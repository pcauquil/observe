/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.save;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.SqlScriptProducerService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeContext;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class SaveLocalUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(SaveLocalUIHandler.class);

    protected ObserveSwingDataSource source;

    public SaveLocalUIHandler(SaveLocalUI ui) {
        super(ui);
    }

    @Override
    public SaveLocalUI getUi() {
        return (SaveLocalUI) super.getUi();
    }

    public void initTabUI(AdminUI ui, SaveLocalUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        String message = t("observe.actions.synchro.launch.operation", t(tabUI.getStep().getOperationLabel()));
        tabUI.getStartAction().setText(message);

    }

    public void updateText() {
        String text = "";
        if (getUi().getStepModel().containsStepForsave(AdminStep.SYNCHRONIZE)) {
            text = t("observe.actions.synchro.referential.message.need.save.for.synchro.operation");
            text += "\n\n";
        }
        if (getUi().getStepModel().containsStepForsave(AdminStep.VALIDATE)) {
            text = t("observe.actions.synchro.referential.message.need.save.for.validation.operation");
            text += "\n\n";
        }
        getUi().needSaveText.setText(text);
    }

    public String updateText(boolean localSourceNeedSave) {
        return localSourceNeedSave ? t("observe.actions.synchro.referential.message.synchro.local.modification") : t("observe.actions.synchro.referential.message.no.local.modification");
    }

    public void skipOperation() {
        sendMessage(t("observe.actions.synchro.referential.message.saveLocal.skip"));
        getModel().setStepState(AdminStep.SAVE_LOCAL, WizardState.SUCCESSED);
//        getUi().getProgression().setText(t("observe.synchro.message.saveLocal.skip"));
        // on passe directement à l'opération suivante
        if (model.getNextStep() != null) {
            model.gotoNextStep();
        }
    }

    public void chooseBackupFile() {
        File f = UIHelper.chooseDirectory(
                getUi(),
                t("observe.title.choose.db.dump.directory"),
                t("observe.action.choose.db.dump.directory"),
                new File(getUi().directoryText.getText())
        );
        changeDirectory(f);
    }

    public void changeDirectory(File f) {
        getUi().getStepModel().setBackupFile(new File(f, getUi().filenameText.getText()));
    }

    public void changeFilename(String filename) {
        getUi().getStepModel().setBackupFile(new File(getUi().directoryText.getText(), filename));
    }

    public void doStartAction() {

        addAdminWorker(getUi().getStartAction().getToolTipText(), this::doAction);

    }

    public WizardState doAction() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug(this);
        }

        source = model.getSafeLocalSource(false);

        SaveLocalModel stepModel = model.getSaveLocalModel();
        if (!stepModel.isLocalSourceNeedSave()) {
            sendMessage("Aucune modification sur la base locale, opération non requise.");
            // pas de modification sur la base locale
            return WizardState.SUCCESSED;
        }

        openSource(source);

        if (stepModel.isDoBackup()) {

            sendMessage("Sauvegarde de la base locale vers " + stepModel.getBackupFile());

            // on effectue une sauvegarde de la base locale
            File backupFile = stepModel.getBackupFile();

            SqlScriptProducerService dumpService = source.newSqlScriptProducerService();
            AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData();
            byte[] dump = dumpService.produceAddSqlScript(request).getSqlCode();

            try (FileOutputStream outputStream = new FileOutputStream(backupFile)) {
                IOUtils.write(dump, outputStream);
            }

        }

        if (stepModel.containsStepForsave(AdminStep.SYNCHRONIZE)) {

            sendMessage("Sauvegarde du référentiel.");
            saveUnidirectionalSynchronizeReferential();
        }

        sendMessage(t("observe.actions.operation.message.done", new Date()));

        return WizardState.SUCCESSED;
    }

    private void saveUnidirectionalSynchronizeReferential() {

        SynchronizeModel stepModel = getModel().getSynchronizeReferentielModel();

        UnidirectionalReferentialSynchronizeContext referentialSynchronizeContext = stepModel.getReferentialSynchronizeContext();

        stepModel.getEngine().finish(source.newUnidirectionalReferentialSynchronizeLocalService(), referentialSynchronizeContext);

        sendMessage(t("observe.actions.synchro.referential.message.apply.done", new Date()));

    }

}
