package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ReferentialSynchronizeMode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.AddedReferenceReferentialSynchroNode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferenceReferentialSynchroNodeSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.RootReferentialSynchroNode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.TypeReferentialSynchroNode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.UpdatedReferenceReferentialSynchroNode;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiff;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffState;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffs;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffsEngine;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Objects;
import java.util.Optional;

/**
 * Created on 11/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeTreeModelsBuilder {

    private final ReferentialSynchronizeDiffsEngine engine;
    private final RootReferentialSynchroNode leftRootNode;
    private final RootReferentialSynchroNode rightRootNode;
    private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, String> leftIdsBuilder = ImmutableSetMultimap.builder();
    private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, String> rightIdsBuilder = ImmutableSetMultimap.builder();

    public ReferentialSynchronizeTreeModelsBuilder(ReferentialSynchronizeMode synchronizeMode, ReferentialSynchronizeDiffsEngine engine) {
        Objects.nonNull(synchronizeMode);
        Objects.nonNull(engine);
        this.engine = engine;
        this.leftRootNode = new RootReferentialSynchroNode(true, synchronizeMode.isLeftWrite());
        this.rightRootNode = new RootReferentialSynchroNode(false, synchronizeMode.isRightWrite());
    }

    public Pair<ReferentialSynchronizeTreeModel, ReferentialSynchronizeTreeModel> build() {

        ReferentialSynchronizeDiffs synchronizeDiffs = engine.build();

        ImmutableSet<Class<? extends ReferentialDto>> referentialNames = synchronizeDiffs.getReferentialNames();

        ReferentialSynchronizeDiff leftDiff = synchronizeDiffs.getLeftDiff();
        ReferentialSynchronizeDiff rightDiff = synchronizeDiffs.getRightDiff();

        boolean rightCanWrite = rightRootNode.isCanWrite();
        boolean leftCanWrite = leftRootNode.isCanWrite();

        CreateAddNode leftAddNode = new CreateAddNode(rightCanWrite, leftCanWrite, false);
        CreateAddNode rightAddNode = new CreateAddNode(leftCanWrite, rightCanWrite, false);
        CreateNode leftUpdateNode = new CreateUpdateNode(rightCanWrite, false, leftCanWrite);
        CreateNode rightUpdateNode = new CreateUpdateNode(leftCanWrite, false, rightCanWrite);

        for (Class<? extends ReferentialDto> referentialName : referentialNames) {

            {
                // Tous les référentiels ajoutés à gauche peuvent être copié à droite
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = leftDiff.getAddedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromLeft(leftRootNode, optionalDiffStates.get(), referentialName, leftAddNode);
                }
            }
            {
                // Tous les référentiels mises à jour à gauche peuvent être copié à droite
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = leftDiff.getUpdatedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromLeft(leftRootNode, optionalDiffStates.get(), referentialName, leftUpdateNode);
                }
            }
            {
                // Tous les référentiels ajoutés à droite peuvent être supprimé ou désactivés
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = rightDiff.getAddedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromRight(rightRootNode, optionalDiffStates.get(), referentialName, rightAddNode);
                }
            }
            {
                // Tous les référentiels mises à jour à droite peuvent être remis en arrière
                Optional<ImmutableSet<ReferentialSynchronizeDiffState>> optionalDiffStates = rightDiff.getUpdatedReferentials(referentialName);
                if (optionalDiffStates.isPresent()) {
                    addFromRight(rightRootNode, optionalDiffStates.get(), referentialName, rightUpdateNode);
                }
            }

        }

        ReferentialSynchronizeTreeModel leftTreeModel = new ReferentialSynchronizeTreeModel(leftRootNode, leftAddNode.getIds());
        ReferentialSynchronizeTreeModel rightTreeModel = new ReferentialSynchronizeTreeModel(rightRootNode, rightAddNode.getIds());
        return Pair.of(leftTreeModel, rightTreeModel);

    }

    private <R extends ReferentialDto> void addFromLeft(RootReferentialSynchroNode rootNode, ImmutableSet<ReferentialSynchronizeDiffState> diffStates, Class<R> referentialName, CreateNode createNode) {
        ReferentialReferenceSet<R> referenceSet = engine.getLeftReferentialReferenceSet(referentialName, diffStates);
        ImmutableSet<ReferentialReference<R>> references = referenceSet.getReferences();
        if (!references.isEmpty()) {
            TypeReferentialSynchroNode typeNode = rootNode.getOrAddTypeNode(referentialName);
            for (ReferentialReference<R> reference : references) {
                createNode.createNode(typeNode, reference);
            }

        }
    }

    private <R extends ReferentialDto> void addFromRight(RootReferentialSynchroNode rootNode, ImmutableSet<ReferentialSynchronizeDiffState> diffStates, Class<R> referentialName, CreateNode createNode) {
        ReferentialReferenceSet<R> referenceSet = engine.getRightReferentialReferenceSet(referentialName, diffStates);
        ImmutableSet<ReferentialReference<R>> references = referenceSet.getReferences();
        if (!references.isEmpty()) {
            TypeReferentialSynchroNode typeNode = rootNode.getOrAddTypeNode(referentialName);
            for (ReferentialReference<R> reference : references) {
                createNode.createNode(typeNode, reference);
            }
        }
    }

    private static abstract class CreateNode {

        protected final boolean canCopy;
        protected final boolean canDelete;
        protected final boolean canRevert;

        protected CreateNode(boolean canCopy, boolean canDelete, boolean canRevert) {
            this.canCopy = canCopy;
            this.canDelete = canDelete;
            this.canRevert = canRevert;
        }

        public abstract void createNode(TypeReferentialSynchroNode typeNode, ReferentialReference<?> reference);

    }

    private static class CreateAddNode extends CreateNode {

        private final ImmutableSetMultimap.Builder<Class<? extends ReferentialDto>, String> idsBuilder = ImmutableSetMultimap.builder();

        protected CreateAddNode(boolean canCopy, boolean canDelete, boolean canRevert) {
            super(canCopy, canDelete, canRevert);
        }

        @Override
        public void createNode(TypeReferentialSynchroNode typeNode, ReferentialReference<?> reference) {
            ReferenceReferentialSynchroNodeSupport node = new AddedReferenceReferentialSynchroNode(reference, canCopy, false, canDelete, canRevert);
            typeNode.add(node);
            idsBuilder.put(reference.getType(), reference.getId());
        }

        public ImmutableSetMultimap<Class<? extends ReferentialDto>, String> getIds() {
            return idsBuilder.build();
        }
    }

    private static class CreateUpdateNode extends CreateNode {

        protected CreateUpdateNode(boolean canCopy, boolean canDelete, boolean canRevert) {
            super(canCopy, canDelete, canRevert);
        }

        @Override
        public void createNode(TypeReferentialSynchroNode typeNode, ReferentialReference<?> reference) {
            ReferenceReferentialSynchroNodeSupport node = new UpdatedReferenceReferentialSynchroNode(reference, false, canCopy, canDelete, canRevert);
            typeNode.add(node);
        }
    }
}
