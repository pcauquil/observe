/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.constants.seine.NonTargetCatchComputedValueSource;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.ReasonForDiscardDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesFateDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDtos;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDto;
import fr.ird.observe.services.dto.seine.SetSeineNonTargetCatchDtos;
import fr.ird.observe.services.service.seine.NonTargetCatchService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class NonTargetCatchUIHandler extends ContentTableUIHandler<SetSeineNonTargetCatchDto, NonTargetCatchDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(NonTargetCatchUIHandler.class);

    /**
     * Ecoute les modifications de la propriété {@link NonTargetCatchDto#getTotalCount()},
     * et repasser alors le flag {@link NonTargetCatchDto#getTotalCountComputedSource()} à
     * {@code null}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener totalCountChanged;

    /**
     * Ecoute les modifications de la propriété {@link NonTargetCatchDto#getCatchWeight()},
     * et repasser alors le flag {@link NonTargetCatchDto#getCatchWeightComputedSource()} à
     * {@code null}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener catchWeightChanged;

    /**
     * Ecoute les modifications de la propriété {@link NonTargetCatchDto#getMeanWeight()},
     * et repasser alors le flag {@link NonTargetCatchDto#getMeanWeightComputedSource()} à
     * {@code null}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener meanWeightChanged;

    /**
     * Ecoute les modifications de la propriété {@link NonTargetCatchDto#getMeanLength()},
     * et repasser alors le flag {@link NonTargetCatchDto#getMeanLengthComputedSource()} à
     * {@code null}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener meanLengthChanged;

    public NonTargetCatchUIHandler(NonTargetCatchUI ui) {
        super(ui, DataContextType.SetSeine);
        totalCountChanged = evt -> {
            NonTargetCatchDto source = (NonTargetCatchDto) evt.getSource();
            source.setTotalCountComputedSource(null);
        };
        catchWeightChanged = evt -> {
            NonTargetCatchDto source = (NonTargetCatchDto) evt.getSource();
            source.setCatchWeightComputedSource(null);
        };
        meanWeightChanged = evt -> {
            NonTargetCatchDto source = (NonTargetCatchDto) evt.getSource();
            source.setMeanWeightComputedSource(null);
        };
        meanLengthChanged = evt -> {
            NonTargetCatchDto source = (NonTargetCatchDto) evt.getSource();
            source.setMeanLengthComputedSource(null);
        };
    }

    @Override
    public NonTargetCatchUI getUi() {
        return (NonTargetCatchUI) super.getUi();
    }

    public String getCatchWeightDataTip(NonTargetCatchComputedValueSource computed) {
        String result;

        if (computed == null) {
            result = t("observe.common.catchWeightComputed.observed.tip");
        } else {
            result = t("observe.common.catchWeightComputed.computed.tip", computed.toString());
        }
        return result;
    }

    public String getTotalCountDataTip(NonTargetCatchComputedValueSource computed) {
        String result;

        if (computed == null) {
            result = t("observe.common.totalCountComputed.observed.tip");
        } else {
            result = t("observe.common.totalCountComputed.computed.tip", computed.toString());

        }
        return result;
    }

    public String getMeanWeightDataTip(NonTargetCatchComputedValueSource computed) {
        String result;

        if (computed == null) {
            result = t("observe.common.meanWeightComputed.observed.tip");
        } else {
            result = t("observe.common.meanWeightComputed.computed.tip", computed.toString());
        }
        return result;
    }

    public String getMeanLengthDataTip(NonTargetCatchComputedValueSource computed) {
        String result;

        if (computed == null) {
            result = t("observe.common.meanLengthComputed.observed.tip");
        } else {
            result = t("observe.common.meanLengthComputed.computed.tip", computed.toString());
        }
        return result;
    }

    public void resetCatchWeightComputed() {
        getTableEditBean().setCatchWeightComputedSource(null);
        getUi().getCatchWeight().grabFocus();
    }

    public void resetTotalCountComputed() {
        getTableEditBean().setTotalCountComputedSource(null);
        getUi().getTotalCount().grabFocus();
    }

    public void resetMeanWeightComputed() {
        getTableEditBean().setMeanWeightComputedSource(null);
        getUi().getMeanWeight().grabFocus();
    }

    public void resetMeanLengthComputed() {
        getTableEditBean().setMeanLengthComputedSource(null);
        getUi().getMeanLength().grabFocus();
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, NonTargetCatchDto bean, boolean create) {
        NonTargetCatchUI ui = getUi();

        ContentTableModel<SetSeineNonTargetCatchDto, NonTargetCatchDto> model = getTableModel();

        if (!model.isEditable()) {

            // rien a faire
            return;
        }

        List<ReferentialReference<SpeciesDto>> availableEspeces;
        JComponent requestFocus;

        if (create) {
            Set<ReferentialReference<SpeciesDto>> speciesSet = getModel().getReferentialReferences(NonTargetCatchDto.PROPERTY_SPECIES);
            availableEspeces = Lists.newArrayList(speciesSet);

            requestFocus = ui.getSpecies();
        } else {
            ReferentialReference<SpeciesDto> species = bean.getSpecies();
            availableEspeces = new ArrayList<>();
            availableEspeces.add(species);
            requestFocus = ui.getTable();
        }

        ui.getSpecies().setData(availableEspeces);
        requestFocus.requestFocus();

        NonTargetCatchDto tableEditBean = getTableEditBean();
        tableEditBean.removePropertyChangeListener(NonTargetCatchDto.PROPERTY_TOTAL_COUNT, totalCountChanged);
        tableEditBean.addPropertyChangeListener(NonTargetCatchDto.PROPERTY_TOTAL_COUNT, totalCountChanged);

        tableEditBean.removePropertyChangeListener(NonTargetCatchDto.PROPERTY_CATCH_WEIGHT, catchWeightChanged);
        tableEditBean.addPropertyChangeListener(NonTargetCatchDto.PROPERTY_CATCH_WEIGHT, catchWeightChanged);

        tableEditBean.removePropertyChangeListener(NonTargetCatchDto.PROPERTY_MEAN_WEIGHT, meanWeightChanged);
        tableEditBean.addPropertyChangeListener(NonTargetCatchDto.PROPERTY_MEAN_WEIGHT, meanWeightChanged);

        tableEditBean.removePropertyChangeListener(NonTargetCatchDto.PROPERTY_MEAN_LENGTH, meanLengthChanged);
        tableEditBean.addPropertyChangeListener(NonTargetCatchDto.PROPERTY_MEAN_LENGTH, meanLengthChanged);
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.fixTableColumnWidth(table, 3, 55);
        UIHelper.fixTableColumnWidth(table, 4, 65);
        UIHelper.fixTableColumnWidth(table, 5, 75);
        UIHelper.fixTableColumnWidth(table, 6, 55);
        UIHelper.fixTableColumnWidth(table, 7, 50);

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.nonTargetCatch.table.speciesFaune"),
                n("observe.content.nonTargetCatch.table.speciesFaune.tip"),
                n("observe.content.nonTargetCatch.table.speciesFate"),
                n("observe.content.nonTargetCatch.table.speciesFate.tip"),
                n("observe.content.nonTargetCatch.table.reasonForDiscard"),
                n("observe.content.nonTargetCatch.table.reasonForDiscard.tip"),
                n("observe.content.nonTargetCatch.table.catchWeight"),
                n("observe.content.nonTargetCatch.table.catchWeight.tip"),
                n("observe.content.nonTargetCatch.table.totalCount"),
                n("observe.content.nonTargetCatch.table.totalCount.tip"),
                n("observe.content.nonTargetCatch.table.meanWeight"),
                n("observe.content.nonTargetCatch.table.meanWeight.tip"),
                n("observe.content.nonTargetCatch.table.meanLength"),
                n("observe.content.nonTargetCatch.table.meanLength.tip"),
                n("observe.content.nonTargetCatch.table.comment"),
                n("observe.content.nonTargetCatch.table.comment.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesFateDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, ReasonForDiscardDto.class));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 5, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 6, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 7, UIHelper.newStringTableCellRenderer(renderer, 10, true));
    }

    @SuppressWarnings("Duplicates")
    @Override
    protected boolean prepareSave(SetSeineNonTargetCatchDto editBean, List<NonTargetCatchDto> objets) throws Exception {
        super.prepareSave(editBean, objets);

        //on cherche si parmis les captures supprimer certaines ont des échantillon
        SetSeineNonTargetCatchDto originalSetSeineNonTargetCatchDto = getModel().getForm().getObject();

        Set<ReferentialReference<SpeciesDto>> speciesToDelete = Sets.newHashSet();

        for (NonTargetCatchDto nonTargetCatchDto : originalSetSeineNonTargetCatchDto.getNonTargetCatch()) {

            if (nonTargetCatchDto.isHasSample()) {

                ReferentialReference<SpeciesDto> species = nonTargetCatchDto.getSpecies();

                Iterable<NonTargetCatchDto> currentTargetCatchSpecies = NonTargetCatchDtos.filterBySpecies(objets, species);

                if (Iterables.isEmpty(currentTargetCatchSpecies)) {

                    speciesToDelete.add(species);

                }
            }

        }

        if (!speciesToDelete.isEmpty()) {

            // il existe des echantillon faune a supprimer on demande une
            // confirmation
            StringBuilder sb = new StringBuilder(512);
            ReferentialReferenceDecorator<SpeciesDto> decorator =
                    getDecoratorService().getReferentialReferenceDecorator(SpeciesDto.class);

            for (ReferentialReference<SpeciesDto> species : speciesToDelete) {
                sb.append("  - ").append(decorator.toString(species)).append('\n');
            }
            int reponse = UIHelper.askUser(
                    t("observe.title.need.confirm"),
                    t("observe.content.nonTargetCatch.message.table.will.delete.nonTargetSample", sb.toString()),
                    JOptionPane.ERROR_MESSAGE,
                    new Object[]{
                            t("observe.choice.continue"),
                            t("observe.choice.cancel")},
                    0);
            if (log.isDebugEnabled()) {
                log.debug("response : " + reponse);
            }
            boolean canContinue = false;
            switch (reponse) {
                case 0:
                    // wil reset ui
                    canContinue = true;
                    break;
            }
            if (!canContinue) {
                // l'utilisateur a choisi de ne pas continuer
                return false;
            }
        }

        return true;
    }

    @Override
    protected void doPersist(SetSeineNonTargetCatchDto bean) {

        SaveResultDto saveResult = getNonTargetCatchService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<SetSeineNonTargetCatchDto> form = getNonTargetCatchService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        SetSeineNonTargetCatchDtos.copySetSeineNonTargetCatchDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case NonTargetCatchDto.PROPERTY_SPECIES: {

                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListSeineNonTargetCatchId();
                String tripSeineId = getDataContext().getSelectedTripSeineId();

                TripSeineService tripSeineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
                result = (List) tripSeineService.getSpeciesByListAndTrip(tripSeineId, speciesListId);

                result = ReferentialReferences.filterEnabled(result);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected NonTargetCatchService getNonTargetCatchService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newNonTargetCatchService();
    }
}
