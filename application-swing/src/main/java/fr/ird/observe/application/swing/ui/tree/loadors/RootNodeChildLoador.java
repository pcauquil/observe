/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree.loadors;

import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.tree.ProgramLonglineNode;
import fr.ird.observe.application.swing.ui.tree.ProgramSeineNode;
import fr.ird.observe.services.dto.constants.GearType;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import jaxx.runtime.swing.nav.NavDataProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * Le chargeur des noeuds fils d'un program (les marees du programme).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class RootNodeChildLoador extends AbstractNodeChildLoador<ReferentialReference<ProgramDto>, ProgramDto> {

    private static final long serialVersionUID = 1L;

    private boolean addData;

    private boolean addReferentiel;

    public RootNodeChildLoador() {
        this(false, false);
    }

    public RootNodeChildLoador(boolean addData, boolean addReferentiel) {
        super(ProgramDto.class);
        this.addData = addData;
        this.addReferentiel = addReferentiel;
    }

    public void setAddData(boolean addData) {
        this.addData = addData;
    }

    public void setAddReferentiel(boolean addReferentiel) {
        this.addReferentiel = addReferentiel;
    }

    @Override
    public List<ReferentialReference<ProgramDto>> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) {

        DataSelectionModel selectionModel = getSelectionModel(dataProvider);

        List<ReferentialReference<ProgramDto>> data = Collections.emptyList();

        if (addData) {

            if (selectionModel != null) {
                data = new ArrayList<>(selectionModel.getDatas().keySet());
            } else {
                Set<ReferentialReference<ProgramDto>> referentialReferenceSet = getDataSource(dataProvider).getReferentialReferences(ProgramDto.class);
                data = new ArrayList<>(referentialReferenceSet);
            }

            ObserveTreeHelper.sortPrograms(data);
        }

        return data;

    }

    @Override
    public void addChildNodes(ObserveNode parentNode, List<ReferentialReference<ProgramDto>> datas, NavDataProvider dataProvider) {
        super.addChildNodes(parentNode, datas, dataProvider);
        if (addReferentiel) {
            parentNode.add(new ObserveNode(
                    String.class,
                    n("observe.type.reference.common"),
                    ObserveTreeHelper.getChildLoador(ReferenceNodeChildLoador.CommonReferenceNodeChildLoador.class),
                    true));
            parentNode.add(new ObserveNode(
                    String.class,
                    n("observe.type.reference.seine"),
                    ObserveTreeHelper.getChildLoador(ReferenceNodeChildLoador.SeineReferenceNodeChildLoador.class),
                    true));
            parentNode.add(new ObserveNode(
                    String.class,
                    n("observe.type.reference.longline"),
                    ObserveTreeHelper.getChildLoador(ReferenceNodeChildLoador.LonglineReferenceNodeChildLoador.class),
                    true));
        }
    }

    @Override
    public ObserveNode createNode(ReferentialReference<ProgramDto> data, NavDataProvider dataProvider) {

        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");

        GearType gearType = (GearType) data.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE);

        ObserveNode result;

        switch (gearType) {

            case seine:

                result = new ProgramSeineNode(data);
                break;

            case longline:

                result = new ProgramLonglineNode(data);
                break;

            default:
                throw new IllegalStateException("The program has a gearType " + gearType + " we can't deal with");

        }

        return result;

    }
}
