/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferenceReferentialSynchroNodeSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.TypeReferentialSynchroNode;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Color;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Le renderer pour décorer l'arbre de sélection des données.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class ReferentialSynchronizeTreeCellRenderer extends DefaultTreeCellRenderer {

    /** Logger */
    private static final Log log = LogFactory.getLog(ReferentialSynchronizeTreeCellRenderer.class);

    private static final long serialVersionUID = 1L;

    protected transient DecoratorService decoratorService;

    public ReferentialSynchronizeTreeCellRenderer() {

        setBackgroundNonSelectionColor(null);
        setBackgroundSelectionColor(null);
        setBackground(null);

        setTextNonSelectionColor(Color.BLACK);
        setTextSelectionColor(Color.BLUE);
    }

    public DecoratorService getDecoratorService() {
        if (decoratorService == null) {
            decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        }
        return decoratorService;
    }

    @Override
    public Component getTreeCellRendererComponent(
            JTree tree,
            Object value,
            boolean sel,
            boolean expanded,
            boolean leaf,
            int row,
            boolean hasFocus) {

        if (!(value instanceof ReferentialSynchroNodeSupport)) {
            return this;
        }

        // get the icon to set for the node
        ReferentialSynchroNodeSupport node = (ReferentialSynchroNodeSupport) value;

        String text = null;
        Icon icon = null;
        if (node instanceof TypeReferentialSynchroNode) {
            TypeReferentialSynchroNode node1 = (TypeReferentialSynchroNode) node;
            icon = node1.getIcon();
            text = "<html><body>" + t(ObserveI18nDecoratorHelper.getTypeI18nKey(node1.getUserObject()));
            int childCount = node1.getChildCount();
            text += " <i>(" + childCount + ")</i>";
        } else if (node instanceof ReferenceReferentialSynchroNodeSupport) {
            ReferenceReferentialSynchroNodeSupport node1 = (ReferenceReferentialSynchroNodeSupport) node;

            icon = node1.getIcon();
            ReferentialReference reference = node1.getUserObject();
            Decorator<?> decorator = getDecoratorService().getReferentialReferenceDecorator(reference.getType());
            text = "<html><body>" + decorator.toString(reference);
            text += " <i>(" + reference.getVersion() + " - " + reference.getLastUpdateDate() + ")</i>";
        }

        Component comp = super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);

        setIcon(icon);
        setToolTipText(text);
        return comp;
    }

    @Override
    public Color getBackgroundNonSelectionColor() {
        // Fixes  http://forge.codelutin.com/issues/830 for jdk 7
        return Color.WHITE;
    }
}
