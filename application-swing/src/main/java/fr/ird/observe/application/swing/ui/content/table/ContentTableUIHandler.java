/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.WeightMeasuresTableModel;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.util.table.EditableTableModelSupport;
import fr.ird.observe.application.swing.ui.util.table.InlineTableAutotSelectRowAndShowPopupAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur d'un écran d'édition avec tableau.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class ContentTableUIHandler<E extends IdDto, D extends IdDto> extends ContentUIHandler<E> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentTableUIHandler.class);

    protected ContentTableUIHandler(ObserveContentTableUI<E, D> ui, DataContextType dataContextType) {
        super(ui, dataContextType, null);
    }

    /**
     * Hook lors du changement d'entrée éditée.
     *
     * @param editingRow la nouvelle entrée à editer
     * @param bean       le bean d'édition d'une entrée
     * @param create     un drapeau pour savoir si l'entrée est en création
     */
    protected abstract void onSelectedRowChanged(int editingRow, D bean, boolean create);

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        // par defaut, on suppose qu'on peut afficher les données
        getModel().setShowData(true);

        String activityId = dataContext.getSelectedActivityId();

        if (getOpenDataManager().isOpenActivity(activityId)) {

            // mode mise a jour
            return ContentMode.UPDATE;
        }

        // mode lecture

        if (dataContext.isSelectedActivityLongline()) {

            // l'activité n'est pas ouverte, mode lecture
            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivityLonglineDto.class),
                       t("observe.storage.activityLongline.message.not.open"));

        } else {

            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivitySeineDto.class),
                       t("observe.storage.activitySeine.message.not.open"));

        }

        return ContentMode.READ;
    }

    @Override
    public ContentTableUIModel<E, D> getModel() {
        return (ContentTableUIModel<E, D>) super.getModel();
    }

    @Override
    public ObserveContentTableUI<E, D> getUi() {
        return (ObserveContentTableUI<E, D>) super.getUi();
    }

    public final D getTableEditBean() {
        return getModel().getTableEditBean();
    }

    public final void updateEditor(ListSelectionEvent event) {

        final ObserveContentTableUI<E, D> ui = getUi();

        if (ui.getValidatorTable() == null) {
            log.debug("skip validator is null!");
            return;
        }
        if (getBean() == null) {
            log.debug("skip bean is null!");
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug(event);
        }
        if (getTableModel().isEmpty()) {
            // le modele est vide, on ne propage rien
            //FIXME le modèle de selection ne devrait plus declancher des evenement
            //FIXME des que le modele est vide ?
            log.debug("empty model, do nothing");
            return;
        }

        ListSelectionModel sModel = (ListSelectionModel) event.getSource();
        int newIndex = sModel.getAnchorSelectionIndex();

        if (newIndex == -1 || sModel.isSelectionEmpty()) {
            //FIXME le modèle de selection ne devrait plus declancher des evenement
            //FIXME des que le modele est vide ?
            log.debug("skip with row == -1, or empty selection, do nothing");
            return;
        }

        SwingUtilities.invokeLater(() -> {
            if (!ui.getSelectionModel().isSelectionEmpty()) {
                // on veut toujours que la ligne sélectionnée soit visible
                Rectangle rect = ui.getTable().getCellRect(ui.getSelectionModel().getAnchorSelectionIndex(), 0, false);
                ui.getTable().scrollRectToVisible(rect);
            }
        });

        int selectedRow = getTableModel().getSelectedRow();
        if (newIndex == selectedRow) {
            // on bloque du code re-entrant
            log.debug("new index already set in model " + newIndex + ", do nothing");
            return;
        }
        // on doit changer de ligne selectionne dans le modele
        getTableModel().changeSelectedRow(newIndex);
    }

    protected abstract void initTableUI(DefaultTableCellRenderer renderer);

    protected abstract String getEditBeanIdToLoad();

    protected E loadEditBean(ContentMode mode) {

        String id = getEditBeanIdToLoad();

        if (id == null) {
            throw new IllegalStateException("Could not find id form " + this);
        }

        E editBean = getBean();

        // preparation du bean d'édition
        loadEditBean(id);

        // initialisation du modèle du tableau
        getUi().getTableModel().attachModel();

        return editBean;
    }

    protected abstract void loadEditBean(String beanId);

    //FIXME
//    @Override
//    protected final void onLoad(TopiaContext tx, E bean) throws TopiaException {
//        super.onLoad(tx, bean);
//
//        // on duplique la liste des fils traites dans le tableau
//        // sinon on reste sur les references d'origine et on aura pas
//        // la possibilité d'annuler l'edition ensuite car on aura modifie
//        // les entrées d'origine...
//        E editBean = getBean();
//        Collection<D> data = loadChilds(tx, editBean);
//        getModel().getChildsUpdator().setChilds(editBean, data);
//    }

    //FIXME
//    protected Collection<D> loadChilds(TopiaContext tx, E bean) throws TopiaException {
//        Collection<D> childs = getModel().getChildsUpdator().getChilds(bean);
//        Collection<D> data = new ArrayList<D>();
//        if (CollectionUtils.isNotEmpty(childs)) {
//            Loador<D> loader = getModel().getChildLoador();
//            TopiaDAO<D> dao = getMainDataSource().getDAO(tx, getTableEditBean());
//            for (D c : childs) {
//                try {
//                    D cUp = dao.findByTopiaId(c.getTopiaId());
//                    D c2 = getModel().newTableEditBean();
//                    loader.load(cUp, c2, true);
//                    data.add(c2);
//                } catch (Exception e) {
//                    throw new RuntimeException(e);
//                }
//            }
//        }
//        return data;
//    }

    @Override
    public void initUI() {

        final ContentTableUIInitializer<E, D, ObserveContentTableUI<E, D>> uiInitializer =
                new ContentTableUIInitializer<>(getUi());
        uiInitializer.initUI();

        getModel().addPropertyChangeListener(ContentUIModel.PROPERTY_FORM, evt -> updateUiWithReferenceSetsFromModel());

    }

    @Override
    public void openUI() {

        super.openUI();

        // récupération du mode de l'écran
        ContentMode mode = computeContentMode();

        ObserveContentTableUI<E, D> ui = getUi();

        int oldIndex = ui.getTableModel().getSelectedRow();

        ui.getTableModel().dettachModel();

        getModel().setMode(mode);

        // chargement du bean d'édition
        loadEditBean(mode);

        boolean canEdit = mode == ContentMode.UPDATE;

        if (canEdit) {
            // on lance le mode edition
            ui.startEdit(null);
        }

        if (!ui.getTableModel().isEmpty()) {
            if (ui.getTableModel().getRowCount() <= oldIndex) {
                oldIndex = 0;
            }
            if (oldIndex == -1) {
                oldIndex = 0;
            }

            // le tableau contient au moins une entrée
            // on la sélectionne la première entrée
            ui.getTableModel().changeSelectedRow(oldIndex);
        }

        if (canEdit) {

            // on force l'ecran en non modification
            //FIXME normalement, on ne devrait pas a avoir a faire ca ?
            //FIXME mais il est possible que les validateurs modifient l'état
            //FIXME modified sur l'ecran lors des bindings d'initilisation...
            getModel().setModified(false);
        }
    }

    @Override
    public void startEditUI(String... binding) {

        addUpdateInfoMessage();
        super.startEditUI(binding);
    }

    protected void addUpdateInfoMessage() {
        String message = n("observe.entity.message.updating");
        ObserveNode node = getTreeHelper(getUi()).getSelectedNode();
        String entityLabel =
                getTypeI18nKey(node.getParent().getInternalClass());
        message = t(message, t(entityLabel));
        addMessage(getUi(), NuitonValidatorScope.INFO, entityLabel, message);
    }

    public void removeSelectedRow(int selectedRow) {
        try {
            getTableModel().doRemoveRow(selectedRow, false);
        } finally {

            // always reset busy model to false
            ObserveSwingApplicationContext.get().getMainUI().setBusy(false);
        }
    }

    @Override
    protected boolean doSave(E bean) throws Exception {

        ObserveContentTableUI<E, D> ui = getUi();

        List<D> objets = ui.getTableModel().getData();

        boolean canContinue;
        try {
            canContinue = prepareSave(bean, objets);
        } catch (Exception e) {
            UIHelper.handlingError(e);
            canContinue = false;
        }
        if (!canContinue) {

            // l'utilisateur a choisi de ne pas sauvegarder
            return false;
        }
        doPersist(bean);

        return true;
    }

    protected abstract void doPersist(E bean);

    //FIXME
//    @Override
//    protected E onUpdate(TopiaContext tx, Object parentBean, E beanToSave) throws TopiaException {
//
//        List<D> childs = getUi().getTableModel().getData();
//
//        E editBean = getBean();
//
//        TopiaEntityBinder<E> binder = getLoadBinder();
//
//        Loador<D> childBinder = getModel().getChildLoador();
//
//        EntityListUpdator<E, D> childUpdator = getModel().getChildsUpdator();
//
//        // recopie des propriétés du bean en excluant toujours la liste des fils
//        binder.copyExcluding(editBean, beanToSave, childUpdator.getPropertyName());
//
//        // on conserve l'ancienne liste des fils (pour traitement ultérieure)
//        Collection<D> oldChilds =
//                new ArrayList<D>(childUpdator.getChilds(beanToSave));
//
//        // suppression des fils dans le bean a sauver
//        childUpdator.removeAll(beanToSave);
//
//        TopiaDAO<D> dao = ObserveDAOHelper.getDAO(tx, getModel().getChildType());
//
//        for (D child : childs) {
//
//            D childToSave;
//
//            if (child.getTopiaId() == null) {
//
//                // creation du fils
//                Map<String, Object> map = childBinder.obtainProperties(child);
//
//                childToSave = dao.create(map);
//
//            } else {
//
//                // mise a jour du fils
//
//                childToSave = dao.findByTopiaId(child.getTopiaId());
//
//                childBinder.load(child, childToSave, false);
//            }
//
//            // ajout du fils au bean à sauver
//            childUpdator.addToList(beanToSave, childToSave);
//        }
//
//        // on donne la main aux implantations pour faire des traitements
//        // supplémentaires
//        onUpdateFinalize(tx, beanToSave, oldChilds);
//
//        return beanToSave;
//    }

    @Override
    protected void afterSave(boolean refresh) {

        // on recharge l'écran
        resetEditUI();
    }

    protected boolean prepareSave(E editBean, List<D> objets) throws Exception {

        // par defaut, rien de specifique a faire avant de faire la sauvegarde
        return true;
    }

    //FIXME
//    protected void onUpdateFinalize(TopiaContext tx, E bean, Collection<D> oldChilds) throws TopiaException {
//        // par défaut, rien à faire
//    }

    protected ContentTableModel<E, D> getTableModel() {
        return getUi().getTableModel();
    }

    protected void resetEditBean() {
        getTableModel().resetEditBean();
    }

    /**
     * @param weightCategory la catégorie de poids
     * @return la référence sur l'espèce de la catégorie de poids
     */
    protected Optional<ReferentialReference<SpeciesDto>> getWeightCategorySpecies(ReferentialReference<WeightCategoryDto> weightCategory) {

        String speciesId = (String) weightCategory.getPropertyValue(WeightCategoryDto.PROPERTY_SPECIES);

        return getModel().tryGetReferentialReferenceById(TargetCatchDto.PROPERTY_SPECIES, speciesId);
    }

    protected <EE extends Serializable, MM extends EditableTableModelSupport<EE>> void initInlineTable(JScrollPane pane,
                                                                                                       JTable table,
                                                                                                       MM tableModel,
                                                                                                       PropertyChangeListener modifiedListener,
                                                                                                       JPopupMenu popup,
                                                                                                       JMenuItem addMeasure,
                                                                                                       JMenuItem deleteMeasure) {

        tableModel.addPropertyChangeListener(WeightMeasuresTableModel.MODIFIED_PROPERTY, modifiedListener);

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableModel.installSelectionListener(table);
        tableModel.installTableKeyListener(table);

        tableModel.addTableModelListener(e -> {
            if (e.getType() == TableModelEvent.DELETE) {
                tableModel.validate();
            }
        });

        new InlineTableAutotSelectRowAndShowPopupAction<>(
                pane,
                table,
                tableModel,
                popup,
                addMeasure,
                deleteMeasure);

    }
}
