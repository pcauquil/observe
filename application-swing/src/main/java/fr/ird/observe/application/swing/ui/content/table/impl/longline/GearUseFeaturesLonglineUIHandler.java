package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDtos;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDtos;
import fr.ird.observe.services.dto.referential.GearDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.service.longline.TripLonglineGearUseService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIInitializer;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.GearUseFeatureMeasurementCellEditor;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.GearUseFeatureMeasurementCellRenderer;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.16
 */
public class GearUseFeaturesLonglineUIHandler extends ContentTableUIHandler<TripLonglineGearUseDto, GearUseFeaturesLonglineDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(GearUseFeaturesLonglineUIHandler.class);

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    private final PropertyChangeListener measurementsTableModelModified;

    public GearUseFeaturesLonglineUIHandler(GearUseFeaturesLonglineUI ui) {
        super(ui, DataContextType.TripLongline);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
        measurementsTableModelModified = evt -> onMeasurementsTableModelModified((Boolean) evt.getNewValue());
    }

    @Override
    public GearUseFeaturesLonglineUI getUi() {
        return (GearUseFeaturesLonglineUI) super.getUi();
    }

    @Override
    public GearUseFeaturesLonglineUIModel getModel() {
        return (GearUseFeaturesLonglineUIModel) super.getModel();
    }

    @Override
    public void initUI() {

        super.initUI();

        getModel().addPropertyChangeListener(GearUseFeaturesLonglineUIModel.PROPERTY_EDITABLE, evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            setTableModelEditable(newValue);
        });

        setTableModelEditable(getModel().isEditable());

        getTableModel().setGearUseFeaturesLonglineUIHandler(this);

    }

    @Override
    public void openUI() {

        if (log.isInfoEnabled()) {
            log.info("OpenUI: " + getModel());
        }

        super.openUI();

        // To be sure always remove listener (could prevent some leaks)
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        getUi().getErrorTableModel().addTableModelListener(computeTabValidStateListener);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

    }

    @Override
    public void startEditUI(String... binding) {

        getModel().getMeasurementsTableModel().setEditable(true);

        super.startEditUI(binding);

    }

    public void addMeasurement() {

        GearUseFeaturesMeasurementLonglinesTableModel tableModel = getUi().getMeasurementsTableModel();
        tableModel.addNewRow();

    }

    public void deleteSelectedMeasurement() {

        GearUseFeaturesMeasurementLonglinesTableModel tableModel = getUi().getMeasurementsTableModel();

        boolean selectionEmpty = tableModel.isSelectionEmpty();

        if (!selectionEmpty) {

            GearUseFeaturesMeasurementLonglineDto data = tableModel.getSelectedRow();

            if (log.isInfoEnabled()) {
                log.info("Delete: " + data);
            }

            ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
            int response = UIHelper.askUser(mainUI,
                                            t("observe.title.delete"),
                                            t("observe.content.measurement.delete.message"),
                                            JOptionPane.WARNING_MESSAGE,
                                            new Object[]{t("observe.choice.confirm.delete"),
                                                         t("observe.choice.cancel")},
                                            1);

            if (response != 0) {

                // user cancel
                return;
            }

            tableModel.removeSelectedRow();

        }

    }

    public List<GearUseFeaturesMeasurementLonglineDto> getDefaultGearUseFeaturesMeasurementLongline(String gearId) {

        ReferentialService referentialService = getDataSource().newReferentialService();

        Form<GearDto> gearDtoForm = referentialService.loadForm(GearDto.class, gearId);

        GearDto gearDto = gearDtoForm.getObject();

        return Lists.newArrayList(
                Iterables.transform(gearDto.getGearCaracteristic(),
                                    input -> {
                                        GearUseFeaturesMeasurementLonglineDto measurementLonglineDto = new GearUseFeaturesMeasurementLonglineDto();
                                        measurementLonglineDto.setGearCaracteristic(input);
                                        String gearCaracterisiticTypeId = (String) input.getPropertyValue(GearCaracteristicDto.PROPERTY_GEAR_CARACTERISTIC_TYPE);
                                        if (GearCaracteristicTypeDtos.isBoolean(gearCaracterisiticTypeId)) {
                                            measurementLonglineDto.setMeasurementValue(Boolean.FALSE.toString());
                                        }
                                        return measurementLonglineDto;
                                    }));
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, GearUseFeaturesLonglineDto bean, boolean create) {

        if (log.isInfoEnabled()) {
            log.info("Selected row changed: " + editingRow + ", create? " + create);
        }

        GearUseFeaturesLonglineUI ui = getUi();

        UIHelper.stopEditing(ui.getMeasurementsTable());

        GearUseFeaturesLonglineTableModel tableModel = getTableModel();
        GearUseFeaturesLonglineUIModel model = getModel();

        boolean emptySelection = editingRow == -1;

        // load size measures

        GearUseFeaturesMeasurementLonglinesTableModel measurementsTableModel = model.getMeasurementsTableModel();
        List<GearUseFeaturesMeasurementLonglineDto> measurements = emptySelection ? Collections.emptyList() : measurementsTableModel.getCacheForRow(editingRow);
        if (measurements == null) {

            if (log.isInfoEnabled()) {
                log.info("init measurements for row " + editingRow);
            }

            // first time coming on this row

            if (tableModel.isCreate()) {

                // create mode: just init with empty list
                measurements = Collections.emptyList();

                if (log.isInfoEnabled()) {
                    log.info("create mode, use an empty list");
                }

            } else {

                measurements = Lists.newArrayList(bean.getGearUseFeaturesMeasurement());

            }

            // init measurements
            measurementsTableModel.initCacheForRow(editingRow, measurements);

        } else {

            if (log.isInfoEnabled()) {
                log.info("Using existing measurements for row " + editingRow + " : " + measurements.size());
            }
        }

        measurementsTableModel.setData(measurements);

        measurementsTableModel.setModified(false);
        measurementsTableModel.setEditable(tableModel.isEditable());

        if (!tableModel.isEditable()) {
            return;
        }

        JComponent requestFocus;

        if (tableModel.isCreate()) {

            // go back to first pane
            ui.getGearUseFeaturesTabPane().setSelectedIndex(0);

            requestFocus = ui.getGear();

        } else {

            requestFocus = ui.getGear();

        }

        requestFocus.requestFocus();

    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        {
            JTable table = getUi().getTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.gearUseFeaturesLongline.table.gear"),
                                                n("observe.content.gearUseFeaturesLongline.table.gear.tip"),
                                                n("observe.content.gearUseFeaturesLongline.table.number"),
                                                n("observe.content.gearUseFeaturesLongline.table.number.tip"),
                                                n("observe.content.gearUseFeaturesLongline.table.usedInTrip"),
                                                n("observe.content.gearUseFeaturesLongline.table.usedInTrip.tip"),
                                                n("observe.content.gearUseFeaturesLongline.table.comment"),
                                                n("observe.content.gearUseFeaturesLongline.table.comment.tip"));

            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, GearDto.class));
            UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 2, UIHelper.newBooleanTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 3, UIHelper.newStringTableCellRenderer(renderer, 10, true));
        }

        {
            // init measurements table
            JTable table = getUi().getMeasurementsTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.gearUseFeaturesLongline.table.gearCaracteristic"),
                                                n("observe.content.gearUseFeaturesLongline.table.gearCaracteristic.tip"),
                                                n("observe.content.gearUseFeaturesLongline.table.value"),
                                                n("observe.content.gearUseFeaturesLongline.table.value.tip"));

            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, GearCaracteristicDto.class));
            UIHelper.setTableColumnRenderer(table, 1, new GearUseFeatureMeasurementCellRenderer(0, renderer));

            table.getTableHeader().setReorderingAllowed(false);

            ReferentialReferenceDecorator<GearCaracteristicDto> decorator = getReferentialReferenceDecorator(GearCaracteristicDto.class);

            List<ReferentialReference<GearCaracteristicDto>> caracteristics = Lists.newArrayList(getDataSource().getReferentialReferences(GearCaracteristicDto.class));

            UIHelper.setTableColumnEditor(table, 0, ContentUIInitializer.newDataColumnEditor(caracteristics, decorator));
            UIHelper.setTableColumnEditor(table, 1, new GearUseFeatureMeasurementCellEditor(0));

            initInlineTable(getUi().getMeasurementsScrollPane(),
                            table,
                            getModel().getMeasurementsTableModel(),
                            measurementsTableModelModified,
                            getUi().getMeasurementsTablePopup(),
                            getUi().getAddMeasurement(),
                            getUi().getDeleteSelectedMeasurement());

        }

        // Adapt layout to let more place for the editor

        getUi().getShowForm().remove(1);
        getUi().getShowForm().add(SwingUtil.boxComponentWithJxLayer(getUi().getEditor()), new GridBagConstraints(0, 1, 1, 1, 1.0, 0.3, 10, 1, new Insets(0, 0, 0, 0), 0, 0), 1);
        getUi().getEditor().remove(0);
        getUi().getEditor().add(SwingUtil.boxComponentWithJxLayer(getUi().getEditorPanel()), new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 10, 1, new Insets(0, 0, 0, 0), 0, 0), 0);


    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedTripLonglineId();
    }

    @Override
    protected void closeSafeUI() {

        if (log.isInfoEnabled()) {
            log.info("CloseUI: " + getModel());
        }
        super.closeSafeUI();

        // remove listener
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);

    }

    @Override
    protected void resetEditBean() {

        UIHelper.stopEditing(getUi().getMeasurementsTable());

        super.resetEditBean();

    }

    @Override
    public void resetEditUI() {

        getModel().getMeasurementsTableModel().clear();

        super.resetEditUI();

    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        // par defaut, on suppose qu'on peut afficher les données
        getModel().setShowData(true);

        String selectedTripId = dataContext.getSelectedTripId();

        if (getOpenDataManager().isOpenTripLongline(selectedTripId)) {

            // mode mise a jour
            return ContentMode.UPDATE;
        }

        // mode lecture : la marée n'est pas ouverte

        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(TripLonglineDto.class),
                   t("observe.content.tripLongline.message.not.open"));

        return ContentMode.READ;
    }

    @Override
    protected GearUseFeaturesLonglineTableModel getTableModel() {
        return (GearUseFeaturesLonglineTableModel) super.getTableModel();
    }

    protected void setTableModelEditable(Boolean newValue) {

        getModel().getMeasurementsTableModel().setEditable(newValue);

    }

    protected void onMeasurementsTableModelModified(Boolean newValue) {

        if (newValue) {

            // modify the validator, since this is the best way to prevent table edit form actions
            // that something was modified on the form
            getUi().getValidatorTable().setChanged(true);

        }

        // recompute table model valid state
        getModel().getMeasurementsTableModel().validate();

    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean generalTabValid = !errorProperties.removeAll(GearUseFeaturesLonglineUIModel.GENERAL_TAB_PROPERTIES);

        GearUseFeaturesLonglineUIModel model = getModel();

        model.setGeneralTabValid(generalTabValid);

    }

    @Override
    protected void doPersist(TripLonglineGearUseDto bean) {

        // On ne persiste pas les measurements qui sont vides
        for (GearUseFeaturesLonglineDto gearUseFeatures : bean.getGearUseFeaturesLongline()) {
            if (!gearUseFeatures.isGearUseFeaturesMeasurementEmpty()) {
                Iterator<GearUseFeaturesMeasurementLonglineDto> iterator = gearUseFeatures.getGearUseFeaturesMeasurement().iterator();
                while (iterator.hasNext()) {
                    GearUseFeaturesMeasurementLonglineDto gearUseFeaturesMeasurement = iterator.next();
                    if (gearUseFeaturesMeasurement.isEmpty()) {
                        iterator.remove();
                    }
                }
            }
        }
        SaveResultDto saveResult = getTripLonglineGearUseService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<TripLonglineGearUseDto> form = getTripLonglineGearUseService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        TripLonglineGearUseDtos.copyTripLonglineGearUseDto(form.getObject(), getBean());
    }

    protected TripLonglineGearUseService getTripLonglineGearUseService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripLonglineGearUseService();
    }

}
