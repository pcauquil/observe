/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage.tabs;


import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.ObserveDbUserDtos;
import fr.ird.observe.services.dto.constants.ObserveDbRole;

import javax.swing.table.AbstractTableModel;
import java.util.Collections;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modèle pour la tableau dans l'export GPS qui contient les activités et les
 * points gps calculés via le fichier gps importé.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class RolesTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    public static final String[] COLUMN_NAMES = {
            n("observe.security.role"),
            n("observe.security.unused"),
            n("observe.security.referentiel"),
            n("observe.security.utilisateur"),
            n("observe.security.technicien")
    };

    public static final String[] COLUMN_NAME_TIPS = {
            n("observe.security.role.tip"),
            n("observe.security.unused.tip"),
            n("observe.security.referentiel.tip"),
            n("observe.security.utilisateur.tip"),
            n("observe.security.technicien.tip")
    };

    protected static final Class<?>[] COLUMN_CLASSES = {
            String.class,
            Boolean.class,
            Boolean.class,
            Boolean.class,
            Boolean.class
    };

    protected SecurityModel model;

    protected List<ObserveDbUserDto> roles;

    public RolesTableModel() {
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // seule la première colonne n'est pas éditable (on ne peut pas
        // changer le lastName d'un role)
        return columnIndex > 0;
    }


    /**
     * Initialise le modèle.
     *
     * @param model le modèle de sécurité attaché
     */
    public void init(SecurityModel model) {

        this.model = model;
        this.roles = Lists.newArrayList(model.getUsersWithoutAdministrator());
        Collections.sort(this.roles, ObserveDbUserDtos.getUserDtoComparator());
        fireTableDataChanged();
    }

    public void clear() {
        model = null;
        roles = null;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return roles == null ? 0 : roles.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_CLASSES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;

        ObserveDbUserDto user = getUser(rowIndex);
        if (user != null) {

            switch (columnIndex) {
                case 0:
                    value = user.getName();
                    break;
                case 1:
                    value = ObserveDbRole.UNUSED.equals(user.getRole());
                    break;
                case 2:
                    value = ObserveDbRole.REFERENTIAL.equals(user.getRole());
                    break;
                case 3:
                    value = ObserveDbRole.USER.equals(user.getRole());
                    break;
                case 4:
                    value = ObserveDbRole.TECHNICAL.equals(user.getRole());
                    break;
                default:
                    throw new IllegalStateException(
                            "can not get value for row " + rowIndex +
                            ", col " + columnIndex);
            }
        }
        return value;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        if (columnIndex == 0) {

            // pas possible
            return;
        }

        Boolean value = (Boolean) aValue;
        ObserveDbUserDto userDto = getUser(rowIndex);

        ObserveDbRole role;

        if (!value) {
            role = null;
        } else {
            switch (columnIndex) {
                case 2:
                    role = ObserveDbRole.REFERENTIAL;
                    break;
                case 3:
                    role = ObserveDbRole.USER;
                    break;
                case 4:
                    role = ObserveDbRole.TECHNICAL;
                    break;
                default:
                    role = ObserveDbRole.UNUSED;
            }
        }

        model.setRole(userDto, role, true);

        // toute la ligne (sauf le role change)
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

    protected ObserveDbUserDto getUser(int rowIndex) {
        return roles == null ? null : roles.get(rowIndex);
    }

}
