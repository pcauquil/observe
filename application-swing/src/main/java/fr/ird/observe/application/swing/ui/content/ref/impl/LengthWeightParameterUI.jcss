/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#generalTab {
  title:{t("observe.content.lengthWeightParameter.tab.general")};
  icon:{getHandler().getErrorIconIfFalse(model.isGeneralTabValid())};
}

#otherTab {
  title:{t("observe.content.lengthWeightParameter.tab.other")};
  icon:{getHandler().getErrorIconIfFalse(model.isOtherTabValid())};
}

#speciesLabel {
  text:"observe.common.species";
  font-style:"italic";
  labelFor:{species};
}

#sexLabel {
  text:"observe.common.sex";
  font-style:"italic";
  labelFor:{sex};
}

#sex {
  property:{LengthWeightParameterDto.PROPERTY_SEX};
  selectedItem:{bean.getSex()};
  enabled:{model.isCreatingMode()};
}

#species {
  property:{LengthWeightParameterDto.PROPERTY_SPECIES};
  selectedItem:{bean.getSpecies()};
  enabled:{model.isCreatingMode()};
}

#oceanLabel {
  text:"observe.common.ocean";
  font-style:"italic";
  labelFor:{ocean};
}

#ocean {
  property:{LengthWeightParameterDto.PROPERTY_OCEAN};
  selectedItem:{bean.getOcean()};
  enabled:{model.isCreatingMode()};
}

#startDateFinValiditeLabel {
  text:"observe.common.startDateFinValidite";
  font-style:"italic";
}

#startDate {
  date:{bean.getStartDate()};
}

#endDate {
  date:{bean.getEndDate()};
}

#relationTable {
  border:{new TitledBorder(t("observe.common.equation"))};
}

#lengthWeightFormulaInformation {
  font-size:11;
  text:"observe.content.label.lengthWeightFormula.info";
  actionIcon:"information";
}

#lengthWeightFormulaLabel {
  text:"observe.common.lengthWeightFormula";
  labelFor:{lengthWeightFormula};
}

#lengthWeightFormula {
  text:{getStringValue(bean.getLengthWeightFormula())};
}

#weightLengthFormulaInformation {
  font-size:11;
  text:"observe.content.label.weightLengthFormula.info";
  actionIcon:"information";
}
#weightLengthFormulaLabel {
  text:"observe.common.weightLengthFormula";
  labelFor:{weightLengthFormula};
}

#weightLengthFormula {
  text:{getStringValue(bean.getWeightLengthFormula())};
}

#coefficientsInformation {
  font-size:11;
  text:"observe.content.label.coefficients.info";
  actionIcon:"information";
}
#coefficientsLabel {
  text:"observe.common.coefficients";
  labelFor:{coefficients};
}

#coefficients {
  text:{getStringValue(bean.getCoefficients())};
}

#meanValuesTable {
  border:{new TitledBorder(t("observe.common.meanValues"))};
}

#meanLengthLabel {
  text:"observe.common.meanLength";
  labelFor:{meanLength};
}

#meanLength {
  property:{LengthWeightParameterDto.PROPERTY_MEAN_LENGTH};
  model:{bean.getMeanLength()};
}

#meanWeightLabel {
  text:"observe.common.meanWeight";
  labelFor:{meanWeight};
}

#meanWeight {
  property:{LengthWeightParameterDto.PROPERTY_MEAN_WEIGHT};
  model:{bean.getMeanWeight()};
}
