/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.ActivityLonglinesUI;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.tree.TripLonglineNode;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.service.longline.ActivityLonglineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Action pour changer le programme d'une ou plusieurs marée dans la liste.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveActivityLonglinesUIAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(MoveActivityLonglinesUIAction.class);

    public static final String ACTION_NAME = "moveActivityLonglines";

    public MoveActivityLonglinesUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.content.action.move.activities.longline"),
              n("observe.content.action.move.activities.longline.tip"),
              "move-activities"
        );
    }

    @Override
    public void actionPerformed(final ActionEvent e) {

        SwingUtilities.invokeLater(() -> {
            JComponent c = (JComponent) e.getSource();
            ContentUI<?> ui = (ContentUI<?>)
                    c.getClientProperty("ui");
            if (ui == null) {
                throw new IllegalStateException(
                        "could not find client property " +
                                "ui on component" + c);
            }

            if (!(ui instanceof ActivityLonglinesUI)) {
                throw new IllegalStateException("Can not come here!");
            }
            ActivityLonglinesUI activityLonglinesUI = (ActivityLonglinesUI) ui;


            // get current triplongline id
            ObserveTreeHelper treeHelper = getMainUI().getTreeHelper();
            ObserveNode oldActivitiesNode = treeHelper.getSelectedNode();
            ObserveNode oldTripLonglineNode = oldActivitiesNode.getParent();

            // choose the new tripLongline
            String tripLonglineId = chooseNewTripLongline(ui, oldTripLonglineNode);

            if (tripLonglineId != null) {

                if (log.isInfoEnabled()) {
                    log.info("Will move activities to trip: " + tripLonglineId);
                }
                // change the tripLongline of the selected activities
                List<DataReference<ActivityLonglineDto>> selectedDatas = activityLonglinesUI.getModel().getSelectedDatas();
                List<String> activityIds = selectedDatas.stream()
                                                        .map(DataReference.ID_FUNCTION)
                                                        .collect(Collectors.toList());
                ActivityLonglineService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivityLonglineService();
//                List<Integer> positions =
                service.moveActivityLonglinesToTripLongline(activityIds, tripLonglineId);

                // update the tree
                updateTree(oldActivitiesNode, tripLonglineId, activityIds);
            }

        });

    }

    protected String chooseNewTripLongline(ContentUI<?> ui, ObserveNode oldTripLonglineNode) {
        ObserveNode programNode = oldTripLonglineNode.getParent();
        String oldTripLonglineId = oldTripLonglineNode.getId();
        int tripLonglineNb = programNode.getChildCount();

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        DataReferenceDecorator<TripLonglineDto> decorator = applicationContext.getDecoratorService().getDataReferenceDecorator(TripLonglineDto.class);

        //on crée un tableau avec une marée en moins car on ne propose pas la marée actuelle
        DecoratedNodeEntity[] decoratedTripLonglines = new DecoratedNodeEntity[tripLonglineNb - 1];

        int j = 0;
        for (int i = 0; i < tripLonglineNb; i++) {

            TripLonglineNode tripLonglineNode = (TripLonglineNode) programNode.getChildAt(i);

            String tripLonglineId = tripLonglineNode.getId();

            if (!oldTripLonglineId.equals(tripLonglineId)) {
                decoratedTripLonglines[j++] = DecoratedNodeEntity.newDecoratedNodeEntity(tripLonglineNode, decorator);
            }
        }

        Object decoratedTripLongline = JOptionPane.showInputDialog(ui,
                                                                   t("observe.action.choose.tripLongline.message"),
                                                                   t("observe.action.choose.tripLongline.title"),
                                                                   JOptionPane.QUESTION_MESSAGE,
                                                                   null,
                                                                   decoratedTripLonglines,
                                                                   null);
        return decoratedTripLongline != null ? ((DecoratedNodeEntity) decoratedTripLongline).getId() : null;
    }

    protected void updateTree(ObserveNode oldActivitiesNode,
                              String tripLonglineId,
                              List<String> activityIds) {

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();
        ObserveTreeHelper treeHelper = getMainUI().getTreeHelper();

        ObserveNode oldTripLonglineNode = oldActivitiesNode.getParent();
        ObserveNode programNode = oldTripLonglineNode.getParent();
        ObserveNode newTripLonglineNode = treeHelper.getChild(programNode, tripLonglineId);
        String activitiesNodeId = ObserveI18nDecoratorHelper.getTypePluralI18nKey(ActivityLonglineDto.class);
        ObserveNode newActivitiesNode = treeHelper.getChild(newTripLonglineNode, activitiesNodeId);

        // Let's check if we're moving an open activity
        Optional<String> openActivity = activityIds
                .stream()
                .filter(openDataManager::isOpenActivityLongline)
                .findFirst();

        // If so, we close it to avoid ending up with an open activity into a closed trip.
        if (openActivity.isPresent()) {
            openDataManager.closeActivityLongline(openActivity.get());
        }

        // Let's reload the sub tree of each activities node.
        // As the change have already be done in database, we just call the child loaders to regenerate the activities nodes sub trees
        treeHelper.reloadNodeSubTree(oldActivitiesNode, true);
        treeHelper.reloadNodeSubTree(newActivitiesNode, true);

        // Let's put the focus on the activities node which received the activities
        treeHelper.selectNode(newActivitiesNode);

    }

}
