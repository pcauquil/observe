/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#speciesLabel {
  text:"observe.common.speciesFaune";
}

#acquisitionModeGroup {
  selectedValue:{ModeSaisieEchantillonEnum.valueOf(tableEditBean.getAcquisitionMode())};
}

#acquisitionModePanel {
  border:{new TitledBorder(t("observe.common.acquisitionMode"))};
  layout:{new GridLayout(1,0)};
}

#acquisitionModeEffectif {
  buttonGroup:"acquisitionModeGroup";
  value:{ModeSaisieEchantillonEnum.byEffectif};
  text:{ModeSaisieEchantillonEnum.byEffectif.getI18nKey()};
  selected:{tableEditBean.getAcquisitionMode() == 0};
  enabled:{!tableModel.isEditable() || tableModel.isCreate()};
}

#acquisitionModeIndividu {
  buttonGroup:"acquisitionModeGroup";
  value:{ModeSaisieEchantillonEnum.byIndividu};
  text:{ModeSaisieEchantillonEnum.byIndividu.getI18nKey()};
  selected:{tableEditBean.getAcquisitionMode() == 1};
  enabled:{!tableModel.isEditable() || tableModel.isCreate()};
}

#lengthLabel {
  text:"observe.common.taille";
}

#length {
  _validatorLabel:{t("observe.common.taille")};
}

#countLabel {
  text:"observe.common.count";
  labelFor:{count};
}

#count {
  property:{NonTargetLengthDto.PROPERTY_COUNT};
  model:{tableEditBean.getCount()};
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
}

#sexLabel {
  text:"observe.common.sex";
  labelFor:{sex};
}

#sex {
  property:{NonTargetLengthDto.PROPERTY_SEX};
  selectedItem:{tableEditBean.getSex()};
}

#picturesReferencesLabel {
  text:"observe.common.picturesReferences";
  labelFor:{picturesReferences};
}

#picturesReferences {
  text:{getStringValue(tableEditBean.getPicturesReferences())};
  _tablePropertyName:{NonTargetLengthDto.PROPERTY_PICTURES_REFERENCES};
}

#resetPicturesReferences{
  toolTipText:"observe.content.action.reset.picturesReferences.tip";
  _resetTablePropertyName:{NonTargetLengthDto.PROPERTY_PICTURES_REFERENCES};
}

#speciesTypeTailleLabel{
  actionIcon:"information";
  text:"observe.common.lengthMeasureType";
  labelFor:{speciesTypeTaille};
}

#speciesTypeTaille {
  font-weight:"bold";
  text:{getSpeciesFauneTypeTaille(tableEditBean.getSpecies())};
}

#comment {
  columnHeaderView:{new JLabel(t("observe.common.comment.nonTargetSample"))};
}

#hideFormInformation {
  text:"observe.message.cant.add.nonTargetSample";
}
