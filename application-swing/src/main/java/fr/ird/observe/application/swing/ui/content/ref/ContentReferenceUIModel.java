/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.ref;

import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.services.dto.referential.I18nReferentialDto;
import fr.ird.observe.services.dto.referential.LengthWeightParameterDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Le modèle pour un écran d'édition du référentiel.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since .14
 */
public abstract class ContentReferenceUIModel<E extends ReferentialDto> extends ContentUIModel<E> {

    private static final String PROPERTY_SELECTED_BEAN = "selectedBean";

    protected static final String SUFFIX_TEXT = ".text";

    protected static final String SUFFIX_MODEL = ".model";

    private static final String SUFFIX_SELECTED_INDEX = ".selectedIndex";

    protected static final String SUFFIX_SELECTED_ITEM = ".selectedItem";

    protected static final String SUFFIX_SELECTED = ".selected";

    private static final String SUFFIX_DATE = ".date";

    /** liste des bindings present sur tous les ecrans du referentiel */
    private final static String[] DEFAULT_DATABINDING = {
            ReferentialDto.PROPERTY_CODE + SUFFIX_TEXT,
            ReferentialDto.PROPERTY_URI + SUFFIX_TEXT,
            ReferentialDto.PROPERTY_STATUS + SUFFIX_SELECTED_INDEX,
            ReferentialDto.PROPERTY_NEED_COMMENT + SUFFIX_SELECTED
    };

    /** liste des proprietes presents sur les entites parametrageLengthWeightAble */
    private final static String[] DEFAULT_LENGTH_WEIGHT_PARAMETER_ABLE_DATABINDING = {
            LengthWeightParameterDto.PROPERTY_START_DATE + SUFFIX_DATE,
            LengthWeightParameterDto.PROPERTY_END_DATE + SUFFIX_DATE,
            LengthWeightParameterDto.PROPERTY_SPECIES + SUFFIX_SELECTED_ITEM,
            LengthWeightParameterDto.PROPERTY_OCEAN + SUFFIX_SELECTED_ITEM,
            LengthWeightParameterDto.PROPERTY_SEX + SUFFIX_SELECTED_INDEX,
            LengthWeightParameterDto.PROPERTY_LENGTH_WEIGHT_FORMULA + SUFFIX_TEXT,
            LengthWeightParameterDto.PROPERTY_WEIGHT_LENGTH_FORMULA + SUFFIX_TEXT,
            LengthWeightParameterDto.PROPERTY_COEFFICIENTS + SUFFIX_TEXT,
            LengthWeightParameterDto.PROPERTY_MEAN_LENGTH + SUFFIX_MODEL,
            LengthWeightParameterDto.PROPERTY_MEAN_WEIGHT + SUFFIX_MODEL
    };

    /** liste des bindings presents sur les entites i18n */
    private final static String[] DEFAULT_I18N_DATABINDING = {
            I18nReferentialDto.PROPERTY_LABEL1 + SUFFIX_TEXT,
            I18nReferentialDto.PROPERTY_LABEL2 + SUFFIX_TEXT,
            I18nReferentialDto.PROPERTY_LABEL3 + SUFFIX_TEXT,
            I18nReferentialDto.PROPERTY_LABEL4 + SUFFIX_TEXT,
            I18nReferentialDto.PROPERTY_LABEL5 + SUFFIX_TEXT,
            I18nReferentialDto.PROPERTY_LABEL6 + SUFFIX_TEXT,
            I18nReferentialDto.PROPERTY_LABEL7 + SUFFIX_TEXT,
            I18nReferentialDto.PROPERTY_LABEL8 + SUFFIX_TEXT
    };

    /** liste des proprietes present sur tous les ecrans du referentiel */
    final static String[] DEFAULT_PROPERTIES = {
            ReferentialDto.PROPERTY_CODE,
            ReferentialDto.PROPERTY_URI,
            ReferentialDto.PROPERTY_STATUS,
            ReferentialDto.PROPERTY_NEED_COMMENT
    };

    /** liste des proprietes presents sur les entites taillePoidsAble */
    private final static String[] DEFAULT_PARAMETRAGE_TAILLE_POIDS_ABLE_PROPERTIES = {
            LengthWeightParameterDto.PROPERTY_START_DATE,
            LengthWeightParameterDto.PROPERTY_END_DATE,
            LengthWeightParameterDto.PROPERTY_OCEAN,
            LengthWeightParameterDto.PROPERTY_SPECIES,
            LengthWeightParameterDto.PROPERTY_SEX,
            LengthWeightParameterDto.PROPERTY_LENGTH_WEIGHT_FORMULA,
            LengthWeightParameterDto.PROPERTY_WEIGHT_LENGTH_FORMULA,
            LengthWeightParameterDto.PROPERTY_COEFFICIENTS,
            LengthWeightParameterDto.PROPERTY_MEAN_LENGTH,
            LengthWeightParameterDto.PROPERTY_MEAN_WEIGHT
    };

    /** liste des proprietes presents sur les entites i18n */
    private final static String[] DEFAULT_I18N_PROPERTIES = {
            I18nReferentialDto.PROPERTY_LABEL1,
            I18nReferentialDto.PROPERTY_LABEL2,
            I18nReferentialDto.PROPERTY_LABEL3,
            I18nReferentialDto.PROPERTY_LABEL4,
            I18nReferentialDto.PROPERTY_LABEL5,
            I18nReferentialDto.PROPERTY_LABEL6,
            I18nReferentialDto.PROPERTY_LABEL7,
            I18nReferentialDto.PROPERTY_LABEL8
    };

    private static final long serialVersionUID = 1L;

    /** la liste des propriétés du bean a charger */
    private final String[] properties;

    /** la liste des propriétés faisant partie de la clef metier */
    private final String[] naturalIds;

    /** la liste des bindings à activer lors de l'ouverture de l'ui */
    private final String[] dataBinding;

    private E selectedBean;

    protected ContentReferenceUIModel(Class<E> beanType) {

        this(beanType, null, null, null);

    }

    protected ContentReferenceUIModel(Class<E> beanType,
                                      String[] extraProperties,
                                      String[] extraBindings) {

        this(beanType, extraProperties, null, extraBindings);

    }

    protected ContentReferenceUIModel(Class<E> beanType,
                                      String[] extraProperties,
                                      String[] naturalIds,
                                      String[] extraBindings) {
        super(beanType);

        boolean useDefault = naturalIds == null;

        // la clef naturelle par defaut est la premiere des properties
        this.naturalIds = useDefault ? new String[]{DEFAULT_PROPERTIES[0]} : naturalIds;

        List<String> properties = buildProperties(useDefault, extraProperties);
        this.properties = properties.toArray(new String[properties.size()]);

        List<String> bindings = buildDataBindings(useDefault, extraBindings);
        this.dataBinding = bindings.toArray(new String[bindings.size()]);


    }

    public String[] getNaturalIds() {
        return naturalIds;
    }

    public String[] getDataBinding() {
        return dataBinding;
    }

    public E getSelectedBean() {
        return selectedBean;
    }

    public void setSelectedBean(E selectedBean) {
        Object oldvalue = this.selectedBean;
        this.selectedBean = selectedBean;
        firePropertyChange(PROPERTY_SELECTED_BEAN, oldvalue, selectedBean);
    }

    /**
     * construction de la liste des propriétés a binder pour une entite.
     *
     * @param usedefault un drapeau pour savoir si on doit conserver la premiere valeur par defaut
     * @param properties les propriétés supplémentaires a ajouter en plus de
     *                   celles communes
     * @return la liste des toutes les proprietes utilises sur l'entite edite dans l'ui
     */
    private final List<String> buildProperties(boolean usedefault, String[] properties) {

        List<String> result = new ArrayList<>();
        result.addAll(Arrays.asList(DEFAULT_PROPERTIES));
        if (properties != null) {
            result.addAll(Arrays.asList(properties));
        }
        Class<E> beanType = getBeanType();

        if (I18nReferentialDto.class.isAssignableFrom(beanType)) {
            result.addAll(Arrays.asList(DEFAULT_I18N_PROPERTIES));
        }
        if (LengthWeightParameterDto.class.isAssignableFrom(beanType)) {
            result.addAll(Arrays.asList(DEFAULT_PARAMETRAGE_TAILLE_POIDS_ABLE_PROPERTIES));
        }
        if (!usedefault) {

            // on supprime le premiere valeur
            result.remove(DEFAULT_PROPERTIES[0]);
        }
        return result;

    }

    /**
     * construction de la liste des databindings utiliser pour une entite.
     *
     * @param usedefault  un drapeau pour savoir si on doit conserver la premiere valeur par defaut
     * @param dataBinding les bindings supplémentaires a utiliser en plus de
     *                    ceux communs
     * @return la liste de tous les databindings utilises sur l'entite edite dans l'ui
     */
    private final List<String> buildDataBindings(boolean usedefault, String[] dataBinding) {

        List<String> result = new ArrayList<>();
        result.addAll(Arrays.asList(DEFAULT_DATABINDING));
        if (dataBinding != null) {
            result.addAll(Arrays.asList(dataBinding));
        }
        Class<E> beanType = getBeanType();

        if (I18nReferentialDto.class.isAssignableFrom(beanType)) {
            result.addAll(Arrays.asList(DEFAULT_I18N_DATABINDING));
        }
        if (LengthWeightParameterDto.class.isAssignableFrom(beanType)) {
            result.addAll(Arrays.asList(DEFAULT_LENGTH_WEIGHT_PARAMETER_ABLE_DATABINDING));
        }
        if (!usedefault) {

            // on supprime le premier binding
            result.remove(DEFAULT_DATABINDING[0]);
        }
        return result;

    }

}
