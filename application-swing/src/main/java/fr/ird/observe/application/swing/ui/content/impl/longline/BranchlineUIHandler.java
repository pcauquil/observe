package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlineDtos;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.i18n.I18n;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JOptionPane;
import javax.swing.event.TableModelListener;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 12/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class BranchlineUIHandler extends ContentUIHandler<BranchlineDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(BranchlineUIHandler.class);

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    private Decorator<BranchlineDto> branchlineDecorator;

    public BranchlineUIHandler(BranchlineUI ui) {
        super(ui, DataContextType.SetLongline, null);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
    }

    public void edit(BranchlineDto branchline) {

        if (log.isInfoEnabled()) {
            log.info("Will edit branchline: " + branchlineDecorator.toString(branchline));
        }

        getModel().setBranchline(branchline);
        BranchlineDtos.copyBranchlineDto(branchline, getBean());

        if (branchline == null) {

            // stop edit
            stopEditUI();

        } else {

            if (getModel().isEnabled() && !getModel().isEditing()) {

                startEditUI();

            }
        }

        // To be sure always remove listener (could prevent some leaks)
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        getUi().getErrorTableModel().addTableModelListener(computeTabValidStateListener);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

        getModel().setModified(false);

    }

    @Override
    public BranchlineUIModel getModel() {
        return (BranchlineUIModel) super.getModel();
    }

    @Override
    public BranchlineUI getUi() {
        return (BranchlineUI) super.getUi();
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        ContentMode contentMode;

        if (getOpenDataManager().isOpenActivityLongline(dataContext.getSelectedActivityLonglineId())) {

            // l'activité est ouverte, mode édition
            contentMode = ContentMode.UPDATE;

        } else {

            contentMode = ContentMode.READ;

        }

        return contentMode;

    }

    @Override
    public void initUI() {

        branchlineDecorator = getDecoratorService().getDecoratorByType(BranchlineDto.class);
        super.initUI();

    }

    @Override
    public void openUI() {

        if (log.isInfoEnabled()) {
            log.info("OpenUI: " + getModel());
        }

        boolean canWrite = computeCanWrite(getDataSource());

        getModel().setCanWrite(canWrite);

        updateActions();

        ContentMode mode = computeContentMode();

        // utilisation du mode requis
        setContentMode(mode);

        // when opening screen do not edit incoming bean
        edit(null);

        if (mode != ContentMode.READ) {
            ui.startEdit(null);
        }

    }

    @Override
    public void startEditUI(String... binding) {

        ContentUIModel<BranchlineDto> model = getModel();

        super.startEditUI(BranchlineUI.BINDING_COMMENT2_TEXT,

                          BranchlineUI.BINDING_TOP_TYPE_SELECTED_ITEM,
                          BranchlineUI.BINDING_TRACELINE_TYPE_SELECTED_ITEM,

                          BranchlineUI.BINDING_DEPTH_RECORDER_SELECTED,
                          BranchlineUI.BINDING_HOOK_LOST_SELECTED,
                          BranchlineUI.BINDING_TRACE_CUT_OFF_SELECTED,

                          BranchlineUI.BINDING_WEIGHTED_SWIVEL_SELECTED,
                          BranchlineUI.BINDING_SWIVEL_WEIGHT_MODEL,
                          BranchlineUI.BINDING_WEIGHTED_SNAP_SELECTED,
                          BranchlineUI.BINDING_SNAP_WEIGHT_MODEL,

                          BranchlineUI.BINDING_BAIT_TYPE_SELECTED_ITEM,
                          BranchlineUI.BINDING_BAIT_SETTING_STATUS_SELECTED_ITEM,
                          BranchlineUI.BINDING_BAIT_HAULING_STATUS_SELECTED_ITEM,

                          BranchlineUI.BINDING_HOOK_TYPE_SELECTED_ITEM,
                          BranchlineUI.BINDING_HOOK_SIZE_SELECTED_ITEM,
                          BranchlineUI.BINDING_HOOK_OFFSET_MODEL,

                          BranchlineUI.BINDING_TIMER_SELECTED,
                          BranchlineUI.BINDING_TIME_SINCE_CONTACT_MODEL,
                          BranchlineUI.BINDING_TIMER_TIME_ON_BOARD_DATE
        );

        model.setModified(false);

    }

    @Override
    protected boolean doSave(BranchlineDto bean) throws Exception {

        // bind back to model branchline
        BranchlineDto beanToSave = getModel().getBranchline();
        BranchlineDtos.copyBranchlineDto(bean, beanToSave);

        // for external models
        getModel().fireSaved();

        return true;

    }

    @Override
    public void resetEditUI() {

        BranchlineDto branchline = getModel().getBranchline();

        // on arrete l'edition
        stopEditUI();

        // on re-ouvre l'écran d'édition
        try {
            openUI();
        } catch (Exception ex) {
            UIHelper.handlingError(ex);
            stopEditUI();
        }

        edit(branchline);

    }

    public boolean tryToQuit() {

        boolean canContinue;

        if (getModel().isEditing() && getModel().isModified()) {

            canContinue = false;

            if (getModel().isValid()) {

                // le formulaire est valide, on demande a l'utilisateur s'il
                // veut la sauvegarder

                int reponse = UIHelper.askUser(
                        I18n.t("observe.title.need.confirm"),
                        I18n.t("observe.content.branchline.message.modified"),
                        JOptionPane.WARNING_MESSAGE,
                        new Object[]{
                                I18n.t("observe.choice.save"),
                                I18n.t("observe.choice.doNotSave"),
                                I18n.t("observe.choice.cancel")},
                        0);
                if (log.isDebugEnabled()) {
                    log.debug("response : " + reponse);
                }

                switch (reponse) {
                    case JOptionPane.CLOSED_OPTION:
                    case 2:

                        break;
                    case 0:
                        // will save ui
                        // sauvegarde des modifications
                        saveUI(false);
                        canContinue = true;
                        break;
                    case 1:

                        // reset edit
                        resetEditUI();
                        canContinue = true;

                        break;
                }

            } else {

                // le formulaire n'est pas valide, on ne peut que proposer la perte
                // des donnees car elles sont ne pas enregistrables

                int reponse = UIHelper.askUser(
                        I18n.t("observe.title.need.confirm"),
                        I18n.t("observe.content.branchline.message.modified.but.invalid"),
                        JOptionPane.ERROR_MESSAGE,
                        new Object[]{
                                I18n.t("observe.choice.continue"),
                                I18n.t("observe.choice.cancel")},
                        0);
                if (log.isDebugEnabled()) {
                    log.debug("response : " + reponse);
                }
                switch (reponse) {
                    case 0:

                        // reset edit
                        resetEditUI();
                        canContinue = true;

                        break;
                }

            }

        } else {

            // pas en mode edition ou formulaire non modifié
            // on peut toujours quitter
            canContinue = true;

        }

        return canContinue;

    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean generalTabValid = !errorProperties.removeAll(BranchlineUIModel.GENERAL_TAB_PROPERTIES);
        boolean hookAndBaitTabValid = !errorProperties.removeAll(BranchlineUIModel.HOOK_AND_BAIT_TAB_PROPERTIES);

        BranchlineUIModel model =  getModel();
        model.setGeneralTabValid(generalTabValid);
        model.setHookAndBaitTabValid(hookAndBaitTabValid);

    }
}
