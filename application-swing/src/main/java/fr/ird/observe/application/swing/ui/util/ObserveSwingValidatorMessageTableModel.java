package fr.ird.observe.application.swing.ui.util;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Predicate;

/**
 * Created on 3/18/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class ObserveSwingValidatorMessageTableModel extends SwingValidatorMessageTableModel {

    private static final long serialVersionUID = 1L;

    @Override
    public void removeMessages(JComponent editor,
                               NuitonValidatorScope scope) {
        if (scope == null) {
            // on supprime tout sans aucun calcul
            int nb = getRowCount();
            if (nb > 0) {
                data.clear();
                fireTableRowsDeleted(0, nb - 1);
            }
        } else {
            super.removeMessages(editor, scope);
        }
    }

    public void removeMessages(Predicate<SwingValidatorMessage> predicate) {

        for (int i = getRowCount() - 1; i > -1; i--) {
            SwingValidatorMessage error = data.get(i);

            if (predicate.test(error)) {
                // remove the message
                data.remove(i);
                fireTableRowsDeleted(i, i);
            }
        }

    }

    public <M extends SwingValidatorMessage> void addMessages(Collection<M> messages) {

        data.addAll(messages);

        // resort datas
        Collections.sort(data);

        // notify
        fireTableDataChanged();

    }
}
