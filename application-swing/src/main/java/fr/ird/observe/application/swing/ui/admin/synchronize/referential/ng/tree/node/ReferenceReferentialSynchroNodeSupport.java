package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.application.swing.ui.UIHelper;

/**
 * Created on 09/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public abstract class ReferenceReferentialSynchroNodeSupport extends ReferentialSynchroNodeSupport {

    private static final long serialVersionUID = 1L;

    private final boolean canAdd;
    private final boolean canUpdate;
    private final boolean canDelete;
    private final boolean canRevert;

    protected ReferenceReferentialSynchroNodeSupport(ReferentialReference<? extends ReferentialDto> referentialReference,
                                                     String iconName,
                                                     boolean canAdd,
                                                     boolean canUpdate,
                                                     boolean canDelete,
                                                     boolean canRevert) {
        super(UIHelper.createActionIcon(iconName), referentialReference);
        this.canAdd = canAdd;
        this.canUpdate = canUpdate;
        this.canDelete = canDelete;
        this.canRevert = canRevert;
    }

    public boolean isCanAdd() {
        return canAdd;
    }

    public boolean isCanUpdate() {
        return canUpdate;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public boolean isCanRevert() {
        return canRevert;
    }

    @Override
    public TypeReferentialSynchroNode getParent() {
        return (TypeReferentialSynchroNode) super.getParent();
    }

    @Override
    public ReferentialReference getUserObject() {
        return (ReferentialReference) super.getUserObject();
    }

}
