/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.dto.AbstractObserveDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.ContentUIManager;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.content.ObserveContentUI;
import fr.ird.observe.application.swing.ui.tree.ObserveNavigationTreeShowPopupAction;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.util.ObserveSwingValidatorMessageTableModel;
import fr.ird.observe.application.swing.ui.util.ObserveValidatorMessageTableRenderer;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.context.DefaultApplicationContext;
import jaxx.runtime.context.JAXXInitialContext;
import jaxx.runtime.swing.ErrorDialogUI;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

import javax.swing.Icon;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import java.awt.Cursor;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * Le handler de l'ui principale.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see ObserveMainUI
 * @since 1.0
 */
@DefaultApplicationContext.AutoLoad
public class ObserveMainUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveMainUIHandler.class);

    public void changeNavigationNode(ObserveMainUI ui, TreeSelectionEvent event) {


        ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
        if (source == null || !source.isOpen()) {

            // no open data source
            if (log.isDebugEnabled()) {
                log.debug("No open Data source.");
            }
            return;
        }
        if (ui.getNavigation().isSelectionEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("No selection, do nothing...");
            }
            return;
        }

        TreePath path = event.getPath();
        ObserveNode node = (ObserveNode) path.getLastPathComponent();
        ContentUIManager manager = ui.getContentUIManager();

        // obtain the ui type to show
        Class<? extends ObserveContentUI<?>> uiClass = manager.convertNodeToContentUI(node);
        if (log.isDebugEnabled()) {
            log.debug("new selected path = " + node + ", ui = " + uiClass);
        }

        if (uiClass == null) {

            // pas d'ui trouvé, on ne fait donc rien
            return;
        }

        ui.setBusy(true);

        try {

            doOpencontent(ui, path, uiClass);

        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {
            ui.setBusy(false);
            String params;
            if (node.isStringNode()) {
                params = t(node.getId());
            } else {
                params = t(ObserveI18nDecoratorHelper.getTypeI18nKey(node.getInternalClass()));
            }
            ui.getStatus().setStatus(t("observe.action.open.screen", params));
        }
    }

    public void doOpencontent(ObserveMainUI ui,
                              TreePath path,
                              Class<? extends ObserveContentUI<?>> uiClass) {

        ContentUIManager manager = ui.getContentUIManager();

        // compute the selected ids to put in data context
        Object[] nodes = path.getPath();
        List<String> ids = new ArrayList<>();
        for (Object o : nodes) {
            ObserveNode n = (ObserveNode) o;

            if (n.isStringNode() || n.isReferentielNode() || n.isRoot() || !n.isDataNode()) {
                continue;
            }
            ids.add(n.getId());
        }

        if (log.isDebugEnabled()) {
            log.debug("new selected ids from tree = " + ids);
        }
        String[] selectedIds = ids.toArray(new String[ids.size()]);

        // update selected ids in data context
        DataContext context = ui.getDataContext();
        context.populateSelectedIds(selectedIds);

        // on recherche l'ui (voir si elle existe déjà)
        ObserveContentUI<?> content = manager.getContent(uiClass);

        if (content == null) {

            content = manager.createContent(uiClass);
        }

        // on ouvre l'écran
        manager.openContent(content);
    }

    /**
     * Methode pour initialiser l'ui principale sans l'afficher.
     *
     * @param context le context applicatif
     * @param config  la configuration a utiliser
     * @return l'ui instancie et initialisee mais non visible encore
     */
    public ObserveMainUI initUI(ObserveSwingApplicationContext context, ObserveSwingApplicationConfig config) {

        SwingValidatorMessageTableModel errorModel =
                new ObserveSwingValidatorMessageTableModel();

        DecoratorService decoratorService = context.getDecoratorService();

        boolean reloadDecorators = false;
        Locale currentLocale = I18n.getDefaultLocale();
//        if (!config.getLocale().equals(
//                I18n.getStore().getLanguage().getLocale())) {
        Locale configurationLocale = config.getLocale();
        if (!configurationLocale.equals(currentLocale)) {
            if (log.isInfoEnabled()) {
                log.info("re-init I18n with locale " + configurationLocale);
            }
            I18n.setDefaultLocale(configurationLocale);
//            I18n.init(configurationLocale);
            reloadDecorators = true;
        }
        if (!config.getDbLocale().equals(
                decoratorService.getReferentialLocale().getLocale())) {
            if (log.isInfoEnabled()) {
                log.info("re-init db with locale " + config.getDbLocale());
            }
            decoratorService.setReferentialLocale(ReferentialLocale.valueOf(config.getDbLocale()));

            reloadDecorators = true;
        }

        if (reloadDecorators) {
            if (log.isInfoEnabled()) {
                log.info("reload decorators");
            }
            decoratorService.reload();
        }

        ObserveTreeHelper treeHelper = new ObserveTreeHelper();
        ObserveSwingDataSource source = context.getDataSourcesManager().getMainDataSource();

        boolean open = context.getDataContext().getEnabled();
        if (open) {
            treeHelper.createModel(source);
        } else {
            treeHelper.createEmptyModel();
        }

        JAXXInitialContext tx = new JAXXInitialContext();

        tx.add(context).add(treeHelper).add(errorModel);

        // show main ui
        ObserveMainUI ui = new ObserveMainUI(tx);
        ui.setUndecorated(config.isFullScreen());

        String title = t("observe.title.welcome.admin");
        title += " v " + config.getVersion();
        ui.setTitle(title);

        context.setMainUI(ui);

        ErrorDialogUI.init(ui);

        // set fullscreen propery on main ui
        ui.getGraphicsConfiguration().getDevice().setFullScreenWindow(config.isFullScreen() ? ui : null);

        // on enregistre les actions communes à toutes les écrans d'édition
        // les bouttons utilisant ces actions seront automatiquement
        // chargés (à partir des actions) lors de l'ouverture des ui
        // et mis à jour

        context.registerShareActions();

        new ObserveNavigationTreeShowPopupAction(treeHelper, ui.getNavigationScrollPane(), ui.getNavigationPopup());

        return ui;
    }

    public static final String H2_SERVER_URL_PATTERN = "jdbc:h2:%s/%s/obstuna";

    public static void restartEdit() {
        ContentUI<?> selectedUI = ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();
        if (selectedUI == null) {

            // pas d'écran selectionne
            return;
        }
        ContentUIModel<? extends AbstractObserveDto> model = selectedUI.getModel();
        if (!model.isEditable()) {

            // modele non editable
            return;
        }

        ContentMode contentMode = model.getMode();
        if (ContentMode.UPDATE != contentMode) {

            // ecran non en mode mis a jour
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("Will restart edit for " + selectedUI.getName());
        }
        selectedUI.restartEdit();

    }

    /**
     * Permet de recharger l'ui principale et de changer de le mode
     * d'affichage.
     *
     * @param rootContext le contexte applicatif
     * @param config      la configuration a utiliser
     */
    protected void reloadUI(ObserveSwingApplicationContext rootContext, ObserveSwingApplicationConfig config) {

        // must remove all properties listener on config        
        config.removeJaxxPropertyChangeListener();

        // scan main ui
        ObserveMainUI ui = getUI(rootContext);

        ObserveSwingDataSource mainStorage = rootContext.getDataSourcesManager().getMainDataSource();

        ObserveUIMode oldMode = null;
        String[] ids = null;
        if (ui != null) {

            oldMode = ui.getMode();

            ids = ui.getTreeHelper().getSelectedIds();

            ErrorDialogUI.init(null);

            rootContext.removeMainUI();

            ui.dispose();

            ui.setVisible(false);

            System.runFinalization();
        }

        ui = initUI(rootContext, config);

        if (oldMode == null) {
            if (mainStorage == null) {
                oldMode = ObserveUIMode.NO_DB;

            } else {
                oldMode = ObserveUIMode.DB;
            }
        }

        ui.setMode(oldMode);

        if (oldMode == ObserveUIMode.DB) {

            // on conserve les noeuds a reselectionner
            rootContext.setNodesToReselect(ids);

            // selection du noeud d'ouverture (le noeud precedemment selectionne,
            // ou le noeud le plus ouvert
            // ou le premier program si aucune donnée ouverte)
            ui.getTreeHelper().selectInitialNode();
        }

        // show ui
        UIHelper.setMainUIVisible(ui);
    }

    protected Icon updateStorageSatutIcon(ObserveMainUI ui, boolean isOpened) {
        Icon icon;

        if (ui.getConfig().isMainStorageOpened()) {

            ObserveSwingDataSource service = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();

            if (service.isLocal()) {
                icon = (Icon) ui.storageStatus.getClientProperty("localIcon");
            } else if (service.isRemote()) {
                icon = (Icon) ui.storageStatus.getClientProperty("remoteIcon");
            } else {
                icon = (Icon) ui.storageStatus.getClientProperty("serverIcon");
            }
        } else {
            icon = (Icon) ui.storageStatus.getClientProperty("noneIcon");
        }
        return icon;
    }

    protected String updateStorageSatutText(ObserveMainUI ui, boolean isOpened) {
        String text;
        if (ui.getConfig().isMainStorageOpened()) {
            ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
            text = source.getLabel();
        } else {
            text = t("observe.message.db.none.loaded");
        }
        return text;
    }

    protected String updateStorageStatutToolTipText(ObserveMainUI ui, boolean isOpened) {
        String text;
        if (ui.getConfig().isMainStorageOpened()) {
            ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
            text = t("observe.message.loaded.tip", source.getLabel());
        } else {
            text = t("observe.message.db.none.loaded.tip");
        }
        return text;
    }

    public boolean acceptMode(ObserveUIMode mode, ObserveUIMode... modes) {
        return acceptMode(mode, true, modes);
    }

    public boolean acceptMode(ObserveUIMode mode, boolean condition, ObserveUIMode... modes) {
        if (condition) {
            for (ObserveUIMode m : modes) {
                if (m.equals(mode)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean acceptMode(ObserveUIMode mode, boolean notBusy, boolean condition, ObserveUIMode... modes) {
        if (notBusy && condition) {
            for (ObserveUIMode m : modes) {
                if (m.equals(mode)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean rejectMode(ObserveUIMode mode, ObserveUIMode... modes) {
        for (ObserveUIMode m : modes) {
            if (m.equals(mode)) {
                return false;
            }
        }
        return true;
    }

    public boolean acceptLocale(Locale l, String expected) {
        return l != null && l.toString().equals(expected);
    }

    public ObserveMainUI getUI(JAXXContext context) {
        if (context instanceof ObserveMainUI) {
            return (ObserveMainUI) context;
        }
        return ObserveSwingApplicationContext.get().getMainUI();
    }

    protected void $afterCompleteSetup(final ObserveMainUI ui) {

        ui.getStatus().init();

        // ajout d'un ecouteur sur la navigation pour toujours mettre la scrollbar
        // tout à droite a chaque selection
        TreeSelectionListener listener;
        listener = e -> {
            changeNavigationNode(ui, e);
            SwingUtilities.invokeLater(() -> {
                ui.getNavigationScrollPane().getHorizontalScrollBar().setValue(0);
                ui.getSplitpane2().resetToPreferredSizes();
            });
        };
        TreeWillExpandListener veteobableTreeWillExpand = new TreeWillExpandListener() {
            @Override
            public void treeWillExpand(TreeExpansionEvent event) {
            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {

                // le seul posant problème est la fermeture d'un noeud parent
                // du noeud courant : cela va changer la selection
                ObserveNode selectedNode = ui.getTreeHelper().getSelectedNode();
                if (selectedNode == null) {
                    // pas de noeud selectionne
                    return;
                }

                ObserveNode o = (ObserveNode) event.getPath().getLastPathComponent();
                if (selectedNode.equals(o) || !selectedNode.isNodeAncestor(o)) {
                    return;
                }
                boolean canChange = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
                if (!canChange) {
                    throw new ExpandVetoException(event, "Can not collapse node " + event.getPath());
                }
            }
        };

        ui.getTreeHelper().setUI(ui.getNavigation(), true, true, listener, veteobableTreeWillExpand);

        SwingValidatorUtil.installUI(ui.getErrorTable(), new ObserveValidatorMessageTableRenderer());

        // installation layer de blocage en mode busy
        UIHelper.setLayerUI(ui.getBody(), ui.getBusyBlockLayerUI());

        // ecoute des changements de l'état busy
        ui.addPropertyChangeListener(ObserveMainUI.PROPERTY_BUSY, evt -> {
            Boolean newvalue = (Boolean) evt.getNewValue();
            updateBusyState(ui, newvalue != null && newvalue);
        });
    }

    protected void updateBusyState(ObserveMainUI ui, boolean busy) {
        if (busy) {
            // ui bloquee
            if (log.isDebugEnabled()) {
                log.debug("block ui in busy mode");
            }
            ui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            // ui debloquee
            if (log.isDebugEnabled()) {
                log.debug("unblock ui in none busy mode");
            }
            ui.setCursor(Cursor.getDefaultCursor());
        }
    }

}
