/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDtos;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.service.ReferentialService;
import fr.ird.observe.services.service.seine.ObjectObservedSpeciesService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ObjectObservedSpeciesUIHandler extends ContentTableUIHandler<FloatingObjectObservedSpeciesDto, ObjectObservedSpeciesDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObjectObservedSpeciesUIHandler.class);

    public ObjectObservedSpeciesUIHandler(ObjectObservedSpeciesUI ui) {
        super(ui, DataContextType.SetSeine);
    }

    @Override
    public ObjectObservedSpeciesUI getUi() {
        return (ObjectObservedSpeciesUI) super.getUi();
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedFloatingObjectId();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, ObjectObservedSpeciesDto bean, boolean create) {
        if (log.isDebugEnabled()) {
            log.debug("Row has changed to " + editingRow);
        }
        if (getTableModel().isEditable()) {
            getUi().getSpecies().requestFocus();
        }
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {
        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(table,
                                            n("observe.content.objectObservedSpecies.table.speciesFaune"),
                                            n("observe.content.objectObservedSpecies.table.speciesFaune.tip"),
                                            n("observe.content.objectObservedSpecies.table.speciesStatus"),
                                            n("observe.content.objectObservedSpecies.table.speciesStatus.tip"),
                                            n("observe.content.objectObservedSpecies.table.count"),
                                            n("observe.content.objectObservedSpecies.table.count.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesStatusDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
    }

    @Override
    protected void doPersist(FloatingObjectObservedSpeciesDto bean) {

        SaveResultDto saveResult = getObjectObservedSpeciesService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<FloatingObjectObservedSpeciesDto> form = getObjectObservedSpeciesService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        FloatingObjectObservedSpeciesDtos.copyFloatingObjectObservedSpeciesDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case ObjectSchoolEstimateDto.PROPERTY_SPECIES: {
                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListSeineObjectObservedSpeciesId();

                ReferentialService referentialService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newReferentialService();
                Form<SpeciesListDto> speciesListDtoForm = referentialService.loadForm(SpeciesListDto.class, speciesListId);
                SpeciesListDto speciesListDto = speciesListDtoForm.getObject();

                Set<String> speciesIds = speciesListDto.getSpeciesIds();

                result = ReferentialReferences.filterContains(result, speciesIds);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected ObjectObservedSpeciesService getObjectObservedSpeciesService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newObjectObservedSpeciesService();
    }
}
