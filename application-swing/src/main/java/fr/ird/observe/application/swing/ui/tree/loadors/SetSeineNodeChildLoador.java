/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree.loadors;


import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.services.dto.seine.DiscardedTargetCatchDto;
import fr.ird.observe.services.dto.seine.DiscardedTargetSampleDto;
import fr.ird.observe.services.dto.seine.KeptTargetCatchDto;
import fr.ird.observe.services.dto.seine.KeptTargetSampleDto;
import fr.ird.observe.services.dto.seine.NonTargetCatchDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import jaxx.runtime.swing.nav.NavDataProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Le chargeur des noeuds de marees.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class SetSeineNodeChildLoador extends AbstractNodeChildLoador<Class, String> {

    private static final long serialVersionUID = 1L;

    private static final Set<Class> PLURALIZE_PROPERTIES = Sets.newHashSet(TargetCatchDto.class,
                                                                           TargetSampleDto.class,
                                                                           NonTargetSampleDto.class,
                                                                           NonTargetCatchDto.class);

    public SetSeineNodeChildLoador() {
        super(String.class);
    }

    @Override
    public List<Class> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) {
        return Arrays.asList(SchoolEstimateDto.class,
                             KeptTargetCatchDto.class,
                             DiscardedTargetCatchDto.class,
                             DiscardedTargetSampleDto.class,
                             KeptTargetSampleDto.class,
                             NonTargetCatchDto.class,
                             NonTargetSampleDto.class);
    }

    @Override
    public ObserveNode createNode(Class data, NavDataProvider dataProvider) {
        return createNode0(PLURALIZE_PROPERTIES, data);
    }
}
