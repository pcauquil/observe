package fr.ird.observe.application.swing.ui;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.ui.actions.CloseApplicationAction;
import fr.ird.observe.application.swing.ui.actions.ReloadStorageAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.ImageIcon;

import static org.nuiton.i18n.I18n.n;

/**
 * Définition des callback possibles lors d'une action.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public enum ObserveUICallback implements Runnable {
    application(n("observe.action.reload.application"),
                "application-reload") {
        @Override
        public void run() {
            if (log.isInfoEnabled()) {
                log.info("will reload application");
            }
            ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
            ObserveMainUIHandler handler = new ObserveMainUIHandler();
            ObserveMainUI ui = handler.getUI(context);

            if (log.isDebugEnabled()) {
                log.debug("Ask to reload.");
            }
            ObserveRunner.getRunner().setReload(true);
            new CloseApplicationAction(ui).run();
        }
    },
    ui(n("observe.action.reload.ui"),
       "ui-reload") {
        @Override
        public void run() {
            if (log.isInfoEnabled()) {
                log.info("will reload ui");
            }
            ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
            ObserveMainUIHandler handler = new ObserveMainUIHandler();
            ObserveMainUI ui = handler.getUI(context);
            ObserveSwingApplicationConfig config = ui.getConfig();
            handler.reloadUI(context, config);
        }
    },
    db(n("observe.action.reload.storage"),
       "db-reload") {
        @Override
        public void run() {
            if (log.isInfoEnabled()) {
                log.info("will reload db");
            }

            ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
            ObserveMainUIHandler handler = new ObserveMainUIHandler();
            ObserveMainUI ui = handler.getUI(context);
            new ReloadStorageAction(ui).run();
//            handler.launchReloadStorage(ui);
        }
    };

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveUICallback.class);

    private final String label;

    private final String iconPath;

    ObserveUICallback(String label, String iconPath) {
        this.iconPath = iconPath;
        this.label = label;

        getIcon();
    }

    public ImageIcon getIcon() {
        return UIHelper.createActionIcon(iconPath);
    }

    public String getLabel() {
        return label;
    }
}
