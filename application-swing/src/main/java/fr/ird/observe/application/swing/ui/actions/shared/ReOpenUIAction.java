/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.shared;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.ContentListUI;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ReOpenUIAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "reopen";

    public ReOpenUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.content.action.reopen"),
              n("observe.content.action.reopen.tip"),
              "reopen"
        );
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        SwingUtilities.invokeLater(() -> {
            JComponent c = (JComponent) e.getSource();
            ContentUI<?> ui = (ContentUI<?>)
                    c.getClientProperty("ui");

            if (ui == null) {
                throw new IllegalStateException(
                        "could not find client property ui " +
                        "on component" + c);
            }

            ContentOpenableUI<?> openUI;

            if (ui instanceof ContentOpenableUI<?>) {

                openUI = (ContentOpenableUI<?>) ui;
            } else if (ui instanceof ContentListUI<?, ?>) {

                DataReference<?> selectedData = ((ContentListUI<?, ?>) ui).getSelectedData();

                String id = selectedData.getId();

                ObserveTreeHelper treeHelper = ui.getTreeHelper();
                ObserveNode selectedNode = treeHelper.getSelectedNode();
                ObserveNode node = treeHelper.findNode(selectedNode, id);

                treeHelper.selectNode(node);

                openUI = (ContentOpenableUI<?>)
                        ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

            } else {
                throw new IllegalStateException("The action " + ACTION_NAME + " can not be executed from ui " + ui);
            }

            if (openUI != null) {
                openUI.openData();
            }
        });
    }
}
