package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDtos;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class SchoolEstimateUIModel extends ContentTableUIModel<SetSeineSchoolEstimateDto, SchoolEstimateDto> {

    private static final long serialVersionUID = 1L;

    public SchoolEstimateUIModel(SchoolEstimateUI ui) {

        super(SetSeineSchoolEstimateDto.class,
              SchoolEstimateDto.class,
              new String[]{
                      SetSeineSchoolEstimateDto.PROPERTY_SCHOOL_ESTIMATE,
                      SetSeineSchoolEstimateDto.PROPERTY_COMMENT},
              new String[]{
                       SchoolEstimateDto.PROPERTY_SPECIES,
                       SchoolEstimateDto.PROPERTY_TOTAL_WEIGHT,
                       SchoolEstimateDto.PROPERTY_MEAN_WEIGHT}
        );

        List<ContentTableMeta<SchoolEstimateDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(SchoolEstimateDto.class, SchoolEstimateDto.PROPERTY_SPECIES, false),
                ContentTableModel.newTableMeta(SchoolEstimateDto.class, SchoolEstimateDto.PROPERTY_TOTAL_WEIGHT, false),
                ContentTableModel.newTableMeta(SchoolEstimateDto.class, SchoolEstimateDto.PROPERTY_MEAN_WEIGHT, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<SetSeineSchoolEstimateDto, SchoolEstimateDto> createTableModel(
            ObserveContentTableUI<SetSeineSchoolEstimateDto, SchoolEstimateDto> ui,
            List<ContentTableMeta<SchoolEstimateDto>> contentTableMetas) {

        return new ContentTableModel<SetSeineSchoolEstimateDto, SchoolEstimateDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<SchoolEstimateDto> getChilds(SetSeineSchoolEstimateDto bean) {
                return bean.getSchoolEstimate();
            }

            @Override
            protected void load(SchoolEstimateDto source, SchoolEstimateDto target) {
                SchoolEstimateDtos.copySchoolEstimateDto(source, target);
            }

            @Override
            protected void setChilds(SetSeineSchoolEstimateDto parent, List<SchoolEstimateDto> childs) {
                bean.setSchoolEstimate(childs);
            }
        };
    }
}
