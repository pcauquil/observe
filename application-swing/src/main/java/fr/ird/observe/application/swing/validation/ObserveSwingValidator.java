/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.validation;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorProvider;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.xwork2.XWork2ValidatorUtil;

/**
 * Une surcharge du validateur swing offert par jaxx pour pouvoir ajouter dans
 * la stack le DataContext (pour faire de la validation sur le context de
 * données d'un niveau supérieur (valider une marée à partir d'une route par
 * exemple).
 *
 * @param <B> le type d'objet a valider
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ObserveSwingValidator<B> extends SwingValidator<B> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveSwingValidator.class);

    /** un etat pour initialiser la stack une unique fois la stack */
    private static boolean init;

    private static ValidationContext validationContext;

    public static <B> ObserveSwingValidator<B> newValidator(Class<B> type,
                                                            String context,
                                                            NuitonValidatorScope... scopes) {
        return new ObserveSwingValidator<>(
                type,
                context,
                scopes
        );
    }

    public static <B> ObserveSwingValidator<B> newValidator(NuitonValidatorProvider provider,
                                                            Class<B> type,
                                                            String context,
                                                            NuitonValidatorScope... scopes) {
        return new ObserveSwingValidator<>(
                provider,
                type,
                context,
                scopes
        );
    }

    public ObserveSwingValidator(
            Class<B> type,
            String context,
            NuitonValidatorScope... scopes) {
        super(NuitonValidatorFactory.getDefaultProvider(), type, context, scopes);
    }

    public ObserveSwingValidator(NuitonValidatorProvider provider,
                                 Class<B> type,
                                 String context,
                                 NuitonValidatorScope... scopes) {
        super(provider, type, context, scopes);
    }

    @Override
    protected void rebuildDelegateValidator(Class<B> beanType,
                                            String context,
                                            NuitonValidatorScope... scopes) {
        super.rebuildDelegateValidator(beanType, context, scopes);

        if (isInit()) {
            // deja initialise
            return;
        }

        // on positionne dans la stack de dataContext pour pouvoir faire de la
        // validation sur des objets dans le scope.

        ValidationContext dataContext = getValidationContext();
        if (dataContext == null) {

            // aucun context de validation enregistré
            if (log.isDebugEnabled()) {
                log.debug("No validation context registred, try in application context...");
            }
            ObserveSwingApplicationContext rootContext = ObserveSwingApplicationContext.get();
            if (rootContext == null) {
                throw new IllegalStateException(
                        "pas de context d'application  enregistré... utiliser la " +
                                "methode " +
                                //FIXME
//                        DataSourceFactory.class.getName() +
                                "#setApplicationContext(context)");
            }
            dataContext = rootContext.getValidationContext();
        }

        reloadDataContext(dataContext, true);

        // on marque pour ne jamais revenir ici
        setInit(true);
    }

    public static void reloadDataContext(ValidationContext validationContext, boolean strict) {
        if (validationContext != ObserveSwingValidator.validationContext) {

            // keep this validation context
            ObserveSwingValidator.validationContext = validationContext;
            setInit(false);
        }
        ValueStack valueStack;
        ActionContext context = ActionContext.getContext();
        if (context == null) {

            if (strict) {
                throw new IllegalStateException(
                        "pas de context xworks enregistré... utiliser la methode " +
                                ActionContext.class.getName() + "#setContext(context)");
            }

            valueStack = XWork2ValidatorUtil.getSharedValueStack();
            if (valueStack == null) {
                return;
            }
        } else {
            valueStack = context.getValueStack();
        }

        if (log.isDebugEnabled()) {
            log.debug("Enregistrement du context de validation [" + validationContext + "] dans la valueStack de " +
                              "validation (" + valueStack + ')');
        }

        valueStack.push(validationContext);
    }

    public static ValidationContext getValidationContext() {
        if (validationContext == null) {
            validationContext = ObserveSwingApplicationContext.get().getValidationContext();
        }
        return validationContext;
    }

    private static boolean isInit() {
        return init;
    }

    private static void setInit(boolean init) {
        ObserveSwingValidator.init = init;
    }


}
