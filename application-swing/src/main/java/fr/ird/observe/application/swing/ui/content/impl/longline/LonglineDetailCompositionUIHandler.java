package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SectionTemplate;
import fr.ird.observe.services.dto.longline.SectionWithTemplate;
import fr.ird.observe.services.dto.longline.SectionWithTemplates;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineDetailCompositionDtos;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.longline.SetLonglineDetailCompositionService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.util.JVetoableTabbedPane;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class LonglineDetailCompositionUIHandler extends ContentUIHandler<SetLonglineDetailCompositionDto> {

    static final String SECTION_TEMPLATES_EDITOR = "sectionTemplatesEditor";

    /** Logger */
    private static final Log log = LogFactory.getLog(LonglineDetailCompositionUIHandler.class);

    private final PropertyChangeListener sectionTemplatesTableModelModified;

    private final PropertyChangeListener sectionsTableModelModified;

    private final TableModelListener sectionsTableModelChanged;

    private final PropertyChangeListener branchlineDetailChanged;

    private final PropertyChangeListener selectedSectionTemplateChanged;

    private final PropertyChangeListener basketsTableModelModified;

    private final TableModelListener basketsTableModelChanged;

    private final PropertyChangeListener branchinesTableModelModified;

    private final TableModelListener branchinesTableModelChanged;

    private final PropertyChangeListener selectedBranchlineChanged;

    private final ChangeListener tabbedPaneChanged;

    private Decorator<SectionDto> sectionDecorator;

    private Decorator<BasketDto> basketDecorator;

    private Decorator<BranchlineDto> branchlineDecorator;

    /**
     * To avoid section flush when selected section changes (used when deleting a section).
     */
    private boolean skipSavePreviousSelectedSection;

    /**
     * To avoid basket flush when selection basket changes (used when deleting a bakset).
     */
    private boolean skipSavePreviousSelectedBasket;

    /**
     * To avoid branchline flush when selection branchline changes (used when deleting a branchline).
     */
    private boolean skipSavePreviousSelectedBranchline;

    /**
     * To avoid basket flush when section is adjusting (used when changing selected section).
     */
    private boolean sectionAdjusting;

    private final PropertyChangeListener modelCanGenerateChanged;

    private final PropertyChangeListener selectedSectionChanged;

    /**
     * To avoid branchline flush when basket is adjusting (used when changing selected basket).
     */
    private boolean basketAdjusting;

    private final PropertyChangeListener selectedBasketChanged;

    private LonglineDetailCompositionUIValidationHelper validationHelper;

    /**
     * To avoid some checks while opening ui.
     */
    private boolean onOpen;

    private final JVetoableTabbedPane.ChangeSelectedIndex tabbedPaneWillChanged;

    public LonglineDetailCompositionUIHandler(LonglineDetailCompositionUI ui) {
        super(ui, DataContextType.ActivityLongline, DataContextType.SetLongline);
        sectionTemplatesTableModelModified = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onSectionTemplatesTableModelModified(newValue);
        };
        sectionsTableModelModified = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onSectionsTableModelModified(newValue);
        };
        sectionsTableModelChanged = e -> {
            SectionsTableModel source = (SectionsTableModel) e.getSource();
            onSectionsTableModelChanged(source.getData());
        };
        branchlineDetailChanged = evt -> getUi().getValidator().setChanged(true);
        selectedSectionTemplateChanged = evt -> {
            SectionTemplate newValue = (SectionTemplate) evt.getNewValue();
            onSelectedSectionTemplateChanged(newValue);
        };
        basketsTableModelModified = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onBasketsTableModelModified(newValue);
        };
        basketsTableModelChanged = e -> {
            BasketsTableModel source = (BasketsTableModel) e.getSource();
            onBasketsTableModelChanged(source.getData());
        };
        branchinesTableModelModified = evt -> onBranchlinesTableModelModified((Boolean) evt.getNewValue());
        branchinesTableModelChanged = e -> {
            BranchlinesTableModel source = (BranchlinesTableModel) e.getSource();
            onBranchlinesTableModelChanged(source.getData());
        };
        selectedBranchlineChanged = evt -> {
            BranchlineDto previousValue = (BranchlineDto) evt.getOldValue();
            BranchlineDto newValue = (BranchlineDto) evt.getNewValue();
            onSelectedBranchlineChanged(previousValue, newValue);
        };
        tabbedPaneChanged = e -> {
            JVetoableTabbedPane source = (JVetoableTabbedPane) e.getSource();
            int previousIndex = source.getPreviousIndex();
            int selectedIndex = source.getSelectedIndex();
            onTabChanged(previousIndex, selectedIndex);
        };
        modelCanGenerateChanged = evt -> {

            sectionAdjusting = true;

            try {
                Boolean newValue = (Boolean) evt.getNewValue();
                onModelCanGenerateChanged(newValue);
            } finally {

                sectionAdjusting = false;
            }
        };
        selectedSectionChanged = evt -> {

            sectionAdjusting = true;

            try {
                SectionDto previousValue = (SectionDto) evt.getOldValue();
                SectionDto newValue = (SectionDto) evt.getNewValue();
                onSelectedSectionChanged(previousValue, newValue);
            } finally {

                sectionAdjusting = false;
            }
        };
        selectedBasketChanged = evt -> {

            basketAdjusting = true;

            try {
                BasketDto previousValue = (BasketDto) evt.getOldValue();
                BasketDto newValue = (BasketDto) evt.getNewValue();
                onSelectedBasketChanged(previousValue, newValue);
            } finally {

                basketAdjusting = false;
            }
        };
        tabbedPaneWillChanged = this::onTabWillChanged;
    }

    @Override
    public LonglineDetailCompositionUI getUi() {
        return (LonglineDetailCompositionUI) super.getUi();
    }

    @Override
    public LonglineDetailCompositionUIModel getModel() {
        return (LonglineDetailCompositionUIModel) super.getModel();
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        ContentMode contentMode;

        if (getOpenDataManager().isOpenActivityLongline(dataContext.getSelectedActivityLonglineId())) {

            // l'activité est ouverte, mode édition
            contentMode = ContentMode.UPDATE;
        } else {

            // l'activité n'est pas ouverte, mode lecture
            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivityLonglineDto.class),
                       t("observe.storage.activityLongline.message.not.open"));

            contentMode = ContentMode.READ;
        }

        return contentMode;

    }

    @Override
    public void initUI() {

        this.sectionDecorator = getDecoratorService().getDecoratorByType(SectionDto.class);
        this.basketDecorator = getDecoratorService().getDecoratorByType(BasketDto.class);
        this.branchlineDecorator = getDecoratorService().getDecoratorByType(BranchlineDto.class);

        LonglineDetailCompositionUIInitializer uiInitializer = new LonglineDetailCompositionUIInitializer(getUi());
        uiInitializer.initUI();

        LonglineDetailCompositionUI ui1 = getUi();

        this.validationHelper = new LonglineDetailCompositionUIValidationHelper(ui1, getDecoratorService());

        getBranchlineDetailUIModel().addPropertyChangeListener(BranchlineUIModel.PROPERTY_SAVED, branchlineDetailChanged);

        {
            // init section templates table

            SectionTemplatesTableModel tableModel = getSectionTemplatesTableModel();
            tableModel.addPropertyChangeListener(SectionTemplatesTableModel.MODIFIED_PROPERTY, sectionTemplatesTableModelModified);

            getModel().addPropertyChangeListener(LonglineDetailCompositionUIModel.PROPERTY_CAN_GENERATE, modelCanGenerateChanged);
        }

        {
            // init sections table

            SectionsTableModel tableModel = getSectionsTableModel();
            tableModel.addPropertyChangeListener(SectionsTableModel.MODIFIED_PROPERTY, sectionsTableModelModified);
            tableModel.addPropertyChangeListener(SectionsTableModel.SELECTED_ROW_PROPERTY, selectedSectionChanged);
            tableModel.addPropertyChangeListener(SectionsTableModel.TEMPLATE_PROPERTY, selectedSectionTemplateChanged);
            tableModel.addTableModelListener(sectionsTableModelChanged);

        }
        {
            // init baskets table

            BasketsTableModel tableModel = getBasketsTableModel();
            tableModel.addPropertyChangeListener(BasketsTableModel.MODIFIED_PROPERTY, basketsTableModelModified);
            tableModel.addPropertyChangeListener(BasketsTableModel.SELECTED_ROW_PROPERTY, selectedBasketChanged);
            tableModel.addTableModelListener(basketsTableModelChanged);

        }

        {
            // init branchlines table

            BranchlinesTableModel tableModel = getBranchlinesTableModel();
            tableModel.addPropertyChangeListener(BranchlinesTableModel.MODIFIED_PROPERTY, branchinesTableModelModified);
            tableModel.addPropertyChangeListener(BranchlinesTableModel.SELECTED_ROW_PROPERTY, selectedBranchlineChanged);
            tableModel.addTableModelListener(branchinesTableModelChanged);

        }

        JVetoableTabbedPane tabPane = ui1.getFishingOperationTabPane();

        tabPane.setChangeSelectedIndex(tabbedPaneWillChanged);
        tabPane.addChangeListener(tabbedPaneChanged);

    }

    @Override
    public void openUI() {

        UIHelper.stopEditing(getUi().getSectionTemplatesTable());
        UIHelper.stopEditing(getUi().getSectionsTable());

        super.openUI();

        {
            // open brancheline detail
            BranchlineUI branchlineDetailUI = getUi().getBranchlineDetailUI();
            branchlineDetailUI.open();
        }

        String setId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info("setId      = " + setId);
        }
        ContentMode mode = computeContentMode();
        // utilisation du mode requis
        setContentMode(mode);

        SetLonglineDetailCompositionService setLonglineService = getSetLonglineDetailCompositionService();

        Form<SetLonglineDetailCompositionDto> setLonglineDtoForm = setLonglineService.loadForm(setId);

        loadReferentialReferenceSetsInModel(setLonglineDtoForm);

        getModel().setForm(setLonglineDtoForm);

        SetLonglineDetailCompositionDto bean = getBean();
        SetLonglineDetailCompositionDtos.copySetLonglineDetailCompositionDto(setLonglineDtoForm.getObject(), bean);

        //FIXME Voir comment gérer ça
        Form<BranchlineDto> branchlineDtoForm = Form.newFormDto(BranchlineDto.class, null, null, null);

        BranchlineUIModel branchlineUIModel = getUi().getBranchlineDetailUI().getModel();
        branchlineUIModel.setReferentialReferenceSets(getModel().getReferentialReferenceSets());
        branchlineUIModel.setForm(branchlineDtoForm);

        //FIXME Validate this default date
        if (log.isInfoEnabled()) {
            log.info("Use as default branchline timer date: " + bean.getSettingStartTimeStamp());
        }
        getBranchlinesTableModel().setDefaultDate(bean.getSettingStartTimeStamp());
        getBranchlinesTableModel().setUseTimer(BooleanUtils.isTrue(bean.getMonitored()));

        // by default, can generate if there is no section in database
        getModel().setCanGenerate(bean.isSectionEmpty());

        // TODO Use a cache of templates on setLongline (session scope)
        getSectionTemplatesTableModel().setData(new ArrayList<>());

        BranchlineUI branchlineDetailUI = getUi().getBranchlineDetailUI();
        branchlineDetailUI.edit(null);

        onOpen = true;

        try {

            List<SectionWithTemplate> section = SectionWithTemplates.getSectionTemplates(bean.getSection());
            getSectionsTableModel().setData(section);

            validationHelper.setObjectValueAdjusting(true);

            try {

                onSectionTemplatesTableModelModified(false);
                onSectionsTableModelModified(false);
                onBasketsTableModelModified(false);
                onBranchlinesTableModelModified(false);

            } finally {

                validationHelper.setObjectValueAdjusting(false);
                validationHelper.whenSectionChanged();

            }

            // Always go back to first tab
            getUi().getFishingOperationTabPane().setSelectedIndex(0);

        } finally {

            onOpen = false;

        }

        if (mode != ContentMode.READ) {
            getUi().startEdit(null);
        }

    }

    @Override
    public void startEditUI(String... binding) {

        ContentUIModel<SetLonglineDetailCompositionDto> model = getModel();
        addInfoMessage(t("observe.content.longlineDetailComposition.message.updating"));

        super.startEditUI(LonglineDetailCompositionUI.BINDING_GENERATE_ALL_ENABLED,
                          LonglineDetailCompositionUI.BINDING_DELETE_ALL_ENABLED);

        model.setModified(false);

    }

    @Override
    protected boolean doSave(SetLonglineDetailCompositionDto bean) throws Exception {

        BranchlineUI branchlineDetailUI = getUi().getBranchlineDetailUI();

        boolean continueSave = true;

        if (getUi().getFishingOperationTabPane().getSelectedIndex() == 2) {

            BranchlineUIHandler branchlineUIHandler = branchlineDetailUI.getHandler();

            continueSave = branchlineUIHandler.tryToQuit();

        }

        if (continueSave) {

            SectionsTableModel sectionsTableModel = getSectionsTableModel();
            SectionWithTemplate selectedSection = sectionsTableModel.getSelectedRow();
            if (selectedSection != null) {

                // flush selected section before save
                flushSection(selectedSection);

            }

            SaveResultDto saveResult = getSetLonglineDetailCompositionService().save(bean);
            saveResult.toDto(bean);

        }

        return continueSave;
    }

    public void generateAllSections() {

        if (log.isInfoEnabled()) {
            log.info("Generate all sections.");
        }

        SetLonglineDetailCompositionDto bean = getBean();
        Integer nbSections = bean.getTotalSectionsCount();
        Integer basketsCount = bean.getBasketsPerSectionCount();
        Integer nbBranchlines = bean.getBranchlinesPerBasketCount();

        SectionTemplate template = null;

        SectionTemplatesTableModel sectionTemplatesTableModel = getSectionTemplatesTableModel();
        List<SectionTemplate> sectionTemplates = sectionTemplatesTableModel.getNotEmptyData();
        if (sectionTemplates.size() == 1) {

            template = sectionTemplates.get(0);

            boolean compiliantWithBasketCount = template.isCompiliantWithBasketCount(basketsCount);
            if (!compiliantWithBasketCount) {

                if (log.isWarnEnabled()) {
                    log.warn("sectionTemplate " + template + " is not compliant with basketCount: " + basketsCount);
                }

                UIHelper.displayWarning(t("observe.content.longlineDetailComposition.title.section.cant.use.firstTemplate"), t("observe.content.longlineDetailComposition.firstTemplate.template.notCompilant.basketCount", template.getFloatlineLengths(), basketsCount));

                template = null;

            }

        }

        boolean usingTemplate = template != null;

        if (usingTemplate) {

            if (log.isInfoEnabled()) {
                log.info("Will use sectionTemplate: " + template);
            }
        }

        validationHelper.setObjectValueAdjusting(true);

        try {

            SectionsTableModel sectionsTableModel = getSectionsTableModel();
            BasketsTableModel basketsTableModel = getBasketsTableModel();
            BranchlinesTableModel branchlinesTableModel = getBranchlinesTableModel();

            List<SectionWithTemplate> sections = new ArrayList<>(nbSections);

            for (int sectionNumber = 0; sectionNumber < nbSections; sectionNumber++) {

                SectionWithTemplate section = sectionsTableModel.createNewRow();
                sections.add(section);

                for (int basketNumber = 0; basketNumber < basketsCount; basketNumber++) {

                    BasketDto basket = basketsTableModel.createNewRow();
                    section.addBasket(basket);

                    for (int branchlineNumber = 0; branchlineNumber < nbBranchlines; branchlineNumber++) {

                        BranchlineDto branchline = branchlinesTableModel.createNewRow();
                        basket.addBranchline(branchline);

                    }

                    LinkedList<BranchlineDto> branchlines = Lists.newLinkedList(basket.getBranchline());
                    branchlinesTableModel.rearrangeIds(branchlines);

                }

                List<BasketDto> baskets = Lists.newLinkedList(section.getBasket());
                basketsTableModel.rearrangeIds(baskets);

                if (usingTemplate) {

                    section.setSectionTemplate(template);
                    template.applyToBaskets(baskets);

                }

            }

            sectionsTableModel.rearrangeIds(sections);
            sectionsTableModel.setData(sections);

            List<SectionDto> sectionDtos = sections.stream()
                                                   .map(SectionWithTemplate::getDelegate)
                                                   .collect(Collectors.toList());

            getBean().addAllSection(sectionDtos);

            getModel().setModified(true);

        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();

        }

    }

    public void deleteAllSections() {

        if (getSectionsTableModel().isEmpty()) {

            // no section
            return;
        }

        boolean canDelete = true;
        for (SectionWithTemplate sectionWithTemplate : getSectionsTableModel().getNotEmptyData()) {

            if (!canDeleteSection(sectionWithTemplate.getDelegate())) {
                canDelete = false;
                break;
            }
        }

        if (!canDelete) {

            // there is some references, can't delete
            UIHelper.displayWarning(t("observe.content.section.cant.delete.title"), t("observe.content.section.cant.delete.message"));
            return;

        }

        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        int response = UIHelper.askUser(mainUI,
                                        t("observe.title.delete"),
                                        t("observe.content.sections.delete.message"),
                                        JOptionPane.WARNING_MESSAGE,
                                        new Object[]{t("observe.choice.confirm.delete"),
                                                t("observe.choice.cancel")},
                                        1);

        if (response != 0) {

            // user cancel
            return;
        }

        if (log.isInfoEnabled()) {
            log.info("Remove all sections.");
        }

        validationHelper.setObjectValueAdjusting(true);

        try {

            getSectionsTableModel().setData(new ArrayList<>());
            getBean().getSection().clear();
            getModel().setModified(true);

        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();

        }

    }

    public void deleteSelectedSectionTemplate() {

        SectionTemplatesTableModel tableModel = getSectionTemplatesTableModel();
        boolean selectionEmpty = tableModel.isSelectionEmpty();

        if (!selectionEmpty) {

            SectionTemplate data = getSectionTemplatesTableModel().getSelectedRow();

            if (log.isInfoEnabled()) {
                log.info("Delete: " + data);
            }

            ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
            int response = UIHelper.askUser(mainUI,
                                            t("observe.title.delete"),
                                            t("observe.content.sectionTemplate.delete.message"),
                                            JOptionPane.WARNING_MESSAGE,
                                            new Object[]{t("observe.choice.confirm.delete"),
                                                    t("observe.choice.cancel")},
                                            1);

            if (response != 0) {

                // user cancel
                return;
            }

            getSectionTemplatesTableModel().removeSelectedRow();

        }

    }

    public void deleteSelectedSection() {

        SectionsTableModel tableModel = getSectionsTableModel();

        boolean selectionEmpty = tableModel.isSelectionEmpty();

        if (!selectionEmpty) {

            SectionWithTemplate selectedSection = tableModel.getSelectedRow();

            boolean canDelete = canDeleteSection(selectedSection.getDelegate());

            if (!canDelete) {
                // there is some references, can't delete
                UIHelper.displayWarning(t("observe.content.section.cant.delete.title"), t("observe.content.section.cant.delete.message"));
                return;
            }

            boolean accept = UIHelper.confirmForEntityDelete(ui, SectionDto.class, selectedSection);

            ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
            mainUI.setBusy(false);

            if (!accept) {
                return;
            }

            if (log.isInfoEnabled()) {
                log.info("Delete section: " + sectionDecorator.toString(selectedSection));
            }

            validationHelper.setObjectValueAdjusting(true);
            skipSavePreviousSelectedSection = true;

            try {

                getBean().removeSection(tableModel.getSelectedRow().getDelegate());
                tableModel.removeSelectedRow();

            } finally {

                skipSavePreviousSelectedSection = false;
                validationHelper.setObjectValueAdjusting(false);
                validationHelper.whenSectionChanged();

            }

        }

    }

    public void insertBeforeSelectedSection() {

        validationHelper.setObjectValueAdjusting(true);

        try {

            SectionsTableModel tableModel = getSectionsTableModel();
            tableModel.insertBeforeSelectedRow();

            SectionWithTemplate newSectionWithTemplate = tableModel.getSelectedRow();
            int selectedRowIndex = tableModel.getSelectedRowIndex();
            List<SectionDto> sections = newArrayList(getBean().getSection());
            sections.add(selectedRowIndex, newSectionWithTemplate.getDelegate());
            getBean().getSection().clear();
            getBean().addAllSection(sections);

        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();

        }

    }

    public void insertAfterSelectedSection() {

        validationHelper.setObjectValueAdjusting(true);

        try {

            SectionsTableModel tableModel = getSectionsTableModel();
            tableModel.insertAfterSelectedRow();

            SectionWithTemplate newSectionWithTemplate = tableModel.getSelectedRow();
            int selectedRowIndex = tableModel.getSelectedRowIndex();
            List<SectionDto> sections = newArrayList(getBean().getSection());
            sections.add(selectedRowIndex, newSectionWithTemplate.getDelegate());
            getBean().getSection().clear();
            getBean().addAllSection(sections);

        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenSectionChanged();

        }

    }

    public void deleteSelectedBasket() {

        BasketsTableModel tableModel = getBasketsTableModel();

        boolean selectionNotEmpty = !tableModel.isSelectionEmpty();

        if (selectionNotEmpty) {

            BasketDto selectedBasket = tableModel.getSelectedRow();

            boolean canDelete = canDeleteBasket(selectedBasket);

            if (!canDelete) {

                // there is some references, can't delete
                UIHelper.displayWarning(t("observe.content.basket.cant.delete.title"), t("observe.content.basket.cant.delete.message"));
                return;

            }

            boolean accept = UIHelper.confirmForEntityDelete(ui, BasketDto.class, selectedBasket);
            ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
            mainUI.setBusy(false);

            if (!accept) {
                return;
            }

            if (log.isInfoEnabled()) {
                log.info("Delete basket: " + basketDecorator.toString(selectedBasket));
            }

            validationHelper.setObjectValueAdjusting(true);
            skipSavePreviousSelectedBasket = true;

            try {

                getSectionsTableModel().getSelectedRow().removeBasket(selectedBasket);
                tableModel.removeSelectedRow();

            } finally {

                skipSavePreviousSelectedBasket = false;

                validationHelper.setObjectValueAdjusting(false);
                validationHelper.whenBasketChanged();

            }

        }

    }

    public void insertBeforeSelectedBasket() {

        validationHelper.setObjectValueAdjusting(true);

        try {

            BasketsTableModel tableModel = getBasketsTableModel();
            tableModel.insertBeforeSelectedRow();

            BasketDto newBasket = tableModel.getSelectedRow();
            int selectedRowIndex = tableModel.getSelectedRowIndex();
            SectionWithTemplate selectedSection = getSectionsTableModel().getSelectedRow();
            if (selectedSection.isBasketEmpty()) {
                selectedSection.setBasket(new LinkedHashSet<>());
            }
            List<BasketDto> baskets = newArrayList(selectedSection.getBasket());
            baskets.add(selectedRowIndex, newBasket);
            selectedSection.getBasket().clear();
            selectedSection.addAllBasket(baskets);

        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBasketChanged();

        }

    }

    public void insertAfterSelectedBasket() {

        validationHelper.setObjectValueAdjusting(true);

        try {

            BasketsTableModel tableModel = getBasketsTableModel();
            tableModel.insertAfterSelectedRow();

            BasketDto newBasket = tableModel.getSelectedRow();
            int selectedRowIndex = tableModel.getSelectedRowIndex();
            SectionWithTemplate selectedSection = getSectionsTableModel().getSelectedRow();
            if (selectedSection.isBasketEmpty()) {
                selectedSection.setBasket(new LinkedHashSet<>());
            }
            List<BasketDto> baskets = newArrayList(selectedSection.getBasket());
            baskets.add(selectedRowIndex, newBasket);
            selectedSection.getBasket().clear();
            selectedSection.addAllBasket(baskets);

        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBasketChanged();

        }

    }

    public void deleteSelectedBranchline() {

        BranchlinesTableModel tableModel = getBranchlinesTableModel();

        boolean selectionNotEmpty = !tableModel.isSelectionEmpty();

        if (selectionNotEmpty) {

            BranchlineDto selectedBranchline = tableModel.getSelectedRow();

            boolean canDelete = canDeleteBranchline(selectedBranchline);

            if (!canDelete) {

                // there is some references, can't delete
                UIHelper.displayWarning(t("observe.content.branchLine.cant.delete.title"), t("observe.content.branchLine.cant.delete.message"));
                return;

            }

            boolean accept = UIHelper.confirmForEntityDelete(ui, BranchlineDto.class, selectedBranchline);
            ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
            mainUI.setBusy(false);
            if (!accept) {
                return;
            }

            if (log.isInfoEnabled()) {
                log.info("Delete branchline: " + branchlineDecorator.toString(selectedBranchline));
            }

            validationHelper.setObjectValueAdjusting(true);
            skipSavePreviousSelectedBranchline = true;

            try {

                getBasketsTableModel().getSelectedRow().removeBranchline(selectedBranchline);
                tableModel.removeSelectedRow();

            } finally {

                skipSavePreviousSelectedBranchline = false;
                validationHelper.setObjectValueAdjusting(false);
                validationHelper.whenBranchlineChanged();

            }

        }

    }

    public void insertBeforeSelectedBranchline() {

        validationHelper.setObjectValueAdjusting(true);

        try {

            BranchlinesTableModel tableModel = getBranchlinesTableModel();
            tableModel.insertBeforeSelectedRow();

            BranchlineDto newBranchline = tableModel.getSelectedRow();
            int selectedRowIndex = tableModel.getSelectedRowIndex();
            BasketDto selectedBasket = getBasketsTableModel().getSelectedRow();
            if (selectedBasket.isBranchlineEmpty()) {
                selectedBasket.setBranchline(new LinkedHashSet<>());
            }

            List<BranchlineDto> branchlines = newArrayList(selectedBasket.getBranchline());
            branchlines.add(selectedRowIndex, newBranchline);
            selectedBasket.getBranchline().clear();
            selectedBasket.addAllBranchline(branchlines);

        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBasketChanged();

        }

    }

    public void insertAfterSelectedBranchline() {

        validationHelper.setObjectValueAdjusting(true);

        try {

            BranchlinesTableModel tableModel = getBranchlinesTableModel();
            tableModel.insertAfterSelectedRow();

            BranchlineDto newBranchline = tableModel.getSelectedRow();
            int selectedRowIndex = tableModel.getSelectedRowIndex();
            BasketDto selectedBasket = getBasketsTableModel().getSelectedRow();
            if (selectedBasket.isBranchlineEmpty()) {
                selectedBasket.setBranchline(new LinkedHashSet<>());
            }
            List<BranchlineDto> branchlines = newArrayList(selectedBasket.getBranchline());
            branchlines.add(selectedRowIndex, newBranchline);
            selectedBasket.getBranchline().clear();
            selectedBasket.addAllBranchline(branchlines);


        } finally {

            validationHelper.setObjectValueAdjusting(false);
            validationHelper.whenBasketChanged();

        }

    }

    protected void onSectionTemplatesTableModelModified(Boolean newValue) {

        // recompute table model valid state
        getSectionTemplatesTableModel().validate();

    }

    protected void onSectionsTableModelModified(Boolean newValue) {

        if (newValue) {

            getModel().setModified(true);

        }

        validationHelper.whenSectionChanged();

    }

    protected void onSectionsTableModelChanged(List<SectionWithTemplate> data) {

        if (log.isInfoEnabled()) {
            log.info("Sections was changed, new size: " + data.size());
        }

        validationHelper.whenSectionChanged();

    }

    protected void onModelCanGenerateChanged(Boolean canGenerate) {

        if (canGenerate) {

            if (getSectionTemplatesTableModel().isEmpty()) {

                // add an empty row
                getSectionTemplatesTableModel().addNewRow();

            }

        }

    }

    protected void onSelectedSectionChanged(SectionDto previousSection, SectionDto newSection) {

        if (log.isInfoEnabled()) {
            log.info("New selected section: " + sectionDecorator.toString(newSection));
        }

        validationHelper.setObjectValueAdjusting(true);

        try {

            if (previousSection != null && !onOpen && !skipSavePreviousSelectedSection) {

                // must flush back baksets to the previous section
                flushSection(previousSection);

            }

            List<BasketDto> baskets = newSection == null ? new ArrayList<>() : newArrayList(newSection.getBasket());
            getBasketsTableModel().setData(baskets);

        } finally {

            validationHelper.setObjectValueAdjusting(false);

        }

    }

    protected void onSelectedSectionTemplateChanged(SectionTemplate newTemplate) {

        if (newTemplate != null) {

            SectionWithTemplate selectedSection = getSectionsTableModel().getSelectedRow();
            if (log.isInfoEnabled()) {
                log.info("Will apply template: " + newTemplate + " to section: " + sectionDecorator.toString(selectedSection));
            }

            validationHelper.setObjectValueAdjusting(true);

            try {

                getBasketsTableModel().applySectionTemplate(newTemplate);

            } finally {

                validationHelper.setObjectValueAdjusting(false);
                validationHelper.whenSectionChanged();

            }

        }

    }

    protected void onBasketsTableModelModified(Boolean newValue) {

        if (newValue) {

            getModel().setModified(true);

        }

        validationHelper.whenBasketChanged();

    }

    protected void onBasketsTableModelChanged(List<BasketDto> data) {

        if (log.isInfoEnabled()) {
            log.info("Baskets was changed, new size: " + data.size());
        }

    }

    protected void onSelectedBasketChanged(BasketDto previousBasket, BasketDto newBasket) {

        if (log.isInfoEnabled()) {
            log.info("New selected basket: " + basketDecorator.toString(newBasket));
        }

        if (previousBasket != null && !sectionAdjusting && !skipSavePreviousSelectedBasket) {

            flushBasket(previousBasket);

        }

        List<BranchlineDto> branchlines = new ArrayList<>();
        if (newBasket != null && newBasket.getBranchline() != null) {
            branchlines.addAll(newBasket.getBranchline());
        }
        getBranchlinesTableModel().setData(branchlines);

    }

    protected void onBranchlinesTableModelModified(Boolean newValue) {

        if (newValue) {

            getModel().setModified(true);

        }

        validationHelper.whenBranchlineChanged();

    }

    protected void onBranchlinesTableModelChanged(List<BranchlineDto> data) {

        if (log.isInfoEnabled()) {
            log.info("Branchlines was changed, new size: " + data.size());
        }

    }

    protected void onSelectedBranchlineChanged(BranchlineDto previousBranchline, BranchlineDto newBranchline) {

        if (log.isInfoEnabled()) {
            log.info("New selected branchline: " + branchlineDecorator.toString(newBranchline));
        }

        if (previousBranchline != null && !basketAdjusting && !skipSavePreviousSelectedBranchline) {

            // must flush back branchline detail to his row
            flushBranchline(previousBranchline);

        }

    }

    protected boolean onTabWillChanged(int selectedIndex, int index) {

        boolean result = true;

        if (!onOpen && index > -1) {

            switch (selectedIndex) {

                case 0:

                    result = getModel().isGenerateTabValid();

                    break;

                case 1:

                    result = getModel().isCompositionTabValid();

                    break;

                case 2:

                    result = getUi().getBranchlineDetailUI().getHandler().tryToQuit();

                    break;
            }

        }

        return result;
    }

    protected void onTabChanged(int previousIndex, int selectedIndex) {

        switch (previousIndex) {
            case 0:

                // abort editing table
                UIHelper.stopEditing(getUi().getSectionTemplatesTable());

                break;
            case 1:

                // abort editing tables
                UIHelper.stopEditing(getUi().getSectionsTable());

                break;
            case 2:

                BranchlineUI branchlineDetailUI = getUi().getBranchlineDetailUI();
                branchlineDetailUI.getModel().removePropertyChangeListener(BranchlineUIModel.PROPERTY_MODIFIED, branchlineDetailChanged);
                branchlineDetailUI.edit(null);

                break;
        }

        switch (selectedIndex) {
            case 0:

                break;
            case 1:

                if (previousIndex == 0) {

                    if (getModel().isCanGenerate()) {

                        // update section templates list
                        List<SectionTemplate> sectionTemplates = getSectionTemplatesTableModel().getNotEmptyData();
                        JComboBox comboBox = (JComboBox) getUi().getSectionsTable().getClientProperty(SECTION_TEMPLATES_EDITOR);
                        LonglineDetailCompositionUIInitializer.prepareComboBoxData(comboBox, sectionTemplates);

                        //FIXME See why templates are not well reselect in cell editor
                        //FIXME See cell editor does not loose focus and empty selection when losing focus

                    }
                }

                break;
            case 2:

                BranchlineDto branchline = getBranchlinesTableModel().getSelectedRow();

                if (branchline != null) {

                    // update branchline detail
                    BranchlineUI branchlineDetailUI = getUi().getBranchlineDetailUI();
                    branchlineDetailUI.edit(branchline);
                    branchlineDetailUI.getModel().addPropertyChangeListener(BranchlineUIModel.PROPERTY_MODIFIED, branchlineDetailChanged);

                }

                break;

        }

    }

    protected void flushSection(SectionDto section) {

        BasketsTableModel basketsTableModel = getBasketsTableModel();

        if (!basketsTableModel.isSelectionEmpty()) {

            // must flush back branchlines to selected basket
            BasketDto selectedBasket = basketsTableModel.getSelectedRow();
            flushBasket(selectedBasket);

        }

        // flush bask baskets to the given section

        List<BasketDto> baskets = basketsTableModel.getNotEmptyData();
        section.setBasket(new LinkedHashSet<>(baskets));

        if (log.isInfoEnabled()) {
            log.info("Flush baskets (" + baskets.size() + ") to his section: " + sectionDecorator.toString(section));
        }

    }

    protected void flushBasket(BasketDto basket) {

        BranchlinesTableModel branchlinesTableModel = getBranchlinesTableModel();

        if (!branchlinesTableModel.isSelectionEmpty()) {

            // must flush back details to selected branchline
            flushBranchline(branchlinesTableModel.getSelectedRow());

        }

        // flush bask branchlines to the given basket

        List<BranchlineDto> branchlines = branchlinesTableModel.getNotEmptyData();
        basket.setBranchline(new LinkedHashSet<>(branchlines));

        if (log.isInfoEnabled()) {
            log.info("Flush branchlines (" + branchlines.size() + ") to his basket: " + basketDecorator.toString(basket));
        }

    }

    protected void flushBranchline(BranchlineDto branchline) {

        if (log.isInfoEnabled()) {
            log.info("Flush branchline details: " + branchlineDecorator.toString(branchline));
        }

    }

    protected SectionTemplatesTableModel getSectionTemplatesTableModel() {
        return getModel().getSectionTemplatesTableModel();
    }

    protected SectionsTableModel getSectionsTableModel() {
        return getModel().getSectionsTableModel();
    }

    protected BasketsTableModel getBasketsTableModel() {
        return getModel().getBasketsTableModel();
    }

    protected BranchlinesTableModel getBranchlinesTableModel() {
        return getModel().getBranchlinesTableModel();
    }

    public BranchlineUIModel getBranchlineDetailUIModel() {
        return getUi().getBranchlineDetailUIModel();
    }

    protected boolean canDeleteSection(SectionDto section) {

        return section.isNotPersisted() || getSetLonglineDetailCompositionService().canDeleteSection(section.getId());

    }

    protected boolean canDeleteBasket(BasketDto basket) {

        return basket.isNotPersisted() || getSetLonglineDetailCompositionService().canDeleteBasket(basket.getId());

    }

    protected boolean canDeleteBranchline(BranchlineDto branchline) {

        return branchline.isNotPersisted() || getSetLonglineDetailCompositionService().canDeleteBranchline(branchline.getId());

    }

    protected SetLonglineDetailCompositionService getSetLonglineDetailCompositionService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newSetLonglineDetailCompositionService();
    }

}
