package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.GearUseFeaturesLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineGearUseDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.List;
import java.util.Set;

/**
 * Created on 3/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.16
 */
public class GearUseFeaturesLonglineUIModel extends ContentTableUIModel<TripLonglineGearUseDto, GearUseFeaturesLonglineDto> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(GearUseFeaturesLonglineDto.PROPERTY_GEAR,
                                               GearUseFeaturesLonglineDto.PROPERTY_NUMBER,
                                               GearUseFeaturesLonglineDto.PROPERTY_USED_IN_TRIP,
                                               GearUseFeaturesLonglineDto.PROPERTY_COMMENT).build();

    protected boolean generalTabValid;

    private final GearUseFeaturesMeasurementLonglinesTableModel measurementsTableModel;

    public GearUseFeaturesLonglineUIModel(GearUseFeaturesLonglineUI ui) {

        super(TripLonglineGearUseDto.class,
              GearUseFeaturesLonglineDto.class,
              new String[]{
                      TripLonglineGearUseDto.PROPERTY_ID,
                      TripLonglineGearUseDto.PROPERTY_GEAR_USE_FEATURES_LONGLINE,
                      TripLonglineGearUseDto.PROPERTY_LAST_UPDATE_DATE,
              },
              new String[]{
                      GearUseFeaturesLonglineDto.PROPERTY_ID,
                      GearUseFeaturesLonglineDto.PROPERTY_COMMENT,
                      GearUseFeaturesLonglineDto.PROPERTY_GEAR,
                      GearUseFeaturesLonglineDto.PROPERTY_NUMBER,
                      GearUseFeaturesLonglineDto.PROPERTY_USED_IN_TRIP});

        this.measurementsTableModel = new GearUseFeaturesMeasurementLonglinesTableModel();

        List<ContentTableMeta<GearUseFeaturesLonglineDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(GearUseFeaturesLonglineDto.class, GearUseFeaturesLonglineDto.PROPERTY_GEAR, false),
                ContentTableModel.newTableMeta(GearUseFeaturesLonglineDto.class, GearUseFeaturesLonglineDto.PROPERTY_NUMBER, false),
                ContentTableModel.newTableMeta(GearUseFeaturesLonglineDto.class, GearUseFeaturesLonglineDto.PROPERTY_USED_IN_TRIP, false),
                ContentTableModel.newTableMeta(GearUseFeaturesLonglineDto.class, GearUseFeaturesLonglineDto.PROPERTY_COMMENT, false)
        );

        initModel(ui, metas);

    }

    @Override
    protected GearUseFeaturesLonglineTableModel createTableModel(ObserveContentTableUI<TripLonglineGearUseDto, GearUseFeaturesLonglineDto> ui, List<ContentTableMeta<GearUseFeaturesLonglineDto>> contentTableMetas) {
        return new GearUseFeaturesLonglineTableModel(ui, contentTableMetas);
    }

    public GearUseFeaturesMeasurementLonglinesTableModel getMeasurementsTableModel() {
        return measurementsTableModel;
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, null, isGeneralTabValid());
    }

}
