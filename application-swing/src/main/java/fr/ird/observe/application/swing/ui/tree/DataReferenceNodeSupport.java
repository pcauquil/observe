package fr.ird.observe.application.swing.ui.tree;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.OpenableDto;
import fr.ird.observe.application.swing.ui.tree.loadors.AbstractNodeChildLoador;
import jaxx.runtime.swing.nav.NavBridge;
import jaxx.runtime.swing.nav.NavDataProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.tree.DefaultTreeModel;

/**
 * Created on 4/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0
 */
public abstract class DataReferenceNodeSupport<E extends DataDto> extends AbstrctReferenceNodeSupport<E, DataReference<E>> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataReferenceNodeSupport.class);

    private static final long serialVersionUID = 1L;

    protected DataReferenceNodeSupport(Class<E> internalClass, DataReference<E> entity) {
        super(internalClass, entity, null);
    }

    protected DataReferenceNodeSupport(Class<E> type, DataReference<E> entity, AbstractNodeChildLoador<?, ?> childLoador) {
        super(type, entity, null, childLoador);
    }

    protected DataReferenceNodeSupport(Class<E> type, DataReference<E> entity, String context, AbstractNodeChildLoador<?, ?> childLoador) {
        super(type, entity, context, childLoador);
    }

    @Override
    public void populateNode(NavBridge<DefaultTreeModel, ObserveNode> bridge,
                             NavDataProvider provider,
                             boolean populateChilds) {


        try {
            super.populateNode(bridge, provider, populateChilds);
        } finally {
            reloadEntity = false;
        }

        if (provider != null && id != null) {

            if (entity == null) {

                loadEntity((ObserveDataProvider) provider);

            }

            if (OpenableDto.class.isAssignableFrom(entity.getType())) {
                ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
                ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();
                open = openDataManager.isOpen(entity.getId());
                if (log.isDebugEnabled()) {
                    log.debug("Set open: " + open + " for entity: " + id);
                }

            }

        }

    }

}
