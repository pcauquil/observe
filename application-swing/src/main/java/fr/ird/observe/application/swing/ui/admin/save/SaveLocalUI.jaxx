<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<!-- ****************************************************************** -->
<!-- L'écran pour sauevegarder la base locale depuis la base de syncho  -->
<!-- ****************************************************************** -->

<fr.ird.observe.application.swing.ui.admin.AdminTabUI>

  <import>
    fr.ird.observe.application.swing.ui.admin.AdminStep
    fr.ird.observe.application.swing.ui.admin.AdminUI

    jaxx.runtime.swing.CardLayout2Ext

    java.awt.Color
    java.io.File
  </import>

  <SaveLocalUIHandler id='handler' constructorParams='this'/>

  <SaveLocalModel id='stepModel' initializer='getModel().getSaveLocalModel()'/>

  <CardLayout2Ext id='pendingLayout'
                  constructorParams='this, "PENDING_content_panel"'/>

  <script><![CDATA[
public SaveLocalUI(AdminUI parentContext) {
    super(AdminStep.SAVE_LOCAL, parentContext);
}

@Override
public void initUI(AdminUI ui) {
    getHandler().initTabUI(ui, this);
}
]]>
  </script>

  <JPanel id='PENDING_content'>
    <Table constraints='BorderLayout.CENTER' fill='both'>
      <row>
        <cell fill='both'>
          <JPanel background='{Color.WHITE}'>
            <JLabel id="PENDING_content_label"/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell fill='both' weighty='1' weightx='1'>

          <JPanel id='PENDING_content_panel'>
            <JButton id="continueAction" constraints='"no"'
                     onActionPerformed='getHandler().skipOperation()'/>

            <JPanel layout='{new BorderLayout()}' constraints='"do"'>

              <JLabel id='needSaveText' constraints='BorderLayout.CENTER'/>

              <Table fill='both' constraints='BorderLayout.SOUTH'>
                <row>
                  <cell columns='3'>
                    <JCheckBox id='doBackup'
                               onStateChanged='getStepModel().setDoBackup(((JCheckBox)event.getSource()).isSelected())'/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id="directoryLabel"/>
                  </cell>
                  <cell weightx='1' fill="horizontal">
                    <JTextField id='directoryText'
                                onKeyReleased='getHandler().changeDirectory(new File(((JTextField)event.getSource()).getText()))'/>
                  </cell>
                  <cell anchor="east">
                    <JButton id="backupFileChooseAction"
                             onActionPerformed="getHandler().chooseBackupFile()"/>
                  </cell>
                </row>
                <row>
                  <cell>
                    <JLabel id="fileLabel"/>
                  </cell>
                  <cell weightx='1' fill="horizontal" columns="2">
                    <JTextField id='filenameText'
                                onKeyReleased='getHandler().changeFilename(((JTextField)event.getSource()).getText())'/>
                  </cell>
                </row>
                <row>
                  <cell fill='both' columns="3">
                    <JButton id="startAction"
                             onActionPerformed="getHandler().doStartAction()"/>
                  </cell>
                </row>
              </Table>
            </JPanel>
          </JPanel>

        </cell>
      </row>
    </Table>
  </JPanel>

</fr.ird.observe.application.swing.ui.admin.AdminTabUI>
