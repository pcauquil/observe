/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.actions.shared.SelectNodeUIAction;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.tree.AbstractObserveTreeCellRenderer;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import jaxx.runtime.swing.editor.bean.BeanListHeader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public abstract class ContentListUIHandler<E extends IdDto, C extends DataDto> extends ContentUIHandler<E> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentListUIHandler.class);

    public ContentListUIHandler(ContentListUI<E, C> ui,
                                DataContextType parentType,
                                DataContextType type) {
        super(ui, parentType, type);
    }

    /**
     * Obtain the list of entities to display on ui from his container
     * {@code bean}.
     *
     * @param parentId the parent id of entities to display
     * @return the list of entities to display
     */
    protected abstract List<DataReference<C>> getChilds(String parentId);

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    public ContentListUI<E, C> getUi() {
        return (ContentListUI<E, C>) super.getUi();
    }

    @Override
    public final ContentListUIModel<E, C> getModel() {
        return (ContentListUIModel<E, C>) super.getModel();
    }

    @Override
    public void initUI() {
        super.initUI();

        // on installe un renderer sur la liste pour afficher les couleurs +
        // icones comme dans l'arbre
        ListCellRenderer renderer = getUi().getList().getCellRenderer();
        ObserveTreeHelper treeHelper = getTreeHelper(getUi());

        ListCellRenderer renderer2 = new EntityListCellRenderer(renderer, treeHelper);

        getUi().getList().setCellRenderer(renderer2);
    }

    @Override
    public final void openUI() {

        super.openUI();

        // init renderer
        EntityListCellRenderer renderer = (EntityListCellRenderer)
                getUi().getList().getCellRenderer();
        renderer.init();

        ContentListUIModel<E, C> model = getModel();

        String selectedId = getSelectedParentId();

        if (log.isInfoEnabled()) {
            log.info(prefix + "selectedId   = " + selectedId);
        }

        ContentMode mode = computeContentMode();

        if (log.isInfoEnabled()) {
            log.info(prefix + "content mode = " + mode);
        }

        // We want to force the bindings on "mode" to be triggered each time we open a list ui.
        //
        // The ui mode can already be set to the mode we're currently applying now.
        // As the state of the buttons is bound to that mode,
        // we need to force a mode change to ensure the buttons state will be refreshed.
        //
        // For example :
        // The button of the activities list of a route "Go to the open activity of another road" may need to disapear
        // because that road has just been deleted..
        //
        // As the ui state of the buttons is usually only bound to the mode of the model
        // and as the state of those buttons usually not only depends on that mode  (but also on attributes of the data context for instance, such as a route/activity/trip is open or not),
        // we need to force the buttons to recompute their state by forcing the mode's listeners to be triggered.
        model.setMode(null);
        model.setMode(mode);

        boolean canReopen = mode == ContentMode.CREATE;
        if (log.isInfoEnabled()) {
            log.info(prefix + "canReopen    = " + canReopen);
        }
        model.setCanReopen(canReopen);

        // il n'est pas nécessaire de charger le bean car seuls ses enfants nous sont utile dans cette ecran

        List<DataReference<C>> data = getChilds(getSelectedParentId());

        model.setData(data);

        if (!model.isEmpty()) {

            // select first data
            getUi().getListSelectionModel().setSelectionInterval(0, 0);

        }

        // finalize openUI with specified code
        finalizeOpenUI();
    }

    @Override
    protected void updateToolbarActions() {
        super.updateToolbarActions();
        // on n'a pas les données nécessaires pour afficher les informations techniques
        getUi().getShowTechnicalInformations().setEnabled(false);
    }

    void addChild() {
        addChild(getModel().getChildType());
    }

    /**
     * When a data has been selected in the list.
     *
     * @param event the mouse event fired
     */
    void onDataSelected(MouseEvent event) {
        DataReference<C> selectedData = getUi().getSelectedData();
        if (event.getClickCount() > 1) {
            gotoChild(selectedData);
            return;
        }
        ObserveNode node = null;
        if (selectedData != null) {

            // obtain the node corresponding to the selected data
            String id = selectedData.getId();
            ObserveTreeHelper helper = getTreeHelper(getUi());
            ObserveNode selectedNode = helper.getSelectedNode();
            node = helper.findNode(selectedNode, id);
        }

        // attach the node to action
        JButton button = getUi().getGotoSelectedChild();
        button.putClientProperty(SelectNodeUIAction.NODE, node);
    }

    <EE> List<EE> updateList(BeanListHeader<EE> list, List<EE> data) {
        String message = t(getUi().getEmptyListMessage());
        return updateList(list, data, message);
    }

    /**
     * Pour effectuer un traitement supplémantaire à la fin de la méthode
     * {@link #openUI()}.
     */
    protected void finalizeOpenUI() {
        // rien par default
    }

    /**
     * Un renderer de liste d'entité qui reprendre la cosmétique de l'arbre
     * de navigation.
     *
     * @since 1.5
     */
    private static class EntityListCellRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = 1L;

        private final transient ListCellRenderer delegate;

        private final transient ObserveTreeHelper treeHelper;

        private final AbstractObserveTreeCellRenderer treeRenderer;

        private ThreadLocal<ObserveNode> containerNode = new ThreadLocal<>();

        EntityListCellRenderer(ListCellRenderer delegate,
                               ObserveTreeHelper treeHelper) {
            this.delegate = delegate;
            this.treeHelper = treeHelper;
            this.treeRenderer = treeHelper.getTreeCellRenderer();
        }

        public void init() {
            containerNode.set(treeHelper.getSelectedNode());
        }

        @Override
        public Component getListCellRendererComponent(JList list,
                                                      Object value,
                                                      int index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus) {

            // obtain the text from the delegate renderer
            JLabel comp = (JLabel)
                    delegate.getListCellRendererComponent(list,
                                                          value,
                                                          index,
                                                          isSelected,
                                                          cellHasFocus
                    );

            if (value == null ||
                    !(value instanceof AbstractReference) ||
                    containerNode.get() == null) {

                // rien de plus a faire
                return comp;
            }

            // recuperation du noeud correspondant dans l'arbre
            ObserveNode node =
                    treeHelper.findNode(containerNode.get(), ((AbstractReference) value).getId());

            if (node == null) {

                // noeud non trouve (cela ne devrait jamais arrive
                return comp;
            }

            Icon icon = treeRenderer.getNavigationIcon(node);
            comp.setIcon(icon);
            Color color = treeRenderer.getNavigationTextColor(node);
            comp.setForeground(color);
            return comp;
        }
    }

}
