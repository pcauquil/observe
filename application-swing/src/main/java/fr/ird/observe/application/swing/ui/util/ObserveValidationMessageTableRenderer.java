package fr.ird.observe.application.swing.ui.util;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.admin.validate.ValidationMessageTableModel;
import fr.ird.observe.services.service.actions.validate.ValidationMessage;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Color;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class ObserveValidationMessageTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        JLabel rendererComponent = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        ImageIcon icon = null;
        String text = null;
        String toolTipText = null;

        column = table.convertColumnIndexToModel(column);
        if (table.getRowSorter() != null) {
            row = table.getRowSorter().convertRowIndexToModel(row);
        }

        ValidationMessageTableModel model = (ValidationMessageTableModel) table.getModel();

        switch (column) {
            case 0:
                // scope
                NuitonValidatorScope scope = (NuitonValidatorScope) value;
                icon = SwingValidatorUtil.getIcon(scope);
                String label = t(scope.getLabel());
                toolTipText = t("observe.validator.scope.tip", label);
                break;

            case 1:
                // field name
                text = getFieldName((String) value);
                toolTipText = t("observe.validator.field.tip", text);
                break;

            case 2:
                // message
                text = getMessage(model, row);
                toolTipText = t("observe.validator.message.tip", text);
                break;
        }

        rendererComponent.setText(text);
        rendererComponent.setToolTipText(toolTipText);
        rendererComponent.setIcon(icon);


        NuitonValidatorScope scope = (NuitonValidatorScope) (column == 0 ? value : model.getValueAt(row, 0));

        Color textColor = scope == NuitonValidatorScope.WARNING ? Color.RED : Color.BLACK;
        rendererComponent.setForeground(textColor);

        return rendererComponent;
    }

    public ImageIcon getIcon(NuitonValidatorScope scope) {
        return SwingValidatorUtil.getIcon(scope);
    }

    private String getMessage(ValidationMessageTableModel tableModel, int row) {
        ValidationMessage validationMessage = tableModel.getRow(row);
        return validationMessage.getMessage();
    }

    private String getFieldName(String value) {
        String fieldName = value.startsWith("observe.") ? value : ObserveI18nDecoratorHelper.getPropertyI18nKey(value);
        return t(fieldName);
    }

}
