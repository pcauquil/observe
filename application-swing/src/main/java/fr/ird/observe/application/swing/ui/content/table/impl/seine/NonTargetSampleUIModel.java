package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDtos;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class NonTargetSampleUIModel extends ContentTableUIModel<NonTargetSampleDto, NonTargetLengthDto> {

    private static final long serialVersionUID = 1L;

    public NonTargetSampleUIModel(NonTargetSampleUI ui) {

        super(NonTargetSampleDto.class,
              NonTargetLengthDto.class,
              new String[]{
                      NonTargetSampleDto.PROPERTY_NON_TARGET_LENGTH,
                      NonTargetSampleDto.PROPERTY_COMMENT},
              new String[]{NonTargetLengthDto.PROPERTY_SPECIES,
                           NonTargetLengthDto.PROPERTY_LENGTH,
                           NonTargetLengthDto.PROPERTY_LENGTH_SOURCE,
                           NonTargetLengthDto.PROPERTY_WEIGHT,
                           NonTargetLengthDto.PROPERTY_WEIGHT_SOURCE,
                           NonTargetLengthDto.PROPERTY_SEX,
                           NonTargetLengthDto.PROPERTY_COUNT,
                           NonTargetLengthDto.PROPERTY_ACQUISITION_MODE,
                           NonTargetLengthDto.PROPERTY_PICTURES_REFERENCES
              });

        List<ContentTableMeta<NonTargetLengthDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_SPECIES, false),
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_LENGTH, false),
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_WEIGHT, false),
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_COUNT, false),
                new ContentTableMeta<NonTargetLengthDto>(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_SEX, false) {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public boolean isNullValue(ContentTableModel<?, ?> m, NonTargetLengthDto bean, int row) {
                        return bean.getSex() != null;
                    }
                },
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_PICTURES_REFERENCES, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<NonTargetSampleDto, NonTargetLengthDto> createTableModel(ObserveContentTableUI<NonTargetSampleDto, NonTargetLengthDto> ui, List<ContentTableMeta<NonTargetLengthDto>> contentTableMetas) {
        return new ContentTableModel<NonTargetSampleDto, NonTargetLengthDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<NonTargetLengthDto> getChilds(NonTargetSampleDto bean) {
                return bean.getNonTargetLength();
            }

            @Override
            protected void load(NonTargetLengthDto source, NonTargetLengthDto target) {
                NonTargetLengthDtos.copyNonTargetLengthDto(source, target);
            }

            @Override
            protected void setChilds(NonTargetSampleDto parent, List<NonTargetLengthDto> childs) {
                parent.setNonTargetLength(Sets.newLinkedHashSet(childs));
            }
        };
    }
}
