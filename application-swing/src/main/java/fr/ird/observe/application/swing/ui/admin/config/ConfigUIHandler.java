/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.config;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUI;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import fr.ird.observe.application.swing.ui.storage.StorageUIHandler;
import fr.ird.observe.application.swing.ui.storage.StorageUILauncher;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTree;
import java.awt.Window;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ConfigUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConfigUIHandler.class);

    public ConfigUIHandler(AdminTabUI ui) {
        super(ui);
    }

    public void initTabUI(AdminUI ui, ConfigUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug("specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        UIHelper.setLayerUI(tabUI.getOperations(), ui.getOperationBlockLayerUI());
        UIHelper.setLayerUI(tabUI.getConfig(), ui.getConfigBlockLayerUI());
        UIHelper.setLayerUI(tabUI.getContent(), null);
    }


    public void initTabUI(AdminUI ui, SelectDataUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        UIHelper.setLayerUI(tabUI.getContent(), null);
        UIHelper.setLayerUI(tabUI.getSelectTreePane(), ui.getConfigBlockLayerUI());

        JTree tree = tabUI.getSelectTree();
        tabUI.getTreeHelper().setUI(tree, false, null);

        // customize tree selection colors
        UIHelper.initUI(tabUI.getSelectTreePane(), tree);
    }

    public void updateOperationState(JCheckBox checkBox) {
        AdminStep op = getOperation(checkBox);
        if (log.isDebugEnabled()) {
            log.debug("Operation " + op + " selection has changed : selected:" + checkBox.isSelected());
        }
        if (checkBox.isSelected()) {
            // ajout de l'operation
            getModel().addOperation(op);
        } else {
            // supprime cette opération
            getModel().removeOperation(op);
        }
    }

    public String updateStorageLabel(StorageUIModel service,
                                     boolean serviceValid,
                                     JLabel label) {
        String text;
        if (serviceValid) {

            // on recupere le label du service
            text = service.getLabel();
        } else {

            // aucun service configuré
            text = t((String) label.getClientProperty("no"));
        }
        return text;
    }

    public AdminStep getOperation(JCheckBox checkBox) {
        return AdminStep.valueOf(checkBox.getName());
    }

    public boolean isOperationSelected(Set<AdminStep> operations,
                                       JCheckBox checkBox) {
        AdminStep scope = getOperation(checkBox);
        return operations.contains(scope);
    }

    public StorageUIHandler getStorageHandler() {
        return ui.getContextValue(StorageUIHandler.class);
    }

    public void obtainIncomingConnexion() {
        StorageUIModel sourceModel = ui.getLocalSourceModel();
        StorageUILauncher.obtainConnexion(ui, ui.getParentContainer(Window.class), sourceModel);
        if (log.isDebugEnabled()) {
            log.debug("After modifiy source model isValid : " + sourceModel.isValid() + " / " + sourceModel.isValidStep());
        }
        model.removeLocalSource();
        model.getLocalSourceModel().validate();
        model.validate();
    }

    public void obtainRemoteConnexion() {
        if (log.isInfoEnabled()) {
            log.info("start obtain remote connexion");
        }
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainConnexion(ui, ui.getParentContainer(Window.class), sourceModel);
        model.removeCentralSource();
        model.getCentralSourceModel().validate(StorageStep.CONFIG);
        model.validate();
    }

    protected String updateDataSourcePolicy(StorageUIModel sourceModel, boolean valid) {
        String text = null;
        if (valid) {
            ObserveDataSourceInformation dataSourceInformation = sourceModel.getDataSourceInformation();

            if (dataSourceInformation != null) {

                ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
                ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
                text = textGenerator.getDataSourcePolicy(dataSourceInformation);
            }

        } else {

            text = t("observe.common.storage.not.valid");

        }

        return text;
    }

    protected boolean canShowLocalStorage(Set<AdminStep> operations,
                                          boolean needLocalStorage) {
        return !operations.isEmpty() && needLocalStorage;
    }

}
