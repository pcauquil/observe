package fr.ird.observe.application.swing.ui.admin.synchronize.data;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DataSynchronizeTaskSupport;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.DefaultListModel;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class DataSynchroModel extends AdminActionModel {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataSynchroModel.class);

    public static final String LEFT_SOURCE_PROPERTY_NAME = "leftSource";
    public static final String RIGHT_SOURCE_PROPERTY_NAME = "rightSource";
    public static final String TASKS_PROPERTY_NAME = "tasks";

    public static final String LEFT_SELECTION_MODEL_CHANGED_PROPERTY_NAME = "leftSelectionModelChanged";
    public static final String RIGHT_SELECTION_MODEL_CHANGED_PROPERTY_NAME = "rightSelectionModelChanged";

    /** la source sur le panneau de gauche. */
    protected ObserveSwingDataSource leftSource;

    /** la source sur le panneau de droite. */
    protected ObserveSwingDataSource rightSource;

    /** les données sélectionnées sur le panneau de gauche. */
    protected final DataSelectionModel leftSelectionDataModel;

    /** les données sélectionnées sur le panneau de droite. */
    protected final DataSelectionModel rightSelectionDataModel;

    protected final DefaultListModel<DataSynchronizeTaskSupport> tasks;

    public DataSynchroModel() {
        super(AdminStep.DATA_SYNCHRONIZE);

        leftSelectionDataModel = new DataSelectionModel();
        leftSelectionDataModel.setUseData(true);

        rightSelectionDataModel = new DataSelectionModel();
        rightSelectionDataModel.setUseData(true);
        tasks = new DefaultListModel<>();

    }

    public ObserveSwingDataSource getLeftSource() {
        return leftSource;
    }

    public void setLeftSource(ObserveSwingDataSource leftSource) {
        this.leftSource = leftSource;
        firePropertyChange(LEFT_SOURCE_PROPERTY_NAME, leftSource);
    }

    public ObserveSwingDataSource getRightSource() {
        return rightSource;
    }

    public void setRightSource(ObserveSwingDataSource rightSource) {
        this.rightSource = rightSource;
        firePropertyChange(RIGHT_SOURCE_PROPERTY_NAME, rightSource);
    }

    public DataSelectionModel getLeftSelectionDataModel() {
        return leftSelectionDataModel;
    }

    public DataSelectionModel getRightSelectionDataModel() {
        return rightSelectionDataModel;
    }

    public void populateLeftSelectionModel() {
        populateSelectionModel(leftSource, leftSelectionDataModel, LEFT_SELECTION_MODEL_CHANGED_PROPERTY_NAME);
    }

    public void populateRightSelectionModel() {
        populateSelectionModel(rightSource, rightSelectionDataModel, RIGHT_SELECTION_MODEL_CHANGED_PROPERTY_NAME);
    }

    public DefaultListModel<DataSynchronizeTaskSupport> getTasks() {
        return tasks;
    }

    public void addTask(DataSynchronizeTaskSupport task) {
        tasks.addElement(task);
    }

    private void populateSelectionModel(ObserveSwingDataSource dataSource, DataSelectionModel selectionDataModel, String propertyName) {

        try {

            DataSelectionModel.populate(selectionDataModel, dataSource);

        } catch (Exception e) {

            if (log.isErrorEnabled()) {
                log.error("could not populate selected model", e);
            }

        } finally {

            // on notifie que le modèle de sélection a changé
            // (il faut donc recalculé l'arbre de sélection)
            firePropertyChange(propertyName, selectionDataModel);

        }
    }
}
