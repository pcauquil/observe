package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.seine.SetSeineTargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import fr.ird.observe.services.dto.seine.TargetCatchDtos;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class TargetDiscardCatchUIModel extends ContentTableUIModel<SetSeineTargetCatchDto, TargetCatchDto> {

    private static final long serialVersionUID = 1L;

    public TargetDiscardCatchUIModel(TargetDiscardCatchUI ui) {

        super(SetSeineTargetCatchDto.class,
              TargetCatchDto.class,
              new String[]{
                      SetSeineTargetCatchDto.PROPERTY_DISCARDED,
                      SetSeineTargetCatchDto.PROPERTY_TARGET_CATCH},
              new String[]{
                           TargetCatchDto.PROPERTY_WEIGHT_CATEGORY,
                           TargetCatchDto.PROPERTY_CATCH_WEIGHT,
                           TargetCatchDto.PROPERTY_REASON_FOR_DISCARD,
                           TargetCatchDto.PROPERTY_DISCARDED,
                           TargetCatchDto.PROPERTY_BROUGHT_ON_DECK,
                           TargetCatchDto.PROPERTY_COMMENT});

        List<ContentTableMeta<TargetCatchDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(TargetCatchDto.class, WeightCategoryDto.PROPERTY_SPECIES, true),
                ContentTableModel.newTableMeta(TargetCatchDto.class, TargetCatchDto.PROPERTY_WEIGHT_CATEGORY, true),
                ContentTableModel.newTableMeta(TargetCatchDto.class, TargetCatchDto.PROPERTY_REASON_FOR_DISCARD, true),
                ContentTableModel.newTableMeta(TargetCatchDto.class, TargetCatchDto.PROPERTY_CATCH_WEIGHT, false),
                ContentTableModel.newTableMeta(TargetCatchDto.class, TargetCatchDto.PROPERTY_BROUGHT_ON_DECK, false),
                ContentTableModel.newTableMeta(TargetCatchDto.class, TargetCatchDto.PROPERTY_COMMENT, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto> createTableModel(
            ObserveContentTableUI<SetSeineTargetCatchDto, TargetCatchDto> ui,
            List<ContentTableMeta<TargetCatchDto>> contentTableMetas) {

        return new ContentTableModel<SetSeineTargetCatchDto, TargetCatchDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<TargetCatchDto> getChilds(SetSeineTargetCatchDto bean) {
                return bean.getTargetCatch();
            }

            @Override
            protected void load(TargetCatchDto source, TargetCatchDto target) {
                TargetCatchDtos.copyTargetCatchDto(source, target);
            }

            @Override
            protected void setChilds(SetSeineTargetCatchDto parent, List<TargetCatchDto> childs) {
                bean.setTargetCatch(childs);
            }
        };
    }
}
