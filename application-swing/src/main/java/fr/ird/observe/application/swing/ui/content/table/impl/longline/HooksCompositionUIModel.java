package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.HooksCompositionDto;
import fr.ird.observe.services.dto.longline.HooksCompositionDtos;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 12/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class HooksCompositionUIModel extends ContentTableUIModel<SetLonglineGlobalCompositionDto, HooksCompositionDto> {

    private static final long serialVersionUID = 1L;

    public HooksCompositionUIModel(HooksCompositionUI ui) {
        super(SetLonglineGlobalCompositionDto.class,
              HooksCompositionDto.class,
              new String[]{
                      SetLonglineGlobalCompositionDto.PROPERTY_HOOKS_COMPOSITION
              },
              new String[]{HooksCompositionDto.PROPERTY_HOOK_TYPE,
                           HooksCompositionDto.PROPERTY_HOOK_SIZE,
                           HooksCompositionDto.PROPERTY_HOOK_OFFSET,
                           HooksCompositionDto.PROPERTY_PROPORTION});

        List<ContentTableMeta<HooksCompositionDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(HooksCompositionDto.class, HooksCompositionDto.PROPERTY_HOOK_TYPE, false),
                ContentTableModel.newTableMeta(HooksCompositionDto.class, HooksCompositionDto.PROPERTY_HOOK_SIZE, false),
                ContentTableModel.newTableMeta(HooksCompositionDto.class, HooksCompositionDto.PROPERTY_HOOK_OFFSET, false),
                ContentTableModel.newTableMeta(HooksCompositionDto.class, HooksCompositionDto.PROPERTY_PROPORTION, false));

        initModel(ui, metas);

    }


    @Override
    protected ContentTableModel<SetLonglineGlobalCompositionDto, HooksCompositionDto> createTableModel(
            ObserveContentTableUI<SetLonglineGlobalCompositionDto, HooksCompositionDto> ui,
            List<ContentTableMeta<HooksCompositionDto>> contentTableMetas) {

        return new ContentTableModel<SetLonglineGlobalCompositionDto, HooksCompositionDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<HooksCompositionDto> getChilds(SetLonglineGlobalCompositionDto bean) {
                return bean.getHooksComposition();
            }

            @Override
            protected void load(HooksCompositionDto source, HooksCompositionDto target) {
                HooksCompositionDtos.copyHooksCompositionDto(source, target);
            }

            @Override
            protected void setChilds(SetLonglineGlobalCompositionDto parent, List<HooksCompositionDto> childs) {
                parent.setHooksComposition(childs);
            }
        };
    }
}
