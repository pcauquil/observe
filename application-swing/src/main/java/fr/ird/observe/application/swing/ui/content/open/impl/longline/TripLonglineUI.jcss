/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

NumberEditor {
  bean:{bean};
  useFloat:false;
  numberPattern:{fr.ird.observe.application.swing.ui.UIHelper.INT_6_DIGITS_PATTERN};
}

#generalTab {
  title: {t("observe.content.tripLongline.tab.general")};
}

#mapTab {
  title: {t("observe.content.tripLongline.tab.map")};
}

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
}

#observerLabel {
  text:"observe.content.tripLongline.observer";
  labelFor:{observer};
}

#observer {
  property:{TripLonglineDto.PROPERTY_OBSERVER};
  selectedItem:{bean.getObserver()};
}

#captainLabel {
  text:"observe.content.tripLongline.captain";
  labelFor:{captain};
}

#captain {
  property:{TripLonglineDto.PROPERTY_CAPTAIN};
  selectedItem:{bean.getCaptain()};
}

#dataEntryOperatorLabel {
  text:"observe.content.tripLongline.dataEntryOperator";
  labelFor:{dataEntryOperator};
}

#dataEntryOperator {
  property:{TripLonglineDto.PROPERTY_DATA_ENTRY_OPERATOR};
  selectedItem:{bean.getDataEntryOperator()};
}

#tripTypeLabel{
  text:"observe.content.tripLongline.tripType";
  labelFor:{tripType};
}

#tripType {
  property:{TripLonglineDto.PROPERTY_TRIP_TYPE};
  selectedItem:{bean.getTripType()};
}

#vesselLabel{
  text:"observe.content.tripLongline.vessel";
  labelFor:{vessel};
}

#vessel {
  property:{TripLonglineDto.PROPERTY_VESSEL};
  selectedItem:{bean.getVessel()};
}

#oceanLabel{
  text:"observe.content.tripLongline.ocean";
  labelFor:{ocean};
}

#ocean {
  property:{TripLonglineDto.PROPERTY_OCEAN};
  selectedItem:{bean.getOcean()};
  enabled:{canEditOcean(bean.getActivityLongline())};
}

#departureHarbourLabel {
  text:"observe.common.departureHarbour";
  labelFor:{departureHarbour};
}

#departureHarbour {
  property:{TripLonglineDto.PROPERTY_DEPARTURE_HARBOUR};
  selectedItem:{bean.getDepartureHarbour()};
}

#landingHarbourLabel {
  text:"observe.common.landingHarbour";
  labelFor:{landingHarbour};
}

#landingHarbour {
  property:{TripLonglineDto.PROPERTY_LANDING_HARBOUR};
  selectedItem:{bean.getLandingHarbour()};
}

#totalFishingOperationsNumberLabel{
  text:"observe.content.tripLongline.totalFishingOperationsNumber";
  labelFor:{totalFishingOperationsNumber};
}

#totalFishingOperationsNumber {
  property:{TripLonglineDto.PROPERTY_TOTAL_FISHING_OPERATIONS_NUMBER};
  model:{bean.getTotalFishingOperationsNumber()};
  useFloat:false;
}

#homeIdLabel {
  text:"observe.content.tripLongline.homeId";
  labelFor:{homeId};
}

#resetHomeId {
  toolTipText:"observe.content.action.reset.homeId.tip";
  _resetPropertyName: {TripLonglineDto.PROPERTY_HOME_ID};
}

#homeId {
  text:{getStringValue(bean.getHomeId())};
  _propertyName: {TripLonglineDto.PROPERTY_HOME_ID};
}

#startDateLabel {
  text:"observe.content.tripLongline.startDate";
  labelFor:{startDate};
}

#startDate {
  date:{bean.getStartDate()};
  _propertyName: {TripLonglineDto.PROPERTY_START_DATE};
}

#endDateLabel {
  text:"observe.content.tripLongline.endDate";
  labelFor:{endDate};
}

#endDate {
  date:{bean.getEndDate()};
  _propertyName: {TripLonglineDto.PROPERTY_END_DATE};
}

#comment {
  columnHeaderView:{new JLabel(t("observe.common.comment"))};
  minimumSize:{new Dimension(10,50)};
}

#comment2 {
  text:{getStringValue(bean.getComment())};
}

#reopen {
  _toolTipText:{t("observe.content.action.reopen.maree.tip")};
}

#close {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid()) && !dataContext.isOpenActivity()};
  _toolTipText:{t("observe.action.close.maree.tip")};
}

#closeAndCreate {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid()) && !dataContext.isOpenActivity()};
  _text:{t("observe.content.action.closeAndCreate.maree")};
  _toolTipText:{t("observe.content.action.closeAndCreate.maree.tip")};
}

#delete {
  _toolTipText:{t("observe.action.delete.maree.tip")};
}
