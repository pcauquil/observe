package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.ReferentialSynchronizeTaskSupport;

import javax.swing.DefaultListModel;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created on 13/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ReferentialSynchronizeTaskListModel extends DefaultListModel<ReferentialSynchronizeTaskSupport> implements Iterable<ReferentialSynchronizeTaskSupport> {

    private static final long serialVersionUID = 1L;

    private boolean tasksIsAdjusting;

    public void addTasks(Collection<ReferentialSynchronizeTaskSupport> addedTasks) {

        if (addedTasks.isEmpty()) {
            return;
        }
        
        tasksIsAdjusting = true;
        try {
            int minIndex = size();
            int maxIndex = minIndex + addedTasks.size() - 1;
            ensureCapacity(maxIndex);

            for (ReferentialSynchronizeTaskSupport addedTask : addedTasks) {
                addElement(addedTask);
            }

            super.fireIntervalAdded(this, minIndex, maxIndex);
        } finally {
            tasksIsAdjusting = false;
        }
    }

    @Override
    protected void fireIntervalAdded(Object source, int index0, int index1) {
        if (tasksIsAdjusting) {
            return;
        }
        super.fireIntervalAdded(source, index0, index1);
    }

    @Override
    public Iterator<ReferentialSynchronizeTaskSupport> iterator() {
        return Collections.list(elements()).iterator();
    }
}
