/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import org.nuiton.decorator.Decorator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

public class ObsoleteReferentialReferenceListCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 1L;

    protected transient DecoratorService decoratorService;

    public DecoratorService getDecoratorService() {
        if (decoratorService == null) {
            decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        }
        return decoratorService;
    }

    @Override
    public Component getListCellRendererComponent(
            JList list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        ObsoleteReferentialReference referentialReference = (ObsoleteReferentialReference) value;

        DecoratorService service = getDecoratorService();
        Class referentialReferenceType = referentialReference.getReferentialReference().getType();
        String type = t(ObserveI18nDecoratorHelper.getTypeI18nKey(referentialReferenceType));
        Decorator<?> decorator = service.getReferentialReferenceDecorator(referentialReferenceType);

        String text = type + " : " + decorator.toString(referentialReference.getReferentialReference());

        return super.getListCellRendererComponent(
                list,
                text,
                index,
                isSelected,
                cellHasFocus
        );
    }
}
