package fr.ird.observe.application.swing.ui.util.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created on 12/12/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public abstract class AutotSelectRowAndShowPopupActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AutotSelectRowAndShowPopupActionSupport.class);

    private final JScrollPane pane;

    private final JTable table;

    private final JPopupMenu popup;

    protected AutotSelectRowAndShowPopupActionSupport(JScrollPane pane, final JTable table, JPopupMenu popup) {
        this.pane = pane;
        this.table = table;
        this.popup = popup;

        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (table.isEnabled()) {
                    openRowMenu(e);
                }
            }
        };
        this.table.addKeyListener(keyAdapter);
        pane.addKeyListener(keyAdapter);

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (AutotSelectRowAndShowPopupActionSupport.this.table.isEnabled()) {

                    autoSelectRowInTable(e);

                }
            }
        };
        this.table.addMouseListener(mouseAdapter);
        pane.addMouseListener(mouseAdapter);
    }

    public JPopupMenu getPopup() {
        return popup;
    }

    public JTable getTable() {
        return table;
    }

    protected abstract void beforeOpenPopup(int modelRowIndex, int modelColumnIndex);

    public void autoSelectRowInTable(MouseEvent e) {

        boolean rightClick = SwingUtilities.isRightMouseButton(e);

        if (rightClick || SwingUtilities.isLeftMouseButton(e)) {

            // get the coordinates of the mouse click
            Point p = e.getPoint();

            int[] selectedRows = table.getSelectedRows();
            int[] selectedColumns = table.getSelectedColumns();

            // get the row index at this point
            int rowIndex = table.rowAtPoint(p);

            // get the column index at this point
            int columnIndex = table.columnAtPoint(p);

            if (log.isDebugEnabled()) {
                log.debug("At point [" + p + "] found Row " + rowIndex + ", Column " + columnIndex);
            }

            boolean canContinue = true;

            if (table.isEditing()) {

                // stop editing
                boolean stopEdit = table.getCellEditor().stopCellEditing();
                if (!stopEdit) {
                    if (log.isWarnEnabled()) {
                        log.warn("Could not stop edit cell...");
                    }
                    canContinue = false;
                }
            }

            if (canContinue) {

                // select row (could empty selection)
                if (rowIndex == -1) {
                    table.clearSelection();
                } else if (!ArrayUtils.contains(selectedRows, rowIndex)) {
                    // set selection
                    table.setRowSelectionInterval(rowIndex, rowIndex);
                }

                // select column (could empty selection)
                if (columnIndex == -1) {
                    table.clearSelection();
                } else if (!ArrayUtils.contains(selectedColumns, columnIndex)) {
                    table.setColumnSelectionInterval(columnIndex, columnIndex);
                }

                if (rightClick) {

                    showPopup(rowIndex, columnIndex, p);

//                    // use now model coordinate
//                    int modelRowIndex = rowIndex == -1 ? -1 : table.convertRowIndexToModel(rowIndex);
//                    int modelColumnIndex = columnIndex == -1 ? -1 : table.convertColumnIndexToModel(columnIndex);
//
//                    beforeOpenPopup(modelRowIndex, modelColumnIndex);
//
//                    // on right click show popup
//                    popup.show(table, e.getX(), e.getY());
                }
            }
        }
    }

    public void openRowMenu(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_CONTEXT_MENU) {

            // get the lowest selected row
            int[] selectedRows = table.getSelectedRows();
            int lowestRow = -1;
            for (int row : selectedRows) {
                lowestRow = Math.max(lowestRow, row);
            }
            // get the selected column
            int selectedColumn = table.getSelectedColumn();
            Rectangle r = table.getCellRect(lowestRow, selectedColumn, true);

            // get the point in the middle lower of the cell
            Point p = new Point(r.x + r.width / 2, r.y + r.height);

            if (log.isDebugEnabled()) {
                log.debug("Row " + lowestRow + " found t point [" + p + "]");
            }

            boolean canContinue = true;

            if (table.isEditing()) {

                // stop editing
                boolean stopEdit = table.getCellEditor().stopCellEditing();
                if (!stopEdit) {
                    if (log.isWarnEnabled()) {
                        log.warn("Could not stop edit cell...");
                    }
                    canContinue = false;
                }
            }

            if (canContinue) {

                showPopup(lowestRow, selectedColumn, p);

//                // use now model coordinate
//                int rowIndex = table.convertRowIndexToModel(lowestRow);
//                int columnIndex = table.convertColumnIndexToModel(selectedColumn);
//                beforeOpenPopup(rowIndex, columnIndex);
//
//                popup.show(table, p.x, p.y);
            }
        }
    }

    protected void showPopup(int row, int column, Point p) {

        // use now model coordinate
        int rowIndex = row == -1 ? -1 : table.convertRowIndexToModel(row);
        int columnIndex = column == -1 ? -1 : table.convertColumnIndexToModel(column);
        beforeOpenPopup(rowIndex, columnIndex);

        popup.show(table, p.x, p.y);

    }

}
