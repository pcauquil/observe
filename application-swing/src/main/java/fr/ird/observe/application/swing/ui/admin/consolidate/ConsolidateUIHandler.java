/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.consolidate;

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUI;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.admin.config.SelectDataUI;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateActivitySeineDataResult;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataRequest;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataResult;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class ConsolidateUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConsolidateUIHandler.class);

    public ConsolidateUIHandler(AdminTabUI ui) {
        super(ui);
    }

    public ConsolidateModel getStepModel() {
        return model.getConsolidateModel();
    }

    public void initTabUI(AdminUI ui, ConsolidateUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() +
                              "] for main ui " + ui.getClass().getName() + "@" +
                              System.identityHashCode(ui));
        }

        tabUI.getStartButton().setText(
                t("observe.actions.synchro.launch.operation",
                  t(tabUI.getStep().getOperationLabel())));

        SelectDataUI selectTabUI = (SelectDataUI) ui.getStepUI(AdminStep.SELECT_DATA);

        PropertyChangeListener listener = evt -> {
            AdminUIModel model1 = (AdminUIModel) evt.getSource();
            if (!model1.containsStep(selectTabUI.getStep())) {
                // avoid multi-cast
                return;
            }
            DataSelectionModel value = (DataSelectionModel) evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug("selection model changed to " + value);
            }
            updateSelectionModel(selectTabUI);
        };
        tabUI.getModel().addPropertyChangeListener(AdminUIModel.SELECTION_MODEL_CHANGED_PROPERTY_NAME, listener);
    }

    public void startAction() {

        addAdminWorker(((ConsolidateUI) ui).getStartButton().getToolTipText(), this::doAction);
    }

    public WizardState doAction() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug(this);
        }

        getStepModel().setSource(model.getSafeLocalSource(false));

        Set<DataReference> trips = model.getSelectionDataModel().getSelectedData();
        ImmutableSet<String> tripIds = ImmutableSet.copyOf(trips.stream()
                                                                .filter(DataReference.newTripSeinePredicate())
                                                                .map(DataReference.ID_FUNCTION)
                                                                .collect(Collectors.toSet()));

        try (ObserveSwingDataSource dataSource = getStepModel().getSource()) {

            ConsolidateDataService consolidateDataService = dataSource.newConsolidateDataService();

            ConsolidateTripSeineDataRequest request = new ConsolidateTripSeineDataRequest();
            request.setTripSeineIds(tripIds);
            request.setFailIfLenghtWeightParameterNotFound(false);

            ImmutableSet<ConsolidateTripSeineDataResult> results = consolidateDataService.consolidateTripSeines(request);

            if (results.isEmpty()) {

                sendMessage(t("observe.actions.consolidate.message.noChanges"));

            } else {

                for (ConsolidateTripSeineDataResult tripSeineDataResult : results) {

                    String tripSeineLabel = tripSeineDataResult.getTripSeineLabel();

                    sendMessage(t("observe.actions.consolidate.message.trip", tripSeineLabel));

                    for (ConsolidateActivitySeineDataResult activitySeineDataResult : tripSeineDataResult.getConsolidateActivitySeineDataResults()) {

                        sendMessage(t("observe.actions.consolidate.message.activity", activitySeineDataResult.getActivitySeineLabel()));

                    }

                }

                sendMessage(t("observe.actions.consolidate.message.save.changes", results.size()));
            }

            sendMessage(t("observe.actions.consolidate.message.operation.done", new Date()));

        }

        return WizardState.SUCCESSED;

    }

}
