package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.DataFileDto;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDtos;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.referential.longline.SensorBrandDto;
import fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto;
import fr.ird.observe.services.dto.referential.longline.SensorTypeDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.longline.ActivityLongLineSensorUsedService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JaxxFileChooser;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 8/31/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class SensorUsedUIHandler extends ContentTableUIHandler<ActivityLonglineSensorUsedDto, SensorUsedDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(SensorUsedUIHandler.class);

    public SensorUsedUIHandler(SensorUsedUI ui) {
        super(ui, DataContextType.ActivityLongline);
    }

    @Override
    public SensorUsedUI getUi() {
        return (SensorUsedUI) super.getUi();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, SensorUsedDto bean, boolean create) {
        if (getTableModel().isEditable()) {
            if (log.isDebugEnabled()) {
                log.debug("Row has changed to " + editingRow);
            }
            getUi().getSensorType().requestFocus();
        }
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {
        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(table,
                                            n("observe.content.sensorUsed.table.sensorType"),
                                            n("observe.content.sensorUsed.table.sensorType.tip"),
                                            n("observe.content.sensorUsed.table.sensorBrand"),
                                            n("observe.content.sensorUsed.table.sensorBrand.tip"),
                                            n("observe.content.sensorUsed.table.sensorDataFormat"),
                                            n("observe.content.sensorUsed.table.sensorDataFormat.tip"),
                                            n("observe.content.sensorUsed.table.sensorSerialNo"),
                                            n("observe.content.sensorUsed.table.sensorSerialNo.tip"),
                                            n("observe.content.sensorUsed.table.data"),
                                            n("observe.content.sensorUsed.table.data.tip"),
                                            n("observe.content.sensorUsed.table.dataLocation"),
                                            n("observe.content.sensorUsed.table.dataLocation.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SensorTypeDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SensorBrandDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SensorDataFormatDto.class));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newStringTableCellRenderer(renderer, 10, true));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 5, UIHelper.newStringTableCellRenderer(renderer, 10, true));
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedActivityId();
    }

    public void importData() {

        File file = UIHelper.chooseFile((Component) ui,
                                        t("observe.content.choose.sensorUsed.title.importData"),
                                        t("observe.action.choose.sensorUsed.importData"),
                                        null);

        if (file != null) {

            if (log.isInfoEnabled()) {
                log.info("Set data from file: " + file);
            }

            DataFileDto dataFileDto = UIHelper.fileToDataFileDto(file);
            getTableEditBean().setData(dataFileDto);
            getTableEditBean().setHasData(true);

        }

    }

    public void deleteData() {

        int response = UIHelper.askUser((Component) ui,
                                        t("observe.title.delete"),
                                        t("observe.content.sensorUsed.delete.data.message"),
                                        JOptionPane.WARNING_MESSAGE,
                                        new Object[]{t("observe.choice.confirm.delete"),
                                                     t("observe.choice.cancel")},
                                        1);

        boolean doDelete = response == 0;

        if (doDelete) {

            if (log.isInfoEnabled()) {
                log.info("Delete sensorUsed data " + getTableEditBean().getData());
            }
            getTableEditBean().setData(null);
            getTableEditBean().setHasData(false);

        }

    }

    public void exportData() {

        DataFileDto dataFile = getTableEditBean().getData();

        if (dataFile == null) {
            dataFile = getActivityLonglineSensorUsedService().getDataFile(getTableEditBean().getId());
        }


        File file = JaxxFileChooser
                .forSaving()
                .setParent(getUi())
                .setTitle(t("observe.content.choose.sensorUsed.title.exportData"))
                .setApprovalText(t("observe.action.choose.sensorUsed.exportData"))
                .setFilename(dataFile.getName())
                .setUseAcceptAllFileFilter(true)
                .choose();

        if (file != null && UIHelper.confirmOverwriteFileIfExist(getUi(), file)) {

            if (log.isInfoEnabled()) {
                log.info("save sensorUsed data to " + file);
            }

            try {

                Files.write(file.toPath(), dataFile.getContent());

                ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
                mainUI.getStatus().setStatus(t("observe.content.sensorUsed.message.data.exported", file));
            } catch (IOException e) {
                throw new ObserveSwingTechnicalException("Could not save binary data to " + file, e);
            }

        }

    }

    @Override
    protected void doPersist(ActivityLonglineSensorUsedDto bean) {

        SaveResultDto saveResult= getActivityLonglineSensorUsedService().save(bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<ActivityLonglineSensorUsedDto> form = getActivityLonglineSensorUsedService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        ActivityLonglineSensorUsedDtos.copyActivityLonglineSensorUsedDto(form.getObject(), getBean());
    }

    protected ActivityLongLineSensorUsedService getActivityLonglineSensorUsedService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivityLongLineSensorUsedService();
    }

}
