/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.tree.loadors.ActivityLonglineNodeChildLoador;
import fr.ird.observe.application.swing.ui.tree.loadors.ActivityLonglinesNodeChildLoador;
import fr.ird.observe.application.swing.ui.tree.loadors.ActivitySeineNodeChildLoador;
import fr.ird.observe.application.swing.ui.tree.loadors.ActivitySeinesNodeChildLoador;
import fr.ird.observe.application.swing.ui.tree.loadors.ProgramLonglineNodeChildLoador;
import fr.ird.observe.application.swing.ui.tree.loadors.ProgramSeineNodeChildLoador;
import fr.ird.observe.application.swing.ui.tree.loadors.RootNodeChildLoador;
import fr.ird.observe.application.swing.ui.tree.loadors.RoutesNodeChildLoador;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.ReferenceBinderEngine;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.tree.NavTreeHelper;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * Class utilitaire pour la bestion de l'arbre de navigation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ObserveTreeHelper extends NavTreeHelper<ObserveNode> {

    /** Logger. */
    static private final Log log = LogFactory.getLog(ObserveTreeHelper.class);

    public static void sortPrograms(List<ReferentialReference<ProgramDto>> data) {

        ObserveSwingApplicationContext.get().getDecoratorService().sort(ProgramDto.class, data);

    }

    public ObserveTreeHelper() {
        super(new ObserveTreeBridge());
    }

    public NavigationTreeSelectionModel newNavigationSelectionModel() {
        NavigationTreeSelectionModel model = new NavigationTreeSelectionModel();
        model.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        return model;
    }

    public AbstractObserveTreeCellRenderer getTreeCellRenderer() {
        JTree t = getUI();
        if (t == null) {
            return null;
        }
        TreeCellRenderer r = t.getCellRenderer();
        return (AbstractObserveTreeCellRenderer)
                (r instanceof AbstractObserveTreeCellRenderer ? r : null);
    }

    public TreeModel createEmptyModel() {

        setDataSource(null);

        ObserveNode node =
                new ObserveNode(n("observe.message.db.none.loaded"), false);

        return createModel(node);
    }

    public TreeModel createModel(ObserveSwingDataSource source) {

        setDataSource(source);

        ObserveDataProvider provider = getDataProvider();
        provider.setCreating(true);

        try {

            RootNodeChildLoador loador = new RootNodeChildLoador(source.canReadData(), true);
            ObserveNode node = new ObserveNode(String.class, "Root node", null, loador, false);

            DefaultTreeModel model = createModel(node);
            node.populateChilds(getBridge(), provider);
            return model;

        } finally {

            provider.setCreating(false);

        }
    }

    public DefaultTreeModel createModel(JAXXContext context,
                                        DataSelectionModel dataModel,
                                        ObserveSwingDataSource source) {

        setDataSource(source);
        context.setContextValue(dataModel);

        if (log.isDebugEnabled()) {
            log.debug("create tree model " + this);
        }

        ObserveDataProvider provider = getDataProvider();
        provider.setSelectionModel(dataModel);
        provider.setCreating(true);

        try {

            RootNodeChildLoador loador = getChildLoador(RootNodeChildLoador.class);
            loador.setAddData(dataModel.isUseData());
            loador.setAddReferentiel(dataModel.isUseReferentiel());
            ObserveNode node = new ObserveNode(String.class, "Root node", loador, false);

            DefaultTreeModel model = createModel(node);
            loadAllNodes(node, provider);
            return model;

        } finally {

            provider.setCreating(false);

        }

    }

    /**
     * Charge dans l'ui un nouveau modèle de navigation.
     *
     * <b>Note:</b> cette méthode doit être appelée après tout rechargement de
     * modèle de naivgation.
     *
     * @param source la source de données
     */
    public void loadNavigationUI(ObserveSwingDataSource source) {

        ObserveNode.count = 0;

        // propagate ui in observe bridge to control which nodes can be loads
        // from ui state
        getBridge().setUi(getUI());

        // build navigation model
        createModel(source);

        // select initial node
        selectInitialNode();

        getUI().setVisible(true);
    }

    /**
     * Nettoye des ui tout ce qui concerne un modèle de navigation.
     *
     * <b>Note:</b> cette méthode doit être appelée avant tout rechargement de
     * modèle de naivgation.
     *
     * @param mainUI l'ui principale
     */
    public void cleanNavigationUI(ObserveMainUI mainUI) {

        // invalidate provider
        setDataSource(null);

        // reset content uis
        mainUI.getContentLayout().reset(mainUI.getContent());

        // clean messages
        mainUI.getContextValue(SwingValidatorMessageTableModel.class).clear();

        // clean tree model

        JTree tree = getUI();

        // remove tree from bridge to disable propagation of any node
        getBridge().setUi(null);

        ObserveNode root = getBridge().getRoot();
        root.removeAllChildren();
        createEmptyModel();

        // no tree navigation view
        tree.setVisible(false);
    }

    public void selectOpenNode(Class<?> type) {

        DataContext context = ObserveSwingApplicationContext.get().getDataContext();

        String[] ids = context.getOpenIds(type);

        if (log.isDebugEnabled()) {
            log.debug("using open ids = " + Arrays.toString(ids));
        }
        if (ids == null) {

            // rien n'est ouvert, rien à selectionner
            return;
        }

        selectNode(ids);
    }

    /**
     * Sélectionne le noeud dans l'arbre de navigation.
     *
     * <b>Note:</b> cette méthode doit être appelée après un rechargement du
     * modèle de navigation.
     */
    public void selectInitialNode() {

        if (log.isDebugEnabled()) {
            log.debug("Will select initial node...");
        }
        DataContext context = ObserveSwingApplicationContext.get().getDataContext();

        String[] path;

        JTree tree = getUI();

        List<String> selectedIds = ObserveSwingApplicationContext.get().getNodesToReselect();
        if (CollectionUtils.isNotEmpty(selectedIds)) {
            if (log.isDebugEnabled()) {
                log.debug("will select previous ids " + selectedIds);
            }
            path = selectedIds.toArray(new String[selectedIds.size()]);
        } else {

            // on trouve le meilleur noeud a selectionner.

            String id = context.getHigherOpenId();

            if (id != null) {

                // on se positionne sur la donnée la plus haute ouverte
                path = context.getOpenIds();
                if (log.isDebugEnabled()) {
                    log.debug("will select open ids " + Arrays.toString(path));
                }
            } else {

                // on selectionne le premier noeud de $root

                ObserveNode node = (ObserveNode) tree.getModel().getRoot();
                if (!node.isLeaf()) {
                    node = node.getFirstChild();
                }
                path = new String[]{node.getId()};
                if (log.isDebugEnabled()) {
                    log.debug("will select first program " + Arrays.toString(path));
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Selected path : " + Arrays.toString(path));
        }

        try {

            selectNode(path);
        } finally {

            // nettoyage du context
            ObserveSwingApplicationContext.get().setNodesToReselect(null);
        }

        // navigation tree should acquire focus
        tree.requestFocus();
    }

    @Override
    public void selectNode(String... path) {
        long count = ObserveNode.count;
        if (log.isDebugEnabled()) {
            log.debug("Will select path : " + Arrays.toString(path));
        }
        getBridge().setPathToSelect(path);
        try {
            super.selectNode(path);
        } finally {
            getBridge().setPathToSelect();
            if (log.isInfoEnabled()) {
                log.info("Creates " + (ObserveNode.count - count) + " nodes to select path " + Arrays.toString(path));
            }
        }
    }

    public ObserveNode addUnsavedNode(ObserveNode parentNode, Class<?> type) {

        // noeud en mode creation
        String label = ObserveI18nDecoratorHelper.getTypeI18nKey(type) + ".unsaved";
        ObserveNode result = new ObserveNode(type, null, label, null, false);
        insertNode(parentNode, result);

        // refresh parent node (render of parent can have changed)
        refreshNode(parentNode, true);

        // Fix bug (if no child in parent node, it will not expand...)
        getUI().fireTreeExpanded(new TreePath(result.getPath()));

        // Select new node
        selectNode(result);

        return result;
    }

    public <E extends IdDto> ObserveNode addOpenable(ObserveNode parentNode, E bean) {

        ReferenceBinderEngine referenceBinderEngine = ObserveSwingApplicationContext.get().getReferenceBinderEngine();
        ReferentialLocale referentialLocale = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialLocale();
        if (bean instanceof TripSeineDto) {

            DataReference<TripSeineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (TripSeineDto) bean);
            return addTripSeine(parentNode, ref);

        } else if (bean instanceof TripLonglineDto) {

            DataReference<TripLonglineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (TripLonglineDto) bean);
            return addTripLongline(parentNode, ref);

        } else if (bean instanceof RouteDto) {

            DataReference<RouteDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (RouteDto) bean);
            return addRoute(parentNode, ref);

        } else if (bean instanceof ActivitySeineDto) {

            DataReference<ActivitySeineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (ActivitySeineDto) bean);
            return addActivitySeine(parentNode, ref);

        } else if (bean instanceof ActivityLonglineDto) {

            DataReference<ActivityLonglineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (ActivityLonglineDto) bean);
            return addActivityLongline(parentNode, ref);

        }
        throw new IllegalStateException("Can not come here!");
    }

    private ObserveNode addTripSeine(ObserveNode parentNode, DataReference<TripSeineDto> bean) {
        ProgramSeineNodeChildLoador loador =
                getChildLoador(ProgramSeineNodeChildLoador.class);
        ObserveNode result = loador.createNode(bean, dataProvider);
        insertNode(parentNode, result);
        return result;
    }

    private ObserveNode addTripLongline(ObserveNode parentNode, DataReference<TripLonglineDto> bean) {
        ProgramLonglineNodeChildLoador loador =
                getChildLoador(ProgramLonglineNodeChildLoador.class);
        ObserveNode result = loador.createNode(bean, dataProvider);
        insertNode(parentNode, result);
        return result;
    }

    private ObserveNode addRoute(ObserveNode parentNode, DataReference<RouteDto> bean) {
        RoutesNodeChildLoador loador =
                getChildLoador(RoutesNodeChildLoador.class);
        ObserveNode result = loador.createNode(bean, dataProvider);
        insertNode(parentNode, result);
        return result;
    }

    private ObserveNode addActivitySeine(ObserveNode parentNode, DataReference<ActivitySeineDto> bean) {
        ActivitySeinesNodeChildLoador loador =
                getChildLoador(ActivitySeinesNodeChildLoador.class);
        ObserveNode result = loador.createNode(bean, dataProvider);
        insertNode(parentNode, result);
        return result;
    }

    private ObserveNode addActivityLongline(ObserveNode parentNode, DataReference<ActivityLonglineDto> bean) {
        ActivityLonglinesNodeChildLoador loador =
                getChildLoador(ActivityLonglinesNodeChildLoador.class);
        ObserveNode result = loador.createNode(bean, dataProvider);
        insertNode(parentNode, result);
        return result;
    }

    public ObserveNode addFloatingObject(ObserveNode parentNode, DataReference<FloatingObjectDto> bean) {
        ActivitySeineNodeChildLoador loador =
                getChildLoador(ActivitySeineNodeChildLoador.class);
        ObserveNode result = loador.createNode(bean, dataProvider);
        insertNode(parentNode, result);
        return result;
    }

    public ObserveNode addSetSeine(ObserveNode parentNode, DataReference<SetSeineDto> bean) {
        ActivitySeineNodeChildLoador loador =
                getChildLoador(ActivitySeineNodeChildLoador.class);
        ObserveNode result = loador.createSetNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    public ObserveNode addSetLongline(ObserveNode parentNode, DataReference<SetLonglineDto> bean) {
        ActivityLonglineNodeChildLoador loador =
                getChildLoador(ActivityLonglineNodeChildLoador.class);
        ObserveNode result = loador.createSetNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    public ObserveNode addProgram(ReferentialReference<ProgramDto> bean) {

        Set<ReferentialReference<ProgramDto>> programs = getDataProvider().getDataSource().getReferentialReferences(ProgramDto.class);
        List<ReferentialReference<ProgramDto>> data = Lists.newArrayList(programs);
        int newIndex = 0;
        if (CollectionUtils.isNotEmpty(data)) {
            sortPrograms(data);
            newIndex = data.indexOf(bean);
        }
        RootNodeChildLoador loador = getChildLoador(RootNodeChildLoador.class);
        ObserveNode result = loador.createNode(bean, dataProvider);
        insertNode(getRootNode(), result, newIndex);
        return result;
    }

    public void removeProgram(String programId) {
        ObserveNode rootNode = getRootNode();
        ObserveNode result = rootNode.getChild(programId, getBridge(), dataProvider);
        Objects.requireNonNull(result, "Could not find program node with id: " + programId);
        removeNode(result);
    }

    public void updateProgram(ProgramDto bean) {
        ObserveNode rootNode = getRootNode();
        ObserveNode result = rootNode.getChild(bean.getId(), getBridge(), dataProvider);
        Objects.requireNonNull(result, "Could not find program node with id: " + bean.getId());
        reloadNode(result, false);
        refreshNode(result, false);
    }

    public void removeTrip(ReferentialReference<ProgramDto> program, DataReference trip) {

        ObserveNode rootNode = getRootNode();
        ObserveNode programNode = rootNode.getChild(program.getId(), getBridge(), dataProvider);
        Objects.requireNonNull(programNode, "Could not find program node with id: " + program);
        ObserveNode tripNode = programNode.getChild(trip.getId(), getBridge(), dataProvider);
        Objects.requireNonNull(tripNode, "Could not find program node with id: " + trip);
        removeNode(tripNode);

        if (programNode.isLeaf()) {
            removeProgram(program.getId());
        }
    }

    public void reloadSelectedNode(boolean refreshFromParent, boolean refreshChilds) {
        ObserveNode node = getSelectedNode();
        ((AbstrctReferenceNodeSupport) node).setReloadEntity(true);

        if (refreshFromParent) {
            node = node.getParent();
        }
        refreshNode(node, refreshChilds);
    }

    public void reloadNode(ObserveNode node, boolean refreshChilds) {
        ((AbstrctReferenceNodeSupport) node).setReloadEntity(true);
        refreshNode(node, refreshChilds);
    }

    @Override
    protected ObserveDataProvider getDataProvider() {
        //        if (provider == null) {
//            ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
//            if (source != null) {
//                provider = new ObserveDataProvider(source);
//                setDataProvider(provider);
//            }
//        }
        return (ObserveDataProvider) super.getDataProvider();
    }

    @Override
    protected ObserveTreeBridge getBridge() {
        return (ObserveTreeBridge) super.getBridge();
    }

    void setDataSource(ObserveSwingDataSource source) {
        NavDataProvider provider = null;
        if (source != null) {
            provider = new ObserveDataProvider(source);
        }
        setDataProvider(provider);
    }

    public void reloadNodeSubTree(ObserveNode node, boolean expandNode) {
        Objects.requireNonNull(node, "node is null, we can not reload its structure");
        ObserveTreeBridge bridge = getBridge();

        // 1. Let's clear node structure
        while (node.getChildCount() > 0) {
            removeNode(node.getFirstChild());
        }

        // 2. We add the node and its parent to the select path in order to force the call to the child loadors when populating the node.
        // see fr.ird.observe.application.swing.ui.tree.ObserveTreeBridge.canLoadChild
        bridge.setPathToSelect(node.getId(), node.getParent().getId());

        try {

            // 3. Let's re-generate node's children by populating the node : this will call the child loaders.
            node.populateNode(bridge, getDataProvider(), true);

        } finally {
            // Clean the path to select
            bridge.setPathToSelect();
        }

        if (expandNode) {

            // Fix bug (if no child in parent node, it will not expand...)
            getUI().fireTreeExpanded(new TreePath(node.getPath()));

        }
    }
}
