package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlineDtos;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDtos;
import fr.ird.observe.services.dto.longline.SizeMeasureDto;
import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.longline.CatchFateLonglineDto;
import fr.ird.observe.services.dto.referential.longline.HealthnessDto;
import fr.ird.observe.services.dto.referential.longline.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.longline.BranchlineService;
import fr.ird.observe.services.service.longline.SetLonglineCatchService;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentUIInitializer;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import jaxx.runtime.context.JAXXContextEntryDef;
import jaxx.runtime.validator.swing.SwingValidator;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class CatchLonglineUIHandler extends ContentTableUIHandler<SetLonglineCatchDto, CatchLonglineDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(CatchLonglineUIHandler.class);

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    private final PropertyChangeListener catchFateChanged;

    private final PropertyChangeListener branchlineChanged;

    private final PropertyChangeListener depredatedChanged;

    private final PropertyChangeListener sizeTableModelModified;

    private final PropertyChangeListener weightTableModelModified;

    private final JAXXContextEntryDef<LonglinePositionHelper<CatchLonglineDto>> POSITION_HELPER_ENTRY =
            UIHelper.newContextEntryDef("CatchLonglineUI-positionHelper", LonglinePositionHelper.class);

    public CatchLonglineUIHandler(CatchLonglineUI ui) {
        super(ui, DataContextType.SetLongline);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
        catchFateChanged = evt -> onCatchFateChanged((ReferentialReference<CatchFateLonglineDto>) evt.getNewValue(), (CatchLonglineDto) evt.getSource());
        branchlineChanged = evt -> onBranchlineChanged((DataReference<BranchlineDto>) evt.getNewValue());
        weightTableModelModified = evt -> onWeightTableModelModified((Boolean) evt.getNewValue());
        sizeTableModelModified = evt -> onSizeTableModelModified((Boolean) evt.getNewValue());
        depredatedChanged = evt -> onDepretadedChanged((Boolean) evt.getNewValue(), (CatchLonglineDto) evt.getSource());
    }

    @Override
    public CatchLonglineUI getUi() {
        return (CatchLonglineUI) super.getUi();
    }

    @Override
    public CatchLonglineUIModel getModel() {
        return (CatchLonglineUIModel) super.getModel();
    }

    @Override
    public void initUI() {

        final CatchLonglineContentTableUIInitializer uiInitializer = new CatchLonglineContentTableUIInitializer(getUi());
        uiInitializer.initUI();

        getModel().addPropertyChangeListener(CatchLonglineUIModel.PROPERTY_SHOW_INDIVIDUAL_TABS, evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            showIndividualTabs(newValue);
        });

        getModel().addPropertyChangeListener(CatchLonglineUIModel.PROPERTY_EDITABLE, evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            setTableModelEditable(newValue);
        });

        getModel().addPropertyChangeListener(ContentUIModel.PROPERTY_FORM, evt -> updateUiWithReferenceSetsFromModel());

        setTableModelEditable(getModel().isEditable());

        LonglinePositionHelper<CatchLonglineDto> positionHelper = new LonglinePositionHelper<>(
                getUi().getSection(),
                getUi().getBasket(),
                getUi().getBranchline(),
                getTableEditBean());

        POSITION_HELPER_ENTRY.setContextValue(getUi(), positionHelper);

    }

    @Override
    public void openUI() {

        if (log.isInfoEnabled()) {
            log.info("OpenUI: " + getModel());
        }

        super.openUI();

        // Reset all sections
        LonglinePositionHelper<CatchLonglineDto> positionHelper = POSITION_HELPER_ENTRY.getContextValue(getUi());
        getUi().getSection().setData(positionHelper.getSections());

        // To be sure always remove listener (could prevent some leaks)
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        getUi().getErrorTableModel().addTableModelListener(computeTabValidStateListener);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

    }

    public void resetBranchline() {

        onBranchlineChanged(null);

        onBranchlineChanged(getTableEditBean().getBranchline());

    }

    public void saveBranchline() {

        if (log.isInfoEnabled()) {
            log.info("Saved modified branchline");
        }

        CatchLonglineUI ui = getUi();

        BranchlineDto branchline = ui.getBranchlineBean();

        String openSetLonglineId = getDataContext().getSelectedSetLonglineId();
        SaveResultDto saveResult = getBranchLineService().save(openSetLonglineId, branchline);
        saveResult.toDto(branchline);

        // on recopie le last update car c'est le laste update de SetLongline qui est renvoyé.
        getBean().setLastUpdateDate(saveResult.getLastUpdateDate());

        getUi().getBranchlineValidator().setChanged(false);

    }

    public void updateCatchAcquisitionMode(CatchAcquisitionModeEnum newMode) {

        if (log.isDebugEnabled()) {
            log.debug("Change CatchAcquisitionMode " + newMode);
        }
        if (newMode == null) {

            // mode null (cela peut arriver avec les bindings)
            return;
        }

        CatchLonglineUI ui = getUi();

        boolean createMode = ui.getTableModel().isCreate();

        CatchLonglineDto editBean = ui.getTableEditBean();

        switch (newMode) {

            case GROUPED:

                if (createMode) {

                    editBean.setTotalWeight(null);
                    editBean.setCount(null);
                    editBean.setHookPosition(null);

                }

                break;

            case INDIVIDUAL:

                if (createMode) {

                    // on positionne le count à 1 (seule valeur possible)
                    editBean.setCount(1);

                }

                break;
        }

        boolean isGrouped = CatchAcquisitionModeEnum.GROUPED.equals(newMode);

        ui.getTotalWeight().setEnabled(isGrouped);
        ui.getCount().setEnabled(isGrouped);

        boolean isIndividual = CatchAcquisitionModeEnum.INDIVIDUAL.equals(newMode);

        ui.getHookPosition().setEnabled(isIndividual);
        ui.getSection().setEnabled(isIndividual);
        ui.getBasket().setEnabled(isIndividual);
        ui.getBranchline().setEnabled(isIndividual);

        if (createMode) {

            // on propage le mode de saisie dans le bean
            editBean.setAcquisitionMode(newMode.ordinal());

        }

        boolean showIndividualTabs = !createMode && isIndividual;

        ui.getModel().setShowIndividualTabs(showIndividualTabs);

    }

    @Override
    protected void onSelectedRowChanged(int editingRow, CatchLonglineDto bean, boolean create) {

        if (log.isInfoEnabled()) {
            log.info("Selected row changed: " + editingRow + ", create? " + create);
        }

        CatchLonglineUI ui = getUi();

        UIHelper.stopEditing(ui.getSizeMeasuresTable());
        UIHelper.stopEditing(ui.getWeightMeasuresTable());

        CatchLonglineTableModel tableModel = getTableModel();
        CatchLonglineUIModel model = getModel();

        boolean emptySelection = editingRow == -1;

        // load size measures

        SizeMeasuresTableModel sizeMeasuresTableModel = model.getSizeMeasuresTableModel();
        List<SizeMeasureDto> sizes = emptySelection ? Collections.emptyList() : sizeMeasuresTableModel.getCacheForRow(editingRow);
        if (sizes == null) {

            if (log.isInfoEnabled()) {
                log.info("init size measures for row " + editingRow);
            }

            // first time coming on this row

            if (tableModel.isCreate()) {

                // create mode: just init with empty list
                sizes = Collections.emptyList();

                if (log.isInfoEnabled()) {
                    log.info("create mode, use an empty list");
                }

            } else {

                // updating mode: loading from db
                sizes = Lists.newArrayList(bean.getSizeMeasure());
                if (log.isInfoEnabled()) {
                    log.info("Loaded sizes (" + bean.getId() + "): " + sizes.size());
                }
            }

            // init size measures
            sizeMeasuresTableModel.initCacheForRow(editingRow, sizes);

        } else {

            if (log.isInfoEnabled()) {
                log.info("Using existing sizes for row " + editingRow + " : " + sizes.size());
            }
        }

        sizeMeasuresTableModel.setData(sizes);

        // load weight measures

        WeightMeasuresTableModel weightMeasuresTableModel = model.getWeightMeasuresTableModel();
        List<WeightMeasureDto> weights = emptySelection ? Collections.emptyList() : weightMeasuresTableModel.getCacheForRow(editingRow);

        if (weights == null) {

            if (tableModel.isCreate()) {

                // create mode: just init with empty list
                weights = Collections.emptyList();

            } else {

                // updating mode: loading from db
                weights = Lists.newArrayList(bean.getWeightMeasure());
                if (log.isInfoEnabled()) {
                    log.info("Loaded weights (" + bean.getId() + "): " + weights.size());
                }

            }

            // init weights measures
            weightMeasuresTableModel.initCacheForRow(editingRow, weights);

        } else {

            if (log.isInfoEnabled()) {
                log.info("Using existing weights for row " + editingRow + ": " + weights.size());
            }
        }

        weightMeasuresTableModel.setData(weights);

        sizeMeasuresTableModel.setModified(false);
        weightMeasuresTableModel.setModified(false);

        if (!tableModel.isEditable()) {
            return;
        }

        JComponent requestFocus;

        if (tableModel.isCreate()) {

            // go back to first pane
            ui.getFishingOperationTabPane().setSelectedIndex(0);

            // select by default individual acquisition mode
            ui.getAcquisitionModeGroup().setSelectedValue(null);
            ui.getAcquisitionModeGroup().setSelectedValue(CatchAcquisitionModeEnum.INDIVIDUAL);

            ui.getPredator().setEnabled(false);
            ui.getHookWhenDiscarded().setEnabled(false);

            ui.getDiscardHealthness().setEnabled(false);
            ui.getBeatDiameter().setEnabled(false);

            requestFocus = ui.getSpeciesCatch();

        } else {

            int acquisitionMode = bean.getAcquisitionMode();
            CatchAcquisitionModeEnum enumValue = CatchAcquisitionModeEnum.valueOf(acquisitionMode);
            ui.getAcquisitionModeGroup().setSelectedValue(null);
            ui.getAcquisitionModeGroup().setSelectedValue(enumValue);

            if (enumValue.equals(CatchAcquisitionModeEnum.GROUPED)) {

                requestFocus = ui.getCount();

            } else {

                requestFocus = ui.getCatchHealthness();

            }

            //FIXME Voir si pas besoin aussi de relancer les binding (catchFateChanged, branchlineChanged) ?
            onDepretadedChanged(bean.getDepredated(), bean);

        }

        bean.removePropertyChangeListener(CatchLonglineDto.PROPERTY_CATCH_FATE_LONGLINE, catchFateChanged);
        bean.addPropertyChangeListener(CatchLonglineDto.PROPERTY_CATCH_FATE_LONGLINE, catchFateChanged);

        bean.removePropertyChangeListener(CatchLonglineDto.PROPERTY_BRANCHLINE, branchlineChanged);
        bean.addPropertyChangeListener(CatchLonglineDto.PROPERTY_BRANCHLINE, branchlineChanged);

        bean.removePropertyChangeListener(CatchLonglineDto.PROPERTY_DEPREDATED, depredatedChanged);
        bean.addPropertyChangeListener(CatchLonglineDto.PROPERTY_DEPREDATED, depredatedChanged);

        LonglinePositionHelper<CatchLonglineDto> positionHelper = POSITION_HELPER_ENTRY.getContextValue(ui);
        positionHelper.resetPosition(bean);

        requestFocus.requestFocus();

    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        CatchLonglineUI ui = getUi();
        {
            // init main table

            JTable table = ui.getTable();

            UIHelper.setI18nTableHeaderRenderer(
                    table,
                    n("observe.content.catchLongline.table.sectionHaulingId"),
                    n("observe.content.catchLongline.table.sectionHaulingId.tip"),
                    n("observe.content.catchLongline.table.basketHaulingId"),
                    n("observe.content.catchLongline.table.basketHaulingId.tip"),
                    n("observe.content.catchLongline.table.branchlineHaulingId"),
                    n("observe.content.catchLongline.table.branchlineHaulingId.tip"),

                    n("observe.content.catchLongline.table.speciesCatch"),
                    n("observe.content.catchLongline.table.speciesCatch.tip"),
                    n("observe.content.catchLongline.table.acquisitionMode"),
                    n("observe.content.catchLongline.table.acquisitionMode.tip"),
                    n("observe.content.catchLongline.table.count"),
                    n("observe.content.catchLongline.table.count.tip"),
                    n("observe.content.catchLongline.table.catchHealthness"),
                    n("observe.content.catchLongline.table.catchHealthness.tip"),
                    n("observe.content.catchLongline.table.catchFateLongline"),
                    n("observe.content.catchLongline.table.catchFateLongline.tip"),
                    n("observe.content.catchLongline.table.discardHealthness"),
                    n("observe.content.catchLongline.table.discardHealthness.tip"),
                    n("observe.content.catchLongline.table.depredated"),
                    n("observe.content.catchLongline.table.depredated.tip"),
                    n("observe.content.catchLongline.table.comment"),
                    n("observe.content.catchLongline.table.comment.tip"));

            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newDataReferenceDecorateTableCellRenderer(renderer, SectionDto.class, DecoratorService.HAULING_IDENTIFIER));
            UIHelper.setTableColumnRenderer(table, 1, UIHelper.newDataReferenceDecorateTableCellRenderer(renderer, BasketDto.class, DecoratorService.HAULING_IDENTIFIER));
            UIHelper.setTableColumnRenderer(table, 2, UIHelper.newDataReferenceDecorateTableCellRenderer(renderer, BranchlineDto.class, DecoratorService.HAULING_IDENTIFIER));

            UIHelper.setTableColumnRenderer(table, 3, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
            UIHelper.setTableColumnRenderer(table, 4, UIHelper.newEnumTableCellRenderer(renderer, CatchAcquisitionModeEnum.class));
            UIHelper.setTableColumnRenderer(table, 5, UIHelper.newEmptyNumberTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 6, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, HealthnessDto.class));
            UIHelper.setTableColumnRenderer(table, 7, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, CatchFateLonglineDto.class));
            UIHelper.setTableColumnRenderer(table, 8, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, HealthnessDto.class));
            UIHelper.setTableColumnRenderer(table, 9, UIHelper.newBooleanTableCellRenderer(renderer));
            UIHelper.setTableColumnRenderer(table, 10, UIHelper.newStringTableCellRenderer(renderer, 10, true));

        }

        {
            // init size measures table
            JTable table = ui.getSizeMeasuresTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.catchLongline.table.sizeMeasureType"),
                                                n("observe.content.catchLongline.table.sizeMeasureType.tip"),
                                                n("observe.content.catchLongline.table.size"),
                                                n("observe.content.catchLongline.table.size.tip"));

            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SizeMeasureTypeDto.class));
            UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));

            ReferentialReferenceDecorator<SizeMeasureTypeDto> decorator = getReferentialReferenceDecorator(SizeMeasureTypeDto.class);

            List<ReferentialReference<SizeMeasureTypeDto>> sizeMeasureTypes =
                    Lists.newArrayList(getDataSource().getReferentialReferences(SizeMeasureTypeDto.class));

            UIHelper.setTableColumnEditor(table, 0, ContentUIInitializer.newDataColumnEditor(sizeMeasureTypes, decorator));
            UIHelper.setTableColumnEditor(table, 1, ContentUIInitializer.newFloatColumnEditor(table));

            initInlineTable(ui.getSizeMeasuresScrollPane(),
                            table,
                            getModel().getSizeMeasuresTableModel(),
                            sizeTableModelModified,
                            ui.getSizeMeasuresTablePopup(),
                            ui.getAddSizeMeasure(),
                            ui.getDeleteSelectedSizeMeasure());

        }

        {
            // init weight measures table
            JTable table = ui.getWeightMeasuresTable();

            UIHelper.setI18nTableHeaderRenderer(table,
                                                n("observe.content.catchLongline.table.weightMeasureType"),
                                                n("observe.content.catchLongline.table.weightMeasureType.tip"),
                                                n("observe.content.catchLongline.table.weight"),
                                                n("observe.content.catchLongline.table.weight.tip"));

            UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, WeightMeasureTypeDto.class));
            UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));

            ReferentialReferenceDecorator<WeightMeasureTypeDto> decorator = getReferentialReferenceDecorator(WeightMeasureTypeDto.class);

            List<ReferentialReference<WeightMeasureTypeDto>> weightMeasureTypes =
                    Lists.newArrayList(getDataSource().getReferentialReferences(WeightMeasureTypeDto.class));

            UIHelper.setTableColumnEditor(table, 0, ContentUIInitializer.newDataColumnEditor(weightMeasureTypes, decorator));
            UIHelper.setTableColumnEditor(table, 1, ContentUIInitializer.newFloatColumnEditor(table));

            initInlineTable(ui.getWeightMeasuresScrollPane(),
                            table,
                            getModel().getWeightMeasuresTableModel(),
                            weightTableModelModified,
                            ui.getWeightMeasuresTablePopup(),
                            ui.getAddWeightMeasure(),
                            ui.getDeleteSelectedWeightMeasure());

        }

        getTableModel().addPropertyChangeListener(ContentTableModel.CREATE_PROPERTY, evt -> {
            Boolean oldValue = (Boolean) evt.getOldValue();
            Boolean newValue = (Boolean) evt.getNewValue();
            setTableModelCreate(oldValue, newValue);
        });
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetLonglineId();
    }

    @Override
    protected void doPersist(SetLonglineCatchDto bean) {
        SaveResultDto saveResult = getSetLonglineCatchService().save(bean);
        bean.setLastUpdateDate(saveResult.getLastUpdateDate());
    }

    @Override
    protected void loadEditBean(String beanId) {

        Form<SetLonglineCatchDto> form = getSetLonglineCatchService().loadForm(beanId);

        CatchLonglineUIModel model = getModel();

        loadReferentialReferenceSetsInModel(form);

        model.setForm(form);

        LonglinePositionHelper<CatchLonglineDto> positionHelper = POSITION_HELPER_ENTRY.getContextValue(getUi());
        positionHelper.initSections(
                form.getObject(),
                form.getObject().getCatchLongline());

        SetLonglineCatchDtos.copySetLonglineCatchDto(form.getObject(), getBean());

    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case CatchLonglineDto.PROPERTY_SPECIES_CATCH:
            case CatchLonglineDto.PROPERTY_PREDATOR: {
                String speciesListId;

                if (CatchLonglineDto.PROPERTY_SPECIES_CATCH.equals(propertyName)) {
                    speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListLonglineCatchId();
                } else {
                    speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListLonglineDepredatorId();
                }

                String tripLonglineId = getDataContext().getSelectedTripLonglineId();

                TripLonglineService tripLonglineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripLonglineService();
                result = (List) tripLonglineService.getSpeciesByListAndTrip(tripLonglineId, speciesListId);

                result = ReferentialReferences.filterEnabled(result);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }

                break;
            }

        }

        return result;

    }

    @Override
    protected void closeSafeUI() {

        if (log.isInfoEnabled()) {
            log.info("CloseUI: " + getModel());
        }
        super.closeSafeUI();

        // remove listener
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);

    }

    @Override
    protected void resetEditBean() {

        UIHelper.stopEditing(getUi().getSizeMeasuresTable());
        UIHelper.stopEditing(getUi().getWeightMeasuresTable());

        onBranchlineChanged(null);

        super.resetEditBean();

        onBranchlineChanged(getTableEditBean().getBranchline());

    }

    protected void setTableModelEditable(Boolean newValue) {

        getModel().getSizeMeasuresTableModel().setEditable(newValue);
        getModel().getWeightMeasuresTableModel().setEditable(newValue);

    }

    protected void setTableModelCreate(Boolean oldValue, Boolean newValue) {

        if (BooleanUtils.isTrue(oldValue) && BooleanUtils.isFalse(newValue)) {

            // just save a row
            // reload acquisition mode (will rebind some stuff)

            CatchAcquisitionModeEnum acquisitionModeEnum = (CatchAcquisitionModeEnum) getUi().getAcquisitionModeGroup().getSelectedValue();

            getUi().getAcquisitionModeGroup().setSelectedValue(null);
            getUi().getAcquisitionModeGroup().setSelectedValue(acquisitionModeEnum);

        }

    }

    protected void showIndividualTabs(boolean newValue) {

        if (log.isInfoEnabled()) {
            log.info("will show individuals tabs ?" + newValue);
        }

        getUi().getFoodAndSexualFormTab().setEnabled(newValue);
        getUi().getSizeMeasuresFormTab().setEnabled(newValue);
        getUi().getWeightMeasuresFormTab().setEnabled(newValue);

        if (!newValue && getUi().getFishingOperationTabPane().getSelectedIndex() > 2) {

            // go back to first tab
            getUi().getFishingOperationTabPane().setSelectedIndex(0);

        }

    }

    protected void onCatchFateChanged(ReferentialReference<CatchFateLonglineDto> newValue, CatchLonglineDto tableEditBean) {

        CatchLonglineUI ui = getUi();

        if (newValue == null || !"fr.ird.observe.entities.referentiel.longline.CatchFateLongline#1239832686125#0.3".equals(newValue.getId())) {

            // not discarded
            ui.getDiscardHealthness().setEnabled(false);
            ui.getHookWhenDiscarded().setEnabled(false);

            getTableEditBean().setHookWhenDiscarded(null);
            getTableEditBean().setDiscardHealthness(null);

        } else {

            // discarded
            ui.getDiscardHealthness().setEnabled(true);
            ui.getHookWhenDiscarded().setEnabled(true);

        }

    }

    protected void onBranchlineChanged(DataReference<BranchlineDto> newValue) {

        CatchLonglineUI ui = getUi();

        BranchlineDto branchline = ui.getBranchlineBean();

        SwingValidator<BranchlineDto> branchlineValidator = ui.getBranchlineValidator();
        if (newValue == null) {

            if (log.isInfoEnabled()) {
                log.info("Remove branchline");
            }

            branchlineValidator.setBean(null);

        } else {

            if (log.isInfoEnabled()) {
                log.info("Use branchline: " + newValue);
            }

            String setLonglineId = getDataContext().getSelectedSetLonglineId();

            Form<BranchlineDto> form = getBranchLineService().loadForm(setLonglineId, newValue.getId());
            BranchlineDtos.copyBranchlineDto(form.getObject(), branchline);

            if (ui.getValidator().getBean() == null) {
                ui.getValidator().setBean(getBean());
            }

            branchlineValidator.setBean(branchline);

        }

        branchlineValidator.setChanged(false);

    }

    protected void onDepretadedChanged(Boolean newValue, CatchLonglineDto tableEditBean) {

        CatchLonglineUI ui = getUi();

        if (BooleanUtils.isTrue(newValue)) {

            // depredated
            ui.getBeatDiameter().setEnabled(true);
            ui.getPredator().setEnabled(true);

        } else {

            // not depredated
            ui.getBeatDiameter().setEnabled(false);
            ui.getPredator().setEnabled(false);
            tableEditBean.setBeatDiameter(null);
            tableEditBean.setPredator(null);

        }

    }

    public void addSizeMeasure() {

        SizeMeasuresTableModel tableModel = getUi().getSizeMeasuresTableModel();

        tableModel.addNewRow();

    }

    public void deleteSelectedSizeMeasure() {

        SizeMeasuresTableModel tableModel = getUi().getSizeMeasuresTableModel();

        boolean selectionEmpty = tableModel.isSelectionEmpty();

        if (!selectionEmpty) {

            SizeMeasureDto data = tableModel.getSelectedRow();

            if (log.isInfoEnabled()) {
                log.info("Delete: " + data);
            }

            ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
            int response = UIHelper.askUser(mainUI,
                                            t("observe.title.delete"),
                                            t("observe.content.sizeMeasure.delete.message"),
                                            JOptionPane.WARNING_MESSAGE,
                                            new Object[]{t("observe.choice.confirm.delete"),
                                                    t("observe.choice.cancel")},
                                            1);

            if (response != 0) {

                // user cancel
                return;
            }

            tableModel.removeSelectedRow();

        }

    }


    protected void onSizeTableModelModified(Boolean newValue) {

        if (newValue) {

            // modify the validator, since this is the best way to prevent table edit form actions
            // that something was modified on the form
            getUi().getValidatorTable().setChanged(true);

        }

        // recompute table model valid state
        getModel().getSizeMeasuresTableModel().validate();

    }

    public void addWeightMeasure() {

        WeightMeasuresTableModel tableModel = getUi().getWeightMeasuresTableModel();

        tableModel.addNewRow();

    }

    public void deleteSelectedWeightMeasure() {

        WeightMeasuresTableModel tableModel = getUi().getWeightMeasuresTableModel();

        boolean selectionEmpty = tableModel.isSelectionEmpty();

        if (!selectionEmpty) {

            WeightMeasureDto data = tableModel.getSelectedRow();

            if (log.isInfoEnabled()) {
                log.info("Delete: " + data);
            }

            ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
            int response = UIHelper.askUser(mainUI,
                                            t("observe.title.delete"),
                                            t("observe.content.weightMeasure.delete.message"),
                                            JOptionPane.WARNING_MESSAGE,
                                            new Object[]{t("observe.choice.confirm.delete"),
                                                    t("observe.choice.cancel")},
                                            1);

            if (response != 0) {

                // user cancel
                return;
            }

            tableModel.removeSelectedRow();

        }

    }

    protected void onWeightTableModelModified(Boolean newValue) {

        if (newValue) {

            // modify the validator, since this is the best way to prevent table edit form actions
            // that something was modified on the form
            getUi().getValidatorTable().setChanged(true);

        }

        // recompute table model valid state
        getModel().getWeightMeasuresTableModel().validate();

    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean caracteristicsTabValid = !errorProperties.removeAll(CatchLonglineUIModel.CARACTERISTIC_TAB_PROPERTIES);
        boolean depredatedTabValid = !errorProperties.removeAll(CatchLonglineUIModel.DEPREDATED_TAB_PROPERTIES);
        boolean foodAndSexualTabValid = !errorProperties.removeAll(CatchLonglineUIModel.FOOD_AND_SEXUAL_TAB_PROPERTIES);
        boolean branchlineTabValid = !errorProperties.removeAll(CatchLonglineUIModel.BRANCHLINE_TAB_PROPERTIES);

        CatchLonglineUIModel model = getModel();

        model.setCaracteristicsTabValid(caracteristicsTabValid);
        model.setDepredatedTabValid(depredatedTabValid);
        model.setFoodAndSexualTabValid(foodAndSexualTabValid);
        model.setBranchlineTabValid(branchlineTabValid);

    }

    @Override
    protected CatchLonglineTableModel getTableModel() {
        return (CatchLonglineTableModel) super.getTableModel();
    }

    protected SetLonglineCatchService getSetLonglineCatchService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newSetLonglineCatchService();
    }

    protected BranchlineService getBranchLineService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newBranchlineService();
    }

}
