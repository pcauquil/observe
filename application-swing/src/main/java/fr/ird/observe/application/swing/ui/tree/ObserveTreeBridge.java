/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import jaxx.runtime.swing.nav.tree.NavTreeBridge;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTree;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Bridge specialise pour reduire le count de chargements de noeuds non visibles.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class ObserveTreeBridge extends NavTreeBridge<ObserveNode> {

    /** Logger. */
    static private final Log log = LogFactory.getLog(ObserveTreeBridge.class);

    protected JTree ui;

    protected final List<String> pathToSelect;

    public ObserveTreeBridge() {
        pathToSelect = new ArrayList<>();
    }

    public void setUi(JTree ui) {
        this.ui = ui;
    }

    @Override
    public boolean canLoadChild(ObserveNode node) {

        ObserveNode root = getRoot();
        if (node.equals(root)) {

            // always allow to load childs from level 0
            return true;
        }
        ObserveNode parent = node.getParent();
        if (parent.equals(root)) {

            // always allow to load childs from level 0
            return true;
        }

        TreeNode[] pathToRoot = getPathToRoot(node);
        TreePath path = new TreePath(pathToRoot);
        if (CollectionUtils.isNotEmpty(pathToSelect)) {

            // select mode is on
            if (log.isDebugEnabled()) {
                log.debug("There is a selected path, try to use it for " + node);
            }
            String nodeId = node.getId();

            ObserveNode containerNode;
            if (nodeId.startsWith("observe.type.")) {
                containerNode = node.getParent();
            } else {
                containerNode = node;
            }

            String containerNodeId = containerNode.getId();
            if (log.isDebugEnabled()) {
                log.debug("Test if data node is in path ? " +
                          containerNodeId + " : " +
                          pathToSelect.contains(containerNodeId));
            }

            if (pathToSelect.contains(containerNodeId)) {

                // ok on the good way...
                return true;
            }

            // not on a good way, skip loading of childs...
            return false;
        }

        if (ui == null) {
            if (log.isDebugEnabled()) {
                log.debug("No ui in bridge!!!");
            }

            // no ui, and no path to select :  so no need to restrict
            return true;
        }

        boolean visible = ui.isVisible(path);

        if (!visible) {

            // only visible node can be loaded
            return false;
        }

        TreePath selected = ui.getSelectionPath();
        if (!path.equals(selected)) {

            // only selected node can be loaded
            return false;
        }

        // ok childs can be loaded
        return true;
    }

    public void setPathToSelect(String... pathToSelect) {
        this.pathToSelect.clear();
        this.pathToSelect.addAll(Arrays.asList(pathToSelect));
    }
}
