package fr.ird.observe.application.swing.ui.util.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created on 12/10/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class EditableList<E extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1L;

    final List<E> original = new ArrayList<>();

    final List<E> data = new ArrayList<>();

    public List<E> getData() {
        return data;
    }

    public void setData(List<E> data) {
        this.data.clear();
        this.data.addAll(data);
    }

    public List<E> getOriginal() {
        return original;
    }

    public void setOriginal(List<E> data) {
        this.original.clear();
        this.original.addAll(data);
    }

    public void reset() {
        setData(Collections.emptyList());
    }
}
