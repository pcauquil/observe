/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content;

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Le modèle d'un écran d'édition
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class ContentUIModel<E extends IdDto> extends AbstractSerializableBean {

    public static final String PROPERTY_EDIT_BEAN = "editBean";

    public static final String PROPERTY_MODE = "mode";

    public static final String PROPERTY_READING_MODE = "readingMode";

    public static final String PROPERTY_CREATING_MODE = "creatingMode";

    public static final String PROPERTY_UPDATING_MODE = "updatingMode";

    public static final String PROPERTY_ENABLED = "enabled";

    public static final String PROPERTY_MODIFIED = "modified";

    public static final String PROPERTY_EDITING = "editing";

    public static final String PROPERTY_EDITABLE = "editable";

    public static final String PROPERTY_VALID = "valid";

    public static final String PROPERTY_CAN_WRITE = "canWrite";

    public static final String PROPERTY_FORM = "form";

    /** Logger */
    static private final Log log = LogFactory.getLog(ContentUIModel.class);

    private static final long serialVersionUID = 1L;

    protected final Class<E> beanType;

    protected Form<E> form;

    protected E bean;

    protected ContentMode mode = ContentMode.READ;

    protected boolean enabled;

    protected boolean editing;

    protected boolean valid;

    protected boolean modified;

    protected boolean editable;

    protected boolean canWrite;

    /**
     * Les référentiels qu'on peut utiliser, ils sont chargés depuis le cache des référentiels et peuvent être filtrés.
     */
    protected ImmutableMap<String, ReferentialReferenceSet<?>> referentialReferenceSetsByPropertyName = ImmutableMap.of();

    /**
     * Les données métier qu'on peut utiliser.
     */
    protected ImmutableMap<String, DataReferenceSet<?>> dataReferenceSetsByPropertyName = ImmutableMap.of();

    public static <E extends IdDto> ContentUIModel<E> newModel(ObserveContentUI<E> ui) {

        String uiName = ui.getClass().getName();
        String modelName = uiName + "Model";

        try {

            Class<ContentUIModel<E>> modelClass = (Class<ContentUIModel<E>>) Class.forName(modelName);
            return modelClass.newInstance();

        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("Could not create model for ui: " + ui, e);
        }

    }

    public ContentUIModel(Class<E> beanType) {
        this.beanType = beanType;
        try {
            setBean(beanType.newInstance());

        } catch (Exception e) {
            // ne devrait jamain arrive
            if (log.isErrorEnabled()) {
                log.error(e);
            }
        }

    }

    public E getBean() {
        return bean;
    }

    public void setBean(E bean) {
        Object oldValue = this.bean;
        this.bean = bean;
        firePropertyChange(PROPERTY_EDIT_BEAN, oldValue, bean);
    }

    public Form<E> getForm() {
        return form;
    }

    public void setForm(Form<E> form) {
        Object oldValue = getForm();
        this.form = form;
        firePropertyChange(PROPERTY_FORM, oldValue, form);
    }

    public ContentMode getMode() {
        return mode;
    }

    public void setMode(ContentMode mode) {
        ContentMode oldValue = this.mode;
        this.mode = mode;
        firePropertyChange(PROPERTY_MODE, oldValue, mode);
        firePropertyChange(PROPERTY_READING_MODE, ContentMode.READ.equals(oldValue), isReadingMode());
        firePropertyChange(PROPERTY_CREATING_MODE, ContentMode.CREATE.equals(oldValue), isCreatingMode());
        firePropertyChange(PROPERTY_UPDATING_MODE, ContentMode.UPDATE.equals(oldValue), isUpdatingMode());
    }

    /**
     * @return {@code true} si l'écran est en mode creation
     * @see ContentMode#CREATE
     */
    public boolean isCreatingMode() {
        return ContentMode.CREATE.equals(getMode());
    }

    /**
     * @return {@code true} si l'écran est en mode mise a jour
     * @see ContentMode#UPDATE
     */
    public boolean isUpdatingMode() {
        return ContentMode.UPDATE.equals(getMode());
    }

    /**
     * @return {@code true} si l'écran est en mode lecture
     * @see ContentMode#READ
     */
    public boolean isReadingMode() {
        return ContentMode.READ.equals(getMode());
    }

    public Class<E> getBeanType() {
        return beanType;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        Object oldValue = this.enabled;
        this.enabled = enabled;
        firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        Object oldValue = this.editable;
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    public boolean isEditing() {
        return editing;
    }

    public void setEditing(boolean editing) {
        Object oldValue = this.editing;
        this.editing = editing;
        firePropertyChange(PROPERTY_EDITING, oldValue, editing);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        Object oldValue = this.valid;
        this.valid = valid;
        firePropertyChange(PROPERTY_VALID, oldValue, valid);
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        Object oldValue = this.modified;
        this.modified = modified;
        firePropertyChange(PROPERTY_MODIFIED, oldValue, modified);
    }

    public boolean isCanWrite() {
        return canWrite;
    }

    public void setCanWrite(boolean canWrite) {
        boolean oldValue = isCanWrite();
        this.canWrite = canWrite;
        firePropertyChange(PROPERTY_CAN_WRITE, oldValue, canWrite);
    }

    public <D extends ReferentialDto> Set<ReferentialReference<D>> getReferentialReferences(String name) {

        ReferentialReferenceSet<D> referentialReferenceSet = getReferentialReferenceSet(name);
        return referentialReferenceSet.getReferences();

    }

    public <D extends DataDto> Set<DataReference<D>> getDataReferences(String name) {

        DataReferenceSet<D> referentialReferenceSet = getDataReferenceSet(name);
        return referentialReferenceSet.getReferences();

    }

    public <D extends ReferentialDto> Optional<ReferentialReference<D>> tryGetReferentialReferenceById(String name, String id) {

        ReferentialReferenceSet<D> referenceSet = getReferentialReferenceSet(name);
        return referenceSet.tryGetReferenceById(id);

    }

    public void setReferentialReferenceSets(ImmutableMap<String, ReferentialReferenceSet<?>> referentialReferenceSetsByPropertyName) {
        this.referentialReferenceSetsByPropertyName = referentialReferenceSetsByPropertyName;
    }

    public void setDataReferenceSetsByPropertyName(ImmutableMap<String, DataReferenceSet<?>> dataReferenceSetsByPropertyName) {
        this.dataReferenceSetsByPropertyName = dataReferenceSetsByPropertyName;
    }

    public ImmutableMap<String, ReferentialReferenceSet<?>> getReferentialReferenceSets() {
        return referentialReferenceSetsByPropertyName;
    }

    public ImmutableMap<String, DataReferenceSet<?>> getDataReferenceSets() {
        return dataReferenceSetsByPropertyName;
    }

    protected <R extends DataDto> Optional<Set<DataReference<R>>> tryToGetDataReferenceSet(String propertyName) {
        DataReferenceSet<R> referenceSet = getDataReferenceSet(propertyName);
        Set<DataReference<R>> references = null;
        if (referenceSet != null) {
            references = referenceSet.getReferences();
        }
        return Optional.ofNullable(references);
    }

    protected <R extends ReferentialDto> Optional<Set<ReferentialReference<R>>> tryToGetReferentialReferenceSet(String propertyName) {
        ReferentialReferenceSet<R> referenceSet = getReferentialReferenceSet(propertyName);
        Set<ReferentialReference<R>> references = null;
        if (referenceSet != null) {
            references = referenceSet.getReferences();
        }
        return Optional.ofNullable(references);
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }

    private <R extends ReferentialDto> ReferentialReferenceSet<R> getReferentialReferenceSet(String propertyName) {
        ImmutableMap<String, ReferentialReferenceSet<?>> referenceSet = this.referentialReferenceSetsByPropertyName;
        Objects.requireNonNull(referenceSet, "Could not find referantialRefenceSet named " + propertyName);
        return (ReferentialReferenceSet<R>) referenceSet.get(propertyName);
    }

    public <R extends DataDto> DataReferenceSet<R> getDataReferenceSet(String propertyName) {
        DataReferenceSet<?> referenceSet = dataReferenceSetsByPropertyName.get(propertyName);
        Objects.requireNonNull(referenceSet, "Could not find dataReferenceSet named " + propertyName);
        return (DataReferenceSet<R>) referenceSet;
    }

}
