/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.export;

import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import java.io.Serializable;
import java.util.Objects;

/** Une classe qui représente une entrée dans le modèle (sans la sélection) */
public class TripEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final ReferentialReference<ProgramDto> program;

    protected final DataReference trip;

    protected final boolean exist;

    public TripEntry(ReferentialReference<ProgramDto> program, DataReference trip, boolean exist) {
        Objects.requireNonNull(program);
        Objects.requireNonNull(trip);
        this.program = program;
        this.trip = trip;
        this.exist = exist;
    }

    public ReferentialReference<ProgramDto> getProgram() {
        return program;
    }

    public String getProgramId() {
        return program.getId();
    }

    public DataReference getTrip() {
        return trip;
    }

    public String getTripId() {
        return trip.getId();
    }

    public boolean isExist() {
        return exist;
    }

}
