package fr.ird.observe.application.swing;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveTextGenerator {

    private static final Log log = LogFactory.getLog(ObserveTextGenerator.class);

    protected static final String DATA_SOURCE_CONFIGURATION_TEMPLATE = "dataSourceConfiguration.ftl";

    protected static final String CONNEXION_TEST_RESULT_TEMPLATE = "connexionTestResult.ftl";

    protected static final String DATA_SOURCE_SELECT_MODE_RESUME_TEMPLATE = "dataSourceSelectModeResume.ftl";

    protected static final String DATA_SOURCE_CONNECTION_REPORT_TEMPLATE = "dataSourceConnectionReport.ftl";

    protected static final String DATA_SOURCE_INFORMATION_TEMPLATE = "dataSourceInformation.ftl";

    protected static final String DATA_SOURCE_POLICY_TEMPLATE = "dataSourcePolicy.ftl";


    protected final Configuration freemarkerConfiguration;

    protected final ObserveSwingApplicationConfig observeConfiguration;

    public ObserveTextGenerator(ObserveSwingApplicationConfig observeConfiguration) {
        this.observeConfiguration = observeConfiguration;

        freemarkerConfiguration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(ObserveTextGenerator.class, "/ftl");
        freemarkerConfiguration.setTemplateLoader(templateLoader);

//        freemarkerConfiguration.setObjectWrapper(new BeansWrapper(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS));
    }

    public String getConfigurationDetail(ObserveDataSourceConfiguration dataSourceConfiguration) {
        return generateHtml(DATA_SOURCE_CONFIGURATION_TEMPLATE, dataSourceConfiguration);
    }

    public String getConnexionTestResultMessage(StorageUIModel model) {
        return generateHtml(CONNEXION_TEST_RESULT_TEMPLATE, model);
    }

    public String getLoadDataSourceResume(StorageUIModel model) {
        return generateHtml(DATA_SOURCE_SELECT_MODE_RESUME_TEMPLATE, model);
    }

    public String getDataSourceConnectionReport(StorageUIModel model) {
        return generateHtml(DATA_SOURCE_CONNECTION_REPORT_TEMPLATE, model);
    }

    public String getDataSourceInfo(ObserveSwingDataSource source) {
        return generateHtml(DATA_SOURCE_INFORMATION_TEMPLATE, source);
    }

    public String getDataSourcePolicy(ObserveDataSourceInformation model) {
        return generateHtml(DATA_SOURCE_POLICY_TEMPLATE, model);
    }

    protected String generateHtml(String templateName, Object model) {
        return generateHtml(observeConfiguration.getLocale(), templateName, model);
    }

    protected String generateHtml(Locale locale, String templateName, Object model) {

        try {

            // Get freemarker template
            Template mapTemplate = freemarkerConfiguration.getTemplate(templateName, locale);

            Writer out = new StringWriter();
            mapTemplate.process(model, out);

            out.flush();

            return out.toString();


        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("error while generating html", ex);
            }
            throw new ApplicationTechnicalException(t("observe.generateHtml.error", templateName), ex);
        }
    }
}
