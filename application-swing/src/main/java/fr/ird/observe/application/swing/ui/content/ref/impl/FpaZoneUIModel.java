package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.FpaZoneDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

/**
 * Created on 9/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class FpaZoneUIModel extends ContentReferenceUIModel<FpaZoneDto> {

    private static final long serialVersionUID = 1L;

    public FpaZoneUIModel() {
        super(FpaZoneDto.class,
              new String[]{FpaZoneDto.PROPERTY_START_DATE, FpaZoneDto.PROPERTY_END_DATE},
              new String[]{FpaZoneUI.BINDING_START_DATE_DATE, FpaZoneUI.BINDING_END_DATE_DATE}
        );
    }

}
