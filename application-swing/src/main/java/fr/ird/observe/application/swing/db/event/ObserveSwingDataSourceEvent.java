/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.db.event;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;

import java.util.EventObject;

/**
 * Les évènements produits lors des changements du cycle de vie d'une source
 * de données.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ObserveSwingDataSourceEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    /** an extra message to be used when using {@link ObserveSwingDataSourceListener#onNewMessage(ObserveSwingDataSourceEvent)}. */
    protected String message;

    public enum MessageLevel {
        FATAL,
        ERROR,
        WARN,
        INFO,
        DEBUG,
        TRACE
    }

    protected MessageLevel messageLevel;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public ObserveSwingDataSourceEvent(ObserveSwingDataSource source) {
        super(source);
    }

    public ObserveSwingDataSourceEvent(ObserveSwingDataSource source,
                                       String message,
                                       MessageLevel messageLevel) {
        super(source);
        this.message = message;
        this.messageLevel = messageLevel;
    }

    @Override
    public ObserveSwingDataSource getSource() {
        return (ObserveSwingDataSource) super.getSource();
    }

    public String getMessage() {
        return message;
    }

    public MessageLevel getMessageLevel() {
        return messageLevel;
    }
}
