/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.export;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Le modèle d'une opération d'export de données observers.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ExportModel extends AdminActionModel {

    /** les index des marées à exporter. */
    protected int[] exportDataSelectedIndex;

    /** les ids des marées déjà existante sur la base distante lors d'un export */
    protected List<DataReference<?>> existingTrips;

    /** les données exportables */
    protected List<TripEntry> data;

    protected ReferentialReferenceDecorator<ProgramDto> programDecorator;

    protected DataReferenceDecorator<TripSeineDto> tripSeineDecorator;

    protected DataReferenceDecorator<TripLonglineDto> tripLonglineDecorator;

    protected ObserveSwingDataSource source;

    protected ObserveSwingDataSource centralSource;

    public ExportModel() {
        super(AdminStep.EXPORT_DATA);
    }

    public void setExportDataSelectedIndex(int... exportDataSelectedIndex) {
        this.exportDataSelectedIndex = exportDataSelectedIndex;
    }

    public List<DataReference<?>> getExistingTrips() {
        return existingTrips;
    }

    public void setExistingTrips(List<DataReference<?>> existingTrips) {
        this.existingTrips = existingTrips;
    }

    @Override
    public void destroy() {
        super.destroy();
        if (existingTrips != null) {
            existingTrips.clear();
            existingTrips = null;
        }
        exportDataSelectedIndex = null;
        tripSeineDecorator = null;
        data = null;
        centralSource = null;
        source = null;
    }

    public List<TripEntry> getData() {
        return data;
    }

    public void setData(DataSelectionModel selectionModel) {

        List<TripEntry> tripEntries = Lists.newArrayList();

        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference>> entry : selectionModel.getSelectedDataByProgram().entrySet()) {

            ReferentialReference<ProgramDto> program = entry.getKey();

            for (DataReference trip : entry.getValue()) {

                boolean exists = existingTrips.contains(trip);

                TripEntry tripEntry = new TripEntry(program, trip, exists);

                tripEntries.add(tripEntry);
            }
        }

        this.data = ImmutableList.copyOf(tripEntries);

    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    public ObserveSwingDataSource getCentralSource() {
        return centralSource;
    }

    public void setCentralSource(ObserveSwingDataSource centralSource) {
        this.centralSource = centralSource;
    }

    public void setProgramDecorator(ReferentialReferenceDecorator<ProgramDto> programDecorator) {
        this.programDecorator = programDecorator;
    }

    public void setTripSeineDecorator(DataReferenceDecorator<TripSeineDto> mareeDecorator) {
        this.tripSeineDecorator = mareeDecorator;
    }

    public void setTripLonglineDecorator(DataReferenceDecorator<TripLonglineDto> tripLonglineDecorator) {
        this.tripLonglineDecorator = tripLonglineDecorator;
    }

    public String decorate(AbstractReference referenceDto) {
        String decor = null;

        if (ProgramDto.class.isAssignableFrom(referenceDto.getType())) {
            decor = programDecorator.toString(referenceDto);
        } else if (TripSeineDto.class.isAssignableFrom(referenceDto.getType())) {
            decor = tripSeineDecorator.toString(referenceDto);
        } else if (TripLonglineDto.class.isAssignableFrom(referenceDto.getType())) {
            decor = tripLonglineDecorator.toString(referenceDto);
        }

        return decor;
    }

    public List<TripEntry> getSelectedTrips() {

        List<TripEntry> entries = new ArrayList<>();

        for (int index : exportDataSelectedIndex) {
            entries.add(data.get(index));
        }
        return entries;

    }

}
