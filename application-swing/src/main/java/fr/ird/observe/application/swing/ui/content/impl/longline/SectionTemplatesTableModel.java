package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.SectionTemplate;
import fr.ird.observe.application.swing.ui.util.table.EditableTableModelSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class SectionTemplatesTableModel extends EditableTableModelSupport<SectionTemplate> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(SectionTemplatesTableModel.class);

    public SectionTemplatesTableModel() {
        super(true);
    }

    @Override
    public boolean isRowNotEmpty(SectionTemplate row) {
        return !(StringUtils.isBlank(row.getId()) && StringUtils.isBlank(row.getFloatlineLengths()));
    }

    @Override
    public boolean isRowValid(SectionTemplate row) {
        return StringUtils.isNotBlank(row.getId()) &&
               StringUtils.isNotBlank(row.getFloatlineLengths()) &&
               row.isFloatlineLengthsValid();
    }

    @Override
    protected SectionTemplate createNewRow() {
        return new SectionTemplate();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SectionTemplate measure = data.get(rowIndex);
        Object result;

        switch (columnIndex) {
            case 0:

                result = measure.getId();
                break;

            case 1:

                result = measure.getFloatlineLengths();
                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

        return result;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        SectionTemplate measure = data.get(rowIndex);

        switch (columnIndex) {
            case 0:

                measure.setId((String) aValue);
                break;

            case 1:

                measure.setFloatlineLengths((String) aValue);
                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

        setModified(true);

    }

    @Override
    protected boolean computeValidState() {

        boolean newValidState = super.computeValidState();
        if (newValidState) {

            // check that we are using unique ids
            Set<String> ids = new HashSet<>();
            for (SectionTemplate sectionTemplate : data) {

                boolean add = ids.add(sectionTemplate.getId());
                if (!add) {

                    if (log.isWarnEnabled()) {
                        log.warn("Template identifiants (" + sectionTemplate.getId() + ") are not unique.");
                    }
                    newValidState = false;
                    break;
                }

            }

        }
        return newValidState;

    }

    @Override
    protected boolean isCanCreateNewRow(int rowIndex) {

        boolean canCreateNewRow = super.isCanCreateNewRow(rowIndex);
        if (canCreateNewRow) {

            // add a new row if and only if all rows are valid
            canCreateNewRow = computeValidState();
        }
        return canCreateNewRow;

    }
}
