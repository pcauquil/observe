/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.validate;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.dto.AbstractReference;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDto;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtos;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

public class ValidateEntityListCellRenderer extends DefaultListCellRenderer implements PropertyChangeListener {

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log = LogFactory.getLog(ValidateEntityListCellRenderer.class);

    protected final ValidateModel model;

    protected final Map<Object, String> renderCache;

    protected DecoratorService service;

    public ValidateEntityListCellRenderer(ValidateModel model) {
        this.model = model;
        renderCache = new HashMap<>();
        // on ecoute les modifications de messages sur le model
        this.model.addPropertyChangeListener(ValidateModel.PROPERTY_MESSAGES, this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        Map<?, ?> value = (Map<?, ?>) evt.getNewValue();
        if (log.isDebugEnabled()) {
            log.debug("messages changed : " +
                              (value == null ? 0 : value.size()) +
                              ", rebuild render cache");
        }
        synchronized (renderCache) {
            renderCache.clear();
            if (model.getMessages() == null) {
                return;
            }

            for (Class<?> klass : model.getMessageTypes()) {
                String type = t(ObserveI18nDecoratorHelper.getTypeI18nKey(klass));
                renderCache.put(klass, type);
            }
        }

    }

    public DecoratorService getService() {
        if (service == null) {
            service = ObserveSwingApplicationContext.get().getDecoratorService();
        }
        return service;
    }

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {
        if (value != null) {
            DecoratorService decoratorService = getService();
            if (value instanceof Class<?>) {
                value = renderCache.get(value);
            } else {
                value = valueFromRefDto(decoratorService, (AbstractReference) value);
            }
        }
        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }

    public Object valueFromRefDto(DecoratorService decoratorService, AbstractReference value) {
        String s = renderCache.get(value);
        if (s != null) {
            return s;
        }

        if (log.isDebugEnabled()) {
            log.debug("compute render cache for " + value.getId());
        }

        ValidateResultForDto validateResultForDto = model.getMessages(value);

        EnumMap<NuitonValidatorScope, Integer> scopes =
                ValidateResultForDtos.getScopesCount(validateResultForDto);

        StringBuilder buffer = new StringBuilder();

        Decorator<?> decorator;
        if (value instanceof ReferentialReference) {
            decorator = decoratorService.getReferentialReferenceDecorator(value.getType());
        } else {
            decorator = decoratorService.getDataReferenceDecorator(value.getType());
        }

        buffer.append(decorator.toString(value));
        buffer.append(" (");

        Iterator<NuitonValidatorScope> itr = scopes.keySet().iterator();
        while (itr.hasNext()) {
            NuitonValidatorScope scope = itr.next();
            int nb = scopes.get(scope);
            String t = t(scope.getLabel());
            buffer.append(t).append(" : ").append(nb);
            if (itr.hasNext()) {
                buffer.append(", ");
            }
        }
        buffer.append(")");
        s = buffer.toString();
        renderCache.put(value, s);
        return s;
    }

}

