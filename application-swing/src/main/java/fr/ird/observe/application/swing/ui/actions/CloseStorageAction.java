package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CloseStorageAction extends AbstractObserveAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(CloseStorageAction.class);

    private final ObserveMainUI ui;

    public CloseStorageAction(ObserveMainUI ui) {

        super(t("observe.action.close.storage"), SwingUtil.getUIManagerActionIcon("db-none"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.close.storage.tip"));
        putValue(MNEMONIC_KEY, (int) 'F');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        run();

    }

    public void run() {

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {
            ui.setBusy(true);

            try {
                ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
                if (log.isDebugEnabled()) {
                    log.debug(">>> close main storage " + source);
                }
                // on doit fermer le storage en cours d'utilisation
                source.close();
            } finally {
                ui.setBusy(false);
            }
        }

    }
}
