package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeServiceProduceSqlsRequest;
import fr.ird.observe.services.service.actions.synchro.referential.ng.task.ReferentialSynchronizeTaskType;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchronizeResources;

import javax.swing.Icon;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 03/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public abstract class ReferentialSynchronizeTaskSupport<R extends ReferentialDto> {

    protected final String dataStr;
    protected final String typeStr;
    protected final String i18nKey;
    private final ReferentialReference<R> referential;
    private final Icon icon;
    private final boolean left;
    private final ReferentialSynchronizeTaskType taskType;

    protected ReferentialSynchronizeTaskSupport(ReferentialSynchronizeResources resource,
                                                boolean left,
                                                ReferentialReference<R> referential) {
        this.left = left;
        this.referential = referential;
        this.icon = resource.getIcon(left);
        this.i18nKey = resource.getTaskLabel(left);
        this.taskType = resource.getTaskType();
        this.dataStr = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(referential.getType()).toString(referential);
        this.typeStr = t(ObserveI18nDecoratorHelper.getTypeI18nKey(referential.getType()));
    }

    public boolean isLeft() {
        return left;
    }

    public ReferentialSynchronizeTaskType getTaskType() {
        return taskType;
    }

    public String getLabel() {
        return t(i18nKey, typeStr, dataStr);
    }

    public String getStripLabel() {
        return getLabel().replaceAll("<[^>]+>", "");
    }

    public Class<R> getType() {
        return referential.getType();
    }

    public String getId() {
        return referential.getId();
    }

    public ReferentialReference<R> getReferential() {
        return referential;
    }

    public Icon getIcon() {
        return icon;
    }

    public void registerTask(ReferentialSynchronizeServiceProduceSqlsRequest.Builder builder) {
        builder.addTask(isLeft(), taskType, getType(), getId(), null);
    }

}
