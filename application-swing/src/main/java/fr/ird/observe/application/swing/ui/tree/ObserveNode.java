/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import fr.ird.observe.services.dto.OpenableDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import jaxx.runtime.swing.nav.tree.NavTreeNode;
import jaxx.runtime.swing.nav.tree.NavTreeNodeChildLoador;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Le modèle d'une noeud.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ObserveNode extends NavTreeNode<ObserveNode> {

    /** Logger */
    static private final Log log = LogFactory.getLog(ObserveNode.class);

    public static long count;

    private static final long serialVersionUID = 1L;

    /** un drapeau pour savoir si le noeud fait parti du referentiel */
    protected final boolean referentiel;

    /**
     * Un etat pour savoir si l'objet (de donnee) encapsule est ouvert.
     *
     * <b>Note:</b> Il faut que l'objet soit de type {@link OpenableDto}.
     */
    protected Boolean open;

    public ObserveNode(String id, boolean referentiel) {
        this(String.class, id, null, null, referentiel);
    }

    public ObserveNode(Class<?> internalClass,
                       String id,
                       boolean referentiel) {
        this(internalClass, id, null, null, referentiel);
    }

    public ObserveNode(Class<?> internalClass,
                       String id,
                       NavTreeNodeChildLoador<?, ?, ObserveNode> childLoador,
                       boolean referentiel) {
        this(internalClass, id, null, childLoador, referentiel);
    }

    public ObserveNode(Class<?> internalClass,
                       String id,
                       String context,
                       NavTreeNodeChildLoador<?, ?, ObserveNode> childLoador,
                       boolean referentiel) {
        super(internalClass, id, context, childLoador);
        this.referentiel = referentiel;
        count++;
        if (log.isDebugEnabled()) {
            log.debug("Creates a new node [" + count + "] " + this);
        }
    }

    public boolean isDataNode() {
        return !referentiel;
    }

    public boolean isReferentielNode() {
        return referentiel;
    }

    public Boolean isOpen() {
        return open;
    }

    public boolean isTripNode() {
        return isDataNode() &&
                (TripSeineDto.class.isAssignableFrom(internalClass)
                        || TripLonglineDto.class.isAssignableFrom(internalClass));
    }

    public boolean isRouteNode() {
        return isDataNode() && RouteDto.class.isAssignableFrom(internalClass);
    }

    public boolean isActivitySeineNode() {
        return isDataNode() && ActivitySeineDto.class.isAssignableFrom(internalClass);
    }

    public boolean isActivityLonglineNode() {
        return isDataNode() && ActivityLonglineDto.class.isAssignableFrom(internalClass);
    }
}
