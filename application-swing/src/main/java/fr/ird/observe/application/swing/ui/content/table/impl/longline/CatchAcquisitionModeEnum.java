package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/10/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public enum CatchAcquisitionModeEnum {

    INDIVIDUAL(n("observe.enum.fr.ird.observe.application.swing.ui.content.table.impl.longline.CatchAcquisitionModeEnum.INDIVIDUAL")),
    GROUPED(n("observe.enum.fr.ird.observe.application.swing.ui.content.table.impl.longline.CatchAcquisitionModeEnum.GROUPED"));

    private final String i18nKey;

    CatchAcquisitionModeEnum(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public static CatchAcquisitionModeEnum valueOf(int ordinal)
            throws IllegalArgumentException {
        for (CatchAcquisitionModeEnum o : values()) {
            if (o.ordinal() == ordinal) {
                return o;
            }
        }
        throw new IllegalArgumentException(
                "could not find a " + CatchAcquisitionModeEnum.class.getSimpleName() +
                " value for ordinal " + ordinal);
    }

    public String getI18nKey() {
        return i18nKey;
    }

    @Override
    public String toString() {
        return t(i18nKey);
    }
}
