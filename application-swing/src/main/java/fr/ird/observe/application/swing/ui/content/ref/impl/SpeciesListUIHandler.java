package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIHandler;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.event.TableModelListener;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 1/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class SpeciesListUIHandler extends ContentReferenceUIHandler<SpeciesListDto> {

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    public SpeciesListUIHandler(ContentReferenceUI<SpeciesListDto> ui) {
        super(ui);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
    }

    @Override
    public void initUI() {

        super.initUI();

        // To be sure always remove listener (could prevent some leaks)
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        getUi().getErrorTableModel().addTableModelListener(computeTabValidStateListener);

    }

    @Override
    public void selectBean(ReferentialReference<SpeciesListDto> selectedBean) {

        super.selectBean(selectedBean);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

        ((SpeciesListUI) getUi()).getTabPane().setSelectedIndex(0);

    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean generalTabValid = !errorProperties.removeAll(SpeciesListUIModel.GENERAL_TAB_PROPERTIES);
        boolean speciesTabValid = !errorProperties.removeAll(SpeciesListUIModel.SPECIES_TAB_PROPERTIES);

        SpeciesListUIModel model = (SpeciesListUIModel) getModel();
        model.setGeneralTabValid(generalTabValid);
        model.setSpeciesTabValid(speciesTabValid);

    }
}
