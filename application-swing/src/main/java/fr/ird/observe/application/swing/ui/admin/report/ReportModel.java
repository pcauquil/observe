/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.report;

import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.service.actions.report.ReportBuilder;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * Le modèle utilisé pour la fonctionnalité de génération de rapport.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ReportModel extends AdminActionModel {

    public static final String SELECTED_MAREE_PROPERTY_NAME = "selectedTrip";

    public static final String SELECTED_REPORT_PROPERTY_NAME = "selectedReport";

    public static final String REPORTS_PROPERTY_NAME = "reports";

    public static final String REPORT_FILE_PROPERTY_NAME = "reportFile";

    public static final String COPY_ROW_HEADERS_PROPERTY_NAME = "copyRowHeaders";

    public static final String COPY_COLUMN_HEADERS_PROPERTY_NAME = "copyColumnHeaders";

    public static final String AUTO_COPY_TO_CLIPBOARD_PROPERTY_NAME = "autoCopyToClipboard";

    public static final String VARIABLES_PROPERTY_NAME = "variables";

    public static final String VALID_PROPERTY_NAME = "valid";

    public static final Pattern REPORT_DEFINITION_PATTERN = Pattern.compile("report.(\\w+).name");

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReportModel.class);

    /** la fichier contenant la définition des reports. */
    protected File reportFile;

    /** la liste des reports connus. */
    protected List<Report> reports;

    /** le report actuellement sélectionné. */
    protected Report selectedReport;

    /** les variables utilisées pour le report. */
    protected final Map<String, Object> variables;

    /** le résultat du report actuellement sélectionné */
    protected ResultTableModel resultModel;

    /**
     * Un drapeau pour automatiquement copier dans le presse papier le
     * tableau des résultats.
     */
    protected boolean autoCopyToClipboard;

    /** Un drapeau pour savoir si la requete est executable. */
    protected Boolean valid;

    /** Pour copier les entêtes de lignes */
    protected boolean copyRowHeaders = true;

    /** Pour copier les entêtes de colonnes */
    protected boolean copyColumnHeaders = true;

    public ReportModel() {
        super(AdminStep.REPORT);
        variables = new TreeMap<>();
    }

    public List<Report> loadReports(URL resource) throws IOException {

        ReportBuilder builder = new ReportBuilder();
        try {

            return builder.load(resource);
        } finally {
            builder.clear();
        }
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(File reportFile) {
        Object old = this.reportFile;
        this.reportFile = reportFile;
        firePropertyChange(REPORT_FILE_PROPERTY_NAME, old, reportFile);
        reports = null;
        getReports();
    }

    public boolean isAutoCopyToClipboard() {
        return autoCopyToClipboard;
    }

    public void setAutoCopyToClipboard(boolean autoCopyToClipboard) {
        boolean oldValue = this.autoCopyToClipboard;
        this.autoCopyToClipboard = autoCopyToClipboard;
        firePropertyChange(AUTO_COPY_TO_CLIPBOARD_PROPERTY_NAME,
                           oldValue,
                           autoCopyToClipboard
        );
    }

    public boolean isCopyRowHeaders() {
        return copyRowHeaders;
    }

    public void setCopyRowHeaders(boolean copyRowHeaders) {
        boolean oldValue = this.copyRowHeaders;
        this.copyRowHeaders = copyRowHeaders;
        firePropertyChange(COPY_ROW_HEADERS_PROPERTY_NAME,
                           oldValue,
                           copyRowHeaders
        );
    }

    public boolean isCopyColumnHeaders() {
        return copyColumnHeaders;
    }

    public void setCopyColumnHeaders(boolean copyColumnHeaders) {
        boolean oldValue = this.copyColumnHeaders;
        this.copyColumnHeaders = copyColumnHeaders;
        firePropertyChange(COPY_COLUMN_HEADERS_PROPERTY_NAME,
                           oldValue,
                           copyColumnHeaders
        );
    }

    public ResultTableModel getResultModel() {
        if (resultModel == null) {
            resultModel = new ResultTableModel();
        }
        return resultModel;
    }

    public boolean isInit() {
        return reports != null;
    }

    public List<Report> getReports() {
        if (reports == null && reportFile.exists()) {


            try {
                URL resource = reportFile.toURI().toURL();
                List<Report> reports = loadReports(resource);
                if (log.isDebugEnabled()) {
                    log.debug("Add loaded " + reports.size() + " report(s).");
                }
                setReports(reports);
            } catch (IOException e) {
                throw new IllegalStateException(
                        "Could not load reports definition file (" + reportFile + ").", e);
            }
        }
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
        firePropertyChange(REPORTS_PROPERTY_NAME, reports);
    }

    public Report getSelectedReport() {
        return selectedReport;
    }

    public void setSelectedReport(Report selectedReport) {

        // suppression des variables
        variables.clear();

        // invalidation du modèle (sans déclanchement d'évènement)
        valid = null;

        // changement de report
        Object old = this.selectedReport;
        this.selectedReport = selectedReport;
        firePropertyChange(SELECTED_REPORT_PROPERTY_NAME, old, selectedReport);
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public boolean isValid() {
        return valid != null && valid;
    }

    public void setValid(boolean valid) {
        Boolean old = this.valid;
        this.valid = valid;
        firePropertyChange(VALID_PROPERTY_NAME, old, valid);
    }

    public void addVariable(String name, Object value) {
        variables.put(name, value);

        // invalidation du modèle (sans déclanchement d'évènement)
        valid = null;

        firePropertyChange(VARIABLES_PROPERTY_NAME, null, variables);
    }

    public void resetVariables() {
        variables.clear();
        firePropertyChange(VARIABLES_PROPERTY_NAME, null, variables);
    }
}
