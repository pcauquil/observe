/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage.tabs;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.ConnexionStatus;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import fr.ird.observe.application.swing.ui.storage.StorageUILauncher;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.application.swing.ui.tree.DataSelectionTreeSelectionModel;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import jaxx.runtime.swing.editor.MyDefaultCellEditor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Date;

import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur comun à tous les onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class StorageTabUIHandler {

    private static final Log log = LogFactory.getLog(StorageTabUIHandler.class);

    public boolean updateCreationModeLayout(ChooseDbModeUI ui,
                                            boolean visible,
                                            JRadioButton button) {
        JPanel panel = ui.getUseCreateMode();
        if (visible) {
            for (Component c : panel.getComponents()) {
                if (button.equals(c)) {
                    // button already in
                    return true;
                }
            }

            panel.add(button);
        } else {
            for (Component c : panel.getComponents()) {
                if (button.equals(c)) {
                    // button still in
                    panel.remove(c);
                    return false;
                }
            }
        }
        return visible;
    }

    public void initUI(final ChooseDbModeUI ui) {

        PropertyChangeListener listener = evt -> {
            StorageUIModel model = (StorageUIModel) evt.getSource();

            ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
            ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
            String txt = textGenerator.getLoadDataSourceResume(model);
            ui.getResume().setText(txt);
        };
        StorageUIModel model = ui.getModel();
        model.addPropertyChangeListener(StorageUIModel.DB_MODE_PROPERTY_NAME, listener);
        model.addPropertyChangeListener(StorageUIModel.CREATION_MODE_PROPERTY_NAME, listener);
        ui.setDescriptionText(t(ui.getStep().getDescription()));
    }

    public void initUI(ConfigUI ui) {

        PropertyChangeListener listener = evt -> {
            StorageUIModel model = (StorageUIModel) evt.getSource();
            String propertyName = evt.getPropertyName();
            if (StorageUIModel.CREATION_MODE_PROPERTY_NAME.equals(propertyName) ||
                    StorageUIModel.DB_MODE_PROPERTY_NAME.equals(propertyName)) {
                String id = null;
                if (model.getDbMode() == DbMode.USE_REMOTE) {
                    id = DbMode.USE_REMOTE.name();
                } else if (model.getDbMode() == DbMode.USE_SERVER) {
                    id = DbMode.USE_SERVER.name();
                } else {
                    if (model.getCreationMode() != null) {
                        id = model.getCreationMode().name();
                    }
                }
                if (id != null) {
                    refreshConfig(ui, id);
                }
            }
        };
        ui.getModel().addPropertyChangeListener(listener);

        ui.getActionMap().put("doChooseFile", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getFileChooserAction().doClick();
            }
        });
        KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK);
        ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(keyStroke, "doChooseFile");

        ui.getActionMap().put("doUseSsl", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getRemoteUseSsl().doClick();
            }
        });
        ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK), "doUseSsl");

        ui.getActionMap().put("doKeepConfiguration", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getStoreRemoteConfig().doClick();
            }
        });
        ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK), "doKeepConfiguration");

    }

    public void initUI(final ConfigReferentielUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));

            PropertyChangeListener listener = evt -> {
                String propertyName = evt.getPropertyName();
                if (StorageUIModel.REFERENTIEL_IMPORT_MODE_PROPERTY_NAME.equals(propertyName)) {
                    CreationMode id = (CreationMode) evt.getNewValue();
                    refreshConfig(ui, id.name());
                }
            };
            ui.getModel().addPropertyChangeListener(listener);
            ui.getModel().setReferentielImportMode(CreationMode.IMPORT_EXTERNAL_DUMP);

            ui.getActionMap().put("doConfigureRemoteSource", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getConfigureCentralSource().doClick();
                }
            });
            ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK), "doConfigureRemoteSource");
            ui.getActionMap().put("doConfigureRemoteServer", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getConfigureCentralSourceServer().doClick();
                }
            });
            ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK), "doConfigureRemoteServer");
        }
    }

    public void initUI(final ConfigDataUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));

            PropertyChangeListener listener = evt -> {
                String propertyName = evt.getPropertyName();
                if (StorageUIModel.DATA_IMPORT_MODE_PROPERTY_NAME.equals(propertyName)) {
                    CreationMode id = (CreationMode) evt.getNewValue();

                    if (id != null) {
                        refreshConfig(ui, id.name());

                    }

                }
            };
            ui.getModel().addPropertyChangeListener(listener);
            ui.getModel().setDataImportMode(CreationMode.EMPTY);
        }
    }

    public void initUI(RolesUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));
        }

        ui.getSecurityModel().addPropertyChangeListener(evt -> {
            if (log.isDebugEnabled()) {
                log.debug("Security model changed [" + evt.getPropertyName() + "] <" + evt.getOldValue() + " : " + evt.getNewValue() + ">");
            }
            ui.getModel().validate();
        });

        JTable table = ui.getRoles();
        table.setRowHeight(24);

        UIHelper.fixTableColumnWidth(table, 1, 100);
        UIHelper.fixTableColumnWidth(table, 2, 100);
        UIHelper.fixTableColumnWidth(table, 3, 100);
        UIHelper.fixTableColumnWidth(table, 4, 100);

        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                RolesTableModel.COLUMN_NAMES[0],
                RolesTableModel.COLUMN_NAME_TIPS[0],
                RolesTableModel.COLUMN_NAMES[1],
                RolesTableModel.COLUMN_NAME_TIPS[1],
                RolesTableModel.COLUMN_NAMES[2],
                RolesTableModel.COLUMN_NAME_TIPS[2],
                RolesTableModel.COLUMN_NAMES[3],
                RolesTableModel.COLUMN_NAME_TIPS[3],
                RolesTableModel.COLUMN_NAMES[4],
                RolesTableModel.COLUMN_NAME_TIPS[4]
        );

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newStringTableCellRenderer(renderer, 50, true));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newBooleanTableCellRenderer(renderer));

        UIHelper.setTableColumnEditor(table, 1, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 2, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 3, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 4, MyDefaultCellEditor.newBooleanEditor());

    }

    public void initUI(SelectDataUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));
        }

        // customize tree selection colors
        UIHelper.initUI(ui.getSelectedTreePane(), ui.getSelectTree());
    }

    public void initUI(BackupUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));

            ui.getActionMap().put("doBackup", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getDoBackup().doClick();
                }
            });
            ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK), "doBackup");
            ui.getActionMap().put("doChooseDirectory", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ui.getChooseFileAction().doClick();
                }
            });
            ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK), "doChooseDirectory");
        }
    }

    public void initUI(ConfirmUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));
        }
    }

    public void chooseBackupFile(BackupUI ui) {
        File f = UIHelper.chooseDirectory(
                ui,
                t("observe.title.choose.db.dump.directory"),
                t("observe.action.choose.db.dump.directory"),
                new File(ui.getDirectoryText().getText())
        );
        if (f != null) {
            changeDirectory(ui, f);
        }
    }

    public void changeDirectory(BackupUI ui, File f) {
        ui.getModel().setBackupFile(new File(f, ui.getFilenameText().getText()));
    }

    public void changeFilename(BackupUI ui, String filename) {
        ui.getModel().setBackupFile(new File(ui.getDirectoryText().getText(), filename));
    }

    public void refreshConfig(ConfigUI ui, String configId) {
        JComponent c = (JComponent) ui.getObjectById(configId);
        if (c != null) {
            if (log.isDebugEnabled()) {
                log.debug(configId);
            }
            ui.configLayout.show(ui.configContent, configId);
            String text = (String) c.getClientProperty("description");
            ui.setDescriptionText(t(text));
            if (c.equals(ui.IMPORT_REMOTE_STORAGE)) {
                ui.IMPORT_REMOTE_STORAGE_content.add(ui.remoteConfig, BorderLayout.CENTER);
            } else if (c.equals(ui.IMPORT_SERVER_STORAGE)) {
                ui.IMPORT_SERVER_STORAGE_content.add(ui.remoteConfig, BorderLayout.CENTER);
            } else if (c.equals(ui.USE_REMOTE)) {
                ui.USE_REMOTE_content.add(ui.remoteConfig, BorderLayout.CENTER);
            } else if (c.equals(ui.USE_SERVER)) {
                ui.USE_SERVER_content.add(ui.remoteConfig, BorderLayout.CENTER);
            }
        }
    }

    public void refreshConfig(ConfigReferentielUI ui, String configId) {
        JComponent c = (JComponent) ui.getObjectById(configId);
        if (c != null) {
            if (log.isDebugEnabled()) {
                log.debug(configId);
            }
            ui.configLayout.show(ui.configContent, configId);
        }
    }

    public void refreshConfig(ConfigDataUI ui, String configId) {
        JComponent c = (JComponent) ui.getObjectById(configId);
        if (c != null) {
            if (log.isDebugEnabled()) {
                log.debug(configId);
            }
            ui.configLayout.show(ui.configContent, configId);
        }
    }

    public Icon updateConnexionStatutIcon(ConfigUI ui, ConnexionStatus status) {
        return (Icon) ui.getConnexionStatus().getClientProperty(status.name().toLowerCase() + "Icon");
    }

    public Color updateConnexionStatutColor(ConfigUI ui, ConnexionStatus status) {
        return (Color) ui.getConnexionStatus().getClientProperty(status.name().toLowerCase() + "Color");
    }

    public String updateConnexionStatutText(ConfigUI ui, ConnexionStatus status) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
        return textGenerator.getConnexionTestResultMessage(ui.getModel());
    }

    public void chooseDumpFile(ConfigUI ui) {
        File f = UIHelper.chooseFile(
                ui,
                t("observe.title.choose.db.dump"),
                t("observe.action.choose.db.dump"),
                ui.getModel().getDumpFile(),
                "^.+\\.sql\\.gz$",
                t("observe.action.choose.db.dump.description"));
        ui.getModel().setDumpFile(f);
    }

    public void chooseDumpFile(ConfigReferentielUI ui) {
        File f = UIHelper.chooseFile(
                ui,
                t("observe.title.choose.db.dump"),
                t("observe.action.choose.db.dump"),
                ui.getModel().getDumpFile(),
                "^.+\\.sql\\.gz$",
                t("observe.action.choose.db.dump.description"));
        ui.getCentralSourceModel().setDumpFile(f);
        ui.getModel().validate();
    }

    public void chooseDumpFile(ConfigDataUI ui) {
        File f = UIHelper.chooseFile(
                ui,
                t("observe.title.choose.db.dump"),
                t("observe.action.choose.db.dump"),
                ui.getModel().getDumpFile(),
                "^.+\\.sql\\.gz$",
                t("observe.action.choose.db.dump.description"));
        ui.getCentralSourceModel().setDumpFile(f);
        ui.getModel().validate();
    }

//    public void chooseSslCertificatFile(ConfigUI ui) {
//        File f = UIHelper.chooseFile(
//                ui,
//                t("observe.title.choose.ssl.cert"),
//                t("observe.action.choose.ssl.cert"),
//                ui.getModel().getSslCertificatFile(),
//                "^.+\\.jks$",
//                t("observe.action.choose.ssl.cert.description"));
//        ui.getModel().setSslCertificatFile(f);
//    }

    public void initTree(SelectDataUI ui, ObserveSwingDataSource source) {
        StorageUIModel model = ui.getModel();
        if (model.isUseSelectData()) {

            DataSelectionModel dataModel = model.getSelectDataModel();
            ObserveTreeHelper helper = new ObserveTreeHelper();
            JTree tree = ui.selectTree;
            helper.setUI(tree, false, null);

            tree.setModel(helper.createModel(ui, dataModel, source));
            DataSelectionTreeSelectionModel selectionModel =
                    ui.getSelectionModel();
            selectionModel.initUI(tree);
            selectionModel.setDataModel(dataModel);
        }
    }

    protected String updateInternalDumpModeLabel(ChooseDbModeUI ui,
                                                 boolean dumpExist) {
        File f = ObserveSwingApplicationContext.get().getConfig().getInitialDbDump();
        String text;
        if (f.exists()) {
            text = t("observe.storage.internalDump.last.modified", new Date(f.lastModified()));
        } else {
            text = t("observe.storage.internalDump.not.exist");
        }
        return I18nEnumHelper.getLabel(CreationMode.IMPORT_INTERNAL_DUMP) + text;
    }

    protected String updateCanMigrateLabel(ChooseDbModeUI ui,
                                           boolean canMigrate) {
        return t("observe.storage.action.canMigrate", ui.getModel().getModelVersion());
    }

    public void obtainRemoteConnexion(ConfigReferentielUI ui) {
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainRemoteConnexion(
                ui.getDelegateContext(),
                ui.getParentContainer(Window.class),
                sourceModel
        );

        sourceModel.validate(StorageStep.CONFIG);

        ui.getModel().validate();
    }

    public void obtainServerConnexion(ConfigReferentielUI ui) {
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainServerConnexion(
                ui.getDelegateContext(),
                ui.getParentContainer(Window.class),
                sourceModel
        );

        sourceModel.validate(StorageStep.CONFIG);

        ui.getModel().validate();
    }

    public void obtainRemoteConnexion(ConfigDataUI ui) {
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainRemoteConnexion(
                ui.getDelegateContext(),
                ui.getParentContainer(Window.class),
                sourceModel
        );

        sourceModel.validate(StorageStep.CONFIG);

        ui.getModel().validate();
    }


    public void obtainServerConnexion(ConfigDataUI ui) {
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainServerConnexion(
                ui.getDelegateContext(),
                ui.getParentContainer(Window.class),
                sourceModel
        );

        sourceModel.validate(StorageStep.CONFIG);

        ui.getModel().validate();
    }

    public String updateStorageLabel(StorageUIModel service,
                                     boolean serviceValid,
                                     JLabel label,
                                     boolean remote) {
        String text;
        if (serviceValid && remote == service.isRemote()) {

            // on recupere le label du service
            text = service.getLabel();
        } else {

            // aucun service configuré
            text = t((String) label.getClientProperty("no"));
        }
        return text;
    }

    protected String updateDataSourcePolicy(StorageUIModel sourceModel,
                                            boolean valid,
                                            boolean remote) {
        String text = null;
        if (valid && remote == sourceModel.isRemote()) {
            ObserveDataSourceInformation dataSourceInformation = sourceModel.getDataSourceInformation();

            if (dataSourceInformation != null) {

                ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
                ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
                text = textGenerator.getDataSourcePolicy(dataSourceInformation);
            }

        } else {

            text = t("observe.common.storage.not.valid");

        }

        return text;
    }
}
