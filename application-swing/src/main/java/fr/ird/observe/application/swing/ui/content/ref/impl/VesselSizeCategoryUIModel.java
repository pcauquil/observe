package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.VesselSizeCategoryDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

/**
 * Created on 9/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class VesselSizeCategoryUIModel extends ContentReferenceUIModel<VesselSizeCategoryDto> {

    private static final long serialVersionUID = 1L;

    public VesselSizeCategoryUIModel() {
        super(VesselSizeCategoryDto.class,
              new String[]{
                      VesselSizeCategoryDto.PROPERTY_GAUGE_LABEL,
                      VesselSizeCategoryDto.PROPERTY_CAPACITY_LABEL
              },
              new String[]{
                      VesselSizeCategoryUI.BINDING_CAPACITY_LABEL_TEXT,
                      VesselSizeCategoryUI.BINDING_GAUGE_LABEL_TEXT}
        );
    }

}
