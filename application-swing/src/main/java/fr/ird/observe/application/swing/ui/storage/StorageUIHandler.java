/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveActionExecutor;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingApplicationDataSourcesManager;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.ConnexionStatus;
import fr.ird.observe.application.swing.db.event.ObserveSwingDataSourceEvent;
import fr.ird.observe.application.swing.db.event.ObserveSwingDataSourceListenerAdapter;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.storage.tabs.RolesTableModel;
import fr.ird.observe.application.swing.ui.storage.tabs.SecurityModel;
import fr.ird.observe.application.swing.ui.storage.tabs.StorageTabUI;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.SqlScriptProducerService;
import jaxx.runtime.context.DefaultApplicationContext.AutoLoad;
import jaxx.runtime.swing.wizard.WizardUILancher;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTabbedPane;
import java.awt.Component;
import java.awt.Window;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Le handler des actions sur base.
 *
 * On retrouve ici les méthodes pour importer, exporter, faire des backup,
 * synchroniser les bases.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
@AutoLoad
public class StorageUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(StorageUIHandler.class);

    /**
     * Prépare une service de persistance à partir d'un modèle.
     *
     * Le service ne sera pas ouvert.
     *
     * @param model le modèle de la source de données
     * @return le service de persistance initialisé mais non ouvert.
     */
    public ObserveSwingDataSource newDataSourceFromModel(
            StorageUIModel model) {

        ObserveSwingDataSource dataSource = null;
        ObserveDataSourceConfiguration configuration;
        switch (model.getDbMode()) {
            case CREATE_LOCAL:
            case USE_LOCAL:
                configuration = model.toH2StorageConfig(t("observe.storage.label.local"));

                dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(configuration);

                dataSource.addObserveSwingDataSourceListener(
                        new ObserveSwingDataSourceListenerAdapter() {

                            @Override
                            public void onOpened(ObserveSwingDataSourceEvent event) {
                                ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
                                // la base locale existe desormais
                                context.getConfig().setLocalStorageExist(true);
                            }
                        });

                break;
            case USE_REMOTE:
                configuration = model.toPGStorageConfig(t("observe.storage.label.remote"));
                dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(configuration);
                break;
            case USE_SERVER:
                configuration = model.toRestStorageConfig(t("observe.storage.label.server"));
                dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(configuration);
                break;
        }

        return dataSource;
    }

    public void initUI(final StorageUI ui) {

        StorageUIModel model = ui.getModel();

        // on écoute les changements d'étapes
        model.addPropertyChangeListener(StorageUIModel.STEP_PROPERTY_NAME, evt -> {
            StorageUIModel model1 = (StorageUIModel) evt.getSource();
            StorageStep oldStep = (StorageStep) evt.getOldValue();
            StorageStep newStep = (StorageStep) evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug("step has changed <old:" + oldStep + ", new:" + newStep + ">");
            }
            int oldStepIndex = oldStep == null ? -1 : model1.getStepIndex(oldStep);
            int newStepIndex = model1.getStepIndex(newStep);
            JTabbedPane tabs = ui.getTabs();
            if (oldStepIndex + 1 == newStepIndex) {

                // creation d'un nouvel onglet
                StorageTabUI c = (StorageTabUI) ui.getObjectById(newStep.name());
                String title = I18nEnumHelper.getLabel(newStep);
                String tip = I18nEnumHelper.getDescription(newStep);

                if (log.isDebugEnabled()) {
                    log.debug("Create tab " + title + " ui = " + c);
                }
                tabs.addTab(title, null, c, tip);
                tabs.setMnemonicAt(newStepIndex, title.charAt(0));

                // selection du nouvel onglet
                int index = tabs.indexOfComponent(c);
                if (index > -1) {
                    tabs.setSelectedIndex(index);
                }
                ui.onStepChanged(oldStep, newStep);

            } else if (oldStepIndex > newStepIndex) {

                // il s'agit d'un retour en arrière
                // on supprime tous les onglets obsoletes
                int index = newStepIndex + 1;
                while (tabs.getTabCount() > index) {
                    if (log.isDebugEnabled()) {
                        log.debug("remove tab : " + index);
                    }
                    tabs.remove(index);
                }

                ui.onStepChanged(oldStep, newStep);
            } else {
                throw new IllegalStateException("can not go from " + oldStep + " to " + newStep);
            }
        });

        // initialisation des onglets
        ui.CHOOSE_DB_MODE.init();
        ui.CONFIG.init();
        ui.CONFIG_REFERENTIEL.init();
        ui.CONFIG_DATA.init();
        ui.BACKUP.init();
        ui.SELECT_DATA.init();
        ui.ROLES.init();
        ui.CONFIRM.init();

        // recuperation de la source de données en cours d'utilisation
        ObserveDataSourceConfiguration dataSourceConfiguration = ui.getContextValue(ObserveDataSourceConfiguration.class);

        // chargement du modèle
        model.init(ui, dataSourceConfiguration);
    }

    public void start(StorageUI ui) {

        // on demarre le modele pour le type de base connu
        ui.getModel().start(ui.getModel().getDbMode());

        // centrage sur la frame parent
        UIHelper.center(ui.getContextValue(Window.class, "parent"), ui);

        // affichage ui
        ui.setVisible(true);
    }

    public void onStepChanged(StorageUI ui, StorageStep oldStep, StorageStep newStep) {
        if (newStep == null) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("new step : " + newStep);
        }
        StorageUIModel model = ui.getModel();

        boolean mustRecompute = oldStep == null || oldStep.ordinal() < newStep.ordinal();

        if (StorageStep.CONFIG == newStep) {

            model.updateEditConfigValues();

            if (mustRecompute) {

                // on redemande un test de connexion a chaque fois qu'on rentre dans l'onglet
                model.setConnexionStatus(ConnexionStatus.UNTESTED);
            }

        }

        if (StorageStep.CONFIG_REFERENTIEL == newStep && mustRecompute) {

            // mise à jour de l'univers des étapes (ajout/suppression étape confg_data et select_data possible)
            ui.getModel().updateUniverse();

        }

        if (StorageStep.CONFIG_DATA == newStep && mustRecompute) {

            // mise à jour de l'univers des étapes (ajout/suppression étape select_data possible)
            ui.getModel().updateUniverse();

        }

        if (StorageStep.SELECT_DATA == newStep && mustRecompute) {

            if (ObstunaAdminAction.CREATE == ui.getModel().getAdminAction()
                    && (model.getSelectDataModel() == null || model.getSelectDataModel().isEmpty())) {

                // récupération des données possibles à importer
                initSelectData(ui);

            }

        }

        if (StorageStep.ROLES == newStep && mustRecompute) {

            // récupération des rôles de la base cible
            updateSecurity(model, ui.getROLES().getRolesModel());

        }

        if (StorageStep.CONFIRM == newStep) {

            // generation du rapport
            String text = computeReport(ui, oldStep);
            ui.CONFIRM.getResume().setText(text);

        }

    }

    public void launchApply(final StorageUI ui) {

        ui.getModel().setAlreadyApplied(true);

        Runnable action = WizardUILancher.APPLY_DEF.getContextValue(ui);

        if (action == null) {
            final StorageUILauncher launcher = ui.getContextValue(StorageUILauncher.class);

            action = () -> {
                try {
                    launcher.doAction(ui);
                } finally {
                    launcher.doClose(ui, false);
                }
            };
        }
        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        if (mainUI == null) {
            if (log.isWarnEnabled()) {
                log.warn("Launch standalone apply action " + action);
            }
            //FIXME Action executor will not execute task until previous is finished...
            action.run();
        } else {
            ObserveActionExecutor executor = ObserveRunner.getActionExecutor();

            executor.addAction(t("observe.action.storage.applyAction"), action);
        }
    }

    public void launchCancel(StorageUI ui) {

        Runnable action = WizardUILancher.CANCEL_DEF.getContextValue(ui);

        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        if (mainUI == null) {
            if (log.isWarnEnabled()) {
                log.warn("Launch standalone cancel action " + action);
            }
            //FIXME Action executor will not execute task until previous is finished...
            if (action != null) {
                action.run();
            }
        } else {
            ObserveActionExecutor executor = ObserveRunner.getActionExecutor();
            executor.addAction(t("observe.storage.action.cancel"), action);
        }
    }

    /**
     * Utiliser le storage defini dans le modèle donné.
     *
     * @param model le model++ du storage a creer ou utiliser
     */
    public void doChangeStorage(StorageUIModel model) {
        ObserveSwingApplicationContext observeContext = ObserveSwingApplicationContext.get();

        ObserveSwingApplicationConfig config = observeContext.getConfig();

        // faut-il detruire la base locale ?
        boolean destroyLocalBase =
                config.isLocalStorageExist() &&
                        model.getDbMode() == DbMode.CREATE_LOCAL;

        if (log.isDebugEnabled()) {
            log.debug(">>> should destroy local db ? " + destroyLocalBase);
        }


        ObserveSwingApplicationDataSourcesManager dataSourcesManager = observeContext.getDataSourcesManager();
        ObserveSwingDataSource currentDataSource = dataSourcesManager.getMainDataSource();

        ObserveSwingDataSource localDataSource = null;

        if (currentDataSource != null && currentDataSource.isLocal()) {
            localDataSource = currentDataSource;
        }

        boolean localDbIsSane = true;
        if (destroyLocalBase || model.isDoBackup()) {
            if (localDataSource == null) {
                ObserveDataSourceConfigurationTopiaH2 localConfiguration = dataSourcesManager.newH2DataSourceConfiguration(config, t("observe.storage.label.local"));

                // la base ne doit pas etre mise a jour dans ce cas
                localConfiguration.setCanMigrate(false);

                // on charge un storage sur la base locale
                localDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(localConfiguration);
            }

            // Let's check if the datasource is opened or not :
            // we could have close the datasource through the ui
            // If the datasource is closed, we try to open it
            if (!localDataSource.isOpen()) {
                try {

                    localDataSource.open();

                } catch (Exception e) {
                    // on a pas reussi à ouvrir la base locale
                    // cela ne doit pas empécher de continuer
                    // il faut juste supprimer physiquement le repertoire
                    // de la base
                    UIHelper.handlingError(t("observe.error.storage.could.not.load.local.db", e.getMessage()), e);

                    // on conserve l'état
                    localDbIsSane = false;

                    // pour la suite on fait comme si il n'y a pas de local storage
                    localDataSource = null;
                }
            }
        }

        if (model.isDoBackup()) {
            if (!localDbIsSane) {

                // la base locale n'est pas saine, on doit arrêter l'objectOperation
                // de changement de base sous peine de perdre la base.
                Exception e = new Exception(t("observe.error.storage.could.not.backup.unsane.local.db"));
                UIHelper.handlingError(e);
                return;
            }
            // effectue la backup de la base locale existante
            File f = model.getBackupFile();
            if (log.isDebugEnabled()) {
                log.debug(">>> do backup with " + localDataSource + " in " + f);
            }
            try {
                SqlScriptProducerService dumpProducerService = localDataSource.newSqlScriptProducerService();
                backupLocalDatabase(dumpProducerService, f);
            } catch (Exception e) {
                UIHelper.handlingError(e);
                return;
            }
        }

        if (destroyLocalBase) {
            if (log.isDebugEnabled()) {
                log.debug(">>> destroy local db " + localDataSource);
            }
            if (!localDbIsSane) {
                // la base locale n'est pas saine, on va supprimer directement
                // le dossier de la base
                File localDBDirectory = config.getLocalDBDirectory();
                if (log.isInfoEnabled()) {
                    log.info(">>> destroy local db directory " + localDBDirectory);
                }
                try {
                    FileUtils.deleteDirectory(localDBDirectory);
                } catch (IOException e) {
                    UIHelper.handlingError(e);
                }
            } else {
                try {
                    localDataSource.destroy();
                } catch (Exception e) {
                    UIHelper.handlingError(e);
                    return;
                }
            }
        }

        // suppression du storage precedent
        if (currentDataSource != null && currentDataSource.isOpen()) {
            if (log.isDebugEnabled()) {
                log.debug(">>> close main storage " + currentDataSource);
            }
            // on doit fermer le storage en cours d'utilisation
            currentDataSource.close();
        }

        // suppression du storage local
        if (localDataSource != null
                && localDataSource.isOpen() // Si la base a été détruite précédemment, elle n'est plus ouverte
                && localDataSource != currentDataSource) {
            // ce cas peut arriver lorsque l'on fait juste une backup
            // sans vouloir supprimer la base locale
            if (log.isDebugEnabled()) {
                log.debug(">>> close local storage " + localDataSource);
            }
            // on doit fermer le storage local ouvert
            localDataSource.close();
        }

        localDataSource = null;

        if (log.isDebugEnabled()) {
            log.debug("Will create new storage...");
        }

        // preparation du nouveau storage

        try {

            currentDataSource = newDataSourceFromModel(model);

            // si on utilise la base local on lance une migration de la base si necessaire
            if (DbMode.USE_LOCAL.equals((model.getDbMode()))) {

                ObserveDataSourceInformation dataSourceInformation = currentDataSource.checkCanConnect();

                currentDataSource.migrateData(dataSourceInformation, config.getModelVersion());

            }

            dataSourcesManager.setMainDataSource(currentDataSource);
            observeContext.getDataSourcesManager().prepareMainStorage(currentDataSource, true);

            if (model.getDbMode() == DbMode.CREATE_LOCAL) {

                DataSourceCreateConfigurationDto creationConfigurationDto = model.getCreationConfigurationDto();

                try {

                    currentDataSource.create(creationConfigurationDto);

                } catch (Exception e) {
                    //si il y a une erreur lor de la création de la base locla

                    // on suprimer la base
                    File localDBDirectory = config.getLocalDBDirectory();
                    if (log.isInfoEnabled()) {
                        log.info(">>> destroy local db directory " + localDBDirectory);
                    }
                    try {
                        FileUtils.deleteDirectory(localDBDirectory);
                    } catch (IOException e2) {
                        UIHelper.handlingError(e2);
                    }

                    config.setLocalStorageExist(false);
                    dataSourcesManager.setMainDataSource(null);

                    throw e;
                }
            } else {
                // ouverture du nouveau storage
                currentDataSource.open();
            }

            if (DbMode.CREATE_LOCAL.equals(model.getDbMode())
                    && (CreationMode.IMPORT_REMOTE_STORAGE.equals(model.getCreationMode()) || CreationMode.IMPORT_SERVER_STORAGE.equals(model.getCreationMode()))
                    && config.isLocalStorageExist()) {
                // si on a creer la base locale a partir d'un import d'une base
                // distante, on sauvegarde la base locale comme dump initial
                // il s'agit d'un dump du référentiel
                File f = config.getInitialDbDump();
                if (f.exists()) {
                    // on supprime le dump sql de la base embarquée
                    if (!f.delete()) {
                        throw new IllegalStateException("could not delete " + f);
                    }
                }
                if (log.isInfoEnabled()) {
                    log.info(">>> create initial dump with " + localDataSource + " in " + f);
                }
                try {
                    SqlScriptProducerService dumpProducerService = observeContext.getMainDataSourceServicesProvider().newSqlScriptProducerService();
                    backupLocalDatabase(dumpProducerService, f);
                    config.setInitialDumpExist(true);
                } catch (Exception e) {
                    UIHelper.handlingError(e);
                }
            }

            if (log.isInfoEnabled()) {
                log.info(">>> main storage opened " + currentDataSource.getLabel());
            }

            if (model.isStoreRemoteConfig()) {
                storeRemoteConfig(model);
            }

        } catch (Exception ex) {
            UIHelper.handlingError(ex);
            throw new RuntimeException(ex);
        }
    }

    /**
     * Sauvegarder dans la configuration de l'application le paramétrage de la
     * source de données distante donnée.
     *
     * @param model le model de la source de données
     */
    public void storeRemoteConfig(StorageUIModel model) {
        ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();
        // on sauvegarde dans le parametrage dans la configuration de
        // l'application

        if (model.isEditRemoteConfig()) {

            ObserveDataSourceConfigurationTopiaPG configurationTopiaPG = model.toPGStorageConfig("");

            config.setObstunaUrl(configurationTopiaPG.getJdbcUrl());
            config.setObstunaLogin(configurationTopiaPG.getUsername());
            config.setObstunaPassword(configurationTopiaPG.getPassword());
            config.setObstunaUseSsl(configurationTopiaPG.isUseSsl());
            config.setShowMigrationProgression(configurationTopiaPG.isShowMigrationProgression());
            config.setShowMigrationSql(configurationTopiaPG.isShowMigrationSql());

            //TODO-TC20100311 : on devrait pas regarder si l'utilisateur veut sauver
            //TODO-TC20100311 : la configuration ?
            config.saveForUser();

        } else if (model.isEditServerConfig()) {

            ObserveDataSourceConfigurationRest configurationRest = model.toRestStorageConfig("");

            config.setServerUrl(configurationRest.getServerUrl());
            config.setServerLogin(configurationRest.getLogin());
            config.setServerPassword(configurationRest.getPassword());
            config.setServerDataBaseName(configurationRest.getOptionalDatabaseName().orElse(null));

            //TODO-TC20100311 : on devrait pas regarder si l'utilisateur veut sauver
            //TODO-TC20100311 : la configuration ?

            config.saveForUser();

        }
    }

    /**
     * @return le lastName par defaut du fichier de sauvegarde de la base locale
     * (expression calculée à partir de la date courante et du pattern
     * {@link ObserveSwingApplicationConfig#BACKUP_DB_PATTERN}).
     */
    public String getDefaultBackupFilename() {
        return String.format(ObserveSwingApplicationConfig.BACKUP_DB_PATTERN, new Date());
    }

    /**
     * Effectue une sauvegarde de la base locale vers le fichier choisi.
     *
     * @param dumpProducerService le service de dump
     * @param dst                 le fichier de sauvegarde
     */
    public void backupLocalDatabase(SqlScriptProducerService dumpProducerService, File dst) {
        if (dst == null) {
            throw new IllegalArgumentException(
                    "file where to backup can not be null");
        }
        if (log.isDebugEnabled()) {
            log.debug(dst);
        }

        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData();
        byte[] dataDump = dumpProducerService.produceAddSqlScript(request).getSqlCode();

        try (FileOutputStream outputStream = new FileOutputStream(dst)) {

            outputStream.write(dataDump);
        } catch (Exception e) {
            UIHelper.handlingError(e);
        }
    }

    /**
     * Effectue une sauvegarde d'un service de persitances vers le fichier
     * choisi.
     *
     * On peut spécifier les donnéees à exporter via le paramètre {@code
     * marees}.
     *
     * <b>Note:</b> Si ce paramètre vaut {@code null}, on export tout.
     *
     * @param dataSource la source de donnée qui encapsule la base locale
     * @param dst        le fichier de sauvegarde
     * @param trips      les marees a exporter (si {@code null} on exporte tout)
     */
    public void backupLocalDatabase(ObserveSwingDataSource dataSource,
                                    File dst,
                                    Set<DataReference> trips) {

        SqlScriptProducerService dumpService = dataSource.newSqlScriptProducerService();


        if (trips == null) {
            if (log.isInfoEnabled()) {
                log.info("will export all datas.");
            }
            backupLocalDatabase(dumpService, dst);
            return;
        }
        if (dst == null) {
            throw new IllegalArgumentException(
                    "file where to backup can not be null");
        }
        if (log.isInfoEnabled()) {
            log.info("will export " + trips.size() + " marees to " + dst);
        }
        if (log.isDebugEnabled()) {
            log.debug(trips);
        }

        // on doit dumper la base distante dans une base h2 et en faire
        // la sauvegarde

        ImmutableSet<String> tripIds = ImmutableSet.copyOf(trips.stream().map(DataReference.ID_FUNCTION).collect(Collectors.toSet()));

        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().dataIdsToAdd(tripIds);
        byte[] dump = dumpService.produceAddSqlScript(request).getSqlCode();

        try (FileOutputStream fileOutputStream = new FileOutputStream(dst)) {

            IOUtils.write(dump, fileOutputStream);

        } catch (IOException e) {
            UIHelper.handlingError(e);
        }
    }

    public String computeReport(StorageUI ui, StorageStep step) {
        if (log.isDebugEnabled()) {
            log.debug("Build report from step " + step);
        }
        StorageUIModel model = ui.getModel();
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
        return textGenerator.getDataSourceConnectionReport(model);
    }

    public void destroy(StorageUI ui) {
        ui.getModel().destroy();
        if (log.isDebugEnabled()) {
            log.debug("destroy ui " + ui.getName());
        }
        UIHelper.TabbedPaneIterator<Component> itr =
                UIHelper.newTabbedPaneIterator(ui.getTabs());
        for (; itr.hasNext(); ) {
            StorageTabUI tab =
                    (StorageTabUI) itr.next();
            if (log.isDebugEnabled()) {
                log.debug("destroy ui " + tab.getName());
            }
            tab.destroy();
        }
        UIHelper.destroy(ui);
    }

    protected DecoratorService getDecoratorService() {
        return ObserveSwingApplicationContext.get().getDecoratorService();
    }

    public void initSelectData(StorageUI ui, ObserveSwingDataSource source, boolean selectAll) {

        StorageUIModel model = ui.getModel();

        try {

            Preconditions.checkState(source != null, "Can't select data on a null dataSource");

            DataSelectionModel dataModel = new DataSelectionModel();
            dataModel.setUseData(true);
            dataModel.setUseOpenData(true);
            dataModel.setUseReferentiel(false);

            DataSelectionModel.populate(dataModel, source);

            if (selectAll) {

                dataModel.addAllSelectedData();

            }

            // positionnement du model de selection de données
            // dans le model du wizard
            model.setSelectDataModel(dataModel);

            // initialisation de l'ui dedié
            ui.getSELECT_DATA().initTree(source);

        } catch (Exception e) {
            throw new RuntimeException("Could not grab data to select", e);
        }

    }

    protected void initSelectData(StorageUI ui) {

        StorageUIModel model = ui.getModel();

        try {

            // Creation de la data source de lecture des données à sélectionner
            ObserveSwingDataSource importDataSource = model.toImportDataSourceConfig();

            Preconditions.checkState(importDataSource != null, "Can't select data on a null dataSource");

            try {

                importDataSource.open();

                model.checkImportDbVersion(importDataSource);

                initSelectData(ui, importDataSource, false);

            } finally {
                importDataSource.close();
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(e, e);
            }
        }
    }

    protected void updateSecurity(StorageUIModel model, RolesTableModel roleModel) {

        SecurityModel security = model.getSecurityModel();

        ObserveSwingDataSource dataSource = null;

        switch (model.getDbMode()) {
            case USE_REMOTE:
                ObserveDataSourceConfigurationTopiaPG pgConfig = model.getPgConfig();

                dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(pgConfig);
                break;
            case USE_SERVER:
                ObserveDataSourceConfigurationRest restConfig = model.getRestConfig();
                dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(restConfig);

        }

        if (dataSource != null) {
            try {

                Set<ObserveDbUserDto> users = dataSource.getUsers();

                if (log.isInfoEnabled()) {
                    log.info("Db roles : " + users);
                }

                security.init(users);
                roleModel.init(security);

            } catch (Exception e) {
                throw new RuntimeException("Could not obtain db roles", e);
            } finally {
                if (dataSource.isOpen()) {
                    dataSource.close();
                }
            }
        }

    }

}
