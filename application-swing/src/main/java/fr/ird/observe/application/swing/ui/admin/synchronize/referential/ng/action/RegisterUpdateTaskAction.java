package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchronizeResources;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.UpdateReferentialSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferenceReferentialSynchroNodeSupport;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class RegisterUpdateTaskAction extends RegisterTasksActionSupport {

    private static final long serialVersionUID = 1L;

    private Collection<ReferenceReferentialSynchroNodeSupport> nodes;

    public RegisterUpdateTaskAction(ReferentialSynchroUI ui, boolean left) {
        super(ui, ReferentialSynchronizeResources.UPDATE, left, false);
    }

    @Override
    protected <R extends ReferentialDto> UpdateReferentialSynchronizeTask<R> createTask(boolean left, ReferentialReference<R> reference, ReferentialReference<R> replaceReference) {
        return new UpdateReferentialSynchronizeTask<>(left, reference);
    }

    @Override
    protected Collection<ReferenceReferentialSynchroNodeSupport> getReferenceReferentialSynchroNodes(Predicate<ReferenceReferentialSynchroNodeSupport> predicate) {
        return nodes;
    }

    public void updateNodes() {
        this.nodes = super.getReferenceReferentialSynchroNodes(predicate);
    }

}
