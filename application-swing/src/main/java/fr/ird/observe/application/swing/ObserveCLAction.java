/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing;

import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationActionDefinition;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption;
import fr.ird.observe.application.swing.ui.ObserveMainUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUILauncher;
import fr.ird.observe.application.swing.ui.storage.ObstunaAdminAction;
import fr.ird.observe.application.swing.ui.storage.RemoteUILauncher;
import jaxx.runtime.swing.application.ActionWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Console;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;

import static org.nuiton.i18n.I18n.t;

/**
 * Les actions appellables via {@link ObserveRunner}.
 *
 * Consulter la classe {@link ObserveSwingApplicationActionDefinition} pour connaitre les actions
 * possibles.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see ObserveSwingApplicationConfig
 * @see ObserveRunner
 * @since 1.0
 */
public class ObserveCLAction {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveCLAction.class);

    /** La configuration de l'application. */
    protected ObserveSwingApplicationConfig config;

    /** Désactiver la possiblite de lancer l'ui principale. */
    public void disableMainUI() {
        if (log.isDebugEnabled()) {
            log.debug(this);
        }
        getConfig().setDisplayMainUI(false);
    }

    @SuppressWarnings("unused")
    public void help() {
        disableMainUI();

        StringBuilder out = new StringBuilder();

        out.append(t("observe.message.help.usage", getConfig().getVersion()));
        out.append('\n');
        out.append("Options (set with --option <key> <value>:");
        out.append('\n');
        for (ObserveSwingApplicationConfigOption o : ObserveSwingApplicationConfigOption.values()) {

            out.append("\t");
            out.append(o.getKey());
            out.append("(");
            out.append(o.getDefaultValue());
            out.append(") :");
            out.append(t(o.getDescription()));
            out.append('\n');
        }

        out.append("Actions:");
        out.append('\n');
        for (ObserveSwingApplicationActionDefinition a : ObserveSwingApplicationActionDefinition.values()) {
            out.append("\t");
            out.append(Arrays.toString(a.aliases));
            out.append("(");
            out.append(a.action);
            out.append("):");
            out.append(t(a.description));
            out.append('\n');
        }
        Console cons;

        if ((cons = System.console()) != null) {
            cons.printf(out.toString());
        }
    }

    public void configure() throws InterruptedException {
        disableMainUI();
        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

        ObserveMainUIHandler handler = context.getContextValue(ObserveMainUIHandler.class);

        Runnable runnable = createRunnable(handler, "showConfig", context);

        launchAction(t("observe.action.showConfig.title"), runnable);
    }

    @SuppressWarnings("unused")
    public void launchAdminUI(String operationName) throws InterruptedException {

        disableMainUI();

        EnumSet<AdminStep> operations = AdminStep.getOperations();

        AdminStep operation = AdminStep.valueOfIgnoreCase(operationName);
        if (operation == null) {
            if (log.isErrorEnabled()) {
                log.error(operationName + " is not a known admin operation.");
                log.error("Use one of these ones : " + operations);
            }
            return;
        }

        if (!operation.isOperation()) {
            if (log.isErrorEnabled()) {
                log.error(operation + " is not a admin operation(just a step in wizard).");
                log.error("Use one of these ones : " + operations);
            }
            return;
        }

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

        AdminUILauncher launcher = AdminUILauncher.newLauncher(context, operation);

        Runnable runnable = createRunnable(launcher, "start");

        launchAction(t(operation.getTitle()), runnable);
    }

    @SuppressWarnings("unused")
    public void launchObstunaAdminUI(String operationName) throws InterruptedException {

        disableMainUI();

        EnumSet<ObstunaAdminAction> operations = EnumSet.allOf(ObstunaAdminAction.class);

        ObstunaAdminAction operation = ObstunaAdminAction.valueOfIgnoreCase(operationName);
        if (operation == null) {
            if (log.isErrorEnabled()) {
                log.error(operationName + " is not a known obstuna admin operation.");
                log.error("Use one of these ones : " + operations);
            }
            return;
        }

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

        // FIXME
//        getConfig().setOption(AbstractDataSourceMigration.AUTO_MIGRATE, "false");

        if (operation == ObstunaAdminAction.UPDATE ||
            operation == ObstunaAdminAction.CREATE) {
            getConfig().setOption(ObserveSwingApplicationConfigOption.OBSTUNA_CAN_MIGRATE, true);
        }

        RemoteUILauncher launcher = operation.newLauncher(context, null);

        Runnable runnable = createRunnable(launcher, "start");
        launchAction(t(launcher.getTitle()), runnable);
    }

    public void launchH2ServerMode() throws InterruptedException {

        if (!config.isLocalStorageExist()) {
            if (log.isErrorEnabled()) {
                log.error("Local database does not exist.");
            }
        } else {

            ObserveSwingApplicationContext.get().setContextValue(
                    true,
                    ObserveSwingApplicationActionDefinition.H2_SERVER_MODE.name());
        }
    }

    public void createId(String className, int nbId) throws IOException {

        disableMainUI();

//        Class<?> klazz = null;

        //FIXME
//        List<ObserveEntityEnum> enums = Lists.newArrayList(Entities.ALL_ENTITIES);
//        enums.remove(ObserveEntityEnum.CommentableEntity);
//        enums.remove(ObserveEntityEnum.OpenableEntity);
//        enums.remove(ObserveEntityEnum.I18nReferenceEntity);
//        enums.remove(ObserveEntityEnum.ReferenceEntity);
//
//        for (ObserveEntityEnum e : enums) {
//            if (className.equals(e.name())) {
//                klazz = e.getContract();
//                break;
//            }
//        }
//
//        if (klazz == null) {
//
//            Collections.sort(enums, new Comparator<ObserveEntityEnum>() {
//                @Override
//                public int compare(ObserveEntityEnum observeEntityEnum, ObserveEntityEnum observeEntityEnum2) {
//                    return observeEntityEnum.name().compareTo(observeEntityEnum2.name());
//                }
//            });
//            if (log.isErrorEnabled()) {
//                log.error(className + " not found! availables names :\n\t" + Joiner.on("\n\t").join(enums));
//            }
//            return;
//        }
//
//        for (int i = 0; i < nbId; i++) {
//            String topiaId = TopiaId.create(klazz);
//            System.out.println(topiaId);
//        }
    }

    protected ObserveSwingApplicationConfig getConfig() {
        if (config == null) {
            config = ObserveSwingApplicationContext.get().getConfig();
        }
        return config;
    }

    protected Runnable createRunnable(Object invoker, String method, Object... args) {
        ObserveActionExecutor executor = ObserveRunner.getActionExecutor();

        return executor.createRunnable(invoker, method, args);
    }

    protected void launchAction(String title, Runnable target) throws InterruptedException {
        ObserveActionExecutor executor = ObserveRunner.getActionExecutor();

        CommandLineActionWorker action = new CommandLineActionWorker(title, target);
        executor.addAction(action);

        // on attends la fin de l'opération
        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
        if (log.isDebugEnabled()) {
            log.debug("Lock main context " + context);
        }
        context.lock();
    }

    /**
     * Un worker pour les opération longues d'administration.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 1.4
     */
    public class CommandLineActionWorker extends ActionWorker<Void, String> {

        public CommandLineActionWorker(String actionLabel, Runnable target) {
            super(actionLabel);
            setTarget(target);
        }

        public ObserveCLAction getAction() {
            return ObserveCLAction.this;
        }
    }
}
