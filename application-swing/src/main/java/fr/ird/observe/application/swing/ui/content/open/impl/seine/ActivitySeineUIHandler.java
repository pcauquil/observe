/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.open.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDtos;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.seine.ActivitySeineService;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUIHandler;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.event.TableModelListener;
import java.util.HashSet;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
class ActivitySeineUIHandler extends ContentOpenableUIHandler<ActivitySeineDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ActivitySeineUIHandler.class);

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

//    static ImmutableSet<String> COORDINATES_PROPERTIES = ImmutableSet.of(ActivitySeine.PROPERTY_QUADRANT,
//                                                                         ActivitySeine.PROPERTY_LATITUDE,
//                                                                         ActivitySeine.PROPERTY_LONGITUDE);

//    private LogPropertyChanges logCoordinatesChanges;


    ActivitySeineUIHandler(ActivitySeineUI ui) {
        super(ui,
              DataContextType.Route,
              DataContextType.ActivitySeine,
              n("observe.storage.activitySeine.message.not.open"));
//        this.logCoordinatesChanges = new LogPropertyChanges(COORDINATES_PROPERTIES);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
    }

    @Override
    public ActivitySeineUI getUi() {
        return (ActivitySeineUI) super.getUi();
    }

    @Override
    protected boolean doOpenData() {
        boolean result = getOpenDataManager().canOpenActivitySeine(getSelectedParentId());
        if (result) {
            getOpenDataManager().openActivitySeine(getSelectedParentId(), getSelectedId());
        }
        return result;
    }

    @Override
    public boolean doCloseData() {
        boolean result = getOpenDataManager().isOpenActivitySeine(getSelectedId());
        if (result) {
            getOpenDataManager().closeActivitySeine(getSelectedId());
        }
        return result;
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String activityId = getSelectedId();

        if (activityId == null) {

            // mode creation
            return ContentMode.CREATE;
        }

        // l'activity existe en base
        if (getOpenDataManager().isOpenActivitySeine(getSelectedId())) {

            // l'activity est ouverte, donc modifiable
            return ContentMode.UPDATE;
        }

        ActivitySeineUI ui = getUi();

        // l'activity n'est pas ouverte, donc pas éditable
        if (!getOpenDataManager().isOpenRoute(getSelectedParentId())) {

            // la route n'est pas ouverte
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(RouteDto.class),
                       t("observe.content.route.message.not.open"));

        } else if (!getOpenDataManager().isOpenTripSeine(dataContext.getSelectedTripSeineId())) {

            // la marée n'est past ouverte
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(TripSeineDto.class),
                       t("observe.content.tripSeine.message.not.open"));

            if (getModel().isHistoricalData()) {

                addInfoMessage(t("observe.message.historical.data"));
            }

        } else {

            // seule l'activity n'est pas ouverte
            addInfoMessage(t(closeMessage));
        }

        return ContentMode.READ;
    }

    @Override
    public void initUI() {

        super.initUI();

        // To be sure always remove listener (could prevent some leaks)
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        getUi().getErrorTableModel().addTableModelListener(computeTabValidStateListener);

//        getBean().removePropertyChangeListener(logCoordinatesChanges);
//        getBean().addPropertyChangeListener(logCoordinatesChanges);
    }

    @Override
    public void openUI() {
        super.openUI();

        getUi().getCoordinatesEditor().resetModel();

        String routeId = getSelectedParentId();
        String activityId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info(prefix + "routeId    = " + routeId);
            log.info(prefix + "activityId = " + activityId);
        }

        ContentMode mode = computeContentMode();
        if (log.isInfoEnabled()) {
            log.info(prefix + "content mode " + mode);
        }
        ActivitySeineDto bean = getBean();

        boolean create = activityId == null;

        Form<ActivitySeineDto> form;
        if (create) {

            // create mode
            form = getActivitySeineService().preCreate(routeId);

        } else {

            // update mode
            form = getActivitySeineService().loadForm(activityId);

        }

        setContentMode(mode);

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        ActivitySeineDtos.copyActivitySeineDto(form.getObject(), bean);

        if (log.isDebugEnabled()) {
            log.debug(prefix + "long  - lat  = " + bean.getLongitude() + " - " + bean.getLatitude());
        }

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

        finalizeOpenUI(mode, create);

        // Mise à jour du composant de coordonnées
        // 1. Mise à jour latitude/longitude:
        getUi().getCoordinatesEditor().setLatitudeAndLongitude(bean.getLatitude(), bean.getLongitude());

        // 2. Mise à jour du quadrant :
        // Si le bean de données contient un quadrant, on met simplement à jour le composant de coordonnées pour sélectionner le quadrant voulu
        // sinon, on réinitialise les quadrants du composant afin qu'aucun d'eux ne soit sélectionné (par exemple dans le cas de la création de la première activité d'une route)
        if (bean.getQuadrant() != null) {
            getUi().getCoordinatesEditor().setQuadrant(bean.getQuadrant());
        } else {
            resetQuadrant(getUi().getCoordinatesEditor());
        }

        // on annule la modification engendree par ce binding
        getModel().setModified(create);
    }

    @Override
    public void startEditUI(String... binding) {
        ActivitySeineUI ui = getUi();

        ContentUIModel<ActivitySeineDto> model = getModel();

        boolean create = model.getMode() == ContentMode.CREATE;
        String contextName = getValidatorContextName(model.getMode());
        ui.getValidator().setContext(contextName);
        if (create) {
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivitySeineDto.class),
                       t("observe.storage.activitySeine.message.creating"));
        } else {
            addMessage(ui,
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivitySeineDto.class),
                       t("observe.storage.activitySeine.message.updating"));
        }

        super.startEditUI(ActivitySeineUI.BINDING_TIME_TIME,
                          ActivitySeineUI.BINDING_VESSEL_SPEED_MODEL,
                          ActivitySeineUI.BINDING_SEA_SURFACE_TEMPERATURE_MODEL,
                          ActivitySeineUI.BINDING_COMMENT2_TEXT,
                          ActivitySeineUI.BINDING_VESSEL_ACTIVITY_SEINE_SELECTED_ITEM,
                          ActivitySeineUI.BINDING_PREVIOUS_FPA_ZONE_SELECTED_ITEM,
                          ActivitySeineUI.BINDING_CURRENT_FPA_ZONE_SELECTED_ITEM,
                          ActivitySeineUI.BINDING_NEXT_FPA_ZONE_SELECTED_ITEM,
                          ActivitySeineUI.BINDING_SURROUNDING_ACTIVITY_SELECTED_ITEM,
                          ActivitySeineUI.BINDING_CLOSE_ENABLED,
                          ActivitySeineUI.BINDING_ADD_SET_ENABLED,
                          ActivitySeineUI.BINDING_CLOSE_AND_CREATE_ENABLED);
        model.setModified(create);
    }

    @Override
    protected boolean doSave(ActivitySeineDto bean) throws Exception {

        boolean notPersisted = bean.isNotPersisted();

        if (log.isDebugEnabled()) {
            log.debug("           long  - lat = " + bean.getLongitude() + " - " + bean.getLatitude());
        }

        String routeId = getSelectedParentId();

        bean.setOpen(true);

        SaveResultDto saveResult = getActivitySeineService().save(routeId, getModel().getBean());
        saveResult.toDto(bean);

        obtainChildPosition(bean);

        // ouverture de l'activité après création
        if (notPersisted) {
            getOpenDataManager().openActivitySeine(getSelectedParentId(), bean.getId());
        }

        return true;
    }


    @Override
    protected int getOpenablePosition(String parentId, ActivitySeineDto bean) {
        return getActivitySeineService().getActivitySeinePositionInRoute(parentId, bean.getId());
    }

    @Override
    protected boolean doDelete(ActivitySeineDto bean) {

        if (askToDelete(bean)) {
            return false;
        }
        if (log.isInfoEnabled()) {
            log.info("Will delete Activity " + bean.getId());
        }

        String routeId = getSelectedParentId();
        getActivitySeineService().delete(routeId, bean.getId());
        getOpenDataManager().closeActivitySeine(bean.getId());

        if (log.isInfoEnabled()) {
            log.info("Delete done for Activity " + bean.getId());
        }
        return true;
    }

    @Override
    protected boolean obtainCanReopen(boolean create) {

        return !create && getOpenDataManager().canOpenActivitySeine(getSelectedParentId());
    }

    String getActivity6Label() {

        Set<ReferentialReference<VesselActivitySeineDto>> activities = getDataSource().getReferentialReferences(VesselActivitySeineDto.class);

        for (ReferentialReference<VesselActivitySeineDto> vesselActivity : activities) {
            if (ActivitySeineDto.ACTIVITY_FIN_DE_PECHE.equals(vesselActivity.getPropertyValue(VesselActivitySeineDto.PROPERTY_CODE))) {
                //                String label = getDecoratorService().decorate(VesselActivitySeineDto.class.getSimpleName(),
//                                                              vesselActivity);
                return getDecoratorService().getReferentialReferenceDecorator(VesselActivitySeineDto.class).toString(vesselActivity);
            }
        }
        throw new IllegalStateException(t("observe.error.no.activity.6"));
    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean generalTabValid = !errorProperties.removeAll(ActivitySeineUIModel.GENERAL_TAB_PROPERTIES);
        boolean measurementsTabValid = !errorProperties.removeAll(ActivitySeineUIModel.MEASUREMENTS_TAB_PROPERTIES);

        ActivitySeineUIModel model = (ActivitySeineUIModel) getModel();
        model.setGeneralTabValid(generalTabValid);
        model.setMeasurementsTabValid(measurementsTabValid);

    }

    private ActivitySeineService getActivitySeineService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivitySeineService();
    }

}
