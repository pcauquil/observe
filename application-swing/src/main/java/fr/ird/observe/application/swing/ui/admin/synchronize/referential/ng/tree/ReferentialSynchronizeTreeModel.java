package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferenceReferentialSynchroNodeSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.ReferentialSynchroNodeSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.RootReferentialSynchroNode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.tree.node.TypeReferentialSynchroNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.RowMapper;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.function.Predicate;

/**
 * Created on 10/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeTreeModel implements TreeSelectionModel, TreeModel, Serializable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialSynchronizeTreeModel.class);

    private final DefaultTreeSelectionModel treeSelectionModel;
    private final DefaultTreeModel treeModel;
    private final ImmutableSetMultimap<Class<? extends ReferentialDto>, String> idsOnlyExistOnThisSide;

    private boolean canAdd;
    private boolean canUpdate;
    private boolean canDelete;
    private boolean canRevert;
    private boolean valueIsAdjusting;

    public ReferentialSynchronizeTreeModel(RootReferentialSynchroNode root,
                                           ImmutableSetMultimap<Class<? extends ReferentialDto>, String> idsOnlyExistOnThisSide) {
        this.treeModel = new DefaultTreeModel(root);
        this.idsOnlyExistOnThisSide = idsOnlyExistOnThisSide;
        this.treeSelectionModel = new DefaultTreeSelectionModel();
        setSelectionMode(DISCONTIGUOUS_TREE_SELECTION);
    }

    public ImmutableSetMultimap<Class<? extends ReferentialDto>, String> getIdsOnlyExistingInThisSide() {
        return idsOnlyExistOnThisSide;
    }


    public synchronized void updateSelectedActions() {

        canAdd = canUpdate = canDelete = canRevert = false;

        for (TreePath selectionPath : getSelectionPaths()) {
            ReferentialSynchroNodeSupport lastPathComponent = (ReferentialSynchroNodeSupport) selectionPath.getLastPathComponent();

            if (lastPathComponent instanceof ReferenceReferentialSynchroNodeSupport) {

                ReferenceReferentialSynchroNodeSupport node = (ReferenceReferentialSynchroNodeSupport) lastPathComponent;
                canAdd |= node.isCanAdd();
                canUpdate |= node.isCanUpdate();
                canDelete |= node.isCanDelete();
                canRevert |= node.isCanRevert();

            }

        }

    }

    public Collection<ReferenceReferentialSynchroNodeSupport> filterSelectedReferenceNodes(Predicate<ReferenceReferentialSynchroNodeSupport> predicate) {

        ImmutableSet.Builder<ReferenceReferentialSynchroNodeSupport> builder = ImmutableSet.builder();

        for (TreePath selectionPath : getSelectionPaths()) {
            ReferentialSynchroNodeSupport lastPathComponent = (ReferentialSynchroNodeSupport) selectionPath.getLastPathComponent();
            if (lastPathComponent instanceof ReferenceReferentialSynchroNodeSupport) {
                ReferenceReferentialSynchroNodeSupport node = (ReferenceReferentialSynchroNodeSupport) lastPathComponent;
                if (predicate.test(node)) {
                    builder.add(node);
                }
            }
        }
        return builder.build();
    }

    public boolean isCanAdd() {
        return canAdd;
    }

    public boolean isCanUpdate() {
        return canUpdate;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public boolean isCanRevert() {
        return canRevert;
    }

    public boolean isCanSkip() {
        return getSelectionCount() > 0;
    }

    public boolean isLeft() {
        return getRoot().isLeft();
    }

    public void removeReferenceNode(ReferenceReferentialSynchroNodeSupport node) {

        TypeReferentialSynchroNode parent = node.getParent();
        treeModel.removeNodeFromParent(node);
        if (parent.isLeaf()) {
            treeModel.removeNodeFromParent(parent);
        } else {
            treeModel.reload(parent);
        }
    }

    @Override
    public RootReferentialSynchroNode getRoot() {
        return (RootReferentialSynchroNode) treeModel.getRoot();
    }

    @Override
    public Object getChild(Object parent, int index) {
        return treeModel.getChild(parent, index);
    }

    @Override
    public int getChildCount(Object parent) {
        return treeModel.getChildCount(parent);
    }

    @Override
    public boolean isLeaf(Object node) {
        return treeModel.isLeaf(node);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        treeModel.valueForPathChanged(path, newValue);
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return treeModel.getIndexOfChild(parent, child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModel.addTreeModelListener(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModel.removeTreeModelListener(l);
    }

    @Override
    public int getSelectionMode() {
        return treeSelectionModel.getSelectionMode();
    }

    @Override
    public void setSelectionMode(int mode) {
        treeSelectionModel.setSelectionMode(mode);
    }

    @Override
    public void addSelectionPath(TreePath path) {
        addSelectionPaths(path);
    }

    @Override
    public void removeSelectionPath(TreePath path) {
        removeSelectionPaths(path);
    }

    @Override
    public void addSelectionPaths(TreePath... paths) {

        valueIsAdjusting = true;

        try {
            Collection<TreePath> collectedPaths = new LinkedList<>();

            collectPaths(collectedPaths, paths);

            if (log.isDebugEnabled()) {
                log.debug("path(s) to add: " + collectedPaths.size());
            }
            treeSelectionModel.addSelectionPaths(collectedPaths.toArray(new TreePath[collectedPaths.size()]));
        } finally {

            valueIsAdjusting = false;

        }
    }

    @Override
    public void removeSelectionPaths(TreePath... paths) {

        if (valueIsAdjusting) {
            return;
        }

        valueIsAdjusting = true;

        try {
            Collection<TreePath> collectedPaths = new LinkedList<>();

            collectPaths(collectedPaths, paths);

            if (log.isDebugEnabled()) {
                log.debug("path(s) to remove: " + collectedPaths.size());
            }
            treeSelectionModel.removeSelectionPaths(collectedPaths.toArray(new TreePath[collectedPaths.size()]));
        } finally {

            valueIsAdjusting = false;

        }

    }

    @Override
    public TreePath getSelectionPath() {
        return treeSelectionModel.getSelectionPath();
    }

    @Override
    public void setSelectionPath(TreePath path) {

        if (isPathSelected(path)) {
            removeSelectionPaths(path);
        } else {
            addSelectionPaths(path);
        }

    }

    @Override
    public TreePath[] getSelectionPaths() {
        return treeSelectionModel.getSelectionPaths();
    }

    @Override
    public void setSelectionPaths(TreePath[] paths) {
        treeSelectionModel.setSelectionPaths(paths);
    }

    @Override
    public int getSelectionCount() {
        return treeSelectionModel.getSelectionCount();
    }

    @Override
    public boolean isPathSelected(TreePath path) {
        return treeSelectionModel.isPathSelected(path);
    }

    @Override
    public boolean isSelectionEmpty() {
        return treeSelectionModel.isSelectionEmpty();
    }

    @Override
    public void clearSelection() {
        treeSelectionModel.clearSelection();
    }

    @Override
    public RowMapper getRowMapper() {
        return treeSelectionModel.getRowMapper();
    }

    @Override
    public void setRowMapper(RowMapper newMapper) {
        treeSelectionModel.setRowMapper(newMapper);
    }

    @Override
    public int[] getSelectionRows() {
        return treeSelectionModel.getSelectionRows();
    }

    @Override
    public int getMinSelectionRow() {
        return treeSelectionModel.getMinSelectionRow();
    }

    @Override
    public int getMaxSelectionRow() {
        return treeSelectionModel.getMaxSelectionRow();
    }

    @Override
    public boolean isRowSelected(int row) {
        return treeSelectionModel.isRowSelected(row);
    }

    @Override
    public void resetRowSelection() {
        treeSelectionModel.resetRowSelection();
    }

    @Override
    public int getLeadSelectionRow() {
        return treeSelectionModel.getLeadSelectionRow();
    }

    @Override
    public TreePath getLeadSelectionPath() {
        return treeSelectionModel.getLeadSelectionPath();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        treeSelectionModel.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        treeSelectionModel.removePropertyChangeListener(listener);
    }

    @Override
    public void addTreeSelectionListener(TreeSelectionListener x) {
        treeSelectionModel.addTreeSelectionListener(x);
    }

    @Override
    public void removeTreeSelectionListener(TreeSelectionListener x) {
        treeSelectionModel.removeTreeSelectionListener(x);
    }

    private void collectPaths(Collection<TreePath> collectedPaths, TreePath... paths) {
        for (TreePath path : paths) {

            collectedPaths.add(path);

            Object node = path.getLastPathComponent();

            if (node instanceof TypeReferentialSynchroNode) {
                TypeReferentialSynchroNode node1 = (TypeReferentialSynchroNode) node;

                Enumeration children = node1.children();
                while (children.hasMoreElements()) {
                    ReferenceReferentialSynchroNodeSupport childNode = (ReferenceReferentialSynchroNodeSupport) children.nextElement();
                    collectedPaths.add(path.pathByAddingChild(childNode));
                }

            }

        }

    }

    public void removeReferenceNodes(Collection<ReferenceReferentialSynchroNodeSupport> removedNodes) {

        valueIsAdjusting = true;

        try {
            for (ReferenceReferentialSynchroNodeSupport removedNode : removedNodes) {
                TypeReferentialSynchroNode typeNode = removedNode.getParent();
                if (typeNode.getChildCount() == 1) {
                    treeModel.removeNodeFromParent(typeNode);
                } else {
                    treeModel.removeNodeFromParent(removedNode);
                }
            }
        } finally {
            valueIsAdjusting = false;
        }

    }
}
