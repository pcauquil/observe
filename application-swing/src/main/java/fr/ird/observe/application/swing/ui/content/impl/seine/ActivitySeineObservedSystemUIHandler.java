/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineObservedSystemDto;
import fr.ird.observe.services.dto.seine.ActivitySeineObservedSystemDtos;
import fr.ird.observe.services.service.seine.ActivitySeineObservedSystemService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ActivitySeineObservedSystemUIHandler extends ContentUIHandler<ActivitySeineObservedSystemDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ActivitySeineObservedSystemUIHandler.class);

    public ActivitySeineObservedSystemUIHandler(ActivitySeineObservedSystemUI ui) {
        super(ui, DataContextType.ActivitySeine, null);
    }

    @Override
    public ActivitySeineObservedSystemUI getUi() {
        return (ActivitySeineObservedSystemUI) super.getUi();
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        if (getOpenDataManager().isOpenActivitySeine(dataContext.getSelectedActivitySeineId())) {

            // l'activity courante est ouverte, on peut modifier
            return ContentMode.UPDATE;
        }

        // activity courante non ouverte
        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(ActivitySeineDto.class),
                   t("observe.storage.activitySeine.message.not.open"));

        return ContentMode.READ;
    }

    @Override
    public void openUI() {
        super.openUI();

        String activityId = getSelectedParentId();

        if (log.isInfoEnabled()) {
            log.info("activityId = " + activityId);
        }

        ContentMode mode = computeContentMode();

        getModel().setMode(mode);

        Form<ActivitySeineObservedSystemDto> form = getActivitySeineObservedSystemService().loadForm(activityId);

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        ActivitySeineObservedSystemDtos.copyActivitySeineObservedSystemDto(form.getObject(), getBean());

        if (mode == ContentMode.UPDATE) {
            getUi().startEdit(null);
        }
    }

    @Override
    public void startEditUI(String... binding) {

        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(ActivitySeineDto.class),
                   t("observe.storage.activitySeine.message.updating"));

        super.startEditUI(binding);
    }

    @Override
    protected boolean doSave(ActivitySeineObservedSystemDto bean) throws Exception {

        // on sauvegarde l'activity (mais pas la calée)
        SaveResultDto saveResult = getActivitySeineObservedSystemService().save(bean);
        saveResult.toDto(bean);

        return true;
    }

    protected ActivitySeineObservedSystemService getActivitySeineObservedSystemService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivitySeineObservedSystemService();
    }
}
