/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUIMode;
import fr.ird.observe.application.swing.ui.UIHelper;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.wizard.BusyChangeListener;
import jaxx.runtime.swing.wizard.WizardUILancher;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTitledPanel;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Le wizard de base pour les opérations complexes.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class AdminUILauncher extends WizardUILancher<AdminStep, AdminUIModel, AdminUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(AdminUILauncher.class);

    protected final AdminStep uniqueAction;

    protected Component parent;

    protected AdminUILauncher(JAXXContext context, AdminStep uniqueAction) {
        super(context,
              AdminUI.class,
              AdminUIModel.class,
              t(uniqueAction.getTitle()),
              t(uniqueAction.getTitleTip()),
              uniqueAction.getIcon()
        );
        this.uniqueAction = uniqueAction;
    }

    public static AdminUILauncher newLauncher(JAXXContext context, AdminStep action) {

        AdminUIHandler handler = context.getContextValue(AdminUIHandler.class);
        if (handler == null) {

            handler = new AdminUIHandler();
            context.setContextValue(handler);

            if (log.isDebugEnabled()) {
                log.debug("Register in context an admin handler : " + handler);
            }
        }
        return new AdminUILauncher(context, action);
    }

    @Override
    protected void init(AdminUI ui) {

        parent = getParent(ui);

        AdminUIModel model = ui.getModel();

        model.addOperation(uniqueAction);

        model.updateUniverse();

        if (uniqueAction != null) {
            // on a juste besoin de faire une action

            ui.blockOperations();
        }

        BusyChangeListener listener = new BusyChangeListener(parent) {

            @Override
            protected void setBusy(Component ui) {
                if (log.isDebugEnabled()) {
                    log.debug("Busy for component " + ui);
                }
                if (parent instanceof ObserveMainUI) {
                    ((ObserveMainUI) parent).setBusy(true);
                    super.setBusy(parent);
                }
                super.setBusy(ui);
            }

            @Override
            protected void setUnBusy(Component ui) {
                if (log.isDebugEnabled()) {
                    log.debug("UnBusy for component " + ui);
                }
                if (parent instanceof ObserveMainUI) {
                    ((ObserveMainUI) parent).setBusy(false);
                    super.setUnBusy(parent);
                }
                super.setUnBusy(ui);
            }
        };
        UIHelper.setLayerUI(ui.getTabs(), ui.getTabBusyBlockLayerUI());
        listener.setBlockingUI(ui.getTabBusyBlockLayerUI());
        model.addPropertyChangeListener(AdminUIModel.BUSY_PROPERTY_NAME, listener);
    }


    protected Component getParent(AdminUI ui) {
        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        ImageIcon icon = (ImageIcon) ui.getClientProperty("icon");
        if (mainUI == null) {
            // pas de fenetre detectee
            // on encapsule l'ui dans un dialog

            JDialog dialog = new JDialog();
            dialog.setSize(1024, 780);

            if (icon != null) {
                dialog.setIconImage(icon.getImage());
            }
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setAlwaysOnTop(true);
            dialog.setModal(true);
            dialog.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosed(WindowEvent e) {
                    if (log.isInfoEnabled()) {
                        log.info("closing dialog " + e.getWindow().getName());
                    }
                    if (AdminUILauncher.this.ui.getModel().getModelState()
                        != WizardState.CANCELED) {
                        if (log.isInfoEnabled()) {
                            log.info("cancel panel from dialog !" +
                                     e.getWindow().getName());
                        }
                        AdminUILauncher.this.ui.cancel();
                    }
                }
            });
            return dialog;
        } else {
            return mainUI;
        }
    }

    @Override
    protected void start(final AdminUI ui) {
        super.start(ui);

        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();

        String title = (String) ui.getClientProperty("title");

        if (mainUI == null) {

            // pas de fenetre detectee
            // on encapsule l'ui dans un dialog

            JDialog dialog = (JDialog) parent;
            dialog.setTitle(t(title));
            dialog.setContentPane(ui);
//            dialog.pack();
            dialog.setVisible(true);
            if (log.isDebugEnabled()) {
                log.debug("After dispay dialog");
            }
        } else {

            // on attache dans la fenetre principale
            String tip = (String) ui.getClientProperty("tip");

            JXTitledPanel panel = mainUI.getSynchroWizard();
            panel.setTitle(t(title));
            panel.setToolTipText(t(tip));
            panel.setContentContainer(ui);
            mainUI.setContextValue(ui.getModel());
            mainUI.setMode(ObserveUIMode.SYNCHRO);

            // refresh selected tab (otherwise it does NOT display (white screen...)
            SwingUtilities.invokeLater(() -> ui.getTabs().getSelectedComponent().validate());
        }
    }

    @Override
    protected void doClose(AdminUI ui, boolean wasCanceled) {
        if (log.isInfoEnabled()) {
            log.info(this + ", was canceled ? " + wasCanceled);
        }

        ObserveMainUI mainUI = null;

        AdminUIModel model = ui.getModel();

        try {


            model.doCloseSource(model.getCentralSource(), false);
            model.doCloseSource(model.getLocalSource(), false);

            if (!(parent instanceof ObserveMainUI)) {
                JDialog dialog = (JDialog) parent;
                if (log.isInfoEnabled()) {
                    log.info("dispose ui!");
                }
                ui.dispose();
                dialog.setVisible(false);
                dialog.dispose();
                if (log.isInfoEnabled()) {
                    log.info("After dispose.");
                }
                ObserveSwingApplicationContext.get().releaseLock();
                return;
            }

            mainUI = (ObserveMainUI) parent;
            mainUI.getSynchroWizard().remove(ui);
            mainUI.removeContextValue(model.getClass());


            ObserveSwingDataSource source = model.getPreviousSource();

            if (source == null) {

                // pas de service auparavant
                mainUI.setMode(ObserveUIMode.NO_DB);
                return;
            }

            model.doCloseSource(source, false);

            // on attache la source a l'ui
            ObserveSwingApplicationContext.get().getDataSourcesManager().prepareMainStorage(source, false);

            source.open();

        } catch (Exception e) {

            // on a pas reussit a reouvrir la base precedente
            UIHelper.handlingError("Could not close", e);

            if (mainUI != null) {
                mainUI.setMode(ObserveUIMode.NO_DB);
            }
        }
    }
}
