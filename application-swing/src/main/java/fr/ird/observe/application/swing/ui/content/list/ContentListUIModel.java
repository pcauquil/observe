/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list;

import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Le modèle pour un écran d'édition avec des fils.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since .14
 */
public abstract class ContentListUIModel<E extends IdDto, C extends DataDto> extends ContentUIModel<E> {

    public static final String PROPERTY_DATA = "data";

    public static final String PROPERTY_SELECTED_DATAS = "selectedDatas";
    public static final String PROPERTY_ONE_SELECTED_DATA = "oneSelectedData";
    public static final String PROPERTY_ONE_OR_MORE_SELECTED_DATA = "oneOrMoreSelectedData";

    public static final String PROPERTY_EMPTY = "empty";

    public static final String PROPERTY_CAN_REOPEN = "canReopen";

    private static final long serialVersionUID = 1L;

    /** Logger */
    static private final Log log = LogFactory.getLog(ContentListUIModel.class);

    /** type des entites */
    protected final Class<C> childType;

    /** liste des entites */
    protected List<DataReference<C>> data;

    /** entités sélectionnées dans la liste */
    protected List<DataReference<C>> selectedDatas;

    /** un drapeau pour savoir si on peut reouvrir l'un des données de la liste. */
    protected boolean canReopen;

    public ContentListUIModel(Class<E> beanType, Class<C> childType) {
        super(beanType);
        this.childType = childType;
    }

    public Class<C> getChildType() {
        return childType;
    }

    public List<DataReference<C>> getData() {
        return data;
    }

    public void setData(List<DataReference<C>> data) {
        boolean wasEmpty = isEmpty();
        this.data = data;
        // on force toujours la propagation de la liste
        firePropertyChange(PROPERTY_DATA, null, data);
        firePropertyChange(PROPERTY_EMPTY, wasEmpty, isEmpty());
        setSelectedDatas(null);
    }

    public DataReference<C> getSelectedData() {
        return CollectionUtils.isNotEmpty(selectedDatas) ? selectedDatas.get(0) : null;
    }

    public List<DataReference<C>> getSelectedDatas() {
        return selectedDatas;
    }

    public void setSelectedDatas(List<DataReference<C>> selectedDatas) {
        boolean oldOneSelectedData = isOneSelectedData();
        boolean oldOneOrMoreSelectedData = isOneOrMoreSelectedData();
        List<DataReference<C>> old = getSelectedDatas();
        this.selectedDatas = selectedDatas;
        if (log.isDebugEnabled()) {
            log.debug("New selected datas : " + selectedDatas);
        }
        firePropertyChange(PROPERTY_SELECTED_DATAS, old, selectedDatas);
        firePropertyChange(PROPERTY_ONE_SELECTED_DATA, oldOneSelectedData, isOneSelectedData());
        firePropertyChange(PROPERTY_ONE_OR_MORE_SELECTED_DATA, oldOneOrMoreSelectedData, isOneOrMoreSelectedData());
    }

    public boolean isOneSelectedData() {
        return selectedDatas != null && selectedDatas.size() == 1;
    }

    public boolean isOneOrMoreSelectedData() {
        return CollectionUtils.isNotEmpty(selectedDatas );
    }

    public boolean isCanReopen() {
        return canReopen;
    }

    public void setCanReopen(boolean canReopen) {
        boolean old = isCanReopen();
        this.canReopen = canReopen;
        firePropertyChange(PROPERTY_CAN_REOPEN, old, canReopen);
    }

    public boolean isEmpty() {
        return data == null || data.isEmpty();
    }

}
