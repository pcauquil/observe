package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.util.table.EditableTableWithCacheTableModelSupport;
import fr.ird.observe.services.dto.longline.GearUseFeaturesMeasurementLonglineDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicDto;
import fr.ird.observe.services.dto.referential.GearCaracteristicTypeDtos;
import fr.ird.observe.services.dto.referential.ReferentialReference;

/**
 * Created on 4/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.16
 */
public class GearUseFeaturesMeasurementLonglinesTableModel extends EditableTableWithCacheTableModelSupport<GearUseFeaturesMeasurementLonglineDto> {

    private static final long serialVersionUID = 1L;

    public GearUseFeaturesMeasurementLonglinesTableModel() {
        super();
    }

    @Override
    public boolean isRowNotEmpty(GearUseFeaturesMeasurementLonglineDto valid) {
        return !(valid.getGearCaracteristic() == null && valid.getMeasurementValue() == null);
    }

    @Override
    protected boolean isRowValid(GearUseFeaturesMeasurementLonglineDto valid) {
        String size = valid.getMeasurementValue();
        return !(valid.getGearCaracteristic() == null || size == null) && !size.isEmpty();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        GearUseFeaturesMeasurementLonglineDto measure = data.get(rowIndex);
        Object result;
        switch (columnIndex) {
            case 0:
                result = measure.getGearCaracteristic();
                break;
            case 1:
                result = measure.getMeasurementValue();
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {

        boolean result;
        switch (columnIndex) {
            case 0:
                result = true;
                break;
            case 1:
                GearUseFeaturesMeasurementLonglineDto measure = data.get(rowIndex);
                result = measure != null && measure.getGearCaracteristic() != null;
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return result;

    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        GearUseFeaturesMeasurementLonglineDto measure = data.get(rowIndex);
        switch (columnIndex) {
            case 0:
                measure.setGearCaracteristic((ReferentialReference<GearCaracteristicDto>) aValue);
                String gearCaracteristicTypeId = (String) measure.getGearCaracteristic().getPropertyValue(GearCaracteristicDto.PROPERTY_GEAR_CARACTERISTIC_TYPE);
                if (GearCaracteristicTypeDtos.isBoolean(gearCaracteristicTypeId)) {
                    // on force à avoir false par défaut
                    measure.setMeasurementValue("false");
                }
                break;
            case 1:

                measure.setMeasurementValue(aValue == null ? null : String.valueOf(aValue));

                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

        setModified(true);

    }

    @Override
    public void removeSelectedRow() {

        super.removeSelectedRow();

        // revalidate table model
        validate();
        setModified(true);

    }

    @Override
    protected GearUseFeaturesMeasurementLonglineDto createNewRow() {
        return new GearUseFeaturesMeasurementLonglineDto();
    }
}
