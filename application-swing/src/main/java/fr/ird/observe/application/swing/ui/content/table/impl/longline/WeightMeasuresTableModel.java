package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.WeightMeasureDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.longline.WeightMeasureTypeDto;
import fr.ird.observe.application.swing.ui.util.table.EditableTableWithCacheTableModelSupport;

/**
 * Created on 12/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class WeightMeasuresTableModel extends EditableTableWithCacheTableModelSupport<WeightMeasureDto> {

    private static final long serialVersionUID = 1L;

    public WeightMeasuresTableModel() {
        super();
    }

    @Override
    public boolean isRowNotEmpty(WeightMeasureDto valid) {
        return !(valid.getWeightMeasureType() == null && valid.getWeight() == null);
    }

    @Override
    public boolean isRowValid(WeightMeasureDto valid) {
        Float weight = valid.getWeight();
        return !(valid.getWeightMeasureType() == null || weight == null) && weight > 0 && weight < 1000;
    }

    @Override
    protected WeightMeasureDto createNewRow() {
        return new WeightMeasureDto();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        WeightMeasureDto measure = data.get(rowIndex);
        Object result;
        switch (columnIndex) {
            case 0:
                result = measure.getWeightMeasureType();
                break;
            case 1:
                result = measure.getWeight();
                break;
            default:
                throw new IllegalStateException("Can't come here");
        }
        return result;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        WeightMeasureDto measure = data.get(rowIndex);
        switch (columnIndex) {
            case 0:

                measure.setWeightMeasureType((ReferentialReference<WeightMeasureTypeDto>) aValue);
                break;
            case 1:

                measure.setWeight((Float) aValue);
                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

        setModified(true);

    }

}
