/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import fr.ird.observe.application.swing.I18nEnumHelper;

import static org.nuiton.i18n.I18n.n;

/**
 * Définition des codes mesures utilisés pour saisir et afficher les
 * échantillons thon.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.9.1
 */
public enum CodeMesureEnum implements Comparable<CodeMesureEnum> {

    /** inconnue */
    unknown,

    /** mesure LD1 */
    ld1,

    /** mesure LF */
    lf;

    static {

        n("observe.enum.fr.ird.observe.application.swing.ui.content.table.impl.seine.CodeMesureEnum.unknown");
        n("observe.enum.fr.ird.observe.application.swing.ui.content.table.impl.seine.CodeMesureEnum.ld1");
        n("observe.enum.fr.ird.observe.application.swing.ui.content.table.impl.seine.CodeMesureEnum.lf");

    }

    public String toString() {
        return I18nEnumHelper.getLabel(this);
    }
}
