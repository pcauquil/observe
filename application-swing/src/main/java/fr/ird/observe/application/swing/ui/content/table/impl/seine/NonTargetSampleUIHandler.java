/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;
import fr.ird.observe.services.dto.seine.NonTargetSampleDtos;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.service.seine.NonTargetSampleService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class NonTargetSampleUIHandler extends ContentTableUIHandler<NonTargetSampleDto, NonTargetLengthDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(NonTargetSampleUIHandler.class);

    /**
     * Ecoute les modifications de la propriété {@link NonTargetLengthDto#getWeight()},
     * et repasser alors le flag {@link NonTargetLengthDto#isWeightSource()} à
     * {@code false}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener weightChanged;

    /**
     * Ecoute les modifications de la propriété {@link NonTargetLengthDto#getLength()},
     * et repasser alors le flag {@link NonTargetLengthDto#isLengthSource()} à
     * {@code false}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener lengthChanged;

    public NonTargetSampleUIHandler(NonTargetSampleUI ui) {
        super(ui, DataContextType.SetSeine);
        weightChanged = evt -> {
            NonTargetLengthDto source = (NonTargetLengthDto) evt.getSource();
            source.setWeightSource(false);
        };
        lengthChanged = evt -> {
            NonTargetLengthDto source = (NonTargetLengthDto) evt.getSource();
            source.setLengthSource(false);
        };
    }

    @Override
    public NonTargetSampleUI getUi() {
        return (NonTargetSampleUI) super.getUi();
    }

    public void resetWeightSource() {
        getTableEditBean().setWeightSource(false);
        getUi().getWeight().grabFocus();
    }

    public void resetLengthSource() {
        getTableEditBean().setLengthSource(false);
        getUi().getLength().grabFocus();
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onSelectedRowChanged(int editingRow,
                                        NonTargetLengthDto bean,
                                        boolean create) {
        ContentTableModel<NonTargetSampleDto, NonTargetLengthDto> model = getTableModel();

        if (!model.isEditable()) {
            return;
        }

        ReferentialReference<SpeciesDto> species = bean.getSpecies();
        NonTargetSampleUI ui = getUi();
        if (log.isDebugEnabled()) {
            log.debug("selected species " + species);
        }
        List<ReferentialReference<SpeciesDto>> availableEspeces;
        JComponent requestFocus;

        if (create) {

            if (model.isCreate()) {

                // on passe le mode de saisie en count
                ui.getAcquisitionModeGroup().setSelectedValue(null);
                ui.getAcquisitionModeGroup().setSelectedValue(ModeSaisieEchantillonEnum.byEffectif);
            }

            Set<ReferentialReference<SpeciesDto>> speciesReferences = getModel().getReferentialReferences(NonTargetLengthDto.PROPERTY_SPECIES);

            availableEspeces = Lists.newArrayList(speciesReferences);
            requestFocus = ui.getSpecies();
        } else {

            requestFocus = ui.getCount();

            // on passe le mode de saisie
            int acquisitionMode = bean.getAcquisitionMode();
            ModeSaisieEchantillonEnum enumValue =
                    ModeSaisieEchantillonEnum.valueOf(acquisitionMode);
            ui.getAcquisitionModeGroup().setSelectedValue(null);
            ui.getAcquisitionModeGroup().setSelectedValue(enumValue);

            availableEspeces = Lists.newArrayList(species);
        }
        ui.getSpecies().setData(availableEspeces);
        requestFocus.requestFocus();

        NonTargetLengthDto tableEditBean = getTableEditBean();
        tableEditBean.removePropertyChangeListener(NonTargetLengthDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.addPropertyChangeListener(NonTargetLengthDto.PROPERTY_WEIGHT, weightChanged);

        tableEditBean.removePropertyChangeListener(NonTargetLengthDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.addPropertyChangeListener(NonTargetLengthDto.PROPERTY_LENGTH, lengthChanged);
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {
        JTable table = getUi().getTable();

        UIHelper.fixTableColumnWidth(table, 1, 100);
        UIHelper.fixTableColumnWidth(table, 2, 100);

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.nonTargetSample.table.speciesFaune"),
                n("observe.content.nonTargetSample.table.speciesFaune.tip"),
                n("observe.content.nonTargetSample.table.length"),
                n("observe.content.nonTargetSample.table.length.tip"),
                n("observe.content.nonTargetSample.table.meanWeight"),
                n("observe.content.nonTargetSample.table.meanWeight.tip"),
                n("observe.content.nonTargetSample.table.count"),
                n("observe.content.nonTargetSample.table.count.tip"),
                n("observe.content.nonTargetSample.table.gender"),
                n("observe.content.nonTargetSample.table.gender.tip"),
                n("observe.content.nonTargetSample.table.picturesReferences"),
                n("observe.content.nonTargetSample.table.picturesReferences.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SexDto.class));
        UIHelper.setTableColumnRenderer(table, 5, renderer);
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {
        ContentMode mode = super.getContentMode(dataContext);

        String setId = getDataContext().getSelectedSetId();

        boolean showData = getNonTargetSampleService().canUseNonTargetSample(setId);

        getUi().getModel().setShowData(showData);

        if (mode == ContentMode.UPDATE && !showData) {

            // on repasse en mode resteint car on ne peut pas éditer l'écran
            mode = ContentMode.READ;

            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(SetSeineDto.class),
                       t("observe.content.setSeine.message.no.nonTargetDiscarded"));
        }
        return mode;
    }

    /**
     * Le mode de saisie a été mis à jour.
     *
     * @param newMode le nouveau de mode de saisie à utiliser
     * @since 3.0
     */
    public void updateModeSaisie(ModeSaisieEchantillonEnum newMode) {

        if (log.isDebugEnabled()) {
            log.debug("Change mode saisie to " + newMode);
        }
        if (newMode == null) {

            // mode null (cela peut arriver avec les bindings)
            return;
        }

        NonTargetSampleUI ui = getUi();

        boolean createMode = ui.getTableModel().isCreate();

        NonTargetLengthDto editBean = ui.getTableEditBean();
        switch (newMode) {

            case byEffectif:

                // le weight n'est pas modifiable
                ui.getWeight().setEnabled(false);

                // l'count est modifiable
                ui.getCount().setEnabled(true);

                if (createMode) {

                    // on supprime le weight (si il a été saisie)
                    editBean.setWeight(null);
                    // on supprime aussi l'count (pour forcer la saisie)
                    editBean.setCount(null);
                }
                break;

            case byIndividu:

                // le weight est pas modifiable
                ui.getWeight().setEnabled(true);

                // l'count n'est pas modifiable et est toujours de 1
                ui.getCount().setEnabled(false);


                if (createMode) {

                    // on positionne l'count à 1 (seule valeur possible)
                    editBean.setCount(1);
                }
                break;
        }

        if (createMode) {

            // on propage le mode de saisie dans le bean
            editBean.setAcquisitionMode(newMode.ordinal());
        }
    }

    @Override
    protected void doPersist(NonTargetSampleDto bean) {

        SaveResultDto saveResult = getNonTargetSampleService().save(getSelectedParentId(), bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<NonTargetSampleDto> form = getNonTargetSampleService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        NonTargetSampleDtos.copyNonTargetSampleDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case NonTargetLengthDto.PROPERTY_SPECIES: {

                result = (Collection) getNonTargetSampleService().getSampleSpecies(getSelectedParentId());

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected NonTargetSampleService getNonTargetSampleService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newNonTargetSampleService();
    }
}
