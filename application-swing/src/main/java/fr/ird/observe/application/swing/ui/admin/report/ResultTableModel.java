/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.report;

import fr.ird.observe.services.dto.actions.report.DataMatrix;
import fr.ird.observe.services.dto.actions.report.Report;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.table.AbstractTableModel;
import java.io.Serializable;

/**
 * Le modèle de tableau de résultats d'un report.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ResultTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ResultTableModel.class);

    /** les données du tableaux. */
    protected transient DataMatrix data;

    public void populate(Report report, DataMatrix data) {

        if (report == null) {

            // suppression du report
            clear();
            return;
        }

        if (log.isInfoEnabled()) {
            log.info("from report : " + report);
        }

        DataMatrix allData = new DataMatrix();

        DataMatrix rowData = null;
        DataMatrix columnData = null;
        int nbRows = data.getHeight();
        int nbCols = data.getWidth();

        String[] columnNames = report.getColumnHeaders();
        String[] rowNames = report.getRowHeaders();

        boolean withColumnHeader = columnNames != null && columnNames.length > 0;
        boolean withRowHeader = rowNames != null && rowNames.length > 0;

        if (withColumnHeader) {

            // on ajoute une première ligne aux données

            nbRows += 1;
            nbCols = columnNames.length;

            // les données commencent à la ligne 1
            data.setY(1);
            rowData = new DataMatrix();
            rowData.setWidth(nbCols);
            rowData.setHeight(1);
            rowData.createData();
            for (int i = 0; i < nbCols; i++) {
                String columnName = columnNames[i];
                rowData.setValue(i, 0, columnName);
            }
            if (withRowHeader) {
                rowData.setX(1);
            }
        }

        if (withRowHeader) {

            // on ajoute une première colonne aux données
            nbCols += 1;
            nbRows = rowNames.length;

            // les données commencent à la colonne 1
            data.setX(1);

            // la matrice colonne 0
            columnData = new DataMatrix();
            columnData.setWidth(1);
            columnData.setHeight(nbRows);

            columnData.createData();
            for (int i = 0; i < nbRows; i++) {
                String rowName = rowNames[i];
                columnData.setValue(0, i, rowName);
            }
            if (withColumnHeader) {
                columnData.setY(1);
                nbRows += 1;
            }
        }

        allData.setHeight(nbRows);
        allData.setWidth(nbCols);

        if (log.isInfoEnabled()) {
            log.info("final data dimension : " +
                     allData.getDimension() + " vs " +
                     data.getDimension()
            );
        }

        // creation de la matrice finale
        allData.createData();

        if (withRowHeader) {

            // ajout de la colonne 0
            allData.copyData(columnData);
        }

        if (withColumnHeader) {

            // ajout de la ligne 0
            allData.copyData(rowData);
        }

        // ajout des données réelles
        allData.copyData(data);
        this.data = allData;
        fireTableStructureChanged();
    }

    public void clear() {
        data = null;
        fireTableStructureChanged();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        clear();
    }

    @Override
    public int getRowCount() {
        return data == null ? 0 : data.getHeight();
    }

    @Override
    public int getColumnCount() {
        return data == null ? 0 : data.getWidth();
    }

    @Override
    public String getColumnName(int columnIndex) {

        // on utilise pas les colonnes du modèle de tableau (on utilise
        // directement les données du tableaux que l'on va distinguer en gras)
        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.data == null ? null : this.data.getValue(columnIndex, rowIndex);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        // les données du tableau n'est pas modifiable
    }

    public String getClipbordContent(boolean copyRowHeaders,
                                     boolean copyColumnHeaders) {

        return data == null ? "" : data.getClipbordContent(copyRowHeaders,
                                                           copyColumnHeaders
        );
    }
}
