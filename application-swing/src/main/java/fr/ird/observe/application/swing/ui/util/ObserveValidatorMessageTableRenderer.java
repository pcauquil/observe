package fr.ird.observe.application.swing.ui.util;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableRenderer;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import javax.swing.JTable;
import java.awt.Color;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 12/13/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class ObserveValidatorMessageTableRenderer extends SwingValidatorMessageTableRenderer {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveValidatorMessageTableRenderer.class);

    @Override
    public String getFieldName(JTable table, String value, int row) {
        SwingValidatorMessageTableModel tableModel = (SwingValidatorMessageTableModel) table.getModel();
        SwingValidatorMessage model = tableModel.getRow(row);
        JComponent editor = model.getEditor();
        if (editor != null) {
            String validatorLabel = (String) editor.getClientProperty("validatorLabel");
            if (validatorLabel != null) {
                if (log.isWarnEnabled()) {
                    log.warn("Using deprecated validatorLabel : " + validatorLabel);
                }
                return validatorLabel;
            }

            Boolean doNotTranslateFieldName = (Boolean) editor.getClientProperty("doNotTranslateFieldName");
            if (BooleanUtils.isTrue(doNotTranslateFieldName)) {
                return value;
            }
        }

        String fieldName = value.startsWith("observe.") ? value : ObserveI18nDecoratorHelper.getPropertyI18nKey(value);
        fieldName = t(fieldName);

        return fieldName;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component result = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        SwingValidatorMessageTableModel model = (SwingValidatorMessageTableModel) table.getModel();

        NuitonValidatorScope scope = (NuitonValidatorScope) (column == 0 ? value : model.getValueAt(row, 0));

        Color textColor = scope == NuitonValidatorScope.WARNING ? Color.RED : Color.BLACK;
        result.setForeground(textColor);
        return result;
    }
}
