package fr.ird.observe.application.swing.ui.content.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;

import java.util.Set;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class SetSeineUIModel extends ContentUIModel<SetSeineDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_MEASUREMENTS_TAB_VALID = "measurementsTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(SetSeineDto.PROPERTY_START_TIME,
                                               SetSeineDto.PROPERTY_END_PURSING_TIME_STAMP,
                                               SetSeineDto.PROPERTY_END_SET_TIME_STAMP,
                                               SetSeineDto.PROPERTY_REASON_FOR_NULL_SET,
                                               SetSeineDto.PROPERTY_SUPPORT_VESSEL_NAME,
                                               SetSeineDto.PROPERTY_TARGET_DISCARDED,
                                               SetSeineDto.PROPERTY_NON_TARGET_DISCARDED,
                                               SetSeineDto.PROPERTY_SCHOOL_TYPE).build();

    public static final Set<String> MEASUREMENTS_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(SetSeineDto.PROPERTY_SCHOOL_THICKNESS,
                                               SetSeineDto.PROPERTY_SCHOOL_MEAN_DEPTH,
                                               SetSeineDto.PROPERTY_SCHOOL_TOP_DEPTH,
                                               SetSeineDto.PROPERTY_CURRENT_SPEED,
                                               SetSeineDto.PROPERTY_CURRENT_DIRECTION,
                                               SetSeineDto.PROPERTY_CURRENT_MEASURE_DEPTH,
                                               SetSeineDto.PROPERTY_MAX_GEAR_DEPTH,
                                               SetSeineDto.PROPERTY_SONAR_USED).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean measurementsTabValid;


    public SetSeineUIModel() {
        super(SetSeineDto.class);
    }

    public boolean isMeasurementsTabValid() {
        return measurementsTabValid;
    }

    public void setMeasurementsTabValid(boolean measurementsTabValid) {
        Object oldValue = isMeasurementsTabValid();
        this.measurementsTabValid = measurementsTabValid;
        firePropertyChange(PROPERTY_MEASUREMENTS_TAB_VALID, oldValue, measurementsTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
