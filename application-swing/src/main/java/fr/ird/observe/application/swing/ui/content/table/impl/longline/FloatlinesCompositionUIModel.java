package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDto;
import fr.ird.observe.services.dto.longline.FloatlinesCompositionDtos;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 12/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class FloatlinesCompositionUIModel extends ContentTableUIModel<SetLonglineGlobalCompositionDto, FloatlinesCompositionDto> {

    private static final long serialVersionUID = 1L;

    public FloatlinesCompositionUIModel(FloatlinesCompositionUI ui) {
        super(SetLonglineGlobalCompositionDto.class,
              FloatlinesCompositionDto.class,
              new String[]{
                      SetLonglineGlobalCompositionDto.PROPERTY_FLOATLINES_COMPOSITION
              },
              new String[]{FloatlinesCompositionDto.PROPERTY_LINE_TYPE,
                           FloatlinesCompositionDto.PROPERTY_LENGTH,
                           FloatlinesCompositionDto.PROPERTY_PROPORTION});

        List<ContentTableMeta<FloatlinesCompositionDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(FloatlinesCompositionDto.class, FloatlinesCompositionDto.PROPERTY_LINE_TYPE, false),
                ContentTableModel.newTableMeta(FloatlinesCompositionDto.class, FloatlinesCompositionDto.PROPERTY_LENGTH, false),
                ContentTableModel.newTableMeta(FloatlinesCompositionDto.class, FloatlinesCompositionDto.PROPERTY_PROPORTION, false));

        initModel(ui, metas);

    }


    @Override
    protected ContentTableModel<SetLonglineGlobalCompositionDto, FloatlinesCompositionDto> createTableModel(
            ObserveContentTableUI<SetLonglineGlobalCompositionDto, FloatlinesCompositionDto> ui,
            List<ContentTableMeta<FloatlinesCompositionDto>> contentTableMetas) {

        return new ContentTableModel<SetLonglineGlobalCompositionDto, FloatlinesCompositionDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<FloatlinesCompositionDto> getChilds(SetLonglineGlobalCompositionDto bean) {
                return bean.getFloatlinesComposition();
            }

            @Override
            protected void load(FloatlinesCompositionDto source, FloatlinesCompositionDto target) {
                FloatlinesCompositionDtos.copyFloatlinesCompositionDto(source, target);
            }

            @Override
            protected void setChilds(SetLonglineGlobalCompositionDto parent, List<FloatlinesCompositionDto> childs) {
                parent.setFloatlinesComposition(childs);
            }
        };
    }
}
