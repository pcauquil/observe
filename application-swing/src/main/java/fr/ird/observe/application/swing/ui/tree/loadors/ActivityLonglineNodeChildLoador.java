package fr.ird.observe.application.swing.ui.tree.loadors;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.tree.ActivityLonglineNode;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.SetLonglineNode;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.EncounterDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import jaxx.runtime.swing.nav.NavDataProvider;
import jaxx.runtime.swing.nav.tree.NavTreeBridge;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created on 8/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class ActivityLonglineNodeChildLoador extends AbstractNodeChildLoador<Class, String> {

    private static final long serialVersionUID = 1L;

    public ActivityLonglineNodeChildLoador() {
        super(String.class);
    }

    @Override
    public void loadChilds(NavTreeBridge<ObserveNode> model,
                           ObserveNode parentNode,
                           NavDataProvider dataProvider) throws Exception {

        ObserveNode containerNode = parentNode.getContainerNode();

        if (containerNode == null) {
            throw new IllegalStateException("Could not find containerNode of " + parentNode);
        }

        DataReference<ActivityLonglineDto> activityLonglineRef = ((ActivityLonglineNode) parentNode).getEntity();

        DataReference<SetLonglineDto> setLonglineRef = (DataReference) activityLonglineRef.getPropertyValue(ActivityLonglineDto.PROPERTY_SET_LONGLINE);
        if (setLonglineRef != null) {
            parentNode.add(createSetNode(setLonglineRef));
        }

        // ajout des autres fils avant la calée
        super.loadChilds(model, parentNode, dataProvider);

    }

    @Override
    public List<Class> getData(Class<?> parentClass, String parentId, NavDataProvider dataService) {
        return Arrays.asList(EncounterDto.class, SensorUsedDto.class);

    }

    @Override
    public ObserveNode createNode(Class data, NavDataProvider dataProvider) {
        return createPluralizeStringNode(data, null);
    }

    public ObserveNode createSetNode(DataReference<SetLonglineDto> data) {
        Objects.requireNonNull(data, "Ne peut pas ajouter un objet null");
        return new SetLonglineNode(data);
    }
}
