<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI
  superGenericType='SetSeineSchoolEstimateDto, SchoolEstimateDto'


  contentTitle='{n("observe.common.schoolEstimate")}'
  saveNewEntryText='{n("observe.content.action.create.schoolEstimate")}'
  saveNewEntryTip='{n("observe.content.action.create.schoolEstimate.tip")}'>

  <style source="../../CommonTable.jcss"/>

  <import>
    fr.ird.observe.services.dto.CommentableDto
    fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto
    fr.ird.observe.services.dto.seine.SchoolEstimateDto
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.SpeciesDto
    fr.ird.observe.application.swing.ui.content.table.*

    jaxx.runtime.swing.editor.NumberEditor
    jaxx.runtime.swing.editor.bean.BeanComboBox

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- handler -->
  <SchoolEstimateUIHandler id='handler' constructorParams='this'/>

  <!-- model -->
  <SchoolEstimateUIModel id='model' constructorParams='this'/>

  <!-- edit bean -->
  <SetSeineSchoolEstimateDto id='bean'/>

  <!-- table edit bean -->
  <SchoolEstimateDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator'
                 autoField='true'
                 beanClass='fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update'>
    <!-- clef unique -->
    <field name="schoolEstimate" component="editorPanel"/>
  </BeanValidator>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable'
                 autoField='true'
                 beanClass='fr.ird.observe.services.dto.seine.SchoolEstimateDto'
                 errorTableModel='{getErrorTableModel()}'
                 context='ui-update'/>

  <!-- formulaire -->
  <Table id='editorPanel' fill='both' insets='1'>

    <!-- species -->
    <row>
      <cell>
        <JLabel id='speciesLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='species' genericType='ReferentialReference&lt;SpeciesDto&gt;' _entityClass='SpeciesDto.class'
                      constructorParams='this'/>
      </cell>
    </row>

    <!-- weight -->
    <row>
      <cell>
        <JLabel id='totalWeightLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <NumberEditor id='totalWeight' constructorParams='this'/>
      </cell>
    </row>

    <!-- mean weight -->
    <row>
      <cell>
        <JLabel id='meanWeightLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <NumberEditor id='meanWeight' constructorParams='this'/>
      </cell>
    </row>
  </Table>

  <Table id='extraZone' fill='both' weightx='1' insets='1'>
    <row>
      <cell weighty='1'>
        <JScrollPane id='comment'
                     onFocusGained='comment2.requestFocus()'>
          <JTextArea id='comment2'
                     onKeyReleased='getBean().setComment(comment2.getText())'/>
        </JScrollPane>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
