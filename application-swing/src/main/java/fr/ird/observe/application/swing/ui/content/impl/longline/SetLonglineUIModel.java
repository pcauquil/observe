package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;

import java.util.Set;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class SetLonglineUIModel extends ContentUIModel<SetLonglineDto> {

    public static final String PROPERTY_SETTING_TAB_VALID = "settingTabValid";

    public static final String PROPERTY_SETTING_CARACTERISTICS_TAB_VALID = "settingCaracteristicsTabValid";

    public static final String PROPERTY_HAULING_TAB_VALID = "haulingTabValid";

    public static final Set<String> SETTING_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(
                    SetLonglineDto.PROPERTY_HOME_ID,
                    SetLonglineDto.PROPERTY_NUMBER,
                    SetLonglineDto.PROPERTY_SETTING_START_TIME_STAMP,
                    SetLonglineDto.PROPERTY_SETTING_START_LATITUDE,
                    SetLonglineDto.PROPERTY_SETTING_START_LONGITUDE,
                    SetLonglineDto.PROPERTY_SETTING_END_TIME_STAMP,
                    SetLonglineDto.PROPERTY_SETTING_END_LATITUDE,
                    SetLonglineDto.PROPERTY_SETTING_END_LONGITUDE
            ).build();

    public static final Set<String> SETTING_CARACTERISTICS_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(
                    SetLonglineDto.PROPERTY_SETTING_SHAPE,
                    SetLonglineDto.PROPERTY_LINE_TYPE,
                    SetLonglineDto.PROPERTY_LIGHTSTICKS_TYPE,
                    SetLonglineDto.PROPERTY_LIGHTSTICKS_COLOR,
                    SetLonglineDto.PROPERTY_SETTING_VESSEL_SPEED,
                    SetLonglineDto.PROPERTY_MAX_DEPTH_TARGETED,
                    SetLonglineDto.PROPERTY_SHOOTER_USED,
                    SetLonglineDto.PROPERTY_SHOOTER_SPEED,
                    SetLonglineDto.PROPERTY_WEIGHTED_SWIVEL,
                    SetLonglineDto.PROPERTY_SWIVEL_WEIGHT,
                    SetLonglineDto.PROPERTY_WEIGHTED_SNAP,
                    SetLonglineDto.PROPERTY_SNAP_WEIGHT,
                    SetLonglineDto.PROPERTY_MONITORED,
                    SetLonglineDto.PROPERTY_TIME_BETWEEN_HOOKS,
                    SetLonglineDto.PROPERTY_BASKETS_PER_SECTION_COUNT,
                    SetLonglineDto.PROPERTY_BRANCHLINES_PER_BASKET_COUNT,
                    SetLonglineDto.PROPERTY_LIGHTSTICKS_PER_BASKET_COUNT,
                    SetLonglineDto.PROPERTY_TOTAL_SECTIONS_COUNT,
                    SetLonglineDto.PROPERTY_TOTAL_BASKETS_COUNT,
                    SetLonglineDto.PROPERTY_TOTAL_HOOKS_COUNT
            ).build();

    public static final Set<String> HAULING_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(
                    SetLonglineDto.PROPERTY_HAULING_DIRECTION_SAME_AS_SETTING,
                    SetLonglineDto.PROPERTY_HAULING_START_LATITUDE,
                    SetLonglineDto.PROPERTY_HAULING_START_LONGITUDE,
                    SetLonglineDto.PROPERTY_HAULING_START_TIME_STAMP,
                    SetLonglineDto.PROPERTY_HAULING_END_LATITUDE,
                    SetLonglineDto.PROPERTY_HAULING_END_LONGITUDE,
                    SetLonglineDto.PROPERTY_HAULING_END_TIME_STAMP,
                    SetLonglineDto.PROPERTY_HAULING_BREAKS
            ).build();

    private static final long serialVersionUID = 1L;

    protected boolean settingTabValid;

    protected boolean settingCaracteristicsTabValid;

    protected boolean haulingTabValid;

    public SetLonglineUIModel() {
        super(SetLonglineDto.class);
    }

    public boolean isHaulingTabValid() {
        return haulingTabValid;
    }

    public void setHaulingTabValid(boolean haulingTabValid) {
        this.haulingTabValid = haulingTabValid;
        firePropertyChange(PROPERTY_HAULING_TAB_VALID, null, haulingTabValid);
    }

    public boolean isSettingCaracteristicsTabValid() {
        return settingCaracteristicsTabValid;
    }

    public void setSettingCaracteristicsTabValid(boolean settingCaracteristicsTabValid) {
        this.settingCaracteristicsTabValid = settingCaracteristicsTabValid;
        firePropertyChange(PROPERTY_SETTING_CARACTERISTICS_TAB_VALID, null, settingCaracteristicsTabValid);
    }

    public boolean isSettingTabValid() {
        return settingTabValid;
    }

    public void setSettingTabValid(boolean settingTabValid) {
        this.settingTabValid = settingTabValid;
        firePropertyChange(PROPERTY_SETTING_TAB_VALID, null, settingTabValid);
    }

}
