package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BasketWithSectionIdDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.BranchlineWithBasketIdDto;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.LonglinePositionAwareDto;
import fr.ird.observe.services.dto.longline.LonglinePositionSetDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import jaxx.runtime.swing.editor.bean.BeanComboBox;

import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 1/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class LonglinePositionHelper<D extends LonglinePositionAwareDto> {

    protected final BeanComboBox<DataReference<SectionDto>> uiSection;

    protected final BeanComboBox<DataReference<BasketDto>> uiBasket;

    protected final BeanComboBox<DataReference<BranchlineDto>> uiBranchline;

    // To avoid any propagation when doing some works on locations
    protected boolean locationIsAdjusting;

    protected Collection<DataReference<SectionDto>> sectionUniverse;

    protected Collection<DataReference<BasketDto>> basketUniverse;

    protected Collection<DataReference<BranchlineDto>> branchlineUniverse;

    public LonglinePositionHelper(BeanComboBox<DataReference<SectionDto>> uiSection,
                                  BeanComboBox<DataReference<BasketDto>> uiBasket,
                                  BeanComboBox<DataReference<BranchlineDto>> uiBranchline,
                                  D tableEditBean) {

        this.uiSection = uiSection;
        this.uiBasket = uiBasket;
        this.uiBranchline = uiBranchline;

        PropertyChangeListener sectionChanged = evt -> onSectionChanged((DataReference<SectionDto>) evt.getNewValue(), (D) evt.getSource());
        tableEditBean.addPropertyChangeListener(CatchLonglineDto.PROPERTY_SECTION, sectionChanged);

        PropertyChangeListener basketChanged = evt -> onBasketChanged((DataReference<BasketDto>) evt.getNewValue(), (D) evt.getSource());
        tableEditBean.addPropertyChangeListener(CatchLonglineDto.PROPERTY_BASKET, basketChanged);

    }

    public void initSections(LonglinePositionSetDto positionSetDto,
                             Collection<D> dtos) {

        this.sectionUniverse = positionSetDto.getSections();
        this.basketUniverse = positionSetDto.getBaskets();
        this.branchlineUniverse = positionSetDto.getBranchlines();

        for (D dto : dtos) {

            DataReference<BranchlineDto> branchline = dto.getBranchline();

            if (branchline != null) {

                DataReference<BasketDto> basket = getBasket(branchline);
                dto.setBasket(basket);

            }

            DataReference<BasketDto> basket = dto.getBasket();

            if (basket != null) {

                DataReference<SectionDto> section = getSection(basket);
                dto.setSection(section);

            }
        }
    }

    public void resetPosition(D dto) {

        DataReference<SectionDto> section = dto.getSection();
        DataReference<BasketDto> basket = dto.getBasket();
        DataReference<BranchlineDto> branchline = dto.getBranchline();

        uiBranchline.setSelectedItem(null);
        uiSection.setSelectedItem(null);
        uiBasket.setSelectedItem(null);

        if (section != null) {

            // reload section (basket and branchlines universe will then changed)
            uiSection.setSelectedItem(section);

        }

        if (basket != null) {

            // reload basket (branchlines universe will then changed)
            uiBasket.setSelectedItem(basket);

        }

        if (branchline != null) {

            // reload branchline
            uiBranchline.setSelectedItem(branchline);

        }

    }

    public void savePosition(List<D> dtos) {

        for (D dto : dtos) {

            if (dto.getBasket() != null) {
                // remove section
                dto.setSection(null);
            }

            if (dto.getBranchline() != null) {

                // remove basket
                dto.setBasket(null);
            }

        }

    }

    public List<DataReference<SectionDto>> getSections() {

        List<DataReference<SectionDto>> sections = Lists.newArrayList();

        if (sectionUniverse != null) {

            sections.addAll(sectionUniverse);

        }

        return sections;
    }

    protected void onSectionChanged(DataReference<SectionDto> newValue, D dto) {

        locationIsAdjusting = true;

        try {

            DataReference<BasketDto> basket = dto.getBasket();
            DataReference<BranchlineDto> branchline = dto.getBranchline();

            // on deselectionne le panier
            dto.setBasket(null);

            // on deselectionne l'avançon
            dto.setBranchline(null);

            // on vide l'ensemble des paniers
            uiBasket.setData(Collections.emptyList());

            // on vide l'ensemble des avançons
            uiBranchline.setData(Collections.emptyList());

            if (newValue != null) {

                // une section est sélectionnée

                // on remplit uniquement les paniers de cette section
                List<DataReference<BasketDto>> baskets = getBaskets(newValue);
                uiBasket.setData(Lists.newArrayList(baskets));

                if (basket != null && baskets.contains(basket)) {

                    // un panier est sélectionné

                    // on repmlit uniquement les avançons du panier
                    List<DataReference<BranchlineDto>> branchlines = getBranchlines(basket);
                    uiBranchline.setData(Lists.newArrayList(branchlines));
                    dto.setBasket(basket);

                    if (branchline != null && branchlines.contains(branchline)) {

                        // un avançon est sélectionné
                        dto.setBranchline(branchline);

                    }

                }

            }

        } finally {

            locationIsAdjusting = false;

        }

    }

    protected void onBasketChanged(DataReference<BasketDto> newValue, D dto) {

        if (!locationIsAdjusting) {

            DataReference<BranchlineDto> branchline = dto.getBranchline();

            // on deselectionne l'avançon
            dto.setBranchline(null);

            // on vide l'ensemble des avançons
            uiBranchline.setData(Collections.emptyList());

            if (newValue != null) {

                // un panier est selectionne

                // on remplit uniquement les avançons des paniers
                List<DataReference<BranchlineDto>> branchlines = getBranchlines(newValue);
                uiBranchline.setData(branchlines);


                if (branchline != null && branchlines.contains(branchline)) {

                    // un avançon est sélectionné
                    dto.setBranchline(branchline);

                }

            }

        }

    }

    protected DataReference<SectionDto> getSection(DataReference<BasketDto> basket) {

        String sectionId = (String) basket.getPropertyValue(BasketWithSectionIdDto.PROPERTY_SECTION_ID);

        return sectionUniverse.stream()
                              // TODO sbavencoff 11/03/2016 utiliser le prédicat AbstractReference.newIdPredicate()
                              // A la migartion vers les prédicats JAVA8
                              .filter(s -> sectionId.equals(s.getId()))
                              .findFirst()
                              .get();

    }

    protected DataReference<BasketDto> getBasket(DataReference<BranchlineDto> branchline) {

        String basketId = (String) branchline.getPropertyValue(BranchlineWithBasketIdDto.PROPERTY_BASKET_ID);

        return basketUniverse.stream()
                             // TODO sbavencoff 11/03/2016 utiliser le prédicat DataReference.newLabelValuePredicate
                             // A la migartion vers les prédicats JAVA8
                             .filter(b -> basketId.equals(b.getId()))
                             .findFirst()
                             .get();

    }

    protected List<DataReference<BasketDto>> getBaskets(DataReference<SectionDto> section) {

        return basketUniverse.stream()
                             // TODO sbavencoff 11/03/2016 utiliser le prédicat DataReference.newLabelValuePredicate
                             // A la migartion vers les prédicat JAVA8
                             .filter(b -> section.getId().equals(b.getPropertyValue(BasketWithSectionIdDto.PROPERTY_SECTION_ID)))
                             .collect(Collectors.toList());
    }

    protected List<DataReference<BranchlineDto>> getBranchlines(DataReference<BasketDto> basket) {

        return branchlineUniverse.stream()
                                 // TODO sbavencoff 11/03/2016 utilise le prédicat DataReference.newLabelValuePredicate
                                 // A la migartion vers les prédicat JAVA8
                                 .filter(b -> basket.getId().equals(b.getPropertyValue(BranchlineWithBasketIdDto.PROPERTY_BASKET_ID)))
                                 .collect(Collectors.toList());
    }

}
