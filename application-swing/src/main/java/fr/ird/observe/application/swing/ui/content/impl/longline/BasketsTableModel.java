package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.SectionTemplate;
import fr.ird.observe.services.dto.longline.BasketDto;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class BasketsTableModel extends LonglineCompositionTableModelSupport<BasketDto> {

    private static final long serialVersionUID = 1L;

    public BasketsTableModel(LonglineDetailCompositionUIModel model) {
        super(model);
    }

    @Override
    protected BasketDto createNewRow() {
        return new BasketDto();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {

        boolean result;

        switch (columnIndex) {

            case 0:

                // can never edit setting id
                result = false;
                break;

            case 1:

                // can edit hauling id if and only if set has hauling breaks
                result = !isGenerateHaulingIds();
                break;

            case 2:

                // can always change length ?
                result = true;
                break;

            case 3:

                // can always change length ?
                result = true;
                break;

            default:
                throw new IllegalStateException("Can't come here");

        }

        return result;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        BasketDto row = data.get(rowIndex);
        Object result;

        switch (columnIndex) {
            case 0:

                result = row.getSettingIdentifier();
                break;

            case 1:

                result = row.getHaulingIdentifier();
                break;

            case 2:

                result = row.getFloatline1Length();
                break;

            case 3:

                result = row.getFloatline2Length();
                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

        return result;

    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        BasketDto row = data.get(rowIndex);

        switch (columnIndex) {
            case 0:

                row.setSettingIdentifier((Integer) aValue);
                break;

            case 1:

                row.setHaulingIdentifier((Integer) aValue);
                break;

            case 2:

                row.setFloatline1Length((Float) aValue);
                break;

            case 3:

                row.setFloatline2Length((Float) aValue);
                break;

            default:
                throw new IllegalStateException("Can't come here");
        }

        setModified(true);

    }

    public void applySectionTemplate(SectionTemplate newTemplate) {

        newTemplate.applyToBaskets(data);
        fireTableRowsUpdated(0, getRowCount() - 1);
        setModified(true);

    }
}
