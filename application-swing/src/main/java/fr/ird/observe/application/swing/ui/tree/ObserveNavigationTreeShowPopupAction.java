package fr.ird.observe.application.swing.ui.tree;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI;
import fr.ird.observe.application.swing.ui.tree.menu.MoveActivityLonglineNodeMenuPopulator;
import fr.ird.observe.application.swing.ui.tree.menu.MoveActivitySeineNodeMenuPopulator;
import fr.ird.observe.application.swing.ui.tree.menu.MoveNodeMenuPopulator;
import fr.ird.observe.application.swing.ui.tree.menu.MoveRouteNodeMenuPopulator;
import fr.ird.observe.application.swing.ui.tree.menu.MoveTripNodeMenuPopulator;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/8/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class ObserveNavigationTreeShowPopupAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveNavigationTreeShowPopupAction.class);

    private static final String TRIP_MENU_ITEMS = "trip";
    private static final String ROUTE_MENU_ITEMS = "route";
    private static final String ACTIVITY_SEINE_MENU_ITEMS = "activitySeine";
    private static final String ACTIVITY_LONGLINE_MENU_ITEMS = "activityLongline";

    static {
        n("observe.navigationMenu.move.trip");
        n("observe.navigationMenu.move.route");
        n("observe.navigationMenu.move.activitySeine");
        n("observe.navigationMenu.move.activityLongline");
    }

    private final ObserveTreeHelper treeHelper;

    private final JPopupMenu popup;

    private final JTree tree;

    private final JMenuItem noAction;
    private final JMenuItem openAction;
    private final JMenuItem closeAction;
    private final JMenuItem moveAction;
    private final JMenuItem deleteAction;

    private final ImmutableMap<String, MoveNodeMenuPopulator> moveNodeDataByNodeType;

    public ObserveNavigationTreeShowPopupAction(ObserveTreeHelper treeHelper, JScrollPane pane, JPopupMenu popup) {

        this.treeHelper = treeHelper;
        this.popup = popup;
        this.tree = treeHelper.getUI();

        JMenuItem noActionComponent = null;
        // trip menus
        JMenuItem moveComponent = null;
        JMenuItem openActionComponent = null;
        JMenuItem closeActionComponent = null;
        JMenuItem deleteActionComponent = null;

        for (MenuElement menuElement : popup.getSubElements()) {

            if (menuElement.getComponent().getName().equals("navigationNoAction")) {
                noActionComponent = (JMenuItem) menuElement.getComponent();
            }
            if (menuElement.getComponent().getName().equals("navigationMoveAction")) {
                moveComponent = (JMenuItem) menuElement.getComponent();
            }
            if (menuElement.getComponent().getName().equals("navigationOpenAction")) {
                openActionComponent = (JMenuItem) menuElement.getComponent();
            }
            if (menuElement.getComponent().getName().equals("navigationCloseAction")) {
                closeActionComponent = (JMenuItem) menuElement.getComponent();
            }
            if (menuElement.getComponent().getName().equals("navigationDeleteAction")) {
                deleteActionComponent = (JMenuItem) menuElement.getComponent();
            }
        }

        this.noAction = noActionComponent;
        this.openAction = openActionComponent;
        this.closeAction = closeActionComponent;
        this.moveAction = moveComponent;
        this.deleteAction = deleteActionComponent;

        moveNodeDataByNodeType = ImmutableMap.of(TRIP_MENU_ITEMS, new MoveTripNodeMenuPopulator(),
                                                 ROUTE_MENU_ITEMS, new MoveRouteNodeMenuPopulator(),
                                                 ACTIVITY_SEINE_MENU_ITEMS, new MoveActivitySeineNodeMenuPopulator(),
                                                 ACTIVITY_LONGLINE_MENU_ITEMS, new MoveActivityLonglineNodeMenuPopulator());

        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (tree.isEnabled()) {
                    openNodeMenu(e);
                }
            }
        };
        tree.addKeyListener(keyAdapter);
        pane.addKeyListener(keyAdapter);

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (tree.isEnabled()) {
                    autoSelectNodeInTree(e);
                }
            }
        };
        tree.addMouseListener(mouseAdapter);
        pane.addMouseListener(mouseAdapter);
    }

    protected void autoSelectNodeInTree(MouseEvent e) {

        boolean rightClick = SwingUtilities.isRightMouseButton(e);

        if (rightClick) {

            // get the coordinates of the mouse click
            Point p = e.getPoint();

            int closestRowForLocation = tree.getClosestRowForLocation(e.getX(), e.getY());

            int rowToSelect = -1;

            if (isRowSelected(closestRowForLocation)) {

                rowToSelect = closestRowForLocation;
            }

            if (rowToSelect == -1) {

                // try to change selection

                TreePath pathForRow = tree.getPathForRow(closestRowForLocation);
                tree.setSelectionPath(pathForRow);

                if (isRowSelected(closestRowForLocation)) {

                    rowToSelect = closestRowForLocation;
                }

            }

            if (rowToSelect != -1) {

                showPopup(rowToSelect, p);

            }

        }
    }

    public void openNodeMenu(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_CONTEXT_MENU && !tree.isSelectionEmpty()) {

            // get the lowest selected row
            int lowestRow = getLowestSelectedRowCount();

            // get the selected column
            Rectangle r = tree.getRowBounds(lowestRow);

            // get the point in the middle lower of the cell
            Point p = new Point(r.x + r.width / 2, r.y + r.height);

            if (log.isDebugEnabled()) {
                log.debug("Row " + lowestRow + " found t point [" + p + "]");
            }

            showPopup(lowestRow, p);

        }
    }

    protected void showPopup(int row, Point p) {

        if (log.isInfoEnabled()) {
            log.info("Will show popup from row: " + row);
        }

        ObserveNode selectedNode = (ObserveNode) tree.getPathForRow(row).getLastPathComponent();

        if (log.isInfoEnabled()) {
            log.info("Found selected node: " + selectedNode);
        }

        beforeOpenPopup(selectedNode);

        popup.show(tree, p.x, p.y);

    }

    protected void beforeOpenPopup(ObserveNode selectedNode) {

        // clean popup
        popup.removeAll();

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        DataContext dataContext = applicationContext.getDataContext();
        ContentUI<?> selectedContentUI = applicationContext.getContentUIManager().getSelectedContentUI();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();

        boolean closeActionEnabled = false;

        if (selectedContentUI instanceof ContentOpenableUI) {
            JButton closeButton = ((ContentOpenableUI) selectedContentUI).getClose();
            closeActionEnabled = closeButton.isEnabled();
        }

        if (selectedNode.isTripNode()) {

            beforeOpenMenu(selectedNode, TRIP_MENU_ITEMS);

            openAction.setEnabled(!dataContext.isOpenTrip());
            closeAction.setEnabled(closeActionEnabled);

        } else if (selectedNode.isRouteNode()) {

            beforeOpenMenu(selectedNode, ROUTE_MENU_ITEMS);

            openAction.setEnabled(openDataManager.canOpenRoute(dataContext.getSelectedTripSeineId()));
            closeAction.setEnabled(closeActionEnabled);

        } else if (selectedNode.isActivitySeineNode()) {

            beforeOpenMenu(selectedNode, ACTIVITY_SEINE_MENU_ITEMS);

            openAction.setEnabled(openDataManager.canOpenActivitySeine(dataContext.getSelectedRouteId()));
            closeAction.setEnabled(closeActionEnabled);

        } else if (selectedNode.isActivityLonglineNode()) {

            beforeOpenMenu(selectedNode, ACTIVITY_LONGLINE_MENU_ITEMS);

            openAction.setEnabled(openDataManager.canOpenActivityLongline(dataContext.getSelectedTripLonglineId()));
            closeAction.setEnabled(closeActionEnabled);

        } else {

            popup.add(noAction);
        }
    }

    protected void beforeOpenMenu(ObserveNode selectedNode, String nodeType) {
        if (log.isInfoEnabled()) {
            log.info("Will load popup for " + nodeType + " node.");
        }

        MoveNodeMenuPopulator moveNodeData = moveNodeDataByNodeType.get(nodeType);

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();

        if (selectedNode.isOpen()) {

            closeAction.putClientProperty("ui", applicationContext.getContentUIManager().getSelectedContentUI());
            popup.add(closeAction);


        } else {

            openAction.putClientProperty("ui", applicationContext.getContentUIManager().getSelectedContentUI());
            popup.add(openAction);

        }

        moveAction.setText(t("observe.navigationMenu.move." + nodeType));
        moveAction.setToolTipText(t("observe.navigationMenu.move." + nodeType));
        moveAction.setIcon(SwingUtil.getUIManagerActionIcon("move-" + nodeType));
        popup.add(moveAction);

        moveAction.removeAll();

        // get the available program for the trip

        String id = selectedNode.getId();

        ObserveSwingDataSource dataSource = treeHelper.getDataProvider().getDataSource();

        List<DecoratedNodeEntity> possibleParentNodes = moveNodeData.getPossibleParentNodes(selectedNode, treeHelper);

        for (DecoratedNodeEntity possibleParent : possibleParentNodes) {

            String possibleParentId = possibleParent.getId();
            JMenuItem item = new JMenuItem(possibleParent.toString());
            item.setName(possibleParentId);


            item.addActionListener(moveNodeData.createChangeActionListener(treeHelper,
                                                                           dataSource,
                                                                           id,
                                                                           possibleParentId));

            moveAction.add(item);
        }

        deleteAction.putClientProperty("ui", applicationContext.getContentUIManager().getSelectedContentUI());
        deleteAction.setEnabled(selectedNode.isOpen());
        popup.add(deleteAction);
    }

    protected boolean isRowSelected(int requiredRow) {

        boolean result = false;

        int[] selectedRows = tree.getSelectionRows();
        if (selectedRows != null) {
            for (int selectedRow : selectedRows) {
                if (requiredRow == selectedRow) {

                    // match
                    result = true;
                    break;
                }
            }
        }

        return result;

    }

    protected int getLowestSelectedRowCount() {

        Preconditions.checkState(!tree.isSelectionEmpty());

        int[] selectedRows = tree.getSelectionRows();
        int lowestRow = -1;
        if (selectedRows != null) {
            for (int row : selectedRows) {
                lowestRow = Math.max(lowestRow, row);
            }
        }
        return lowestRow;

    }

}
