package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.SetLonglineTdrDto;
import fr.ird.observe.services.dto.longline.TdrDto;
import fr.ird.observe.services.dto.longline.TdrDtos;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created on 9/30/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class TdrUIModel extends ContentTableUIModel<SetLonglineTdrDto, TdrDto> {

    public static final Set<String> CARACTERISTIC_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(TdrDto.PROPERTY_HOME_ID,
                                               TdrDto.PROPERTY_SERIAL_NO,
                                               TdrDto.PROPERTY_SENSOR_BRAND,
                                               TdrDto.PROPERTY_HAS_DATA,
                                               TdrDto.PROPERTY_DATA).build();

    public static final Set<String> LOCALISATION_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(TdrDto.PROPERTY_SECTION,
                                               TdrDto.PROPERTY_BASKET,
                                               TdrDto.PROPERTY_BRANCHLINE,
                                               TdrDto.PROPERTY_ITEM_HORIZONTAL_POSITION,
                                               TdrDto.PROPERTY_ITEM_VERTICAL_POSITION,
                                               TdrDto.PROPERTY_FLOATLINE1_LENGTH,
                                               TdrDto.PROPERTY_FLOATLINE2_LENGTH).build();


    public static final Set<String> TIMESTAMP_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(TdrDto.PROPERTY_DEPLOYEMENT_START,
                                               TdrDto.PROPERTY_DEPLOYEMENT_END,
                                               TdrDto.PROPERTY_FISHING_START,
                                               TdrDto.PROPERTY_FISHING_END
            ).build();

    public static final Set<String> KEY_DATA_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(TdrDto.PROPERTY_FISHING_START_DEPTH,
                                               TdrDto.PROPERTY_FISHING_END_DEPTH,
                                               TdrDto.PROPERTY_MEAN_DEPLOYEMENT_DEPTH,
                                               TdrDto.PROPERTY_MEDIAN_DEPLOYEMENT_DEPTH,
                                               TdrDto.PROPERTY_MEAN_FISHING_DEPTH,
                                               TdrDto.PROPERTY_MEDIAN_FISHING_DEPTH,
                                               TdrDto.PROPERTY_MIN_FISHING_DEPTH,
                                               TdrDto.PROPERTY_MAX_FISHING_DEPTH).build();

    public static final Set<String> SPECIES_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(TdrDto.PROPERTY_SPECIES).build();

    public static final String PROPERTY_CARACTERISTICS_TAB_VALID = "caracteristicsTabValid";

    public static final String PROPERTY_LOCALISATION_TAB_VALID = "localisationTabValid";

    public static final String PROPERTY_TIMESTAMP_TAB_VALID = "timestampTabValid";

    public static final String PROPERTY_KEY_DATA_TAB_VALID = "keyDataTabValid";

    public static final String PROPERTY_SPECIES_TAB_VALID = "speciesTabValid";

    private static final long serialVersionUID = 1L;

    protected boolean caracteristicsTabValid;

    protected boolean localisationTabValid;

    protected boolean timestampTabValid;

    protected boolean keyDataTabValid;

    protected boolean speciesTabValid;

    public TdrUIModel(TdrUI ui) {
        super(SetLonglineTdrDto.class,
              TdrDto.class,
              new String[]{
                      SetLonglineTdrDto.PROPERTY_TDR,
                      SetLonglineTdrDto.PROPERTY_SETTING_START_TIME_STAMP
              },
              new String[]{

                      // caracteristics tab
                      TdrDto.PROPERTY_HOME_ID,
                      TdrDto.PROPERTY_SERIAL_NO,
                      TdrDto.PROPERTY_SENSOR_BRAND,
                      TdrDto.PROPERTY_HAS_DATA,
                      TdrDto.PROPERTY_DATA,

                      // localisation tab
                      TdrDto.PROPERTY_SECTION,
                      TdrDto.PROPERTY_BASKET,
                      TdrDto.PROPERTY_BRANCHLINE,
                      TdrDto.PROPERTY_ITEM_HORIZONTAL_POSITION,
                      TdrDto.PROPERTY_ITEM_VERTICAL_POSITION,
                      TdrDto.PROPERTY_FLOATLINE1_LENGTH,
                      TdrDto.PROPERTY_FLOATLINE2_LENGTH,

                      // timestamp tab
                      TdrDto.PROPERTY_DEPLOYEMENT_START,
                      TdrDto.PROPERTY_DEPLOYEMENT_END,
                      TdrDto.PROPERTY_FISHING_START,
                      TdrDto.PROPERTY_FISHING_END,

                      // key data tab
                      TdrDto.PROPERTY_FISHING_START_DEPTH,
                      TdrDto.PROPERTY_FISHING_END_DEPTH,
                      TdrDto.PROPERTY_MEAN_DEPLOYEMENT_DEPTH,
                      TdrDto.PROPERTY_MEDIAN_DEPLOYEMENT_DEPTH,
                      TdrDto.PROPERTY_MEAN_FISHING_DEPTH,
                      TdrDto.PROPERTY_MEDIAN_FISHING_DEPTH,
                      TdrDto.PROPERTY_MIN_FISHING_DEPTH,
                      TdrDto.PROPERTY_MAX_FISHING_DEPTH,

                      // species tab
                      TdrDto.PROPERTY_SPECIES});

        List<ContentTableMeta<TdrDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(TdrDto.class, TdrDto.PROPERTY_HOME_ID, false),
                ContentTableModel.newTableMeta(TdrDto.class, TdrDto.PROPERTY_SERIAL_NO, false),
                ContentTableModel.newTableMeta(TdrDto.class, TdrDto.PROPERTY_SENSOR_BRAND, false),
                ContentTableModel.newTableMeta(TdrDto.class, TdrDto.PROPERTY_HAS_DATA, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<SetLonglineTdrDto, TdrDto> createTableModel(
            ObserveContentTableUI<SetLonglineTdrDto, TdrDto> ui,
            List<ContentTableMeta<TdrDto>> contentTableMetas) {
        return new ContentTableModel<SetLonglineTdrDto, TdrDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<TdrDto> getChilds(SetLonglineTdrDto bean) {
                return bean.getTdr();
            }

            @Override
            protected void load(TdrDto source, TdrDto target) {
                TdrDtos.copyTdrDto(source, target);
            }

            @Override
            protected void setChilds(SetLonglineTdrDto parent, List<TdrDto> childs) {
                parent.setTdr(childs);
            }
        };
    }

    public boolean isCaracteristicsTabValid() {
        return caracteristicsTabValid;
    }

    public void setCaracteristicsTabValid(boolean caracteristicsTabValid) {
        this.caracteristicsTabValid = caracteristicsTabValid;
        firePropertyChange(PROPERTY_CARACTERISTICS_TAB_VALID, null, caracteristicsTabValid);
    }

    public boolean isLocalisationTabValid() {
        return localisationTabValid;
    }

    public void setLocalisationTabValid(boolean localisationTabValid) {
        this.localisationTabValid = localisationTabValid;
        firePropertyChange(PROPERTY_LOCALISATION_TAB_VALID, null, localisationTabValid);
    }

    public boolean isKeyDataTabValid() {
        return keyDataTabValid;
    }

    public void setKeyDataTabValid(boolean keyDataTabValid) {
        this.keyDataTabValid = keyDataTabValid;
        firePropertyChange(PROPERTY_KEY_DATA_TAB_VALID, null, keyDataTabValid);
    }

    public boolean isSpeciesTabValid() {
        return speciesTabValid;
    }

    public void setSpeciesTabValid(boolean speciesTabValid) {
        this.speciesTabValid = speciesTabValid;
        firePropertyChange(PROPERTY_SPECIES_TAB_VALID, null, speciesTabValid);
    }

    public boolean isTimestampTabValid() {
        return timestampTabValid;
    }

    public void setTimestampTabValid(boolean timestampTabValid) {
        this.timestampTabValid = timestampTabValid;
        firePropertyChange(PROPERTY_TIMESTAMP_TAB_VALID, null, timestampTabValid);
    }
}
