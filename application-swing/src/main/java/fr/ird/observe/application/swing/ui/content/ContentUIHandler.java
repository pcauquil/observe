/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.shared.AbstractUIAction;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIHandler;
import fr.ird.observe.application.swing.ui.tree.AbstractObserveTreeCellRenderer;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.util.SpringUtilities;
import fr.ird.observe.application.swing.validation.ValidationContext;
import fr.ird.observe.services.dto.AbstractObserveDto;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinition;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestDefinitions;
import fr.ird.observe.services.dto.reference.request.ReferenceSetRequestKeyDefinition;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.JAXXButtonGroup;
import jaxx.runtime.swing.editor.bean.BeanComboBox;
import jaxx.runtime.swing.editor.bean.BeanListHeader;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.select.FilterableDoubleList;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SpringLayout;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur d'un écran d'édition.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class ContentUIHandler<E extends IdDto> {

    /** Logger */
    static private final Log log = LogFactory.getLog(ContentUIHandler.class);

    protected final String prefix;

    private final DataContextType parentType;

    private final DataContextType type;

    private final Icon errorIcon;

    /** l'interface graphique que le controleur utilise */
    protected final ObserveContentUI<E> ui;

    static <E extends IdDto> ContentUIHandler<E> newHandler(ObserveContentUI<E> ui) {

        String uiName = ui.getClass().getName();
        String modelName = uiName + "Handler";

        try {

            Class<ContentUIHandler<E>> handlerClass = (Class<ContentUIHandler<E>>) Class.forName(modelName);
            return ConstructorUtils.invokeConstructor(handlerClass, ui);

        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("Could not create handler for ui: " + ui, e);
        }

    }

    public static ObserveTreeHelper getTreeHelper(JAXXContext context) {
        return context.getContextValue(ObserveTreeHelper.class);
    }

    /**
     * @param ui the current ui to close
     * @return {@code true} if ui can be safelty closed, {@code false}
     * otherwise.
     */
    protected static boolean checkEdit(ObserveContentUI<?> ui) {

        ContentUIModel<? extends AbstractObserveDto> model = ui.getModel();

        if (model.isCreatingMode()) {

            // on peut quitter le mode creation si le modele est non modifie et valide
            if (!(model.isEnabled() && model.isEditing() && (model.isModified() || !model.isValid()))) {
                if (model.isEditing()) {
                    ui.stopEdit();
                }
                // ui was not in edit mode, nor modified in edit mode, so can
                // safely quit
                return true;
            }
        } else {

            // on peut quiiter le mode mise a jour si le modele est non modifie
            if (!(model.isEnabled() && model.isEditing() && model.isModified())) {
                if (model.isEditing()) {
                    ui.stopEdit();
                }
                // ui was not in edit mode, nor modified in edit mode, so can
                // safely quit
                return true;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("previousUI is in edit mode");
        }

        boolean wasClosed = true;

        if (!ui.getDataSource().isExpired()) {
            if (model.isValid()) {
                // ask user if wants to save
                int reponse = UIHelper.askUser(
                        (JComponent) ui,
                        t("observe.title.need.confirm"),
                        t("observe.message.quit.valid.edit"),
                        JOptionPane.WARNING_MESSAGE,
                        new Object[]{
                                t("observe.choice.save"),
                                t("observe.choice.doNotSave"),
                                t("observe.choice.cancel")},
                        0);
                if (log.isDebugEnabled()) {
                    log.debug("response : " + reponse);
                }

                switch (reponse) {
                    case JOptionPane.CLOSED_OPTION:
                    case 2:
                        wasClosed = false;
                        break;
                    case 0:
                        // will save ui
                        ui.save(false);
                        break;
                    case 1:
                        // wil reset ui
                        ui.resetEdit();
                        break;
                }

            } else {
                // ask user if wants to quit without saving since edit is not valid
                int reponse = UIHelper.askUser(
                        (JComponent) ui,
                        t("observe.title.need.confirm"),
                        t("observe.message.quit.invalid.edit"),
                        JOptionPane.ERROR_MESSAGE,
                        new Object[]{
                                t("observe.choice.continue"),
                                t("observe.choice.cancel")},
                        0);
                if (log.isDebugEnabled()) {
                    log.debug("response : " + reponse);
                }
                switch (reponse) {
                    case JOptionPane.CLOSED_OPTION:
                    case 1:
                        wasClosed = false;
                        break;
                    case 0:
                        // wil reset ui
                        ui.resetEdit();
                        break;
                }
            }
        }
        if (wasClosed) {
            ui.stopEdit();
        }
        return wasClosed;
    }

    public static void addMessage(ObserveContentUI<?> ui, NuitonValidatorScope scope, String fieldName, String message) {
        SwingValidatorMessageTableModel model = ui.getErrorTableModel();
        model.addMessages((JComponent) ui, fieldName, scope, message);
    }

    public static void removeAllMessages(ContentUI<?> ui, NuitonValidatorScope scope, String fieldName) {
        SwingValidatorMessageTableModel model = ui.getErrorTableModel();
        model.removeMessages(ui, fieldName, scope);
    }

    protected static void removeAllMessages(ObserveContentUI<?> ui) {
        SwingValidatorMessageTableModel model = ui.getErrorTableModel();
        model.removeMessages((JComponent) ui, null);
    }

    public ContentUIHandler(ObserveContentUI<E> ui, DataContextType parentType, DataContextType type) {
        this.ui = ui;
        this.parentType = parentType;
        this.type = type;
        prefix = "[" + ui.getClass().getSimpleName() + "] ";
        if (log.isDebugEnabled()) {
            log.debug("New handler [" + this + "] for ui " + prefix);
        }
        errorIcon = UIManager.getIcon("action.error");
        Locale locale = ObserveSwingApplicationContext.get().getConfig().getLocale();
        if (Locale.ENGLISH.equals(locale)) {
            dateFormat = FastDateFormat.getInstance("yyyy-MM-ddZZ hh:mm:ss");
        } else {
            dateFormat = FastDateFormat.getInstance("dd/MM/yyyy hh:mm:ss");
        }
    }

    public ObserveContentUI<E> getUi() {
        return ui;
    }

    public ContentUIModel<E> getModel() {
        return getUi().getModel();
    }

    public Class<E> getBeanType() {
        return getModel().getBeanType();
    }

    public E getBean() {
        ContentUIModel<E> model = getModel();
        return model.getBean();
    }

    public DecoratorService getDecoratorService() {
        return ObserveSwingApplicationContext.get().getDecoratorService();
    }

    public void initUI() {

        final ContentUIInitializer<E, ObserveContentUI<E>> uiInitializer = new ContentUIInitializer<>(ui);
        uiInitializer.initUI();

        getModel().addPropertyChangeListener(ContentUIModel.PROPERTY_FORM, evt -> updateUiWithReferenceSetsFromModel());

    }

    public void openUI() {

        boolean canWrite = computeCanWrite(getDataSource());

        getModel().setCanWrite(canWrite);

        //FIXME chemit 20100913 : il vaudrait le faire uniquement lors de l'édition
        // chaque arrive sur un ecran invalide le cache de validation
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ValidationContext context = applicationContext.getValidationContext();
        context.cleanCache();

        if (log.isDebugEnabled()) {
            log.debug("ui " + ui.getClass());
        }
        // suppression des messages de validation
        removeAllMessages(ui);
        ObserveTreeHelper treeHelper = getTreeHelper(ui);
        AbstractObserveTreeCellRenderer render =
                treeHelper.getTreeCellRenderer();
        ObserveNode node = treeHelper.getSelectedNode();
        if (ContentReferenceUIHandler.class.isAssignableFrom(getClass())) {
            node = node.getParent();
        }
        ui.setContentIcon(render.getNavigationIcon(node));

        updateActions();
    }

    public Icon getErrorIconIfFalse(boolean valid) {
        Icon icon = null;
        if (!valid) {
            icon = errorIcon;
        }
        return icon;
    }

    public void startEditUI(String... binding) {

        E editBean = getBean();

        prepareValidationContext();

        // reset all validators
        SwingValidatorUtil.setValidatorBean(ui, null);
        // mark ui as editing
        getModel().setEditing(true);

        // attach validators
        SwingValidatorUtil.setValidatorBean(ui, editBean);
        if (binding.length > 0) {

            // force widget binding
            UIHelper.processDataBinding(ui, binding);
        }
        UIHelper.processDataBinding(ui, "reset.enabled", "save.enabled");
    }

    public void stopEditUI() {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ValidationContext context = applicationContext.getValidationContext();
        context.cleanCache();

        ContentUIModel<E> model = getModel();

        // mark ui as not editing
        model.setEditing(false);

        // mark ui as valid while not editing
        model.setValid(true);

        // mark ui as not modified
        model.setModified(false);

        // removed initial edit object from context
        ui.removeContextValue(model.getBeanType(), "edit");

        // dettach all validators
        SwingValidatorUtil.setValidatorBean(ui, null);
    }

    public void resetEditUI() {

        // on arrete l'edition
        stopEditUI();

        // suppression des messages de validation
        removeAllMessages(ui);

        // on re-ouvre l'écran d'édition
        try {
            openUI();
        } catch (Exception ex) {
            UIHelper.handlingError(ex);
            stopEditUI();
        }
    }

    void restartEditUI() {

        ContentUIModel<E> model = getModel();

        if (!model.isEditable()) {

            // l'ecran n'est pas éditable, on ne re-ouvre pas
            // on quite de suite
            return;
        }
        if (log.isInfoEnabled()) {
            log.info(prefix + "Will restart edit " + getUi().getClass().getName());
        }

        // on ne peut redemarrer une edition que si la donnee
        // est exactement une entites (pas possible sur une liste)
        ObserveTreeHelper treeHelper = getTreeHelper(ui);
//        ObserveNode node = treeHelper.getSelectedNode();

        removeAllMessages(ui);

//        treeHelper.refreshNode(node, true);
        treeHelper.reloadSelectedNode(false, true);

        model.setMode(ContentMode.UPDATE);

        // redemarrage de l'édition
        ui.startEdit(null);

        updateActions();
    }

    protected final void saveUI(boolean refresh) {
        boolean ok = false;
        try {
            ok = doSave(getBean());

        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {
            if (ok) {
                afterSave(refresh);
            }
        }
    }

    final boolean closeUI() {
        boolean b = checkEdit(ui);
        if (log.isDebugEnabled()) {
            log.debug("Can close " + ui.getClass() + " : " + b);
        }
        if (b) {
            closeSafeUI();
        }
        return b;
    }

    final void deleteUI() {
        boolean ok = false;
        ui.stopEdit();
        removeAllMessages(ui);

        try {
            ok = doDelete(getBean());

        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {
            if (ok) {
                afterDelete();
            }
        }
    }

    protected <C extends DataDto> void gotoChild(DataReference<C> entity) {
        if (entity == null) {

            // rien a faire
            return;
        }
        ObserveTreeHelper treeHelper = getTreeHelper(ui);
        ObserveNode parentNode = treeHelper.getSelectedNode();
        String id = entity.getId();
        ObserveNode node = treeHelper.findNode(parentNode, id);
        if (log.isInfoEnabled()) {
            log.info("will go to node " + node + " for " + id);
        }
        treeHelper.selectNode(node);
    }

    public void addChild(Class<?> type) {
        ObserveTreeHelper treeHelper = getTreeHelper(ui);
        ObserveNode parentNode = treeHelper.getSelectedNode();
        treeHelper.addUnsavedNode(parentNode, type);
    }

    protected <EE> List<EE> updateList(BeanListHeader<EE> list, List<EE> data, String message) {
        if (data != null && !data.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug(list.getName() + " - " + data.size());
            }
            Decorator<EE> decorator = list.getHandler().getDecorator();
            DecoratorUtil.sort((JXPathDecorator<EE>) decorator, data, 0);
            return data;
        } else {
            return new ArrayList<>();
        }
    }

    public void openLink(String url) {

        try {

            UIHelper.openLink(url);
        } catch (Exception e) {
            UIHelper.handlingError(e);
        }
    }

    String updateTitle(String title) {
        if (ContentReferenceUIHandler.class.isAssignableFrom(getClass())) {
            return ObserveI18nDecoratorHelper.getTypeI18nKey(getBeanType());
        }
        return title;
    }

    protected void loadReferentialReferenceSetsInModel(Form<E> form) {

        String requestName = form.getReferentialReferenceSetsRequestName();

        if (requestName != null) {

            loadReferentialReferenceSetsInModel(requestName);

        }

    }

    protected void loadReferentialReferenceSetsInModel(String requestName) {

        ImmutableMap.Builder<String, ReferentialReferenceSet<?>> modelReferentialReferenceSets = ImmutableMap.builder();

        if (log.isInfoEnabled()) {
            log.info("Update referential reference sets for: " + requestName);
        }

        // mettre à jour le cache de référentiel
        ImmutableMap<Class<?>, ReferentialReferenceSet<?>> referentialReferenceSetsByType = getDataSource().updateReferentialReferenceSetsCache(requestName);

        // calculer les listes de référentiels à utiliser dans le modèle

        ReferenceSetRequestDefinition requestDefinition = ReferenceSetRequestDefinitions.get(requestName);

        for (ReferenceSetRequestKeyDefinition propertyDefinition : requestDefinition.getPropertyDefinitions()) {

            if (!propertyDefinition.isReferential()) {
                continue;
            }

            String propertyName = propertyDefinition.getName();
            ReferentialReferenceSet referentialReferenceSet = referentialReferenceSetsByType.get(propertyDefinition.getType());
            ReferentialReferenceSet filteredReferentialReferenceSet = filterReferentialReferenceSet(propertyDefinition, referentialReferenceSet);

            modelReferentialReferenceSets.put(propertyName, filteredReferentialReferenceSet);

        }

        getModel().setReferentialReferenceSets(modelReferentialReferenceSets.build());

    }

    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        if (log.isDebugEnabled()) {
            log.debug(String.format("Filter referential references (type %s - property %s), original size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
        }
        // by default always remove obsolete references
        List<ReferentialReference<D>> referentialReferences = ReferentialReferences.filterEnabled(incomingReferences);
        if (log.isDebugEnabled()) {
            log.debug(String.format("Filter referential references (type %s - property %s), without diabled size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
        }
        return referentialReferences;

    }

    /**
     * Calcule le mode de l'écran.
     *
     * @param dataContext le context de données
     * @return {@code null} si l'écran est éditable, autrement le mode
     * restreint READ  @param dataContext le context de données
     */
    protected abstract ContentMode getContentMode(DataContext dataContext);

    /**
     * Pour calculer la propriete {@code canWrite} du modèle.
     *
     * @param dataSsource la base source de l'ui
     * @return {@code true} si on peut écrire (donc éditer), {@code false} autrement.
     */
    protected abstract boolean computeCanWrite(ObserveSwingDataSource dataSsource);

    protected final String getSelectedParentId() {
        String s = null;
        if (parentType != null) {
            s = parentType.getSelectedId(getDataContext());
        }
        return s;
    }

    protected final String getSelectedId() {
        String s = null;
        if (type != null) {
            s = type.getSelectedId(getDataContext());
        }
        return s;
    }

    protected DataContext getDataContext() {
        return ui.getDataContext();
    }

    protected ObserveSwingDataSource getDataSource() {
        return ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
    }

    protected ObserveOpenDataManager getOpenDataManager() {
        return ObserveSwingApplicationContext.get().getOpenDataManager();
    }

    protected boolean doSave(E bean) throws Exception {
        return false;
    }

    protected void afterSave(boolean refresh) {

        // mettre les validateurs en mode non modifié
        SwingValidatorUtil.setValidatorChanged(ui, false);
    }

    protected void closeSafeUI() {

        removeAllMessages(ui);
        ContentUIModel<E> model = getModel();

        boolean create = model.isCreatingMode();
        if (create && model.isEditable()) {
            // remove node
            ObserveTreeHelper treehelper = getTreeHelper(ui);
            ObserveNode node = treehelper.getSelectedNode();

            ObserveNode parentNode = node.getParent();
            if (parentNode != null) {
                // node still attached, so remove it
                treehelper.removeNode(node);
                treehelper.selectNode(parentNode);
            }
        }

    }

    protected boolean doDelete(E bean) {
        return false;
    }

    protected boolean askToDelete(E bean) {
        boolean accept =
                UIHelper.confirmForEntityDelete(ui, getBeanType(), bean);

        return !accept;
    }

    protected void afterDelete() {
        ui.stopEdit();
        removeAllMessages(ui);
        if (!(getBean() instanceof ReferentialDto)) {

            ObserveTreeHelper treeHelper = getTreeHelper(ui);
            ObserveNode node = treeHelper.getSelectedNode();
            ObserveNode parentNode = treeHelper.removeNode(node);
            treeHelper.selectNode(parentNode);
        }
    }

    protected void addInfoMessage(String message) {
        addMessage(ui,
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(getModel().getBeanType()),
                   t(message)
        );
    }

    protected String getTypeI18nKey(Class<?> klass) {
        return ObserveI18nDecoratorHelper.getTypeI18nKey(klass);
    }

    protected void prepareValidationContext() {

        // reset validation cache
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ValidationContext context = applicationContext.getValidationContext();
        context.cleanCache();
    }

    protected final ContentMode computeContentMode() {
        ContentMode mode;

        if (!getModel().isCanWrite()) {
            if (ReferentialDto.class.isAssignableFrom(getBeanType())) {
                addInfoMessage(t("observe.message.referentiel.no.editable"));
            } else {
                addInfoMessage(t("observe.message.can.not.write.data"));
            }
            mode = ContentMode.READ;
        } else {

            // ask specified handler which mode to use
            mode = getContentMode(getDataContext());
        }
        return mode;
    }

    protected final String getValidatorContextName(ContentMode mode) {
        return mode == ContentMode.CREATE ? "ui-create" : "ui-update";
    }

    protected void updateActions() {

        updateToolbarActions();

        ActionMap actionMap = ObserveSwingApplicationContext.get().getActionMap();

        for (String name : ui.get$objectMap().keySet()) {
            Object o = ui.getObjectById(name);

            if (o instanceof AbstractButton) {

                AbstractButton button = (AbstractButton) o;

                String actionId = (String) button.getClientProperty(ContentUIInitializer.OBSERVE_ACTION);

                if (actionId == null) {
                    continue;
                }
                AbstractUIAction action = (AbstractUIAction) actionMap.get(actionId);

                if (log.isDebugEnabled()) {
                    log.debug("update common action " + actionId);
                }

                action.updateAction(ui, button);
            }
        }
    }

    protected void updateToolbarActions() {
        ObserveContentUI<E> ui = getUi();
        JToolBar toolBar = ui.getTitleRightToolBar();
        Component[] components = toolBar.getComponents();
        for (Component component : components) {
            if (component instanceof JComponent) {
                JComponent jcompo = (JComponent) component;
                if (jcompo.getClientProperty("original") != null) {

                    // on conserve ce component
                    // on le rend de nouveau visible (il se peut que certains écrans le cache)
                    jcompo.setEnabled(true);
                    continue;
                }
            }

            toolBar.remove(component);
        }
    }

    Icon updateModeIcon(ContentMode mode) {
        Icon icon = null;
        if (mode != null) {
            icon = (Icon) ((JComponent) ui).getClientProperty(mode.name() + "Icon");
        }
        return icon;
    }

    String updateModeTip(ContentMode mode) {
        String tip = null;
        if (mode != null) {
            tip = (String) ((JComponent) ui).getClientProperty(mode.name() + "Tip");
        }
        return tip;
    }

    protected void setContentMode(ContentMode newMode) {
        // pour reforcer le binding sur le mode
        // cela est nécessaire car le mode peut-être positionné avant d'arriver
        // sur le bon noeud et donc les calculs d'accessibilité de certaines
        // actions ne sont pas corrects (par exemple action goDown)
        getModel().setMode(null);
        getModel().setMode(newMode);
    }

    //FIXME Do it in jaxx
    protected void resetQuadrant(CoordinatesEditor editor) {

        editor.setQuadrant(null);
        JAXXButtonGroup quadrantBG = editor.getQuadrantBG();

        quadrantBG.setSelectedValue(null);
        quadrantBG.setSelectedButton(null);

        quadrantBG.remove(editor.getQuadrant1());
        quadrantBG.remove(editor.getQuadrant2());
        quadrantBG.remove(editor.getQuadrant3());
        quadrantBG.remove(editor.getQuadrant4());

        editor.getQuadrant1().setSelected(false);
        editor.getQuadrant2().setSelected(false);
        editor.getQuadrant3().setSelected(false);
        editor.getQuadrant4().setSelected(false);

        quadrantBG.add(editor.getQuadrant1());
        quadrantBG.add(editor.getQuadrant2());
        quadrantBG.add(editor.getQuadrant3());
        quadrantBG.add(editor.getQuadrant4());

    }

    /**
     * Pour mettre à jour les composants graphiques avec les référentiels chargés dans le modèle
     */
    protected void updateUiWithReferenceSetsFromModel() {

        for (String name : ui.get$objectMap().keySet()) {
            Object o = ui.getObjectById(name);

            if (o == null) {
                continue;
            }

            if (o instanceof BeanComboBox) {

                BeanComboBox beanComboBox = (BeanComboBox) o;

                Class dtoClass = getDtoClass(beanComboBox);
                if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
                    updateReferentialBeanComboBox(dtoClass, beanComboBox);
                } else {
                    updateDataBeanComboBox(dtoClass, beanComboBox);
                }

                continue;
            }

            if (o instanceof BeanListHeader) {

                BeanListHeader beanListHeader = (BeanListHeader) o;

                Class dtoClass = getDtoClass(beanListHeader);
                if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
                    updateReferentialBeanListHeader(dtoClass, beanListHeader);
                } else {
                    updateDataBeanListHeader(dtoClass, beanListHeader);
                }

                continue;
            }

            if (o instanceof FilterableDoubleList) {

                FilterableDoubleList filterableDoubleList = (FilterableDoubleList) o;

                Class dtoClass = getDtoClass(filterableDoubleList);
                if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
                    updateReferentialFilterableDoubleList(dtoClass, filterableDoubleList);
                } else {
                    updateDataFilterableDoubleList(dtoClass, filterableDoubleList);
                }

            }

        }

    }

    private <R extends DataDto> void updateDataFilterableDoubleList(Class<R> dtoClass, FilterableDoubleList<DataReference<R>> list) {

        List<DataReference<R>> data;

        if (getModel().getForm() == null) {
            data = Collections.emptyList();
        } else {

            Set<DataReference<R>> referenceSetDto = getModel().getDataReferences(list.getModel().getProperty());
            data = new ArrayList<>(referenceSetDto);

        }

        // sort data from first decorator context
        DataReferenceDecorator<R> decorator = getDataReferenceDecorator(dtoClass);
        DecoratorUtil.sort(decorator, data, 0);

        //FIXME A finir (bien vérifier que la sélection n'est plus dans l'univers)
        List<DataReference<R>> selected = list.getModel().getSelected();
        list.setUniverse(data);
        list.setSelected(selected);

        list.putClientProperty("data", data);
    }

    private <R extends ReferentialDto> void updateReferentialFilterableDoubleList(Class<R> dtoClass, FilterableDoubleList<ReferentialReference<R>> list) {

        Boolean forceLoadComboBox = (Boolean) list.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_FORCE_LOAD);

        List<ReferentialReference<R>> data;

        if (BooleanUtils.isNotTrue(forceLoadComboBox) && getModel().getForm() == null) {
            data = Collections.emptyList();
        } else {

            Set<ReferentialReference<R>> referenceSetDto = getModel().getReferentialReferences(list.getModel().getProperty());
            data = new ArrayList<>(referenceSetDto);

        }

        // sort data from first decorator context
        ReferentialReferenceDecorator<R> decorator = getReferentialReferenceDecorator(dtoClass);
        DecoratorUtil.sort(decorator, data, 0);

        //FIXME A finir (bien vérifier que la sélection n'est plus dans l'univers)
        List<ReferentialReference<R>> selected = list.getModel().getSelected();
        list.setUniverse(data);
        list.setSelected(selected);

        list.putClientProperty("data", data);
    }

    protected <R extends ReferentialDto> void updateReferentialBeanListHeader(Class<R> dtoClass, BeanListHeader<ReferentialReference<R>> list) {

        Boolean noLoad = (Boolean) list.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_LIST_NO_LOAD);

        List<ReferentialReference<R>> data;

        String propertyName = list.getName();

        if (!"referentialListHeader".equals(propertyName) && (BooleanUtils.isTrue(noLoad) || getModel().getForm() == null)) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Skip loading of BeanListHeader [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            }
            data = Collections.emptyList();

        } else {

            Set<ReferentialReference<R>> referenceSetDto = getModel().getReferentialReferences(propertyName);
            data = new ArrayList<>(referenceSetDto);

        }

        // sort data from first decorator context
        ReferentialReferenceDecorator<R> decorator = getReferentialReferenceDecorator(dtoClass);
        DecoratorUtil.sort(decorator, data, 0);

        list.setData(data);
        list.putClientProperty("data", data);
        list.getList().setListData(data.toArray());

    }

    private <R extends DataDto> void updateDataBeanListHeader(Class<R> dtoClass, BeanListHeader<DataReference<R>> list) {

        Boolean noLoad = (Boolean) list.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_LIST_NO_LOAD);

        List<DataReference<R>> data;

        String propertyName = list.getName();

        if (BooleanUtils.isTrue(noLoad) || getModel().getForm() == null) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Skip loading of BeanListHeader [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            }
            data = Collections.emptyList();

        } else {

            Set<DataReference<R>> referenceSetDto = getModel().getDataReferences(propertyName);
            data = new ArrayList<>(referenceSetDto);

        }

        // sort data from first decorator context
        DataReferenceDecorator<R> decorator = getDataReferenceDecorator(dtoClass);
        DecoratorUtil.sort(decorator, data, 0);

        list.setData(data);
        list.putClientProperty("data", data);

    }

    private <R extends ReferentialDto> void updateReferentialBeanComboBox(Class<R> dtoClass, BeanComboBox<ReferentialReference<R>> comboBox) {

        Boolean noLoad = (Boolean) comboBox.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_LIST_NO_LOAD);
        Boolean forceLoadComboBox = (Boolean) comboBox.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_FORCE_LOAD);
        String propertyName = comboBox.getProperty();

        List<ReferentialReference<R>> data;

        if (BooleanUtils.isNotTrue(forceLoadComboBox) && (BooleanUtils.isTrue(noLoad) || getModel().getForm() == null)) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Skip loading of comboBox [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            }
            data = Collections.emptyList();

        } else {

            Optional<Set<ReferentialReference<R>>> optionalReferenceSetDto = getModel().tryToGetReferentialReferenceSet(propertyName);

            if (optionalReferenceSetDto.isPresent()) {

                Set<ReferentialReference<R>> references = optionalReferenceSetDto.get();
                data = Lists.newArrayList(references);

            } else {

                data = Collections.emptyList();

            }

        }

        if (log.isInfoEnabled()) {
            log.info(String.format("comboBox [%s-%s] : %d", dtoClass.getSimpleName(), propertyName, data.size()));
        }

        comboBox.setData(data);

    }

    private <R extends DataDto> void updateDataBeanComboBox(Class<R> dtoClass, BeanComboBox<DataReference<R>> comboBox) {

        Boolean noLoad = (Boolean) comboBox.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_LIST_NO_LOAD);
        String propertyName = comboBox.getProperty();

        List<DataReference<R>> data;

        if (BooleanUtils.isTrue(noLoad) || getModel().getForm() == null) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Skip loading of comboBox [%s-%s] (listNoLoad property found or form is null)", dtoClass.getSimpleName(), propertyName));
            }
            data = Collections.emptyList();

        } else {

            Optional<Set<DataReference<R>>> optionalReferenceSetDto = getModel().tryToGetDataReferenceSet(propertyName);

            if (optionalReferenceSetDto.isPresent()) {

                Set<DataReference<R>> references = optionalReferenceSetDto.get();
                data = Lists.newArrayList(references);

            } else {
                data = Collections.emptyList();
            }

        }

        if (log.isInfoEnabled()) {
            log.info(String.format("entity comboBox [%s-%s] : %d", dtoClass.getSimpleName(), propertyName, data.size()));
        }

        comboBox.setData(data);
    }

    private <R extends IdDto> Class<R> getDtoClass(JComponent list) {
        Object clientProperty = list.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_ENTITY_CLASS);
        return (Class<R>) clientProperty;
    }

    protected <R extends DataDto> DataReferenceDecorator<R> getDataReferenceDecorator(Class<R> dtoType) {
        return getDecoratorService().getDataReferenceDecorator(dtoType);
    }

    protected <R extends ReferentialDto> ReferentialReferenceDecorator<R> getReferentialReferenceDecorator(Class<R> dtoType) {
        return getDecoratorService().getReferentialReferenceDecorator(dtoType);
    }

    private <D extends ReferentialDto> ReferentialReferenceSet<D> filterReferentialReferenceSet(ReferenceSetRequestKeyDefinition<D> propertyDefinition,
                                                                                                ReferentialReferenceSet<D> incomingReferentialReferenceSet) {

        Class<D> dtoType = propertyDefinition.getType();
        String propertyName = propertyDefinition.getName();

        LinkedList<ReferentialReference<D>> incomingReferences = Lists.newLinkedList(incomingReferentialReferenceSet.getReferences());

        Iterable<ReferentialReference<D>> filtredReferentialReferences = filterReferentialReferences(dtoType, propertyName, incomingReferences);

        ImmutableSet<ReferentialReference<D>> references1 = ImmutableSet.copyOf(filtredReferentialReferences);
        return ReferentialReferenceSet.of(dtoType, references1, incomingReferentialReferenceSet.getLastUpdate());

    }

    private final FastDateFormat dateFormat;

    protected E getSelectedBean() {
        return getModel().getBean();
    }

    /**
     * Pour afficher une popup avec l'ensemble des informations techniques.
     *
     * @param button le boutton qui a declanche l'action
     */
    void showTechnicalInformations(JButton button) {

        E bean = getModel().isEditing() ? getBean() : getSelectedBean();

        if (bean.getLastUpdateDate() == null) {
            return;
        }

        JPanel content = new JPanel(new SpringLayout());

        FocusListener l = new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                JTextField source = (JTextField) e.getSource();
                source.setSelectionStart(0);
                source.setSelectionEnd(source.getText().length());
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        };

        {
            content.add(new JLabel(t("observe.common.topiaId")));
            JTextField comp = new JTextField(bean.getId());
            comp.setEditable(false);
            comp.addFocusListener(l);
            content.add(comp);
        }

        boolean isReferential = ReferentialDto.class.isAssignableFrom(getBeanType());
        if (isReferential) {
            content.add(new JLabel(t("observe.common.topiaCreateDate")));
            JTextField comp = new JTextField(dateFormat.format(((ReferentialDto) bean).getCreateDate()));
            comp.setEditable(false);
            comp.addFocusListener(l);
            content.add(comp);
        }

        {
            content.add(new JLabel(t("observe.common.lastUpdateDate")));
            JTextField comp = new JTextField(dateFormat.format(bean.getLastUpdateDate()));
            comp.setEditable(false);
            comp.addFocusListener(l);
            content.add(comp);
        }

        if (isReferential) {
            content.add(new JLabel(t("observe.common.topiaVersion")));
            JTextField comp = new JTextField(String.valueOf(((ReferentialDto) bean).getVersion()));
            comp.setEditable(false);
            comp.addFocusListener(l);
            content.add(comp);
        }

        SpringUtilities.makeCompactGrid(content, isReferential ? 4 : 2, 2, 5, 5, 5, 5);

        Decorator<E> decorator = getDecoratorService().getDecoratorByType(getBeanType());
        Objects.requireNonNull(decorator, "Cant find decorator of type " + getBeanType());
        String title = t("observe.title.technical.informations", "\n" + decorator.toString(bean));

        content.setBorder(new TitledBorder(title));

        JPopupMenu popup = new JPopupMenu();
        popup.setBorderPainted(true);
        popup.add(content);
        popup.pack();
        Dimension dim = popup.getPreferredSize();
        int x = (int) (button.getPreferredSize().getWidth() - dim.getWidth());
        int y = button.getHeight();
        popup.show(button, x, y);
    }


//    protected static class LogPropertyChanges implements PropertyChangeListener {
//
//        private final ImmutableSet<String> propertyNames;
//
//        public LogPropertyChanges(ImmutableSet<String> propertyNames) {
//            this.propertyNames = propertyNames;
//        }
//
//        @Override
//        public void propertyChange(PropertyChangeEvent evt) {
//
//            if (propertyNames.contains(evt.getPropertyName())) {
//                if (log.isInfoEnabled()) {
//                    log.info(String.format("Property %s changed (%s → %s)", evt.getPropertyName(), evt.getOldValue(), evt.getNewValue()));
//                }
//
//            }
//
//        }
//
//    }
}
