package fr.ird.observe.application.swing.ui.util.tripMap;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.vividsolutions.jts.geom.Coordinate;
import org.geotools.styling.Style;
import org.opengis.feature.simple.SimpleFeature;

import java.awt.Color;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveMapPaneLegendItem {

    protected static final int LEGEND_ITEM_HEIGHT = 20;
    protected static final int LEGEND_SYMBOL_WIDTH = 50;
    protected static final int LEGEND_MARGIN = 5;
    protected static final Color LEGEND_BACKGROUND = new Color(1f, 1f, 1f, 0.8f);


    public static Coordinate[] lineCoordinates() {
        Coordinate[] coordinates = new Coordinate[2];
        coordinates[0] = new Coordinate(LEGEND_MARGIN, LEGEND_ITEM_HEIGHT / 2);
        coordinates[1] = new Coordinate(LEGEND_SYMBOL_WIDTH - LEGEND_MARGIN, LEGEND_ITEM_HEIGHT / 2);
        return coordinates;
    }

    public static Coordinate pointCoordinates() {
        return new Coordinate(LEGEND_SYMBOL_WIDTH / 2, LEGEND_ITEM_HEIGHT / 2);
    }


    protected SimpleFeature simpleFeature;

    protected Style style;

    protected String label;

    public ObserveMapPaneLegendItem(SimpleFeature simpleFeature, Style style, String label) {
        this.simpleFeature = simpleFeature;
        this.style = style;
        this.label = label;
    }

    public SimpleFeature getSimpleFeature() {
        return simpleFeature;
    }

    public void setSimpleFeature(SimpleFeature simpleFeature) {
        this.simpleFeature = simpleFeature;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
