/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

JToolBar {
  borderPainted:false;
  floatable:false;
  opaque:false;
}

#PENDING_content {
  layout:{new BorderLayout()};
}

#northPanel {
  layout:{new BorderLayout()};
}

#reportVariableSelectorPanel {
  border:{new TitledBorder(t("observe.actions.report.variables"))};
  layout:{new GridLayout(0,1)};
}

#copyOptions {
  border:{new TitledBorder(t("observe.actions.report.copy.options"))};
  layout:{new GridLayout(0,1)};
}

#resultPane {
  border:{new TitledBorder(t("observe.actions.report.result"))};
  /*columnHeaderView:{resultTable.getTableHeader()};*/
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED};
}

#resultTable {
  model:{stepModel.getResultModel()};
  fillsViewportHeight:true;
}

#requestSelectorPane {
  border:{new TitledBorder(t("observe.actions.report.select"))};
  layout:{new BorderLayout()};
}

#reportSelector {
  model:{new DefaultComboBoxModel()};
  selectedItem:{stepModel.getSelectedReport()};
}

#reportDescriptionPane {
  columnHeaderView:{UIHelper.newLabel(t("observe.actions.report.report.description") ,"information", 10)};
  minimumSize:{new Dimension(0,0)};
}

#reportDescription {
  text:{getHandler().updateSelectedReportDescrption(stepModel.getSelectedReport())};
  editable:false;
  focusable:false;
}

#resetSelectedReport {
  actionIcon:"combobox-reset";
  toolTipText:"observe.actions.synchro.report.reset.tip";
  enabled:{stepModel.getSelectedReport() != null};
  mnemonic:R;
}

#copyPane {
  layout:{new BorderLayout()};
}
#autoCopyToClipboard {
  text:"observe.action.auto.copy.to.clipboard";
  toolTipText:"observe.action.auto.copy.to.clipboard.tip";
  selected:{stepModel.isAutoCopyToClipboard()};
}

#copyRowHeaders {
  text:"observe.action.copy.row.headers";
  toolTipText:"observe.action.copy.row.headers.tip";
  selected:{stepModel.isCopyRowHeaders()};
}

#copyColumnHeaders {
  text:"observe.action.copy.column.headers";
  toolTipText:"observe.action.copy.column.headers.tip";
  selected:{stepModel.isCopyColumnHeaders()};
}

#copy {
  text:"observe.action.copy";
  toolTipText:"observe.actions.synchro.copy.tip";
  actionIcon:"report-copy";
  /*enabled:{stepModel.getSelectedReport() != null};*/
  enabled:{stepModel.isValid() };
  mnemonic:C;
}
