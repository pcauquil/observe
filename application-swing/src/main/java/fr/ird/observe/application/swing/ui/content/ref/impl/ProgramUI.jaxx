<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI superGenericType='ProgramDto'>

  <style source="ReferenceEntity.jcss"/>
  <style source="I18nReferenceEntity.jcss"/>

  <import>
    fr.ird.observe.services.dto.constants.ReferenceStatus
    fr.ird.observe.services.dto.constants.GearType
    fr.ird.observe.services.dto.referential.ReferentialReference
    fr.ird.observe.services.dto.referential.ProgramDto
    fr.ird.observe.services.dto.referential.OrganismDto
    fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel
    fr.ird.observe.application.swing.ui.content.ref.impl.ProgramObservationEnum

    jaxx.runtime.swing.editor.bean.BeanComboBox

    org.jdesktop.swingx.JXDatePicker

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true' context='ui-create'
                 beanClass='fr.ird.observe.services.dto.referential.ProgramDto' errorTableModel='{getErrorTableModel()}'/>

  <!-- model -->
  <ProgramUIModel id='model'/>

  <!-- handler -->
  <ProgramUIHandler id='handler' constructorParams="this"/>

  <!-- edit bean -->
  <ProgramDto id='bean'/>

  <Table id="editView" insets="0" fill="both">
    <row>
      <cell anchor="north" weightx="1">
        <JTabbedPane id='tabPane'>
          <tab id='generalTab'>
            <Table fill="both">

              <!-- uri -->
              <row>
                <cell anchor="west">
                  <JLabel id='uriLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <JTextField id='uri' onKeyReleased='getBean().setUri(uri.getText())'/>
                </cell>
              </row>

              <!-- code / status -->
              <row>
                <cell anchor="west">
                  <JLabel id='codeStatusLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <JPanel id='codeStatusPanel' layout='{new BorderLayout()}'>
                    <JTextField id='code' constraints='BorderLayout.WEST'
                                onKeyReleased='getBean().setCode(code.getText())'/>
                    <EnumEditor id='status' constraints='BorderLayout.CENTER'
                                constructorParams='ReferenceStatus.class'
                                genericType='ReferenceStatus'
                                onItemStateChanged='getBean().setStatus((ReferenceStatus)status.getSelectedItem())'/>
                  </JPanel>
                </cell>
              </row>

              <!-- organism -->
              <row>
                <cell anchor='west'>
                  <JLabel id='organismLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <BeanComboBox id='organism' constructorParams='this'
                                genericType='ReferentialReference&lt;OrganismDto&gt;' _entityClass='OrganismDto.class'/>
                </cell>
              </row>

              <row>
                <!-- gearType -->
                <cell anchor='west'>
                  <JLabel id='gearTypeLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='gearType'
                              constructorParams='GearType.class'
                              genericType='GearType'
                              onItemStateChanged='getBean().setGearType((GearType) gearType.getSelectedItem())'/>
                </cell>
              </row>

              <!-- date debut - fin de validite -->
              <row>
                <cell anchor='west'>
                  <JLabel id='startDateFinProgramLabel'/>
                </cell>
                <cell anchor='west'>
                  <JPanel layout='{new GridLayout()}'>
                    <JXDatePicker id='startDate' onActionPerformed='getBean().setStartDate(startDate.getDate())'/>
                    <JXDatePicker id='endDate' onActionPerformed='getBean().setEndDate(endDate.getDate())'/>
                  </JPanel>
                </cell>
              </row>

              <!-- needComment -->
              <row>
                <cell anchor='east' weightx="1" fill="both" columns="2">
                  <JCheckBox id='needComment' onItemStateChanged='getBean().setNeedComment(needComment.isSelected())'/>
                </cell>
              </row>

              <row>
                <cell columns="2">
                  <Table id='editI18nTable2'>
                    <row>
                      <cell anchor="west">
                        <JLabel id='label1Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label1' onKeyReleased='getBean().setLabel1(label1.getText())'/>
                      </cell>
                      <cell anchor="west">
                        <JLabel id='label2Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label2' onKeyReleased='getBean().setLabel2(label2.getText())'/>
                      </cell>
                    </row>
                    <row>
                      <cell anchor="west">
                        <JLabel id='label3Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label3' onKeyReleased='getBean().setLabel3(label3.getText())'/>
                      </cell>
                      <cell anchor="west">
                        <JLabel id='label4Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label4' onKeyReleased='getBean().setLabel4(label4.getText())'/>
                      </cell>
                    </row>
                    <row>
                      <cell anchor="west">
                        <JLabel id='label5Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label5' onKeyReleased='getBean().setLabel5(label5.getText())'/>
                      </cell>
                      <cell anchor="west">
                        <JLabel id='label6Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label6' onKeyReleased='getBean().setLabel6(label6.getText())'/>
                      </cell>
                    </row>
                    <row>
                      <cell anchor="west">
                        <JLabel id='label7Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label7' onKeyReleased='getBean().setLabel7(label7.getText())'/>
                      </cell>
                      <cell anchor="west">
                        <JLabel id='label8Label'/>
                      </cell>
                      <cell anchor='east' weightx="1" fill="both">
                        <JTextField id='label8' onKeyReleased='getBean().setLabel8(label8.getText())'/>
                      </cell>
                    </row>
                  </Table>
                </cell>
              </row>
            </Table>
          </tab>

          <tab id='observationsTab'>
            <Table fill="both">

              <row>

                <!-- nonTargetObservation -->
                <cell anchor='west'>
                  <JLabel id='nonTargetObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='nonTargetObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getNonTargetObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setNonTargetObservation(nonTargetObservation.getSelectedIndex())'/>
                </cell>

              </row>
              <row>
                <!-- targetDiscardsObservation -->
                <cell anchor='west'>
                  <JLabel id='targetDiscardsObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='targetDiscardsObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getTargetDiscardsObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setTargetDiscardsObservation(targetDiscardsObservation.getSelectedIndex())'/>
                </cell>
              </row>
              <row>

                <!-- samplesObservation -->
                <cell anchor='west'>
                  <JLabel id='samplesObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='samplesObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getSamplesObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setSamplesObservation(samplesObservation.getSelectedIndex())'/>
                </cell>
              </row>
              <row>
                <!-- objectsObservation -->
                <cell anchor='west'>
                  <JLabel id='objectsObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='objectsObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getObjectsObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setObjectsObservation(objectsObservation.getSelectedIndex())'/>
                </cell>
              </row>
              <row>

                <!-- detailledActivitiesObservation -->
                <cell anchor='west'>
                  <JLabel id='detailledActivitiesObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='detailledActivitiesObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getDetailledActivitiesObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setDetailledActivitiesObservation(detailledActivitiesObservation.getSelectedIndex())'/>
                </cell>
              </row>
              <row>
                <!-- mammalsObservation -->
                <cell anchor='west'>
                  <JLabel id='mammalsObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='mammalsObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getMammalsObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setMammalsObservation(mammalsObservation.getSelectedIndex())'/>
                </cell>
              </row>
              <row>

                <!-- birdsObservation -->
                <cell anchor='west'>
                  <JLabel id='birdsObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='birdsObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getBirdsObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setBirdsObservation(birdsObservation.getSelectedIndex())'/>
                </cell>
              </row>
              <row>
                <!-- baitObservation -->
                <cell anchor='west'>
                  <JLabel id='baitObservationLabel'/>
                </cell>
                <cell anchor='east' weightx="1" fill="both">
                  <EnumEditor id='baitObservation'
                              constructorParams='ProgramObservationEnum.class, getConfig().getBaitObservation()'
                              genericType='ProgramObservationEnum'
                              onItemStateChanged='getBean().setBaitObservation(baitObservation.getSelectedIndex())'/>
                </cell>
              </row>

              <row>
                <cell columns="2" weighty="1">
                  <JLabel/>
                </cell>
              </row>
            </Table>
          </tab>
        </JTabbedPane>
      </cell>
    </row>

    <!-- Comment -->
    <row>
      <cell weightx="1" weighty="1" fill="both">
        <JScrollPane id='comment' onFocusGained='comment2.requestFocus()'>
          <JTextArea id='comment2' onKeyReleased='getBean().setComment(comment2.getText())'/>
        </JScrollPane>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI>
