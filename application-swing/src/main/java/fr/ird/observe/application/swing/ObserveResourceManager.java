/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing;

import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.SortedProperties;
import org.nuiton.util.ZipUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.t;

/**
 * La classe responsable du chargement de toutes les resources qui viennent de
 * l'application et qui seont ensuite redispatchés dans le répertoire de
 * resource de l'utilisateur.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ObserveResourceManager {

    public static final String OBSERVE_UI_PROPERTIES = "/observe-ui.properties";

    public static final String OBSERVE_APPLICATION_PROPERTIES = "/observe-application.properties";

    public static final String OBSERVE_REPORTS_PROPERTIES = "/observe-reports.properties";

    public static final String OBSERVE_MAP_ARCHIVE = "/map.zip";

    public static final String OBSERVE_LOG_CONFIGURATION_FILE_PROPERTIES = "/observe-log4j.properties";

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveResourceManager.class);

    public enum Resource {

        ui(OBSERVE_UI_PROPERTIES),
        application(OBSERVE_APPLICATION_PROPERTIES),
        report(OBSERVE_REPORTS_PROPERTIES),
        LOG_CONFIGURATION_FILE(OBSERVE_LOG_CONFIGURATION_FILE_PROPERTIES),
        mapLayers(OBSERVE_MAP_ARCHIVE);

        private final String location;

        private URL url;

        Resource(String location) {
            this.location = location;
        }

        public URL getUrl() {
            if (url == null) {
                url = getResource(location);
            }
            return url;
        }

        public boolean exists(File directory) {
            File file = getFile(directory);
            return file.exists();
        }

        public File getFile(File directory) {
            return new File(directory, location.substring(1));
        }

    }

    protected Map<String, Properties> resources;

    public Map<String, Properties> getResources() {
        if (resources == null) {
            resources = new TreeMap<>();
        }
        return resources;
    }


    public Properties getResource(Resource resource) throws IOException {

        URL url = resource.getUrl();
        return getResource(url);
    }

    public Properties getResource(File file) throws IOException {

        URL url = file.toURI().toURL();

        return load(url);
    }


    public Properties getResource(URL url) throws IOException {

        String path = url.toString();

        Properties result = getResources().get(path);

        if (result == null) {

            result = load(url);

            // on sauvegarde dans le cache
            if (log.isDebugEnabled()) {
                log.debug("Store configuration [" + path + "]");
            }
            getResources().put(path, result);
        }

        // toujours faire une copie pour eviter toute altération des
        // configuration par défaut.
        SortedProperties tmp = new SortedProperties();
        tmp.putAll(result);
        return tmp;
    }

    protected Properties load(URL url) throws IOException {
        Properties result;// chargement une unique fois de la resource
        InputStream in = openInternalStream(url);

        try {
            result = new Properties();
            result.load(in);


        } finally {
            in.close();
        }
        return result;
    }

    public void copyResource(Resource resource,
                             File file,
                             String message) throws IOException {

        FileOutputStream out = new FileOutputStream(file);
        try {
            // on fait une copie brute en ne passant pas par un Properties
            // qui perd le formatage et les commentaires

            // chargement des ressources
            InputStreamReader in =
                    new InputStreamReader(
                            new BufferedInputStream(
                                    openInternalStream(resource.getUrl())),
                            "utf-8");

            // sauvegarde dans le fichier cible
            try {
                IOUtils.copy(in, out, "utf-8");
            } finally {
                in.close();
            }

            if (log.isInfoEnabled()) {
                log.info(message);
            }
        } finally {
            out.close();
        }
    }

    public void copyResource(URL resource,
                             File file,
                             String message) throws IOException {

        FileOutputStream out = new FileOutputStream(file);
        try {
            // on fait une copie brute en ne passant pas par un Properties
            // qui perd le formatage et les commentaires

            // chargement des ressources
            InputStreamReader in =
                    new InputStreamReader(
                            new BufferedInputStream(
                                    openInternalStream(resource)),
                            "utf-8");

            // sauvegarde dans le fichier cible
            try {
                IOUtils.copy(in, out, "utf-8");
                in.close();
            } finally {
                IOUtils.closeQuietly(in);
            }

            if (log.isInfoEnabled()) {
                log.info(message);
            }
            out.close();
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Créer le répertoire si nécessaire à partir le l'option donnée.
     *
     * @param config la configuration utilisée
     * @param option l'option qui représentent le répertoire.
     * @return le fichier
     * @throws IOException pour tout problème de création de répertoire
     */
    protected File createDirectory(ObserveSwingApplicationConfig config,
                                   ObserveSwingApplicationConfigOption option) throws IOException {
        File dir = config.getOptionAsFile(option.getKey());
        createDirectory(dir);
        return dir;
    }

    /**
     * Créer tous les répertoires parents nécessaires à partir des options sur
     * répertoire ou fichier.
     *
     * @param config  la configuration utilisée
     * @param options les options qui représentent des répertoires ou fichiers.
     * @throws IOException pour tout problème de création de répertoire
     */
    protected void createParentDirectory(ObserveSwingApplicationConfig config,
                                         ObserveSwingApplicationConfigOption... options) throws IOException {
        for (ObserveSwingApplicationConfigOption option : options) {
            File dir = config.getOptionAsFile(option.getKey()).getParentFile();
            createDirectory(dir);
        }
    }

    /**
     * Créer un répertoire s'il n'existe pas.
     *
     * @param dir le répertoire à créer
     * @throws IOException pour tout problème de création de répertoire
     */
    protected void createDirectory(File dir) throws IOException {

        if (!dir.exists()) {
            if (log.isInfoEnabled()) {
                log.info(t("observe.runner.create.directory", dir));
            } else {
                log.info(t("observe.runner.exists.directory", dir));
            }
            boolean b = dir.mkdirs();
            if (!b) {
                throw new IOException(
                        t("observe.error.can.not.create.directory", dir));
            }
        }
    }

    protected static URL getResource(String location) {
        URL resource = ObserveResourceManager.class.getResource(location);
        try {
            // test que la resource existe bien dans le class-path
            InputStream in = openInternalStream(resource);
            in.close();
            return resource;
        } catch (Exception e) {
            throw new IllegalStateException("Could not treat internal resource " + location);
        }
    }

    protected static InputStream openInternalStream(URL resource) {
        try {
            InputStream in = resource.openStream();
            if (in == null) {
                throw new IllegalStateException("Could not find internal resource " + resource);
            }

            return in;
        } catch (Exception e) {
            throw new IllegalStateException("Could not treat internal resource " + resource);
        }
    }

    public File unzipToDirectory(Resource resource, ObserveSwingApplicationConfig config, ObserveSwingApplicationConfigOption option, String message) throws IOException {

        File dir = config.getOptionAsFile(option.getKey());
        createDirectory(dir);
        InputStream inputStream = openInternalStream(resource.getUrl());
        try {
            ZipUtil.uncompress(inputStream, dir);
            return dir;
        } finally {
            inputStream.close();

        }
    }

}
