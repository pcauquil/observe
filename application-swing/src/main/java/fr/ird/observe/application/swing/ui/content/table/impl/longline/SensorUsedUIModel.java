package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto;
import fr.ird.observe.services.dto.longline.SensorUsedDto;
import fr.ird.observe.services.dto.longline.SensorUsedDtos;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class SensorUsedUIModel extends ContentTableUIModel<ActivityLonglineSensorUsedDto, SensorUsedDto> {

    private static final long serialVersionUID = 1L;

    public SensorUsedUIModel(SensorUsedUI ui) {

        super(ActivityLonglineSensorUsedDto.class,
              SensorUsedDto.class,
              new String[]{
                      ActivityLonglineSensorUsedDto.PROPERTY_SENSOR_USED,
                      ActivityLonglineSensorUsedDto.PROPERTY_COMMENT},
              new String[]{SensorUsedDto.PROPERTY_HAS_DATA,
                           SensorUsedDto.PROPERTY_DATA,
                           SensorUsedDto.PROPERTY_SENSOR_SERIAL_NO,
                           SensorUsedDto.PROPERTY_SENSOR_TYPE,
                           SensorUsedDto.PROPERTY_SENSOR_BRAND,
                           SensorUsedDto.PROPERTY_SENSOR_DATA_FORMAT});

        List<ContentTableMeta<SensorUsedDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(SensorUsedDto.class, SensorUsedDto.PROPERTY_SENSOR_TYPE, false),
                ContentTableModel.newTableMeta(SensorUsedDto.class, SensorUsedDto.PROPERTY_SENSOR_BRAND, false),
                ContentTableModel.newTableMeta(SensorUsedDto.class, SensorUsedDto.PROPERTY_SENSOR_DATA_FORMAT, false),
                ContentTableModel.newTableMeta(SensorUsedDto.class, SensorUsedDto.PROPERTY_SENSOR_SERIAL_NO, false),
                ContentTableModel.newTableMeta(SensorUsedDto.class, SensorUsedDto.PROPERTY_HAS_DATA, false),
                ContentTableModel.newTableMeta(SensorUsedDto.class, SensorUsedDto.PROPERTY_DATA_LOCATION, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<ActivityLonglineSensorUsedDto, SensorUsedDto> createTableModel(
            ObserveContentTableUI<ActivityLonglineSensorUsedDto, SensorUsedDto> ui,
            List<ContentTableMeta<SensorUsedDto>> contentTableMetas) {

        return new ContentTableModel<ActivityLonglineSensorUsedDto, SensorUsedDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<SensorUsedDto> getChilds(ActivityLonglineSensorUsedDto bean) {
                return bean.getSensorUsed();
            }

            @Override
            protected void load(SensorUsedDto source, SensorUsedDto target) {
                SensorUsedDtos.copySensorUsedDto(source, target);
            }

            @Override
            protected void setChilds(ActivityLonglineSensorUsedDto parent, List<SensorUsedDto> childs) {
                parent.setSensorUsed(childs);
            }
        };
    }
}
