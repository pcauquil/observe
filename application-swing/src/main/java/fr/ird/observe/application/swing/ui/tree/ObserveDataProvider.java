/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import jaxx.runtime.swing.nav.NavDataProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Provider de données pour les noeuds des arbres.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class ObserveDataProvider implements NavDataProvider {

    /** Logger */
    static private final Log log = LogFactory.getLog(ObserveDataProvider.class);

    protected ObserveSwingDataSource dataSource;

    protected DataSelectionModel selectionModel;

    private boolean creating;

    public ObserveDataProvider(ObserveSwingDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setSource(ObserveSwingDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setSelectionModel(DataSelectionModel selectionModel) {
        if (log.isDebugEnabled()) {
            log.debug("Set selection model : " + selectionModel);
        }
        this.selectionModel = selectionModel;
    }

    @Override
    public boolean isEnabled() {
        return dataSource != null && dataSource.isOpen() || selectionModel != null;
    }

    public ObserveSwingDataSource getDataSource() {
        return dataSource;
    }

    public DataSelectionModel getSelectionModel() {
        return selectionModel;
    }

    public boolean isCreating() {
        return creating;
    }

    public void setCreating(boolean creating) {
        this.creating = creating;
    }

}
