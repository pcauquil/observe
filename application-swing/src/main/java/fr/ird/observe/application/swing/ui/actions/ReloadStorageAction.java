package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;
/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ReloadStorageAction extends AbstractObserveAction {


    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReloadStorageAction.class);

    private final ObserveMainUI ui;

    public ReloadStorageAction(ObserveMainUI ui) {

        super(t("observe.action.reload.storage"), SwingUtil.getUIManagerActionIcon("db-reload"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.reload.storage.tip"));
        putValue(MNEMONIC_KEY, (int) 'R');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        run();

    }

    public void run() {

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();
        boolean canContinue = context.getContentUIManager().closeSelectedContentUI();

        ObserveSwingDataSource storage = context.getDataSourcesManager().getMainDataSource();

        if (canContinue && storage != null) {

            ui.setBusy(false);

            String[] ids = ui.getTreeHelper().getSelectedIds();

            try {

                if (log.isDebugEnabled()) {
                    log.debug(">>> close main storage " + storage);
                }
                // fermeture du storage courant
                storage.close();

                ObserveSwingApplicationConfig appConfig = context.getConfig();

                if (storage.isLocal()) {

                    ObserveDataSourceConfigurationTopiaH2 h2Config = (ObserveDataSourceConfigurationTopiaH2) storage.getConfiguration();

                    h2Config.setCanMigrate(appConfig.isCanMigrateH2());
                } else if (storage.isRemote()) {
                    ObserveDataSourceConfigurationTopiaPG pgConfig = (ObserveDataSourceConfigurationTopiaPG) storage.getConfiguration();
                    pgConfig.setCanMigrate(appConfig.isCanMigrateObstuna());
                }

                try {


                    // on conserve les noeuds a selectionner
                    context.setNodesToReselect(ids);

                    // attachement a l'ui
                    context.getDataSourcesManager().prepareMainStorage(storage, false);

                    // ouverture du service
                    storage.open();
                } catch (Exception e) {
                    UIHelper.handlingError(e);
                }

            } finally {
                ui.setBusy(false);
            }

        }
    }
}
