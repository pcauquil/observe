package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;

import java.util.Set;

/**
 * Created on 9/27/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class ProgramUIModel extends ContentReferenceUIModel<ProgramDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_OBSERVATIONS_TAB_VALID = "observationsTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(ProgramDto.PROPERTY_URI,
                                               ProgramDto.PROPERTY_CODE,
                                               ProgramDto.PROPERTY_STATUS,
                                               ProgramDto.PROPERTY_NEED_COMMENT,
                                               ProgramDto.PROPERTY_GEAR_TYPE,
                                               ProgramDto.PROPERTY_ORGANISM,
                                               ProgramDto.PROPERTY_LABEL1,
                                               ProgramDto.PROPERTY_LABEL2,
                                               ProgramDto.PROPERTY_LABEL3,
                                               ProgramDto.PROPERTY_LABEL4,
                                               ProgramDto.PROPERTY_LABEL5,
                                               ProgramDto.PROPERTY_LABEL6,
                                               ProgramDto.PROPERTY_LABEL7,
                                               ProgramDto.PROPERTY_LABEL8).build();

    public static final Set<String> OBSERVATIONS_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(ProgramDto.PROPERTY_DETAILLED_ACTIVITIES_OBSERVATION,
                                               ProgramDto.PROPERTY_NON_TARGET_OBSERVATION,
                                               ProgramDto.PROPERTY_BAIT_OBSERVATION,
                                               ProgramDto.PROPERTY_MAMMALS_OBSERVATION,
                                               ProgramDto.PROPERTY_SAMPLES_OBSERVATION,
                                               ProgramDto.PROPERTY_OBJECTS_OBSERVATION,
                                               ProgramDto.PROPERTY_BIRDS_OBSERVATION,
                                               ProgramDto.PROPERTY_TARGET_DISCARDS_OBSERVATION).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean observationsTabValid;

    public ProgramUIModel() {
        super(ProgramDto.class,
              new String[]{
                      ProgramDto.PROPERTY_ORGANISM,
                      ProgramDto.PROPERTY_START_DATE,
                      ProgramDto.PROPERTY_END_DATE,
                      ProgramDto.PROPERTY_DETAILLED_ACTIVITIES_OBSERVATION,
                      ProgramDto.PROPERTY_NON_TARGET_OBSERVATION,
                      ProgramDto.PROPERTY_BAIT_OBSERVATION,
                      ProgramDto.PROPERTY_MAMMALS_OBSERVATION,
                      ProgramDto.PROPERTY_SAMPLES_OBSERVATION,
                      ProgramDto.PROPERTY_OBJECTS_OBSERVATION,
                      ProgramDto.PROPERTY_BIRDS_OBSERVATION,
                      ProgramDto.PROPERTY_TARGET_DISCARDS_OBSERVATION,
                      ProgramDto.PROPERTY_GEAR_TYPE},
              new String[]{
                      ProgramUI.BINDING_ORGANISM_SELECTED_ITEM,
                      ProgramUI.BINDING_START_DATE_DATE,
                      ProgramUI.BINDING_END_DATE_DATE,
                      ProgramUI.BINDING_DETAILLED_ACTIVITIES_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_NON_TARGET_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_BAIT_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_MAMMALS_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_SAMPLES_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_OBJECTS_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_BIRDS_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_TARGET_DISCARDS_OBSERVATION_SELECTED_INDEX,
                      ProgramUI.BINDING_GEAR_TYPE_SELECTED_ITEM}
        );
    }

    public boolean isObservationsTabValid() {
        return observationsTabValid;
    }

    public void setObservationsTabValid(boolean observationsTabValid) {
        Object oldValue = isObservationsTabValid();
        this.observationsTabValid = observationsTabValid;
        firePropertyChange(PROPERTY_OBSERVATIONS_TAB_VALID, oldValue, observationsTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
