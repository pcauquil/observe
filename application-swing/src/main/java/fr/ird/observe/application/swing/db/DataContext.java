package fr.ird.observe.application.swing.db;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.IdDtos;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DataContext extends AbstractSerializableBean {

    public static final String PROPERTY_OPEN_PROGRAM = "openProgram";

    public static final String PROPERTY_OPEN_TRIP = "openTrip";
    public static final String PROPERTY_OPEN_TRIP_SEINE = "openTripSeine";
    public static final String PROPERTY_OPEN_TRIP_LONGLINE = "openTripLongline";

    public static final String PROPERTY_OPEN_ROUTE = "openRoute";

    public static final String PROPERTY_OPEN_ACTIVITY = "openActivity";
    public static final String PROPERTY_OPEN_ACTIVITY_SEINE = "openActivitySeine";
    public static final String PROPERTY_OPEN_ACTIVITY_LONGLINE = "openActivityLongline";

    public static final String PROPERTY_OPEN_SET = "openSet";
    public static final String PROPERTY_OPEN_SET_SEINE = "openSetSeine";
    public static final String PROPERTY_OPEN_SET_LONGLINE = "openSetLongline";

    public static final String PROPERTY_OPEN_PROGRAM_ID = "openProgramId";

    public static final String PROPERTY_OPEN_TRIP_ID = "openTripId";
    public static final String PROPERTY_OPEN_TRIP_SEINE_ID = "openTripSeineId";
    public static final String PROPERTY_OPEN_TRIP_LONGLINE_ID = "openTripLonglineId";

    public static final String PROPERTY_OPEN_ROUTE_ID = "openRouteId";

    public static final String PROPERTY_OPEN_ACTIVITY_ID = "openActivityId";
    public static final String PROPERTY_OPEN_ACTIVITY_SEINE_ID = "openActivitySeineId";
    public static final String PROPERTY_OPEN_ACTIVITY_LONGLINE_ID = "openActivityLonglineId";

    public static final String PROPERTY_OPEN_SET_ID = "openSetId";
    public static final String PROPERTY_OPEN_SET_SEINE_ID = "openSetSeineId";
    public static final String PROPERTY_OPEN_SET_LONGLINE_ID = "openSetLonglineId";

    public static final String PROPERTY_SELECTED_PROGRAM_ID = "selectedProgramId";

    public static final String PROPERTY_SELECTED_TRIP_ID = "selectedTripId";

    public static final String PROPERTY_SELECTED_ROUTE_ID = "selectedRouteId";

    public static final String PROPERTY_SELECTED_ACTIVITY_ID = "selectedActivityId";

    public static final String PROPERTY_SELECTED_SET_ID = "selectedSetId";

    public static final String PROPERTY_SELECTED_FLOATING_OBJECT_ID = "selectedFloatingObjectId";

    public static final String PROPERTY_ENABLED = "enabled";

    public static final String PROPERTY_SELECTION_CHANGED = "selectionChanged";

    protected String openProgramId;

    protected String openTripSeineId;

    protected String openTripLonglineId;

    protected String openRouteId;

    protected String openActivitySeineId;

    protected String openActivityLonglineId;

    protected String openSetSeineId;

    protected String openSetLonglineId;

    protected String selectedProgramId;

    protected String selectedTripId;

    protected String selectedRouteId;

    protected String selectedActivityId;

    protected String selectedSetId;

    protected String selectedFloatingObjectId;

    protected boolean enabled;

    protected boolean selectionChanged;

    protected TripSeineDto validationTripSeine;

    protected RouteDto validationRoute;

    protected ActivitySeineDto validationActivitySeine;

    protected SetSeineDto validationSetSeine;

    protected FloatingObjectDto validationFloatingObject;

    /** Logger */
    private static final Log log = LogFactory.getLog(DataContext.class);

    private static final long serialVersionUID = 1L;

    public String getOpenProgramId() {
        return openProgramId;
    }

    public void setOpenProgramId(String openProgramId) {
        String oldValue = getOpenProgramId();
        boolean oldValue2 = isOpenProgram();
        this.openProgramId = openProgramId;
        firePropertyChange(PROPERTY_OPEN_PROGRAM_ID, oldValue, this.openProgramId);
        firePropertyChange(PROPERTY_OPEN_PROGRAM, oldValue2, isOpenProgram());
    }

    public String getOpenTripId() {
        return openTripSeineId != null ? openTripSeineId : openTripLonglineId;
    }

    public String getOpenTripSeineId() {
        return openTripSeineId;
    }

    public String getOpenTripLonglineId() {
        return openTripLonglineId;
    }

    public void setOpenTripSeineId(String openTripId) {
        String oldValue = getOpenTripId();
        String oldValue2 = getOpenTripSeineId();
        boolean oldValue3 = isOpenTrip();
        boolean oldValue4 = isOpenTripSeine();
        this.openTripSeineId = openTripId;
        firePropertyChange(PROPERTY_OPEN_TRIP_ID, oldValue, this.openTripSeineId);
        firePropertyChange(PROPERTY_OPEN_TRIP_SEINE_ID, oldValue2, this.openTripSeineId);
        firePropertyChange(PROPERTY_OPEN_TRIP, oldValue3, isOpenTrip());
        firePropertyChange(PROPERTY_OPEN_TRIP_SEINE, oldValue4, isOpenTripSeine());
    }

    public void setOpenTripLonglineId(String openTripId) {
        String oldValue = getOpenTripId();
        String oldValue2 = getOpenTripLonglineId();
        boolean oldValue3 = isOpenTrip();
        boolean oldValue4 = isOpenTripLongline();
        this.openTripLonglineId = openTripId;
        firePropertyChange(PROPERTY_OPEN_TRIP_ID, oldValue, this.openTripLonglineId);
        firePropertyChange(PROPERTY_OPEN_TRIP_LONGLINE_ID, oldValue2, this.openTripLonglineId);
        firePropertyChange(PROPERTY_OPEN_TRIP, oldValue3, isOpenTrip());
        firePropertyChange(PROPERTY_OPEN_TRIP_LONGLINE, oldValue4, isOpenTripLongline());
    }

    public String getOpenRouteId() {
        return openRouteId;
    }

    public void setOpenRouteId(String openRouteId) {
        String oldValue = getOpenRouteId();
        boolean oldValue2 = isOpenRoute();
        this.openRouteId = openRouteId;
        firePropertyChange(PROPERTY_OPEN_ROUTE_ID, oldValue, openRouteId);
        firePropertyChange(PROPERTY_OPEN_ROUTE, oldValue2, isOpenRoute());
    }

    public String getOpenActivityId() {
        return openActivitySeineId != null ? openActivitySeineId : openActivityLonglineId;
    }

    public String getOpenActivityLonglineId() {
        return openActivityLonglineId;
    }

    public String getOpenActivitySeineId() {
        return openActivitySeineId;
    }

    public void setOpenActivitySeineId(String openActivityId) {
        String oldValue = getOpenActivityId();
        String oldValue2 = getOpenActivitySeineId();
        boolean oldValue3 = isOpenActivity();
        boolean oldValue4 = isOpenActivitySeine();
        this.openActivitySeineId = openActivityId;
        firePropertyChange(PROPERTY_OPEN_ACTIVITY_ID, oldValue, this.openActivitySeineId);
        firePropertyChange(PROPERTY_OPEN_ACTIVITY_SEINE_ID, oldValue2, this.openActivitySeineId);
        firePropertyChange(PROPERTY_OPEN_ACTIVITY, oldValue3, isOpenActivity());
        firePropertyChange(PROPERTY_OPEN_ACTIVITY_SEINE, oldValue4, isOpenActivitySeine());
    }

    public void setOpenActivityLonglineId(String openActivityId) {
        String oldValue = getOpenActivityId();
        String oldValue2 = getOpenActivityLonglineId();
        boolean oldValue3 = isOpenActivity();
        boolean oldValue4 = isOpenActivityLongline();
        this.openActivityLonglineId = openActivityId;
        firePropertyChange(PROPERTY_OPEN_ACTIVITY_ID, oldValue, this.openActivityLonglineId);
        firePropertyChange(PROPERTY_OPEN_ACTIVITY_LONGLINE_ID, oldValue2, this.openActivityLonglineId);
        firePropertyChange(PROPERTY_OPEN_ACTIVITY, oldValue3, isOpenActivity());
        firePropertyChange(PROPERTY_OPEN_ACTIVITY_LONGLINE, oldValue4, isOpenActivityLongline());
    }

    public String getOpenSetId() {
        return openSetSeineId != null ? openSetSeineId : openSetLonglineId;
    }

    public String getOpenSetLonglineId() {
        return openSetSeineId;
    }

    public String getOpenSetSeineId() {
        return openSetLonglineId;
    }

    public void setOpenSetSeineId(String openSetId) {
        String oldValue = getOpenSetId();
        String oldValue2 = getOpenSetSeineId();
        boolean oldValue3 = isOpenSet();
        boolean oldValue4 = isOpenSetSeine();
        this.openSetSeineId = openSetId;
        firePropertyChange(PROPERTY_OPEN_SET_ID, oldValue, this.openSetSeineId);
        firePropertyChange(PROPERTY_OPEN_SET_SEINE_ID, oldValue2, this.openSetSeineId);
        firePropertyChange(PROPERTY_OPEN_SET, oldValue3, isOpenSet());
        firePropertyChange(PROPERTY_OPEN_SET_SEINE, oldValue4, isOpenSetSeine());
    }

    public void setOpenSetLonglineId(String openSetId) {
        String oldValue = getOpenSetId();
        String oldValue2 = getOpenSetLonglineId();
        boolean oldValue3 = isOpenSet();
        boolean oldValue4 = isOpenSetLongline();
        this.openSetLonglineId = openSetId;
        firePropertyChange(PROPERTY_OPEN_SET_ID, oldValue, this.openSetLonglineId);
        firePropertyChange(PROPERTY_OPEN_SET_LONGLINE_ID, oldValue2, this.openSetLonglineId);
        firePropertyChange(PROPERTY_OPEN_SET, oldValue3, isOpenSet());
        firePropertyChange(PROPERTY_OPEN_SET_LONGLINE, oldValue4, isOpenSetLongline());
    }

    public String getSelectedProgramId() {
        return selectedProgramId;
    }

    public void setSelectedProgramId(String selectedProgramId) {
        String oldValue = getSelectedProgramId();
        this.selectedProgramId = selectedProgramId;
        firePropertyChange(PROPERTY_SELECTED_PROGRAM_ID, oldValue, this.selectedProgramId);
    }

    public String getSelectedTripId() {
        return selectedTripId;
    }

    public String getSelectedTripLonglineId() {
        return isSelectedTripLongline() ? getSelectedTripId() : null;
    }

    public String getSelectedTripSeineId() {
        return isSelectedTripSeine() ? getSelectedTripId() : null;
    }

    public void setSelectedTripId(String selectedTripId) {
        String oldValue = getSelectedTripId();
        this.selectedTripId = selectedTripId;
        firePropertyChange(PROPERTY_SELECTED_TRIP_ID, oldValue, this.selectedTripId);
    }

    public boolean isSelectedTripLongline() {
        return selectedTripId != null && IdDtos.isLonglineId(selectedTripId);
    }

    public boolean isSelectedTripSeine() {
        return selectedTripId != null && IdDtos.isSeineId(selectedTripId);
    }

    public String getSelectedRouteId() {
        return selectedRouteId;
    }

    public void setSelectedRouteId(String selectedRouteId) {
        String oldValue = getSelectedRouteId();
        this.selectedRouteId = selectedRouteId;
        firePropertyChange(PROPERTY_SELECTED_ROUTE_ID, oldValue, selectedRouteId);
    }

    public String getSelectedActivityId() {
        return selectedActivityId;
    }

    public String getSelectedActivityLonglineId() {
        return isSelectedActivityLongline() ? getSelectedActivityId() : null;
    }

    public String getSelectedActivitySeineId() {
        return isSelectedActivitySeine() ? getSelectedActivityId() : null;
    }

    public boolean isSelectedActivityLongline() {
        return selectedActivityId != null && IdDtos.isLonglineId(selectedActivityId);
    }

    public boolean isSelectedActivitySeine() {
        return selectedActivityId != null && IdDtos.isSeineId(selectedActivityId);
    }

    public void setSelectedActivityId(String selectedActivityId) {
        String oldValue = getSelectedActivityId();
        this.selectedActivityId = selectedActivityId;
        firePropertyChange(PROPERTY_SELECTED_ACTIVITY_ID, oldValue, this.selectedActivityId);
    }

    public String getSelectedSetId() {
        return selectedSetId;
    }

    public String getSelectedSetLonglineId() {
        return isSelectedActivityLongline() ? getSelectedSetId() : null;
    }

    public String getSelectedSetSeineId() {
        return isSelectedActivitySeine() ? getSelectedSetId() : null;
    }

    public void setSelectedSetId(String selectedSetId) {
        String oldValue = getSelectedSetId();
        this.selectedSetId = selectedSetId;
        firePropertyChange(PROPERTY_SELECTED_SET_ID, oldValue, this.selectedSetId);
    }

    public String getSelectedFloatingObjectId() {
        return selectedFloatingObjectId;
    }

    public void setSelectedFloatingObjectId(String selectedFloatingObjectId) {
        String oldValue = getSelectedFloatingObjectId();
        this.selectedFloatingObjectId = selectedFloatingObjectId;
        firePropertyChange(PROPERTY_SELECTED_FLOATING_OBJECT_ID, oldValue, this.selectedFloatingObjectId);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        boolean oldValue = getEnabled();
        this.enabled = enabled;
        firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    }

    public boolean isSelectionChanged() {
        return selectionChanged;
    }

    public boolean getSelectionChanged() {
        return selectionChanged;
    }

    public void setSelectionChanged(boolean selectionChanged) {
        boolean oldValue = getSelectionChanged();
        this.selectionChanged = selectionChanged;
        firePropertyChange(PROPERTY_SELECTION_CHANGED, oldValue, selectionChanged);
    }

    public DataContext() {
    }

    static DataContextType[] types;

    static DataContextType[] reverseTypes;

    public DataContextType[] getTypes() {
        if (types == null) {
            types = DataContextType.values();
        }
        return types;
    }

    public DataContextType[] getReverseTypes() {
        if (reverseTypes == null) {
            List<DataContextType> list = Arrays.asList(DataContextType.values());
            Collections.reverse(list);
            reverseTypes = list.toArray(new DataContextType[list.size()]);
        }

        return reverseTypes;
    }

    public String getHigherOpenId() {
        for (DataContextType type : getReverseTypes()) {
            String result = type.getOpenId(this);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    public String getHigherSelectedId() {

        for (DataContextType type : getReverseTypes()) {
            String result = type.getSelectedId(this);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    public String[] getSelectedIds() {

        List<String> ids = new ArrayList<>();

        for (DataContextType type : getTypes()) {
            String result = type.getSelectedId(this);
            if (result != null) {
                ids.add(result);
            }
        }
        return ids.toArray(new String[ids.size()]);
    }

    public String[] getOpenIds() {

        List<String> ids = new ArrayList<>();

        for (DataContextType type : getTypes()) {
            String result = type.getOpenId(this);
            if (result != null) {
                ids.add(result);
            }
        }
        return ids.toArray(new String[ids.size()]);
    }

    public String[] getOpenIds(Class<?> type) {

        String[] result = null;

        if (isOpenProgram()) {

            List<String> ids = new ArrayList<>();
            ids.add(getOpenProgramId());

            if (!ProgramDto.class.equals(type) && isOpenTrip()) {

                String tripId = getOpenTripId();
                ids.add(tripId);

                if (IdDtos.isTripSeineId(tripId)) {

                    // on a seine trip
                    if (!IdDtos.isTripClass(type) && isOpenRoute()) {

                        ids.add(getOpenRouteId());

                        if (!RouteDto.class.equals(type) && isOpenActivitySeine()) {

                            ids.add(getOpenActivitySeineId());

                            if (!IdDtos.isActivityClass(type) && isOpenSetSeine()) {
                                ids.add(getOpenSetSeineId());
                            }
                        }
                    }

                } else {

                    // on a longline trip
                    if (!IdDtos.isTripClass(type) && isOpenActivityLongline()) {

                        ids.add(getOpenActivityLonglineId());

                        if (!IdDtos.isActivityClass(type) && isOpenSetLongline()) {
                            ids.add(getOpenSetLonglineId());
                        }

                    }

                }

            }

            result = ids.toArray(new String[ids.size()]);

        }

        return result;
    }

    public boolean isOpenProgram() {
        return openProgramId != null;
    }

    public boolean isOpenTrip() {
        return isOpenTripLongline() || isOpenTripSeine();
    }

    public boolean isOpenTripLongline() {
        return openTripLonglineId != null;
    }

    public boolean isOpenTripSeine() {
        return openTripSeineId != null;
    }

    public boolean isOpenRoute() {
        return openRouteId != null;
    }

    public boolean isOpenActivity() {
        return isOpenActivityLongline() || isOpenActivitySeine();
    }

    public boolean isOpenActivityLongline() {
        return openActivityLonglineId != null;
    }

    public boolean isOpenActivitySeine() {
        return openActivitySeineId != null;
    }


    public boolean isOpenSet() {
        return isOpenSetLongline() || isOpenSetSeine();
    }

    public boolean isOpenSetLongline() {
        return openSetLonglineId != null;
    }

    public boolean isOpenSetSeine() {
        return openSetSeineId != null;
    }

    public boolean isSelectedOpen(Class<?> type) {

        for (DataContextType entityType : DataContextType.values()) {
            if (entityType.acceptType(type)) {

                // bon type trouve

                String openId = entityType.getOpenId(this);
                String selectedId = entityType.getSelectedId(this);
                if (selectedId == null || openId == null) {

                    // pas selectionne ou rien d'ouvert
                    return false;
                }

                return selectedId.equals(openId);
            }
        }

        // type non trouve
        return false;
    }

    public void populateSelectedIds(String... selectedId) {

        List<String> oldSelection = Arrays.asList(getSelectedIds());

        if (log.isDebugEnabled()) {
            log.debug("old selection = " + oldSelection);
        }

        // on nettoye toujours toutes les anciennes sélections
        // avant de positionner les nouvelles
        resetSelect();

        List<String> realSelection = null;
        if (selectedId != null && selectedId.length > 0) {

            realSelection = new ArrayList<>();
            for (String s : selectedId) {
                if (s == null) {

                    // ce cas peut arriver lors de la creation d'un nouvel objet
                    continue;
                }
                DataContextType type = DataContextType.acceptId(s);
                if (type == null) {

                    if (log.isWarnEnabled()) {
                        log.warn("Could not find type for id : " + s);
                    }
                    continue;
                }

                type.setSelectedId(this, s);
                realSelection.add(s);
                if (log.isDebugEnabled()) {
                    log.debug("add selectedId : " + s);
                }
            }
        }

        if (oldSelection.equals(realSelection)) {

            // rien a change
            if (log.isDebugEnabled()) {
                log.debug("selection does not changed");
            }
            return;
        }

        if (log.isInfoEnabled()) {
            log.info("new selection = " + realSelection);
        }

        // on change toujours l'état de selection

        setSelectionChanged(true);
    }

    public void populateOpens(String... openIds) {
        if (!getEnabled()) {

            if (log.isWarnEnabled()) {
                log.warn(this + " is not enabled");
            }
            // service non initialisé
            resetOpen();
            return;
        }

        List<String> oldSelection = Arrays.asList(getOpenIds());
        List<String> realSelection = new ArrayList<>();

        if (openIds != null && openIds.length > 0) {

            for (String s : openIds) {
                if (s == null) {

                    // ce cas peut arriver lors de la creation d'un nouvel objet
                    continue;
                }
                DataContextType type = DataContextType.acceptId(s);
                if (type == null) {

                    if (log.isWarnEnabled()) {
                        log.warn("Could not find type for id : " + s);
                    }
                    continue;
                }

                if (!type.canOpen()) {

                    // pas de traitement open
                    continue;
                }

                type.setOpenId(this, s);
                realSelection.add(s);
                if (log.isDebugEnabled()) {
                    log.debug("add openId : " + s);
                }
            }
        }

        // if only the program is open, remove it
        if (realSelection.size() == 1) {
            setOpenProgramId(null);
            realSelection.clear();
        }

        if (oldSelection.equals(realSelection)) {

            // rien a change
            if (log.isDebugEnabled()) {
                log.debug("openIds selection does not changed");
            }
            return;
        }

        if (log.isInfoEnabled()) {
            log.info("new openIds selection = " + realSelection);
            log.info("old openIds selection = " + oldSelection);
        }

        for (String s : oldSelection) {

            if (realSelection.contains(s)) {
                // pas bouge rien a faire
                continue;
            }

            DataContextType type = DataContextType.acceptId(s);
            if (type == null) {

                if (log.isWarnEnabled()) {
                    log.warn("Could not find type for id : " + s);
                }
                continue;
            }

            if (!type.canOpen()) {

                // pas de traitement open
                continue;
            }

            String newId = type.getOpenId(this);
            if (newId != null) {

                if (log.isInfoEnabled()) {
                    log.info("will remove old obsolete open id : " + s);
                }

                // plus d'id pour ce type, on le supprime
                type.setOpenId(this, null);
            }
        }
    }

    protected void reset() {
        resetOpen();
        resetSelect();
    }

    protected void resetSelect() {
        setSelectedProgramId(null);
        setSelectedTripId(null);
        setSelectedRouteId(null);
        setSelectedActivityId(null);
        setSelectedFloatingObjectId(null);
        setSelectedSetId(null);
        selectionChanged = false;
    }

    public void resetOpen() {
        setOpenProgramId(null);
        setOpenTripSeineId(null);
        setOpenTripLonglineId(null);
        setOpenRouteId(null);
        setOpenActivitySeineId(null);
        setOpenActivityLonglineId(null);
    }

    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }

} //DataContext
