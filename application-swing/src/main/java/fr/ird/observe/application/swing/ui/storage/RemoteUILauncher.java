/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.storage.tabs.SecurityModel;
import jaxx.runtime.JAXXContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

/**
 * Un wizard pour effectuer des opération de création ou mise à jour d'une
 * base distante.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class RemoteUILauncher extends StorageUILauncher {

    /** Logger */
    private static final Log log = LogFactory.getLog(RemoteUILauncher.class);

    protected ObstunaAdminAction action;

    public RemoteUILauncher(ObstunaAdminAction action,
                            JAXXContext context,
                            Window frame,
                            String title) {
        super(context, frame, title);
        this.action = action;
    }

    @Override
    protected void init(StorageUI ui) {
        super.init(ui);

        StorageUIModel model = ui.getModel();
        model.setCanCreateLocalService(false);
        model.setCanUseLocalService(false);
        model.setCanUseRemoteService(true);
        model.setCanUseServerService(true);
        model.setDbMode(DbMode.USE_REMOTE);
        model.setAdminAction(action);

        List<StorageStep> steps = new ArrayList<>();
        steps.add(StorageStep.CONFIG);

        if (action == ObstunaAdminAction.CREATE) {

            // configuration de l'import de référentiel
            steps.add(StorageStep.CONFIG_REFERENTIEL);

            // configuration de l'import de données
            steps.add(StorageStep.CONFIG_DATA);

            // choix des données à importer
            steps.add(StorageStep.SELECT_DATA);

        }
        steps.add(StorageStep.ROLES);
        steps.add(StorageStep.CONFIRM);

        model.setSteps(steps.toArray(new StorageStep[steps.size()]));
        ui.setSize(800, 600);
    }

    @Override
    protected void doAction(StorageUI ui) {
        super.doAction(ui);
        StorageUIModel model = ui.getModel();
        SecurityModel securityModel = model.getSecurityModel();
        if (log.isInfoEnabled()) {
            log.info("Will use security model " + securityModel);
        }

        try {

            initTask(model);
        } catch (Exception e) {
            UIHelper.handlingError("Could not init task.", e);
            throw new RuntimeException(e);
        }

        try {
            execute();

        } catch (Exception e) {
            UIHelper.handlingError("Could not create db.", e);
            throw new RuntimeException(e);
        }

        try {
            applySecurity();
        } catch (Exception e) {
            UIHelper.handlingError("Could not apply security to db.", e);
            throw new RuntimeException(e);
        }
    }

//    @Override
//    protected void doClose(StorageUI ui, boolean wasCanceld) {
//        super.doClose(ui, wasCanceld);
//        ui.setVisible(false);
//    }

    protected abstract String getPgLabel();

    protected void initTask(StorageUIModel model) throws Exception {
        // FIXME
        // task.init(model.toPostgresStorageConfig(getPgLabel()), model.getSecurityModel(), false);
    }

    protected void execute() throws Exception {
    }

    protected void applySecurity() {
    }
}
