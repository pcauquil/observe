package fr.ird.observe.application.swing;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.dto.IdDtos;
import fr.ird.observe.services.dto.referential.ProgramDto;

import java.io.Closeable;
import java.util.Objects;

/**
 * Pour gérer les données ouvertes sur une source de données dans l'application.
 * À l'ouverture d'une source de données rien n'est ouvert.
 * Les états de cet objet sont liés au cylce de vie d'une source de données dans l'application.
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveOpenDataManager implements Closeable {

    private final DataContext dataContext;

    public ObserveOpenDataManager(DataContext dataContext) {
        this.dataContext = dataContext;
    }

    // Program
    public boolean canOpenProgram() {
        return !dataContext.isOpenProgram();
    }

    public void openProgram(String programId) {
        Objects.requireNonNull(programId, "id cant be null");
        Preconditions.checkState(canOpenProgram(), "a program is already opened");
        dataContext.setOpenProgramId(programId);
    }

    public boolean isOpenProgram(String programId) {
        Objects.requireNonNull(programId, "id cant be null");
        return programId.equals(dataContext.getOpenProgramId());
    }

    public void closeProgram(String programId) {
        Objects.requireNonNull(programId, "id cant be null");
        Preconditions.checkState(isOpenProgram(programId), "this program is not opened");
        dataContext.setOpenProgramId(null);
    }


    // Trip Seine
    public boolean canOpenTripSeine() {
        return !dataContext.isOpenTrip();
    }

    public void openTripSeine(String programId, String tripSeineId) {
        openProgram(programId);
        Objects.requireNonNull(tripSeineId, "id cant be null");
        Preconditions.checkState(canOpenTripSeine(), "a trip is already opened");
        dataContext.setOpenTripSeineId(tripSeineId);
    }

    public boolean isOpenTripSeine(String tripSeineId) {
        Objects.requireNonNull(tripSeineId, "id cant be null");
        return tripSeineId.equals(dataContext.getOpenTripSeineId());
    }

    public void closeTripSeine(String tripSeineId) {
        Objects.requireNonNull(tripSeineId, "id cant be null");
        Preconditions.checkState(isOpenTripSeine(tripSeineId), "this trip is not opened");

        if (dataContext.isOpenRoute()) {
            String openRouteId = dataContext.getOpenRouteId();
            closeRoute(openRouteId);
        }

        dataContext.setOpenTripSeineId(null);
        dataContext.setOpenProgramId(null);
    }

    // Route
    public boolean canOpenRoute(String parentTripSeineId) {
        Objects.requireNonNull(parentTripSeineId, "id cant be null");
        return isOpenTripSeine(parentTripSeineId) && !dataContext.isOpenRoute();
    }

    public void openRoute(String parentTripSeineId, String routeId) {
        Objects.requireNonNull(parentTripSeineId, "id cant be null");
        Objects.requireNonNull(routeId, "id cant be null");
        Preconditions.checkState(canOpenRoute(parentTripSeineId), "the trip is not opened or another route already opened");
        dataContext.setOpenRouteId(routeId);
    }

    public boolean isOpenRoute(String routeId) {
        Objects.requireNonNull(routeId, "id cant be null");
        return routeId.equals(dataContext.getOpenRouteId());
    }

    public void closeRoute(String routeId) {
        Objects.requireNonNull(routeId, "id cant be null");
        Preconditions.checkState(isOpenRoute(routeId), "this route is not opened");

        if (dataContext.isOpenActivitySeine()) {
            String openActivitySeineId = dataContext.getOpenActivitySeineId();
            closeActivitySeine(openActivitySeineId);
        }

        dataContext.setOpenRouteId(null);
    }

    // Activity Seine
    public boolean canOpenActivitySeine(String parentRouteId) {
        Objects.requireNonNull(parentRouteId, "id cant be null");
        return isOpenRoute(parentRouteId) && !dataContext.isOpenActivitySeine();
    }

    public void openActivitySeine(String parentRouteId, String activitySeineId) {
        Objects.requireNonNull(parentRouteId, "id cant be null");
        Objects.requireNonNull(activitySeineId, "id cant be null");
        Preconditions.checkState(canOpenActivitySeine(parentRouteId), "the route is not opened or another activity is already opened");
        dataContext.setOpenActivitySeineId(activitySeineId);
    }

    public boolean isOpenActivitySeine(String activitySeineId) {
        Objects.requireNonNull(activitySeineId, "id cant be null");
        return activitySeineId.equals(dataContext.getOpenActivitySeineId());
    }

    public void closeActivitySeine(String activitySeineId) {
        Objects.requireNonNull(activitySeineId, "id cant be null");
        Preconditions.checkState(isOpenActivitySeine(activitySeineId), "this activity is not opened");
        dataContext.setOpenActivitySeineId(null);
    }

    // Trip Longline
    public boolean canOpenTripLongline() {
        return !dataContext.isOpenTrip();
    }

    public void openTripLongline(String programId, String tripLongLineId) {
        openProgram(programId);
        Objects.requireNonNull(tripLongLineId, "id cant be null");
        Preconditions.checkState(canOpenTripLongline(), "a trip is already opened");
        dataContext.setOpenTripLonglineId(tripLongLineId);
    }

    public boolean isOpenTripLongline(String tripLongLineId) {
        Objects.requireNonNull(tripLongLineId, "id cant be null");
        return tripLongLineId.equals(dataContext.getOpenTripLonglineId());
    }

    public void closeTripLongline(String tripLongLineId) {
        Objects.requireNonNull(tripLongLineId, "id cant be null");
        Preconditions.checkState(isOpenTripLongline(tripLongLineId), "this trip is not opened");

        if (dataContext.isOpenActivityLongline()) {
            String openActivityLonglineId = dataContext.getOpenActivityLonglineId();
            closeActivityLongline(openActivityLonglineId);
        }

        dataContext.setOpenTripLonglineId(null);
        dataContext.setOpenProgramId(null);
    }

    // Activity Longline
    public boolean canOpenActivityLongline(String parentTripLonglineId) {
        Objects.requireNonNull(parentTripLonglineId, "id cant be null");
        return isOpenTripLongline(parentTripLonglineId) && !dataContext.isOpenActivityLongline();
    }

    public void openActivityLongline(String parentTripLonglineId, String activityLonglineId) {
        Objects.requireNonNull(parentTripLonglineId, "id cant be null");
        Objects.requireNonNull(activityLonglineId, "id cant be null");
        Preconditions.checkState(canOpenActivityLongline(parentTripLonglineId), "the trip is not opened or another activity is already opened");
        dataContext.setOpenActivityLonglineId(activityLonglineId);
    }

    public boolean isOpenActivityLongline(String activityLonglineId) {
        Objects.requireNonNull(activityLonglineId, "id cant be null");
        return activityLonglineId.equals(dataContext.getOpenActivityLonglineId());
    }

    public void closeActivityLongline(String activityLonglineId) {
        Objects.requireNonNull(activityLonglineId, "id cant be null");
        Preconditions.checkState(isOpenActivityLongline(activityLonglineId), "this activity is not opened");
        dataContext.setOpenActivityLonglineId(null);
    }

    public boolean isOpen(String openableId) {
        return isOpenTripSeine(openableId)
                || isOpenRoute(openableId)
                || isOpenActivitySeine(openableId)
                || isOpenTripLongline(openableId)
                || isOpenActivityLongline(openableId);
    }

    public boolean isOpenActivity(String activityId) {
        return isOpenActivitySeine(activityId)
                || isOpenActivityLongline(activityId);
    }

    @Override
    public void close() {
        dataContext.resetOpen();
    }

    public void sanitizeOpenIds(String[] ids) {
        if (ids != null) {

            boolean exists = true;

            ObserveServicesProvider servicesProvider = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider();

            for (int i = 0, l = ids.length; i < l; i++) {
                String id = ids[i];

                // si l'id précédent existe, on vérifie l'actuel
                // sinon, on met à null
                if (exists) {
                    if (IdDtos.isProgramId(id)) {

                        exists = servicesProvider.newReferentialService().exists(ProgramDto.class, id);

                    } else if (IdDtos.isTripSeineId(id)) {

                        exists = servicesProvider.newTripSeineService().exists(id);

                    } else if (IdDtos.isRouteId(id)) {

                        exists = servicesProvider.newRouteService().exists(id);

                    } else if (IdDtos.isActivitySeineId(id)) {

                        exists = servicesProvider.newActivitySeineService().exists(id);

                    } else if (IdDtos.isSetSeineId(id)) {

                        exists = servicesProvider.newSetSeineService().exists(id);

                    } else if (IdDtos.isFloatingObjectId(id)) {

                        exists = servicesProvider.newFloatingObjectService().exists(id);

                    } else if (IdDtos.isTripLonglineId(id)) {

                        exists = servicesProvider.newTripLonglineService().exists(id);

                    } else if (IdDtos.isActivityLonglineId(id)) {

                        exists = servicesProvider.newActivityLonglineService().exists(id);

                    } else if (IdDtos.isSetLonglineId(id)) {

                        exists = servicesProvider.newSetLonglineService().exists(id);

                    } else {

                        exists = false;
                    }
                }

                if (!exists) {
                    ids[i] = null;
                }
            }
        }
    }
}
