package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.tools.Server;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class StartH2WebServerAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(StartH2WebServerAction.class);

    private final ObserveMainUI ui;

    public StartH2WebServerAction(ObserveMainUI ui) {

        super(t("observe.action.start.h2.web.server"), SwingUtil.getUIManagerActionIcon("db-start-server"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.start.h2.web.server.tip"));
        putValue(MNEMONIC_KEY, (int) 'S');

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        if (ui.isH2WebServer()) {

            if (log.isWarnEnabled()) {
                log.warn("Can not start h2 web server... (web server already found)");
            }

        } else {

            if (log.isInfoEnabled()) {
                log.info("Will start h2 web server ...");
            }

            try {

                Server server = Server.createWebServer("-webAllowOthers");
                server.start();

                Server.openBrowser(server.getURL());

                // On mémorise l'instance du server dans le contexte applicatif afin de pouvoir la récupérer plus tard,
                // par exemple lorsque l'on souhaitera arrêter le server.
                ObserveSwingApplicationContext.get().setH2WebServer(server);

                ui.setH2WebServer(true);

            } catch (Exception e) {

                UIHelper.handlingError("Could not start h2 web server ", e);
            }

        }


    }

}
