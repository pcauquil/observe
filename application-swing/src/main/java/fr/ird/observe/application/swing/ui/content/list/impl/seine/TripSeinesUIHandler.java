/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.list.ContentListUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TripSeinesUIHandler extends ContentListUIHandler<ProgramDto, TripSeineDto> {

    /** Logger */
    static private final Log log = LogFactory.getLog(TripSeinesUIHandler.class);

    public TripSeinesUIHandler(TripSeinesUI ui) {
        super(ui, DataContextType.Program, DataContextType.TripSeine);
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String openProgramId = dataContext.getOpenProgramId();

        if (openProgramId == null) {

            // pas de program ouvert (donc pas de maree ouverte)
            // on peut reouvrir une maree
            addInfoMessage(n("observe.content.tripSeine.message.no.active.found"));
            return ContentMode.CREATE;
        }

        //
        // il existe un maree ouverte
        //

        if (dataContext.isSelectedOpen(ProgramDto.class)) {

            // le program courant a une maree ouverte
            addInfoMessage(n("observe.content.tripSeine.message.active.found"));
            return ContentMode.UPDATE;
        }

        //
        // la marée ouverte est dans un autre program
        //

        addInfoMessage(n("observe.content.tripSeine.message.active.found.for.other.program"));
        return ContentMode.READ;
    }

    @Override
    protected void finalizeOpenUI() {
        Class referenceType = ProgramDto.class;
        ReferentialReference<ProgramDto> programRef = getDataSource().getReferentialReference(referenceType, getSelectedParentId());
        String title = getDecoratorService().getReferentialReferenceDecorator(referenceType).toString(programRef);
        getUi().setContentTitle(title);
    }

    @Override
    protected List<DataReference<TripSeineDto>> getChilds(String parentId) {

        TripSeineService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
        DataReferenceSet<TripSeineDto> tripSeineByProgram = service.getTripSeineByProgram(parentId);

        if (log.isDebugEnabled()) {
            log.debug("Will use " + tripSeineByProgram.sizeReference() + " trips.");
        }

        return new ArrayList<>(tripSeineByProgram.getReferences());

    }
}
