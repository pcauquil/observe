package fr.ird.observe.application.swing.ui.content.open.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDto;
import fr.ird.observe.services.dto.referential.longline.VesselActivityLonglineDtos;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUIModel;
import org.nuiton.util.DateUtil;

import java.util.Date;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class ActivityLonglineUIModel extends ContentOpenableUIModel<ActivityLonglineDto> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SET_OPERATION = "setOperation";

    public static final String PROPERTY_DATE = "date";

    public static final String PROPERTY_TIME = "time";

    public ActivityLonglineUIModel() {
        super(ActivityLonglineDto.class);

        getBean().addPropertyChangeListener(ActivityLonglineDto.PROPERTY_VESSEL_ACTIVITY_LONGLINE, evt -> {
            boolean oldValue = VesselActivityLonglineDtos.isSetOperation((ReferentialReference<VesselActivityLonglineDto>) evt.getOldValue());
            boolean newValue = VesselActivityLonglineDtos.isSetOperation((ReferentialReference<VesselActivityLonglineDto>) evt.getNewValue());
            firePropertyChange(PROPERTY_SET_OPERATION, oldValue, newValue);
        });
    }

    public boolean isSetOperation() {
        return VesselActivityLonglineDtos.isSetOperation(bean.getVesselActivityLongline());
    }

    public Date getDate() {
        Date timeStamp = bean.getTimeStamp();
        return timeStamp == null ? null : DateUtil.getDay(timeStamp);
    }

    public Date getTime() {
        Date timeStamp = bean.getTimeStamp();
        return timeStamp == null ? null : DateUtil.getTime(timeStamp, false, false);
    }

    public void setDate(Date date) {
        Date timeStamp = bean.getTimeStamp();
        if (timeStamp != null) {
            Date dateAndTime = date == null ? timeStamp : DateUtil.getDateAndTime(date, timeStamp, true, false);
            bean.setTimeStamp(dateAndTime);
        }
    }

    public void setTime(Date time) {
        Date timeStamp = bean.getTimeStamp();
        if (timeStamp != null) {
            Date dateAndTime = time == null ? timeStamp : DateUtil.getDateAndTime(timeStamp, time, false, false);
            bean.setTimeStamp(dateAndTime);
        }
    }
}
