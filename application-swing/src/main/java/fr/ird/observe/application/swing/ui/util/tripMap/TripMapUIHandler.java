package fr.ird.observe.application.swing.ui.util.tripMap;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.services.dto.IdDtos;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.TripMapPointDto;
import fr.ird.observe.application.swing.ui.UIHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.swing.JMapPane;
import org.geotools.swing.event.MapPaneEvent;
import org.geotools.swing.event.MapPaneListener;

import javax.imageio.ImageIO;
import java.awt.CardLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripMapUIHandler {

    public static final double ZOOM_STEP_RATIO = 0.1;

    public static TripMapUIHandler newHandler(TripMapUI view) {
        return new TripMapUIHandler(view);
    }

    private static final Log log = LogFactory.getLog(TripMapUIHandler.class);

    protected final TripMapUI view;

    private ObserveSwingApplicationConfig config;
    protected ReferencedEnvelope tripArea;

    protected boolean rendererRunning;

    public TripMapUIHandler(TripMapUI view) {
        this.view = view;
    }

    public void initUI() {

        ObserveMapPane mapPane = getObserveMapPane();

        MouseMapListener mouseMapListener = new MouseMapListener();
        mapPane.addMouseWheelListener(mouseMapListener);
        mapPane.addMouseMotionListener(mouseMapListener);
        mapPane.addMouseListener(mouseMapListener);
        mapPane.addMapPaneListener(new TripMapListener());

        mapPane.setComponentPopupMenu(view.getMapPopupMenu());

        rendererRunning = false;

    }

    public void setConfig(ObserveSwingApplicationConfig config) {
        this.config = config;

        JMapPane mapPane = getObserveMapPane();
        mapPane.setBackground(config.getMapBackgroundColor());
    }

    protected ObserveMapPane getObserveMapPane() {
        return view.getObserveMapPane();
    }

    public void doOpenMap(TripMapDto tripMapDto) {

        try {
            ((CardLayout) view.getLayout()).first(view);
            ObserveSwingApplicationContext.get().getMainUI().setBusy(true);

            ObserveMapPane mapPane = getObserveMapPane();

            if (mapPane.getMapContent() != null) {
                // appeler pour libéré les listeners
                mapPane.getMapContent().dispose();
            }

            List<TripMapPointDto> tripMapPoints = Lists.newArrayList(tripMapDto.getPoints());


            TripMapContentBuilder mapContentBuilder = new TripMapContentBuilder();
            mapContentBuilder.setStyledLayerDescriptor(config.getMapStyleFile());

            for (File layerFile : config.getMapLayerFiles()) {
                mapContentBuilder.addLayer(layerFile);
            }

            if (IdDtos.isSeineId(tripMapDto.getId())) {

                mapContentBuilder.addTripLine(tripMapPoints);

            } else if (IdDtos.isLonglineId(tripMapDto.getId())) {

                mapContentBuilder.addLonglineFishingZone(tripMapPoints);
                mapContentBuilder.addLonglineLine(tripMapPoints);

            }

            mapContentBuilder.addPoints(tripMapPoints);

            // set zoom
            tripArea = new ReferencedEnvelope();
            for (TripMapPointDto point : tripMapPoints) {
                tripArea.expandToInclude(new DirectPosition2D(point.getLongitude(), point.getLatitude()));
            }
            tripArea.expandBy(1.1);

            mapPane.setMapContent(mapContentBuilder.getMapContent());
            mapPane.setLegendItems(mapContentBuilder.getLegendItems());

        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("Unable to load trip map activity points", e);
        } finally {
            ObserveSwingApplicationContext.get().getMainUI().setBusy(false);
        }

    }

    public void doCloseMap() {
        ((CardLayout) view.getLayout()).first(view);
    }

    public void zoomIt() {
        if (! tripArea.isEmpty()) {
            JMapPane mapPane = getObserveMapPane();
            mapPane.setDisplayArea(tripArea);
        }
    }

    public void exportPng() {

        File file = UIHelper.chooseFile(
                view,
                t("observe.content.map.export.chooseFile.title"),
                t("observe.content.map.export.chooseFile.ok"),
                null,
                "^.+\\.png|.+\\.PNG$",
                t("observe.content.map.export.chooseFile.png"));

        if (file != null && UIHelper.confirmOverwriteFileIfExist(view, file)) {

            BufferedImage im = new BufferedImage(view.getWidth(), view.getHeight(), BufferedImage.TYPE_INT_ARGB);
            view.paint(im.getGraphics());
            try {
                ImageIO.write(im, "PNG", file);
            } catch (IOException e) {
                throw new ObserveSwingTechnicalException("unable to export map ", e);
            }

            UIHelper.displayInfo(t("observe.content.map.export.success", file));
        }
    }

    protected double zoomRatio = 1;
    protected Point zoomCenter;

    protected void zoomApply() {
        if (zoomRatio != 1 && ! rendererRunning) {

            JMapPane mapPane = getObserveMapPane();

            ReferencedEnvelope displayArea = mapPane.getDisplayArea();

            double deltaWidth = displayArea.getWidth() * (zoomRatio -1);
            double deltaHeight = displayArea.getHeight() * (zoomRatio - 1);

            double ratioLeft  = zoomCenter.getX() * 1d / mapPane.getWidth();

            // l'axe de Y est inversé entre le référentiel du composant swing et le référentiel géographique
            double ratioTop  = 1 - (zoomCenter.getY() * 1d / mapPane.getHeight());

            double deltaLeft = deltaWidth * ratioLeft;
            double deltaRight = deltaLeft - deltaWidth;

            double deltaTop = deltaHeight * ratioTop;
            double deltaBottom = deltaTop - deltaHeight;

            if (log.isDebugEnabled()) {
                log.debug(String.format("Map mouse zoom (zoom ratio : %s, deltaLeft : %s, deltaRight : %s, deltaTop : %s, deltaBottom : %s)",
                        zoomRatio, deltaLeft, deltaRight, deltaRight, deltaBottom));
            }

            ReferencedEnvelope newDisplayArea = new ReferencedEnvelope(
                    displayArea.getMinX() + deltaLeft,
                    displayArea.getMaxX() + deltaRight,
                    displayArea.getMinY() + deltaTop,
                    displayArea.getMaxY() + deltaBottom,
                    displayArea.getCoordinateReferenceSystem()
            );

            mapPane.setDisplayArea(newDisplayArea);

            zoomRatio = 1;

        }

    }

    private class MouseMapListener implements MouseWheelListener, MouseListener, MouseMotionListener {

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            int notches = e.getWheelRotation();
            zoomRatio = zoomRatio * (1 + (ZOOM_STEP_RATIO * notches * -1));
            zoomCenter = e.getPoint();
            zoomApply();
        }

        @Override
        public void mouseClicked(MouseEvent e) {

        }

        protected Point2D startPointInWorld;
        protected AffineTransform startScreenToWorldTransform;
        protected ReferencedEnvelope startDisplayArea;

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                startMove(e.getPoint());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                endMove(e.getPoint());
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }

        @Override
        public void mouseDragged(MouseEvent e) {
//            endMove(e.getPoint());
        }

        @Override
        public void mouseMoved(MouseEvent e) {

        }

        protected void startMove(Point2D startPointInScreen) {
            JMapPane mapPane = getObserveMapPane();

            startDisplayArea = mapPane.getDisplayArea();

            startScreenToWorldTransform = mapPane.getScreenToWorldTransform();

            startPointInWorld = new Point2D.Double();

            startScreenToWorldTransform.transform(startPointInScreen, startPointInWorld);

        }

        protected void endMove(Point2D endPointInScreen) {

            Point2D endPointInWorld = new Point2D.Double();

            startScreenToWorldTransform.transform(endPointInScreen, endPointInWorld);

            double transX = startPointInWorld.getX() - endPointInWorld.getX();
            double transY = startPointInWorld.getY() - endPointInWorld.getY();

            ReferencedEnvelope endDisplayArea = new ReferencedEnvelope(startDisplayArea);

            endDisplayArea.translate(transX, transY);

            JMapPane mapPane = getObserveMapPane();

            mapPane.setDisplayArea(endDisplayArea);

            if (log.isDebugEnabled()) {
                log.debug(String.format("Translate (x : %s, y : %s)", transX, transY));
            }
        }
    }

    protected class TripMapListener implements MapPaneListener {

        protected boolean firstRendering;

        @Override
        public void onNewMapContent(MapPaneEvent ev) {
            firstRendering = true;
        }

        @Override
        public void onDisplayAreaChanged(MapPaneEvent ev) {
        }

        @Override
        public void onRenderingStarted(MapPaneEvent ev) {
            rendererRunning = true;
        }

        @Override
        public void onRenderingStopped(MapPaneEvent ev) {
            rendererRunning = false;
            if (firstRendering) {
                zoomIt();
                ((CardLayout) view.getLayout()).last(view);
                firstRendering = false;
            } else {
                zoomApply();
            }
        }
    }

}
