/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.tree;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.UIHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;

/**
 * Le modèle de sélection de l'abre de navigation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class NavigationTreeSelectionModel extends DefaultTreeSelectionModel {

    /** Logger */
    static private final Log log =
            LogFactory.getLog(NavigationTreeSelectionModel.class);

    private static final long serialVersionUID = 1L;

    @Override
    public void addSelectionPath(TreePath newPath) {
        TreePath oldPath = getSelectionPath();
        if (log.isDebugEnabled()) {
            log.debug(">------------------------------------------------------------------------------");
            log.debug("Try to change selection");
            log.debug("old path " + oldPath);
            log.debug("new path " + newPath);
        }
        boolean canChange = beforeSelectionPath(oldPath, newPath);
        if (log.isDebugEnabled()) {
            log.debug("Can change path ? " + canChange);
            log.debug("<------------------------------------------------------------------------------");
        }
        if (!canChange) {
            return;
        }

        // ok can safely select the new path
        if (log.isTraceEnabled()) {
            log.trace("will select path " + newPath);
        }
        super.addSelectionPath(newPath);
    }

    @Override
    public void setSelectionPath(TreePath newPath) {
        TreePath oldPath = getSelectionPath();
        if (log.isDebugEnabled()) {
            log.debug(">------------------------------------------------------------------------------");
            log.debug("Try to change selection");
            log.debug("old path " + oldPath);
            log.debug("new path " + newPath);
        }
        boolean canChange = beforeSelectionPath(oldPath, newPath);
        if (log.isDebugEnabled()) {
            log.debug("Can change path ? " + canChange);
            log.debug("<------------------------------------------------------------------------------");
        }
        if (!canChange) {
            return;
        }

        // ok can safely select the new path
        if (log.isTraceEnabled()) {
            log.trace("will select path " + newPath);
        }
        try {
            super.setSelectionPath(newPath);
        } catch (Exception e) {
            UIHelper.handlingError(e);
        }
    }

    protected boolean beforeSelectionPath(TreePath oldPath, TreePath newPath) {

        boolean canChange = true;

        if (newPath.equals(oldPath)) {
            // stay on same node, can skip
            if (log.isDebugEnabled()) {
                log.debug("skip stay on path " + newPath);
            }
            canChange = false;
        }

        if (canChange && !isSelectionEmpty()) {
            canChange = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        }

        return canChange;
    }
}
