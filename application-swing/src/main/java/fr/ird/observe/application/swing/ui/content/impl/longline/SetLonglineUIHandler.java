package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDtos;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.longline.SetLonglineService;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import jaxx.runtime.swing.HidorButton;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import jaxx.runtime.validator.swing.SwingValidatorMessageTableModel;
import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditorModel;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.event.TableModelListener;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/1/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class SetLonglineUIHandler extends ContentUIHandler<SetLonglineDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(SetLonglineUIHandler.class);

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    // Change toogle target visible on all TimeEditors
    protected final PropertyChangeListener onToogleTimeEditorSliderChangedListener;

    // Change toogle target visible on all TimeEditors
    protected final PropertyChangeListener onCoordinateFormatChangedListener;

//    private final LogPropertyChanges logCoordinatesChanges;

    protected boolean toogleTimeEditorSliderIsChanging;

    protected boolean coordinateFormatChangedIsChanging;

//    static ImmutableSet<String> COORDINATES_PROPERTIES = ImmutableSet.of(
//            SetLongline.PROPERTY_HAULING_START_LONGITUDE,
//            SetLongline.PROPERTY_HAULING_START_LATITUDE,
//            SetLongline.PROPERTY_HAULING_START_QUADRANT,
//            SetLongline.PROPERTY_SETTING_START_LONGITUDE,
//            SetLongline.PROPERTY_SETTING_START_LATITUDE,
//            SetLongline.PROPERTY_SETTING_START_QUADRANT,
//            SetLongline.PROPERTY_HAULING_END_LONGITUDE,
//            SetLongline.PROPERTY_HAULING_END_LATITUDE,
//            SetLongline.PROPERTY_HAULING_END_QUADRANT,
//            SetLongline.PROPERTY_SETTING_END_LONGITUDE,
//            SetLongline.PROPERTY_SETTING_END_LATITUDE,
//            SetLongline.PROPERTY_SETTING_END_QUADRANT);

    public SetLonglineUIHandler(SetLonglineUI ui) {
        super(ui, DataContextType.ActivityLongline, DataContextType.SetLongline);
//        this.logCoordinatesChanges = new LogPropertyChanges(COORDINATES_PROPERTIES);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
        onToogleTimeEditorSliderChangedListener = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onToogleTimeEditorSliderChanged(newValue);
        };
        onCoordinateFormatChangedListener = evt -> {
            CoordinateFormat newValue = (CoordinateFormat) evt.getNewValue();
            onCoordinateFormatChanged(newValue);
        };
    }

    @Override
    public SetLonglineUI getUi() {
        return (SetLonglineUI) super.getUi();
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String setId = getSelectedId();

        if (setId == null) {

            // mode creation
            return ContentMode.CREATE;

        }

        if (getOpenDataManager().isOpenActivityLongline(dataContext.getSelectedActivityLonglineId())) {

            // l'activité est ouverte, mode édition
            return ContentMode.UPDATE;

        }

        // l'activité n'est pas ouverte, mode lecture
        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(ActivityLonglineDto.class),
                   t("observe.storage.activityLongline.message.not.open"));

        return ContentMode.READ;

    }

    @Override
    public void initUI() {

        super.initUI();

        getUi().getSettingStartTimeStamp().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        getUi().getSettingEndTimeStamp().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        getUi().getHaulingStartTimeStamp().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        getUi().getHaulingEndTimeStamp().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);

        getUi().getSettingStartCoordinates().getModel().addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_FORMAT, onCoordinateFormatChangedListener);
        getUi().getSettingEndCoordinates().getModel().addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_FORMAT, onCoordinateFormatChangedListener);
        getUi().getHaulingStartCoordinates().getModel().addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_FORMAT, onCoordinateFormatChangedListener);
        getUi().getHaulingEndCoordinates().getModel().addPropertyChangeListener(CoordinatesEditorModel.PROPERTY_FORMAT, onCoordinateFormatChangedListener);

//        getBean().removePropertyChangeListener(logCoordinatesChanges);
//        getBean().addPropertyChangeListener(logCoordinatesChanges);
    }


    @Override
    public void openUI() {

        getUi().getSettingStartCoordinates().resetModel();
        getUi().getSettingEndCoordinates().resetModel();
        getUi().getHaulingStartCoordinates().resetModel();
        getUi().getHaulingEndCoordinates().resetModel();

        // Reset quandrant
        resetQuadrant(getUi().getSettingStartCoordinates());
        resetQuadrant(getUi().getSettingEndCoordinates());
        resetQuadrant(getUi().getHaulingStartCoordinates());
        resetQuadrant(getUi().getHaulingEndCoordinates());

        if (log.isInfoEnabled()) {
            log.info("OpenUI: " + getModel());
        }
        super.openUI();

        String activityId = getSelectedParentId();
        String setId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info("activityId = " + activityId);
            log.info("setId      = " + setId);
        }
        ContentMode mode = computeContentMode();

        SetLonglineDto bean = getBean();

        Form<SetLonglineDto> form;
        if (setId == null) {

            // create mode
            form = getSetLonglineService().preCreate(activityId);

        } else {

            // update mode
            form = getSetLonglineService().loadForm(setId);

        }

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        SetLonglineDtos.copySetLonglineDto(form.getObject(), bean);

        getUi().getSettingStartCoordinates().setLatitudeAndLongitude(bean.getSettingStartLatitude(), bean.getSettingStartLongitude());
        getUi().getSettingEndCoordinates().setLatitudeAndLongitude(bean.getSettingEndLatitude(), bean.getSettingEndLongitude());
        getUi().getHaulingStartCoordinates().setLatitudeAndLongitude(bean.getHaulingStartLatitude(), bean.getHaulingStartLongitude());
        getUi().getHaulingEndCoordinates().setLatitudeAndLongitude(bean.getHaulingEndLatitude(), bean.getHaulingEndLongitude());

        // utilisation du mode requis
        setContentMode(mode);

        // To be sure always remove listener (could prevent some leaks)
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        getUi().getErrorTableModel().addTableModelListener(computeTabValidStateListener);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

        if (mode != ContentMode.READ) {
            getUi().startEdit(null);
        }

    }

    @Override
    protected void closeSafeUI() {

        if (log.isInfoEnabled()) {
            log.info("CloseUI: " + getModel());
        }
        super.closeSafeUI();

        // remove listener
        getUi().getErrorTableModel().removeTableModelListener(computeTabValidStateListener);

    }

    @Override
    public void startEditUI(String... binding) {

        ContentUIModel<SetLonglineDto> model = getModel();
        boolean create = model.getMode() == ContentMode.CREATE;
        String contextName = getValidatorContextName(model.getMode());
        getUi().getValidator().setContext(contextName);
        if (create) {
            addInfoMessage(t("observe.content.setLongline.message.creating"));
        } else {
            addInfoMessage(t("observe.content.setLongline.message.updating"));
        }

        super.startEditUI(SetLonglineUI.BINDING_COMMENT2_TEXT,

                          // setting tab
                          SetLonglineUI.BINDING_HOME_ID_TEXT,
                          SetLonglineUI.BINDING_NUMBER_MODEL,

                          SetLonglineUI.BINDING_SETTING_START_TIME_STAMP_DATE,
                          SetLonglineUI.BINDING_SETTING_START_COORDINATES_LATITUDE,
                          SetLonglineUI.BINDING_SETTING_START_COORDINATES_LONGITUDE,

                          SetLonglineUI.BINDING_SETTING_END_TIME_STAMP_DATE,
                          SetLonglineUI.BINDING_SETTING_END_COORDINATES_LATITUDE,
                          SetLonglineUI.BINDING_SETTING_END_COORDINATES_LONGITUDE,

                          // setting caracteristics tab
                          SetLonglineUI.BINDING_SETTING_VESSEL_SPEED_MODEL,
                          SetLonglineUI.BINDING_SHOOTER_USED_SELECTED,
                          SetLonglineUI.BINDING_SHOOTER_SPEED_MODEL,
                          SetLonglineUI.BINDING_TIME_BETWEEN_HOOKS_MODEL,
                          SetLonglineUI.BINDING_SETTING_SHAPE_SELECTED_ITEM,
                          SetLonglineUI.BINDING_WEIGHTED_SWIVEL_SELECTED,
                          SetLonglineUI.BINDING_SWIVEL_WEIGHT_MODEL,
                          SetLonglineUI.BINDING_WEIGHTED_SNAP_SELECTED,
                          SetLonglineUI.BINDING_SNAP_WEIGHT_MODEL,
                          SetLonglineUI.BINDING_LINE_TYPE_SELECTED_ITEM,
                          SetLonglineUI.BINDING_MAX_DEPTH_TARGETED_MODEL,
                          SetLonglineUI.BINDING_MONITORED_SELECTED,
                          SetLonglineUI.BINDING_LIGHTSTICKS_TYPE_SELECTED_ITEM,
                          SetLonglineUI.BINDING_LIGHTSTICKS_COLOR_SELECTED_ITEM,
                          SetLonglineUI.BINDING_LIGHTSTICKS_PER_BASKET_COUNT_MODEL,
                          SetLonglineUI.BINDING_BASKETS_PER_SECTION_COUNT_MODEL,
                          SetLonglineUI.BINDING_TOTAL_BASKETS_COUNT_MODEL,
                          SetLonglineUI.BINDING_BRANCHLINES_PER_BASKET_COUNT_MODEL,
                          SetLonglineUI.BINDING_TOTAL_SECTIONS_COUNT_MODEL,
                          SetLonglineUI.BINDING_TOTAL_HOOKS_COUNT_MODEL,

                          // hauling tab
                          SetLonglineUI.BINDING_HAULING_DIRECTION_SAME_AS_SETTING_BOOLEAN_VALUE,

                          SetLonglineUI.BINDING_HAULING_START_TIME_STAMP_DATE,
                          SetLonglineUI.BINDING_HAULING_START_COORDINATES_LATITUDE,
                          SetLonglineUI.BINDING_HAULING_START_COORDINATES_LONGITUDE,

                          SetLonglineUI.BINDING_HAULING_END_TIME_STAMP_DATE,
                          SetLonglineUI.BINDING_HAULING_END_COORDINATES_LATITUDE,
                          SetLonglineUI.BINDING_HAULING_END_COORDINATES_LONGITUDE,

                          SetLonglineUI.BINDING_HAULING_BREAKS_MODEL);

        model.setModified(create);

    }

    @Override
    protected boolean doSave(SetLonglineDto bean) throws Exception {

        String activityId = getSelectedParentId();

        SaveResultDto saveResult = getSetLonglineService().save(activityId, bean);
        saveResult.toDto(bean);

        return true;

    }

    @Override
    protected void afterSave(boolean refresh) {

        super.afterSave(refresh);

        SetLonglineDto bean = getBean();

        SwingValidatorUtil.setValidatorChanged(getUi(), false);

        ObserveTreeHelper treeHelper = getTreeHelper(getUi());

        ObserveNode node = treeHelper.getSelectedNode();
        boolean create = node.getId() == null;

        if (create) {

            getModel().setMode(ContentMode.UPDATE);

            // remove old node and recreate new node
            ObserveNode parentNode = treeHelper.removeNode(node);

            DataReference<SetLonglineDto> beanRef = ObserveSwingApplicationContext.get().getReferenceBinderEngine().transformDataDtoToReference(getDecoratorService().getReferentialLocale(), bean);
            node = treeHelper.addSetLongline(parentNode, beanRef);

            stopEditUI();
            treeHelper.selectNode(node);

        } else {

            // select ancestor node
//            treeHelper.refreshNode(node, false);
            treeHelper.reloadSelectedNode(false, false);

        }

    }

    @Override
    protected boolean doDelete(SetLonglineDto bean) {

        if (askToDelete(bean)) {
            return false;
        }

        String activityId = getSelectedParentId();
        getSetLonglineService().delete(activityId, bean.getId());

        return true;

    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean settingTabValid = !errorProperties.removeAll(SetLonglineUIModel.SETTING_TAB_PROPERTIES);
        boolean settingCaracteristicsTabValid = !errorProperties.removeAll(SetLonglineUIModel.SETTING_CARACTERISTICS_TAB_PROPERTIES);
        boolean haulingTabValid = !errorProperties.removeAll(SetLonglineUIModel.HAULING_TAB_PROPERTIES);

        SetLonglineUIModel model = (SetLonglineUIModel) getModel();
        model.setSettingTabValid(settingTabValid);
        model.setSettingCaracteristicsTabValid(settingCaracteristicsTabValid);
        model.setHaulingTabValid(haulingTabValid);

    }

    protected void onToogleTimeEditorSliderChanged(boolean newValue) {

        if (!toogleTimeEditorSliderIsChanging) {

            toogleTimeEditorSliderIsChanging = true;

            boolean selected = !newValue;

            try {

                getUi().getSettingStartTimeStamp().getSliderHidor().setSelected(selected);
                getUi().getSettingStartTimeStamp().getSliderHidor().setTargetVisible(newValue);

                getUi().getSettingEndTimeStamp().getSliderHidor().setSelected(selected);
                getUi().getSettingEndTimeStamp().getSliderHidor().setTargetVisible(newValue);

                getUi().getHaulingStartTimeStamp().getSliderHidor().setSelected(selected);
                getUi().getHaulingStartTimeStamp().getSliderHidor().setTargetVisible(newValue);

                getUi().getHaulingEndTimeStamp().getSliderHidor().setSelected(selected);
                getUi().getHaulingEndTimeStamp().getSliderHidor().setTargetVisible(newValue);

            } finally {

                toogleTimeEditorSliderIsChanging = false;

            }

        }

    }

    protected synchronized void onCoordinateFormatChanged(CoordinateFormat newValue) {

        if (!coordinateFormatChangedIsChanging) {

            coordinateFormatChangedIsChanging = true;

            try {

                getUi().getSettingStartCoordinates().setFormat(newValue);
                getUi().getSettingEndCoordinates().setFormat(newValue);

                getUi().getHaulingStartCoordinates().setFormat(newValue);
                getUi().getHaulingEndCoordinates().setFormat(newValue);

            } finally {

                coordinateFormatChangedIsChanging = false;

            }

        }

    }

    protected SetLonglineService getSetLonglineService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newSetLonglineService();
    }
}
