package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.action;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractObserveAction;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ReferentialSynchronizeMode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroUIHandler;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchronizeTaskListModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.task.ReferentialSynchronizeTaskSupport;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeServiceEngine;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeServiceProduceSqlsRequest;
import fr.ird.observe.services.service.actions.synchro.referential.ng.ReferentialSynchronizeServiceProduceSqlsResult;
import jaxx.runtime.swing.wizard.ext.WizardState;

import java.awt.event.ActionEvent;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 12/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class ApplyAction extends AbstractObserveAction {

    private static final long serialVersionUID = 1L;

    private final ReferentialSynchroUI ui;

    public ApplyAction(ReferentialSynchroUI ui) {
        super(t("observe.action.apply"), UIHelper.getUIManagerActionIcon("accept"));
        this.ui = ui;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ui.getHandler().addAdminWorker("Application des modifications.", this::apply);

    }

    private WizardState apply() {

        ReferentialSynchroModel stepModel = ui.getStepModel();

        ReferentialSynchroUIHandler handler = ui.getHandler();

        ReferentialSynchronizeMode synchronizeMode = stepModel.getSynchronizeMode();
        boolean leftIsWrite = synchronizeMode.isLeftWrite();
        boolean rightIsWrite = synchronizeMode.isRightWrite();

        Set<Class<? extends ReferentialDto>> referentialTypesInShell = stepModel.getLeftSource().newDataSourceService().getReferentialTypesInShell();

        ReferentialSynchronizeServiceProduceSqlsRequest.Builder builder =
                ReferentialSynchronizeServiceProduceSqlsRequest.builder(stepModel.getLeftTreeModel().getIdsOnlyExistingInThisSide(),
                                                                        stepModel.getRightTreeModel().getIdsOnlyExistingInThisSide(),
                                                                        referentialTypesInShell);

        ReferentialSynchronizeTaskListModel tasks = stepModel.getTasks();

        for (ReferentialSynchronizeTaskSupport task : tasks) {

            String taskLabel = t("observe.actions.synchro.referential.task.prepare", task.getStripLabel());

            handler.sendMessage(taskLabel);

            task.registerTask(builder);

        }

        ReferentialSynchronizeServiceProduceSqlsRequest produceSqlsRequests = builder.build();

        ReferentialSynchronizeServiceEngine engine = stepModel.newReferentialSynchronizeServiceEngine();

        ReferentialSynchronizeServiceProduceSqlsResult sqlsRequests = engine.produceSqlsRequests(produceSqlsRequests);

        engine.executeSqlRequests(sqlsRequests);

        return WizardState.SUCCESSED;
    }

}
