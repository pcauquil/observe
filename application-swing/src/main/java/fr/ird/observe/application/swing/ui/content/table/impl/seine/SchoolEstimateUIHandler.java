/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialReferences;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDtos;
import fr.ird.observe.services.service.seine.SchoolEstimateService;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class SchoolEstimateUIHandler extends ContentTableUIHandler<SetSeineSchoolEstimateDto, SchoolEstimateDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(SchoolEstimateUIHandler.class);

    public SchoolEstimateUIHandler(SchoolEstimateUI ui) {
        super(ui, DataContextType.SetSeine);
    }

    @Override
    public SchoolEstimateUI getUi() {
        return (SchoolEstimateUI) super.getUi();
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, SchoolEstimateDto bean, boolean create) {

        ContentTableModel<SetSeineSchoolEstimateDto, SchoolEstimateDto> model = getTableModel();

        if (!model.isEditable()) {

            // rien a faire
            return;
        }

        SchoolEstimateUI ui = getUi();
        List<ReferentialReference<SpeciesDto>> availableEspeces;
        JComponent requestFocus;

        if (create) {
            Set<ReferentialReference<SpeciesDto>> references = getModel().getReferentialReferences(SchoolEstimateDto.PROPERTY_SPECIES);

            List<ReferentialReference<SpeciesDto>> listSpeciesUsed = model.getColumnValues(0);

            Set<String> listSpeciesIdUsed = listSpeciesUsed.stream().map(ReferentialReference::getId).collect(Collectors.toSet());

            availableEspeces = ReferentialReferences.filterNotContains(references,listSpeciesIdUsed);

            requestFocus = ui.getSpecies();
        } else {
            ReferentialReference<SpeciesDto> species = bean.getSpecies();
            availableEspeces = Collections.singletonList(species);
            requestFocus = ui.getTotalWeight();
        }
        ui.getSpecies().setData(availableEspeces);
        requestFocus.requestFocus();
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {
        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.schoolEstimate.table.speciesThon"),
                n("observe.content.schoolEstimate.table.speciesThon.tip"),
                n("observe.content.schoolEstimate.table.weight"),
                n("observe.content.schoolEstimate.table.weight.tip"),
                n("observe.content.schoolEstimate.table.meanWeight"),
                n("observe.content.schoolEstimate.table.meanWeight.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
    }

    @Override
    protected void doPersist(SetSeineSchoolEstimateDto bean) {

        SaveResultDto saveResult = getSchoolEstimateService().save(bean);
        saveResult.toDto(bean);
    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<SetSeineSchoolEstimateDto> form = getSchoolEstimateService().loadForm(beanId);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        SetSeineSchoolEstimateDtos.copySetSeineSchoolEstimateDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case ObjectSchoolEstimateDto.PROPERTY_SPECIES: {

                String speciesListId = ObserveSwingApplicationContext.get().getConfig().getSpeciesListSeineSchoolEstimateId();
                String tripSeineId = getDataContext().getSelectedTripSeineId();

                TripSeineService tripSeineService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTripSeineService();
                result = (List) tripSeineService.getSpeciesByListAndTrip(tripSeineId, speciesListId);

                result = ReferentialReferences.filterEnabled(result);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }
            }

            break;

        }

        return result;

    }

    protected SchoolEstimateService getSchoolEstimateService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newSchoolEstimateService();
    }
}
