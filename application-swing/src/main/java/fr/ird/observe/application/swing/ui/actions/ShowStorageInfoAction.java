package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import jaxx.runtime.SwingUtil;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ShowStorageInfoAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private final ObserveMainUI ui;

    public ShowStorageInfoAction(ObserveMainUI ui) {

        super(t("observe.action.info.storage"), SwingUtil.getUIManagerActionIcon("information"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.info.storage.tip"));
        putValue(MNEMONIC_KEY, (int) 'I');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();

        ObserveSwingDataSource source;
        if (ui.getConfig().isMainStorageOpened()) {
            source = applicationContext.getDataSourcesManager().getMainDataSource();
        } else {
            source = null;
        }

        ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
        String text = textGenerator.getDataSourceInfo(source);
        JOptionPane.showMessageDialog(
                ui,
                new JLabel(text),
                t("observe.title.storage.info"),
                JOptionPane.INFORMATION_MESSAGE);

    }

}
