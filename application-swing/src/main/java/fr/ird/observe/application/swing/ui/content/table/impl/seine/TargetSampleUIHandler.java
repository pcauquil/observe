/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TargetSampleDtos;
import fr.ird.observe.services.service.seine.TargetSampleService;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUI;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TargetSampleUIHandler extends ContentTableUIHandler<TargetSampleDto, TargetLengthDto> {

    /** Logger */
    private static final Log log = LogFactory.getLog(TargetSampleUIHandler.class);

    /**
     * Pour différencier positionner l'invariant de l'écran
     * {@link TargetSampleDto#getDiscarded()}.
     *
     * @since 1.5
     */
    protected final boolean discarded;

    /**
     * Ecoute les modifications de la propriété {@link TargetLengthDto#getWeight()},
     * et repasser alors le flag {@link TargetLengthDto#isWeightSource()} à
     * {@code false}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener weightChanged;

    /**
     * Ecoute les modifications de la propriété {@link TargetLengthDto#getLength()},
     * et repasser alors le flag {@link TargetLengthDto#isLengthSource()} à
     * {@code false}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener lengthChanged;

    public TargetSampleUIHandler(ContentTableUI<TargetSampleDto, TargetLengthDto> ui,
                                 boolean discarded) {
        super(ui, DataContextType.SetSeine);
        this.discarded = discarded;
        weightChanged = evt -> {
            TargetLengthDto source = (TargetLengthDto) evt.getSource();
            source.setWeightSource(false);
        };
        lengthChanged = evt -> {
            TargetLengthDto source = (TargetLengthDto) evt.getSource();
            source.setLengthSource(false);
        };
    }

    @Override
    public TargetSampleUI getUi() {
        return (TargetSampleUI) super.getUi();
    }

    public void resetWeightSource() {
        getTableEditBean().setWeightSource(false);
        getUi().getWeight().grabFocus();
    }

    public void resetLengthSource() {
        getTableEditBean().setLengthSource(false);
        getUi().getLength().grabFocus();
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    protected void onSelectedRowChanged(int editingRow, TargetLengthDto bean, boolean create) {

        ContentTableModel<TargetSampleDto, TargetLengthDto> model = getTableModel();
        TargetSampleUI ui = getUi();
        if (!model.isEditable()) {
            return;
        }

        ReferentialReference<SpeciesDto> species = bean.getSpecies();

        JComponent requestFocus;

        if (create) {

//            if (model.isCreate() && editingRow > 0) {
            if (model.isCreate()) {

                if (editingRow > 0) {
                    // on recupere l'species de la ligne precedente
                    TargetLengthDto editBean = model.getValueAt(editingRow - 1);
                    species = editBean.getSpecies();
                }

                // on passe le mode de saisie en count
                ui.getAcquisitionModeGroup().setSelectedValue(null);
                ui.getAcquisitionModeGroup().setSelectedValue(ModeSaisieEchantillonEnum.byEffectif);

                // on utilise par défaut le code mesure lf
                ui.getMeasureType().setSelectedItem(null);
                ui.getMeasureType().setSelectedItem(CodeMesureEnum.lf);
            }

            requestFocus = ui.getSpecies();

        } else {

            requestFocus = ui.getCount();

            // on passe le mode de saisie
            int acquisitionMode = bean.getAcquisitionMode();
            ModeSaisieEchantillonEnum enumValue =
                    ModeSaisieEchantillonEnum.valueOf(acquisitionMode);
            ui.getAcquisitionModeGroup().setSelectedValue(null);
            ui.getAcquisitionModeGroup().setSelectedValue(enumValue);
        }

        // on met a jour l'espce        
        ui.getSpecies().setSelectedItem(null);

        if (species != null) {
            if (log.isDebugEnabled()) {
                log.debug("species to use " + species);
            }
            ui.getSpecies().setSelectedItem(species);
        }
        requestFocus.requestFocus();

        TargetLengthDto tableEditBean = getTableEditBean();
        tableEditBean.removePropertyChangeListener(TargetLengthDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.addPropertyChangeListener(TargetLengthDto.PROPERTY_WEIGHT, weightChanged);

        tableEditBean.removePropertyChangeListener(TargetLengthDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.addPropertyChangeListener(TargetLengthDto.PROPERTY_LENGTH, lengthChanged);
    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.targetSample.table.speciesThon"),
                n("observe.content.targetSample.table.speciesThon.tip"),
                n("observe.content.targetSample.table.measureType"),
                n("observe.content.targetSample.table.measureType.tip"),
                n("observe.content.targetSample.table.length"),
                n("observe.content.targetSample.table.length.tip"),
                n("observe.content.targetSample.table.meanWeight"),
                n("observe.content.targetSample.table.meanWeight.tip"),
                n("observe.content.targetSample.table.count"),
                n("observe.content.targetSample.table.count.tip"),
                n("observe.content.targetSample.table.totalWeight"),
                n("observe.content.targetSample.table.totalWeight.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newEnumTableCellRenderer(renderer, CodeMesureEnum.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 5, UIHelper.newEmptyNumberTableCellRenderer(renderer));
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {
        ContentMode mode = super.getContentMode(dataContext);

        String setId = dataContext.getSelectedSetId();

        boolean showData = getTargetSampleService().canUseTargetSample(setId, discarded);

        getModel().setShowData(showData);

        if (mode == ContentMode.UPDATE && !showData) {

            mode = ContentMode.READ;

            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(SetSeineDto.class),
                       discarded ?
                       t("observe.content.setSeine.message.no.targetCatch") :
                       t("observe.content.setSeine.message.no.targetDiscarded")
            );
        }
        return mode;
    }

    /**
     * Le mode de saisie a été mis à jour.
     *
     * @param newMode le nouveau de mode de saisie à utiliser
     * @since 1.8
     */
    public void updateModeSaisie(ModeSaisieEchantillonEnum newMode) {

        if (log.isDebugEnabled()) {
            log.debug("Change mode saisie to " + newMode);
        }
        if (newMode == null) {

            // mode null (cela peut arriver avec les bindings)
            return;
        }

        TargetSampleUI ui = getUi();

        boolean createMode = ui.getTableModel().isCreate();

        TargetLengthDto editBean = ui.getTableEditBean();
        switch (newMode) {

            case byEffectif:

                // le weight n'est pas modifiable
                ui.getWeight().setEnabled(false);

                // l'count est modifiable
                ui.getCount().setEnabled(true);

                if (createMode) {

                    // on supprime le weight (si il a été saisie)
                    editBean.setWeight(null);
                    // on supprime aussi l'count (pour forcer la saisie)
                    editBean.setCount(null);
                }
                break;

            case byIndividu:

                // le weight est pas modifiable
                ui.getWeight().setEnabled(true);

                // l'count n'est pas modifiable et est toujours de 1
                ui.getCount().setEnabled(false);


                if (createMode) {

                    // on positionne l'count à 1 (seule valeur possible)
                    editBean.setCount(1);
                }
                break;
        }

        if (createMode) {

            // on propage le mode de saisie dans le bean
            editBean.setAcquisitionMode(newMode.ordinal());
        }
    }

    @Override
    protected void doPersist(TargetSampleDto bean) {

        SaveResultDto saveResult = getTargetSampleService().save(getSelectedParentId(), bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<TargetSampleDto> form = getTargetSampleService().loadForm(beanId, discarded);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        TargetSampleDtos.copyTargetSampleDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case TargetLengthDto.PROPERTY_SPECIES: {

                result = (List) getTargetSampleService().getSampleSpecies(getSelectedParentId(), discarded);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }

            }

            break;

        }

        return result;

    }

    protected TargetSampleService getTargetSampleService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTargetSampleService();
    }
}
