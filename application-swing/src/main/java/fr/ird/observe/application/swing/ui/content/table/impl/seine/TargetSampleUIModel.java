package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetLengthDtos;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;

import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class TargetSampleUIModel extends ContentTableUIModel<TargetSampleDto, TargetLengthDto> {

    private static final long serialVersionUID = 1L;

    public TargetSampleUIModel(TargetSampleUI ui) {

        super(TargetSampleDto.class,
              TargetLengthDto.class,
              new String[]{
                      TargetSampleDto.PROPERTY_TARGET_LENGTH,
                      TargetSampleDto.PROPERTY_COMMENT},
              new String[]{
                      TargetLengthDto.PROPERTY_SPECIES,
                      TargetLengthDto.PROPERTY_LENGTH,
                      TargetLengthDto.PROPERTY_LENGTH_SOURCE,
                      TargetLengthDto.PROPERTY_WEIGHT,
                      TargetLengthDto.PROPERTY_WEIGHT_SOURCE,
                      TargetLengthDto.PROPERTY_COUNT,
                      TargetLengthDto.PROPERTY_MEASURE_TYPE,
                      TargetLengthDto.PROPERTY_ACQUISITION_MODE});

        List<ContentTableMeta<TargetLengthDto>> metas = Lists.newArrayList(
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_SPECIES, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_MEASURE_TYPE, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_LENGTH, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_WEIGHT, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_COUNT, false),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_TOTAL_WEIGHT, true));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<TargetSampleDto, TargetLengthDto> createTableModel(
            ObserveContentTableUI<TargetSampleDto, TargetLengthDto> ui,
            List<ContentTableMeta<TargetLengthDto>> contentTableMetas) {
        return new ContentTableModel<TargetSampleDto, TargetLengthDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;
            @Override
            protected Collection<TargetLengthDto> getChilds(TargetSampleDto bean) {
                return bean.getTargetLength();
            }

            @Override
            protected void load(TargetLengthDto source, TargetLengthDto target) {
                TargetLengthDtos.copyTargetLengthDto(source, target);
            }

            @Override
            protected void setChilds(TargetSampleDto parent, List<TargetLengthDto> childs) {
                parent.setTargetLength(Sets.newLinkedHashSet(childs));
            }
        };
    }
}
