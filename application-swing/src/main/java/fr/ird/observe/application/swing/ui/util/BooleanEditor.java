/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.util;


import org.apache.commons.lang3.StringUtils;

import javax.swing.JComboBox;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Un éditeur de {@link Boolean} avec trois valeurs possibles :
 * <ul>
 * <li>{@code Null}</li>
 * <li>{@code false}</li>
 * <li>{@code true}</li>
 * </ul>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class BooleanEditor extends JComboBox {

    private static final long serialVersionUID = 1L;

    public enum Value {
        NULL(n("observe.common.boolean.null"), null),
        FALSE(n("observe.common.boolean.false"), false),
        TRUE(n("observe.common.boolean.true"), true);

        private final String defaultLibelle;

        private final Boolean booleanValue;

        Value(String defaultLibelle, Boolean booleanValue) {
            this.defaultLibelle = defaultLibelle;
            this.booleanValue = booleanValue;
        }

        public Boolean getBooleanValue() {
            return booleanValue;
        }

        public String getDefaultLibelle() {
            return defaultLibelle;
        }

        public static Value valueOf(Boolean value) {
            Value result = null;
            for (Value v : values()) {
                if (v.getBooleanValue() == value) {
                    result = v;
                    break;
                }
            }
            return result;
        }
    }

    protected static class ValueEntry {

        protected final Value value;

        protected final String text;

        ValueEntry(Value value, String text) {
            this.value = value;
            this.text = text;
        }

        public Value getValue() {
            return value;
        }

        public String getText() {
            return text;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ValueEntry that = (ValueEntry) o;

            if (!text.equals(that.text)) return false;
            return value == that.value;

        }

        @Override
        public int hashCode() {
            int result = value.hashCode();
            result = 31 * result + text.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    protected static ValueEntry[] buildModel(String nullLibelle,
                                             String falseLibelle,
                                             String trueLibelle) {
        ValueEntry[] result = new ValueEntry[3];

        String text;
        if (StringUtils.isBlank(nullLibelle)) {
            text = Value.NULL.getDefaultLibelle();
        } else {
            text = nullLibelle;
        }
        result[0] = new ValueEntry(Value.NULL, t(text));

        if (StringUtils.isBlank(falseLibelle)) {
            text = Value.FALSE.getDefaultLibelle();
        } else {
            text = falseLibelle;
        }
        result[1] = new ValueEntry(Value.FALSE, t(text));

        if (StringUtils.isBlank(trueLibelle)) {
            text = Value.TRUE.getDefaultLibelle();
        } else {
            text = trueLibelle;
        }
        result[2] = new ValueEntry(Value.TRUE, t(text));
        return result;
    }

    public BooleanEditor() {
        this(null, null, null);
    }

    public BooleanEditor(String nullLibelle,
                         String falseLibelle,
                         String trueLibelle) {
        super(buildModel(nullLibelle, falseLibelle, trueLibelle));
    }

    public void setBooleanValue(Boolean b) {
        Value v = Value.valueOf(b);
        ValueEntry e = (ValueEntry) dataModel.getElementAt(v.ordinal());
        setSelectedItem(e);
    }

    public Boolean getBooleanValue() {
        Object o = getSelectedItem();
        if (o == null) {
            return null;
        }
        Boolean result;
        ValueEntry v = (ValueEntry) o;
        result = v.getValue().getBooleanValue();
        return result;
    }
}
