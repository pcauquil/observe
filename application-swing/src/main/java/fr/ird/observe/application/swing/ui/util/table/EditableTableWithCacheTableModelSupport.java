package fr.ird.observe.application.swing.ui.util.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ird.observe.services.dto.IdDto;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public abstract class EditableTableWithCacheTableModelSupport<E extends IdDto> extends EditableTableModelSupport<E> {

    private static final long serialVersionUID = 1L;

    /**
     * Cache de liste de données par numéro de ligne. Cela sert à pouvoir conserver plusieurs listes
     * de données issues de ce table. Typiquement dans l'écran de capture ll ou on doit conserver pour une capture
     * (donc une ligne du tableau principal) n mesures de taille et n meausre de poids.
     */
    protected final Map<Integer, EditableList<E>> cacheByRow;

    protected EditableTableWithCacheTableModelSupport() {
        super(true);
        this.cacheByRow = new TreeMap<>();
    }

    public List<E> getCacheForRow(int rowIndex) {
        EditableList<E> measures = cacheByRow.get(rowIndex);
        return measures == null ? null : measures.getData();
    }

    public void initCacheForRow(int editingRow, List<E> data) {

        EditableList<E> editableList = cacheByRow.get(editingRow);
        Preconditions.checkState(editableList == null, "Cant have a list for row: " + editingRow);
        editableList = new EditableList<>();

        // Get a copy of the list (to avoid to edit the content of the list)
        List<E> original = copyList(data);
        editableList.setOriginal(original);
        editableList.setData(data);
        cacheByRow.put(editingRow, editableList);

    }

    public void storeInCacheForRow(int editingRow) {

        List<E> measures = getData();
        EditableList<E> editableList = cacheByRow.get(editingRow);
        Preconditions.checkState(editableList != null, "No list found for row: " + editingRow);
        editableList.setData(measures);

    }

    public void removeCacheForRow(int rowToDelete) {

        cacheByRow.remove(rowToDelete);

        List<Integer> rows = Lists.newArrayList(getCacheRowsChanged());
        Collections.sort(rows);

        for (Integer row : rows) {

            if (row > rowToDelete) {

                // set now to row -1
                EditableList<E> remove = cacheByRow.remove(row);
                cacheByRow.put(row - 1, remove);

            }
        }

    }

    public void resetCacheForRow(int rowtoReset) {

        EditableList<E> editableList = cacheByRow.get(rowtoReset);
        Preconditions.checkState(editableList != null);

        // remove data from cache
        editableList.reset();

        // restore a copy of original data
        List<E> newData = copyList(editableList.getOriginal());
        editableList.setData(newData);
        setData(newData);

        validate();

    }

    public Set<Integer> getCacheRowsChanged() {
        return cacheByRow.keySet();
    }

    @Override
    public void clear() {

        cacheByRow.clear();
        super.clear();

    }

    protected List<E> copyList(List<E> data) {
        List<E> copy = new ArrayList<>(data.size());
        for (E measure : data) {
            E originalMeasure = createNewRow();

            Class<E> sourceType = (Class<E>) originalMeasure.getClass();
            Binder<E, E> binder = BinderFactory.newBinder(sourceType);
            binder.copy(measure, originalMeasure);

            copy.add(originalMeasure);
        }
        return copy;
    }
}
