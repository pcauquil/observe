/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.open;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.validation.ValidationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public abstract class ContentOpenableUIHandler<E extends IdDto> extends ContentUIHandler<E> {

    protected static final String POSITION_OPENABLE = "positionOpenable";

    /** Logger */
    static private final Log log = LogFactory.getLog(ContentOpenableUIHandler.class);

    protected final String closeMessage;

    public ContentOpenableUIHandler(ContentOpenableUI<E> ui,
                                    DataContextType parentType,
                                    DataContextType type,
                                    String closeMessage) {
        super(ui, parentType, type);
        this.closeMessage = closeMessage;
    }

    protected abstract boolean obtainCanReopen(boolean create);

    @Override
    public ContentOpenableUI<E> getUi() {
        return (ContentOpenableUI<E>) super.getUi();
    }

    @Override
    public ContentOpenableUIModel<E> getModel() {
        return (ContentOpenableUIModel<E>) super.getModel();
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    protected abstract boolean doOpenData();

    public final void openDataUI() {
        boolean ok = false;
        try {
            ok = doOpenData();
        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {

            if (ok) {
                afterOpenData();
            }
        }
    }

    public final void afterOpenData() {

        ContentOpenableUI<E> ui = getUi();

        // On charge les ensembles de références utilisées dans les combobox
        updateUiWithReferenceSetsFromModel();

        // on repaint le parent (le program devient alors ouvert)
        ObserveTreeHelper treeHelper = getTreeHelper(ui);
        treeHelper.reloadSelectedNode(true, true);
//        ObserveNode node = treeHelper.getSelectedNode();
//        treeHelper.refreshNode(node.getParent(), true);

        // on chage l'état 'canReopen' du model
        ui.getModel().setCanReopen(false);

        // on lance l'édition
        ui.restartEdit();
    }

    public final void closeDataUI() {
        boolean ok = false;
        try {
            ok = doCloseData();
        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {

            if (ok) {
                afterCloseData();
            }
        }
    }

    public abstract boolean doCloseData();

    public final void afterCloseData() {

        ContentOpenableUI<E> ui = getUi();

        ContentOpenableUIModel<E> model = getModel();

        model.setCanReopen(true);
        E bean = getBean();

        ui.stopEdit();
        model.setMode(ContentMode.READ);

        removeAllMessages(ui);
        addMessage(ui, NuitonValidatorScope.INFO,
                   getTypeI18nKey(bean.getClass()), t(closeMessage));

        ObserveTreeHelper treeHelper = getTreeHelper(ui);
//        ObserveNode selectedNode = treeHelper.getSelectedNode();
//        if (bean instanceof Trip) {
//            // le program peut change d'etat open
//            selectedNode = selectedNode.getParent();
//        }
//        treeHelper.refreshNode(selectedNode, true);
        treeHelper.reloadSelectedNode(bean instanceof TripSeineDto || bean instanceof TripLonglineDto, true);
        updateActions();
    }

    @Override
    protected void afterSave(boolean refresh) {

        super.afterSave(refresh);

        ContentOpenableUI<E> ui = getUi();

        int position = ui.getContextValue(Integer.class, POSITION_OPENABLE);
        ObserveTreeHelper treeHelper = getTreeHelper(ui);

        ObserveNode node = treeHelper.getSelectedNode();
        ObserveNode parentNode = node.getParent();
        boolean create = node.getId() == null;

        E bean = getBean();

        int oldPosition;
        if (create) {
            oldPosition = parentNode.getChildCount();
        } else {
            oldPosition = parentNode.getIndex(node);
        }

        if (create) {

            // on passe en mode mise à jour
            getModel().setMode(ContentMode.UPDATE);

            // on supprime le noeud temporaire
            treeHelper.removeNode(node);

            // on crée le noeud final de la marée
            node = treeHelper.addOpenable(parentNode, bean);

            // arrêt de l'édition de l'écran courant
            stopEditUI();

            if (oldPosition != position) {

                // on doit repositionner le noeud
                treeHelper.moveNode(parentNode, node, position);
            }

            // on sélectionne le nouveau noeud
            treeHelper.selectNode(node);
        } else {
            if (oldPosition != position) {

                // on doit repositionner le noeud
                treeHelper.moveNode(parentNode, node, position);

                // et le selectionner
                treeHelper.selectNode(node);
            }

            // on repaint le noeud et ses enfants
//            treeHelper.refreshNode(node, true);
            treeHelper.reloadSelectedNode(false, true);

//            treeHelper.refreshNode(node, false);
        }
    }

    /**
     * Clôturer la marée ouverte puis en créer une nouvelle.
     *
     * @since 1.5
     */
    public final void closeAndCreateDataUI() {

        // fermeture de la donnée
        try {
            boolean b = doCloseData();
            if (!b) {

                // la fermeture a ete abandonnee
                if (log.isInfoEnabled()) {
                    log.info(prefix + "Stop closing data...");
                }
                return;
            }
        } catch (Exception eee) {
            UIHelper.handlingError("Could not close current data", eee);
        }

        stopEditUI();

        // création d'une nouvelle donnée

        ObserveTreeHelper treeHelper = getTreeHelper(getUi());

        treeHelper.reloadSelectedNode(false, true);

        ObserveNode parentNode = treeHelper.getSelectedNode().getParent();
        if (log.isDebugEnabled()) {
            log.debug("PARENT NODE = " + parentNode);
        }
        treeHelper.addUnsavedNode(parentNode, getBeanType());
    }

    protected final void obtainChildPosition(E bean) {

        String containerId = getSelectedParentId();

        int position = getOpenablePosition(containerId, bean);

        if (log.isDebugEnabled()) {
            log.debug("Position of child : " + position);
        }

        getUi().setContextValue(position, POSITION_OPENABLE);
    }

    protected abstract int getOpenablePosition(String parentId, E bean);

    protected final void finalizeOpenUI(ContentMode mode, boolean create) {

//        // utilisation du mode requis
//        setContentMode(mode);

        boolean historicalData = false;

        ContentOpenableUIModel<E> model = getUi().getModel();

        if (!create) {

            ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
            ValidationContext context = applicationContext.getValidationContext();
            TripSeineDto currentTripSeine = context.getCurrentTripSeine();

            if (currentTripSeine != null && currentTripSeine.isHistoricalData()) {

                if (log.isInfoEnabled()) {
                    log.info(prefix + "Using a historical fish trip " + currentTripSeine.getId());
                }
                historicalData = true;
            }

        }

        model.setHistoricalData(historicalData);

        boolean canReopen = obtainCanReopen(create);

        if (log.isInfoEnabled()) {
            log.info(prefix + "historical data ? = " + historicalData);
            log.info(prefix + "can reopen      ? = " + canReopen);
        }
        model.setCanReopen(canReopen);

        if (mode != ContentMode.READ) {
            getUi().startEdit(null);
        }
    }

    private static final String UPDATE_TRIP_NODE = "updateTripNode";

    protected void repaintTripNode() {

        Boolean updateTripNode = getUi().getContextValue(Boolean.class, UPDATE_TRIP_NODE);

        getUi().removeContextValue(Boolean.class, UPDATE_TRIP_NODE);

        if (updateTripNode == null || !updateTripNode) {
            return;
        }

        ObserveTreeHelper treeHelper = getTreeHelper(getUi());
        ObserveNode tripNode = treeHelper.getSelectedNode().getParent().getParent();
        if (log.isInfoEnabled()) {
            log.info("Refresh trip node : " + tripNode);
        }
        treeHelper.reloadNode(tripNode, false);

    }

    protected void setUpdateMareeNodeTag(boolean wasUpdated) {

        if (wasUpdated) {

            // la date de fin a ete modifiee, il faut : redessiner le noeud de la maree le repositionner
            getUi().setContextValue(Boolean.TRUE, UPDATE_TRIP_NODE);

        } else {

            getUi().removeContextValue(Boolean.class, UPDATE_TRIP_NODE);

        }

    }

}