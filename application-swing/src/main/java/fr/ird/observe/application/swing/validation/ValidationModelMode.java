/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.validation;

/**
 * Pour caractériser le type de modele de validation a utiliser.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public enum ValidationModelMode {

    /** validation du referentiel */
    REFERENTIEL(true, false),

    /** validation de donnees observer */
    DATA(false, true),

    /** validation du referentiel et de donnees observer */
    ALL(true, false);

    private final boolean referential;

    private final boolean data;

    ValidationModelMode(boolean referential,
                        boolean data) {
        this.referential = referential;
        this.data = data;
    }

    public boolean isReferential() {
        return referential;
    }

    public boolean isData() {
        return data;
    }
}
