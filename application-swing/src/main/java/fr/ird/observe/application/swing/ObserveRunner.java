/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing;

import com.google.common.base.Preconditions;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationActionDefinition;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveMainUIHandler;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.StartServerModeAction;
import fr.ird.observe.application.swing.ui.util.FloatConverter;
import fr.ird.observe.util.ObserveUtil;
import jaxx.runtime.swing.application.ApplicationRunner;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.jdesktop.swingx.plaf.basic.CalendarHeaderHandler;
import org.jdesktop.swingx.plaf.basic.SpinningCalendarHeaderHandler;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.converter.ConverterUtil;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.i18n.init.UserI18nInitializer;
import org.nuiton.util.StringUtil;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.UIManager;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.BACKUP_DIRECTORY;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.DATA_DIRECTORY;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.DB_DIRECTORY;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.INITIAL_DB_DUMP;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.MAP_DIRECTORY;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.REPORT_DIRECTORY;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.RESOURCES_DIRECTORY;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.TMP_DIRECTORY;
import static fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption.VALIDATION_REPORT_DIRECTORY;
import static org.nuiton.i18n.I18n.t;

/**
 * Le lanceur de l'application ObServe.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class ObserveRunner extends ApplicationRunner {

    /** Logger */
    private static Log log = LogFactory.getLog(ObserveRunner.class);

    private static ObserveResourceManager resourceManager;

    private static ObserveActionExecutor actionExecutor;

    public static ObserveRunner getRunner() {
        return (ObserveRunner) ApplicationRunner.getRunner();
    }

    public static ObserveActionExecutor getActionExecutor() {
        if (actionExecutor == null) {
            actionExecutor = new ObserveActionExecutor();
        }
        return actionExecutor;
    }

    public abstract String getRunnerName();

    private final boolean init;

    public ObserveRunner(String... args) {
        super(args);
        init = true;
        initOnce();
    }

    private static void runAction(String actionLabel, Runnable action) {
        getActionExecutor().addAction(actionLabel, action);
    }

    private static void runAction(String actionLabel,
                                  Object invoker,
                                  String methodName,
                                  Object... arguments) {
        Runnable action;
        try {
            action = getActionExecutor().createRunnable(
                    invoker,
                    methodName,
                    arguments
            );
        } catch (Exception e) {
            throw new RuntimeException(
                    "could not create action " + actionLabel, e);
        }
        runAction(actionLabel, action);
    }

    @Override
    protected void initOnce() {
        if (!init) {
            return;
        }

        // check script engine exist
        checkScriptEngineFound();

        // on veut avoir les traductions dès le début
        // on charge dans un premier temps les traductions fournies
        // par l'application
        I18n.init(new DefaultI18nInitializer("observe-i18n"), null);

        // to enable javassist on webstart, must remove any securityManager,
        // see if this can be dangerous (should not be since jnlp is signed ?)
        // moreover it speeds up the loading :)
//        System.setSecurityManager(null);

        resourceManager = new ObserveResourceManager();

        // initialisation des converteurs

        Converter converter = ConverterUtil.getConverter(Date.class);
        if (converter != null) {
            ConvertUtils.deregister(Date.class);
        }
        DateConverter dateConverter = new DateConverter();
        dateConverter.setUseLocaleFormat(true);
        ConvertUtils.register(dateConverter, Date.class);

        converter = ConverterUtil.getConverter(Float.class);
        if (converter != null) {
            ConvertUtils.deregister(Float.class);
        }
        ConvertUtils.register(new FloatConverter(), Float.class);

        // initialisation du thread d'action
        getActionExecutor();

        UIManager.put(CalendarHeaderHandler.uiControllerID, SpinningCalendarHeaderHandler.class.getName());
        UIManager.put(SpinningCalendarHeaderHandler.ARROWS_SURROUND_MONTH, true);
        UIManager.put(SpinningCalendarHeaderHandler.FOCUSABLE_SPINNER_TEXT, true);
    }

    @Override
    protected void onInit() throws Exception {

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.init", new Date(),
                       Arrays.toString(args)));
        }

        long t0 = System.nanoTime();

        // 1 - preparation de la configuration

        ObserveSwingApplicationConfig config = initConfig();

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.config.loaded", config.getVersion()));
        }

        // 2 - preparation des répertoires utilisateurs

        initUserDirectories(config);

        // 3 - Chargement de la configuration des logs utilisateur
        if (!config.isDevMode()) {
            initLog(config);
        }

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.user.directories.loaded", config.getDataDirectory()));
        }

        // 3 - preparation i18n

        initI18n(config);

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.i18n.loaded", config.getLocale().getDisplayLanguage()));
        }

        // 4 - preparation de la configuration des ui

        initUIConfiguration(config);

        // 5 - preparation du context applicatif

        new ObserveSwingApplicationContext(config);

        // 6 - détection de la base locale

        detectLocalDataBase(config);

        String time = StringUtil.convertTime(t0, System.nanoTime());

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.context.loaded", time));
        }
    }

    private void initLog(ObserveSwingApplicationConfig config) throws IOException {

        File logFile = config.getLogConfigurationFile();
        Preconditions.checkState(logFile.exists(), "Le fichier de configuration des logs %s n'existe pas.", logFile);

        URL resource;
        try {
            resource = logFile.toURI().toURL();
        } catch (MalformedURLException mue) {
            throw new ObserveSwingTechnicalException("Unable to load log configuration", mue);
        }

        if (log.isInfoEnabled()) {
            log.info("Chargement du fichier de de log4j depuis " + resource);
        }
        Properties logConfigurationProperties = resourceManager.load(resource);
        Properties finalLogConfigurationProperties = ObserveUtil.loadProperties(logConfigurationProperties, config);

        LogManager.resetConfiguration();
        PropertyConfigurator.configure(finalLogConfigurationProperties);

        log = LogFactory.getLog(ObserveRunner.class);
        if (log.isInfoEnabled()) {
            log.info("Initialisation des logs depuis " + logFile);
        }

    }

    @Override
    protected void onStart() throws Exception {

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.start", new Date(), Arrays.toString(args)));
        }

        ObserveSwingApplicationContext context = ObserveSwingApplicationContext.get();

        if (log.isDebugEnabled()) {
            log.debug("Will use context : " + context);
        }

        // 1 - launch commandline actions

        ObserveSwingApplicationConfig config = context.getConfig();

        if (config.containActions(ObserveSwingApplicationConfig.Step.AfterInit)) {

            config.doAction(ObserveSwingApplicationConfig.Step.AfterInit.ordinal());

            if (log.isInfoEnabled()) {
                log.info("Operation terminées...");
            }
        }

        if (!config.isDisplayMainUI()) {
            if (log.isInfoEnabled()) {
                log.info(t("observe.runner.quit.withno.ui"));
            }

            unlock();
            return;
        }

        // 2 - init ui

        ObserveMainUI ui = startUI(context, config);

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.ui.loaded"));
        }

        Boolean h2ServerMode = ObserveSwingApplicationContext.Entries.H2_SERVER_MODE.get();

        if (BooleanUtils.isTrue(h2ServerMode)) {

            // starts in h2 server mode

            ObserveSwingApplicationContext.get().removeH2ServerMode();

            new StartServerModeAction(ui).run();

            // on ne charge rien au démarrage
            ui.getStatus().setStatus(t("observe.runner.loaded", config.getVersion()));

            return;
        }
        if (!config.isLoadLocalStorage()) {

            // on ne charge rien au démarrage
            ui.getStatus().setStatus(t("observe.runner.loaded", config.getVersion()));
            return;
        }

        // 3 - init storage

        runAction(t("observe.runner.load.database"), context.getDataSourcesManager(), "initStorage", config, ui, true);

    }

    @Override
    protected void onClose(boolean reload) throws Exception {
        if (ObserveSwingApplicationContext.isInit()) {

            if (log.isDebugEnabled()) {
                log.debug("Will close context...");
            }
            ObserveSwingApplicationContext.get().close();
        }
    }

    @Override
    protected void onShutdown() throws Exception {
        if (log.isInfoEnabled()) {
            log.info("ObServe shutdown at " + new Date());
        }

        try {

            // on éteint le context applicatif
            onClose(false);

            // on ferme le service de traduction uniquement si on quitte
            // definitivement l'application
            I18n.close();

            getActionExecutor().terminatesAndWaits();

        } finally {

            Runtime.getRuntime().halt(0);

        }


    }

    @Override
    protected void onShutdown(Exception ex) {
        if (log.isErrorEnabled()) {
            log.error("error while closing " + ex.getMessage(), ex);
        }
        Runtime.getRuntime().halt(1);
    }

    @Override
    protected void onError(Exception e) {
        UIHelper.handlingError(e);
    }

    private ObserveSwingApplicationConfig initConfig() throws Exception {

        ObserveSwingApplicationConfig config = new ObserveSwingApplicationConfig();

        // init config (load application configuration)
        config.initConfig(
                resourceManager.getResource(ObserveResourceManager.Resource.application),
                (ConfigActionDef[]) ObserveSwingApplicationActionDefinition.values());

        // init config arguments
        config.parse(args);

        // install save action on option modification
        config.installSaveAction();

        if (log.isInfoEnabled()) {
            String message = config.getConfigurationDescription();
            log.info(message);
        }

        return config;
    }

    private void initUserDirectories(ObserveSwingApplicationConfig config) throws IOException {

        // 1 - user data directory

        File dataDirectory = resourceManager.createDirectory(config, DATA_DIRECTORY);

        if (log.isDebugEnabled()) {
            log.debug("user data directory : " + dataDirectory);
        }

        resourceManager.createParentDirectory(config, DB_DIRECTORY, INITIAL_DB_DUMP);

        // 2 - tmp directory

        resourceManager.createDirectory(config, TMP_DIRECTORY);

        // suppression du contenu du répertoire temporaire
        FileUtils.cleanDirectory(config.getTmpDirectory());

        // 3 - backup directory

        resourceManager.createDirectory(config, BACKUP_DIRECTORY);

        // 4 - resources directory

        File resourcesDirectory = resourceManager.createDirectory(config, RESOURCES_DIRECTORY);

        if (log.isDebugEnabled()) {
            log.debug("user resource data directory : " + resourcesDirectory);
        }

        // 5 - application ui

        File file = ObserveResourceManager.Resource.ui.getFile(resourcesDirectory);

        if (!file.exists()) {

            String message = t("observe.runner.copy.default.ui.file", file);

            resourceManager.copyResource(ObserveResourceManager.Resource.ui, file, message);
        }

        // 6 - resources log configuration file

        file = ObserveResourceManager.Resource.LOG_CONFIGURATION_FILE.getFile(resourcesDirectory);

        if (!file.exists()) {

            String message = t("observe.runner.copy.default.logConfigurationFile.file", file);

            resourceManager.copyResource(ObserveResourceManager.Resource.LOG_CONFIGURATION_FILE, file, message);

        }

        // 7 - resources report

        File reportDirectory = resourceManager.createDirectory(config, REPORT_DIRECTORY);

        file = ObserveResourceManager.Resource.report.getFile(reportDirectory);

        if (!file.exists()) {

            String message = t("observe.runner.copy.default.report.file", file);

            resourceManager.copyResource(ObserveResourceManager.Resource.report, file, message);

        }

        // 8 - validation report directory

        resourceManager.createDirectory(config, VALIDATION_REPORT_DIRECTORY);

        // 9 - resources shapeFiles

        File mapdirectory = resourceManager.createDirectory(config, MAP_DIRECTORY);

        String message = t("observe.runner.copy.default.map.file", mapdirectory);

        resourceManager.unzipToDirectory(ObserveResourceManager.Resource.mapLayers, config, RESOURCES_DIRECTORY, message);

    }

    private void detectLocalDataBase(ObserveSwingApplicationConfig config) {
        boolean hasLocalStorage = new File(config.getLocalDBDirectory(), ObserveSwingApplicationConfig.DB_NAME).exists();
        config.setLocalStorageExist(hasLocalStorage);
        if (!hasLocalStorage) {
            if (log.isInfoEnabled()) {
                log.info(t("observe.init.no.local.db.detected", config.getLocalDBDirectory()));
            }
        }

        boolean hasInitialDb = config.getInitialDbDump().exists();
        config.setInitialDumpExist(hasInitialDb);

        if (!hasInitialDb) {
            if (log.isInfoEnabled()) {
                log.info(t("observe.init.no.initial.dump.detected", config.getInitialDbDump()));
            }
        }
    }

    private void initI18n(ObserveSwingApplicationConfig config) {

        I18n.close();

        File i18nDirectory = config.getI18nDirectory();

        UserI18nInitializer i18nInitializer = new UserI18nInitializer(i18nDirectory, new DefaultI18nInitializer("observe-i18n")) {

            @Override
            protected void createUserI18nLayout(File directory) throws Exception {
                super.createUserI18nLayout(directory);

                // add also the i18n csv bundle

                URL resource = ObserveResourceManager.getResource("/META-INF/observe-i18n.csv");
                resourceManager.copyResource(resource, new File(directory, "observe-i18n.csv"), "Copy i18n csv bundle");
            }
        };

        long t00 = System.nanoTime();

        Locale locale = config.getLocale();

        I18n.init(i18nInitializer, locale);

        if (log.isDebugEnabled()) {
            log.debug("i18n language : " + locale);
            log.debug("i18n loading time : " + StringUtil.convertTime(t00, System.nanoTime()));
        }
    }

    private void initUIConfiguration(ObserveSwingApplicationConfig config) {

        // prepare ui look&feel and load ui properties
        try {
            UIHelper.initNimbusLoookAndFeel();
        } catch (Exception e) {
            // could not find nimbus look-and-feel
            if (log.isWarnEnabled()) {
                log.warn(t("observe.warning.nimbus.landf"));
            }
        } catch (Throwable e) {
            if (log.isWarnEnabled()) {
                log.warn(t("observe.warning.no.ui"));
            }
            // pas d'environnement d'ui
            config.setCanUseUI(false);
        }

        if (config.isCanUseUI()) {

            // chargement de la configuration des uis
            loadUIConfig(config);

        }
    }

    public void loadUIConfig(ObserveSwingApplicationConfig config) {

        // chargement de la configuration des uis
        File dir = config.getResourcesDirectory();
        File file = ObserveResourceManager.Resource.ui.getFile(dir);


        if (!file.exists()) {

            String message = t("observe.runner.copy.default.ui.file", file);

            try {
                resourceManager.copyResource(ObserveResourceManager.Resource.ui, file, message);
            } catch (IOException e) {
                throw new ObserveSwingTechnicalException("could not copy ui configuration: " + file, e);
            }
        }

        if (log.isInfoEnabled()) {
            log.info(t("observe.runner.loading.ui.configuration", file));
        }
        try {
            Properties p = resourceManager.getResource(file);
            UIHelper.loadUIConfig(p);
        } catch (IOException e) {

            throw new ObserveSwingTechnicalException("could not load ui configuration: " + file, e);

        }

    }

    private ObserveMainUI startUI(ObserveSwingApplicationContext context, ObserveSwingApplicationConfig config) {

        ObserveMainUIHandler uiHandler = context.getContextValue(ObserveMainUIHandler.class);

        ObserveMainUI ui = uiHandler.initUI(context, config);

        UIHelper.setMainUIVisible(ui);
        return ui;
    }

    /**
     * Method pour vérifier que le moteur de scripting est bien présent.
     *
     * Voir http://forge.codelutin.com/issues/829
     *
     * @since 2.5
     */
    private void checkScriptEngineFound() {

        ScriptEngineManager factory = new ScriptEngineManager();

        ScriptEngine scriptEngine = factory.getEngineByExtension("js");

        if (scriptEngine == null) {

            // script engine pas trouvé.
            String message = "Could not found script engine, use a Oracle jvm";
            if (Locale.FRANCE.getCountry().equals(Locale.getDefault().getCountry())) {
                message = "Moteur de scripting non trouvé, Utiliser une jvm fournie par Oracle.";
            }
            onError(new ScriptException(message));
        }
    }

}
