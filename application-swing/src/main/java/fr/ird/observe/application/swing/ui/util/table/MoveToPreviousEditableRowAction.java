package fr.ird.observe.application.swing.ui.util.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import java.awt.event.ActionEvent;

/**
 * Action to select previous editable row in a table.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class MoveToPreviousEditableRowAction<M extends EditableTableModelSupport> extends AbstractSelectTableAction<M> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(MoveToPreviousEditableRowAction.class);

    protected MoveToPreviousEditableRowAction(M model, JTable table) {
        super(model, table);
    }

    public static <M extends EditableTableModelSupport> MoveToPreviousEditableRowAction<M> newAction(M model, JTable table) {
        return new MoveToPreviousEditableRowAction<>(model, table);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        int currentRow = getSelectedRow();
        int currentColumn = getSelectedColumn();

        if (log.isDebugEnabled()) {
            log.debug("Move to previous row editable cell " +
                      getCellCoordinate(currentRow, currentColumn));
        }

        if (currentRow > 0) {

            // go to next cell
            currentRow--;
            doSelectCell(currentRow, currentColumn);
        }
    }
}
