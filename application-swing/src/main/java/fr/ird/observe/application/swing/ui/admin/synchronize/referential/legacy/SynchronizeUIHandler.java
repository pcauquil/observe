/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy;

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffsEngine;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeCallbackRequest;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeCallbackRequests;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeCallbackResults;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeContext;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeEngine;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeLocalService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeResult;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import jaxx.runtime.swing.CardLayout2;
import jaxx.runtime.swing.editor.bean.BeanComboBox;
import jaxx.runtime.swing.model.JaxxDefaultListModel;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class SynchronizeUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(SynchronizeUIHandler.class);

    public SynchronizeUIHandler(SynchronizeUI ui) {
        super(ui);
    }

    public SynchronizeModel getStepModel() {
        return model.getSynchronizeReferentielModel();
    }

    @Override
    public SynchronizeUI getUi() {
        return (SynchronizeUI) super.getUi();
    }

    public void initTabUI(AdminUI ui, SynchronizeUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        tabUI.getStartAction().setText(t("observe.actions.synchro.referential.legacy.launch.operation", t(tabUI.getStep().getOperationLabel())));

        DefaultListSelectionModel obsoleteReferenceSelectionModel = tabUI.getModel().getSynchronizeReferentielModel().getObsoleteReferencesSelectionModel();
        obsoleteReferenceSelectionModel.addListSelectionListener(this::updateSelectedObsoleteEntity);
        obsoleteReferenceSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    }

    public void doStartAction() {

        addAdminWorker(getUi().getStartAction().getToolTipText(), this::doStartAction0);

    }

    private WizardState doStartAction0() throws Exception {

        if (log.isDebugEnabled()) {
            log.debug(this);
        }

        SynchronizeModel stepModel = getStepModel();

        // on cree les sources de données

        ObserveSwingDataSource source = model.getSafeLocalSource(false);
        stepModel.setSource(source);

        ObserveSwingDataSource centralSource = model.getSafeCentralSource(false);
        stepModel.setCentralSource(centralSource);

        openSource(source);
        openSource(centralSource);

        // construction du différentiel
        sendMessage(t("observe.actions.synchro.referential.message.build.diff", centralSource.getLabel()));

        ReferentialSynchronizeDiffsEngine diffsEngine = new ReferentialSynchronizeDiffsEngine(
                source.newReferentialSynchronizeDiffService(),
                centralSource.newReferentialSynchronizeDiffService()
        );

        UnidirectionalReferentialSynchronizeLocalService localService = source.newUnidirectionalReferentialSynchronizeLocalService();
        UnidirectionalReferentialSynchronizeEngine engine = new UnidirectionalReferentialSynchronizeEngine(
                diffsEngine
        );

        stepModel.setEngine(engine);

        UnidirectionalReferentialSynchronizeContext unidirectionalReferentialSynchronizeContext = engine.prepareContext(localService);

        stepModel.setReferentialSynchronizeContext(unidirectionalReferentialSynchronizeContext);

        boolean needCallback = unidirectionalReferentialSynchronizeContext.isNeedCallback();

        if (needCallback) {

            // il existe des références obsolètes à traiter

            sendMessage(t("observe.actions.operation.message.needFix"));

            stepModel.setReferentialSynchronizeCallbackResults(new UnidirectionalReferentialSynchronizeCallbackResults());

            // des références obsolètes ont été détectées, on prépare les interfaces graphiques
            // avec les données à corriger

            List<ObsoleteReferentialReference> obsoleteReferentialReferences = new LinkedList<>();
            UnidirectionalReferentialSynchronizeCallbackRequests callbackRequests = stepModel.getReferentialSynchronizeContext().getCallbackRequests();

            for (UnidirectionalReferentialSynchronizeCallbackRequest<?> callbackRequest : callbackRequests) {

                Class referentialName = callbackRequest.getReferentialName();

                obsoleteReferentialReferences.addAll(
                        callbackRequest.getReferentialsToReplace().stream()
                                       .map(referentialReference -> new ObsoleteReferentialReference(referentialName, referentialReference))
                                       .collect(Collectors.toList()));

            }

            stepModel.setObsoleteReferences(obsoleteReferentialReferences);

            return WizardState.NEED_FIX;

        }

        // pas de reference obsolete à traiter

        // on termine le traitement
        beforeSuccess();

        return WizardState.SUCCESSED;

    }

    public <R extends ReferentialDto> void resolveObsoleteReference() {

        ObsoleteReferentialReference<R> obsoleteRef = (ObsoleteReferentialReference) getUi().getObsoleteReferencesList().getSelectedValue();

        BeanComboBox<ReferentialReference<R>> safeComboBox = (BeanComboBox<ReferentialReference<R>>) getSafeComboBox();
        ReferentialReference<R> safeRef = (ReferentialReference<R>) safeComboBox.getSelectedItem();

        UnidirectionalReferentialSynchronizeCallbackResults referentialSynchronizeCallbackResults = getStepModel().getReferentialSynchronizeCallbackResults();
        referentialSynchronizeCallbackResults.addCallbackResult(obsoleteRef.getType(), obsoleteRef.getId(), safeRef.getId());

        // On supprime le référentiel corrigé de la liste des référentiels à corriger

        getStepModel().getObsoleteReferences().removeElement(obsoleteRef);

        // on supprime la sélection de la liste des remplacements

        safeComboBox.setSelectedItem(null);

        // S'il ne reste plus de référentiel à corriger, on peut terminer le traitement

        if (getStepModel().getObsoleteReferences().isEmpty()) {

            beforeSuccess();

            getModel().setStepState(WizardState.SUCCESSED);
//            updateState(getUi(), WizardState.SUCCESSED);

        }

    }

    private void updateSelectedObsoleteEntity(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            return;
        }

        SynchronizeUI ui = getUi();

        int row = getStepModel().getObsoleteReferencesSelectionModel().getMinSelectionIndex();

        JaxxDefaultListModel<ObsoleteReferentialReference> obsoleteReferences = getStepModel().getObsoleteReferences();

        if (row != -1 && obsoleteReferences.getElementAt(row) != null) {

            ObsoleteReferentialReference referentialReference = obsoleteReferences.getElementAt(row);

            String key = referentialReference.getReferentialName().getName();
            CardLayout2 safeRefsPanelLayout = ui.getSafeRefsPanelLayout();
            JPanel safeRefsPanel = ui.getSafeRefsPanel();
            if (safeRefsPanelLayout.contains(key)) {

                // la liste déroulante existe deja pour ce type
                BeanComboBox<?> list = (BeanComboBox<?>) safeRefsPanelLayout.getComponent(safeRefsPanel, key);

                if (!getSafeComboBox().equals(list)) {

                    // on l'affiche
                    safeRefsPanelLayout.show(safeRefsPanel, key);

                }

            } else {

                ReferentialReferenceDecorator decorator = getDecoratorService().getReferentialReferenceDecorator(referentialReference.getType());

                UnidirectionalReferentialSynchronizeCallbackRequest<?> callbackRequest = getStepModel().getReferentialSynchronizeContext().getCallbackRequests().getCallbackRequest(referentialReference.getReferentialName());
                ImmutableSet<? extends ReferentialReference<?>> availableReferentials = callbackRequest.getAvailableReferentials();

                List<ReferentialReference> data = new ArrayList<>(availableReferentials);

                // la liste n'existe pas encore
                BeanComboBox<ReferentialReference> box = new BeanComboBox<>(ui);
                box.setBean(this);
                box.setProperty("safeEntity");
                box.setShowReset(true);
                box.addPropertyChangeListener("selectedItem", evt -> updateCanApply());
                safeRefsPanel.add(box, key);
                box.init(decorator, data);

                safeRefsPanelLayout.show(safeRefsPanel, key);

            }

        }
    }

    private BeanComboBox<?> getSafeComboBox() {
        SynchronizeUI ui = getUi();
        JPanel panel = ui.getSafeRefsPanel();
        return (BeanComboBox<?>) ui.getSafeRefsPanelLayout().getVisibleComponent(panel);
    }

    private void updateCanApply() {
        SynchronizeUI ui = getUi();
        BeanComboBox<?> safeComboBox = getSafeComboBox();
        ui.setCanApply(safeComboBox != null && safeComboBox.getSelectedItem() != null);
    }

    private void beforeSuccess() {

        SynchronizeModel stepModel = getStepModel();

        UnidirectionalReferentialSynchronizeEngine engine = stepModel.getEngine();

        UnidirectionalReferentialSynchronizeContext referentialSynchronizeContext = stepModel.getReferentialSynchronizeContext();

        UnidirectionalReferentialSynchronizeCallbackResults referentialSynchronizeCallbackResults = stepModel.getReferentialSynchronizeCallbackResults();

        UnidirectionalReferentialSynchronizeLocalService localService = stepModel.getSource().newUnidirectionalReferentialSynchronizeLocalService();
        UnidirectionalReferentialSynchronizeResult referentialSynchronizeResult = engine.prepareResult(localService, referentialSynchronizeContext, referentialSynchronizeCallbackResults);
        stepModel.setReferentialSynchronizeResult(referentialSynchronizeResult);

        if (referentialSynchronizeResult.isEmpty()) {

            sendMessage(t("observe.actions.synchro.referential.message.ref.is.updtodate"));

        } else {

            for (Class<? extends ReferentialDto> referentialName : referentialSynchronizeResult.getReferentialNames()) {

                String referentialStr = t(ObserveI18nDecoratorHelper.getTypeI18nKey(referentialName));

                Collection<String> referentialAdded = referentialSynchronizeResult.getReferentialAdded(referentialName);
                if (CollectionUtils.isNotEmpty(referentialAdded)) {

                    sendMessage(t("observe.actions.synchro.referential.message.referentiel.was.added", referentialStr, referentialAdded.size()));
                    for (String id : referentialAdded) {
                        sendMessage("  - " + id);
                    }

                }

                Collection<String> referentialUpdated = referentialSynchronizeResult.getReferentialUpdated(referentialName);
                if (CollectionUtils.isNotEmpty(referentialUpdated)) {

                    sendMessage(t("observe.actions.synchro.referential.message.referentiel.was.modified", referentialStr, referentialUpdated.size()));
                    for (String id : referentialUpdated) {
                        sendMessage("  - " + id);
                    }

                }

                Collection<String> referentialRemoved = referentialSynchronizeResult.getReferentialRemoved(referentialName);
                if (CollectionUtils.isNotEmpty(referentialRemoved)) {

                    sendMessage(t("observe.actions.synchro.referential.message.referentiel.was.removed", referentialStr, referentialRemoved.size()));
                    for (String id : referentialRemoved) {
                        sendMessage("  - " + id);
                    }

                }

                Collection<Pair<String, String>> referentialReplaced = referentialSynchronizeResult.getReferentialReplaced(referentialName);
                if (CollectionUtils.isNotEmpty(referentialReplaced)) {

                    sendMessage(t("observe.actions.synchro.referential.message.referentiel.was.replaced", referentialStr, referentialReplaced.size()));
                    for (Pair<String, String> ids : referentialReplaced) {
                        sendMessage("  - " + ids.getLeft() + " → " + ids.getRight());
                    }

                }

            }

            getModel().getSaveLocalModel().addStepForSave(AdminStep.SYNCHRONIZE);

        }

        sendMessage(t("observe.actions.operation.message.done", new Date()));

    }

}
