package fr.ird.observe.application.swing.ui.util.table;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.util.function.Predicate;

/**
 * TODO A remonter dans JAXX.
 * 
 * Created on 14/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0.1.1
 */
public class ObserveBooleanTableCellRenderer extends JCheckBox implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    protected final TableCellRenderer defaultDelegate;

    private final Predicate predicate;

    private static final Predicate<?> DEFAULT_PREDICATE = input -> (input != null && (Boolean) input);

    public ObserveBooleanTableCellRenderer(TableCellRenderer delegate, Predicate<?> predicate) {
        this(delegate, null, predicate);
    }

    public ObserveBooleanTableCellRenderer(TableCellRenderer delegate) {
        //super(new BorderLayout());
        this(delegate, DEFAULT_PREDICATE);

    }

    public ObserveBooleanTableCellRenderer(TableCellRenderer delegate, Icon icon, Predicate<?> predicate) {
        this.defaultDelegate = delegate;
        this.predicate = predicate;
        setOpaque(true);
        setBorderPainted(true);
        setHorizontalAlignment(JLabel.CENTER);
        setIcon(icon);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        ((JComponent) defaultDelegate).setBackground(null);
        JComponent render = (JComponent) defaultDelegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(render.getForeground());
            setBackground(render.getBackground());
            if (row % 2 == 0) {
                setBackground(Color.WHITE);
            }
        }

        boolean selectCheckBox = predicate.test(value);
        setSelected(selectCheckBox);

        setBorder(render.getBorder());
        return this;
    }
}
