<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<!-- **************************** -->
<!-- La base des contenu d'onglet -->
<!-- **************************** -->

<JPanel id="mainPanel" abstract='true'
        implements='jaxx.runtime.swing.wizard.WizardStepUI&lt;AdminStep, AdminUIModel&gt;, java.beans.PropertyChangeListener'>

  <import>
    fr.ird.observe.application.swing.ObserveSwingApplicationContext
    fr.ird.observe.application.swing.ui.UIHelper
    fr.ird.observe.application.swing.ui.admin.*
    fr.ird.observe.application.swing.ui.storage.StorageUIModel

    jaxx.runtime.swing.wizard.ext.WizardState
    jaxx.runtime.swing.CardLayout2

    java.awt.Color
    java.awt.Dimension
    java.beans.PropertyChangeEvent
    javax.swing.border.LineBorder
  </import>

  <AdminUIModel id='model' initializer='getContextValue(AdminUIModel.class)'/>

  <AdminActionModel id='stepModel' initializer='model.getStepModel(getStep())'/>

  <StorageUIModel id='localSourceModel' initializer='getModel().getLocalSourceModel()'/>

  <StorageUIModel id='centralSourceModel' initializer='getModel().getCentralSourceModel()'/>

  <AdminTabUIHandler id='handler' initializer='null'/>

  <AdminStep id='step' initializer='null'/>

  <script><![CDATA[

public abstract void initUI(AdminUI ui);

protected AdminTabUI(AdminStep step, AdminUI parentContext) {
    this.step = step;
    UIHelper.checkJAXXContextEntry(parentContext, UIHelper.newContextEntryDef(AdminUIModel.class));
    UIHelper.initContext(this, parentContext);
    $initialize();
    // pour etre sur que step est positionne (la methode d'init supprime la reference...
    this.step = step;
}

public void destroy() {
    description.setText("");
    localSourceModel = null;
    centralSourceModel = null;
    UIHelper.destroy(this);
}

@Override
protected void finalize() throws Throwable {
    super.finalize();
    destroy();
}

@Override
public void propertyChange(PropertyChangeEvent evt) {
    //propertyChange.super(evt);
}

public void addMessage(AdminStep step, String text) {
    getHandler().addMessage(step, text);
}

public void updateState(WizardState newState) {
    getHandler().updateState(this, newState);
}

]]>
  </script>

  <CardLayout2 id='contentLayout'/>

  <!-- titre -->
  <JPanel layout='{new BorderLayout()}' constraints='BorderLayout.NORTH'>
    <JProgressBar id='progress' constraints='BorderLayout.CENTER'/>
  </JPanel>

  <!-- content -->
  <JPanel id='content' decorator='boxed' constraints='BorderLayout.CENTER'>

    <JPanel id='PENDING_content'
            constraints='WizardState.PENDING.name()'/>


    <Table id="NEED_FIX_panel" fill='both'
           constraints='WizardState.NEED_FIX.name()'>
      <row>
        <cell fill='both'>
          <JPanel id="NEED_FIX_labelPanel">
            <JLabel id='NEED_FIX_label'/>
          </JPanel>

        </cell>
      </row>
      <row>
        <cell fill='both' weighty='1' weightx='1'>
          <JPanel id='NEED_FIX_content'/>
        </cell>
      </row>

    </Table>

    <Table id="FAILED_panel" fill='both'
           constraints='WizardState.FAILED.name()'>
      <row>
        <cell fill='both'>
          <JLabel id='FAILED_label'/>
        </cell>
      </row>
      <row>
        <cell fill='both' weighty='1' weightx='1'>
          <JScrollPane id='FAILED_progressionPane'/>
        </cell>
      </row>

    </Table>

    <Table id="RUNNING_panel" fill='both'
           constraints='WizardState.RUNNING.name()'>
      <row>
        <cell fill='both' weighty='0.3'>
          <JPanel id="RUNNING_top">
            <JLabel id='RUNNING_label' constraints='BorderLayout.CENTER'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell fill='both' weighty='0.7' weightx='1'>
          <JScrollPane id='RUNNING_progressionPane'/>
        </cell>
      </row>
    </Table>

    <Table id="SUCCESSED_panel" fill='both'
           constraints='WizardState.SUCCESSED.name()'>
      <row>
        <cell fill='both' weighty='0.3'>
          <JLabel id='SUCCESSED_label'/>
        </cell>
      </row>
      <row>
        <cell fill='both' weighty='0.7' weightx='1'>
          <JScrollPane id='SUCCESSED_progressionPane'/>
        </cell>
      </row>
    </Table>

    <Table fill='both' constraints='WizardState.CANCELED.name()'>
      <row>
        <cell fill='both' weighty='0.3'>
          <JLabel id='CANCELED_label'/>
        </cell>
      </row>
      <row>
        <cell fill='both' weighty='0.7' weightx='1'>
          <JScrollPane id='CANCELED_progressionPane'/>
        </cell>
      </row>
    </Table>

    <JTextArea id='progression' constraints='"PROGRESSION"'/>
    <JToolBar id='progressionTop' constraints='"PROGRESSION_TOP"'>
      <JLabel id='progressionTopLabel'/>
      <javax.swing.Box.Filler
        constructorParams='UIHelper.newMinDimension(), UIHelper.newMinDimension(), UIHelper.newMaxXDimension()'/>
      <JButton id="progressionTopCopyCliptBoard"
               onActionPerformed='UIHelper.copyToClipBoard(progression.getText())'/>
    </JToolBar>
  </JPanel>

  <!-- description -->
  <JScrollPane id="descriptionPane" constraints='BorderLayout.SOUTH'>
    <JTextArea id='description'/>
  </JScrollPane>

  <JPanel id='invisiblePanel' visible="false" constraints='BorderLayout.EAST'>

    </JPanel>

</JPanel>
