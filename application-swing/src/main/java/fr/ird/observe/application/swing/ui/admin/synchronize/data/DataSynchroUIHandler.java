package fr.ird.observe.application.swing.ui.admin.synchronize.data;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUI;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.CopyToLeftDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.CopyToRightDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DataSynchronizeTaskSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DeleteFromLeftDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DeleteFromRightDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.trip.DeleteTripRequest;
import fr.ird.observe.services.service.trip.DeleteTripResult;
import fr.ird.observe.services.service.trip.ExportTripRequest;
import fr.ird.observe.services.service.trip.ExportTripResult;
import fr.ird.observe.services.service.trip.ImportTripRequest;
import fr.ird.observe.services.service.trip.ImportTripResult;
import fr.ird.observe.services.service.trip.TripManagementService;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.DefaultListModel;
import javax.swing.border.TitledBorder;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class DataSynchroUIHandler extends AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(DataSynchroUIHandler.class);

    public DataSynchroUIHandler(DataSynchroUI ui) {
        super(ui);
    }

    public DataSynchroModel getStepModel() {
        return model.getDataSynchroModel();
    }

    @Override
    public DataSynchroUI getUi() {
        return (DataSynchroUI) super.getUi();
    }

    @Override
    public void updateState(AdminTabUI ui, WizardState newState) {
        super.updateState(ui, newState);
    }

    public void initTabUI(AdminUI ui, DataSynchroUI tabUI) {

        super.initTabUI(ui, tabUI);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + tabUI.getStep() + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        getStepModel().getLeftSelectionDataModel().addPropertyChangeListener(evt -> {

            DataSelectionModel model = (DataSelectionModel) evt.getSource();
            boolean withDataSelected = !model.isDataEmpty();

            tabUI.getCopyToRight().setEnabled(withDataSelected);
            tabUI.getDeleteFromLeft().setEnabled(withDataSelected);

        });
        getStepModel().getRightSelectionDataModel().addPropertyChangeListener(evt -> {

            DataSelectionModel model = (DataSelectionModel) evt.getSource();
            boolean withDataSelected = !model.isDataEmpty();

            tabUI.getCopyToLeft().setEnabled(withDataSelected);
            tabUI.getDeleteFromRight().setEnabled(withDataSelected);

        });

        UIHelper.initUI(tabUI.getLeftTreePane(), tabUI.getLeftTree());
        UIHelper.initUI(tabUI.getRightTreePane(), tabUI.getRightTree());

    }

    public void doStartAction() {

        addAdminWorker(getUi().getStartAction().getToolTipText(), this::doStartAction0);

    }

    public void addCopyToLeftTasks() {

        DataSelectionModel rightSelectionDataModel = getStepModel().getRightSelectionDataModel();
        DataSelectionModel leftSelectionDataModel = getStepModel().getLeftSelectionDataModel();
        ObserveTreeHelper rightTreeHelper = getUi().getRightTreeHelper();
        ObserveTreeHelper leftTreeHelper = getUi().getLeftTreeHelper();
        Map<ReferentialReference<ProgramDto>, List<DataReference>> selectedDataByProgram = rightSelectionDataModel.getSelectedDataByProgram();
        rightSelectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {
                boolean tripExistOnLeft = leftSelectionDataModel.containsData(program, trip);
                getStepModel().addTask(new CopyToLeftDataSynchronizeTask(program, trip, tripExistOnLeft));
                rightTreeHelper.removeTrip(program, trip);
                rightSelectionDataModel.removeTrip(program, trip);

                if (tripExistOnLeft) {
                    leftTreeHelper.removeTrip(program, trip);
                    leftSelectionDataModel.removeTrip(program, trip);
                }
            }
        }


    }

    public void addCopyToRightTasks() {

        DataSelectionModel leftSelectionDataModel = getStepModel().getLeftSelectionDataModel();
        DataSelectionModel rightSelectionDataModel = getStepModel().getRightSelectionDataModel();
        ObserveTreeHelper leftTreeHelper = getUi().getLeftTreeHelper();
        ObserveTreeHelper rightTreeHelper = getUi().getRightTreeHelper();
        Map<ReferentialReference<ProgramDto>, List<DataReference>> selectedDataByProgram = leftSelectionDataModel.getSelectedDataByProgram();
        leftSelectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {

                boolean tripExistOnRight = rightSelectionDataModel.containsData(program, trip);
                getStepModel().addTask(new CopyToRightDataSynchronizeTask(program, trip, tripExistOnRight));
                leftTreeHelper.removeTrip(program, trip);
                leftSelectionDataModel.removeTrip(program, trip);

                if (tripExistOnRight) {
                    rightTreeHelper.removeTrip(program, trip);
                    rightSelectionDataModel.removeTrip(program, trip);
                }
            }
        }

    }

    public void addDeleteFromLeftTasks() {

        DataSelectionModel selectionDataModel = getStepModel().getLeftSelectionDataModel();
        ObserveTreeHelper treeHelper = getUi().getLeftTreeHelper();
        Map<ReferentialReference<ProgramDto>, List<DataReference>> selectedDataByProgram = selectionDataModel.getSelectedDataByProgram();
        selectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {
                getStepModel().addTask(new DeleteFromLeftDataSynchronizeTask(program, trip));
                treeHelper.removeTrip(program, trip);
                selectionDataModel.removeTrip(program, trip);
            }
        }

    }

    public void addDeleteFromRightTasks() {

        DataSelectionModel selectionDataModel = getStepModel().getRightSelectionDataModel();
        ObserveTreeHelper treeHelper = getUi().getRightTreeHelper();
        Map<ReferentialReference<ProgramDto>, List<DataReference>> selectedDataByProgram = selectionDataModel.getSelectedDataByProgram();
        selectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {
                getStepModel().addTask(new DeleteFromRightDataSynchronizeTask(program, trip));
                treeHelper.removeTrip(program, trip);
                selectionDataModel.removeTrip(program, trip);
            }
        }

    }

    public void doExecuteAction() {

        addAdminWorker(getUi().getStartAction().getToolTipText(), this::doExecuteAction0);

    }

    private WizardState doStartAction0() {

        DataSynchroUI tabUI = getUi();
        DataSynchroModel stepModel = getStepModel();
        ConfigUI configUI = (ConfigUI) parentUI.getStepUI(AdminStep.CONFIG);

        ObserveSwingDataSource leftSource = getModel().getSafeLocalSource(true);
        stepModel.setLeftSource(leftSource);

        ObserveSwingDataSource rightSource = getModel().getSafeCentralSource(true);
        stepModel.setRightSource(rightSource);

        tabUI.getLeftTreePane().setBorder(new TitledBorder(getModel().getLocalSourceModel().getLabel()));
        stepModel.populateLeftSelectionModel();
        updateSelectionModel(tabUI, tabUI.getLeftTreeHelper(), tabUI.getLeftTree(), leftSource, stepModel.getLeftSelectionDataModel(), tabUI.getLeftSelectionModel());
        sendMessage(t("observe.actions.synchro.referential.message.data.leftData.loaded"));

        configUI.getLocalSourceConfig().setBorder(new TitledBorder(getModel().getLocalSourceLabel()));

        tabUI.getRightTreePane().setBorder(new TitledBorder(getModel().getCentralSourceModel().getLabel()));
        stepModel.populateRightSelectionModel();
        updateSelectionModel(tabUI, tabUI.getRightTreeHelper(), tabUI.getRightTree(), rightSource, stepModel.getRightSelectionDataModel(), tabUI.getRightSelectionModel());
        sendMessage(t("observe.actions.synchro.referential.message.data.rightData.loaded"));

        configUI.getCentralSourceConfig().setBorder(new TitledBorder(getModel().getCentralSourceLabel()));

        return WizardState.NEED_FIX;

    }

    private WizardState doExecuteAction0() {

        ObserveSwingDataSource leftSource = getStepModel().getLeftSource();
        boolean leftSourceIsPG = leftSource.getConfiguration() instanceof ObserveDataSourceConfigurationTopiaPG;

        ObserveSwingDataSource rightSource = getStepModel().getRightSource();
        boolean rightSourceIsPG = rightSource.getConfiguration() instanceof ObserveDataSourceConfigurationTopiaPG;

        DefaultListModel<DataSynchronizeTaskSupport> tasks = getStepModel().getTasks();
        int size = tasks.size();

        DecoratorService decoratorService = getDecoratorService();
        ReferentialReferenceDecorator<ProgramDto> programDecorator = decoratorService.getReferentialReferenceDecorator(ProgramDto.class);

        TripManagementService leftTripManagementService = leftSource.newTripManagementService();
        TripManagementService rightTripManagementService = rightSource.newTripManagementService();

        for (int i = 0; i < size; i++) {

            DataSynchronizeTaskSupport task = tasks.getElementAt(i);

            ReferentialReference<ProgramDto> program = task.getProgram();
            String programId = program.getId();
            String programStr = programDecorator.toString(program);

            DataReference trip = task.getTrip();
            String tripId = trip.getId();
            String tripStr = decoratorService.getTripReferenceDecorator(trip).toString(trip);

            if (task instanceof DeleteFromLeftDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.deleteFromLeftTask", programStr, tripStr));

                DeleteTripRequest deleteTripRequest = new DeleteTripRequest(programId, tripId);
                DeleteTripResult deleteTripResult = leftTripManagementService.deleteTrip(deleteTripRequest);
                logDeleteResult(n("observe.actions.synchro.data.result.delete.left.trip"),
                                deleteTripResult,
                                programDecorator,
                                program,
                                trip);
                continue;

            }

            if (task instanceof DeleteFromRightDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.deleteFromRightTask", programStr, tripStr));

                DeleteTripRequest deleteTripRequest = new DeleteTripRequest(programId, tripId);
                DeleteTripResult deleteTripResult = rightTripManagementService.deleteTrip(deleteTripRequest);
                logDeleteResult(n("observe.actions.synchro.data.result.delete.right.trip"),
                                deleteTripResult,
                                programDecorator,
                                program,
                                trip);
                continue;

            }

            if (task instanceof CopyToLeftDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.copyToLeftTask", programStr, tripStr));

                ExportTripRequest exportTripRequest = new ExportTripRequest(rightSourceIsPG, programId, tripId);
                ExportTripResult exportTripResult = rightTripManagementService.exportTrip(exportTripRequest);
                logExportResult(n("observe.actions.synchro.data.result.export.right.trip"),
                                exportTripResult,
                                programDecorator,
                                program,
                                trip);

                ImportTripRequest importTripRequest = new ImportTripRequest(exportTripResult);
                ImportTripResult importTripResult = leftTripManagementService.importTrip(importTripRequest);
                logImportResult(n("observe.actions.synchro.data.result.import.left.trip"),
                                n("observe.actions.synchro.data.result.delete.left.trip"),
                                importTripResult,
                                programDecorator,
                                program,
                                trip);
                continue;

            }

            if (task instanceof CopyToRightDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.copyToRightTask", programStr, tripStr));

                ExportTripRequest exportTripRequest = new ExportTripRequest(leftSourceIsPG, programId, tripId);
                ExportTripResult exportTripResult = leftTripManagementService.exportTrip(exportTripRequest);
                logExportResult(n("observe.actions.synchro.data.result.export.left.trip"),
                                exportTripResult,
                                programDecorator,
                                program,
                                trip);

                ImportTripRequest importTripRequest = new ImportTripRequest(exportTripResult);
                ImportTripResult importTripResult = rightTripManagementService.importTrip(importTripRequest);
                logImportResult(n("observe.actions.synchro.data.result.import.right.trip"),
                                n("observe.actions.synchro.data.result.delete.right.trip"),
                                importTripResult,
                                programDecorator,
                                program,
                                trip);

            }

        }

        return WizardState.SUCCESSED;

    }

}
