/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.consolidate.ConsolidateModel;
import fr.ird.observe.application.swing.ui.admin.export.ExportModel;
import fr.ird.observe.application.swing.ui.admin.report.ReportModel;
import fr.ird.observe.application.swing.ui.admin.save.SaveLocalModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.DataSynchroModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ReferentialSynchronizeMode;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.application.swing.ui.admin.validate.ValidateModel;
import fr.ird.observe.application.swing.ui.storage.StorageUIHandler;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.validation.ValidationModelMode;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.IdDtos;
import fr.ird.observe.services.dto.actions.report.Report;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseDestroyNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import fr.ird.observe.services.service.longline.TripLonglineService;
import fr.ird.observe.services.service.seine.TripSeineService;
import jaxx.runtime.swing.wizard.ext.WizardExtModel;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.border.TitledBorder;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static fr.ird.observe.application.swing.ObserveResourceManager.Resource;
import static fr.ird.observe.application.swing.configuration.constants.DbMode.USE_LOCAL;
import static fr.ird.observe.application.swing.configuration.constants.DbMode.USE_REMOTE;
import static fr.ird.observe.application.swing.configuration.constants.DbMode.USE_SERVER;
import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le modele de l'ui pour effectuer des opérations de synchro et validation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class AdminUIModel extends WizardExtModel<AdminStep> {

    public static final String SELECTION_MODEL_CHANGED_PROPERTY_NAME = "selectionModelChanged";

    public static final String SELECTED_TRIP_PROPERTY_NAME = "selectedTrip";

    /** Logger */
    private static final Log log = LogFactory.getLog(AdminUIModel.class);

    public static final PropertyChangeListener LOG_PROPERTY_CHANGE_LISTENER = new LogPropertyChanged();

    public static final String PROPERTY_LOCAL_SOURCE_LABEL = "localSourceLabel";
    public static final String PROPERTY_CENTRAL_SOURCE_LABEL = "centralSourceLabel";

    /** la source de données en cours d'utilisation par l'application */
    protected ObserveSwingDataSource previousSource;

    /** la source de données sur laquel on veut travailler */
    protected ObserveSwingDataSource localSource;

    /** la source de données sur laquel on veut travailler */
    protected ObserveDataSourceInformation localSourceInformation;

    /** la source de données dite central (contenant le référentiel valide) */
    protected ObserveSwingDataSource centralSource;

    /** la configuration de la base source */
    protected final StorageUIModel localSourceModel;

    /** la configuration de la base central */
    protected final StorageUIModel centralSourceModel;

    /** le controleur pour configurer les sources */
    protected StorageUIHandler storageHandler;

    /** selection des donnees a valider */
    protected final DataSelectionModel selectionDataModel;

    /** la liste des modes disponibles en entrée */
    protected final EnumSet<DbMode> availableIncomingModes;

    private final PropertyChangeListener listenStepChanged;

    private final PropertyChangeListener listenSelectModified;

    private final PropertyChangeListener listenValidationModified;

    private final PropertyChangeListener listenReportModified;

    private final PropertyChangeListener listenerSelectedDataForReport;

    private final PropertyChangeListener listenConsolidateModified;

    private final PropertyChangeListener listenSaveLocalChanged;
    private final PropertyChangeListener listenReferentialConfigChanged;

    private String localSourceLabel = t("observe.storage.config.source.storage");
    private String centralSourceLabel = t("observe.storage.config.referentiel.storage");

    public String getLocalSourceLabel() {
        return localSourceLabel;
    }

    public void setLocalSourceLabel(String localSourceLabel) {
        Object oldValue = getLocalSourceLabel();
        this.localSourceLabel = localSourceLabel;
        firePropertyChange(PROPERTY_LOCAL_SOURCE_LABEL, oldValue, localSourceLabel);
    }

    public String getCentralSourceLabel() {
        return centralSourceLabel;
    }

    public void setCentralSourceLabel(String centralSourceLabel) {
        Object oldValue = getCentralSourceLabel();
        this.centralSourceLabel = centralSourceLabel;
        firePropertyChange(PROPERTY_CENTRAL_SOURCE_LABEL, oldValue, centralSourceLabel);
    }

    public AdminUIModel() {
        super(AdminStep.class);

        localSourceModel = new StorageUIModel() {

            @Override
            public String getLabel() {
                String txt;
                if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE) || containsOperation(AdminStep.DATA_SYNCHRONIZE)) {
                    txt = n("observe.storage.label.synchro.leftSource");
                } else {
                    txt = n("observe.storage.label.synchro.incoming");
                }
                String params;

                if (getDbMode() == USE_SERVER) {
                    params = t("observe.storage.server.db") + " " + getRemoteUrl();
                } else if (getDbMode() == USE_REMOTE) {
                    params = t("observe.storage.remote.db") + " " + getRemoteUrl();
                } else {
                    params = t("observe.storage.locale.db") + " " + getH2Config().getDirectory().getAbsolutePath();
                }
                txt = t(txt, params);
                return txt;
            }

            @Override
            public void validate() {
                super.validate();

                // on declanche la revalidation du modèle
                firePropertyChange(VALID_PROPERTY_NAME, isValid());
            }

        };

        centralSourceModel = new StorageUIModel() {

            @Override
            public String getLabel() {
                String txt;
                if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE) || containsOperation(AdminStep.DATA_SYNCHRONIZE)) {
                    txt = n("observe.storage.label.synchro.rightSource");
                } else {
                    txt = n("observe.storage.label.synchro.referentiel");
                }
                String params;

                if (getDbMode() == USE_SERVER) {
                    params = t("observe.storage.server.db") + " " + getRemoteUrl();
                } else if (getDbMode() == USE_REMOTE) {
                    params = t("observe.storage.remote.db") + " " + getRemoteUrl();
                } else {
                    params = t("observe.storage.locale.db") + " " + getH2Config().getDirectory().getAbsolutePath();
                }
                txt = t(txt, params);
                return txt;
            }

            @Override
            public void validate() {
                super.validate();

                // on declanche la revalidation du modèle
                firePropertyChange(VALID_PROPERTY_NAME, isValid());
            }
        };

        selectionDataModel = new DataSelectionModel();

        availableIncomingModes = EnumSet.noneOf(DbMode.class);

        if (log.isDebugEnabled()) {
            log.debug("model [" + this + "] is instanciate.");
        }

        listenReferentialConfigChanged = evt -> {


            if (isWasStarted()) {

                // on ne propage plus rien (il n'y a plus de configuration possible...)
                if (log.isDebugEnabled()) {
                    log.debug("Stop propagation, was started... " + evt.getPropertyName());
                }
                return;
            }

            if (ReferentialSynchroModel.SYNCHRONIZE_MODE_PROPERTY_NAME.equals(evt.getPropertyName())) {

                validate();

            }

        };

        listenStepChanged = evt -> {

            if (isWasStarted()) {

                // on ne propage plus rien (il n'y a plus de configuration possible...)
                if (log.isDebugEnabled()) {
                    log.debug("Stop propagation, was started... " + evt.getPropertyName());
                }
                return;
            }

            AdminStep oldStep = (AdminStep) evt.getOldValue();
            AdminStep newStep = (AdminStep) evt.getNewValue();

            if (AdminStep.REPORT == newStep) {

                if (oldStep != null && oldStep.ordinal() < newStep.ordinal()) {

                    int oldIndex = getStepIndex(oldStep);
                    int newIndex = getStepIndex(newStep);

                    if (oldIndex < newIndex) {

                        // on reinitialise le report sélectionné
                        getReportModel().setSelectedReport(null);
                    }
                }
            }

            if (AdminStep.CONFIG == newStep) {

                // on repasse sur l'écran de configuration

                if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE) || containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

                    setLocalSourceLabel(t("observe.storage.config.left.storage"));
                    setCentralSourceLabel(t("observe.storage.config.right.storage"));

                }

                return;
            }

            if (oldStep != null && oldStep != AdminStep.CONFIG) {

                // on fait rien si on ne vient pas de la configuration
                return;
            }

            // on était sur l'écran de configuration

            // mise à jour des modèles de sélection si on arrive sur une étape
            // qui le requière

            boolean needSelect = needSelect();

            if (!needSelect) {

                // pas besoin d'agir sur le model de sélection de données
                return;
            }

            DataSelectionModel selectModel = getSelectionDataModel();

            if (containsOperation(AdminStep.EXPORT_DATA)) {

                // on doit aussi calculer les ids des marées distantes
                computeExistingTrips(getSafeCentralSource(true));

            }

            // on remplit le modèle de sélection
            if (log.isDebugEnabled()) {
                log.debug("Will refill selectionModel [" + newStep + "] : " + selectModel);
            }
            ObserveSwingDataSource dataSource = getSafeLocalSource(true);
            populateSelectionModel(dataSource);
        };
        listenSelectModified = evt -> {
            DataSelectionModel source = (DataSelectionModel) evt.getSource();
            if (log.isDebugEnabled()) {
                log.debug("selection data model [" + source + "] changed on " +
                                  evt.getPropertyName() + ", new value = " +
                                  evt.getNewValue());
            }
            validate();
            if (log.isDebugEnabled()) {
                log.debug("nb selected export datas = " +
                                  source.getSelectedData().size());
            }

            // on declanche la revalidation du modèle
            firePropertyChange(VALID_STEP_PROPERTY_NAME, validStep);
        };
        listenValidationModified = evt -> {
            ValidateModel source = (ValidateModel) evt.getSource();
            if (log.isDebugEnabled()) {
                log.debug("validation model [" + source + "] changed on " +
                                  evt.getPropertyName() + ", new value = " +
                                  evt.getNewValue());
            }
            validate();
            if (log.isDebugEnabled()) {
                log.debug("nb validators = " +
                                  source.getValidators().size()
                );
            }
            firePropertyChange(VALID_STEP_PROPERTY_NAME, validStep);
        };
        listenReportModified = evt -> {
            ReportModel source = (ReportModel) evt.getSource();
            if (log.isDebugEnabled()) {
                log.debug("report model [" + source + "] changed on " +
                                  evt.getPropertyName() + ", new value = " +
                                  evt.getNewValue());
            }
            validate();
            firePropertyChange(VALID_STEP_PROPERTY_NAME, validStep);
        };
        listenerSelectedDataForReport = evt -> {

            // la modification de la sélection entraine la suppression d'un report sélectionné
            getReportModel().setSelectedReport(null);

            // cela entraine aussi la modification de la marée sélectionnée
            firePropertyChange(SELECTED_TRIP_PROPERTY_NAME, getSelectedTrip());

        };
        listenConsolidateModified = evt -> {
            ConsolidateModel source = (ConsolidateModel) evt.getSource();
            if (log.isDebugEnabled()) {
                log.debug("consolidate model [" + source + "] changed on " +
                                  evt.getPropertyName() + ", new value = " +
                                  evt.getNewValue());
            }
            validate();
            firePropertyChange(VALID_STEP_PROPERTY_NAME, validStep);
        };
        listenSaveLocalChanged = evt -> {

            String propertyName = evt.getPropertyName();

            if (SaveLocalModel.BACKUP_FILE_PROPERTY_NAME.equals(propertyName)) {

                validate();
                return;
            }

            if (SaveLocalModel.LOCAL_SOURCE_NEED_SAVE_PROPERTY_NAME.equals(propertyName)) {
                boolean localSourceNeedSave = (Boolean) evt.getNewValue();
                if (localSourceNeedSave) {
                    // l'opération de sauvegarde est obligatoire
                    setStepState(AdminStep.SAVE_LOCAL, WizardState.PENDING);
                }
                validate();
                return;
            }

            if (log.isDebugEnabled()) {
                log.debug("Skip property " + propertyName);
            }
        };
    }

    public DataSelectionModel getSelectionDataModel() {
        return selectionDataModel;
    }

    public boolean needSelect() {
        if (WizardState.CANCELED == getModelState()) {
            return false;
        }
        Set<AdminStep> operations = getOperations();
        for (AdminStep operation : operations) {
            if (operation.isNeedSelect()) {
                return true;
            }
        }
        return false;
    }

    public DataReference getSelectedTrip() {
        Set<DataReference> data = getSelectionDataModel().getSelectedData();
        if (data.isEmpty()) {

            // pas de Trip selectionne
            return null;
        }
        DataReference dto = data.iterator().next();
        if (IdDtos.isTrip(dto)) {
            return dto;
        }

        // la donnée n'est pas une marée
        return null;
    }

    @Override
    public AdminActionModel getStepModel(AdminStep operation) {
        return (AdminActionModel) super.getStepModel(operation);
    }

    /**
     * Pour savoir si on a besoin d'une base source pour l'opération.
     *
     * Actuellement, seul l'opération d'import acces n'a pas besoin d'une telle
     * base.
     *
     * @return {@code true} si au moins une des opérations a besoin d'une source
     * d'entrée, {@code false} autrement.
     * @since 2.0
     */
    public boolean isNeedIncomingDataSource() {
        for (AdminStep op : getOperations()) {
            if (op.hasIncomingModes()) {

                // l'operation requiere une base en entree
                return true;
            }
        }

        // aucune operation ne requiere pas de base en entree
        return false;
    }

    /**
     * Pour savoir si on a besoin d'une base de référence pour l'opération.
     *
     * @return {@code true} si au moins une des opérations a besoin d'une source
     * de référence, {@code false} autrement.
     * @since 2.0
     */
    public boolean isNeedReferentielDataSource() {
        for (AdminStep op : getOperations()) {
            if (op.isNeedReferentiel()) {

                // l'operation requiere une base de référence
                return true;
            }
        }

        // aucune operation ne requiere pas de base de référence
        return false;
    }

    /**
     * Construit l'ensemble des modes disponibles pour la base en entrée.
     *
     * @return l'ensemble des modes disponibles.
     */
    public EnumSet<DbMode> getIncomingDataSourceMode() {

        EnumSet<DbMode> result = EnumSet.noneOf(DbMode.class);
        getOperations().stream().filter(AdminStep::hasIncomingModes).forEach(op -> result.addAll(Arrays.asList(op.getIncomingModes())));
        return result;
    }

    public void start(AdminUI ui) {

        availableIncomingModes.clear();

        storageHandler = ui.getContextValue(StorageUIHandler.class);

        ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();

        // demarrage du modèle : on fixe ici une fois pour toute les liste
        // des onglets visibles
        start();

        // preparation de l'ui (création des onglets visibles)
        ui.blockOperations();

        if (log.isInfoEnabled()) {
            log.info("enables steps = " + steps);
            log.info("enables operations = " + operations);
        }

        // on positionne la source courante de l'application comme service
        // entrant (on se sert de la configuration de ce service s'il existe
        // pour la source locale).
        ObserveSwingDataSource previousSource = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
        setPreviousSource(previousSource);

        startIncomingSourceModel(ui);

        startCentralSourceModel(ui);

        startSelectModel();

        // avant le demarrage du wizard, on ferme toujours la source
        // en cours d'utilisation

        if (previousSource != null && previousSource.isOpen()) {

            if (log.isDebugEnabled()) {
                log.debug("Close previous source " +
                                  previousSource.getLabel());
            }
            doCloseSource(previousSource, false);
        }

        if (containsOperation(AdminStep.SAVE_LOCAL)) {

            File backupfile = new File(config.getBackupDirectory(), storageHandler.getDefaultBackupFilename());

            getSaveLocalModel().setBackupFile(backupfile);
            getSaveLocalModel().setDoBackup(true);
            getSaveLocalModel().removePropertyChangeListener(listenSaveLocalChanged);
            getSaveLocalModel().addPropertyChangeListener(listenSaveLocalChanged);
        }

        if (containsOperation(AdminStep.VALIDATE)) {

            getValidateModel().removePropertyChangeListener(listenValidationModified);
            getValidateModel().addPropertyChangeListener(listenValidationModified);
            getValidateModel().addScope(NuitonValidatorScope.ERROR);
            getValidateModel().setModelMode(ValidationModelMode.DATA);
            getValidateModel().setContextName(ValidateService.SERVICE_VALIDATION_CONTEXT);
            getValidateModel().setGenerateReport(true);
            if (!config.getValidationReportDirectory().exists()) {
                boolean b = config.getValidationReportDirectory().mkdirs();
                if (!b) {
                    throw new RuntimeException("Could not create directory " + config.getValidationReportDirectory());
                }
            }
            File reportFile = new File(config.getValidationReportDirectory(),
                                       getValidateModel().getDefaultReportFilename());
            getValidateModel().setReportFile(reportFile);
        }

        if (containsOperation(AdminStep.REPORT)) {

            File reportFile = Resource.report.getFile(config.getReportDirectory());

            if (reportFile.exists()) {
                if (log.isInfoEnabled()) {
                    log.info("Will use report file : " + reportFile);
                }

                getReportModel().setReportFile(reportFile);
            } else {
                if (log.isWarnEnabled())
                    log.warn("Default report file " + reportFile + " does not exists.");
            }
            // on charge la liste des reports disponilbes
            List<Report> reports = getReportModel().getReports();

            // on ecoute la modification du modèle
            getReportModel().removePropertyChangeListener(listenReportModified);
            getReportModel().addPropertyChangeListener(listenReportModified);

            // on ecoute la modification de la sélection
            getSelectionDataModel().removePropertyChangeListener(DataSelectionModel.PROPERTY_SELECTED_DATA, listenerSelectedDataForReport);
            getSelectionDataModel().addPropertyChangeListener(DataSelectionModel.PROPERTY_SELECTED_DATA, listenerSelectedDataForReport);

            if (log.isInfoEnabled()) {
                log.info("Detects " + reports.size() + " report(s).");
            }
        }

        if (containsOperation(AdminStep.CONSOLIDATE)) {

            // Anything to init ?
            getConsolidateModel().removePropertyChangeListener(listenConsolidateModified);
            getConsolidateModel().addPropertyChangeListener(listenConsolidateModified);
        }

        if (containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

            ConfigUI configUI = (ConfigUI) ui.getStepUI(AdminStep.CONFIG);

            configUI.getLocalSourceConfig().setBorder(new TitledBorder(getLocalSourceLabel()));
            configUI.getCentralSourceConfig().setBorder(new TitledBorder(getCentralSourceLabel()));

        }
        if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {

            getReferentialSynchroModel().removePropertyChangeListener(listenReferentialConfigChanged);
            getReferentialSynchroModel().addPropertyChangeListener(listenReferentialConfigChanged);

            getReferentialSynchroModel().addPropertyChangeListener(ReferentialSynchroModel.SYNCHRONIZE_MODE_PROPERTY_NAME, evt -> {

                ReferentialSynchronizeMode newValue = (ReferentialSynchronizeMode) evt.getNewValue();

                if (ui.getTabs().getComponentCount() > 1 && newValue != null) {
                    ui.getTabs().setTitleAt(1, t("observe.actions.synchro.referential.withMode", I18nEnumHelper.getLabel(newValue)));
                }

            });

            getReferentialSynchroModel().setSynchronizeMode(ReferentialSynchronizeMode.FROM_LEFT_TO_RIGHT);

        }

        // on ecoute les modifications d'étapes pour remplir les modèles de sélection
        // si nécessaire

        removePropertyChangeListener(STEP_PROPERTY_NAME, listenStepChanged);
        addPropertyChangeListener(STEP_PROPERTY_NAME, listenStepChanged);

        if (log.isInfoEnabled()) {
            log.info("End of start...");
        }

        // on revalide le modèle (tout est prêt)
        validate();
    }

    protected void startCentralSourceModel(AdminUI ui) {

        if (!isNeedReferentielDataSource()) {

            // pas besoin de la base distante
            return;
        }

        // par default, on tente d'utiliser la base distance
        centralSourceModel.init(ui, null);
        centralSourceModel.setCanCreateLocalService(false);
        boolean canUseLocalSource = getOperations().contains(AdminStep.DATA_SYNCHRONIZE) || getOperations().contains(AdminStep.REFERENTIAL_SYNCHRONIZE);
        centralSourceModel.setCanUseLocalService(canUseLocalSource);
        centralSourceModel.setCanUseRemoteService(true);
        centralSourceModel.setCanUseServerService(true);
        centralSourceModel.start(USE_REMOTE);

        if (log.isDebugEnabled()) {
            centralSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            centralSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }

        // on teste si la connexion distante existe
        if (centralSourceModel.isValid()) {
            centralSourceModel.testRemote();
        }
    }

    protected void startIncomingSourceModel(AdminUI ui) {

        if (!isNeedIncomingDataSource()) {

            // pas besoin de la base locale
            return;
        }

        ObserveSwingDataSource previousSource = getPreviousSource();

        ObserveDataSourceConfiguration previousSourceConfig = null;
        ObserveDataSourceInformation previousSourceInfo = null;

        EnumSet<DbMode> authorizedModes = getIncomingDataSourceMode();
        EnumSet<DbMode> modes = EnumSet.noneOf(DbMode.class);

        if (authorizedModes.contains(USE_LOCAL)) {

            // ce mode est disponible uniquement si une base locale existe
            if (ObserveSwingApplicationContext.get().getConfig().isLocalStorageExist()) {
                modes.add(USE_LOCAL);
            }
        }

        if (authorizedModes.contains(USE_REMOTE)) {

            modes.add(USE_REMOTE);
        }

        if (authorizedModes.contains(USE_SERVER)) {

            modes.add(USE_SERVER);
        }

        if (authorizedModes.contains(DbMode.CREATE_LOCAL)) {

            modes.add(DbMode.CREATE_LOCAL);
        }

        if (previousSource != null) {

            try {
                previousSourceConfig = previousSource.getConfiguration().clone();
                previousSourceInfo = previousSource.getInformation();
            } catch (CloneNotSupportedException e) {
                if (log.isErrorEnabled()) {
                    log.error("con not clone previous data configuration", e);
                }
            }

            if (previousSource.isRemote()) {

                if (!modes.contains(USE_REMOTE)) {

                    // pas autorise a utiliser cette source en entree
                    previousSource = null;
                }
            }

            if (previousSource != null && previousSource.isServer()) {

                if (!modes.contains(USE_SERVER)) {

                    // pas autorise a utiliser cette source en entree
                    previousSource = null;
                }
            }

            if (previousSource != null && previousSource.isLocal()) {

                if (!modes.contains(USE_LOCAL)) {

                    // pas autorise a utiliser cette source en entree
                    previousSource = null;
                }
            }
        }

        availableIncomingModes.addAll(modes);

        localSourceModel.setCanCreateLocalService(modes.contains(DbMode.CREATE_LOCAL));
        localSourceModel.setCanUseLocalService(modes.contains(USE_LOCAL));
        localSourceModel.setCanUseRemoteService(modes.contains(USE_REMOTE));
        localSourceModel.setCanUseServerService(modes.contains(USE_SERVER));

        if (previousSource == null) {

            // on intialize le modèle de la source locale à partir du service entrant (s'il existe)
            localSourceModel.init(ui, null);

        } else {

            // on intialize le modèle de la source locale à partir du service entrant (s'il existe)
            localSourceModel.initFromPreviousConfig(ui, previousSourceConfig, previousSourceInfo);
        }

        DbMode dbMode = localSourceModel.getDbMode();
        localSourceModel.start(dbMode);

        if (log.isDebugEnabled()) {
            localSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            localSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }
    }

    protected void startSelectModel() {

        if (!needSelect()) {

            // pas d'opération avec une sélection de données, rien a faire ici
            return;
        }

        DataSelectionModel selectionModel = getSelectionDataModel();

        if (containsOperation(AdminStep.EXPORT_DATA)) {

            selectionModel.setUseData(true);
            selectionModel.setUseOpenData(false);
            selectionModel.setUseReferentiel(false);
        }

        if (containsOperation(AdminStep.REPORT)) {

            selectionModel.setUseData(true);
            selectionModel.setUseOpenData(true);
            selectionModel.setUseReferentiel(false);
        }

        if (containsOperation(AdminStep.VALIDATE)) {

            selectionModel.setUseOpenData(true);
        }

        if (containsOperation(AdminStep.CONSOLIDATE)) {

            selectionModel.setUseOpenData(true);
            selectionModel.setUseData(true);
            selectionModel.setUseReferentiel(false);
        }

        selectionModel.addPropertyChangeListener(listenSelectModified);
    }

    @Override
    public void destroy() {
        localSourceModel.destroy();
        centralSourceModel.destroy();
        selectionDataModel.destroy();
        super.destroy();
    }

    public ObserveSwingDataSource getPreviousSource() {
        return previousSource;
    }

    public void setPreviousSource(ObserveSwingDataSource previousSource) {
        this.previousSource = previousSource;
    }

    public ObserveSwingDataSource getLocalSource() {
        return localSource;
    }

    public ObserveDataSourceInformation getLocalSourceInformation() {
        if (localSourceInformation == null) {

            ObserveSwingDataSource localSource = getSafeLocalSource(false);

            try {

                localSourceInformation = localSource.checkCanConnect();

            } catch (Exception e) {
                //FIXME ! il faut faire quelque chose dans ce cas précis, au moins avertir l'utilisateur
                if (log.isErrorEnabled()) {
                    log.error("unable to find local source information", e);
                }
            }

        }

        return localSourceInformation;
    }

    public ObserveSwingDataSource getCentralSource() {
        return centralSource;
    }

    public ObserveSwingDataSource getSafeLocalSource(boolean open) {
        if (localSource == null || open && !localSource.isOpen()) {

            localSource = createDataSource(getLocalSourceModel());
        }
        if (open) {

            doOpenSource(localSource);
        }
        return localSource;
    }

    public ObserveSwingDataSource getSafeCentralSource(boolean open) {
        if (centralSource == null || open && !centralSource.isOpen()) {

            centralSource = createDataSource(getCentralSourceModel());
        }
        if (open) {
            doOpenSource(centralSource);
        }
        return centralSource;
    }

    public StorageUIModel getLocalSourceModel() {
        return localSourceModel;
    }

    public StorageUIModel getCentralSourceModel() {
        return centralSourceModel;
    }

    public ValidateModel getValidateModel() {
        return (ValidateModel) getStepModel(AdminStep.VALIDATE);
    }

    public SynchronizeModel getSynchronizeReferentielModel() {
        return (SynchronizeModel) getStepModel(AdminStep.SYNCHRONIZE);
    }

    public ExportModel getExportModel() {
        return (ExportModel) getStepModel(AdminStep.EXPORT_DATA);
    }

    public ReportModel getReportModel() {
        return (ReportModel) getStepModel(AdminStep.REPORT);
    }

    public ConsolidateModel getConsolidateModel() {
        return (ConsolidateModel) getStepModel(AdminStep.CONSOLIDATE);
    }

    public SaveLocalModel getSaveLocalModel() {
        return (SaveLocalModel) getStepModel(AdminStep.SAVE_LOCAL);
    }

    public DataSynchroModel getDataSynchroModel() {
        return (DataSynchroModel) getStepModel(AdminStep.DATA_SYNCHRONIZE);
    }

    public ReferentialSynchroModel getReferentialSynchroModel() {
        return (ReferentialSynchroModel) getStepModel(AdminStep.REFERENTIAL_SYNCHRONIZE);
    }

    @Override
    public void cancel() {
        super.cancel();
        // on affiche l'onglet de resume si requis
        AdminStep newStep = getSteps().get(getSteps().size() - 1);
        if (log.isDebugEnabled()) {
            log.debug("Operation canceled, will go to final step " + newStep);
        }
        gotoStep(newStep);
    }

    @Override
    public AdminUIModel addOperation(AdminStep step) {
        if (AdminStep.EXPORT_DATA == step) {

            // pour exporter les données utilisateurs
            // l'opération de synchronisation de référentiel est nécessaire
            getOperations().add(AdminStep.SYNCHRONIZE);

            // l'opération de calcul des données est aussi obligatoire
            getOperations().add(AdminStep.CONSOLIDATE);
        }

        if (AdminStep.REPORT == step) {

            // l'opération de calcul des données est obligatoire
            getOperations().add(AdminStep.CONSOLIDATE);
        }
        return (AdminUIModel) super.addOperation(step);
    }

    @Override
    public void removeOperation(AdminStep step) {
        if (AdminStep.SYNCHRONIZE == step) {

            // pour exporter les données utilisateurs
            // l'objectOperation de synchronisation de référentiel est nécessaire
            getOperations().remove(AdminStep.EXPORT_DATA);
        }

        if (AdminStep.CONSOLIDATE == step) {

            // pour exporter les données utilisateurs ou effectuer des reports
            // l'objectOperation de calcul des données est nécessaire
            getOperations().remove(AdminStep.EXPORT_DATA);
            getOperations().remove(AdminStep.REPORT);
        }

        super.removeOperation(step);
    }

    @Override
    public boolean validate(AdminStep step) {

        boolean validate = super.validate(step) && !getOperations().isEmpty();
        if (!validate) {
            if (log.isDebugEnabled()) {
                log.debug("not valid from generic states...");
            }
            return false;
        }

        DataSelectionModel selectionModel = getSelectionDataModel();

        switch (step) {
            case CONFIG:
                if (isNeedIncomingDataSource()) {
                    if (!localSourceModel.isValid()) {
                        if (log.isDebugEnabled()) {
                            log.debug("incoming service is not valid");
                        }
                        validate = false;
                    }
                }
                if (isNeedReferentielDataSource()) {
                    if (!centralSourceModel.isValid()) {
                        if (log.isDebugEnabled()) {
                            log.debug("referentiel service is not valid");
                        }
                        validate = false;
                    }
                }

                if (containsOperation(AdminStep.VALIDATE)) {

                    // il faut avoir selectionner un context
                    // il faut avoir au moins selectionner un scope
                    if (StringUtils.isEmpty(getValidateModel().getContextName())) {
                        if (log.isDebugEnabled()) {
                            log.debug("no validation context name");
                        }
                        return false;
                    }
                    if (getValidateModel().getScopes().isEmpty()) {
                        if (log.isDebugEnabled()) {
                            log.debug("no validation scopes");
                        }
                        return false;
                    }
                    if (getValidateModel().getModelMode() == null) {
                        if (log.isDebugEnabled()) {
                            log.debug("no validation model mode");
                        }
                        return false;
                    }
                    if (getValidateModel().isGenerateReport()) {

                        // le fichier de rapport ne doit pas exister
                        File file = getValidateModel().getReportFile();

                        if (file.exists()) {
                            if (log.isDebugEnabled()) {
                                log.debug("report file already exists");
                            }
                            return false;
                        }

                        // le repertoire du rapport doit exister
                        File parentFile = file.getParentFile();
                        if (parentFile == null || !parentFile.exists()) {
                            return false;
                        }
                    }
                    if (getValidateModel().getValidators().isEmpty()) {
                        if (log.isDebugEnabled()) {
                            log.debug("no validators detected");
                        }
                        return false;
                    }

                    // la base precedente doit etre ouverte
                    ObserveDataSourceInformation dataSourceInformation = getLocalSourceInformation();

                    // si le test de connection a echoue
                    if (dataSourceInformation == null) {
                        if (log.isDebugEnabled()) {
                            log.debug("can not connect to data source ");
                        }
                        return false;
                    }


                    // pour valider une base il faut les droits
                    if (selectionModel.isUseData()) {

                        // il faut les droits en Lecture sur les donnes
                        if (!(dataSourceInformation.canReadData())) {
                            if (log.isDebugEnabled()) {
                                log.debug("can not read data");
                            }
                            return false;
                        }
                    }
                    if (selectionModel.isUseReferentiel()) {

                        // il faut les droits en L sur le referentiel
                        if (!(dataSourceInformation.canReadReferential())) {
                            if (log.isDebugEnabled()) {
                                log.debug("can not read referentiel");
                            }
                            return false;
                        }
                    }
                }
                if (validate && containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

                    // les deux bases (source et referentiel) doivent etre different
                    validate = validateNotSameDataSources();

                    ObserveDataSourceInformation leftDataSourceInformation = getLocalSourceInformation();
                    if (!(leftDataSourceInformation.canReadData() && leftDataSourceInformation.canWriteData())) {
                        if (log.isDebugEnabled()) {
                            log.debug("can not read and write data on left data source");
                        }
                        return false;
                    }

                    if (centralSourceModel.getDataSourceInformation() != null) {

                        ObserveDataSourceInformation rightDataSourceInformation = centralSourceModel.getDataSourceInformation();
                        if (!(rightDataSourceInformation.canReadData() && rightDataSourceInformation.canWriteData())) {
                            if (log.isDebugEnabled()) {
                                log.debug("can not read and write data on right data source");
                            }
                            return false;
                        }

                    }

                }

                if (validate && containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {

                    // les deux bases (source et referentiel) doivent etre different
                    validate = validateNotSameDataSources();

                    ObserveDataSourceInformation leftDataSourceInformation = getLocalSourceInformation();
                    if (leftDataSourceInformation == null) {
                        return false;
                    }
                    if (!(leftDataSourceInformation.canReadReferential())) {
                        if (log.isDebugEnabled()) {
                            log.debug("can not read and write referential on left data source");
                        }
                        return false;
                    }

                    ObserveDataSourceInformation rightDataSourceInformation = centralSourceModel.getDataSourceInformation();
                    if (rightDataSourceInformation == null) {
                        return false;
                    }

                    if (!(rightDataSourceInformation.canReadReferential())) {
                        if (log.isDebugEnabled()) {
                            log.debug("can not read and write referential on right data source");
                        }
                        return false;
                    }

                    ReferentialSynchronizeMode synchronizeMode = getReferentialSynchroModel().getSynchronizeMode();
                    if (synchronizeMode == null) {
                        return false;
                    }

                    if (synchronizeMode.isLeftWrite()) {

                        validate = localSourceModel.isLocal() || leftDataSourceInformation.canWriteReferential();

                    }

                    if (synchronizeMode.isRightWrite()) {

                        validate = centralSourceModel.isLocal() || rightDataSourceInformation.canWriteReferential();

                    }

                }

                if (containsOperation(AdminStep.SYNCHRONIZE)) {

                    if (!localSourceModel.isValid()) {
                        return  false;
                    }

                    ObserveDataSourceInformation leftDataSourceInformation = getLocalSourceInformation();
                    if (leftDataSourceInformation == null) {
                        return false;
                    }

                    if (!centralSourceModel.isValid()) {
                        return  false;
                    }

                    ObserveDataSourceInformation rightDataSourceInformation = centralSourceModel.getDataSourceInformation();
                    if (rightDataSourceInformation == null) {
                        return false;
                    }

                    if (!(rightDataSourceInformation.canReadReferential())) {
                        if (log.isDebugEnabled()) {
                            log.debug("can not read referential on central source");
                        }
                        return false;
                    }

                    // les deux bases (source et referentiel) doivent etre different
                    validate = validateNotSameDataSources();

                }
                if (containsOperation(AdminStep.EXPORT_DATA)) {

                    // il faut une base locale et une connexion distante
                    // avec des droits d'écriture de données
                    validate &= centralSourceModel.getDataSourceInformation() != null
                            && centralSourceModel.getDataSourceInformation().canWriteData();

                }

                if (containsOperation(AdminStep.REPORT)) {

                    // il faut le fichier di'mport existe
                    File reportFile = getReportModel().getReportFile();
                    validate &= reportFile != null &&
                            reportFile.exists();
                }
                break;
            case SELECT_DATA:

                if (containsOperation(AdminStep.VALIDATE)) {

                    // la config doit etre ok
                    validate = validate(AdminStep.CONFIG);
                    if (validate) {

                        // il faut au moins une donnee de selectionnee
                        boolean empty = selectionModel.isEmpty();
                        validate = !empty;
                    }
                }
                if (containsOperation(AdminStep.EXPORT_DATA)) {

                    // la config doit etre ok
                    validate &= validate(AdminStep.CONFIG);
                    if (validate) {

                        // il faut au moins une donnee de selectionnee
                        boolean empty = selectionModel.isDataEmpty();
                        validate = !empty;
                    }
                }
                if (containsOperation(AdminStep.CONSOLIDATE)) {

                    // la config doit etre ok
                    validate &= validate(AdminStep.CONFIG);
                    if (validate) {

                        // il faut au moins une donnee de selectionnee
                        boolean empty = selectionModel.isDataEmpty();
                        validate = !empty;
                    }
                }
                if (containsOperation(AdminStep.REPORT)) {

                    // la config doit etre ok
                    validate &= validate(AdminStep.CONFIG);
                    if (validate) {

                        // il faut exactement une Trip de selectionnee
                        boolean empty = selectionModel.isDataEmpty();
                        validate = !empty &&
                                selectionModel.getSelectedData().size() == 1;
                    }
                }
                break;
            case VALIDATE:
                validate = validate(AdminStep.SELECT_DATA) &&
                        getStepState(step) == WizardState.SUCCESSED;
                break;
            case EXPORT_DATA:
                validate = validate(AdminStep.SELECT_DATA) &&
                        getStepState(step) == WizardState.SUCCESSED;
                break;
            case CONSOLIDATE:
                validate = validate(AdminStep.SELECT_DATA) &&
                        getStepState(step) == WizardState.SUCCESSED;
                break;
            case REPORT:

                // pour acceder a l'onglet des report, il faut que
                // l'onglet de sélection des données soit ok
                validate = validate(AdminStep.SELECT_DATA);
                break;
            case SYNCHRONIZE:
                validate = getStepState(step) == WizardState.SUCCESSED;
                break;
            case SAVE_LOCAL:
                // valide si l'action a ete executee avec success
                validate = !getSaveLocalModel().isDoBackup() ||
                        getStepState(step) == WizardState.SUCCESSED;
                break;
            case SHOW_RESUME:
                validate = true;
        }
        return validate;
    }

    @Override
    protected AdminStep[] updateStepUniverse() {

        List<AdminStep> universe = new ArrayList<>();

        // toujours l'onglet de configuration des opérations
        universe.add(AdminStep.CONFIG);

        // pour savoir si on doit ajouter l'onglet de résumé (cas par défaut)
        boolean needResume = true;
        if (!operations.isEmpty()) {

            if (needSelect()) {

                // ajout de l'onglet de selection des donnees
                universe.add(AdminStep.SELECT_DATA);
            }

            if (containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

                universe.add(AdminStep.DATA_SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {

                universe.add(AdminStep.REFERENTIAL_SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.SYNCHRONIZE)) {

                // ajout de l'onglet de resolution des entites obsoletes
                universe.add(AdminStep.SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.VALIDATE)) {

                // ajout de l'onglet de validation
                universe.add(AdminStep.VALIDATE);
            }

            if (containsOperation(AdminStep.CONSOLIDATE)) {

                // ajout de l'onglet de consolidation
                universe.add(AdminStep.CONSOLIDATE);
            }

            if (containsOperation(AdminStep.REPORT)) {

                universe.add(AdminStep.REPORT);

                // pas de page de resume
                needResume = false;
            }

            updateSaveLocalOperation();

            if (containsOperation(AdminStep.SAVE_LOCAL)) {

                // ajout de l'onglet de sauvegarde de la base locale
                universe.add(AdminStep.SAVE_LOCAL);
            }

            if (containsOperation(AdminStep.EXPORT_DATA)) {

                // ajout de l'onglet de sauvegarde de la base distante
                universe.add(AdminStep.EXPORT_DATA);
            }

            if (needResume) {

                // ajout d'un onglet de resume
                universe.add(AdminStep.SHOW_RESUME);
            }
        }
        return universe.toArray(new AdminStep[universe.size()]);
    }

    public void removeLocalSource() {
        if (localSource != null) {
            doCloseSource(localSource, false);
            localSource = null;
            localSourceInformation = null;
        }
    }

    public void removeCentralSource() {
        if (centralSource != null) {
            doCloseSource(centralSource, false);
            centralSource = null;
        }
    }

    public void populateSelectionModel(ObserveSwingDataSource source) {
        try {
            DataSelectionModel.populate(getSelectionDataModel(), source);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("could not populate selected model", e);
            }
        } finally {

            // on notifie que le modèle de sélection a changé
            // (il faut donc recalculé l'arbre de sélection)
            firePropertyChange(SELECTION_MODEL_CHANGED_PROPERTY_NAME, getSelectionDataModel());
        }
    }

    protected void computeExistingTrips(ObserveSwingDataSource source) {

        List<DataReference<?>> existingTrip = Lists.newArrayList();

        TripSeineService tripSeineService = source.newTripSeineService();
        DataReferenceSet<TripSeineDto> tripSeineSet = tripSeineService.getAllTripSeine();
        existingTrip.addAll(tripSeineSet.getReferences());

        TripLonglineService tripLonglineService = source.newTripLonglineService();
        DataReferenceSet<TripLonglineDto> tripLonglineSet = tripLonglineService.getAllTripLongline();
        existingTrip.addAll(tripLonglineSet.getReferences());


        getExportModel().setExistingTrips(existingTrip);

    }

    protected void updateSaveLocalOperation() {
        boolean shouldAdd = false;
        for (AdminStep s : operations) {
            if (s.isNeedSave()) {
                shouldAdd = true;
            }
        }
        if (shouldAdd) {

            // ajout de l'operations
            operations.add(AdminStep.SAVE_LOCAL);
        } else {

            // on doit supprimer l'operations
            operations.remove(AdminStep.SAVE_LOCAL);
        }
    }

    protected ObserveSwingDataSource createDataSource(StorageUIModel model) {

        return storageHandler.newDataSourceFromModel(model);
    }

    protected void doOpenSource(ObserveSwingDataSource source) {

        if (source != null && !source.isOpen()) {
            try {
                source.open();
            } catch (DatabaseConnexionNotAuthorizedException | DatabaseNotFoundException | BabModelVersionException e) {
                throw new IllegalStateException("Could not open " + source, e);
            }
        }

    }

    protected void doCloseSource(ObserveSwingDataSource source, boolean destroy) {

        if (source != null && source.isOpen()) {
            if (destroy) {
                try {
                    source.destroy();
                } catch (DatabaseDestroyNotAuthorizedException e) {
                    throw new IllegalStateException("Could not destroy " + source, e);
                }
            } else {
                source.close();
            }
        }

    }

    private static class LogPropertyChanged implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String name = evt.getPropertyName();
            Object oldValue = evt.getOldValue();
            Object newValue = evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug(evt.getSource() + " - Property [" + name + "] has changed from  " + oldValue + " to " + newValue);
            }

        }
    }

    private boolean validateNotSameDataSources() {
        boolean validate = true;
        DbMode dbMode = localSourceModel.getDbMode();
        if (dbMode != null && dbMode == centralSourceModel.getDbMode()) {
            switch (dbMode) {
                case USE_REMOTE:
                    validate = !Objects.equals(localSourceModel.getPgConfig().getJdbcUrl(), centralSourceModel.getPgConfig().getJdbcUrl());
                    break;
                case USE_SERVER:
                    validate = !Objects.equals(localSourceModel.getRestConfig().getServerUrl(), centralSourceModel.getRestConfig().getServerUrl())
                            || !Objects.equals(localSourceModel.getRestConfig().getOptionalDatabaseName(), centralSourceModel.getRestConfig().getOptionalDatabaseName());

                    break;
                default:
                    validate = false;
            }
        }
        return validate;
    }

}
