/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import jaxx.runtime.swing.application.ActionWorker;
import jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.concurrent.Callable;

/**
 * Un worker pour les opération longues d'administration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class AdminActionWorker extends ActionWorker<WizardState, String> {

    protected final AdminTabUIHandler handler;

    protected AdminActionWorker(AdminTabUIHandler handler,
                                String actionLabel) {
        super(actionLabel);
        this.handler = handler;
    }

    public static AdminActionWorker newWorker(
            AdminTabUIHandler handler,
            String actionLabel,
            Callable<WizardState> target) {
        AdminActionWorker adminActionWorker = new AdminActionWorker(handler, actionLabel);
        adminActionWorker.setTarget(target);
        return adminActionWorker;
    }

    public AdminTabUIHandler getHandler() {
        return handler;
    }
}
