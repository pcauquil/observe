/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table;

import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.BinderModelBuilder;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;
import java.util.List;

/**
 * Le modèle d'un écran d'édition avec tableau.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public abstract class ContentTableUIModel<E extends IdDto, D extends IdDto> extends ContentUIModel<E> {

    public static final String PROPERTY_CAN_SAVE_ROW = "canSaveRow";

    public static final String PROPERTY_CAN_RESET_ROW = "canResetRow";

    public static final String PROPERTY_SHOW_DATA = "showData";

    public static final String PROPERTY_ROW_SAVED = "rowSaved";

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentTableUIModel.class);

    protected final Class<D> childType;

    protected final String[] parentProperties;

    protected final String[] childProperties;

    private final transient ListSelectionModel selectionModel;

    protected D tableEditBean;

    protected boolean canSaveRow;

    protected boolean canResetRow;

    /**
     * Pour savoir si on peut voir les données du l'écran.
     *
     * Cela diffère du mode de l'écran, à utiliser par exemple pour les écrans
     * d'échantillons lorsqu'aucune discarded n'est effectuée. on se retrouve en
     * mode ContentMode.READ mais ce n'est pas suffisant.
     *
     * @since 3.0
     */
    protected boolean showData;

    protected boolean rowSaved;

    private ContentTableModel<E, D> tableModel;

    public ContentTableUIModel(Class<E> beanType,
                               Class<D> childType,
                               String[] parentProperties,
                               String[] childProperties) {
        super(beanType);
        this.childType = childType;
        this.parentProperties = parentProperties;
        this.childProperties = childProperties;
        this.selectionModel = new ContentTableListSelectionModel();
    }

    public static <E extends IdDto, D extends IdDto> ContentTableUIModel<E, D> newModel(ObserveContentTableUI<E, D> ui) {

        String uiName = ui.getClass().getName();
        String modelName = uiName + "Model";

        try {

            Class<ContentTableUIModel<E, D>> modelClass = (Class<ContentTableUIModel<E, D>>) Class.forName(modelName);
            return ConstructorUtils.invokeConstructor(modelClass, ui);

        } catch (Exception e) {
            throw new ObserveSwingTechnicalException("Could not create model for ui: " + ui, e);
        }

    }

    public void initModel(ObserveContentTableUI<E, D> ui, List<ContentTableMeta<D>> metas) {

        this.tableModel = createTableModel(ui, metas);

    }

    protected abstract ContentTableModel<E, D> createTableModel(ObserveContentTableUI<E, D> ui, List<ContentTableMeta<D>> metas) ;

    public Class<D> getChildType() {
        return childType;
    }

    public String[] getParentProperties() {
        return parentProperties;
    }

    public String[] getChildProperties() {
        return childProperties;
    }

    public ContentTableModel<E, D> getTableModel() {
        return tableModel;
    }

    public ListSelectionModel getSelectionModel() {
        return selectionModel;
    }

    /** @return le bean d'une ligne du tableau actuellement sélectionné */
    public D getTableEditBean() {
        if (tableEditBean == null) {
            try {
                tableEditBean = newTableEditBean();
            } catch (Exception e) {

                // ne devrait jamais arrive...
                if (log.isErrorEnabled()) {
                    log.error(e);
                }
            }
        }
        return tableEditBean;
    }

    public final D newTableEditBean() {
        try {

            return getChildType().getConstructor().newInstance();

        } catch (Exception ex) {
            throw new RuntimeException(ex);

        }
    }

    public boolean isCanSaveRow() {
        return canSaveRow;
    }

    public void setCanSaveRow(boolean canSaveRow) {
        boolean oldValue = this.canSaveRow;
        this.canSaveRow = canSaveRow;
        firePropertyChange(PROPERTY_CAN_SAVE_ROW, oldValue, canSaveRow);
    }

    public boolean isCanResetRow() {
        return canResetRow;
    }

    public void setCanResetRow(boolean canResetRow) {
        boolean oldValue = this.canResetRow;
        this.canResetRow = canResetRow;
        firePropertyChange(PROPERTY_CAN_RESET_ROW, oldValue, canResetRow);
    }

    public boolean isRowSaved() {
        return rowSaved;
    }

    public void setRowSaved(boolean rowSaved) {
        boolean oldValue = this.rowSaved;
        this.rowSaved = rowSaved;
        firePropertyChange(PROPERTY_ROW_SAVED, oldValue, rowSaved);
    }

    public boolean isShowData() {
        return showData;
    }

    public void setShowData(boolean showData) {
        boolean oldValue = this.showData;
        this.showData = showData;
        firePropertyChange(PROPERTY_SHOW_DATA, oldValue, showData);
    }

    protected BinderModelBuilder<D, D> prepareChildLoador(String binderName) {

        String[] properties = getChildProperties();

        BinderModelBuilder<D, D> builder = BinderModelBuilder.newEmptyBuilder(childType);
        builder.addSimpleProperties(properties);

        return builder;

    }

    /**
     * Le modèle de sélection pour un tableau.
     *
     * Le modèle permet le blocage du changement de ligne selectionnee dans le
     * tableau lorsque les conditions suivantes sont remplies :
     *
     * - le tableau est editable - une entrée etait precedemment selectionne -
     * l'entrée en cours d'édition est modifié
     *
     * Si on ne peut pas explicitement changer de ligne d'édition, on demande alors
     * à l'utilisateur s'il veut sauver l'entrée en cours d'édition (si elle est
     * valide), ou bien la perte des données (si elle n'est pas valide).
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 1.0
     */
    public class ContentTableListSelectionModel extends DefaultListSelectionModel {

        private static final long serialVersionUID = 1L;

        public ContentTableListSelectionModel() {
        }

        @Override
        public void setSelectionInterval(int index0, int index1) {
            if (log.isTraceEnabled()) {
                log.trace(index0 + " - " + index1);
            }

            ContentTableModel<?, ?> model = getTableModel();

            boolean canContinue = canContinue(model, index0);

            if (log.isDebugEnabled()) {
                log.debug("can continue  for row " + index0 + " : " + canContinue);
            }

            if (canContinue) {
                // on peut aller sur la nouvelle ligne demandée
                super.setSelectionInterval(index0, index1);
            }
        }

        protected boolean canContinue(ContentTableModel<?, ?> model, int newIndex) {
            boolean canContinue;
            if (model == null) {
                // le model n'est pas encore disponible
                return false;
            }

            if (!model.isEditable()) {
                // le modèle n'est pas editable
                // donc aucun controle a effectuer
                // on peut change de selection
                return true;
            }

            if (model.isValueAdjusting()) {
                // le modele a demande un ajustement
                // donc rien a valider
                return true;
            }

            // le modèle est editable

            int editingRow = model.getSelectedRow();

            if (isSelectionEmpty() || editingRow == -1) {
                // pas de precedente selection
                // ou pas de ligne en cours d'edition
                // on peut donc selectionner cette nouvelle ligne
                return true;
            }

            // le modele a une ligne en cours d'edition

            if (editingRow == newIndex) {
                // on veut aller sur la ligne en cours d'edition
                // aucune verification a faire
                // ce cas peut arriver lors d'un adjustement de ligne
                return true;
            }

            // une ligne en cours d'edition est différente de celle qu'on
            // veut attendre on demande au modèle si on peut quitter cette ligne
            canContinue = model.isCanQuitEditingRow();

            return canContinue;
        }

    }

}
