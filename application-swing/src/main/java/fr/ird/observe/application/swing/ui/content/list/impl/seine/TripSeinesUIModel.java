package fr.ird.observe.application.swing.ui.content.list.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.application.swing.ui.content.list.ContentListUIModel;

import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class TripSeinesUIModel extends ContentListUIModel<ProgramDto, TripSeineDto> {

    private static final long serialVersionUID = 1L;

    public TripSeinesUIModel() {
        super(ProgramDto.class, TripSeineDto.class);
    }

    @Override
    public void setData(List<DataReference<TripSeineDto>> data) {
        super.setData(data);
        setSelectedDatas(null);
    }

}
