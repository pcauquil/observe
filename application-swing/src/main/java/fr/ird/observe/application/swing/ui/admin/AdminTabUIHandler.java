/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.admin.config.SelectDataUI;
import fr.ird.observe.application.swing.ui.admin.resume.ShowResumeUI;
import fr.ird.observe.application.swing.ui.storage.tabs.DataSelectionModel;
import fr.ird.observe.application.swing.ui.tree.DataSelectionTreeSelectionModel;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.trip.DeleteTripResult;
import fr.ird.observe.services.service.trip.ExportTripResult;
import fr.ird.observe.services.service.trip.ImportTripResult;
import jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.Callable;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class AdminTabUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(AdminTabUIHandler.class);

    protected final AdminTabUI ui;

    protected final AdminUIModel model;

    protected AdminUI parentUI;

    /** Service de decoration. */
    private DecoratorService decoratorService;

    public AdminTabUIHandler(AdminTabUI ui) {
        this.ui = ui;
        model = ui.getModel();
    }

    public AdminTabUI getUi() {
        return ui;
    }

    public AdminUIModel getModel() {
        return model;
    }

    public final DecoratorService getDecoratorService() {
        if (decoratorService == null) {
            decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        }
        return decoratorService;
    }

    public final WizardState onCancel(Exception e) {
        if (log.isDebugEnabled()) {
            log.debug(this, e);
        }
        sendMessage(t("observe.actions.operation.message.canceled"));
        return WizardState.CANCELED;
    }

    public final WizardState onError(Exception e) {
        model.getStepModel(model.getOperation()).setError(e);
        if (log.isErrorEnabled()) {
            log.error(e.getMessage(), e);
        }
        sendMessage(t("observe.actions.operation.message.failed"));
        return WizardState.FAILED;
    }

    public void initTabUI(AdminUI ui, AdminTabUI tabUI) {

        parentUI = ui;
        AdminStep step = tabUI.getStep();

        tabUI.addPropertyChangeListener(tabUI);

        if (log.isDebugEnabled()) {
            log.debug("common for [" + step + "] for main ui " + ui.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        String stepLabel = I18nEnumHelper.getLabel(step);
        tabUI.getRUNNING_label().setText(t("observe.actions.operation.message.running", stepLabel));
        tabUI.SUCCESSED_label.setText(t("observe.actions.operation.message.successed", stepLabel));
        tabUI.NEED_FIX_label.setText(t("observe.actions.operation.message.needFix", stepLabel));
        tabUI.CANCELED_label.setText(t("observe.actions.operation.message.canceled", stepLabel));
        tabUI.FAILED_label.setText(t("observe.actions.operation.message.failed", stepLabel));
        tabUI.progression.setVisible(true);
        tabUI.progressionTop.setVisible(true);
    }

    public void updateState(AdminTabUI ui, WizardState newState) {
        if (newState == null) {
            newState = WizardState.PENDING;
        }
        ui.getContentLayout().show(ui.content, newState.name());
        switch (newState) {
            case PENDING:
                ui.description.setText(t(ui.getStep().getDescription()));
                return;
            case RUNNING:
                ui.RUNNING_progressionPane.getViewport().setView(ui.progression);
                ui.RUNNING_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
            case SUCCESSED:
                ui.SUCCESSED_progressionPane.getViewport().setView(ui.progression);
                ui.SUCCESSED_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
            case FAILED:
                Exception e = ui.getStepModel().getError();
                StringWriter w = new StringWriter();
                e.printStackTrace(new PrintWriter(w));
                ui.addMessage(ui.getStep(), w.toString());
                ui.FAILED_progressionPane.getViewport().setView(ui.progression);
                ui.FAILED_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
            case NEED_FIX:
                break;
            case CANCELED:
                ui.CANCELED_progressionPane.getViewport().setView(ui.progression);
                ui.CANCELED_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
        }
    }

    public void sendMessage(String message) {

        AdminStep step = ui.getStep();
        ui.addMessage(step, message);
        ShowResumeUI resumeUI = (ShowResumeUI) parentUI.getStepUI(AdminStep.SHOW_RESUME);
        if (resumeUI != null) {
            resumeUI.addMessage(step, message);
        }
    }

    public String getProgressString(int currentStep, int nbStep) {
        String txt = "";
        AdminStep step = ui.getStep();
        if (step != null) {
            txt = n("observe.storage.step.label");
            txt = t(txt, currentStep + 1, nbStep, I18nEnumHelper.getLabel(step));
        }
        return txt;
    }

    public void updateSelectionModel(SelectDataUI tabUI) {

        JTree selectTree = tabUI.getSelectTree();
        DataSelectionModel selectDataModel = tabUI.getSelectDataModel();
        DataSelectionTreeSelectionModel selectionModel = tabUI.getSelectionModel();
        ObserveTreeHelper helper = tabUI.getTreeHelper();
        ObserveSwingDataSource source = tabUI.getModel().getSafeLocalSource(true);

        updateSelectionModel(tabUI, helper, selectTree, source, selectDataModel, selectionModel);

    }

    protected void checkStepIsOperation(AdminTabUI ui) {
        if (!ui.getStep().isOperation()) {
            throw new IllegalStateException("can not launch objectOperation on none operation step " + ui.getStep());
        }
    }

    protected ObserveSwingDataSource openSource(ObserveSwingDataSource service) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        if (!service.isOpen()) {
            service.open();
        }
        return service;
    }

    public void addAdminWorker(String label, Callable<WizardState> callable) {
        AdminActionWorker worker = AdminActionWorker.newWorker(this, label, callable);
        ObserveRunner.getActionExecutor().addAction(worker);
    }

    protected void updateSelectionModel(AdminTabUI tabUI,
                                        ObserveTreeHelper helper,
                                        JTree selectTree,
                                        ObserveSwingDataSource source,
                                        DataSelectionModel selectDataModel,
                                        DataSelectionTreeSelectionModel selectionModel) {


        if (log.isDebugEnabled()) {
            log.debug("reload model " + selectDataModel);
        }

        selectionModel.clearSelection();

        if (log.isDebugEnabled()) {
            log.debug("Will treeHelper : " + helper);
            log.debug("selection model : " + selectDataModel);
            log.debug("use referentiel : " + selectDataModel.isUseReferentiel());
            log.debug("use data        : " + selectDataModel.isUseData());
        }

        helper.setUI(selectTree, false);

        TreeModel model = helper.createModel(tabUI, selectDataModel, source);
        selectTree.setModel(model);

        selectionModel.initUI(selectTree);
    }

    public void addMessage(AdminStep step, String text) {
        ui.getProgression().append(text + "\n");
    }

       protected void logExportResult(String i18nKey,
                                   ExportTripResult tripResult,
                                   ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                   ReferentialReference<ProgramDto> program,
                                   DataReference trip) {

        String message = sendLogResultMessage(i18nKey, programDecorator, program, trip, tripResult.getTime());
        if (log.isInfoEnabled()) {
            log.info(message);
        }

    }


    protected void logImportResult(String importI18nKey,
                                   String deleteI18nKey,
                                   ImportTripResult tripResult,
                                   ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                   ReferentialReference<ProgramDto> program,
                                   DataReference trip) {

        DecoratorService decoratorService = getDecoratorService();

        String programStr = programDecorator.toString(program);

        String tripStr = decoratorService.getTripReferenceDecorator(trip).toString(trip);

        if (tripResult.isDeleted()) {

            String message = sendLogResultMessage(deleteI18nKey, programDecorator, program, trip, tripResult.getDeleteTime());
            if (log.isInfoEnabled()) {
                log.info(message);
            }

        }

        if (tripResult.isImported()) {

            String message = sendLogResultMessage(importI18nKey, programDecorator, program, trip, tripResult.getImportTime());
            if (log.isInfoEnabled()) {
                log.info(message);
            }

        }

    }

    protected void logDeleteResult(String deleteI18nKey,
                                   DeleteTripResult tripResult,
                                   ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                   ReferentialReference<ProgramDto> program,
                                   DataReference trip) {

        String message = sendLogResultMessage(deleteI18nKey, programDecorator, program, trip, tripResult.getTime());
        if (log.isInfoEnabled()) {
            log.info(message);
        }

    }

    protected String sendLogResultMessage(String i18nKey,
                                          ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                          ReferentialReference<ProgramDto> program,
                                          DataReference trip,
                                          long time) {

        DecoratorService decoratorService = getDecoratorService();
        String programStr = programDecorator.toString(program);
        String tripStr = decoratorService.getTripReferenceDecorator(trip).toString(trip);
        String timeStr = StringUtil.convertTime(time);
        String message = StringUtils.leftPad(timeStr, 20) + " - " + t(i18nKey, programStr, tripStr);
        sendMessage(message);
        return message;
    }

}
