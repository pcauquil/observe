/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.export;

import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.referential.ReferentialReference;

import javax.swing.table.AbstractTableModel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Le modèle pour la tableau dans l'export GPS qui contient les activités et les
 * points gps calculés via le fichier gps importé.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TripToExportTableModel extends AbstractTableModel {

    protected static final Class<?>[] COLUMN_CLASSES = {
            Boolean.class,
            ReferentialReference.class,  //program
            DataReference.class,  // trip
            Boolean.class
    };

    private static final long serialVersionUID = 1L;

    protected TripEntry[] data;

    protected final Set<Integer> selected;

    protected boolean selectAll;

    public TripToExportTableModel() {
        selected = new HashSet<>();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // seule la première colonne est éditable (selection des marées à
        // exporter)
        return columnIndex == 0;
    }

    public int[] getSelected() {
        int[] result = new int[selected.size()];
        int i = 0;
        for (Integer index : selected) {
            result[i++] = index;
        }
        return result;
    }

    public boolean hasSelection() {
        return !selected.isEmpty();
    }

    /**
     * Initialise le modèle.
     *
     * @param data les entrées du modèles (program-maree-existe à distance)
     */
    public void init(List<TripEntry> data) {

        this.data = data.toArray(new TripEntry[data.size()]);

        selected.clear();
        // par defaut, on selectionne toutes les references
        setSelectAll(true);
    }

    public void clear() {
        data = null;
    }

    @Override
    public int getRowCount() {
        return data == null ? 0 : data.length;
    }

    @Override
    public int getColumnCount() {
        return COLUMN_CLASSES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        TripEntry tripEntry = data == null ? null : data[rowIndex];
        if (tripEntry != null) {
            switch (columnIndex) {
                case 0:
                    value = selected.contains(rowIndex);
                    break;
                case 1:
                    value = tripEntry.getProgram();
                    break;
                case 2:
                    value = tripEntry.getTrip();
                    break;
                case 3:
                    value = tripEntry.isExist();
                    break;
                default:
                    throw new IllegalStateException("can not get value for row " + rowIndex + ", col " + columnIndex);
            }
        }
        return value;
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
        if (selectAll) {
            for (int i = 0, max = getRowCount(); i < max; i++) {
                selected.add(i);
            }
        } else {
            selected.clear();
        }
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            Boolean value = (Boolean) aValue;
            if (value) {
                selected.add(rowIndex);
                if (selected.size() == getRowCount()) {
                    selectAll = true;
                }
            } else {
                selected.remove(rowIndex);
                if (selected.isEmpty()) {
                    selectAll = false;
                }
            }
            fireTableCellUpdated(rowIndex, columnIndex);
        }

        // no edit for others columns
    }

}
