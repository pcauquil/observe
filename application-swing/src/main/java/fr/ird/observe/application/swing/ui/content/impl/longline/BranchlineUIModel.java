package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;

import java.util.Set;

/**
 * Created on 12/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class BranchlineUIModel extends ContentUIModel<BranchlineDto> {

    public static final String PROPERTY_SAVED = "saved";

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_HOOK_AND_BAIT_TAB_VALID = "hookAndBaitTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(BranchlineDto.PROPERTY_TOP_TYPE,
                                               BranchlineDto.PROPERTY_TRACELINE_TYPE,
                                               BranchlineDto.PROPERTY_DEPTH_RECORDER,
                                               BranchlineDto.PROPERTY_HOOK_LOST,
                                               BranchlineDto.PROPERTY_TRACE_CUT_OFF,
                                               BranchlineDto.PROPERTY_TIMER,
                                               BranchlineDto.PROPERTY_TIME_SINCE_CONTACT,
                                               BranchlineDto.PROPERTY_TIMER_TIME_ON_BOARD,
                                               BranchlineDto.PROPERTY_WEIGHTED_SNAP,
                                               BranchlineDto.PROPERTY_SNAP_WEIGHT,
                                               BranchlineDto.PROPERTY_WEIGHTED_SWIVEL,
                                               BranchlineDto.PROPERTY_SWIVEL_WEIGHT).build();

    public static final Set<String> HOOK_AND_BAIT_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(BranchlineDto.PROPERTY_HOOK_TYPE,
                                               BranchlineDto.PROPERTY_HOOK_SIZE,
                                               BranchlineDto.PROPERTY_HOOK_OFFSET,
                                               BranchlineDto.PROPERTY_BAIT_TYPE,
                                               BranchlineDto.PROPERTY_BAIT_SETTING_STATUS,
                                               BranchlineDto.PROPERTY_BAIT_HAULING_STATUS).build();

    private BranchlineDto branchlineDto;

    private boolean generalTabValid;

    private boolean hookAndBaitTabValid;

    public BranchlineUIModel() {
        super(BranchlineDto.class);
    }

    public BranchlineDto getBranchline() {
        return branchlineDto;
    }

    public void setBranchline(BranchlineDto branchlineDto) {
        this.branchlineDto = branchlineDto;
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, null, generalTabValid);
    }

    public boolean isHookAndBaitTabValid() {
        return hookAndBaitTabValid;
    }

    public void setHookAndBaitTabValid(boolean hookAndBaitTabValid) {
        this.hookAndBaitTabValid = hookAndBaitTabValid;
        firePropertyChange(PROPERTY_HOOK_AND_BAIT_TAB_VALID, null, hookAndBaitTabValid);
    }

    // For external model known when a saved action was done
    public void fireSaved() {
        firePropertyChange(PROPERTY_SAVED, null, true);
    }

}
