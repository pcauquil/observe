package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.services.dto.longline.BasketDto;
import fr.ird.observe.services.dto.longline.BranchlineDto;
import fr.ird.observe.services.dto.longline.SectionDto;
import fr.ird.observe.services.dto.longline.SectionWithTemplate;
import fr.ird.observe.application.swing.ui.util.ObserveSwingValidatorMessageTableModel;
import jaxx.runtime.validator.swing.SwingValidator;
import jaxx.runtime.validator.swing.SwingValidatorMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;

import javax.swing.JComponent;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/18/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class LonglineDetailCompositionUIValidationHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LonglineDetailCompositionUIValidationHelper.class);

    private final LonglineDetailCompositionUIModel model;

    private final ObserveSwingValidatorMessageTableModel errorTableModel;

    private boolean objectValueAdjusting;

    private final LonglineDetailCompositionValidatorService validatorService;

    private final Set<SwingValidator> validators;

    public LonglineDetailCompositionUIValidationHelper(LonglineDetailCompositionUI ui, DecoratorService decoratorService) {
        this.model = ui.getModel();
        this.errorTableModel = (ObserveSwingValidatorMessageTableModel) ui.getErrorTableModel();
        SwingValidator<SectionDto> sectionValidator = ui.getSectionValidator();
        SwingValidator<BasketDto> basketValidator = ui.getBasketValidator();
        SwingValidator<BranchlineDto> branchlineValidator = ui.getBranchlineValidator();

        this.validators = ImmutableSet.<SwingValidator>builder().add(
                sectionValidator,
                basketValidator,
                branchlineValidator
        ).build();
        Map<SwingValidator, JComponent> validatorEditor = ImmutableMap.<SwingValidator, JComponent>builder()
                .put(sectionValidator, ui.getSectionsPane())
                .put(basketValidator, ui.getBasketsPane())
                .put(branchlineValidator, ui.getBranchlinesPane())
                .build();

        this.validatorService = new LonglineDetailCompositionValidatorService(
                sectionValidator,
                basketValidator,
                branchlineValidator,
                validatorEditor,
                decoratorService
        );
    }

    public void whenSectionChanged() {

        if (!objectValueAdjusting) {

            if (log.isInfoEnabled()) {
                log.info("Rebuild messages, section model changes.");
            }

            List<SectionWithTemplate> notEmptyData = model.getSectionsTableModel().getNotEmptyData();
            List<SwingValidatorMessage> messages = validatorService.validateSections(notEmptyData);

            removeOldMessages();

            errorTableModel.addMessages(messages);

            model.setCompositionTabValid(messages.isEmpty());

        }
    }

    public void whenBasketChanged() {

        if (!objectValueAdjusting) {

            if (log.isInfoEnabled()) {
                log.info("Rebuild messages, basket model changes.");
            }
            List<SectionWithTemplate> notEmptyData = model.getSectionsTableModel().getNotEmptyData();
            List<SwingValidatorMessage> messages = validatorService.validateSections(notEmptyData);

            removeOldMessages();

            errorTableModel.addMessages(messages);

            model.setCompositionTabValid(messages.isEmpty());

        }

    }

    public void whenBranchlineChanged() {

        if (!objectValueAdjusting) {

            if (log.isInfoEnabled()) {
                log.info("Rebuild messages, branchline model changes.");
            }

            List<SectionWithTemplate> notEmptyData = model.getSectionsTableModel().getNotEmptyData();
            List<SwingValidatorMessage> messages = validatorService.validateSections(notEmptyData);

            removeOldMessages();

            errorTableModel.addMessages(messages);

            model.setCompositionTabValid(messages.isEmpty());

        }

    }

    public void removeOldMessages() {
        errorTableModel.removeMessages(input -> {
            SimpleBeanValidator<?> validator = input.getValidator();
            return validators.contains(validator);
        });
    }

    public void setObjectValueAdjusting(boolean objectValueAdjusting) {
        this.objectValueAdjusting = objectValueAdjusting;
    }

}
