package fr.ird.observe.application.swing.ui.tree.menu;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.IdDtos;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.ui.tree.ObserveNode;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeHelper;
import fr.ird.observe.application.swing.ui.tree.TripLonglineNode;
import fr.ird.observe.application.swing.ui.tree.actions.ChangeActivityTripActionListener;
import fr.ird.observe.application.swing.ui.tree.actions.NodeChangeActionListener;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveActivityLonglineNodeMenuPopulator extends MoveNodeMenuPopulator {

    @Override
    public NodeChangeActionListener createChangeActionListener(ObserveTreeHelper treeHelper,
                                                               ObserveSwingDataSource dataSource,
                                                               String id,
                                                               String parentId) {
        return new ChangeActivityTripActionListener(treeHelper, dataSource, id, parentId);
    }

    @Override
    public List<DecoratedNodeEntity> getPossibleParentNodes(ObserveNode activityLonglineNode, ObserveTreeHelper treeHelper) {

        // noeud de marée parent
        ObserveNode parentNode = activityLonglineNode.getParent().getParent();

        // noeud de route de la marée sans le parent actuel
        List<DecoratedNodeEntity> possibleParents = new ArrayList<>();

        // noeud du programme
        ObserveNode programNode = parentNode.getParent();

        DecoratorService decoratorService = treeHelper.getTreeCellRenderer().getDecoratorService();
        DataReferenceDecorator<TripLonglineDto> tripDecorator = decoratorService.getDataReferenceDecorator(TripLonglineDto.class);

        for (int i = 0, n = programNode.getChildCount(); i < n; i++) {

            TripLonglineNode tripNode = (TripLonglineNode) programNode.getChildAt(i);
            String tripId = tripNode.getId();

            // si le noeud de marée n'est pas le même que le parent actuel
            // si le noeud est bien un noeud de marée longline
            if (!parentNode.equals(tripNode) && IdDtos.isTripLonglineId(tripId)) {

                possibleParents.add(DecoratedNodeEntity.newDecoratedNodeEntity(tripNode, tripDecorator));

            }
        }

        return possibleParents;
    }
}
