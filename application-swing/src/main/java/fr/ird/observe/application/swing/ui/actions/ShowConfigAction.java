package fr.ird.observe.application.swing.ui.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveMainUIHandler;
import fr.ird.observe.application.swing.ui.ObserveUICallback;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.config.ConfigUIHelper;
import jaxx.runtime.swing.config.model.ConfigUIModelBuilder;
import jaxx.runtime.swing.config.model.MainCallBackFinalizer;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.MultiJXPathDecorator;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JTable;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.EventObject;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ShowConfigAction extends AbstractAction {

    private static final long serialVersionUID = 3038774900992805790L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ShowConfigAction.class);

    private final ObserveMainUI ui;

    public ShowConfigAction(ObserveMainUI ui) {

        super(t("observe.action.configuration"), SwingUtil.getUIManagerActionIcon("config"));
        this.ui = ui;
        putValue(SHORT_DESCRIPTION, t("observe.action.configuration.tip"));
        putValue(MNEMONIC_KEY, (int) 'C');

    }

    @Override
    public void actionPerformed(ActionEvent event) {


        if (log.isInfoEnabled()) {
            log.info("ObServe opening configuration ui...");
        }
        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {

            ObserveSwingDataSource dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();

            ObserveSwingApplicationConfig config = ui.getConfig();

            ObserveConfigUIBuilder helper;

            helper = buildUI(config, dataSource);

            helper.buildUI(ui, "observe.config.category.directories");

            helper.displayUI(ui, false);

        }

    }

    protected ObserveConfigUIBuilder buildUI(ObserveSwingApplicationConfig config, ObserveSwingDataSource dataSource) {

        SpeciesListTableCellEditor editor = null;
        SpeciesListsTableCellRenderer renderer = null;

        if (dataSource != null) {

            Set<ReferentialReference<SpeciesListDto>> speciesLists = dataSource.getReferentialReferences(SpeciesListDto.class);

            Map<String, ReferentialReference<SpeciesListDto>> speciesListMap = ReferentialReference.splitById(speciesLists);

            ReferentialReferenceDecorator<SpeciesListDto> referenceDecorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(SpeciesListDto.class);

            editor = new SpeciesListTableCellEditor(speciesLists, speciesListMap, referenceDecorator);
            renderer = new SpeciesListsTableCellRenderer(speciesListMap, referenceDecorator);

        }

        ObserveConfigUIBuilder helper = new ObserveConfigUIBuilder(config, editor, renderer);


        for (ObserveUICallback callback : ObserveUICallback.values()) {
            helper.registerCallBack(callback);
        }

        helper.setFinalizer(new MainCallBackFinalizer(ObserveUICallback.application.name()));

        helper.setCloseAction(ObserveMainUIHandler::restartEdit);

        // categorie repertoires
        addDirectoriesOptions(helper);

        // categorie h2
        addH2Options(helper);

        // categorie obtuna
        addObstunaOptions(helper);

        // categorie change storage
        addChangeStorageOptions(helper);

        // categorie gps
        addGpsOptions(helper);

        // categorie synchro
        addSynchroOptions(helper);

        // categorie observations (seine)
        addObservationsOptions(helper);

        if (dataSource != null) {

            // categorie speciesList (seine)
            addSeineSpeciesListOptions(helper);

            // categorie speciesList (longline)
            addLonglineSpeciesListOptions(helper);

        }

        // catégories map
        addMapOptions(helper);

        // others
        addOthersOptions(helper);

        return helper;

    }

    protected void addDirectoriesOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(
                n("observe.config.category.directories"),
                n("observe.config.category.directories.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.CONFIG_FILE);
        helper.addOption(ObserveSwingApplicationConfigOption.DATA_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.DB_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.BACKUP_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.TMP_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.VALIDATION_REPORT_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.RESOURCES_DIRECTORY, ObserveUICallback.application);

    }

    protected void addH2Options(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.h2"),
                           n("observe.config.category.h2.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.H2_LOGIN);
        helper.addOption(ObserveSwingApplicationConfigOption.H2_PASSWORD);
        helper.addOption(ObserveSwingApplicationConfigOption.H2_CAN_MIGRATE);
        helper.addOption(ObserveSwingApplicationConfigOption.H2_SERVER_PORT);

    }

    protected void addObstunaOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.obstuna"),
                           n("observe.config.category.obstuna.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.OBSTUNA_URL);
        helper.addOption(ObserveSwingApplicationConfigOption.OBSTUNA_LOGIN);
        helper.addOption(ObserveSwingApplicationConfigOption.OBSTUNA_USE_SSL_CERT);
//        helper.addOption(ObserveSwingApplicationConfigOption.OBSTUNA_SSL_CERTIFICAT_FILE);

        helper.addOption(ObserveSwingApplicationConfigOption.OBSTUNA_CAN_MIGRATE, ObserveUICallback.db);

    }

    protected void addChangeStorageOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(
                n("observe.config.category.changeStorage"),
                n("observe.config.category.changeStorage.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.DEFAULT_DB_MODE);
        helper.addOption(ObserveSwingApplicationConfigOption.DEFAULT_CREATION_MODE);
        helper.addOption(ObserveSwingApplicationConfigOption.STORE_REMOTE_STORAGE);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_PROGRESSION);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_SQL);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_SQL, ObserveUICallback.application);

    }

    protected void addGpsOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.gps"),
                           n("observe.config.category.gps.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.DEFAULT_GPS_MAX_DELAY);
        helper.addOption(ObserveSwingApplicationConfigOption.DEFAULT_GPS_MAX_SPEED);

    }

    protected void addSynchroOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.synchro"),
                           n("observe.config.category.synchro.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.CHANGE_SYNCHRO_SRC);
        helper.addOption(ObserveSwingApplicationConfigOption.H2_CAN_MIGRATE);

    }

    protected void addObservationsOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(
                n("observe.config.category.observation"),
                n("observe.config.category.observation.description"),
                ObserveUICallback.ui.name());

        helper.addOption(ObserveSwingApplicationConfigOption.DETAILLED_ACTIVITIES_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.NON_TARGET_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.BAIT_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.MAMMALS_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.SAMPLES_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.OBJECTS_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.BIRDS_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.TARGET_DISCARDS_OBSERVATION);

    }

    protected void addSeineSpeciesListOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(
                n("observe.config.category.speciesList.seine"),
                n("observe.config.category.speciesList.seine.description"),
                ObserveUICallback.ui.name());

        helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_TARGET_CATCH_ID);
        helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID);
        helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID);
        helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID);
        helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID);

    }

    protected void addLonglineSpeciesListOptions(ObserveConfigUIBuilder helper) {


        helper.addCategory(
                n("observe.config.category.speciesList.longline"),
                n("observe.config.category.speciesList.longline.description"),
                ObserveUICallback.ui.name());

        helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_CATCH_ID);
        helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_ENCOUNTER_ID);

    }

    protected void addMapOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(
                n("observe.config.category.map"),
                n("observe.config.category.map.description"),
                ObserveUICallback.ui.name());

        helper.addOption(ObserveSwingApplicationConfigOption.MAP_BACKGROUND_COLOR);
        for (ObserveSwingApplicationConfigOption layerOption : ObserveSwingApplicationConfigOption.MAP_LAYERS) {
            helper.addOption(layerOption);
        }
        helper.addOption(ObserveSwingApplicationConfigOption.MAP_STYLE_FILE);

    }

    protected void addOthersOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.other"),
                           n("observe.config.category.other.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_NUMBER_EDITOR_BUTTON);
        helper.addOption(ObserveSwingApplicationConfigOption.AUTO_POPUP_NUMBER_EDITOR);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_DATE_TIME_EDITOR_SLIDER);
        helper.addOption(ObserveSwingApplicationConfigOption.LOAD_LOCAL_STORAGE, ObserveUICallback.application);
//        helper.addOption(ObserveSwingApplicationConfigOption.FULL_SCREEN, ObserveUICallback.ui);
        helper.addOption(ObserveSwingApplicationConfigOption.LOCALE, ObserveUICallback.ui);
        helper.addOption(ObserveSwingApplicationConfigOption.DB_LOCALE, ObserveUICallback.ui);

    }

    protected class ObserveConfigUIBuilder extends ConfigUIHelper {

        private final SpeciesListTableCellEditor speciesListTableCellEditor;

        private final SpeciesListsTableCellRenderer speciesListsTableCellRenderer;

        protected ObserveConfigUIBuilder(ObserveSwingApplicationConfig config,
                                         SpeciesListTableCellEditor speciesListTableCellEditor,
                                         SpeciesListsTableCellRenderer speciesListsTableCellRenderer) {
            super(config);
            this.speciesListTableCellEditor = speciesListTableCellEditor;
            this.speciesListsTableCellRenderer = speciesListsTableCellRenderer;
        }

        @Override
        public ConfigUIModelBuilder addOption(ConfigOptionDef option) {

            // add the option
            super.addOption(option);

            String beanProperty = ((ObserveSwingApplicationConfigOption) option).getPropertyKey();

            if (StringUtils.isNotEmpty(beanProperty)) {
                setOptionPropertyName(beanProperty);
            }

            return modelBuilder;

        }

        public ConfigUIModelBuilder addOption(ObserveSwingApplicationConfigOption option, ObserveUICallback callBack) {

            addOption(option);
            setOptionCallBack(callBack.name());

            return modelBuilder;

        }

        public ConfigUIModelBuilder registerCallBack(ObserveUICallback callback) {

            String name = callback.name();
            String description = callback.getLabel();
            Icon icon = callback.getIcon();
            registerCallBack(name, description, icon, callback);

            return modelBuilder;

        }

        protected ConfigUIModelBuilder addSpeciesListOption(ObserveSwingApplicationConfigOption option) {

            addOption(option)
                    .setOptionPropertyName(option.getPropertyKey())
                    .setOptionEditor(speciesListTableCellEditor)
                    .setOptionRenderer(speciesListsTableCellRenderer);

            return modelBuilder;

        }

    }

    protected class SpeciesListsTableCellRenderer extends DefaultTableRenderer {

        private static final long serialVersionUID = 1L;

        private final Map<String, ReferentialReference<SpeciesListDto>> entityMap;

        private final Decorator<ReferentialReference<SpeciesListDto>> decorator;

        public SpeciesListsTableCellRenderer(Map<String, ReferentialReference<SpeciesListDto>> entityMap, Decorator<ReferentialReference<SpeciesListDto>> decorator) {
            this.entityMap = entityMap;
            this.decorator = decorator;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {

            String speciesListId = String.valueOf(value);
            ReferentialReference<SpeciesListDto> speciesList = entityMap.get(speciesListId);
            return super.getTableCellRendererComponent(table, decorator.toString(speciesList), isSelected, hasFocus, row, column);

        }
    }

    protected class SpeciesListTableCellEditor extends DefaultCellEditor {

        private static final long serialVersionUID = 1L;

        private final Map<String, ReferentialReference<SpeciesListDto>> entityMap;

        private final ReferentialReferenceDecorator<SpeciesListDto> decorator;

        protected SpeciesListTableCellEditor(Collection<ReferentialReference<SpeciesListDto>> entities,
                                             Map<String, ReferentialReference<SpeciesListDto>> entityMap,
                                             ReferentialReferenceDecorator<SpeciesListDto> decorator) {

            super(new JComboBox());

            this.entityMap = entityMap;
            this.decorator = decorator;

            final BeanFilterableComboBox<ReferentialReference<SpeciesListDto>> component = new BeanFilterableComboBox<>();
            component.setI18nPrefix("observe.common.");
            component.setShowReset(true);
            component.setBeanType((Class) ReferentialReference.class);

            setClickCountToStart(1);

            editorComponent = component;
            delegate = new DefaultCellEditor.EditorDelegate() {

                private static final long serialVersionUID = 1L;

                @Override
                public void setValue(Object value) {
                    if (value != null && String.class.isInstance(value)) {

                        value = SpeciesListTableCellEditor.this.entityMap.get(value);
                    }
                    component.setSelectedItem(value);
                }

                @Override
                public Object getCellEditorValue() {
                    String result = null;
                    Object selectedItem = component.getSelectedItem();
                    if (selectedItem != null && ReferentialReference.class.isInstance(selectedItem)) {
                        ReferentialReference reference = (ReferentialReference) selectedItem;
                        result = reference.getId();
                    }
                    return result;
                }

                @Override
                public boolean shouldSelectCell(EventObject anEvent) {
                    if (anEvent instanceof MouseEvent) {
                        MouseEvent e = (MouseEvent) anEvent;
                        return e.getID() != MouseEvent.MOUSE_DRAGGED;
                    }
                    return true;
                }

                @Override
                public boolean stopCellEditing() {
                    if (component.isEditable()) {
                        // Commit edited value.
                        component.getCombobox().actionPerformed(
                                new ActionEvent(SpeciesListTableCellEditor.this, 0, ""));
                    }
                    return super.stopCellEditing();
                }
            };

            component.init((MultiJXPathDecorator) this.decorator, Lists.newArrayList(entities));
        }

    }
}
