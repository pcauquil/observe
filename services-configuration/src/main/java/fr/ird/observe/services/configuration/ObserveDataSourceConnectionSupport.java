package fr.ird.observe.services.configuration;

/*
 * #%L
 * ObServe :: Services Configuration API
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import org.nuiton.version.Version;

import java.util.Objects;

/**
 * Created on 04/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class ObserveDataSourceConnectionSupport implements ObserveDataSourceConnection {

    private static final long serialVersionUID = 1L;

    protected final String authenticationToken;

    protected final boolean readReferential;

    protected final boolean writeReferential;

    protected final boolean readData;

    protected final boolean writeData;

    protected final Version version;

    protected ObserveDataSourceConnectionSupport(String authenticationToken,
                                                 boolean readReferential,
                                                 boolean writeReferential,
                                                 boolean readData,
                                                 boolean writeData,
                                                 Version version) {
        this.authenticationToken = authenticationToken;
        this.writeData = writeData;
        this.readReferential = readReferential;
        this.writeReferential = writeReferential;
        this.readData = readData;
        this.version = version;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    @Override
    public boolean canReadReferential() {
        return readReferential;
    }

    @Override
    public boolean canWriteReferential() {
        return writeReferential;
    }

    @Override
    public boolean canReadData() {
        return readData;
    }

    @Override
    public boolean canWriteData() {
        return writeData;
    }

    @Override
    public Version getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObserveDataSourceConnectionSupport)) return false;
        ObserveDataSourceConnectionSupport that = (ObserveDataSourceConnectionSupport) o;
        return Objects.equals(authenticationToken, that.authenticationToken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authenticationToken);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("authenticationToken", authenticationToken)
                .add("readReferential", readReferential)
                .add("writeReferential", writeReferential)
                .add("readData", readData)
                .add("writeData", writeData)
                .add("version", version)
                .toString();
    }

}
