.. -
.. * #%L
.. * ObServe
.. * %%
.. * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===============================
Installation du serveur Obstuna
===============================

:Author: Tony Chemit <chemit@codelutin.com>
:Date: 28/04/2009
:Abstract: Ce document explique comment installer la base Obtuna sur un serveur.

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2


Pré-requis
----------

- Un certain nombre d'outils et scripts sont intégérés dans ObServe, il faut au
  préalable récupérer la dernière version de l'application (mode administrateur)
  (*observe-admin-cli-XXX-bin.zip*) à l'adresse suivante :

  `downloads`_

- Un serveur Postgres doit être installé sur le serveur cible.

- Une machine possèdant une jvm ayant les droits d'accès au serveur cible.

- A partir de la version 1.7, ObServe intègre des interfaces graphiques pour
  effectuer les opérations d'administration sur les bases obstuna.

  Pour utiliser ces nouveaux outils, il faut remplir ces conditions :

  - posséder une base postgres accessible par le programme
  - un utilisateur propriétaire de la base
  - tous les autres utilisateurs créés sur le serveur postgres

Création d'une base obstuna
---------------------------

La mise en place d'une nouvelle base obstuna se fait en 3 étapes :

- création de la base vierge et des rôles sur le serveur postgres
- configuration de la base sur le serveur postgres
- création du contenu de la base via ObServe

Note::

  **Il faut de plus avoir à disposition une autre base obstuna qui contient le
  référentiel à importer dans la nouvelle base.**
  
Phase 1
_______

Pour créer une nouvelle instance d'obstuna, il faut au préalable avoir une
base vierge (sans schéma) et les utilisateurs sur le serveur postgres.

Il existe une script qui permet de faire ça en tant qu'utilisateur *postgres*.

::

  sudo su postgres

  (cd obstuna-admin/createdb ; ./create-ird_obstuna.sh)

Ce script va créer :

- le propriétaire de la base (**admin**)
- les rôles de technicien **adamiano**, **lfloch**, **pcauquil**
- les rôles de simple utilisateur **utilisateur**
- les rôles d'utilisateur de référentiel **referentiel**
- la base vierge (nommée **obstuna**)

Ce script a été conçu pour la configuration de l'ird, il ne fait qu'appeler
un autre script bash avec les bons paramètres.

::

  obstuna-admin/createdb/create-empty-obstuna.sh

Il est donc très facile d'écrire un nouveau script de configuration avec le
bon nom de la base, du propriétaire et des utilisateurs.

Par exemple pour créer une base avec la configuration suivante

- nom *obstuna-test*
- propriétaire *admin-test*
- utilisateurs *techinicien-test*, *utilisateur-test*, *referentiel-test*

on lance la commande :

::

  ( cd obstuna-admin/createdb ; ./create-empty-obstuna.sh obstuna-test admin-test "technicien-test utilisateur-test referentiel-test")

Phase 1 (base de test)
______________________

Pour créer la base de test utilisée pour tous les tests d'intégration, on lance
un autre script :

::

  sudo su postgres

  (cd obstuna-admin/createdb ; ./create-test_obstuna.sh)

Cela crée une base avec la configuration suivante :

- nom *obstuna-test*
- propriétaire *admin*
- techniciens *technicien1*, *technicien2*, *technicien3*
- utilisateurs *utilisateur*
- utilisateurs de référentiel *referentiel*

Phase 2
_______

Ajouter une ligne dans le fichier de configuration pg_hba.conf

::

  host    obstuna     all         0.0.0.0/0          md5

Pour la base de test ajouter en plus une ligne

::

  host    obstuna-test all         0.0.0.0/0          md5


Selon la configuration du serveur, il est possible de devoir aussi modifier
le fichier de configuration postgresql.conf (pour renseigner la propriété
listen_addresses='*' pour autoriser le tcp depuis l'extérieur).

Redémarrer le serveur postgres :

::

  sudo /etc/init.d/postgresql-8.3 restart

ou

::

  sudo service postgresql-8.3 restart

tester la connexion

::

  psql -h localhost obstuna admin

Phase 3
_______

Il suffit enfin de lancer l'application en mode *création de base obstuna* via
le raccourci suivant :

::

  (cd obstuna-admin ; ./create-obstuna.sh)

ou

::

  ./obstuna-admin/create-obstuna.bat

Cela va effectuer les opérations suivantes après configuration de la base cible,
de la base d'import de référentiel et des rôles de sécurité :

- supprimer toutes les tables de la base si elles existent
- créer le schéma à partir du mapping de l'ORM (hibernate)
- remplir le référentiel à partir de celui d'une autre base obstuna à jour (topia-service-migration)
- appliquer la sécurité sur les rôles de la base

Note::

  Le script appelle l'application avec l'action **--obstuna-admin create**

Mise à jour d'une base obstuna
------------------------------

Pour mettre à jour une base obstuna via le service de migration intégré dans
ObServe, il suffit de lancer l'application en mode *mise à jour obstuna* via le
raccourci suivant :

::

  (cd obstuna-admin ; ./update-obstuna.sh)

ou

::

  ./obstuna-admin/update-obstuna.bat

Ce mode va effectuer les opérations suivantes après configuration de la base
cible et des rôles de sécurité :

- mettre à jour le schéma et les données via le service de migration intégré (topia)
- appliquer la sécurité sur les rôles de la base

Note::

  Le script appelle l'application avec l'action **--obstuna-admin update**

Mise à jour de la sécurité
--------------------------

On différencie dans ObServe plusieurs types d'utilisateurs pour les connexions
distantes :

- ceux qui peuvent juste lire le référentiel sans rien modifier (les referentiels)
- ceux qui peuvent juste lire des données sans rien modifier (les lecteurs)
- ceux qui ont le droit de modifier les données et le référentiel (les techniciens et administrateurs).

Pour appliquer la sécurité sur les utilisateurs d'une base obstuna, il suffit
de lancer Observe en mode *mise à jour sécurité obstuna* via le raccourci
suivant :

::

  (cd obstuna-admin ; ./update-security-obstuna.sh)

ou

::
  ./obstuna-admin/update-security-obstuna.bat

Ce mode va effectuer les opérations suivantes après configuration de la base
cible et des rôles de sécurité :

- appliquer la sécurité sur les rôles de la base

Note::

  Le script appelle l'application avec l'action **--obstuna-admin security**

Vider une base obstuna
----------------------

Pour vider une base obstuna (ne vue de la recréer par exemple), il suffit de
lancer ObServe en mode *suppression obstuna* via le raccourci suivant :

::

  (cd obstuna-admin ; ./drop-obstuna.sh)

ou

::

  ./obstuna-admin/drop-obstuna.bat

Ce mode va effectuer les opérations suivantes après configuration de la base
cible :

- supprimer toutes les tables gérer de la base

Note::

  Le script appelle l'application avec l'action **--obstuna-admin drop**

Appliquer des scripts sql supplémentaires
-----------------------------------------

Depuis la version 3.0, il est possible de lancer sur la base de scripts sql supplémentaires placé dans le répertoire **extra**.

On lance ensuite la commande

::

  (cd obstuna-admin; ./apply-extra.sh)

Le script demande

- le nom de la base et l'utilisateur de connexion
- une confirmation d'exécution pour chaque script qu'il a trouvé dans le répertoire **extra**

.. _downloads: http://forge.codelutin.com/projects/observe/files
