.. -
.. * #%L
.. * ObServe
.. * %%
.. * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

======================
Installation d'ObServe
======================

:Author: Tony Chemit <chemit@codelutin.com>
:Date: 05/02/2009
:Abstract: Ce document explique comment installer le logiciel *ObServe* et modifier sa configuration.

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2


Configuration des interfaces graphiques
---------------------------------------

Il est possible de modifier certains aspects de l'interfaces graphiques en modifiant le fichier
ui.properties qui se trouve à la racine du jar principal sans pour autant à avoir à recompiler
le projet :

- modifier les icones de la navigation

- modifier les icones des actions

- modifiers les couleurs

Configuration des validateurs
-----------------------------

Il est possible de modifier les règles de validations à partir du jar du module de validation.

Configuration de l'utilisateur
------------------------------

L'application crée un fichier de configuration sur chaque poste où elle est installé.

Ce fichier se nomme .observe et se trouve dans le répertoire principal de l'utilisateur.

TODO définir ce qu'il faut y mettre.
