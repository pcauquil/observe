.. -
.. * #%L
.. * ObServe
.. * %%
.. * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=======
ObServe
=======

:Author: Tony Chemit <chemit@codelutin.com>

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2



Présentation
------------

Le logiciel *ObServe*... TODO

Les documents
-------------

- `Architecture logicielle`_
- `Road Map`_
- `Validation`_
- `Administration obstuna`_
- `Administration web`_
- `Client lourd`_
- `Le projet maven`_

.. _Architecture logicielle: architecture-logicielle.html
.. _Administration obstuna: install-serverPG.html
.. _Administration web: administration-web.html
.. _Validation: Validation.html
.. _Road Map: Avancement.html
.. _Client lourd: Observe-Swing.html
.. _Le projet maven: Observe-maven.html



