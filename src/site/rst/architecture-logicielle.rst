.. -
.. * #%L
.. * ObServe
.. * %%
.. * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=================================
Architecture logicielle d'ObServe
=================================

:Author: Tony Chemit <chemit@codelutin.com>
:Author: Benjamin Poussin <poussin@codelutin.com>

.. contents::

Abstract
--------

Ce document décrit l'architecture logicielle du projet *ObServe*.

Les différentes couches de l'application
----------------------------------------

Ce logiciel est une application de type "client lourd" totalement écrit en `Java`_.

Il est basé sur une architecture n-tiers en couches, à savoir :

- une couche de Présentation
- une couche Métier
- une couche de Services
- une couche de Persistance

.. image:: images/architecture.png
   :scale: 50
   :align: center

La couche de Présentation
~~~~~~~~~~~~~~~~~~~~~~~~~

Cette couche incarne l'IHM de l'application et est implémentée en *Java Swing*. Le rendu, quant à lui, utilise le style `Nimbus`_ disponible à partir de la version 6u10 de la *JRE* de Sun et qui
sera le nouveau style par défaut à partir de la version 7 de *Java*.

La couche de présentation respecte rigoureusement le modèle de conception *MVC* qui découple
la vue du modèle et du controleur, et ne communique qu'avec la couche Métier et la couche de Services.

Construction des interfaces graphiques
**************************************

Les interfaces graphiques sont construites à l'aide de la technologie `JAXX`_ qui permet de spécifier les IHM  en *xml* puis de les générer automatiquement à partir de ces spécifications.

Cette technologie permet, entre autre et de manière transparente, de gérer :

- l'internalisation,
- la gestion des évènements
- la validation des formulaires


La couche Métier
~~~~~~~~~~~~~~~~

C'est la couche qui contient la logique métier de l'application et interroge :

- la couche de Persistance pour communiquer avec la base de données locale,
- la couche de Services pour les besoins de traçabilité, de synchronisation avec le serveur *Obstuna* et de validation de niveau 2.


La couche de Services
~~~~~~~~~~~~~~~~~~~~~

Cette couche transverse propose différents services aux autres couches applicatives, à savoir :

- le service de Tracabilité (basé sur la librairie *common-logging*) permet de tracer les différents évènements produits durant l'exécution du logiciel. Ce service sera notamment
  utilisé lors de la validation de niveau 2 des données d'observateur, ainsi
  que lors de la synchronisation avec le serveur *Obstuna*.
- Le service de Validation.


le service de Validation
************************

La validation des données est réalisée à partir du moteur de validation `XWorks`_ actuellement utilisé par le célébre framework web `Struts 2`_. 
Pour répondre aux besoins spécifiques du projet Observe, ce moteur a été "customisé" pour prendre en compte 2 niveaux de gravité d'erreurs : erreur ou anomalie. 

L'implémentation de la validation est "non intrusive" vis-à-vis des données quelle valide. Toutes les règles de validation sont externalisées et décrites dans des fichiers xml. Elles peuvent donc être modifiées et affinées sans avoir à recompiler.
Elles sont rassemblées dans un *jar* indépendant pour en faciliter l'évolution.

Ce service est utilisé par la couche Présentation lors de la saisie des formulaires et par la couche Métier lors de la consolidation des données (validation dite de "niveau 2"). 

La couche de persistance
~~~~~~~~~~~~~~~~~~~~~~~~

ToPIA
*****

L'application utilise le composant *ToPIA-persistence* provenant framework `ToPIA`_ développé par la société
`Codelutin`_. Ce composant assure l'accès aux différentes bases de données de manière transparente.

Actuellement *ToPIA-persistence* s'appuie sur `Hibernate 3`_ pour la persistance et
les transactions. Prochainement *ToPIA-persistence* s'appuiera sur la norme *JPA* de persistance préconisée par Sun.

Les bases de données de l'application
*************************************

La base de données centrale `Obstuna` sera implantée sur un moteur `PostgreSQL`_.

Les bases de données déployées sur les Tablet-PC utiliseront un moteur `h2`_. Ce choix est conditionné par le fait que ce moteur est très léger (< à 1Moctets), ne nécessite aucune installation spécifique et fournit un mode `Postgres`_ rendant une base de données `h2`_ compatible avec un moteur `PostgreSQL`_ (simplification des opérations de synchronisation entre bases de données locales et base de données centrale).

 
.. _java: http://java.sun.com/javase/

.. _Codelutin: http://www.codelutin.com

.. _Nimbus: http://java.sun.com/developer/technicalArticles/javase/java6u10/index.html

.. _JAXX: http://buix.labs.libre-entreprise.org/jaxx/

.. _ToPIA: http://topia.labs.libre-entreprise.org/topia/topia-persistence/

.. _Struts 2: http://struts.apache.org/2.x/  

.. _XWorks: http://www.opensymphony.com/xwork/

.. _Hibernate 3: http://www.hibernate.org/

.. _h2: http://www.h2database.com/html/main.html

.. _Postgres: http://www.postgresql.org/

.. _PostgreSQL: http://www.postgresql.org/
