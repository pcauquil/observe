package fr.ird.observe.services.configuration.rest;

/*
 * #%L
 * ObServe :: Services REST Configuration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceType;
import org.nuiton.version.Version;

import java.net.URL;
import java.util.Optional;

/**
 * Configuration d'une source de données Rest non connectée.
 *
 * Created on 19/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveDataSourceConfigurationRest implements ObserveDataSourceConfiguration {

    private static final long serialVersionUID = 1L;

    /**
     * Le libellé de la source de données.
     */
    private String label;

    /**
     * L'url du serveur à utiliser.
     */
    private URL serverUrl;

    /**
     * Le login de l'utilisateur.
     */
    private String login;

    /**
     * Le mot de passe de l'utilisateur.
     */
    private char[] password;

    /**
     * Un nom optionel de base à utiliser, si rien n'est préciser, on utilisera la base par défaut du serveur.
     */
    private String optionalDatabaseName;

    /**
     * La version de la base demmandé
     */
    private Version modelVersion;


    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public ObserveDataSourceType getType() {
        return ObserveDataSourceType.SERVER;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public URL getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(URL serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char... password) {
        this.password = password;
    }

    public boolean withDatabaseName() {
        return optionalDatabaseName != null;
    }

    public Optional<String> getOptionalDatabaseName() {
        return Optional.ofNullable(optionalDatabaseName);
    }

    public void setOptionalDatabaseName(String optionalDatabaseName) {
        this.optionalDatabaseName = optionalDatabaseName;
    }

    @Override
    public Version getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(Version modelVersion) {
        this.modelVersion = modelVersion;
    }

    @Override
    public ObserveDataSourceConfigurationRest clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfigurationRest) super.clone();
    }

    @Override
    public boolean isAutoMigrate() {
        // on ne peut jamais migrer sur un serveur
        return false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("label", label)
                .add("serverUrl", serverUrl)
                .add("login", login)
                .add("password", "***")
                .add("optionalDatabaseName", optionalDatabaseName)
                .add("modelVersion", modelVersion)
                .toString();
    }
}
