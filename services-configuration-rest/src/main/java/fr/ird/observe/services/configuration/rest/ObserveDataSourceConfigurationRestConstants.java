package fr.ird.observe.services.configuration.rest;

/*
 * #%L
 * ObServe :: Services REST Configuration
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 04/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface ObserveDataSourceConfigurationRestConstants {

    Package ROOT_SERVICES_PACKAGE = Package.getPackage("fr.ird.observe.services.service");

    String PARAMETER_DATA_SOURCE_CONFIGURATION = "dataSourceConfiguration";

    String REQUEST_APPLICATION_LOCALE = "applicationLocale";

    String REQUEST_REFERENTIAL_LOCALE = "referentialLocale";

    String REQUEST_SPECIES_LIST_CONFIGURATION = "speciesListConfiguration";

    String REQUEST_AUTHENTICATION_TOKEN = "authenticationToken";

    String REQUEST_ADMIN_API_KEY = "adminApiKey";

    String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";

    String PARAMETERIZED_TYPE_PREFIX = "parameterized_type_";

}
