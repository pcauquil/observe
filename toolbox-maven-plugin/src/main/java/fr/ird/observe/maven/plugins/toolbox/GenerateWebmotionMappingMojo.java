package fr.ird.observe.maven.plugins.toolbox;

/*
 * #%L
 * ObServe :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.reflections.Reflections;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Generate webmotion mapping file.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
@Mojo(name = "generate-webmotion-mapping", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE)
public class GenerateWebmotionMappingMojo extends ToolboxMojoSupport {


    @Parameter(property = "generateWebmotionMapping.sourceApiPackageName", required = true)
    private String sourceApiPackageName;

    @Parameter(property = "generateWebmotionMapping.targetApiPackageName", required = true)
    private String targetApiPackageName;

    @Parameter(property = "generateWebmotionMapping.targetApiClassSuffix", required = true)
    private String targetApiClassSuffix;

    /**
     * To set the type of class to seek to generate the mapping.
     */
    @Parameter(property = "generateWebmotionMapping.classTypeName")
    private String classTypeName;

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateWebmotionMapping.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateWebmotionMapping.skip", defaultValue = "false")
    private boolean skip;

    /**
     * The root directory where to generated.
     */
    @Parameter(property = "generateWebmotionMapping.mappingFile", defaultValue = "${project.basedir}/src/main/resources/mapping", required = true)
    private File mappingFile;

    private Map<Class<?>, String> translationMap;
    protected Class<? extends Annotation> postRequestAnnotationType;
    protected Class<? extends Annotation> deleteRequestAnnotationType;

    @Override
    protected Path createOutputFile() throws IOException {

        Files.createDirectories(mappingFile.getParentFile().toPath());
        return mappingFile.toPath();

    }

    @Override
    protected boolean isSkip() {
        return skip;
    }

    @Override
    protected void init() throws Exception {

        if (skip) {
            return;
        }

        super.init();

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();

        try {

            ClassLoader cl = initClassLoader(getProject(), null, false, true, false, true, true);
            Thread.currentThread().setContextClassLoader(cl);

            postRequestAnnotationType = (Class) Class.forName("fr.ird.observe.services.spi.PostRequest");
            deleteRequestAnnotationType = (Class) Class.forName("fr.ird.observe.services.spi.DeleteRequest");

            Class<?> classType = Class.forName(classTypeName);

            Map<Class<?>, String> translationMap = new LinkedHashMap<>();

            for (Class<?> serviceContract : new Reflections(sourceApiPackageName).getSubTypesOf(classType)) {

                String sourceName = serviceContract.getName();
                String sourceSimpleNameName = serviceContract.getSimpleName();

                String sourcePackageNameSuffix = sourceName.substring(sourceApiPackageName.length() + 1, sourceName.length() - sourceSimpleNameName.length());

                String targetPackageName;

                if (sourcePackageNameSuffix.isEmpty()) {

                    // no sub package
                    targetPackageName = targetApiPackageName;

                } else {

                    // sub package
                    targetPackageName = targetApiPackageName + "." + sourcePackageNameSuffix.substring(0, sourcePackageNameSuffix.length() - 1);

                }

                String targetSimpleName = serviceContract.getSimpleName() + targetApiClassSuffix;

                String targetName = targetPackageName + "." + targetSimpleName;

                if (isVerbose()) {
                    getLog().info("Found maching class to scan : " + serviceContract.getName() + " → " + targetName);
                }
                translationMap.put(serviceContract, targetName);

            }

            getLog().info(translationMap.size() + " type(s) detected.");

            this.translationMap = Collections.unmodifiableMap(translationMap);
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }

    }

    @Override
    protected boolean checkSkip() {
        if (skip) {
            getLog().info("Skipping goal (skip flag is on).");
            return false;
        }

        if (translationMap.isEmpty()) {
            getLog().warn("Skipping goal (No matching class).");
            return false;
        }

        return super.checkSkip();
    }

    @Override
    public void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("project = " + getProject());
        }

        getLog().info("Generate to " + getOutputFile());

        Map<String, String> rules = new TreeMap<>();
        for (Map.Entry<Class<?>, String> entry : translationMap.entrySet()) {

            Class<?> sourceClass = entry.getKey();
            String targetClassName = entry.getValue();

            generateForClass(sourceClass, targetClassName, rules);

        }

        int ruleMax = 2 + rules.keySet().stream().mapToInt(String::length).max().orElse(0);

        getLog().info(rules.size() + " rule(s) detected.");

        StringBuilder rulesBuilder = new StringBuilder();
        for (Map.Entry<String, String> rule : rules.entrySet()) {
            rulesBuilder.append(StringUtils.rightPad(rule.getKey(), ruleMax)).append(rule.getValue()).append("\n");
        }
        String rulesStr = rulesBuilder.toString();

        String content = new String(Files.readAllBytes(mappingFile.toPath()));

        int indexOf = content.indexOf("# →→→ Generated dynamic mapping");
        content = content.substring(0, indexOf + "# →→→ Generated dynamic mapping".length());

        try (BufferedWriter writer = Files.newBufferedWriter(getOutputFile(), StandardCharsets.UTF_8)) {
            writer.append(content).append("\n\n# Do not modify below lines, they are generated at each maven build.\n\n").append(rulesStr);
        }

    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private void generateForClass(Class<?> sourceClass, String targetClassName, Map<String, String> rules) throws MissingMethodException, MismatchMethodParameterNameException, MissingClassException, IOException, ClassNotFoundException {

        Objects.requireNonNull(sourceClass);
        Objects.requireNonNull(targetClassName);
        Objects.requireNonNull(rules);

        Method[] sourceDeclaredMethods = sourceClass.getDeclaredMethods();

        if (isVerbose()) {

            getLog().info("Check " + sourceClass.getName());

        }
        String packagePrefix = "v1." + targetClassName.substring(targetApiPackageName.length() + 1) + ".";
        String rulePrefix = " /api/" + packagePrefix.replace("Controller", "").replaceAll("\\.", "/");

        for (Method sourceMethod : sourceDeclaredMethods) {

            if (isVerbose()) {
                getLog().info("Generate for " + sourceClass.getName() + "#" + sourceMethod.getName());
            }

            String methods;
            if (sourceMethod.isAnnotationPresent(postRequestAnnotationType)) {
                methods = "POST";
            } else if (sourceMethod.isAnnotationPresent(deleteRequestAnnotationType)) {
                methods = "DELETE";
            } else {
                methods = "GET";
            }
            String method = packagePrefix + sourceMethod.getName().replace("/", ".");
            String rule = methods + rulePrefix + sourceMethod.getName();

            // generate entry in mapping
            rules.put(rule, method);

        }

    }

}

