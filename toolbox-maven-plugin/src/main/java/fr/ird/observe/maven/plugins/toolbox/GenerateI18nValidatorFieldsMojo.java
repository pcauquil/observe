package fr.ird.observe.maven.plugins.toolbox;

/*-
 * #%L
 * ObServe :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Pour générer les clefs i18n des champs utilisés dans les validateurs.
 *
 * Created on 31/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
@Mojo(name = "generate-i18n-validator-fields", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE)
class GenerateI18nValidatorFieldsMojo extends GenerateValidatorMojoSupport {

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateI18nValidatorFields.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateI18nValidatorFields.skip", defaultValue = "false")
    private boolean skip;

    /**
     * To set the package fully qualified name of the generated class.
     *
     * By default, will use groupId.artifactId (with {@code -} replaced by {@code .}).
     */
    @Parameter(property = "generateI18nValidatorFields.packageName")
    private String packageName;

    /**
     * Name of the generated class.
     */
    @Parameter(property = "generateI18nValidatorFields.className", defaultValue = "I18nValidatorHelper", required = true)
    private String className;

    /**
     * Prefix to add to generated i18n keys.
     */
    @Parameter(property = "generateI18nValidatorFields.prefix")
    private String prefix;

    /**
     * The root directory where to generated.
     */
    @Parameter(property = "generateI18nValidatorFields.outputDirectory", defaultValue = "${basedir}/target/generated-sources/java", required = true)
    private File outputDirectory;

    @Override
    protected Path createOutputFile() throws IOException {

        if (packageName == null) {

            packageName = getProject().getGroupId() + "." + getProject().getArtifactId().replaceAll("-", ".");

        }
        Path directory = PluginHelper.getFile(outputDirectory, packageName.trim().split("\\.")).toPath();

        Files.createDirectories(directory);

        return directory.resolve(className + ".java");
    }

    @Override
    protected boolean isSkip() {
        return skip;
    }

    @Override
    public void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("project = " + getProject());
        }

        getLog().info("Generate to " + getOutputFile());

        List<String> compileSourceRoots = getProject().getCompileSourceRoots();

        if (!compileSourceRoots.contains(outputDirectory.getAbsolutePath())) {

            getLog().info("Add to compile source root: " + outputDirectory);

            getProject().addCompileSourceRoot(outputDirectory.getAbsolutePath());
        }

        Set<String> fields = new TreeSet<>();
        for (ValidatorsCache.ValidatorInfo validator : getValidators()) {
//TODO            To preappend with type name, should be this one day to be more precise
//            String typeName = Introspector.decapitalize(StringUtils.removeEnd(validator.getType().getSimpleName(), "Dto") + ".");
//            fields.addAll(validator.getFields().stream().map(field -> typeName + field).collect(Collectors.toList()));
            fields.addAll(validator.getFields());
        }

        getLog().info(fields.size() + " validator files(s) detected.");

        generate(packageName, className, prefix, fields, getOutputFile());

    }


    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

}
