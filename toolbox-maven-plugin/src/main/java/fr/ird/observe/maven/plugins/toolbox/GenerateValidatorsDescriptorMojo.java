package fr.ird.observe.maven.plugins.toolbox;

/*-
 * #%L
 * ObServe :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializer;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * Pour générer les clefs i18n des champs utilisés dans les validateurs.
 *
 * Created on 31/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
@Mojo(name = "generate-validators-descriptor", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE)
class GenerateValidatorsDescriptorMojo extends GenerateValidatorMojoSupport {

    /**
     * The root directory where to generated.
     */
    @Parameter(property = "generateValidatorsDescriptor.outputFile", defaultValue = "${project.build.outputDirectory}/META-INF/validators/${project.artifactId}.json", required = true)
    private File outputFile;

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateValidatorsDescriptor.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateValidatorsDescriptor.skip", defaultValue = "false")
    private boolean skip;

    @Override
    protected Path createOutputFile() throws IOException {

        Files.createDirectories(outputFile.getParentFile().toPath());
        return outputFile.toPath();

    }

    @Override
    protected boolean isSkip() {
        return skip;
    }

    @Override
    public void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("project = " + getProject());
        }

        getLog().info("Generate to " + getOutputFile());

        Set<String> fields = new TreeSet<>();
        for (ValidatorsCache.ValidatorInfo validator : getValidators()) {
            fields.addAll(validator.getFields());
        }

        getLog().info(fields.size() + " validator files(s) detected.");

        ArrayList<ValidatorsCache.ValidatorInfo> validatorList = new ArrayList<>(getValidators());

        Collections.sort(validatorList, (o1, o2) -> o1.getType().getName().compareToIgnoreCase(o2.getType().getName()));

        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(ValidatorsCache.ValidatorInfo.class, (JsonSerializer<ValidatorsCache.ValidatorInfo>) (src, typeOfSrc, context) -> {

                    JsonObject element2 = new JsonObject();
                    element2.add("type", context.serialize(src.getType().getName()));
                    element2.add("context", context.serialize(src.getContext()));
                    element2.add("scope", context.serialize(src.getScope()));
                    element2.add("fields", context.serialize(src.getFields()));

                    return element2;
                })
                .create();
        String validatorsJson = gson.toJson(validatorList);
        Files.write(outputFile.toPath(), validatorsJson.getBytes(StandardCharsets.UTF_8));

    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

}
