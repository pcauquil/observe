package fr.ird.observe.maven.plugins.toolbox;

/*-
 * #%L
 * ObServe :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.atteo.evo.inflector.English;
import org.nuiton.plugin.PluginHelper;
import org.reflections.Reflections;

import java.beans.Introspector;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Pour générer les clefs i18n des différents types de dto utilisés dans l'application.
 *
 * Created on 31/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
@Mojo(name = "generate-i18n-types", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE)
class GenerateI18nTypesMojo extends ToolboxMojoSupport {

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateI18nTypes.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "generateI18nTypes.skip", defaultValue = "false")
    private boolean skip;

    /**
     * To set the package fully qualified name of the generated class.
     *
     * By default, will use groupId.artifactId (with {@code -} replaced by {@code .}).
     */
    @Parameter(property = "generateI18nTypes.packageName")
    private String packageName;

    /**
     * Name of the generated class.
     */
    @Parameter(property = "generateI18nTypes.className", defaultValue = "I18nTypeHelper", required = true)
    private String className;

    /**
     * Prefix to add to generated i18n keys.
     */
    @Parameter(property = "generateI18nTypes.prefix")
    private String prefix;

    /**
     * Possible suffix to remove to detected class. Can contains multiple values separated by a comma.
     */
    @Parameter(property = "generateI18nTypes.removeSuffix")
    private String removeSuffix;

    /**
     * Possible suffix to skip some detected classes. Can contains multiple values separated by a comma.
     */
    @Parameter(property = "generateI18nTypes.skipSuffix")
    private String skipSuffix;

    /**
     * The root directory where to generated.
     */
    @Parameter(property = "generateI18nTypes.outputDirectory", defaultValue = "${basedir}/target/generated-sources/java", required = true)
    private File outputDirectory;

    private Set<String> typeNames;

    @Override
    protected Path createOutputFile() throws IOException {

        if (packageName == null) {

            packageName = getProject().getGroupId() + "." + getProject().getArtifactId().replaceAll("-", ".");

        }
        Path directory = PluginHelper.getFile(outputDirectory, packageName.trim().split("\\.")).toPath();

        Files.createDirectories(directory);

        return directory.resolve(className + ".java");
    }

    @Override
    protected boolean isSkip() {
        return skip;
    }

    @Override
    public void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("project = " + getProject());
        }

        getLog().info("Generate to " + getOutputFile());

        List<String> compileSourceRoots = getProject().getCompileSourceRoots();

        if (!compileSourceRoots.contains(outputDirectory.getAbsolutePath())) {

            getLog().info("Add to compile source root: " + outputDirectory);
            getProject().addCompileSourceRoot(outputDirectory.getAbsolutePath());

        }

        generate(packageName, className, prefix, typeNames, getOutputFile());

    }

    @Override
    protected void init() throws Exception {
        super.init();

        typeNames = new TreeSet<>();

        List<String> removeSuffixes = Arrays.asList(removeSuffix.split("\\s*,\\s*"));
        List<String> skipSuffixes = Arrays.asList(skipSuffix.split("\\s*,\\s*"));
        Collections.sort(removeSuffixes);
        Collections.reverse(removeSuffixes);
        // On ne met pas la classe directement car on ne veut pas lie le module a services-model
        Class<?> idDtoClass = Class.forName("fr.ird.observe.services.dto.IdDto");
        for (Class<?> aClass : new Reflections("fr.ird.observe.services.dto").getSubTypesOf(idDtoClass)) {

            if (Modifier.isAbstract(aClass.getModifiers())) {
                continue;
            }

            String simpleName = aClass.getSimpleName();

            boolean skip = false;
            for (String skipSuffix : skipSuffixes) {
                if (simpleName.endsWith(skipSuffix)) {

                    skip = true;
                    break;
                }
            }

            if (skip) {
                continue;
            }

            for (String suffix : removeSuffixes) {
                if (simpleName.endsWith(suffix)) {
                    String typeName = Introspector.decapitalize(StringUtils.removeEnd(simpleName, suffix));
                    typeNames.add(typeName);
                    typeNames.add(English.plural(typeName));
                    break;
                }
            }

        }

        getLog().info(typeNames.size() + " types(s) detected.");

    }

    @Override
    protected boolean checkSkip() {

        if (isSkip()) {
            getLog().info("Skipping goal (skip flag is on).");
            return false;
        }
        if (typeNames.isEmpty()) {
            getLog().info("Skipping goal (no type detected).");
            return false;
        }
        return true;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

}
