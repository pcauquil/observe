package fr.ird.observe.maven.plugins.toolbox;

/*
 * #%L
 * ObServe :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.reflect.ClassPath;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Check API against some others, says check if source api is exactly fullfilled by the target API and the method parameters are exactly the same.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
@Mojo(name = "check-api", defaultPhase = LifecyclePhase.PROCESS_CLASSES, requiresDependencyResolution = ResolutionScope.COMPILE)
public class CheckApiMojo extends AbstractPlugin {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    protected MavenProject project;

    @Parameter(property = "checkApi.sourceApiRootDirectory", required = true, defaultValue = "${project.build.sourceDirectory}")
    protected File sourceApiRootDirectory;

    @Parameter(property = "checkApi.sourceApiPackageName", required = true)
    protected String sourceApiPackageName;

    @Parameter(property = "checkApi.sourceApiIncludePattern", required = true)
    protected String sourceApiIncludePattern;

//    @Parameter(property = "checkApi.sourceApiExludePattern", required = true)
//    protected String sourceApiExludePattern;

    @Parameter(property = "checkApi.targetApiPackageName", required = true)
    protected String targetApiPackageName;

    @Parameter(property = "checkApi.targetApiClassSuffix", required = true)
    protected String targetApiClassSuffix;

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.0
     */
    @Parameter(property = "checkApi.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "checkApi.skip", defaultValue = "false")
    protected boolean skip;

    /**
     * Encoding a utiliser pour lire et ecrire les fichiers.
     *
     * @since 1.0.0
     */
    @Parameter(property = "checkApi.encoding", defaultValue = "${project.build.sourceEncoding}", required = true)
    protected String encoding;
    protected Map<ClassPath.ClassInfo, ClassPath.ClassInfo> translationMap;

    @Override
    protected void init() throws Exception {

        if (skip) {
            return;
        }
        if (getLog().isDebugEnabled()) {
            setVerbose(true);
        }

        Pattern sourceNameMatchingPattern = Pattern.compile(sourceApiIncludePattern);

        ClassLoader cl = initClassLoader(project, sourceApiRootDirectory, false, true, false, true, true);
        Set<ClassPath.ClassInfo> sourceApiClassesInPackage = ClassPath.from(cl).getTopLevelClassesRecursive(sourceApiPackageName);
        Set<ClassPath.ClassInfo> targetApiClassesInPackage = ClassPath.from(cl).getTopLevelClassesRecursive(targetApiPackageName);

        ImmutableMap<String, ClassPath.ClassInfo> targetClassInfosByClassName = Maps.uniqueIndex(targetApiClassesInPackage, ClassPath.ClassInfo::getName);

        Map<ClassPath.ClassInfo, ClassPath.ClassInfo> translationMap = new LinkedHashMap<>();

        for (ClassPath.ClassInfo sourceClassInfo : sourceApiClassesInPackage) {

            Matcher matcher = sourceNameMatchingPattern.matcher(sourceClassInfo.getSimpleName());
            if (matcher.matches()) {

                if (isVerbose()) {
                    getLog().info("Found maching source class: " + sourceClassInfo);
                }

                String sourceName = sourceClassInfo.getName();
                String sourceSimpleNameName = sourceClassInfo.getSimpleName();

                String sourcePackageNameSuffix = sourceName.substring(sourceApiPackageName.length() + 1, sourceName.length() - sourceSimpleNameName.length());

                String targetPackageName;

                if (sourcePackageNameSuffix.isEmpty()) {

                    // no sub package
                    targetPackageName = targetApiPackageName;

                } else {

                    // sub package
                    targetPackageName = targetApiPackageName + "." + sourcePackageNameSuffix.substring(0, sourcePackageNameSuffix.length() - 1);

                }

                String targetSimpleName = sourceSimpleNameName + targetApiClassSuffix;

                String targetName = targetPackageName + "." + targetSimpleName;

                ClassPath.ClassInfo targetClassInfo;

                if (!targetClassInfosByClassName.containsKey(targetName)) {

                    targetClassInfo = null;
                } else {
                    targetClassInfo = targetClassInfosByClassName.get(targetName);
                }

                if (isVerbose()) {
                    getLog().info("Found maching class to check : " + sourceClassInfo + " → " + targetClassInfo);
                }

                translationMap.put(sourceClassInfo, targetClassInfo);
            }


        }

        this.translationMap = Collections.unmodifiableMap(translationMap);

        getLog().info(translationMap.size() + " type(s) detected.");

    }

    @Override
    protected boolean checkSkip() {
        if (skip) {
            getLog().info("Skipping goal (skip flag is on).");
            return false;
        }

        if (translationMap.isEmpty()) {
            getLog().warn("Skipping goal (No matching class).");
            return false;
        }

        return super.checkSkip();
    }

    @Override
    public void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("project = " + project);
        }

        for (Map.Entry<ClassPath.ClassInfo, ClassPath.ClassInfo> entry : translationMap.entrySet()) {
            ClassPath.ClassInfo sourceClassInfo = entry.getKey();
            ClassPath.ClassInfo targetClassInfo = entry.getValue();

            Class<?> sourceClass = sourceClassInfo.load();
            Class<?> targetClass = targetClassInfo == null ? null : targetClassInfo.load();

            checkClass(sourceClass, targetClass);

        }

        getLog().info(translationMap.size() + " class(es) checked.");
        getLog().info(methodCount + " method(s) checked.");

    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private int methodCount;

    private void checkClass(Class<?> sourceClass, Class<?> targetClass) throws MissingMethodException, MismatchMethodParameterNameException, MissingClassException {

        Method[] sourceDeclaredMethods = sourceClass.getDeclaredMethods();

        if (isVerbose()) {

            getLog().info("Check " + sourceClass.getName());

        }

        if (targetClass == null) {
            throw new MissingClassException(sourceClass.getName());
        }

        for (Method sourceMethod : sourceDeclaredMethods) {

            methodCount++;

            Method targetMethod;
            try {
                targetMethod = targetClass.getDeclaredMethod(sourceMethod.getName(), (Class<?>[]) sourceMethod.getParameterTypes());
            } catch (NoSuchMethodException e) {
                throw new MissingMethodException("Could not find method " + sourceMethod.getName() + " on target class: " + targetClass);
            }

            if (isVerbose()) {

                getLog().info("Check " + sourceClass.getName() + "#" + sourceMethod.getName());

            }

            java.lang.reflect.Parameter[] sourceParameters = sourceMethod.getParameters();
            java.lang.reflect.Parameter[] targetParameters = targetMethod.getParameters();

            for (int i = 0, max = sourceParameters.length; i < max; i++) {
                java.lang.reflect.Parameter sourceParameter = sourceParameters[i];
                java.lang.reflect.Parameter targetParameter = targetParameters[i];

                if (isVerbose()) {

                    getLog().info("Check " + sourceClass.getName() + "#" + sourceMethod.getName() + "→" + sourceParameter.getName() + " vs " + targetParameter.getName());

                }

                if (!sourceParameter.getName().equals(targetParameter.getName())) {

                    throw new MismatchMethodParameterNameException(sourceClass.getName(),
                                                                   sourceMethod.getName(),
                                                                   sourceParameter.getName(),
                                                                   i,
                                                                   targetClass.getName(),
                                                                   targetMethod.getName(),
                                                                   targetParameter.getName());
                }

            }

        }

        if (isVerbose()) {
            getLog().info(targetClass.getName() + " is conform to #" + sourceClass.getName());
        }

    }

}

