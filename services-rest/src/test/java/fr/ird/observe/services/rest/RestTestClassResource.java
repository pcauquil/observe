package fr.ird.observe.services.rest;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.dto.ObserveSpeciesListConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.test.TestClassResourceSupport;
import fr.ird.observe.test.spi.DatabaseClassifier;
import org.junit.runner.Description;
import org.nuiton.version.Version;

import java.net.URL;
import java.util.Locale;

/**
 * Created on 03/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class RestTestClassResource extends TestClassResourceSupport {

    private final ObserveServiceFactoryRest serviceFactory;

    public RestTestClassResource() {

        super(DatabaseClassifier.DEFAULT);
        this.serviceFactory = new ObserveServiceFactoryRest();
        this.serviceFactory.setMainServiceFactory(serviceFactory);
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {
        ObserveServiceInitializer observeServiceInitializer = ObserveServiceInitializer.create(
                Locale.FRANCE,
                ReferentialLocale.FR,
                temporaryDirectoryRoot.toFile(),
                ObserveSpeciesListConfiguration.newDefaultConfiguration(),
                dataSourceConfiguration
        );
        return serviceFactory.newService(observeServiceInitializer, serviceType);
    }

    public <S extends ObserveService> S newService(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {
        ObserveServiceInitializer observeServiceInitializer = ObserveServiceInitializer.create(
                Locale.FRANCE,
                ReferentialLocale.FR,
                temporaryDirectoryRoot.toFile(),
                ObserveSpeciesListConfiguration.newDefaultConfiguration(),
                dataSourceConnection
        );
        return serviceFactory.newService(observeServiceInitializer, serviceType);
    }

    @Override
    protected void after(Description description) {

        super.after(description);

        serviceFactory.close();

    }

    ObserveDataSourceConfigurationRest createDataSourceConfigurationRest(Class<?> testClass,
                                                                         String databaseName,
                                                                         Version dbVersion,
                                                                         URL serverUrl,
                                                                         String login,
                                                                         char... password) {

        ObserveDataSourceConfigurationRest configurationRest = new ObserveDataSourceConfigurationRest();
        configurationRest.setLabel(testClass.getSimpleName() + "#" + serverUrl);
        configurationRest.setServerUrl(serverUrl);
        configurationRest.setLogin(login);
        configurationRest.setPassword(password);
        configurationRest.setOptionalDatabaseName(databaseName);
        configurationRest.setModelVersion(dbVersion);

        return configurationRest;

    }

}
