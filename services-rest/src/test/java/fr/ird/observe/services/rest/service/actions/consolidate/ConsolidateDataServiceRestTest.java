package fr.ird.observe.services.rest.service.actions.consolidate;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.services.rest.service.AbstractServiceRestTest;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataRequest;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataResult;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created on 29/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ConsolidateDataServiceRestTest extends AbstractServiceRestTest {

    protected ConsolidateDataService service;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        service = newService(ConsolidateDataService.class);
    }

    @Test
    public void testConsolidateTripSeines() throws Exception {

        List<String> tripSeineIds = Lists.newArrayList(ObserveFixtures.TRIP_SEINE_ID_1, ObserveFixtures.TRIP_SEINE_ID_2, ObserveFixtures.TRIP_SEINE_ID_3);

        ConsolidateTripSeineDataRequest request = new ConsolidateTripSeineDataRequest();
        request.setTripSeineIds(ImmutableSet.copyOf(tripSeineIds));
        request.setFailIfLenghtWeightParameterNotFound(false);

        ImmutableSet<ConsolidateTripSeineDataResult> results = service.consolidateTripSeines(request);
        Assert.assertNotNull(results);
        //FIXME Il faudrait avoir des données modifiées dans la base de test
        Assert.assertEquals(0, results.size());

    }
}
