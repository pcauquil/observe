package fr.ird.observe.services.rest.service.seine;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.HarbourDto;
import fr.ird.observe.services.dto.referential.OceanDto;
import fr.ird.observe.services.dto.referential.PersonDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.VesselDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.rest.service.AbstractServiceRestTest;
import fr.ird.observe.services.service.seine.TripSeineService;
import fr.ird.observe.test.ObserveFixtures;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.util.DateUtil;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */

public class TripSeineServiceRestTest extends AbstractServiceRestTest {

    protected TripSeineService service;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        service = newService(TripSeineService.class);
    }

    @Test
    public void getTripSeineByProgramTest() {

        DataReferenceSet<TripSeineDto> stubDtos = service.getTripSeineByProgram(ObserveFixtures.PROGRAM_ID);

        Assert.assertNotNull(stubDtos);

        Assert.assertEquals(134, stubDtos.sizeReference());

        DataReference<TripSeineDto> tripSeineStub1Dto = DataReference.find(stubDtos.getReferences(), ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertEquals(ObserveFixtures.TRIP_SEINE_ID_1, tripSeineStub1Dto.getId());
        Assert.assertEquals(DateUtil.createDate(25, 1, 2013), tripSeineStub1Dto.getPropertyValue(TripSeineDto.PROPERTY_START_DATE));
        Assert.assertEquals(DateUtil.createDate(27, 2, 2013), tripSeineStub1Dto.getPropertyValue(TripSeineDto.PROPERTY_END_DATE));
        Assert.assertEquals("BERNICA", tripSeineStub1Dto.getPropertyValue(TripSeineDto.PROPERTY_VESSEL));
        Assert.assertEquals("Varenne Fanchon", tripSeineStub1Dto.getPropertyValue(TripSeineDto.PROPERTY_OBSERVER));

        DataReference<TripSeineDto> tripSeineStub2Dto = DataReference.find(stubDtos.getReferences(), ObserveFixtures.TRIP_SEINE_ID_2);

        Assert.assertEquals(ObserveFixtures.TRIP_SEINE_ID_2, tripSeineStub2Dto.getId());
        Assert.assertEquals(DateUtil.createDate(26, 1, 2013), tripSeineStub2Dto.getPropertyValue(TripSeineDto.PROPERTY_START_DATE));
        Assert.assertEquals(DateUtil.createDate(12, 3, 2013), tripSeineStub2Dto.getPropertyValue(TripSeineDto.PROPERTY_END_DATE));
        Assert.assertEquals("VIA EUROS", tripSeineStub2Dto.getPropertyValue(TripSeineDto.PROPERTY_VESSEL));
        Assert.assertEquals("Protat Martin", tripSeineStub2Dto.getPropertyValue(TripSeineDto.PROPERTY_OBSERVER));

        DataReference<TripSeineDto> tripSeineStub3Dto = DataReference.find(stubDtos.getReferences(), ObserveFixtures.TRIP_SEINE_ID_3);

        Assert.assertEquals(ObserveFixtures.TRIP_SEINE_ID_3, tripSeineStub3Dto.getId());
        Assert.assertEquals(DateUtil.createDate(1, 2, 2013), tripSeineStub3Dto.getPropertyValue(TripSeineDto.PROPERTY_START_DATE));
        Assert.assertEquals(DateUtil.createDate(17, 3, 2013), tripSeineStub3Dto.getPropertyValue(TripSeineDto.PROPERTY_END_DATE));
        Assert.assertEquals("GUERIDEN", tripSeineStub3Dto.getPropertyValue(TripSeineDto.PROPERTY_VESSEL));
        Assert.assertEquals("Le Bourdonnec Pierre", tripSeineStub3Dto.getPropertyValue(TripSeineDto.PROPERTY_OBSERVER));

    }

    @Test
    public void loadFormTest() throws Exception {

        Form<TripSeineDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        Assert.assertNotNull(form);
        TripSeineDto tripSeineDto = form.getObject();

        Assert.assertNull(tripSeineDto.getCaptain());

        Assert.assertEquals("fr.ird.observe.entities.referentiel.Person#1355399844272#0.32586441962131485", tripSeineDto.getObserver().getId());
        Assert.assertEquals("Fanchon", tripSeineDto.getObserver().getPropertyValue(PersonDto.PROPERTY_FIRST_NAME));
        Assert.assertEquals("Varenne", tripSeineDto.getObserver().getPropertyValue(PersonDto.PROPERTY_LAST_NAME));
        Assert.assertNull(tripSeineDto.getDataEntryOperator());
        Assert.assertEquals("fr.ird.observe.entities.referentiel.Vessel#1306847717532#0.7435948873477364", tripSeineDto.getVessel().getId());
        Assert.assertEquals("835", tripSeineDto.getVessel().getPropertyValue(VesselDto.PROPERTY_CODE));
        Assert.assertEquals("BERNICA", tripSeineDto.getVessel().getPropertyValue("label"));
        Assert.assertEquals("fr.ird.observe.entities.referentiel.Ocean#1239832686152#0.8325731048817705", tripSeineDto.getOcean().getId());
        Assert.assertEquals("2", tripSeineDto.getOcean().getPropertyValue(OceanDto.PROPERTY_CODE));
        Assert.assertEquals("Indien", tripSeineDto.getOcean().getPropertyValue("label"));
        Assert.assertNull(tripSeineDto.getDepartureHarbour());
        Assert.assertNull(tripSeineDto.getLandingHarbour());
        Assert.assertNull(tripSeineDto.getErsId());
        Assert.assertEquals(DateUtil.createDate(25, 1, 2013), tripSeineDto.getStartDate());
        Assert.assertEquals(DateUtil.createDate(27, 2, 2013), tripSeineDto.getEndDate());
        Assert.assertNull(tripSeineDto.getFormsUrl());
        Assert.assertNull(tripSeineDto.getReportsUrl());
        Assert.assertTrue(tripSeineDto.getComment().startsWith("Caractéristiques de la senne"));

        assertEditLabels(form, 8,
                         ProgramDto.class,
                         PersonDto.class,
                         VesselDto.class,
                         OceanDto.class,
                         HarbourDto.class);

    }

    @Test
    public void preCreateTest() {
        Form<TripSeineDto> form = service.preCreate(ObserveFixtures.PROGRAM_ID);

        Assert.assertNotNull(form);
        TripSeineDto tripSeineDto = form.getObject();

        Assert.assertNull(tripSeineDto.getCaptain());
        Assert.assertNull(tripSeineDto.getObserver());
        Assert.assertNull(tripSeineDto.getDataEntryOperator());
        Assert.assertNull(tripSeineDto.getVessel());
        Assert.assertNull(tripSeineDto.getOcean());
        Assert.assertNull(tripSeineDto.getDepartureHarbour());
        Assert.assertNull(tripSeineDto.getLandingHarbour());
        Assert.assertNull(tripSeineDto.getErsId());
        //Assert.assertEquals(DateUtil.getDay(ObserveServiceContextTopiaTaiste.DATE), tripSeineDto.getStartDate());
        //Assert.assertEquals(DateUtil.getDay(ObserveServiceContextTopiaTaiste.DATE), tripSeineDto.getEndDate());
        Assert.assertNull(tripSeineDto.getFormsUrl());
        Assert.assertNull(tripSeineDto.getReportsUrl());
        Assert.assertNull(tripSeineDto.getComment());

        assertEditLabels(form, 8,
                         ProgramDto.class,
                         PersonDto.class,
                         VesselDto.class,
                         OceanDto.class,
                         HarbourDto.class);

    }

    //FIXME Ce test modifie la base, il faut trouver un moyen d'utiliser une autre base
    @Ignore
    @Test
    public void saveUpdateTest() {

        Form<TripSeineDto> form = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);

        TripSeineDto tripSeineDto = form.getObject();

//        ReferenceSetDto<PersonDto> personRefs = formDto.getReferenceSetDto(PersonDto.class);
//        ReferenceSetDto<OceanDto> oceanRefs = formDto.getReferenceSetDto(OceanDto.class);
//        ReferenceSetDto<HarbourDto> harbourRefs = formDto.getReferenceSetDto(HarbourDto.class);
//
//        tripSeineDto.setCaptain((ReferentialReference<PersonDto>) personRefs.getReferences(0));
//        tripSeineDto.setDataEntryOperator((ReferentialReference<PersonDto>) personRefs.getReferences(2));
//
//        tripSeineDto.setOcean((ReferentialReference<OceanDto>) oceanRefs.getReferences(0));
//
//        tripSeineDto.setDepartureHarbour((ReferentialReference<HarbourDto>) harbourRefs.getReferences(0));
//        tripSeineDto.setLandingHarbour((ReferentialReference<HarbourDto>) harbourRefs.getReferences(1));

        tripSeineDto.setErsId("ersid");

        tripSeineDto.setStartDate(DateUtil.createDate(24, 8, 2015));
        tripSeineDto.setEndDate(DateUtil.createDate(24, 9, 2015));

        tripSeineDto.setFormsUrl("http://une.url.com/formulaire");
        tripSeineDto.setReportsUrl("http://une.url.com/rapport");

        tripSeineDto.setComment("Un commentaire");

        service.save(form.getObject());

        Form<TripSeineDto> formReload = service.loadForm(ObserveFixtures.TRIP_SEINE_ID_1);
        TripSeineDto tripSeineDtoReload = formReload.getObject();

        Assert.assertEquals(tripSeineDto.getCaptain().getId(), tripSeineDtoReload.getCaptain().getId());
        Assert.assertEquals(tripSeineDto.getObserver().getId(), tripSeineDtoReload.getObserver().getId());
        Assert.assertEquals(tripSeineDto.getDataEntryOperator().getId(), tripSeineDtoReload.getDataEntryOperator().getId());
        Assert.assertEquals(tripSeineDto.getVessel().getId(), tripSeineDtoReload.getVessel().getId());
        Assert.assertEquals(tripSeineDto.getOcean().getId(), tripSeineDtoReload.getOcean().getId());
        Assert.assertEquals(tripSeineDto.getDepartureHarbour().getId(), tripSeineDtoReload.getDepartureHarbour().getId());
        Assert.assertEquals(tripSeineDto.getLandingHarbour().getId(), tripSeineDtoReload.getLandingHarbour().getId());
        Assert.assertEquals(tripSeineDto.getErsId(), tripSeineDtoReload.getErsId());
        Assert.assertEquals(tripSeineDto.getStartDate(), tripSeineDtoReload.getStartDate());
        Assert.assertEquals(tripSeineDto.getEndDate(), tripSeineDtoReload.getEndDate());
        Assert.assertEquals(tripSeineDto.getFormsUrl(), tripSeineDtoReload.getFormsUrl());
        Assert.assertEquals(tripSeineDto.getReportsUrl(), tripSeineDtoReload.getReportsUrl());
        Assert.assertEquals(tripSeineDto.getComment(), tripSeineDtoReload.getComment());

    }

    @Ignore
    @Test
    public void deleteTest() {

        service.delete(ObserveFixtures.TRIP_SEINE_ID_1);

        DataReferenceSet<TripSeineDto> tripSeineByProgram = service.getTripSeineByProgram(ObserveFixtures.PROGRAM_ID);

        Assert.assertEquals(2, tripSeineByProgram.sizeReference());

        Assert.assertTrue(Iterables.isEmpty(DataReference.filterById(tripSeineByProgram.getReferences(), ObserveFixtures.TRIP_SEINE_ID_1)));

    }


}
