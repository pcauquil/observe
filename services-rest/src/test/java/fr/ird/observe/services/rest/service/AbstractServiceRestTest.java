package fr.ird.observe.services.rest.service;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.rest.RestTestClassResource;
import fr.ird.observe.services.rest.RestTestMethodResource;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.test.ObserveTestConfiguration;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
@DatabaseLoginConfiguration(ObserveTestConfiguration.WEB_LOGIN)
@DatabasePasswordConfiguration(ObserveTestConfiguration.WEB_PASSWORD)
@DatabaseUrlConfiguration(ObserveTestConfiguration.WEB_URL)
public abstract class AbstractServiceRestTest {

    @ClassRule
    public static final RestTestClassResource REST_TEST_CLASS_RESOURCE = new RestTestClassResource();

    @Rule
    public final RestTestMethodResource restTestMethodResource = new RestTestMethodResource(REST_TEST_CLASS_RESOURCE);

    private ObserveDataSourceConnection dataSourceConnection;

    private DataSourceService dataSourceService;

    @Before
    public void setUp() throws Exception {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = restTestMethodResource.getDataSourceConfiguration();

        dataSourceService = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        dataSourceConnection = dataSourceService.open(dataSourceConfiguration);
    }

    @After
    public void tearDown() {

        if (dataSourceConnection != null) {
            dataSourceService.close();
        }

    }

    public <S extends ObserveService> S newService(Class<S> serviceType) {
        return REST_TEST_CLASS_RESOURCE.newService(dataSourceConnection, serviceType);
    }

    protected <T extends IdDto> void assertEditLabels(Form<T> form, int expectedLabels, Class<?>... expectedTypes) {

        //FIXME Rest test
//        Assert.assertNotNull(formDto.getLabels());
//
//        Set<Class<?>> types = ReferenceSetDtos.getTypes(formDto.getLabels());
//
//        Assert.assertEquals(expectedTypes.length, types.size());
//
//        for (Class<?> expectedType : expectedTypes) {
//            Assert.assertTrue(types.contains(expectedType));
//        }
//        Assert.assertEquals(expectedLabels, formDto.sizeLabels());
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertTrue(referenceSetDto.sizeReference() > 0);
//
//        }
    }

    protected <T extends IdDto> void assertReadLabels(Form<T> form, int expectedLabels, Class<?>... expectedTypes) {

        //FIXME Rest test
//        Assert.assertNotNull(formDto.getLabels());
//
//        Set<Class<?>> types = ReferenceSetDtos.getTypes(formDto.getLabels());
//
//        Assert.assertEquals(expectedTypes.length, types.size());
//
//        for (Class<?> expectedType : expectedTypes) {
//            Assert.assertTrue(types.contains(expectedType));
//        }
//        Assert.assertEquals(expectedLabels, formDto.sizeLabels());
//
//        for (ReferenceSetDto referenceSetDto : formDto.getLabels()) {
//
//            Assert.assertTrue(referenceSetDto.isReferenceEmpty());
//
//        }
    }

}
