package fr.ird.observe.services.rest.service;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

/**
 * Created on 03/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DataSourceServiceRestTest extends AbstractServiceRestTest {

    //FIXME
    @Ignore
    @Test
    public void testOpenNotExistingDatabase() throws IOException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, CloneNotSupportedException, BabModelVersionException {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = restTestMethodResource.getDataSourceConfiguration().clone();
        dataSourceConfiguration.setLogin(dataSourceConfiguration.getLogin() + System.nanoTime());
        DataSourceService service = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        service.open(dataSourceConfiguration);

    }

    @Test
    public void testOpen() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {

        ObserveDataSourceConfigurationRest dataSourceConfiguration = restTestMethodResource.getDataSourceConfiguration();

        DataSourceService service1 = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        ObserveDataSourceConnection dataSourceConnection = service1.open(dataSourceConfiguration);
        Assert.assertNotNull(dataSourceConnection);
        Assert.assertNotNull(dataSourceConnection.getAuthenticationToken());

        DataSourceService service2 = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        ObserveDataSourceConnection dataSourceConnection2 = service2.open(dataSourceConfiguration);
        Assert.assertNotNull(dataSourceConnection2);
        Assert.assertNotNull(dataSourceConnection2.getAuthenticationToken());

        service1.close();
        service2.close();

    }

    //FIXME
    @Ignore
    @Test
    public void testCreateEmptyDataSource() throws IOException, IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        //FIXME Should get an not implemented exception for this service
        ObserveDataSourceConfigurationRest dataSourceConfiguration = restTestMethodResource.getDataSourceConfiguration();
        DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
        dataSourceCreateConfiguration.setCanCreateEmptyDatabase(true);

        DataSourceService service = REST_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class);
        service.create(dataSourceConfiguration, dataSourceCreateConfiguration);

    }

}
