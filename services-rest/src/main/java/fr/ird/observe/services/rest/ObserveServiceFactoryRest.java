package fr.ird.observe.services.rest;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.Reflection;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceFactorySupport;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRestConstants;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConnectionRest;
import fr.ird.observe.services.dto.gson.ObserveDtoGsonSupplier;
import fr.ird.observe.services.http.ObserveHttpError;
import fr.ird.observe.services.rest.http.ObserveRequest;
import fr.ird.observe.services.rest.http.ObserveRequestBuilder;
import fr.ird.observe.services.rest.http.ObserveRequestMethod;
import fr.ird.observe.services.rest.http.ObserveResponse;
import fr.ird.observe.services.rest.http.ObserveResponseBuilder;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.spi.DeleteRequest;
import fr.ird.observe.services.spi.PostRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.TimeLog;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.TypeVariable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveServiceFactoryRest extends ObserveServiceFactorySupport implements ObserveDataSourceConfigurationRestConstants {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveServiceFactoryRest.class);
    private static final TimeLog TIME_LOG = new TimeLog(ObserveServiceFactoryRest.class, 100, 1000);

    protected final Supplier<Gson> gsonSupplier = new ObserveDtoGsonSupplier() {

        @Override
        protected GsonBuilder getGsonBuilder(boolean prettyPrint) {

            GsonBuilder builder = super.getGsonBuilder(prettyPrint);

            // Les ObserveDataSourceConnection sont obligatoirement de type ObserveDataSourceConnectionRest
            builder.registerTypeAdapter(ObserveDataSourceConnection.class, new ObserveDataSourceConnectionAdapter());
            builder.registerTypeAdapter(ObserveHttpError.class, new ObserveHttpErrorAdapter());
            return builder;

        }
    };

    protected final ObserveResponseBuilder responseBuilder = ObserveResponseBuilder.create(gsonSupplier);

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {

        Objects.requireNonNull(dataSourceConfiguration, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return dataSourceConfiguration instanceof ObserveDataSourceConfigurationRest;

    }

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {

        Objects.requireNonNull(dataSourceConnection, "dataSourceConfiguration can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        return dataSourceConnection instanceof ObserveDataSourceConnectionRest;

    }

    @Override
    public <S extends ObserveService> S newService(ObserveServiceInitializer observeServiceInitializer, Class<S> serviceType) {

        Objects.requireNonNull(observeServiceInitializer, "observeServiceInitializerContext can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");
        Objects.requireNonNull(observeServiceInitializer.getApplicationLocale(), "applicationLocale can't be null.");
        Objects.requireNonNull(observeServiceInitializer.getReferentialLocale(), "referentialLocale can't be null.");
        Objects.requireNonNull(observeServiceInitializer.getTemporaryDirectoryRoot(), "temporaryDirectoryRoot can't be null.");

        if (observeServiceInitializer.withDataSourceConnection()) {

            // Connecté
            ObserveDataSourceConnection dataSourceConnection = observeServiceInitializer.getDataSourceConnection();
            Preconditions.checkArgument(dataSourceConnection instanceof ObserveDataSourceConnectionRest, "dataSourceConfiguration must be of type " + ObserveDataSourceConnectionRest.class.getName());

        } else {

            // Pas encore connecté on utilise la configuration de la source de données
            ObserveDataSourceConfiguration dataSourceConfiguration = observeServiceInitializer.getDataSourceConfiguration();
            Objects.requireNonNull(dataSourceConfiguration, "dataSourceConnection and dataSourceConfiguration can't be null.");
            Preconditions.checkArgument(dataSourceConfiguration instanceof ObserveDataSourceConfigurationRest, "dataSourceConfiguration must be of type " + ObserveDataSourceConfigurationRest.class.getName());

        }

        return newRemoteProxyServiceInstance(serviceType, observeServiceInitializer);

    }

    @Override
    public void close() {
        responseBuilder.close();
    }

    private <S extends ObserveService> S newRemoteProxyServiceInstance(Class<S> serviceType, ObserveServiceInitializer observeServiceInitializer) {

        RemoteInvocationHandler handler = new RemoteInvocationHandler<>(serviceType, observeServiceInitializer, gsonSupplier, responseBuilder);
        return Reflection.newProxy(serviceType, handler);

    }

    protected static class RemoteInvocationHandler<E extends ObserveService> implements InvocationHandler {

        protected final ObserveServiceInitializer observeServiceInitializer;

        protected final Supplier<Gson> gsonSupplier;

        protected final Class<E> serviceClass;

        protected final String applicationLocale;

        protected final String referentialLocale;

        protected final String speciesListConfigurationAsString;

        protected final String locateService;

        protected final ObserveResponseBuilder responseBuilder;

        public RemoteInvocationHandler(Class<E> serviceClass, ObserveServiceInitializer observeServiceInitializer, Supplier<Gson> gsonSupplier, ObserveResponseBuilder responseBuilder) {
            this.serviceClass = serviceClass;
            this.locateService = serviceClass.getCanonicalName().replace(ROOT_SERVICES_PACKAGE.getName(), "").replace(".", "/");

            this.observeServiceInitializer = observeServiceInitializer;

            if (!observeServiceInitializer.withDataSourceConnection() && !observeServiceInitializer.withDataSourceConfiguration()) {

                throw new IllegalStateException("No data source configuration, nor connection defined");

            }
            this.gsonSupplier = gsonSupplier;
            this.applicationLocale = observeServiceInitializer.getApplicationLocale().toString();
            this.referentialLocale = observeServiceInitializer.getReferentialLocale().getLocale().toString();
            this.speciesListConfigurationAsString = gsonSupplier.get().toJson(observeServiceInitializer.getSpeciesListConfiguration());
            this.responseBuilder = responseBuilder;
        }

        protected String getServiceUrl() {
            String serviceUrl;
            if (observeServiceInitializer.withDataSourceConnection()) {

                ObserveDataSourceConnectionRest dataSourceConnection = (ObserveDataSourceConnectionRest) observeServiceInitializer.getDataSourceConnection();
                serviceUrl = dataSourceConnection.getServerUrl() + locateService;

            } else if (observeServiceInitializer.withDataSourceConfiguration()) {

                ObserveDataSourceConfigurationRest dataSourceConfiguration = (ObserveDataSourceConfigurationRest) observeServiceInitializer.getDataSourceConfiguration();
                serviceUrl = dataSourceConfiguration.getServerUrl() + locateService;

            } else {

                throw new IllegalStateException("No data source configuration, nor connection defined");

            }
            return serviceUrl;
        }

        protected ObserveDataSourceConfigurationRest getDataSourceConfiguration() {
            ObserveDataSourceConfigurationRest dataSourceConnection = null;
            if (observeServiceInitializer.withDataSourceConfiguration()) {
                dataSourceConnection = (ObserveDataSourceConfigurationRest) observeServiceInitializer.getDataSourceConfiguration();
            }
            return dataSourceConnection;
        }

        protected ObserveDataSourceConnectionRest getDataSourceConnection() {
            ObserveDataSourceConnectionRest dataSourceConnection = null;
            if (observeServiceInitializer.withDataSourceConnection()) {
                dataSourceConnection = (ObserveDataSourceConnectionRest) observeServiceInitializer.getDataSourceConnection();
            }
            return dataSourceConnection;
        }


        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            String methodName = method.getName();
            if ("toString".equals(methodName)) {
                return String.format("%s<%s>.toString()", RemoteInvocationHandler.class.getName(), serviceClass.getName());
            }
            if ("hashcode".equals(methodName)) {
                return String.format("%s<%s>.hashcode()", RemoteInvocationHandler.class.getName(), serviceClass.getName()).hashCode();
            }
            if ("equals".equals(methodName)) {
                return false;
            }
            if ("finalize".equals(methodName)) {
                return null;
            }
            if ("clone".equals(methodName)) {
                return null;
            }

            long t0 = TimeLog.getTime();

            Object result;
            try {
                ObserveRequest request = createRequest(method, methodName, args);

                ObserveResponse<?> response = responseBuilder.build(request, method.getGenericReturnType());

                result = response.getResultObject();

                if (DataSourceService.class.equals(serviceClass)) {

                    if (OPEN_CONNECTION_METHOD_NAMES.contains(methodName)) {

                        ObserveDataSourceConnection dataSourceConnection = (ObserveDataSourceConnection) result;

                        // On enregistre la connexion
                        observeServiceInitializer.setDataSourceConnection(dataSourceConnection);

                        // On supprime la configuration
                        observeServiceInitializer.setDataSourceConfiguration(null);

                    } else if (CLOSE_CONNECTION_METHOD_NAMES.contains(methodName)) {

                        // On supprime la connexion
                        observeServiceInitializer.setDataSourceConnection(null);

                    }
                }
            } finally {
                TIME_LOG.log(t0, "invokeMethod", methodName);
            }

            return result;

        }

        protected ObserveRequest createRequest(Method method, String methodName, Object... args) throws IOException {

            ObserveRequestBuilder requestBuilder = ObserveRequestBuilder.create(gsonSupplier);
            addParameters(requestBuilder, method, args);
            addHeaders(requestBuilder);

            boolean post = method.isAnnotationPresent(PostRequest.class);
            boolean delete = method.isAnnotationPresent(DeleteRequest.class);
            if (post) {
                requestBuilder.setRequestMethod(ObserveRequestMethod.POST);
            } else if (delete) {
                requestBuilder.setRequestMethod(ObserveRequestMethod.DELETE);
            } else {
                requestBuilder.setRequestMethod(ObserveRequestMethod.GET);
            }

            String serviceUrl = getServiceUrl();
            String url = String.format("%s/%s", serviceUrl, methodName);
            if (log.isDebugEnabled()) {
                log.debug(String.format("Invoke remote service on endpoint: %s", url));
            }

            return requestBuilder.build(url);

        }

        protected static final ImmutableSet<String> OPEN_CONNECTION_METHOD_NAMES = ImmutableSet.of("create", "open");

        protected static final ImmutableSet<String> CLOSE_CONNECTION_METHOD_NAMES = ImmutableSet.of("close");

        protected void addParameters(ObserveRequestBuilder requestBuilder, Method method, Object... args) throws IOException {

            boolean reinjectConfiguration = false;

            if (DataSourceService.class.equals(serviceClass)) {

                if (OPEN_CONNECTION_METHOD_NAMES.contains(method.getName()) && getDataSourceConnection() == null && getDataSourceConfiguration() == null) {

                    // On va essayer de réinjecter la configuration
                    reinjectConfiguration = true;

                }

            }

            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);

            int index = 0;
            for (Parameter parameter : method.getParameters()) {
                String name = parameter.getName();
                if (args.length >= (index + 1)) {
                    Object value = args[index];
                    if (value != null && value instanceof String) {
                        requestBuilder.addParameter(name, (String) value); // Make sure String is not converted to JSON
                    } else if (value != null && value instanceof Collection) {
                        requestBuilder.addParameter(name, (Collection) value); // Make sure List is not converted to JSON
                    } else if (value != null && value instanceof Date) {
                        requestBuilder.addParameter(name, dateFormat.format(value));
                    } else if (value != null) {
                        requestBuilder.addParameter(name, value);
                    }
                    // si c'est un type parametré, on envoie la class correspondante a ce type
                    if (parameter.getParameterizedType() instanceof TypeVariable) {
                        TypeVariable typeVariable = (TypeVariable) parameter.getParameterizedType();
                        requestBuilder.addParameter(PARAMETERIZED_TYPE_PREFIX + typeVariable.getName(), value.getClass().getCanonicalName());

                        if (log.isInfoEnabled()) {
                            log.info("add parameterized type " + typeVariable.getName() + " as " + value.getClass() + " into request");
                        }

                    }

                }

                if (reinjectConfiguration && PARAMETER_DATA_SOURCE_CONFIGURATION.equals(name)) {
                    observeServiceInitializer.setDataSourceConfiguration((ObserveDataSourceConfiguration) args[index]);
                }

                index++;

            }

        }

        protected void addHeaders(ObserveRequestBuilder requestBuilder) {

            ObserveDataSourceConnectionRest dataSourceConnection = getDataSourceConnection();
            if (dataSourceConnection != null) {

                requestBuilder.addHeader(REQUEST_AUTHENTICATION_TOKEN, dataSourceConnection.getAuthenticationToken());

            }

            if (!Strings.isNullOrEmpty(applicationLocale)) {
                requestBuilder.addHeader(REQUEST_APPLICATION_LOCALE, applicationLocale);
            }
            if (!Strings.isNullOrEmpty(referentialLocale)) {
                requestBuilder.addHeader(REQUEST_REFERENTIAL_LOCALE, referentialLocale);
            }

            requestBuilder.addHeader(REQUEST_SPECIES_LIST_CONFIGURATION, speciesListConfigurationAsString);

        }

    }

}
