package fr.ird.observe.services.rest.http;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.http.NameValuePair;

import java.io.File;

/**
 * Created on 06/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveRequest {

    private final ObserveRequestMethod requestMethod;

    private final String baseUrl;

    private final String contentType;

    private final String requestBody;

    private final ImmutableMap<String, String> headers;

    private final ImmutableList<NameValuePair> parameters;

    private final ImmutableMap<String, File> files;

    public ObserveRequest(ObserveRequestMethod requestMethod,
                          String baseUrl,
                          String requestBody,
                          String contentType,
                          ImmutableMap<String, String> header,
                          ImmutableList<NameValuePair> params,
                          ImmutableMap<String, File> files) {
        this.requestMethod = requestMethod;
        this.baseUrl = baseUrl;
        this.requestBody = requestBody;
        this.contentType = contentType;
        this.headers = header;
        this.parameters = params;
        this.files = files;
    }

    public ObserveRequestMethod getRequestMethod() {
        return requestMethod;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getContentType() {
        return contentType;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public ImmutableMap<String, String> getHeaders() {
        return headers;
    }

    public ImmutableList<NameValuePair> getParameters() {
        return parameters;
    }

    public ImmutableMap<String, File> getFiles() {
        return files;
    }

    public boolean withoutFiles() {
        return files.isEmpty();
    }
}
