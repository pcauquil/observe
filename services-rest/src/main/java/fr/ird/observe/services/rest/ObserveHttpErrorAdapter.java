package fr.ird.observe.services.rest;

/*-
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.services.http.ObserveHttpError;

import java.lang.reflect.Type;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveHttpErrorAdapter implements JsonDeserializer<ObserveHttpError> {
    @Override
    public ObserveHttpError deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        Integer httpCode = context.deserialize(jsonObject.get(ObserveHttpError.PROPERTY_HTTP_CODE), Integer.class);

        String message = context.deserialize(jsonObject.get(ObserveHttpError.PROPERTY_MESSAGE), String.class);

        Class exceptionType = context.deserialize(jsonObject.get(ObserveHttpError.PROPERTY_EXECPTION_TYPE), Class.class);

        Throwable exception = null;

        if (exceptionType != null) {

            exception = context.deserialize(jsonObject.get(ObserveHttpError.PROPERTY_EXCEPTION), exceptionType);

        }

        return new ObserveHttpError(httpCode, exceptionType, message, exception);
    }
}
