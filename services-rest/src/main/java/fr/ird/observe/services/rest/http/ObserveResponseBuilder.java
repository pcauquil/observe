package fr.ird.observe.services.rest.http;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import fr.ird.observe.services.http.ObserveHttpError;
import fr.ird.observe.services.rest.ObserveServiceRestErrorException;
import fr.ird.observe.services.rest.ObserveServiceRestNotAvailableException;
import fr.ird.observe.services.dto.gson.ObserveDtoGsonSupplier;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveResponseBuilder {

    private static final Log log = LogFactory.getLog(ObserveResponseBuilder.class);

    public static ObserveResponseBuilder create(Supplier<Gson> gsonSupplier) {
        return new ObserveResponseBuilder(new DefaultHttpClient(new PoolingClientConnectionManager()), gsonSupplier);
    }

    public static ObserveResponseBuilder create(HttpClient client, Supplier<Gson> gsonSupplier) {
        return new ObserveResponseBuilder(client, gsonSupplier);
    }

    protected final HttpClient client;

    protected final Gson gson;

    protected class ResponseContext {

        final Integer statusCode;

        final String responseAsString;

        final Header[] responseHeaders;

        public ResponseContext(Integer statusCode, String responseAsString, Header... responseHeaders) {
            this.statusCode = statusCode;
            this.responseAsString = responseAsString;
            this.responseHeaders = responseHeaders;
        }

    }

    public <T> ObserveResponse<T> build(ObserveRequest request, Type resultType) throws Throwable {

        String baseUrl = request.getBaseUrl();
        Objects.requireNonNull(baseUrl, "'baseUrl' can't be null");

        ObserveRequestMethod requestMethod = request.getRequestMethod();
        Preconditions.checkState(requestMethod != null, "'requestMethod' was not setted");

        ResponseContext responseContext = executeRequest(request);

        if (responseContext.statusCode != 200) {

            if (log.isWarnEnabled()) {
                log.warn(String.format("Unexpected status code for url: %s\n%s", baseUrl, responseContext.responseAsString));
            }

            if (responseContext.statusCode >= 400 && responseContext.statusCode < 500) {

                throw new ObserveServiceRestNotAvailableException(new URL(baseUrl));

            } else {
                ObserveHttpError error;

                try {
                    error = convertJson(responseContext.responseAsString, ObserveHttpError.class);
                } catch (Exception e) {
                    // si le reponse n'est pas un Json on envoie la reponse tel quel;
                    throw new ObserveServiceRestErrorException(responseContext.statusCode + " : \n" + responseContext.responseAsString);
                }

                if (error == null) {

                    throw new ObserveServiceRestErrorException("" + responseContext.statusCode);

                } else {

                    if (error.getException() != null) {

                        throw error.getException();

                    } else {

                        throw new ObserveServiceRestErrorException(error.getHttpCode() + " : " + error.getMessage());

                    }
                }
            }
        }

        T resultObject = convertJson(responseContext.responseAsString, resultType);

        return new ObserveResponse<>(responseContext.statusCode, responseContext.responseHeaders, responseContext.responseAsString, resultObject);

    }

    protected ResponseContext executeRequest(ObserveRequest request) throws IOException {

        Pair<? extends HttpRequestBase, HttpResponse> responsePair = null;

        try {

            ObserveRequestMethod requestMethod = request.getRequestMethod();
            switch (requestMethod) {
                case GET:
                    responsePair = get0(request);
                    break;
                case POST:
                    responsePair = post0(request, null);
                    break;
                case PUT:
                    responsePair = put0(request);
                    break;
                case DELETE:
                    responsePair = delete0(request);
                    break;
                default:
                    throw new IllegalStateException("Can't come here!");
            }

            HttpResponse response = responsePair.getRight();

            return consumeResponse(request, response);

        } finally {
            if (responsePair != null) {
                close(responsePair);
            }
        }

    }

    protected ResponseContext consumeResponse(ObserveRequest request, HttpResponse response) throws IOException {

        String baseUrl = request.getBaseUrl();

        Header[] responseHeaders = response.getAllHeaders();
        int statusCode = response.getStatusLine().getStatusCode();
        if (log.isDebugEnabled()) {
            log.debug(request.getRequestMethod() + " '" + baseUrl + "' return status code : " + statusCode);
        }

        response.getEntity();

        String responseAsString;

        try (InputStream inputStream = response.getEntity().getContent()) {
            try (StringWriter writer = new StringWriter()) {
                IOUtils.copy(inputStream, writer, "UTF-8");
                responseAsString = writer.toString();
            }
        }
        return new ResponseContext(statusCode, responseAsString, responseHeaders);
    }

    protected ObserveResponseBuilder(HttpClient client, Supplier<Gson> gsonSupplier) {
        this.client = client;
        this.gson = MoreObjects.firstNonNull(gsonSupplier, ObserveDtoGsonSupplier.DEFAULT_GSON_SUPPLIER).get();
    }

    protected Pair<HttpGet, HttpResponse> get0(ObserveRequest request) throws IOException {

        String baseUrl = request.getBaseUrl();
        String url = buildUrlWithParameters(baseUrl, request.getParameters());

        HttpGet getMethod = new HttpGet(url);

        addHeaders(getMethod, request.getHeaders());

        HttpResponse response = executeRequest(getMethod);

        if (log.isDebugEnabled()) {
            log.debug("GET '" + baseUrl + "' return status code : " + response.getStatusLine().getStatusCode());
        }

        return Pair.of(getMethod, response);

    }

    protected Pair<HttpPost, HttpResponse> post0(ObserveRequest request, Integer timeout) throws IOException {

        String baseUrl = request.getBaseUrl();
        String contentType = request.getContentType();
        String requestBody = request.getRequestBody();

        HttpPost postMethod = new HttpPost(baseUrl);

        if (timeout != null) {
            HttpParams httpParams = new BasicHttpParams();
            httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);
            httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout);
            postMethod.setParams(httpParams);
        }

        if (request.withoutFiles()) {
            addHeaders(postMethod, request.getHeaders());
            addParameters(postMethod, contentType, request.getParameters());
            addRequestBody(postMethod, contentType, requestBody);
        } else {
            addHeaders(postMethod, request.getHeaders());
            addParameters(postMethod, contentType, request.getParameters());
            addRequestBody(postMethod, contentType, requestBody);
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            for (Map.Entry<String, File> paramFile : request.getFiles().entrySet()) {
                entity.addPart(paramFile.getKey(), new FileBody(paramFile.getValue()));
            }
            for (NameValuePair param : request.getParameters()) {
                if (StringUtils.isBlank(contentType)) {
                    contentType = "text/plain";
                }
                entity.addPart(param.getName(), new StringBody(param.getValue(), contentType,
                                                               Charset.forName("UTF-8")));
            }
            postMethod.setEntity(entity);
        }

        HttpResponse response = executeRequest(postMethod);

        if (log.isDebugEnabled()) {
            log.debug("POST '" + baseUrl + "' return status code : " + response.getStatusLine().getStatusCode());
        }

        return Pair.of(postMethod, response);

    }

    protected Pair<HttpPut, HttpResponse> put0(ObserveRequest request) throws IOException {

        String baseUrl = request.getBaseUrl();

        HttpPut putMethod = new HttpPut(baseUrl);
        addHeaders(putMethod, request.getHeaders());

        String contentType = request.getContentType();
        addParameters(putMethod, contentType, request.getParameters());
        addRequestBody(putMethod, contentType, request.getRequestBody());

        HttpResponse response = executeRequest(putMethod);

        if (log.isDebugEnabled()) {
            log.debug("PUT '" + baseUrl + "' return status code : " + response.getStatusLine().getStatusCode());
        }

        return Pair.of(putMethod, response);

    }

    protected Pair<HttpDelete, HttpResponse> delete0(ObserveRequest request) throws IOException {

        String baseUrl = request.getBaseUrl();

        HttpDelete deleteMethod = new HttpDelete(buildUrlWithParameters(baseUrl, request.getParameters()));

        addHeaders(deleteMethod, request.getHeaders());

        HttpResponse response = executeRequest(deleteMethod);

        if (log.isDebugEnabled()) {
            log.debug("DELETE '" + baseUrl + "' return status code : " + response.getStatusLine().getStatusCode());
        }

        return Pair.of(deleteMethod, response);

    }

    protected HttpResponse executeRequest(HttpRequestBase request) throws IOException, ObserveServiceRestNotAvailableException {

        try {
            return client.execute(request);
        } catch (UnknownHostException | ConnectException e) {
            // Le service n'est pas accessible
            throw new ObserveServiceRestNotAvailableException(request.getURI().toURL());
        }

    }

    protected String buildUrlWithParameters(String baseUrl, List<NameValuePair> parameters) {
        String result = baseUrl;
        if (!parameters.isEmpty()) {
            result += "?" + URLEncodedUtils.format(parameters, Charsets.UTF_8);
        }
        return result;
    }

    protected <M extends HttpRequestBase> void addHeaders(M httpMethod, Map<String, String> headers) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            httpMethod.setHeader(entry.getKey(), entry.getValue());
        }
    }

    protected <M extends HttpEntityEnclosingRequestBase> void addParameters(M method, String contentType, List<? extends NameValuePair> parameters) {
        UrlEncodedFormEntity encodedFormEntity = new UrlEncodedFormEntity(parameters, Charsets.UTF_8);
        if (StringUtils.isNotBlank(contentType)) {
            encodedFormEntity.setContentType(contentType);
        }
        method.setEntity(encodedFormEntity);
    }

    protected <M extends HttpEntityEnclosingRequestBase> void addRequestBody(M method, String contentType, String requestBody) throws UnsupportedEncodingException {
        if (StringUtils.isNotEmpty(requestBody)) {
            if (StringUtils.isNotBlank(contentType)) {
                method.setEntity(new StringEntity(requestBody, ContentType.parse(contentType)));
            } else {
                method.setEntity(new StringEntity(requestBody));
            }
        }
    }

    public <T> T convertJson(String json, Type type) {

        Objects.requireNonNull(json);
        Objects.requireNonNull(gson);

        // we must try to convert json to a T instance
        T result = null;

//        try {

        // let's first try to convert json to T
        if (!Void.TYPE.equals(type)) {
            result = gson.fromJson(json, type);
        }

//        } catch (IOException e) {
//
//            // conversion to T failed, we will throw a json exception
//            RemoteCallUnexpectedJsonException remoteCallUnexpectedJsonException;
//
//            try {
//
//                // before, try to read json as an error message
//                remoteCallUnexpectedJsonException = RemoteCallUnexpectedJsonException.newFromJson(json);
//
//                if (log.isInfoEnabled()) {
//                    log.info("service returned a json showing an error occurred ", remoteCallUnexpectedJsonException);
//                }
//
//            } catch (IOException ee) {
//
//                if (log.isErrorEnabled()) {
//                    log.error("json parsing failed, json=" + json, e);
//                }
//
//                // json is not of type T and not an error message,
//                // throw the exception with e as cause cause it may be the real problem
//                remoteCallUnexpectedJsonException = RemoteCallUnexpectedJsonException.newFromParsingException(e);
//
//            }
//
//            throw remoteCallUnexpectedJsonException;
//
//        }

        if (log.isTraceEnabled()) {
            log.trace("parsing json " + json + " returns " + result);
        }


        return result;
    }

    protected void close(Pair<? extends HttpRequestBase, HttpResponse> responsePair) throws IOException {

        HttpRequestBase httpRequestBase = responsePair.getLeft();
        HttpResponse response = responsePair.getRight();


        if (response != null && response.getEntity() != null) {
            response.getEntity().getContent().close();
        }

        // Release the connection.
        httpRequestBase.releaseConnection();

    }

    public void close() {
        if (getConnectionManager() != null) {
            getConnectionManager().shutdown();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        if (getConnectionManager() != null) {
            getConnectionManager().closeExpiredConnections();
            getConnectionManager().closeIdleConnections(1, TimeUnit.MINUTES);
        }
        super.finalize();
    }

    protected ClientConnectionManager getConnectionManager() {
        return client.getConnectionManager();
    }

}
