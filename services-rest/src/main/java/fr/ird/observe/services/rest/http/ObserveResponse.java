package fr.ird.observe.services.rest.http;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class ObserveResponse<T> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveResponse.class);

    protected final int statusCode;

    protected final Header[] headers;

    protected final String responseAsString;

    protected final T resultObject;

    protected ObserveResponse(int statusCode, Header[] headers, String responseAsString, T resultObject) {
        this.statusCode = statusCode;
        this.headers = headers;
        this.resultObject = resultObject;
        this.responseAsString = responseAsString;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Header[] getHeaders() {
        return headers;
    }

    public String getHeader(String key) {
        return getHeader(key, headers);
    }

    public boolean containsHeader(String key) {
        for (Header header : headers) {
            if (key.equals(header.getName())) {
                return true;
            }
        }
        return false;
    }

    protected String getHeader(String key, Header[] headers) {

        for (Header header : headers) {
            if (key.equals(header.getName())) {
                return header.getValue();
            }
        }
        throw new IllegalArgumentException(key + " key is not found in header");
    }

    public String getResponse() {
        return responseAsString;
    }

    public T getResultObject() {
        return resultObject;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder().
                append("HTTP ").append(statusCode).append('\n');
        if (headers != null) {
            for (Header header : headers) {
                buffer.append('[').append(header.getName()).append("] ").append(header.getValue()).append('\n');
            }
        }
        buffer.append('\n').append(responseAsString);
        return buffer.toString();
    }
}
