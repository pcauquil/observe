package fr.ird.observe.application.web.controller.v1.seine;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.service.seine.ActivitySeineService;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ActivitySeineServiceController extends ObserveAuthenticatedServiceControllerSupport<ActivitySeineService> implements ActivitySeineService {

    public ActivitySeineServiceController() {
        super(ActivitySeineService.class);
    }

    @Override
    public DataReferenceSet<ActivitySeineDto> getActivitySeineByRoute(String routeId) {
        return service.getActivitySeineByRoute(routeId);
    }

    @Override
    public int getActivitySeinePositionInRoute(String routeId, String activitySeineId) {
        return service.getActivitySeinePositionInRoute(routeId, activitySeineId);
    }

    @Override
    public Form<ActivitySeineDto> loadForm(String activitySeineId) {
        return service.loadForm(activitySeineId);
    }

    @Override
    public ActivitySeineDto loadDto(String activitySeineId) {
        return service.loadDto(activitySeineId);
    }

    @Override
    public DataReference<ActivitySeineDto> loadReferenceToRead(String activitySeineId) {
        return service.loadReferenceToRead(activitySeineId);
    }

    @Override
    public boolean exists(String activitySeineId) {
        return service.exists(activitySeineId);
    }

    @Override
    public Form<ActivitySeineDto> preCreate(String routeId) {
        return service.preCreate(routeId);
    }

    @Override
    public SaveResultDto save(String routeId, ActivitySeineDto dto) {
        return service.save(routeId, dto);
    }

    @Override
    public void delete(String routeId, String activitySeineId) {
        service.delete(routeId, activitySeineId);
    }

    @Override
    public int moveActivitySeineToRoute(String activitySeineId, String routeId) {
        return service.moveActivitySeineToRoute(activitySeineId, routeId);
    }

    @Override
    public List<Integer> moveActivitySeinesToRoute(List<String> activitySeineIds, String routeId) {
        return service.moveActivitySeinesToRoute(activitySeineIds, routeId);
    }
}
