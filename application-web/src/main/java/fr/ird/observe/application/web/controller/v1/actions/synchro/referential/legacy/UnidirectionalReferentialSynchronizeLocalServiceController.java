package fr.ird.observe.application.web.controller.v1.actions.synchro.referential.legacy;

/*-
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeLocalService;
import fr.ird.observe.services.service.actions.synchro.referential.legacy.UnidirectionalReferentialSynchronizeRequest;

import java.util.Set;

/**
 * Created on 20/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class UnidirectionalReferentialSynchronizeLocalServiceController extends ObserveAuthenticatedServiceControllerSupport<UnidirectionalReferentialSynchronizeLocalService> implements UnidirectionalReferentialSynchronizeLocalService {

    public UnidirectionalReferentialSynchronizeLocalServiceController() {
        super(UnidirectionalReferentialSynchronizeLocalService.class);
    }

    @Override
    public <R extends ReferentialDto>Set<String> filterIdsUsedInLocalSource(Class<R> referentialName, Set<String> ids) {
        return getAuthenticatedService().filterIdsUsedInLocalSource(referentialName, ids);
    }

    @Override
    public <R extends ReferentialDto> Set<ReferentialReference<R>> getLocalSourceReferentialToDelete(Class<R> referentialName, Set<String> ids) {
        return getAuthenticatedService().getLocalSourceReferentialToDelete(referentialName, ids);
    }

    @Override
    public <R extends ReferentialDto> Set<String> generateSqlRequests(UnidirectionalReferentialSynchronizeRequest<R> request) {
        return getAuthenticatedService().generateSqlRequests(request);
    }

    @Override
    public void applySqlRequests(Set<String> sqlRequests) {
        getAuthenticatedService().applySqlRequests(sqlRequests);
    }

    @Override
    public void updateLastUpdateDates() {
        getAuthenticatedService().updateLastUpdateDates();
    }

}
