package fr.ird.observe.application.web.controller;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.web.configuration.db.InvalidObserveWebDatabaseException;
import fr.ird.observe.application.web.configuration.db.InvalidObserveWebDatabaseRoleException;
import fr.ird.observe.application.web.configuration.db.InvalidObserveWebDatabasesException;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabases;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabasesHelper;
import fr.ird.observe.application.web.configuration.user.InvalidObserveWebUserException;
import fr.ird.observe.application.web.configuration.user.InvalidObserveWebUserPermissionException;
import fr.ird.observe.application.web.configuration.user.InvalidObserveWebUsersException;
import fr.ird.observe.application.web.configuration.user.ObserveWebUsers;
import fr.ird.observe.application.web.configuration.user.ObserveWebUsersHelper;
import fr.ird.observe.application.web.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.services.configuration.ObserveDataSourceConfigurationAndConnection;
import org.apache.commons.io.IOUtils;
import org.debux.webmotion.server.WebMotionContextable;
import org.debux.webmotion.server.render.RenderContent;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Created on 8/30/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ConfigurationController extends ObserveWebMotionController {

    @Override
    public void setContextable(WebMotionContextable contextable) {
        super.setContextable(contextable);
        getRequestContext().checkAdminApiKeyIsValid();
    }

    protected RenderContent toTextPlain(String content) {
        return (RenderContent) renderContent(content, "text/plain");
    }

    public RenderContent home() throws IOException {

        StringBuilder builder = new StringBuilder();

        {
            String content = getApplicationConfiguration().getConfigurationDescription();
            builder.append("\n~~~ Configuration         ~~~\n").append(content);
        }
        {
            try (StringWriter writer = new StringWriter()) {

                ObserveWebDatabasesHelper observeWebDatabasesHelper = new ObserveWebDatabasesHelper();
                ObserveWebDatabases databases = getApplicationContext().getDatabases();
                observeWebDatabasesHelper.store(databases, writer);

                writer.flush();
                builder.append("\n~~~ Databases         ~~~\n").append(writer.toString());
            }

        }
        {
            try (StringWriter writer = new StringWriter()) {

                ObserveWebUsersHelper observeWebUsersHelper = new ObserveWebUsersHelper();
                ObserveWebUsers databases = getApplicationContext().getUsers();
                observeWebUsersHelper.store(databases, writer);

                writer.flush();
                builder.append("\n~~~ Users         ~~~\n").append(writer.toString());
            }

        }
        {
            ObserveWebSecurityApplicationContext securityApplicationContext = getSecurityApplicationContext();

            StringBuilder tokens = new StringBuilder();
            ImmutableMap<String, ObserveDataSourceConfigurationAndConnection> cache = securityApplicationContext.getConfigurationByAuthenticationToken();

            tokens.append("Number of authentication tokens: ").append(cache.size());
            for (Map.Entry<String, ObserveDataSourceConfigurationAndConnection> entry : cache.entrySet()) {
                tokens.append("\n").append(entry.getKey()).append(" - ").append(entry.getValue().getConfiguration());
            }
            builder.append("\n~~~ AuthenticationTokens         ~~~\n").append(tokens.toString());
        }
        {

            try (InputStream mappingUrl = getClass().getResourceAsStream("/mapping")) {
                String content = IOUtils.toString(mappingUrl, StandardCharsets.UTF_8);
                builder.append("\n~~~ Mapping         ~~~\n").append(content);
            }

        }

        return toTextPlain(builder.toString());
    }

    public RenderContent resetAuthenticationTokens() {

        ObserveWebSecurityApplicationContext securityApplicationContext = getSecurityApplicationContext();

        StringBuilder builder = new StringBuilder();
        ImmutableMap<String, ObserveDataSourceConfigurationAndConnection> authenticationTokensCache = securityApplicationContext.getConfigurationByAuthenticationToken();

        builder.append("Number of authentication tokens to reset: ").append(authenticationTokensCache.size());
        for (Map.Entry<String, ObserveDataSourceConfigurationAndConnection> entry : authenticationTokensCache.entrySet()) {
            builder.append("\n").append(entry.getKey()).append(" - ").append(entry.getValue().getConfiguration());
        }

        for (String authenticationToken : authenticationTokensCache.keySet()) {
            securityApplicationContext.invalidateAuthenticationToken(authenticationToken);
        }

        return toTextPlain(builder.toString());

    }

    public RenderContent reloadConfiguration() throws IOException, InvalidObserveWebUserPermissionException, InvalidObserveWebDatabaseException, InvalidObserveWebDatabasesException, InvalidObserveWebDatabaseRoleException, InvalidObserveWebUsersException, InvalidObserveWebUserException {

        // Reset all connexions

        ObserveWebSecurityApplicationContext securityApplicationContext = getSecurityApplicationContext();
        ImmutableMap<String, ObserveDataSourceConfigurationAndConnection> authenticationTokensCache = securityApplicationContext.getConfigurationByAuthenticationToken();

        for (String authenticationToken : authenticationTokensCache.keySet()) {
            securityApplicationContext.invalidateAuthenticationToken(authenticationToken);
        }

        getApplicationContext().reloadConfiguration();

        return home();

    }

}
