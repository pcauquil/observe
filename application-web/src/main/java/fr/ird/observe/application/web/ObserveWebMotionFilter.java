package fr.ird.observe.application.web;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.gson.Gson;
import fr.ird.observe.application.web.request.ObserveWebRequestContext;
import fr.ird.observe.services.dto.ObserveSpeciesListConfiguration;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRestConstants;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import org.apache.commons.lang3.StringUtils;
import org.debux.webmotion.server.WebMotionFilter;
import org.debux.webmotion.server.call.HttpContext;
import org.nuiton.converter.ConverterUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebMotionFilter extends WebMotionFilter implements ObserveDataSourceConfigurationRestConstants {

    public void inject(HttpContext context) {

        ObserveWebApplicationContext applicationContext = ObserveWebApplicationContext.getApplicationContext(context);

        HttpServletRequest request = context.getRequest();

        Locale applicationLocale = getApplicationLocale(request);
        ReferentialLocale referentialLocale = getReferentialLocale(request);
        ObserveSpeciesListConfiguration speciesListConfiguration = getSpeciesListConfiguration(applicationContext.getGsonSupplier().get(), request);

        String adminApiKey = getRequestHeaderOrParameterValueOrNull(request, REQUEST_ADMIN_API_KEY);
        if (Strings.isNullOrEmpty(adminApiKey)) {
            adminApiKey = null;
        }

        String authenticationToken = getRequestHeaderOrParameterValueOrNull(request, REQUEST_AUTHENTICATION_TOKEN);
        if (Strings.isNullOrEmpty(authenticationToken)) {
            authenticationToken = null;
        }

        ObserveWebRequestContext requestContext = new ObserveWebRequestContext(applicationContext, applicationLocale, referentialLocale, speciesListConfiguration, adminApiKey, authenticationToken);
        ObserveWebRequestContext.setRequestContext(context, requestContext);

        doProcess();

    }

    protected ReferentialLocale getReferentialLocale(HttpServletRequest request) {

        ReferentialLocale referentialLocale = null;
        String referentialLocaleStr = getRequestHeaderOrParameterValueOrNull(request, REQUEST_REFERENTIAL_LOCALE);
        if (referentialLocaleStr != null) {
            Locale referentialLoca = ConverterUtil.convert(Locale.class, referentialLocaleStr);
            referentialLocale = ReferentialLocale.valueOf(referentialLoca);
        }
        return referentialLocale;

    }

    protected ObserveSpeciesListConfiguration getSpeciesListConfiguration(Gson gson, HttpServletRequest request) {

        ObserveSpeciesListConfiguration speciesListConfiguration = null;
        String speciesListConfigurationStr = getRequestHeaderOrParameterValueOrNull(request, REQUEST_SPECIES_LIST_CONFIGURATION);
        if (speciesListConfigurationStr != null) {
            speciesListConfiguration = gson.fromJson(speciesListConfigurationStr, ObserveSpeciesListConfiguration.class);
        }
        return speciesListConfiguration;

    }

    protected Locale getApplicationLocale(HttpServletRequest request) {

        Locale applicationLocale = null;
        String applicationLocaleStr = getRequestHeaderOrParameterValueOrNull(request, REQUEST_APPLICATION_LOCALE);
        if (applicationLocaleStr != null) {
            applicationLocale = ConverterUtil.convert(Locale.class, applicationLocaleStr);
        }
        return applicationLocale;

    }

    protected String getRequestHeaderOrParameterValueOrNull(HttpServletRequest request, String parameterName) {

        String result = request.getHeader(parameterName);
        if (StringUtils.isBlank(result)) {
            result = getRequestParameterValueOrNull(request, parameterName);
        }
        return result;

    }

    protected String getRequestParameterValueOrNull(HttpServletRequest request, String parameterName) {

        String parameterValue = request.getParameter(parameterName);
        if (StringUtils.isBlank(parameterValue)) {
            parameterValue = null;
        }
        return parameterValue;

    }

}
