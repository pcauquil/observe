package fr.ird.observe.application.web.controller.v1.seine;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.seine.TripSeineService;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripSeineServiceController extends ObserveAuthenticatedServiceControllerSupport<TripSeineService> implements TripSeineService {

    public TripSeineServiceController() {
        super(TripSeineService.class);
    }

    @Override
    public DataReferenceSet<TripSeineDto> getAllTripSeine() {
        return service.getAllTripSeine();
    }

    @Override
    public DataReferenceSet<TripSeineDto> getTripSeineByProgram(String programId) {
        return service.getTripSeineByProgram(programId);
    }

    @Override
    public int getTripSeinePositionInProgram(String programId, String tripSeineId) {
        return service.getTripSeinePositionInProgram(programId, tripSeineId);
    }

    @Override
    public TripMapDto getTripSeineMap(String tripSeineId) {
        return service.getTripSeineMap(tripSeineId);
    }

    @Override
    public Form<TripSeineDto> loadForm(String tripSeineId) {
        return service.loadForm(tripSeineId);
    }

    @Override
    public TripSeineDto loadDto(String tripSeineId) {
        return service.loadDto(tripSeineId);
    }

    @Override
    public DataReference<TripSeineDto> loadReferenceToRead(String tripSeineId) {
        return service.loadReferenceToRead(tripSeineId);
    }

    @Override
    public boolean exists(String tripSeineId) {
        return service.exists(tripSeineId);
    }

    @Override
    public Form<TripSeineDto> preCreate(String programId) {
        return service.preCreate(programId);
    }

    @Override
    public SaveResultDto save(TripSeineDto dto) {
        return service.save(dto);
    }

    @Override
    public void delete(String tripSeineId) {
        service.delete(tripSeineId);
    }

    @Override
    public int moveTripSeineToProgram(String tripSeineId, String programId) {
        return service.moveTripSeineToProgram(tripSeineId, programId);
    }

    @Override
    public List<Integer> moveTripSeinesToProgram(List<String> tripSeineIds, String programId) {
        return service.moveTripSeinesToProgram(tripSeineIds, programId);
    }

    @Override
    public ImmutableList<ReferentialReference<SpeciesDto>> getSpeciesByListAndTrip(String tripSeineId, String speciesListId) {
        return service.getSpeciesByListAndTrip(tripSeineId, speciesListId);
    }
}
