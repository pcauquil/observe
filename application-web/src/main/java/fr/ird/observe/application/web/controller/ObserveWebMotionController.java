package fr.ird.observe.application.web.controller;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.ObserveWebApplicationContext;
import fr.ird.observe.application.web.configuration.ObserveWebApplicationConfiguration;
import fr.ird.observe.application.web.request.ObserveWebRequestContext;
import fr.ird.observe.application.web.security.ObserveWebSecurityApplicationContext;
import org.debux.webmotion.server.WebMotionController;

/**
 * @author Tony Chemit - tchemit@codelutin.com
 */
public abstract class ObserveWebMotionController extends WebMotionController {

    protected ObserveWebApplicationContext getApplicationContext() {
        return getRequestContext().getApplicationContext();
    }

    protected ObserveWebSecurityApplicationContext getSecurityApplicationContext() {
        return getApplicationContext().getSecurityApplicationContext();
    }

    protected ObserveWebApplicationConfiguration getApplicationConfiguration() {
        return getApplicationContext().getApplicationConfiguration();
    }

    protected ObserveWebRequestContext getRequestContext() {
        return ObserveWebRequestContext.getRequestContext(getContext());
    }

//    public <S extends ObserveService> S newService(Class<S> serviceType) {
//        ObserveWebRequestContext requestContext = getRequestContext();
//
//        S service = requestContext.newService(serviceType);
//        return service;
//    }

}
