package fr.ird.observe.application.web.security;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import fr.ird.observe.services.configuration.ObserveDataSourceConfigurationAndConnection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Closeable;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Le cache des jetons d'authentification.
 *
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebSecurityAuthenticationTokenCache implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveWebSecurityAuthenticationTokenCache.class);

    /**
     * Le cache des jetons de sécurité liés aux configurations de sources de données.
     */
    protected final Cache<String, ObserveDataSourceConfigurationAndConnection> authenticationTokenCache;

    public ObserveWebSecurityAuthenticationTokenCache(int maximumSize, int expireDelay) {
        this.authenticationTokenCache = CacheBuilder.newBuilder()
                .maximumSize(maximumSize)
                .expireAfterWrite(expireDelay, TimeUnit.MINUTES)
                .expireAfterAccess(expireDelay, TimeUnit.MINUTES)
                .removalListener((RemovalListener<String, ObserveDataSourceConfigurationAndConnection>) notification -> {
                    if (log.isInfoEnabled()) {
                        log.info(String.format("Remove authentication token: %s - %s", notification.getKey(), notification.getValue()));
                    }
                })
                .build();
    }

    public ObserveDataSourceConfigurationAndConnection getDataSourceConfigurationAndConnectionIfPresent(String authenticationToken) {

        return authenticationTokenCache.getIfPresent(authenticationToken);
    }


    public String registerDataSourceConfiguration(ObserveDataSourceConfigurationAndConnection configurationAndConnection) {
        String authenticationToken = UUID.randomUUID().toString();
        if (log.isInfoEnabled()) {
            log.info(String.format("Add authenticationToken: %s for data source configuration: %s", authenticationToken, configurationAndConnection.getConfiguration()));
        }
        authenticationTokenCache.put(authenticationToken, configurationAndConnection);
        return authenticationToken;
    }

    public void removeAuthenticationToken(String authenticationToken) {
        if (log.isInfoEnabled()) {
            log.info(String.format("Remove authenticationToken: %s ", authenticationToken));
        }
        authenticationTokenCache.invalidate(authenticationToken);
    }

    public void removeAllAuthenticationTokens() {
        if (log.isInfoEnabled()) {
            log.info("Remove all authenticationTokens");
        }
        authenticationTokenCache.invalidateAll();
    }

    public Cache<String, ObserveDataSourceConfigurationAndConnection> getAuthenticationTokenCache() {
        return authenticationTokenCache;
    }

    @Override
    public void close() {
        removeAllAuthenticationTokens();
    }

}
