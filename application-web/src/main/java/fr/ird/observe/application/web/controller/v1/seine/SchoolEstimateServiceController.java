package fr.ird.observe.application.web.controller.v1.seine;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.SetSeineSchoolEstimateDto;
import fr.ird.observe.services.service.seine.SchoolEstimateService;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SchoolEstimateServiceController extends ObserveAuthenticatedServiceControllerSupport<SchoolEstimateService> implements SchoolEstimateService {

    public SchoolEstimateServiceController() {
        super(SchoolEstimateService.class);
    }

    @Override
    public Form<SetSeineSchoolEstimateDto> loadForm(String setSeineId) {
        return service.loadForm(setSeineId);
    }

    @Override
    public SaveResultDto save(SetSeineSchoolEstimateDto dto) {
        return service.save(dto);
    }
}
