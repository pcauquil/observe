package fr.ird.observe.application.web.injector;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRestConstants;
import fr.ird.observe.services.dto.ObserveDto;
import org.apache.commons.beanutils.converters.AbstractConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/**
 * Created on 07/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveDtoInjector extends AbstractConverter implements ExecutorParametersInjectorHandler.Injector  {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveDtoInjector.class);

    protected final Gson gson;

    public ObserveDtoInjector(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {

        ObserveDto observeDto = null;
        if (ObserveDto.class.isAssignableFrom(type)) {

            Call.ParameterTree parameterTree = call.getParameterTree().getObject().get(name);

            String gsonContent= ((String[])parameterTree.getValue())[0];

            if (generic instanceof TypeVariable) {
                TypeVariable typeVariable = (TypeVariable) generic;

                String parameterizedTypeParam = ObserveDataSourceConfigurationRestConstants.PARAMETERIZED_TYPE_PREFIX + typeVariable.getName();

                Call.ParameterTree parameterizedTypeParameterTree = call.getParameterTree().getObject().get(parameterizedTypeParam);

                String parameterizedTypeClassName= ((String[])parameterizedTypeParameterTree.getValue())[0];

                try {
                    generic = Class.forName(parameterizedTypeClassName);
                } catch (ClassNotFoundException cnfe) {
                    if (log.isErrorEnabled()) {
                        log.error("An exception occurred", cnfe);
                    }
                    throw new JsonParseException("Class not found: " + parameterizedTypeClassName, cnfe);
                }

                if (log.isInfoEnabled()) {
                    log.info("find parameterized type " + typeVariable.getName() + " as " + parameterizedTypeClassName);
                }

            }

            observeDto = gson.fromJson(gsonContent, generic);

            if (log.isInfoEnabled()) {
                log.info("Inject observeDto: " + observeDto);
            }

        }

        return observeDto;

    }

    @Override
    protected <T> T convertToType(Class<T> type, Object value) throws Throwable {
        T observeDto;

        if (ObserveDto.class.isAssignableFrom(type)) {

            String gsonContent = value.toString();

            observeDto = gson.fromJson(gsonContent, type);

            if (log.isInfoEnabled()) {
                log.info("convert observeDto: " + observeDto);
            }

        } else {
            throw conversionException(type, value);
        }

        return observeDto;

    }

    @Override
    protected Class<?> getDefaultType() {
        return ObserveDto.class;
    }
}
