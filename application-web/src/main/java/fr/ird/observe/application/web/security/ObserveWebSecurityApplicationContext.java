package fr.ird.observe.application.web.security;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.web.configuration.ObserveWebApplicationConfiguration;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabase;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabaseRole;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabases;
import fr.ird.observe.application.web.configuration.user.ObserveWebUser;
import fr.ird.observe.application.web.configuration.user.ObserveWebUserPermission;
import fr.ird.observe.application.web.configuration.user.ObserveWebUsers;
import fr.ird.observe.services.runner.ObserveDataSourceConfigurationMainFactory;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfigurationAndConnection;
import fr.ird.observe.services.security.BadObserveWebUserPasswordException;
import fr.ird.observe.services.security.InvalidAuthenticationTokenException;
import fr.ird.observe.services.security.UnknownObserveWebUserException;
import fr.ird.observe.services.security.UnknownObserveWebUserForDatabaseException;
import fr.ird.observe.services.security.UserLoginNotFoundException;
import fr.ird.observe.services.security.UserPasswordNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

import java.io.Closeable;
import java.util.Objects;
import java.util.Optional;

/**
 * Pour conserver les données applicatives liée à la sécurité (principale le cache des utilisateurs connectés).
 *
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebSecurityApplicationContext implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveWebSecurityApplicationContext.class);

    /**
     * Le cache des jetons de sécurité générés liés aux configurations de sources de données.
     */
    protected final ObserveWebSecurityAuthenticationTokenCache authenticateCache;

    /**
     * L'usine de configuration de sources de données.
     */
    protected final ObserveDataSourceConfigurationMainFactory dataSourceConfigurationFactory;

    /**
     * Le cache des configurations disponibles pour les couple (utilisateur#base) connus du système.
     *
     * @see #getUserKey(String, String)
     */
    protected ImmutableMap<String, ObserveDataSourceConfiguration> dataSourceConfigurationCache;

    /**
     * Le nom de la base par défaut à utiliser si elle n'est pas spécifiée.
     *
     * @see ObserveWebDatabases#getDefaultDatabase()
     */
    protected String defaultDatabaseName;

    protected ObserveWebDatabases<?> databases;

    protected ObserveWebUsers<?> users;

    public ObserveWebSecurityApplicationContext(ObserveWebApplicationConfiguration configuration) {
        this.authenticateCache = new ObserveWebSecurityAuthenticationTokenCache(
                configuration.getSessionMaximumSize(),
                configuration.getSessionExpirationDelay()
        );
        this.dataSourceConfigurationFactory = new ObserveDataSourceConfigurationMainFactory();
    }

    public synchronized void init(ObserveWebDatabases<?> databases, ObserveWebUsers<?> users, Version modelVersion) {
        this.databases = databases;
        this.users = users;

        authenticateCache.removeAllAuthenticationTokens();
        ObserveWebDatabase defaultDatabase = databases.getDefaultDatabase();
        Objects.requireNonNull(defaultDatabase);
        defaultDatabaseName = defaultDatabase.getName();

        ImmutableMap.Builder<String, ObserveDataSourceConfiguration> dataSourceConfigurationsCacheBuilder = new ImmutableMap.Builder<>();

        for (ObserveWebUser<?> observeWebUser : users.getUsers()) {

            for (ObserveWebUserPermission observeWebUserPermission : observeWebUser.getPermissions()) {

                String databaseName = observeWebUserPermission.getDatabase();
                Optional<? extends ObserveWebDatabase> optionalDatabase = databases.getDatabaseByName(databaseName);
                Preconditions.checkArgument(optionalDatabase.isPresent());

                ObserveWebDatabase<?> database = optionalDatabase.get();

                String role = observeWebUserPermission.getRole();
                Optional<? extends ObserveWebDatabaseRole> optionalDatabaseRole = database.getDatabaseRoleByLogin(role);
                Preconditions.checkArgument(optionalDatabaseRole.isPresent());
                ObserveWebDatabaseRole databaseRole = optionalDatabaseRole.get();

                String jdbcUrl = database.getUrl();
                String login = databaseRole.getLogin();
                String password = databaseRole.getPassword();
                String userKey = getUserKey(observeWebUser.getLogin(), databaseName);

                // Create DataSourceConfiguration
                ObserveDataSourceConfiguration configuration = dataSourceConfigurationFactory.createObserveDataSourceConfigurationTopiaPG(
                        userKey,
                        jdbcUrl,
                        login,
                        password.toCharArray(),
                        true,
                        true,
                        true,
                        modelVersion);


                if (log.isInfoEnabled()) {
                    log.info(String.format("Creates data source configuration for userKey %s : %s", userKey, configuration));
                }
                dataSourceConfigurationsCacheBuilder.put(userKey, configuration);

            }

        }

        dataSourceConfigurationCache = dataSourceConfigurationsCacheBuilder.build();

    }

    /**
     * Récupére la configuration de la source de données associée à l'utilisateur et à la base passée en paramètre.
     *
     * Si la base n'est pas spécifiée, on utilise alors la base par défaut du serveur.
     *
     * @param userLogin            le login de l'utilisateur
     * @param optionalDatabaseName le nom de la base à utiliser
     * @return la configuration de source de données associée à l'utilisateur et la base donnée
     * @throws UnknownObserveWebUserForDatabaseException
     */
    public ObserveDataSourceConfiguration getDataSourceConfiguration(String userLogin, Optional<String> optionalDatabaseName) throws UnknownObserveWebUserForDatabaseException {

        // Get database name
        String databaseName;
        if (optionalDatabaseName.isPresent()) {
            databaseName = optionalDatabaseName.get();
        } else {
            databaseName = defaultDatabaseName;
        }

        // Get data source configuration key cache
        String userKey = getUserKey(userLogin, databaseName);
        if (log.isInfoEnabled()) {
            log.info("Try to find data source configuration for: " + userKey);
        }

        // Get data source configuration
        ObserveDataSourceConfiguration dataSourceConfiguration = dataSourceConfigurationCache.get(userKey);
        if (dataSourceConfiguration == null) {

            // unknown userLogin - database
            throw new UnknownObserveWebUserForDatabaseException(databaseName, userLogin);
        }

        if (log.isInfoEnabled()) {
            log.info("Will use database configuration: " + dataSourceConfiguration);
        }

        return dataSourceConfiguration;

    }

    public String registerDataSourceConfiguration(ObserveDataSourceConfigurationAndConnection configurationAndConnection) {

        // Register data source configuration in cache
        return authenticateCache.registerDataSourceConfiguration(configurationAndConnection);

    }

    public ObserveDataSourceConfiguration getDataSourceConfiguration(String userLogin, String userPassword, Optional<String> optionalDatabaseName) {

        if (Strings.isNullOrEmpty(userLogin)) {
            throw new UserLoginNotFoundException();
        }

        if (Strings.isNullOrEmpty(userPassword)) {
            throw new UserPasswordNotFoundException();
        }

        // Get user
        Optional<? extends ObserveWebUser> optionalUser = users.getUserByLogin(userLogin);
        if (!optionalUser.isPresent()) {
            throw new UnknownObserveWebUserException(userLogin);
        }
        ObserveWebUser user = optionalUser.get();
        if (!Objects.equals(user.getPassword(), userPassword)) {
            throw new BadObserveWebUserPasswordException(userLogin, userPassword);
        }
        return getDataSourceConfiguration(userLogin, optionalDatabaseName);

    }

    /**
     * Pour récupérer la configuration de la data source à partir d'un jeton d'authentification.
     *
     * @param authenticationToken le jeton de sécurité
     * @return la configuration de la data source associée au jeton
     * @throws InvalidAuthenticationTokenException si le jeton n'est pas connu
     */
    public ObserveDataSourceConfigurationAndConnection getDataSourceConfigurationAndConnection(String authenticationToken) {
        ObserveDataSourceConfigurationAndConnection configurationAndConnection = authenticateCache.getDataSourceConfigurationAndConnectionIfPresent(authenticationToken);
        if (configurationAndConnection == null) {
            throw new InvalidAuthenticationTokenException(authenticationToken);
        }
        return configurationAndConnection;

    }

    public void invalidateAuthenticationToken(String authenticationToken) {
        authenticateCache.removeAuthenticationToken(authenticationToken);
    }

    public ImmutableMap<String, ObserveDataSourceConfigurationAndConnection> getConfigurationByAuthenticationToken() {
        return ImmutableMap.copyOf(authenticateCache.getAuthenticationTokenCache().asMap());
    }

    @Override
    public void close() {
        authenticateCache.close();
    }

    protected String getUserKey(String userLogin, String databaseName) {
        return userLogin + "--" + databaseName;
    }
}
