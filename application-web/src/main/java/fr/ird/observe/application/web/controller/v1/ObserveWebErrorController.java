package fr.ird.observe.application.web.controller.v1;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.security.ObserveWebSecurityExceptionSupport;
import fr.ird.observe.services.dto.UnauthorizedException;
import fr.ird.observe.services.http.ObserveHttpError;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.WebMotionException;
import org.debux.webmotion.server.call.HttpContext;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Pour gérer les erreurs.
 *
 * On retourne un rendu json avec le status http, l'erreur déclanchée...
 * Created on 07/09/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebErrorController extends WebMotionController {

    public ObserveHttpError error(HttpContext.ErrorData errorData) {

        @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
        Throwable exception = errorData.getException();

        if (exception instanceof WebMotionException) {

            if (exception.getCause() == exception) {
                exception.initCause(null);
            } else {
                exception = exception.getCause();
            }

        }

        if (exception instanceof InvocationTargetException) {

            exception = exception.getCause();

        }

        Integer statusCode = errorData.getStatusCode();

        String message = errorData.getMessage();

        if (exception != null) {

            Set<StackTraceElement> stackTraceElements = new LinkedHashSet<>();
            if (exception.getStackTrace() != null) {
                for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
                    if (stackTraceElement.getClassName().contains("sun.reflect.")) {
                        continue;
                    }
                    stackTraceElements.add(stackTraceElement);
                }
            }

            if (exception instanceof UnauthorizedException) {
                statusCode = 403;
            }

            if (exception instanceof ObserveWebSecurityExceptionSupport) {
                statusCode = 401;
            }

            exception.setStackTrace(stackTraceElements.toArray(new StackTraceElement[stackTraceElements.size()]));

            // FIXME sbavencoff 15/03/2016 si la cause est déjà initialisé il n'est pas possible de la changer
            //exception.initCause(null);

            message = exception.getMessage();

        }

        return new ObserveHttpError(statusCode,
                                    exception == null ? null : exception.getClass(),
                                    message,
                                    exception);

    }

}
