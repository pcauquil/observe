package fr.ird.observe.application.web.controller.v1.trip;

/*-
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.service.trip.DeleteTripRequest;
import fr.ird.observe.services.service.trip.DeleteTripResult;
import fr.ird.observe.services.service.trip.ExportTripRequest;
import fr.ird.observe.services.service.trip.ExportTripResult;
import fr.ird.observe.services.service.trip.ImportTripRequest;
import fr.ird.observe.services.service.trip.ImportTripResult;
import fr.ird.observe.services.service.trip.TripManagementService;

/**
 * Created on 20/07/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class TripManagementServiceController extends ObserveAuthenticatedServiceControllerSupport<TripManagementService> implements TripManagementService {

    public TripManagementServiceController() {
        super(TripManagementService.class);
    }

    @Override
    public ExportTripResult exportTrip(ExportTripRequest exportRequest) {
        return service.exportTrip(exportRequest);
    }

    @Override
    public DeleteTripResult deleteTrip(DeleteTripRequest request) {
        return service.deleteTrip(request);
    }

    @Override
    public ImportTripResult importTrip(ImportTripRequest request) {
        return service.importTrip(request);
    }

}
