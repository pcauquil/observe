package fr.ird.observe.application.web.controller.v1.seine;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.service.seine.FloatingObjectService;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FloatingObjectServiceController extends ObserveAuthenticatedServiceControllerSupport<FloatingObjectService> implements FloatingObjectService {

    public FloatingObjectServiceController() {
        super(FloatingObjectService.class);
    }

    @Override
    public DataReferenceSet<FloatingObjectDto> getFloatingObjectByActivitySeine(String activitySeineId) {
        return service.getFloatingObjectByActivitySeine(activitySeineId);
    }

    @Override
    public Form<FloatingObjectDto> loadForm(String floatingObjectId) {
        return service.loadForm(floatingObjectId);
    }

    @Override
    public FloatingObjectDto loadDto(String floatingObjectId) {
        return service.loadDto(floatingObjectId);
    }

    @Override
    public DataReference<FloatingObjectDto> loadReferenceToRead(String floatingObjectId) {
        return service.loadReferenceToRead(floatingObjectId);
    }

    @Override
    public boolean exists(String floatingObjectId) {
        return service.exists(floatingObjectId);
    }

    @Override
    public Form<FloatingObjectDto> preCreate(String activitySeineId) {
        return service.preCreate(activitySeineId);
    }

    @Override
    public SaveResultDto save(String activitySeineId, FloatingObjectDto dto) {
        return service.save(activitySeineId, dto);
    }

    @Override
    public void delete(String activitySeineId, String floatingObjectId) {
        service.delete(activitySeineId, floatingObjectId);
    }

}
