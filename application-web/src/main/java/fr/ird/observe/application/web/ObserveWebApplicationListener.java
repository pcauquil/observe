package fr.ird.observe.application.web;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.application.web.converter.ObserveDtoConverter;
import fr.ird.observe.application.web.injector.DateInjector;
import fr.ird.observe.application.web.injector.ImmutableSetInjector;
import fr.ird.observe.application.web.injector.ObserveClassInjector;
import fr.ird.observe.application.web.injector.ObserveDataSourceConfigurationInjector;
import fr.ird.observe.application.web.injector.ObserveDtoInjector;
import fr.ird.observe.application.web.injector.ObserveReferenceSetRequestInjector;
import fr.ird.observe.application.web.injector.SqlScriptProducerRequestInjector;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRestConstants;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionServerListener;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.mapping.Mapping;

/**
 * @author Tony Chemit - tchemit@codelutin.com
 */
public class ObserveWebApplicationListener implements WebMotionServerListener {

    private static final Log log = LogFactory.getLog(ObserveWebApplicationListener.class);

    protected ObserveWebApplicationContext applicationContext;

    @Override
    public void onStart(Mapping mapping, ServerContext context) {

        if (log.isInfoEnabled()) {
            log.info("Initializing " + ObserveWebApplicationListener.class.getName());
        }

        applicationContext = new ObserveWebApplicationContext();
        try {
            applicationContext.init();
        } catch (Exception e) {
            throw new ObserveWebApplicationContextInitException("Impossible d'initialiser le context applicatif", e);
        }

        Gson gson = applicationContext.getGsonSupplier().get();

        context.addInjector(new ObserveDataSourceConfigurationInjector(gson));
        context.addInjector(new ObserveClassInjector());
        context.addInjector(new ObserveDtoInjector(gson));
        context.addInjector(new ObserveReferenceSetRequestInjector(gson));
        context.addInjector(new DateInjector(ObserveDataSourceConfigurationRestConstants.DATE_PATTERN));
        context.addInjector(new SqlScriptProducerRequestInjector(gson));
        context.addInjector(new ImmutableSetInjector());
        context.addConverter(new ObserveDtoConverter(gson), ObserveDbUserDto.class);

        context.getServletContext().setAttribute(
                ObserveWebApplicationContext.APPLICATION_CONTEXT_PARAMETER, applicationContext);

    }

    @Override
    public void onStop(ServerContext context) {

        if (log.isInfoEnabled()) {
            log.info("Destroying " + ObserveWebApplicationListener.class.getName());
        }

        applicationContext.close();

    }

}
