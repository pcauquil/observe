package fr.ird.observe.application.web.controller.v1.actions.synchro.referential.diff;

/*-
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialMultimap;
import fr.ird.observe.services.dto.referential.ReferentialReferenceSet;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialDataSourceStates;
import fr.ird.observe.services.service.actions.synchro.referential.diff.ReferentialSynchronizeDiffService;

/**
 * Created on 10/08/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ReferentialSynchronizeDiffServiceController extends ObserveAuthenticatedServiceControllerSupport<ReferentialSynchronizeDiffService> implements ReferentialSynchronizeDiffService {
    public ReferentialSynchronizeDiffServiceController() {
        super(ReferentialSynchronizeDiffService.class);
    }

    @Override
    public <R extends ReferentialDto> ReferentialReferenceSet<R> getEnabledReferentialReferenceSet(Class<R> referentialName) {
        return service.getEnabledReferentialReferenceSet(referentialName);
    }

    @Override
    public <R extends ReferentialDto> ReferentialReferenceSet<R> getReferentialReferenceSet(Class<R> referentialName, ImmutableSet<String> ids) {
        return service.getReferentialReferenceSet(referentialName, ids);
    }

    @Override
    public <R extends ReferentialDto> ReferentialMultimap<R> getReferentials(Class<R> referentialName, ImmutableSet<String> ids) {
        return service.getReferentials(referentialName, ids);
    }

    @Override
    public ReferentialDataSourceStates getSourceReferentialStates() {
        return service.getSourceReferentialStates();
    }
}
