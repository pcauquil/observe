package fr.ird.observe.application.web;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.render.Render;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * To render any entities.
 *
 * @author Tony chemit - chemit@codelutin.com
 */
public class ObserveWebMotionRender<T> extends Render {

    protected final T model;

    public ObserveWebMotionRender(T model) {
        this.model = model;
    }

    @Override
    public void create(Mapping mapping, Call call) throws IOException, ServletException {

        HttpContext context = call.getContext();
        HttpServletResponse response = context.getResponse();
        response.setContentType("application/json");

        ObserveWebApplicationContext applicationContext =
                ObserveWebApplicationContext.getApplicationContext(context);

        Gson gson = applicationContext.getGsonSupplier().get();
        String json = gson.toJson(model);

        PrintWriter out = context.getOut();
        out.print(json);

    }

}
