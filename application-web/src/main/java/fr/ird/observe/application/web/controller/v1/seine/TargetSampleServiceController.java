package fr.ird.observe.application.web.controller.v1.seine;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.service.seine.TargetSampleService;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TargetSampleServiceController extends ObserveAuthenticatedServiceControllerSupport<TargetSampleService> implements TargetSampleService {

    public TargetSampleServiceController() {
        super(TargetSampleService.class);
    }

    @Override
    public boolean canUseTargetSample(String setSeineId, boolean discarded) {
        return service.canUseTargetSample(setSeineId, discarded);
    }

    @Override
    public Form<TargetSampleDto> loadForm(String setSeineId, boolean discarded) {
        return service.loadForm(setSeineId, discarded);
    }

    @Override
    public ImmutableList<ReferentialReference<SpeciesDto>> getSampleSpecies(String setSeineId, boolean discarded) {
        return service.getSampleSpecies(setSeineId, discarded);
    }

    @Override
    public SaveResultDto save(String setSeineId, TargetSampleDto dto) {
        return service.save(setSeineId, dto);
    }
}
