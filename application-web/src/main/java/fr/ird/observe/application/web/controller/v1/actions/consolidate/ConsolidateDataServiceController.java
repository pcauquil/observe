package fr.ird.observe.application.web.controller.v1.actions.consolidate;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataRequest;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataResult;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ConsolidateDataServiceController extends ObserveAuthenticatedServiceControllerSupport<ConsolidateDataService> implements ConsolidateDataService {

    public ConsolidateDataServiceController() {
        super(ConsolidateDataService.class);
    }

    @Override
    public ImmutableSet<ConsolidateTripSeineDataResult> consolidateTripSeines(ConsolidateTripSeineDataRequest consolidateTripSeineDataRequest) {
        return service.consolidateTripSeines(consolidateTripSeineDataRequest);
    }
}
