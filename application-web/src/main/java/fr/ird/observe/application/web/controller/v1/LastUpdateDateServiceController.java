package fr.ird.observe.application.web.controller.v1;

import fr.ird.observe.services.service.LastUpdateDateService;

/**
 * Created on 07/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 5.0
 */
public class LastUpdateDateServiceController extends ObserveServiceControllerSupport<LastUpdateDateService> implements LastUpdateDateService {

    public LastUpdateDateServiceController() {
        super(LastUpdateDateService.class);
    }

    @Override
    public void updateReferentialLastUpdateDates() {
        getAuthenticatedService().updateReferentialLastUpdateDates();
    }

    @Override
    public void updateDataLastUpdateDates() {
        getAuthenticatedService().updateDataLastUpdateDates();
    }
}
