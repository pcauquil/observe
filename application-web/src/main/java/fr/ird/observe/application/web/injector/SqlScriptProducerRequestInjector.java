package fr.ird.observe.application.web.injector;

/*-
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.services.service.AddSqlScriptProducerRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class SqlScriptProducerRequestInjector implements ExecutorParametersInjectorHandler.Injector {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SqlScriptProducerRequestInjector.class);

    protected final Gson gson;

    public SqlScriptProducerRequestInjector(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {

        AddSqlScriptProducerRequest addSqlScriptProducerRequest = null;

        if (type.equals(AddSqlScriptProducerRequest.class)) {

            Call.ParameterTree parameterTree = call.getParameterTree().getObject().get(name);

            Objects.requireNonNull(parameterTree, "Le paramètre " + name + " n'as pas été trouvé, recompiler (parameter)!");

            String gsonContent = ((String[]) parameterTree.getValue())[0];

            addSqlScriptProducerRequest = gson.fromJson(gsonContent, AddSqlScriptProducerRequest.class);

            if (log.isInfoEnabled()) {
                log.info("Inject addSqlScriptProducerRequest: " + addSqlScriptProducerRequest);
            }

        }

        return addSqlScriptProducerRequest;

    }

}
