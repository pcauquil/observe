package fr.ird.observe.application.web.controller.v1.longline;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.TripMapDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.longline.TripLonglineService;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class TripLonglineServiceController extends ObserveAuthenticatedServiceControllerSupport<TripLonglineService> implements TripLonglineService {

    public TripLonglineServiceController() {
        super(TripLonglineService.class);
    }

    @Override
    public DataReferenceSet<TripLonglineDto> getAllTripLongline() {
        return service.getAllTripLongline();
    }

    @Override
    public DataReferenceSet<TripLonglineDto> getTripLonglineByProgram(String programId) {
        return service.getTripLonglineByProgram(programId);
    }

    @Override
    public int getTripLonglinePositionInProgram(String programId, String tripLonglineId) {
        return service.getTripLonglinePositionInProgram(programId, tripLonglineId);
    }

    @Override
    public TripMapDto getTripLonglineMap(String tripLonglineId) {
        return service.getTripLonglineMap(tripLonglineId);
    }

    @Override
    public Form<TripLonglineDto> loadForm(String tripLonglineId) {
        return service.loadForm(tripLonglineId);
    }

    @Override
    public TripLonglineDto loadDto(String tripLonglineId) {
        return service.loadDto(tripLonglineId);
    }

    @Override
    public DataReference<TripLonglineDto> loadReferenceToRead(String tripLonglineId) {
        return service.loadReferenceToRead(tripLonglineId);
    }

    @Override
    public boolean exists(String tripLonglineId) {
        return service.exists(tripLonglineId);
    }

    @Override
    public Form<TripLonglineDto> preCreate(String programId) {
        return service.preCreate(programId);
    }

    @Override
    public SaveResultDto save(TripLonglineDto dto) {
        return service.save(dto);
    }

    @Override
    public void delete(String tripLonglineId) {
        service.delete(tripLonglineId);
    }

    @Override
    public int moveTripLonglineToProgram(String tripLonglineId, String programId) {
        return service.moveTripLonglineToProgram(tripLonglineId, programId);
    }

    @Override
    public List<Integer> moveTripLonglinesToProgram(List<String> tripLonglineIds, String programId) {
        return service.moveTripLonglinesToProgram(tripLonglineIds, programId);
    }

    @Override
    public ImmutableList<ReferentialReference<SpeciesDto>> getSpeciesByListAndTrip(String tripLonglineId, String speciesListId) {
        return service.getSpeciesByListAndTrip(tripLonglineId, speciesListId);
    }
}
