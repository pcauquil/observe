package fr.ird.observe.application.web.controller.v1;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import org.debux.webmotion.server.WebMotionContextable;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class ObserveAuthenticatedServiceControllerSupport<S extends ObserveService> extends ObserveServiceControllerSupport<S> {

    protected S service;

    protected ObserveAuthenticatedServiceControllerSupport(Class<S> serviceType) {
        super(serviceType);
    }

    @Override
    public void setContextable(WebMotionContextable contextable) {
        super.setContextable(contextable);
        service = getAuthenticatedService();
    }

    protected S getAnonymousService(ObserveDataSourceConfiguration dataSourceConfiguration) {
        throw new IllegalStateException("Can't get an anonymous service from this class");
    }

}
