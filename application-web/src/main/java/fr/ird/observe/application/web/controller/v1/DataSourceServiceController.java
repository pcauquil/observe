package fr.ird.observe.application.web.controller.v1;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.observe.application.web.request.ObserveWebRequestContext;
import fr.ird.observe.application.web.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfigurationAndConnection;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConnectionRest;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.dto.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseDestroyNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class DataSourceServiceController extends ObserveServiceControllerSupport<DataSourceService> implements DataSourceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataSourceServiceController.class);

    public DataSourceServiceController() {
        super(DataSourceService.class);
    }

    @Override
    public ObserveDataSourceInformation checkCanConnect(ObserveDataSourceConfiguration dataSourceConfiguration) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException {

        ObserveDataSourceConfiguration dataSourceConfigurationTopia = getTopiaDataSourceConfiguration(dataSourceConfiguration);

        return getAnonymousService(dataSourceConfigurationTopia).checkCanConnect(dataSourceConfigurationTopia);
    }

    @Override
    public ObserveDataSourceConnectionRest create(ObserveDataSourceConfiguration dataSourceConfiguration, DataSourceCreateConfigurationDto dataSourceCreateConfiguration)
            throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException,
            DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        ObserveDataSourceConfiguration dataSourceConfigurationTopia = getTopiaDataSourceConfiguration(dataSourceConfiguration);

        ObserveDataSourceConnection observeDataSourceConnection = getAnonymousService(dataSourceConfigurationTopia).create(dataSourceConfigurationTopia, dataSourceCreateConfiguration);

        String authenticationToken = registerDataSourceConfiguration(dataSourceConfigurationTopia, observeDataSourceConnection);
        return createDataSourceConnection(observeDataSourceConnection, authenticationToken);

    }

    @Override
    public ObserveDataSourceConnectionRest open(ObserveDataSourceConfiguration dataSourceConfiguration) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException {

        ObserveDataSourceConfiguration dataSourceConfigurationTopia = getTopiaDataSourceConfiguration(dataSourceConfiguration);

        ObserveDataSourceConnection observeDataSourceConnection = getAnonymousService(dataSourceConfigurationTopia).open(dataSourceConfigurationTopia);

        String authenticationToken = registerDataSourceConfiguration(dataSourceConfigurationTopia, observeDataSourceConnection);
        return createDataSourceConnection(observeDataSourceConnection, authenticationToken);

    }

    @Override
    public void close() {

        getAuthenticatedService().close();

        ObserveWebRequestContext requestContext = getRequestContext();
        String authenticationToken = requestContext.getAuthenticationToken();
        getSecurityApplicationContext().invalidateAuthenticationToken(authenticationToken);
        if (log.isInfoEnabled()) {
            log.info("Invalidate authenticationToken: " + authenticationToken);
        }

    }

    @Override
    public void destroy() throws DatabaseDestroyNotAuthorizedException {
        getAuthenticatedService().destroy();
    }

    @Override
    public void backup(File backupFile) {

    }

    @Override
    public Set<ObserveDbUserDto> getUsers(ObserveDataSourceConfiguration dataSourceConfiguration) {
        ObserveDataSourceConfiguration dataSourceConfigurationTopia = getTopiaDataSourceConfiguration(dataSourceConfiguration);
        return getAnonymousService(dataSourceConfigurationTopia).getUsers(dataSourceConfigurationTopia);
    }

    @Override
    public void applySecurity(ObserveDataSourceConfiguration dataSourceConfiguration, Set<ObserveDbUserDto> users) {
        ObserveDataSourceConfiguration dataSourceConfigurationTopia = getTopiaDataSourceConfiguration(dataSourceConfiguration);
        getAnonymousService(dataSourceConfigurationTopia).applySecurity(dataSourceConfigurationTopia, users);

    }

    @Override
    public void migrateData(ObserveDataSourceConfiguration dataSourceConfiguration) {
        ObserveDataSourceConfiguration dataSourceConfigurationTopia = getTopiaDataSourceConfiguration(dataSourceConfiguration);
        getAnonymousService(dataSourceConfigurationTopia).migrateData(dataSourceConfiguration);
    }

    @Override
    public Set<Class<? extends ReferentialDto>> getReferentialTypesInShell() {
        return getAuthenticatedService().getReferentialTypesInShell();
    }

    protected ObserveDataSourceConfiguration getTopiaDataSourceConfiguration(ObserveDataSourceConfiguration dataSourceConfigurationFromRequest) {

        Preconditions.checkArgument(dataSourceConfigurationFromRequest instanceof ObserveDataSourceConfigurationRest);
        ObserveDataSourceConfigurationRest dataSourceConfigurationRest = (ObserveDataSourceConfigurationRest) dataSourceConfigurationFromRequest;

        String login = dataSourceConfigurationRest.getLogin();

        String password = new String(dataSourceConfigurationRest.getPassword());

        Optional<String> optionalDatabaseName = dataSourceConfigurationRest.getOptionalDatabaseName();

        ObserveWebSecurityApplicationContext securityApplicationContext = getSecurityApplicationContext();
        return securityApplicationContext.getDataSourceConfiguration(login, password, optionalDatabaseName);

    }

    protected String registerDataSourceConfiguration(ObserveDataSourceConfiguration dataSourceConfiguration, ObserveDataSourceConnection dataSourceConnection) {

        ObserveDataSourceConfigurationAndConnection configurationAndConnection =
                new ObserveDataSourceConfigurationAndConnection(dataSourceConfiguration, dataSourceConnection);

        return getSecurityApplicationContext().registerDataSourceConfiguration(configurationAndConnection);

    }

    protected ObserveDataSourceConnectionRest createDataSourceConnection(ObserveDataSourceConnection observeDataSourceConnection, String authenticationToken) {

        return new ObserveDataSourceConnectionRest(
                getApplicationConfiguration().getApiUrl(),
                authenticationToken,
                observeDataSourceConnection.canReadReferential(),
                observeDataSourceConnection.canWriteReferential(),
                observeDataSourceConnection.canReadData(),
                observeDataSourceConnection.canWriteData(),
                observeDataSourceConnection.getVersion()
        );

    }

}
