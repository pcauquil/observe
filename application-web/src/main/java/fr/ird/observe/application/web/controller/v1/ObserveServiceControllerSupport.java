package fr.ird.observe.application.web.controller.v1;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.observe.application.web.controller.ObserveWebMotionController;
import fr.ird.observe.application.web.request.ObserveWebRequestContext;
import fr.ird.observe.application.web.security.ObserveWebSecurityApplicationContext;
import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConfigurationAndConnection;
import fr.ird.observe.services.spi.NoDataAccess;

import java.lang.reflect.Method;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class ObserveServiceControllerSupport<S extends ObserveService> extends ObserveWebMotionController implements ObserveService {

    protected final Class<S> serviceType;

    protected ObserveServiceControllerSupport(Class<S> serviceType) {
        this.serviceType = serviceType;
    }

    protected S getAuthenticatedService() {

        Method method = contextable.getCall().getCurrent().getMethod();
        Method serviceTypeMethod = getServiceMethod(method);
        boolean requiredAuthentication = serviceTypeMethod.getAnnotation(NoDataAccess.class) == null;
        Preconditions.checkState(requiredAuthentication, "Vous avez demandé un service authentifié, alors que l'annotation " + NoDataAccess.class.getName() + " est présente sur la méthode du service");

        ObserveWebRequestContext requestContext = getRequestContext();

        // On récupère le jeton d'authentification
        String authenticationToken = requestContext.getAuthenticationToken();

        // On recherche la source de données associée
        ObserveWebSecurityApplicationContext securityApplicationContext = getSecurityApplicationContext();
        ObserveDataSourceConfigurationAndConnection configurationAndConnection = securityApplicationContext.getDataSourceConfigurationAndConnection(authenticationToken);

        return requestContext.newService(serviceType, configurationAndConnection);
    }

    protected S getAnonymousService(ObserveDataSourceConfiguration dataSourceConfiguration) {

        Method method = contextable.getCall().getCurrent().getMethod();
        Method serviceTypeMethod = getServiceMethod(method);
        boolean requiredAuthentication = serviceTypeMethod.getAnnotation(NoDataAccess.class) == null;
        Preconditions.checkState(!requiredAuthentication, "Vous avez demandé un service anonyme, alors que l'annotation " + NoDataAccess.class.getName() + " n'est pas présente sur la méthode du service");

        ObserveWebRequestContext requestContext = getRequestContext();

        return requestContext.newService(serviceType, dataSourceConfiguration);
    }

    protected Method getServiceMethod(Method method) {
        try {
            return serviceType.getMethod(method.getName(), method.getParameterTypes());
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("Can't happen!");
        }
    }

}
