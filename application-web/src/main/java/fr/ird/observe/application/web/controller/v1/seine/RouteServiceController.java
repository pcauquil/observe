package fr.ird.observe.application.web.controller.v1.seine;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.web.controller.v1.ObserveAuthenticatedServiceControllerSupport;
import fr.ird.observe.services.dto.DataReference;
import fr.ird.observe.services.dto.DataReferenceSet;
import fr.ird.observe.services.dto.Form;
import fr.ird.observe.services.dto.result.TripChildSaveResultDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.service.seine.RouteService;

import java.util.List;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class RouteServiceController extends ObserveAuthenticatedServiceControllerSupport<RouteService> implements RouteService {

    public RouteServiceController() {
        super(RouteService.class);
    }

    @Override
    public DataReferenceSet<RouteDto> getRouteByTripSeine(String tripSeineId) {
        return service.getRouteByTripSeine(tripSeineId);
    }

    @Override
    public int getRoutePositionInTripSeine(String tripSeineId, String routeId) {
        return service.getRoutePositionInTripSeine(tripSeineId, routeId);
    }

    @Override
    public Form<RouteDto> loadForm(String routeId) {
        return service.loadForm(routeId);
    }

    @Override
    public RouteDto loadDto(String routeId) {
        return service.loadDto(routeId);
    }

    @Override
    public DataReference<RouteDto> loadReferenceToRead(String routeId) {
        return service.loadReferenceToRead(routeId);
    }

    @Override
    public boolean exists(String routeId) {
        return service.exists(routeId);
    }

    @Override
    public Form<RouteDto> preCreate(String tripSeineId) {
        return service.preCreate(tripSeineId);
    }

    @Override
    public TripChildSaveResultDto save(String tripSeineId, RouteDto dto) {
        return service.save(tripSeineId, dto);
    }

    @Override
    public boolean delete(String tripSeineId, String routeId) {
        return service.delete(tripSeineId, routeId);
    }

    @Override
    public int moveRouteToTripSeine(String routeId, String tripSeineId) {
        return service.moveRouteToTripSeine(routeId, tripSeineId);
    }

    @Override
    public List<Integer> moveRoutesToTripSeine(List<String> routeIds, String tripSeineId) {
        return service.moveRoutesToTripSeine(routeIds, tripSeineId);
    }
}
