package fr.ird.observe.application.web.configuration.user;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Iterables;
import com.google.common.io.Files;
import fr.ird.observe.application.web.configuration.ObserveWebApplicationConfiguration;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabasesHelper;
import fr.ird.observe.application.web.configuration.db.ObserveWebDatabasesHelperTest;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabasesImmutable;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUserBean;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUserImmutable;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUserPermissionBean;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUserPermissionImmutable;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUsersBean;
import fr.ird.observe.application.web.configuration.user.impl.ObserveWebUsersImmutable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebUsersHelperTest {

    public static final String FILE_CONTENT = "users: \n" +
                                              "- login: user1\n" +
                                              "  password: password1\n" +
                                              "  permissions: \n" +
                                              "  - database: production\n" +
                                              "    role: admin\n" +
                                              "  - database: test\n" +
                                              "    role: technicien\n" +
                                              "- login: user2\n" +
                                              "  password: password2\n" +
                                              "  permissions: \n" +
                                              "  - database: test\n" +
                                              "    role: referentiel\n";

    protected ObserveWebApplicationConfiguration configuration;

    protected ObserveWebUsersHelper observeWebUsersHelper;

    protected File usersConfigurationFile;

    @Before
    public void setUp() throws Exception {
        configuration = new ObserveWebApplicationConfiguration("observeweb-test.conf");
        configuration.init();
        observeWebUsersHelper = new ObserveWebUsersHelper();
        usersConfigurationFile = configuration.getUsersConfigurationFile();
    }

    @Test
    public void testLoad() throws Exception {

        File databasesConfigurationFileMock = ObserveWebDatabasesHelperTest.loadDatabasesConfigurationFileMock(configuration);
        ObserveWebDatabasesImmutable databases = new ObserveWebDatabasesHelper().load(databasesConfigurationFileMock);

        Files.write(FILE_CONTENT, usersConfigurationFile, Charsets.UTF_8);

        ObserveWebUsersImmutable users = observeWebUsersHelper.load(databases, usersConfigurationFile);
        Assert.assertNotNull(users);
        Collection<ObserveWebUserImmutable> usersSet = users.getUsers();
        Assert.assertNotNull(usersSet);
        Assert.assertEquals(2, usersSet.size());

        Optional<ObserveWebUserImmutable> production = users.getUserByLogin("user1");
        Assert.assertTrue(production.isPresent());

        Optional<ObserveWebUserImmutable> production2 = users.getUserByLogin("user3");
        Assert.assertFalse(production2.isPresent());

        {
            ObserveWebUserImmutable user = Iterables.get(usersSet, 0);
            Assert.assertNotNull(user);
            Assert.assertEquals("user1", user.getLogin());
            Assert.assertNotNull(user.getPermissions());
            Assert.assertEquals(2, user.getPermissions().size());

            Optional<ObserveWebUserPermissionImmutable> administrateur = user.getPermissionByDatabaseName("production");
            Assert.assertTrue(administrateur.isPresent());

            Optional<ObserveWebUserPermissionImmutable> administrateur2 = user.getPermissionByDatabaseName("administrateur2");
            Assert.assertFalse(administrateur2.isPresent());
        }
        {
            ObserveWebUserImmutable user = Iterables.get(usersSet, 1);
            Assert.assertNotNull(user);
            Assert.assertEquals("user2", user.getLogin());
            Assert.assertNotNull(user.getPermissions());
            Assert.assertEquals(1, user.getPermissions().size());

            Optional<ObserveWebUserPermissionImmutable> administrateur = user.getPermissionByDatabaseName("test");
            Assert.assertTrue(administrateur.isPresent());

        }

    }

    @Test
    public void testLoadBean() throws Exception {

        Files.write(FILE_CONTENT, usersConfigurationFile, Charsets.UTF_8);

        ObserveWebUsersBean users = observeWebUsersHelper.loadBean(usersConfigurationFile);
        Assert.assertNotNull(users);
        Set<ObserveWebUserBean> usersSet = users.getUsers();
        Assert.assertNotNull(usersSet);
        Assert.assertEquals(2, usersSet.size());

        {
            ObserveWebUser user = Iterables.get(usersSet, 0);
            Assert.assertNotNull(user);
            Assert.assertEquals("user1", user.getLogin());
            Assert.assertNotNull(user.getPermissions());
            Assert.assertEquals(2, user.getPermissions().size());
        }
        {
            ObserveWebUser user = Iterables.get(usersSet, 1);
            Assert.assertNotNull(user);
            Assert.assertEquals("user2", user.getLogin());
            Assert.assertNotNull(user.getPermissions());
            Assert.assertEquals(1, user.getPermissions().size());
        }
    }

    @Test
    public void testStoreBean() throws Exception {

        LinkedHashSet<ObserveWebUserBean> userSet = new LinkedHashSet<>();
        {
            ObserveWebUserBean user = new ObserveWebUserBean();
            user.setLogin("user1");
            user.setPassword("password1");
            LinkedHashSet<ObserveWebUserPermissionBean> permissions = new LinkedHashSet<>();
            {
                ObserveWebUserPermissionBean permission = new ObserveWebUserPermissionBean();
                permission.setDatabase("production");
                permission.setRole("admin");
                permissions.add(permission);
            }
            {
                ObserveWebUserPermissionBean permission = new ObserveWebUserPermissionBean();
                permission.setDatabase("test");
                permission.setRole("technicien");
                permissions.add(permission);
            }

            user.setPermissions(permissions);
            userSet.add(user);
        }
        {
            ObserveWebUserBean user = new ObserveWebUserBean();
            user.setLogin("user2");
            user.setPassword("password2");
            LinkedHashSet<ObserveWebUserPermissionBean> permissions = new LinkedHashSet<>();
            {
                ObserveWebUserPermissionBean permission = new ObserveWebUserPermissionBean();
                permission.setDatabase("test");
                permission.setRole("referentiel");
                permissions.add(permission);
            }

            user.setPermissions(permissions);
            userSet.add(user);
        }
        ObserveWebUsersBean users = new ObserveWebUsersBean();
        users.setUsers(userSet);

        observeWebUsersHelper.store(users, usersConfigurationFile);

        String fileContent = Files.asCharSource(usersConfigurationFile, Charsets.UTF_8).read();

        Assert.assertEquals(FILE_CONTENT, fileContent);
        System.out.println(fileContent);

        observeWebUsersHelper.store(users.toImmutable(), usersConfigurationFile);

        String fileContent2 = Files.asCharSource(usersConfigurationFile, Charsets.UTF_8).read();
        Assert.assertEquals(FILE_CONTENT, fileContent2);

    }
}
