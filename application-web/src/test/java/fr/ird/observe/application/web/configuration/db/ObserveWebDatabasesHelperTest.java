package fr.ird.observe.application.web.configuration.db;

/*
 * #%L
 * ObServe :: Application Web
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Iterables;
import com.google.common.io.Files;
import fr.ird.observe.application.web.configuration.ObserveWebApplicationConfiguration;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabaseBean;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabaseImmutable;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabaseRoleBean;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabaseRoleImmutable;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabasesBean;
import fr.ird.observe.application.web.configuration.db.impl.ObserveWebDatabasesImmutable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveWebDatabasesHelperTest {

    public static final String FILE_CONTENT = "databases: \n" +
                                              "- name: production\n" +
                                              "  defaultDatabase: true\n" +
                                              "  roles: \n" +
                                              "  - login: admin\n" +
                                              "    password: passwordAdmin\n" +
                                              "  - login: technicien\n" +
                                              "    password: passwordTechnicien\n" +
                                              "  - login: utilisateur\n" +
                                              "    password: passwordUtilisateur\n" +
                                              "  - login: referentiel\n" +
                                              "    password: passwordReferentiel\n" +
                                              "  url: jdbc:postgresql://localhost:5432/production\n" +
                                              "- name: test\n" +
                                              "  roles: \n" +
                                              "  - login: admin\n" +
                                              "    password: passwordAdmin\n" +
                                              "  - login: technicien\n" +
                                              "    password: passwordTechnicien\n" +
                                              "  - login: utilisateur\n" +
                                              "    password: passwordUtilisateur\n" +
                                              "  - login: referentiel\n" +
                                              "    password: passwordReferentiel\n" +
                                              "  url: jdbc:postgresql://localhost:5432/test\n";

    protected ObserveWebApplicationConfiguration configuration;

    protected ObserveWebDatabasesHelper observeWebDatabasesHelper;

    protected File databasesConfigurationFile;

    public static File loadDatabasesConfigurationFileMock(ObserveWebApplicationConfiguration configuration) throws IOException {
        File databasesConfigurationFile = configuration.getDatabasesConfigurationFile();
        Files.write(FILE_CONTENT, databasesConfigurationFile, Charsets.UTF_8);
        return databasesConfigurationFile;
    }

    @Before
    public void setUp() {
        configuration = new ObserveWebApplicationConfiguration("observeweb-test.conf");
        configuration.init();
        observeWebDatabasesHelper = new ObserveWebDatabasesHelper();
        databasesConfigurationFile = configuration.getDatabasesConfigurationFile();
    }

    @Test
    public void testLoadBean() throws Exception {

        loadDatabasesConfigurationFileMock(configuration);

        ObserveWebDatabasesBean databases = observeWebDatabasesHelper.loadBean(databasesConfigurationFile);
        Assert.assertNotNull(databases);
        Set<ObserveWebDatabaseBean> databasesSet = databases.getDatabases();
        Assert.assertNotNull(databasesSet);
        Assert.assertEquals(2, databasesSet.size());

        {
            ObserveWebDatabase database = Iterables.get(databasesSet, 0);
            Assert.assertNotNull(database);
            Assert.assertEquals("production", database.getName());
            Assert.assertNotNull(database.getRoles());
            Assert.assertEquals(4, database.getRoles().size());
        }
        {
            ObserveWebDatabase database = Iterables.get(databasesSet, 1);
            Assert.assertNotNull(database);
            Assert.assertEquals("test", database.getName());
            Assert.assertNotNull(database.getRoles());
            Assert.assertEquals(4, database.getRoles().size());
        }

    }

    @Test
    public void testLoad() throws Exception {

        loadDatabasesConfigurationFileMock(configuration);

        ObserveWebDatabasesImmutable databases = observeWebDatabasesHelper.load(databasesConfigurationFile);
        Assert.assertNotNull(databases);
        Collection<ObserveWebDatabaseImmutable> databasesSet = databases.getDatabases();
        Assert.assertNotNull(databasesSet);
        Assert.assertEquals(2, databasesSet.size());

        ObserveWebDatabaseImmutable defaultDatabase = databases.getDefaultDatabase();
        Assert.assertNotNull(defaultDatabase);
        Assert.assertEquals("production", defaultDatabase.getName());

        Optional<ObserveWebDatabaseImmutable> production = databases.getDatabaseByName("production");
        Assert.assertTrue(production.isPresent());

        Optional<ObserveWebDatabaseImmutable> production2 = databases.getDatabaseByName("production2");
        Assert.assertFalse(production2.isPresent());

        {
            ObserveWebDatabaseImmutable database = Iterables.get(databasesSet, 0);
            Assert.assertNotNull(database);
            Assert.assertEquals("production", database.getName());
            Assert.assertNotNull(database.getRoles());
            Assert.assertEquals(4, database.getRoles().size());

            Optional<ObserveWebDatabaseRoleImmutable> administrateur = database.getDatabaseRoleByLogin("admin");
            Assert.assertTrue(administrateur.isPresent());

            Optional<ObserveWebDatabaseRoleImmutable> administrateur2 = database.getDatabaseRoleByLogin("administrateur2");
            Assert.assertFalse(administrateur2.isPresent());
        }
        {
            ObserveWebDatabaseImmutable database = Iterables.get(databasesSet, 1);
            Assert.assertNotNull(database);
            Assert.assertEquals("test", database.getName());
            Assert.assertNotNull(database.getRoles());
            Assert.assertEquals(4, database.getRoles().size());

            Optional<ObserveWebDatabaseRoleImmutable> administrateur = database.getDatabaseRoleByLogin("admin");
            Assert.assertTrue(administrateur.isPresent());

            Optional<ObserveWebDatabaseRoleImmutable> administrateur2 = database.getDatabaseRoleByLogin("administrateur2");
            Assert.assertFalse(administrateur2.isPresent());
        }

    }


    @Test
    public void testStoreBean() throws Exception {

        LinkedHashSet<ObserveWebDatabaseBean> databaseSet = new LinkedHashSet<>();
        {
            ObserveWebDatabaseBean database = new ObserveWebDatabaseBean();
            database.setDefaultDatabase(true);
            database.setName("production");
            database.setUrl("jdbc:postgresql://localhost:5432/production");
            LinkedHashSet<ObserveWebDatabaseRoleBean> roles = new LinkedHashSet<>();
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("admin");
                databaseRole.setPassword("passwordAdmin");
                roles.add(databaseRole);
            }
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("technicien");
                databaseRole.setPassword("passwordTechnicien");
                roles.add(databaseRole);
            }
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("utilisateur");
                databaseRole.setPassword("passwordUtilisateur");
                roles.add(databaseRole);
            }
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("referentiel");
                databaseRole.setPassword("passwordReferentiel");
                roles.add(databaseRole);
            }
            database.setRoles(roles);
            databaseSet.add(database);
        }
        {
            ObserveWebDatabaseBean database = new ObserveWebDatabaseBean();
            database.setDefaultDatabase(false);
            database.setName("test");
            database.setUrl("jdbc:postgresql://localhost:5432/test");
            LinkedHashSet<ObserveWebDatabaseRoleBean> roles = new LinkedHashSet<>();
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("admin");
                databaseRole.setPassword("passwordAdmin");
                roles.add(databaseRole);
            }
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("technicien");
                databaseRole.setPassword("passwordTechnicien");
                roles.add(databaseRole);
            }
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("utilisateur");
                databaseRole.setPassword("passwordUtilisateur");
                roles.add(databaseRole);
            }
            {
                ObserveWebDatabaseRoleBean databaseRole = new ObserveWebDatabaseRoleBean();
                databaseRole.setLogin("referentiel");
                databaseRole.setPassword("passwordReferentiel");
                roles.add(databaseRole);
            }
            database.setRoles(roles);
            databaseSet.add(database);
        }
        ObserveWebDatabasesBean databases = new ObserveWebDatabasesBean();
        databases.setDatabases(databaseSet);

        observeWebDatabasesHelper.store(databases, databasesConfigurationFile);

        String fileContent = Files.asCharSource(databasesConfigurationFile, Charsets.UTF_8).read();

        Assert.assertEquals(FILE_CONTENT, fileContent);
        System.out.println(fileContent);

        observeWebDatabasesHelper.store(databases.toImmutable(), databasesConfigurationFile);

        String fileContent2 = Files.asCharSource(databasesConfigurationFile, Charsets.UTF_8).read();
        Assert.assertEquals(FILE_CONTENT, fileContent2);

    }
}
