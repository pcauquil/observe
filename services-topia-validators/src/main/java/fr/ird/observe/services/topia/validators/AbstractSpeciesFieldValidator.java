package fr.ird.observe.services.topia.validators;

/*
 * #%L
 * ObServe :: Services ToPIA Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.entities.referentiel.Species;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <!-- START SNIPPET: javadoc -->
 * Ce validateur verifie qu'une espece respece bien les tailles ou
 * les poids définis par les bornes de l'espece :
 * <ul>
 * <li>minLength</li>
 * <li>maxLength</li>
 * <li>minWeight</li>
 * <li>maxWeight</li>
 * </ul>
 *
 * Lorsqu'il s'agit d'une espece faune, si aucune borne n'est trouvée, alors
 * on se base sur les bornes définis dans son groupe d'espece (si il est défini).
 *
 * Le paramètre {@link #ratio} permet de spécifier une marge à appliquer sur
 * les bornes, il s'agit d'un pourcentage décimal.
 *
 * Example : si ratio = 10, alors on utilise les bornes suivantes :
 * <pre>
 * bMin -10% et bMax + 10%
 * </pre>
 * <!-- END SNIPPET: javadoc -->
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public abstract class AbstractSpeciesFieldValidator extends FieldValidatorSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractSpeciesFieldValidator.class);

    public static class Bound {

        private final Float min;

        private final Float max;

        Bound(Float min, Float max) {
            this.min = min;
            this.max = max;
        }

        public Float getMin() {
            return min;
        }

        public Float getMax() {
            return max;
        }

        public Bound applyRatio(float ratio) {
            float delta = min / 100 * ratio;
            float min = this.min - delta;
            if (min < 0) {
                min = 0f;
            }
            delta = max / 100 * ratio;
            float max = this.max + delta;
            return new Bound(min, max);
        }

        @Override
        public String toString() {
            return super.toString() + '<' + min + ',' + max + '>';
        }
    }

    /**
     * la stack de validation interceptée lors de la création du validateur.
     *
     * Utilisée pour pousser des données dans le context.
     */
    protected ValueStack valueStack;

    /** le ratio a appliquer sur les bornes définies dans le référentiel */
    protected Float ratio;

    /**
     * Une expression qui si elle est remplie doit être vérifié avant de faire
     * la validation par borne, si l'expression n'est pas vérifiée, alors
     * le test sur les borne n'est pas effectué.
     *
     * @since 2.3
     */
    protected String expression;

    protected String speciesField = "espece";

    public ValueStack getValueStack() {
        return valueStack;
    }

    public String getSpeciesField() {
        return speciesField;
    }

    @Override
    public void setValueStack(ValueStack valueStack) {
        this.valueStack = valueStack;
        super.setValueStack(valueStack);
    }

    public void setSpeciesField(String speciesField) {
        this.speciesField = speciesField;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    protected abstract Float getBoundMin(Species referentiel);

    protected abstract Float getBoundMax(Species referentiel);

    protected boolean shouldValidate(Object object) throws ValidationException {
        Object obj = null;
        Boolean answer;
        if (StringUtils.isNotEmpty(expression)) {
            try {
                obj = getFieldValue(expression, object);
            } catch (ValidationException e) {
                throw e;
            } catch (Exception e) {
                // let this pass, but it will be logged right below
            }

            if (obj != null && obj instanceof Boolean) {
                answer = (Boolean) obj;
            } else {
                answer = false;
                if (log.isWarnEnabled()) {
                    log.warn("Got result of " + obj +
                             " when trying to get Boolean with expression [" +
                             expression + "].");
                }
            }
        } else {

            // no pre-expression, always wants to validate
            answer = true;
        }

        return answer;
    }

    @Override
    public void validate(Object object) throws ValidationException {

        if (ratio == null) {
            throw new ValidationException("No parameter 'ratio' filled");
        }

        String fieldName = getFieldName();
        if (fieldName == null) {
            throw new ValidationException("No parameter 'fieldName' filled");
        }

        String speciesFieldName = getSpeciesField();
        if (speciesFieldName == null) {
            throw new ValidationException("No parameter 'speciesFieldName' filled");
        }

        boolean shouldValidate = shouldValidate(object);

        if (!shouldValidate) {
            return;
        }

        // la donnee a valider
        Object value = getFieldValue(fieldName, object);
        Float data = value == null ? null : Float.valueOf(String.valueOf(value));

        if (data == null) {
            // la donnee a valider n'est pas définie
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("data to validate : " + data);
        }

        // l'species associée
        Species species = (Species) getFieldValue(speciesFieldName, object);

        if (species == null) {

            // pas de species trouvée, on ne peut pas valider
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("Espece to validate : " + species);
        }

        Bound bound = getBound(species);

        if (log.isDebugEnabled()) {
            log.debug("Espece Bound to validate : " + bound);
        }

        if (bound == null) {

            // pas de donnée dans le référentiel acceptable
            return;
        }

        Bound boundWithRatio = bound.applyRatio(ratio);

        if (log.isInfoEnabled()) {
            log.info("Bound             : " + bound);
            log.info("Ratio to validate : " + ratio);
            log.info("Bound with ratio  : " + boundWithRatio);
        }

        boolean valid = validateBound(data, boundWithRatio);

        if (!valid) {

            valueStack.push(bound);

            try {
                addFieldError(fieldName, object);
            } finally {
                valueStack.pop();
            }
        }
    }

    protected Bound getBound(Species referentiel) {

        Float min = getBoundMin(referentiel);
        Float max = getBoundMax(referentiel);

        if (min == null || min == 0 || max == null || max == 0) {
            // l'une des deux borne n'est pas définie, on ne peut pas utiliser
            // la données
            return null;
        }
        return new Bound(min, max);
    }

    protected boolean validateBound(Float value,
                                    Bound bound) {
        if (value == null) {

            // valeur non définie
            return true;
        }
        boolean valid;

        float min = bound.getMin();
        float max = bound.getMax();

        valid = min <= value && value <= max;
        return valid;
    }

}
