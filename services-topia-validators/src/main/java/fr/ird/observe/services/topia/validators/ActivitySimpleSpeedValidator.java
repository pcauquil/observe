package fr.ird.observe.services.topia.validators;

/*
 * #%L
 * ObServe :: Services ToPIA Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.entities.seine.ActivitySeine;
import fr.ird.observe.entities.seine.ActivitySeines;
import fr.ird.observe.entities.seine.Route;
import fr.ird.observe.util.GPSPoint;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <!-- START SNIPPET: javadoc --> ActivitySimpleSpeedValidator vérifie que
 * la cohérence de vitesse entre l'activité courante et sa précédente. <!-- END SNIPPET: javadoc
 * -->
 *
 *
 * <!-- START SNIPPET: parameters --> <ul> <li>fieldName - The field name this
 * validator is validating. Required if using Plain-Validator Syntax otherwise
 * not required</li> </ul> <!-- END SNIPPET: parameters -->
 *
 * <pre>
 * <!-- START SNIPPET: examples -->
 *     &lt;validators&gt;
 *         &lt;!-- Plain-Validator Syntax --&gt;
 *         &lt;validator type="invalidLochMatin"&gt;
 *             &lt;param name="fieldName"&gt;startLogValue&lt;/param&gt;
 *             &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *         &lt;/validator&gt;
 *
 *         &lt;!-- Field-Validator Syntax --&gt;
 *         &lt;field name="startLogValue"&gt;
 *         	  &lt;field-validator type="invalidLochMatin"&gt;
 *                 &lt;message&gt;loch matin must be greater or equals to last
 * closed route loch soir&lt;/message&gt;
 *            &lt;/field-validator&gt;
 *         &lt;/field&gt;
 *     &lt;/validators&gt;
 * <!-- END SNIPPET: examples -->
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ActivitySimpleSpeedValidator extends FieldValidatorSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ActivitySimpleSpeedValidator.class);

    private Float speed;

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public void setValueStack(ValueStack stack) {
        this.stack = stack;
        super.setValueStack(stack);
    }

    @Override
    public void validate(Object object) throws ValidationException {

        if (speed == null) {
            throw new ValidationException("le parametre speed est obligatoire");
        }

        if (object == null) {

            // pas d'objet, donc rien a faire
            return;
        }

        ActivitySeine activity = (ActivitySeine) object;

        if (activity.getTime() == null) {

            // heure d'observation non encore positionne, on ne peut pas valider
            if (log.isDebugEnabled()) {
                log.debug("Missing time on current activity : " + decorate(activity) + ", skip speed computation");
            }
            return;
        }


        if (activity.getLatitude() == null ||
            activity.getLongitude() == null) {

            //  pas de position, on ne peut pas valider
            if (log.isDebugEnabled()) {
                log.debug("Missing latitude or longitude on current activity : " + decorate(activity) + ", skip speed computation");
            }
            return;
        }

        Route route = (Route) stack.findValue("routeEntity");

        ActivitySeine previousActivity = ActivitySeines.getPreviousActivity(route, activity);

        if (previousActivity == null) {

            // pas d'activity avant, rien à valider
            if (log.isDebugEnabled()) {
                log.debug("No previous activity for current activity : " + decorate(activity) + ", skip speed computation");
            }
            return;
        }

        if (previousActivity.getLatitude() == null ||
            previousActivity.getLongitude() == null) {

            //  pas de position, on ne peut pas valider
            if (log.isDebugEnabled()) {
                log.debug("Missing latitude or longitude on previous activity : " + decorate(previousActivity) + ", skip speed computation");
            }
            return;
        }

        GPSPoint currentPoint = GPSPoint.newPoint(route.getDate(), activity.getTime(), activity.getLatitude(), activity.getLongitude());
        GPSPoint previousPoint = GPSPoint.newPoint(route.getDate(), previousActivity.getTime(), previousActivity.getLatitude(), previousActivity.getLongitude());

        float computedSpeed = previousPoint.getSpeed(currentPoint);

        if (log.isDebugEnabled()) {
            log.debug("Speed computed between previous activity point " + decorate(previousPoint) + " to current activity point " + decorate(currentPoint) + ", speed is : " + computedSpeed);
        }
        boolean b = computedSpeed <= speed;

        if (!b) {

            stack.set("foundSpeed", computedSpeed);

            // vitesse trop grande
            addFieldError(getFieldName(), object);
        }
    }

    @Override
    public String getValidatorType() {
        return "activitySimpleSpeed";
    }

    //FIXME
    protected String decorate(ActivitySeine activitySeine) {
        return activitySeine.toString();
    }

    //FIXME
    protected String decorate(GPSPoint currentPoint) {
        return currentPoint.toString();
    }

}
