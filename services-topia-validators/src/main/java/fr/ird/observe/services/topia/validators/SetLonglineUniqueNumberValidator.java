package fr.ird.observe.services.topia.validators;

/*
 * #%L
 * ObServe :: Services ToPIA Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import fr.ird.observe.entities.longline.ActivityLongline;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.entities.longline.TripLongline;

import java.util.Objects;
import java.util.Set;

/**
 * Created on 12/7/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class SetLonglineUniqueNumberValidator extends FieldValidatorSupport {

    @Override
    public void setValueStack(ValueStack stack) {
        this.stack = stack;
        super.setValueStack(stack);
    }

    @Override
    public void validate(Object object) throws ValidationException {

        SetLongline setLongline = (SetLongline) object;
        Integer number = setLongline.getNumber();

        if (number != null) {

            String setLonglineTopiaId = setLongline.getTopiaId();

            TripLongline tripLongline = (TripLongline) stack.findValue("tripEntity");

            Set<ActivityLongline> activityLonglines = tripLongline.getActivityLongline();

            boolean notValid = false;

            for (ActivityLongline activityLongline : activityLonglines) {

                SetLongline setLongline1 = activityLongline.getSetLongline();


                if (setLongline1 != null
                    && !Objects.equals(setLonglineTopiaId, setLongline1.getTopiaId())
                    && number.equals(setLongline1.getNumber())) {

                    notValid = true;

                    stack.set("duplicatedActivity", activityLongline);

                    break;

                }

            }

            if (notValid) {

                // vitesse trop grande
                addFieldError(getFieldName(), object);
            }
        }


    }

    @Override
    public String getValidatorType() {
        return "setLonglineUniqueNumber";
    }
}
