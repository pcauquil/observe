package fr.ird.observe.services.topia.validators;

/*
 * #%L
 * ObServe :: Services ToPIA Validators
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;

/**
 * Validateur sur le weight d'une species.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5
 */
public class SpeciesWeightFieldValidator extends AbstractSpeciesFieldValidator {

    @Override
    protected Float getBoundMin(Species referentiel) {
        return referentiel.getMinWeight();
    }

    @Override
    protected Float getBoundMax(Species referentiel) {
        return referentiel.getMaxWeight();
    }

    @Override
    public String getValidatorType() {
        return "species_weight";
    }
}
