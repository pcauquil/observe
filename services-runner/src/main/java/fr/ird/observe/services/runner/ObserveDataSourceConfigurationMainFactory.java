package fr.ird.observe.services.runner;

/*
 * #%L
 * ObServe :: Services Runner
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import org.nuiton.version.Version;

import java.io.File;
import java.net.URL;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveDataSourceConfigurationMainFactory {

    public ObserveDataSourceConfigurationTopiaPG createObserveDataSourceConfigurationTopiaPG(String label,
                                                                                             String jdbcUrl,
                                                                                             String username,
                                                                                             char[] password,
                                                                                             boolean useSsl,
                                                                                             boolean showMigrationProgression,
                                                                                             boolean showMigrationSql,
                                                                                             Version dbVersion) {

        ObserveDataSourceConfigurationTopiaPG result = new ObserveDataSourceConfigurationTopiaPG();
        result.setLabel(label);
        result.setJdbcUrl(jdbcUrl);
        result.setUsername(username);
        result.setPassword(password);
        result.setUseSsl(useSsl);
        result.setShowMigrationProgression(showMigrationProgression);
        result.setShowMigrationSql(showMigrationSql);
        result.setModelVersion(dbVersion);

        return result;

    }

    public ObserveDataSourceConfigurationTopiaH2 createObserveDataSourceConfigurationTopiaH2(String label,
                                                                                             File directory,
                                                                                             String databaseName,
                                                                                             String username,
                                                                                             char[] password,
                                                                                             boolean showMigrationProgression,
                                                                                             boolean showMigrationSql,
                                                                                             Version dbVersion) {

        ObserveDataSourceConfigurationTopiaH2 result = new ObserveDataSourceConfigurationTopiaH2();
        result.setLabel(label);
        result.setDbName(databaseName);
        result.setUsername(username);
        result.setPassword(password);
        result.setDirectory(directory);
        result.setShowMigrationProgression(showMigrationProgression);
        result.setShowMigrationSql(showMigrationSql);
        result.setModelVersion(dbVersion);

        return result;

    }

    public ObserveDataSourceConfigurationRest createObserveDataSourceConfigurationRest(String label,
                                                                                       URL serverUrl,
                                                                                       String login,
                                                                                       char[] password,
                                                                                       String databaseName,
                                                                                       Version dbVersion) {

        ObserveDataSourceConfigurationRest result = new ObserveDataSourceConfigurationRest();
        result.setLabel(label);
        result.setServerUrl(serverUrl);
        result.setLogin(login);
        result.setPassword(password);
        result.setOptionalDatabaseName(databaseName);
        result.setModelVersion(dbVersion);

        return result;

    }

}

