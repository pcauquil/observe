package fr.ird.observe.services.runner;

/*
 * #%L
 * ObServe :: Services Runner
 * %%
 * Copyright (C) 2008 - 2016 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.ObserveService;
import fr.ird.observe.services.ObserveServiceFactory;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ObserveServiceMainFactory implements ObserveServiceFactory {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObserveServiceMainFactory.class);

    private static final ObserveServiceMainFactory GET = new ObserveServiceMainFactory();

    private final Set<ObserveServiceFactory> delegateFactories;

    public static ObserveServiceMainFactory get() {
        return GET;
    }

    @Override
    public ObserveServiceFactory getMainServiceFactory() {
        return this;
    }

    @Override
    public void setMainServiceFactory(ObserveServiceFactory mainServiceFactory) {
        // Rien à faire on est déjà sur l'usine principale
    }

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {

        ObserveServiceFactory factory = getFactory(dataSourceConfiguration, serviceType);
        return factory != null;

    }

    @Override
    public <S extends ObserveService> boolean accept(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {

        ObserveServiceFactory factory = getFactory(dataSourceConnection, serviceType);
        return factory != null;

    }

    @Override
    public <S extends ObserveService> S newService(ObserveServiceInitializer observeServiceInitializer, Class<S> serviceType) {

        Objects.requireNonNull(observeServiceInitializer, "observeServiceInitializerContext can't be null.");
        Objects.requireNonNull(serviceType, "serviceType can't be null.");

        ObserveServiceFactory factory;
        if (observeServiceInitializer.withDataSourceConnection()) {
            factory = getFactory(observeServiceInitializer.getDataSourceConnection(), serviceType);
        } else if (observeServiceInitializer.withDataSourceConfiguration()) {
            factory = getFactory(observeServiceInitializer.getDataSourceConfiguration(), serviceType);
        } else {
            throw new IllegalStateException("No dataSourceConnection, nor dataSourceConfiguration given.");
        }

        Objects.requireNonNull(factory, "factory can't be null.");

        if (log.isDebugEnabled()) {
            log.debug("Using factory: " + factory);
        }

        S service = factory.newService(observeServiceInitializer, serviceType);

        if (log.isInfoEnabled()) {
            log.info("New service created: " + service);
        }

        return service;

    }

    @Override
    public void close() {

        delegateFactories.forEach(ObserveServiceFactory::close);

    }

    private ObserveServiceMainFactory() {

        if (log.isInfoEnabled()) {
            log.info("Init MainServiceFactory.");
        }
        Set<ObserveServiceFactory> builder = new LinkedHashSet<>();
        Set<Class<? extends ObserveServiceFactory>> factoryTypes = new Reflections("fr.ird.observe.services").getSubTypesOf(ObserveServiceFactory.class);

        factoryTypes.stream()
                    .filter(factoryType -> !Modifier.isAbstract(factoryType.getModifiers()))
                    .filter(factoryType -> !ObserveServiceMainFactory.class.equals(factoryType))
                    .forEach(factoryType -> {
                        try {
                            ObserveServiceFactory factory = factoryType.newInstance();
                            if (log.isInfoEnabled()) {
                                log.info("Found service factory: " + factory);
                            }
                            factory.setMainServiceFactory(this);
                            builder.add(factory);
                        } catch (InstantiationException | IllegalAccessException e) {
                            throw new ExceptionInInitializerError(e);
                        }
                    });
        delegateFactories = Collections.unmodifiableSet(builder);

    }

    private <S extends ObserveService> ObserveServiceFactory getFactory(ObserveDataSourceConfiguration dataSourceConfiguration, Class<S> serviceType) {

        Objects.requireNonNull(dataSourceConfiguration);
        Objects.requireNonNull(serviceType);

        ObserveServiceFactory result = null;
        for (ObserveServiceFactory serviceFactory : delegateFactories) {
            if (serviceFactory.accept(dataSourceConfiguration, serviceType)) {
                result = serviceFactory;
                break;
            }
        }

        Objects.requireNonNull(result, String.format("No factory found for dataSourceConfiguration: %s and serviceType: %s", dataSourceConfiguration, serviceType.getName()));
        return result;

    }

    private <S extends ObserveService> ObserveServiceFactory getFactory(ObserveDataSourceConnection dataSourceConnection, Class<S> serviceType) {

        Objects.requireNonNull(dataSourceConnection);
        Objects.requireNonNull(serviceType);

        ObserveServiceFactory result = null;
        for (ObserveServiceFactory serviceFactory : delegateFactories) {
            if (serviceFactory.accept(dataSourceConnection, serviceType)) {
                result = serviceFactory;
                break;
            }
        }

        Objects.requireNonNull(result, String.format("No factory found for dataSourceConnection: %s and serviceType: %s", dataSourceConnection, serviceType.getName()));
        return result;

    }

}
